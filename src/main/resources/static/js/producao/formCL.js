//var anoDigitado = $('#ano').val();
//var ano2 = '2019';
//console.log(anoDigitado);

//$('#ano').change(function() {
//if(anoDigitado == ano2){
//$('#ano').val(' ');
//Materialize.toast('O ano fiscal já é existente.',10000, 'rounded');
//}
//});

$(function() {
	//$("#btn-add-producao").prop("disabled", true);

	$("#situacao").hide();
	$("#razaoInatividade").hide();
	
	$("#form").submit(function () {
        $("#btn-add-producao").attr("disabled", true);
        return true;
    });
	
})

var timer;
$("#ano").on("blur", function() {
	if ($("#ano").val() == "") {
		console.log("Trigger false")
		$("#btn-add-producao").prop("disabled", true);
	} else {
		var searchid = $(this).val().trim();
		clearInterval(timer);
		timer = setTimeout(function() {
			var disabled = checkVigencia($("#ano").val());
			//checkForProducaoExistente(disabled);
		}, 500);
	}
});

function checkForProducaoExistente(disabled) {

	var ano = $("#ano").val();
	var cliente = $("#clienteId").val();
	var linha = $('input[name=contratoId]:checked')
	var produto = linha.parent().parent().parent().find('#produtoId').text();

	var urlCheck = "/producoes/" + cliente + "/" + produto + "/" + ano
			+ "/prod.json"

	$.ajax({
		url : urlCheck,
		type : 'GET',
		dataType : 'json',
		success : function(d) {
			if (d == "0") {
				if (disabled == false) {
					$("#btn-add-producao").attr("disabled", false);
				}
			} else {
				Materialize.toast(
						'Produção duplicada para o ano e produto informados.',
						10000, 'rounded');
				$("#btn-add-producao").attr("disabled", true);
			}
		},
	});

	// if( data !== "null" && data !== "undefined" ){
	// Materialize.toast('Produção existente encontrada para o ano/produto.',
	// 10000, 'rounded');
	// $("#btn-add-producao").attr("disabled", true);
	// } else {
	// Materialize.toast('OK', 10000, 'rounded');
	// $("#btn-add-producao").attr("disabled", false);
	// }

}

function checkVigencia(ano) {

	var selecionado = $('input[name=contratoId]:checked', '#form').parent()
			.parent().parent();

	if (selecionado != null) {
		if ($("#fim").val() == null) {
		} else {

			var iniVig = selecionado.children().eq(2).text().substr(6, 9);
			var fimVig = selecionado.children().eq(3).text().substr(6, 9);

			if (iniVig != "") {

				if (fimVig == "" || fimVig == 'RMINADA') {
					fimVig = 3000
				}
				if (ano <= fimVig && ano >= iniVig) {
					Materialize.toast(
							'Ano inserido dentro da vigência do contrato!',
							10000, 'rounded');
					
					$('#select-situacao').removeAttr('disabled');
					$('#select-situacao').material_select();
					
					$("#btn-add-producao").attr("disabled", false);
					
					return false;
				} else {

					Materialize.toast(
							'Ano inserido fora da vigência do contrato!',
							10000, 'rounded');
					Materialize
							.toast(
									'Travando a situação em "Negociação", especifique o motivo...',
									10000, 'rounded');
					$("#btn-add-producao").attr("disabled", false);

					$('#select-situacao option:eq(5)').prop('selected', true);
					$('#select-situacao').material_select();
					
					$('#select-situacao').prop("disabled", true)
					$('#select-situacao').material_select();	
					
					return true;
				}

				$('.tabs').tabs();
				$("#informacoes").show();

			}
		}
	}

}