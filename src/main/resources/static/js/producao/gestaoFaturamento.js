$(function(){
	$(".select").material_select();
	filtroTabelaCliente();
})


function filtroTabelaCliente() {
	var $rows = $('#tabela-empresas tbody tr');
	$('#txt-pesq-empresa').keyup(function() {
	    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

	    $rows.show().filter(function() {
	        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
	        return !~text.indexOf(val);
	    }).hide();
	});
}


