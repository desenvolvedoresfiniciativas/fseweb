$(function () {

    $('select').material_select();
    carregaSintetico();
    somaTotais();
})

function carregaSintetico() {
	var div = document.querySelector("div[class*='preloader-wrapper big active loader']");
	div.style.display = "block";
	
    var prods = []

    $.when(solicitadorPrevRels()).done(function (prevs) {
        for (var i = 0; i < prevs.length; i++) {
            var item = prevs[i];
            var valor;
            if ($.isNumeric(item.valor)) {
                valor = item.valor
            } else {
                valor = 0
            }
            $('#' + item.producao.id + 'prev').text(valor)
        }
    });

    $.when(solicitadorRels()).done(function (relatorios) {
        for (var i = 0; i < relatorios.length; i++) {
            var relatorio = relatorios[i];

            var countRelCad = parseInt($('#' + relatorio.producao.id + 'cadastrados').text())
            $('#' + relatorio.producao.id + 'cadastrados').text(countRelCad + 1)

            if (relatorio.documentacaoCompleta == true) {
                var countDocumenta = parseInt($('#' + relatorio.producao.id + 'document').text())
                $('#' + relatorio.producao.id + 'document').text(countDocumenta + 1)

                if (relatorio.estado == "PENDENTE") {
                    var countDocRed = parseInt($('#' + relatorio.producao.id + 'docCompleta').text())
                    $('#' + relatorio.producao.id + 'docCompleta').text(countDocRed + 1)
                }

            }

            switch (relatorio.estado) {
                case "PENDENTE":
                    var countPendente = parseInt($('#' + relatorio.producao.id + 'aRedigir').text())
                    $('#' + relatorio.producao.id + 'aRedigir').text(countPendente + 1)
                    break;
                case "EM_REDAÇÃO":
                    var countRedacao = parseInt($('#' + relatorio.producao.id + 'redacao').text())
                    $('#' + relatorio.producao.id + 'redacao').text(countRedacao + 1)
                    break;
                case "EM_CORRECAO":
                    var countCorrecao = parseInt($('#' + relatorio.producao.id + 'correcao').text())
                    $('#' + relatorio.producao.id + 'correcao').text(countCorrecao + 1)
                    break;
                case "COMPLETO":
                    var countCompleto = parseInt($('#' + relatorio.producao.id + 'completo').text())
                    $('#' + relatorio.producao.id + 'completo').text(countCompleto + 1)
                    break;
                case "EM_REVISAO":
                    var countRevisao = parseInt($('#' + relatorio.producao.id + 'revisao').text())
                    $('#' + relatorio.producao.id + 'revisao').text(countRevisao + 1)
                    break;
                case "CLIENTE":
                    var countCliente = parseInt($('#' + relatorio.producao.id + 'cliente').text())
                    $('#' + relatorio.producao.id + 'cliente').text(countCliente + 1)
                    break;
                case "APROVADO":
                    var countAprovado = parseInt($('#' + relatorio.producao.id + 'aprovado').text())
                    $('#' + relatorio.producao.id + 'aprovado').text(countAprovado + 1)
                    break;
            }

        }
	div.style.display = "none";
});	
}

function somaTotais() {

    // Previsão Relatórios
    var sumPrev = 0;
    var previsao = [];
    $(".previsaoRelColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
            sumPrev += parseInt(value);
            $("#prevRel").text(sumPrev)
        }
    }

    // Rel. Cadastrados
    var sumRelCad = 0;
    var previsao = [];
    $(".cadastrodsColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumRelCad += parseInt(value);
            $("#relCad").text(sumRelCad)
        }
    }

    // Documentação Completa
    var sumDoc = 0;
    var doc = [];
    $(".documentColumn").each(function () {
        if ($(this).is(":visible")) {
        	doc.push($(this))
        }
    });
    for (var i = 0; i < doc.length; i++) {
        var value = doc[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumDoc += parseInt(value);
            $("#DocCompleta").text(sumDoc)
        }
    }

    // Á Redigir
    var sumRed = 0;
    var previsao = [];
    $(".aRedigirColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumRed += parseInt(value);
            $("#aRedigir").text(sumRed)
        }
    }

    // Á Redigir Doc. Completa
    var sumARed = 0;
    var previsao = [];
    $(".docCompletaColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumARed += parseInt(value);
            $("#aRedigirDoc").text(sumARed)
        }
    }

    // Redação
    var sumReda = 0;
    var previsao = [];
    $(".redacaoColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumReda += parseInt(value);
            $("#redacao").text(sumReda)
        }
    }

    // Correção
    var sumCorre = 0;
    var previsao = [];
    $(".correcaoColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumCorre += parseInt(value);
            $("#correcao").text(sumCorre)
        }
    }

    // Revisão
    var sumRev = 0;
    var previsao = [];
    $(".revisaoColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumRev += parseInt(value);
            $("#revisao").text(sumRev)
        }
    }

    // Completo
    var sumCompleto = 0;
    var previsao = [];
    $(".completoColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumCompleto += parseInt(value);
            $("#completo").text(sumCompleto)
        }
    }

    // Cliente
    var sumCliente = 0;
    var previsao = [];
    $(".clienteColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumCliente += parseInt(value);
            $("#cliente").text(sumCliente)
        }
    }

    // Aprovado
    var sumAprova = 0;
    var previsao = [];
    $(".aprovadoColumn").each(function () {
        if ($(this).is(":visible")) {
            previsao.push($(this))
        }
    });
    for (var i = 0; i < previsao.length; i++) {
        var value = previsao[i].text();
        if (!isNaN(value) && value.length != 0) {
        	sumAprova += parseInt(value);
            $("#aprovado").text(sumAprova)
        }
    }
}

function unique(list) {
    var result = [];
    $.each(list, function (i, e) {
        var matchingItems = $.grep(result, function (item) {
            return item.id === e.id;
        });
        if (matchingItems.length === 0) {
            result.push(e);
        }
    });
    return result;
}

function solicitadorRels() {
	
	console.log("solicitadorRels Javascript");
	
	var ano = $('#ano').val();
	var idConsultor = $('#idConsultor').val();
	var idEquipe = $('#idEquipe').val();
	var setorAtividade = $('#setorAtividade').val();
	
	setorAtividade = encodeURIComponent(setorAtividade);
	
	console.log()
	
	if(ano==="" || ano==undefined){
		ano="NULO"
	}
	
	if(idConsultor==="" || idConsultor==undefined){
		idConsultor="NULO"
	}
	
	if(idEquipe==="" || idEquipe==undefined){
		idEquipe="NULO"
	}
	
	if(setorAtividade==="" || setorAtividade==undefined || setorAtividade=="undefined"){
		setorAtividade="NULO"
	}

    var url = "/relatorios/"+ano+"/"+setorAtividade+"/"+idConsultor+"/"+idEquipe+"/rels.json";

	console.log(url)

    return $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data) {}
    });

}

function solicitadorPrevRels() {
	
	console.log("solicitadorPrevRels Javascript");
	
	var ano = $('#ano').val();
	var idConsultor = $('#idConsultor').val();
	var idEquipe = $('#idEquipe').val();
	var setorAtividade = $('#setorAtividade').val();
	
	setorAtividade = encodeURIComponent(setorAtividade);
	
	console.log('Setor '+setorAtividade)
	
	if(ano==="" || ano==undefined){
		ano="NULO"
	}
	
	if(idConsultor==="" || idConsultor==undefined){
		idConsultor="NULO"
	}
	
	if(idEquipe==="" || idEquipe==undefined){
		idEquipe="NULO"
	}
	
	if(setorAtividade==="" || setorAtividade==undefined || setorAtividade=="undefined"){
		setorAtividade="NULO"
	}
	
	
	
    var url = "/relatorios/"+ano+"/"+setorAtividade+"/"+idConsultor+"/"+idEquipe+"/prev.json"
  
    console.log(url)
  return $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data) {}
    });

}


function fnExcelReport() {
    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Relatório Sintético</x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + $('#tabela-clientes').html();
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, 'sintetico.xls');
        }
    } else {
        $('#excelBtn').attr('href', data_type + ', ' + escape(tab_text));
        $('#excelBtn').attr('download', 'sintetico.xls');
    }
}
