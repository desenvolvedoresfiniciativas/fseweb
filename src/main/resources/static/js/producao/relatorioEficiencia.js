$(function () {

    $('select').material_select();

})


function fnExcelReport() {
    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Relatório eFIciência</x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + $('#tabela-clientes').html();
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=UTF-8;"
            });
            navigator.msSaveBlob(blob, 'eficiencia.xls');
        }
    } else {
        $('#excelBtn').attr('href', data_type + ', ' + escape(tab_text));
        $('#excelBtn').attr('download', 'eficiencia.xls');
    }
}



$(function formataData(){
	$(".data").each(function(){
		var data = $(this).text();
		console.log("data",data)
		if(data == ""){
			dataFormatada = "";
		}else{
		var dataFormatada = data.split("-")
		var dia = dataFormatada[2];
		var mes = dataFormatada[1];
		var ano = dataFormatada[0];
		dataFormatada = dia+"/"+mes+'/'+ano;
		}
		
		$(this).text(dataFormatada);
	})
})

$(function maskValor(){
	
	$(".moneyClass").each(function(){
		var texto = $(this).text();
		if(texto == ""){
			$(this).text(texto);
		}else{
			texto = parseFloat(texto).toFixed(2).toLocaleString('pt-BR')
			texto = mascaraValor(texto)
			$(this).text("R$ " + texto);
		}
	})
})

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

