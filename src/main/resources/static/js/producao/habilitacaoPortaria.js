
$(function() {
	formatarCnpj();
	$('.select').material_select();
})

function formatarCnpj() {
	$("#cnpj").mask('00.000.000/0000-00', {
		reverse : true
	});
	$("#cnpjFaturar").mask('00.000.000/0000-00', {
		reverse : true
	});
}