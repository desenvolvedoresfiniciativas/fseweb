$("#salvar").click(salvaItens);

$(function() {
	populaTarefas();
	inicializadores();
	$('#modalEficiencia').modal();
	
})

function populaTarefas() {

	$.when(solicitadoresItens()).done(function(itemValores) {

		var idConclusao = $("#idConclusao").val();
		var idProd = $("#idProd").val();
		console.log(idProd);

		// Handler de páginas fixas.
		$('.corpo').each(function(i, obj) {

			var tarefaId = $(this).attr('id');
			var tarefaAtual = $(this).attr('name');

			// Tarefas fixas
			if (tarefaId == 149) {
				var button = $("<a href='/producoes/balancete/" + idConclusao + "' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar balancetes</a>")
				$('#' + tarefaId).append(button)
			}

			if (tarefaId == 272) {
				var button = $("<a href='/producoes/balanceteROTA/" + idConclusao + "' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar balancetes</a>")
				$('#' + tarefaId).append(button)
			}

			if (tarefaId == 152) {
				var button = $("<a href='/producoes/relatorios/" + idConclusao + "' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar relatórios</a>")
				$('#' + tarefaId).append(button)
			}

			if (tarefaId == 182) {
				var button = $("<a href='/producoes/projetos/" + idConclusao + "' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Projetos</a>")
				$('#' + tarefaId).append(button)
			}

			if (tarefaId == 193) {
				var button = $("<a href='/producoes/habilitacaoPortaria/" + idConclusao + "' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Habilitação de Portarias</a>")
				$('#' + tarefaId).append(button)
			}

			if (tarefaId == 174) {
				var button = $("<a href='/producoes/auditoriaEconomica/" + idConclusao + "' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Auditoria Econômica</a>")
				$('#' + tarefaId).append(button)
			}

			if (tarefaId == 171) {
				var button = $("<a href='/producoes/auditoriaProjetos/" + idConclusao + "' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Auditoria de Projetos</a>")
				$('#' + tarefaId).append(button)
			}

			// Append de Item valores no Corpo HTML
			for (var i = 0; i < itemValores.length; i++) {
				var item = itemValores[i];
				console.log("Camilo")
				console.log(item)
				if (item.tarefa.id == tarefaId) {
					console.log("item, item tarefa id: ",item.tarefa.id," tarefaid: ", tarefaId ," -> " , item);
					switch (item.item.tipo) {

						// -- !! CAMPO PARA DATA !! -- //
						case 'DATA':
							var div = $("<div>")
							var label = $("<label>").text(item.item.nome)
							var input = null;
							if(tarefaId == '15' && item.item.nome == 'FECHA DE INICIO ' || tarefaId == '15' && item.item.nome == 'FECHA DE TÉRMINO' || tarefaId == '15' && item.item.nome == 'FECHA PRESENTACIÓN CERTIFICACIÓN' ) input = $("<br/><input readonly style=' cursor: not-allowed; background: #DCDCDC ; width: 100px; color: black;' type='text' placeholder='" + item.item.tipo + "' class='' id=" + item.id + " value='" + item.valor + "'>");
							else input = $("<br/><input style='width: 100px; color: black;' type='text' placeholder='" + item.item.tipo + "' class='datepicker' id=" + item.id + " value='" + item.valor + "'>");
							label.appendTo(div);
							input.appendTo(div)
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
							$('#' + tarefaId).append(div)
							break;

						// -- !! CAMPO PARA CHECK !! -- //
						case 'CHECK':
							var div = $("<div style='padding-bottom: 10px; 'padding-top: 10px;'>")
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							var input = $("<input type='checkbox' name='finalizador." + item.item.defineRealizacao + "' class='filled-in' id=" + item.id + ">");
							console.log("input check",item.id);
							if (item.valor == "TRUE") {
								input.attr("checked", true);
							} else { input.attr("checked", false); }

							input.appendTo(div);
							label.appendTo(div);
							div.append("<br/>")
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")

							$('#' + tarefaId).append(div)
							break;


						// -- !! CAMPO PARA TEXTO !! -- //
						case 'TEXTO':
							var div = $("<div>")
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							var holder = $("#cnpjHolder").val();
							var cnpjTester = "CNPJ";
							if (item.item.nome == cnpjTester) {
								var input = $("<input id='" + item.id + "' style='width: 600px' type='text' class='texto' value='" + holder + "' />");
							} else {
								var input = null;
								if(tarefaId == '15' && item.item.nome == 'CÓDIGO SIA?' || tarefaId == '15' && item.item.nome == 'CÓDIGO DEL PROYECTO CERTIFICACIÓN') input = $("<input readonly id='" + item.id + "' placeholder='" + item.item.tipo + "' style='cursor: not-allowed; background: #DCDCDC ;width: 600px' type='text' class='texto' value='" + item.valor + "' />");
								else input = $("<input  id='" + item.id + "' placeholder='" + item.item.tipo + "' style='width: 600px' type='text' class='texto' value='" + item.valor + "' />");
							}

							label.append("<br/>")
							label.appendTo(div);
							input.appendTo(div);
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")

							$('#' + tarefaId).append(div)
							break;

						// -- !! CAMPO PARA TEXTO LONGO !! -- //
						case 'TEXTOLONGO':
							var div = $("<div>")
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							var input = $("<input id='" + item.id + "' placeholder='" + item.item.tipo + "' type='text' class='texto' value='" + item.valor + "' />");
							label.append("<br/>")
							label.appendTo(div);
							input.appendTo(div);
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")

							$('#' + tarefaId).append(div)
							break;

						// -- !! CAMPO PARA DINHEIRO !! -- //
						case 'DINHEIRO':
							var div = $("<div>")
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							var input = null;
							if(tarefaId == '15' && item.item.nome == 'VALOR CERTIFICADO') input = $("<input readonly id='" + item.id + "' placeholder='" + item.item.tipo + "' style=' cursor: not-allowed; background: #DCDCDC ;width: 200px' type='text' class='money' value='" + item.valor + "' />");
							else input = $("<input id='" + item.id + "' placeholder='" + item.item.tipo + "' style='width: 200px' type='text' class='money' value='" + item.valor + "' />");
							label.append("<br/>")
							label.appendTo(div);
							input.appendTo(div);
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")

							$('#' + tarefaId).append(div)
							break;

						// -- !! CAMPO PARA PORCENTAGEM !! -- //
						case 'PORCENTAGEM':

							var div = $("<div>");
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							var input = $("<input id='" + item.id + "' placeholder='" + item.item.tipo + "' onchange='handleChange(this)' class='percent' style='width: 200px' type='text' value='" + item.valor + "' />");
							label.append("<br/>")
							label.appendTo(div);
							input.appendTo(div);
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")

							$('#' + tarefaId).append(div)
							break;

						// -- !! CAMPO PARA CONSULTOR !! -- //
						case 'LISTA_CONSULTOR':

							var selecionado = 'Selecione';
							var valorSelecionado = '';
							if (item.valor != "") {
								selecionado = item.valor;
								valorSelecionado = item.valor;
							}

							var div = $("<div>")

							var select = $("<select id='" + item.id + "' class='consultores select'>");
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							select.append($('<option>', {
								value: valorSelecionado,
								text: selecionado,
								disabled: 'disabled',
								selected: 'selected',
							}));
							select.append($('<option>', {
								value: "",
								text: "",
							}));

							label.appendTo(div);
							select.appendTo(div);
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")

							$('#' + tarefaId).append(div)

							break;

						// -- !! CAMPO PARA LISTA DE VALORES !! -- //
						case 'LISTA':

							var selecionado = 'Selecione';
							var valorSelecionado = '';
							if (item.valor != "") {
								selecionado = item.valor;
								valorSelecionado = item.valor;
							}

							var div = $("<div>")

							var select = $("<select id='" + item.id + "' name='" + item.item.id + "' class='lista select'>");
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							select.append($('<option>', {
								value: valorSelecionado,
								text: selecionado,
								disabled: 'disabled',
								selected: 'selected',
							}));
							select.append($('<option>', {
								value: "",
								text: "",
							}));

							label.appendTo(div);
							select.appendTo(div);
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")

							$('#' + tarefaId).append(div)

							break;

						// -- !! CAMPO PARA LISTA DE SELEÇÃO MULTIPLA !! -- //
						case 'LISTA_MULTI':

							if (item.valor == "") {
								var selecionado = 'Selecione';
								var valorSelecionado = '';
							}

							var div = $("<div>")

							var select = $("<select multiple id='" + item.id + "' name='" + item.item.id + "' class='listaM select'>");
							var label = $("<label for=" + item.id + ">").text(item.item.nome)
							select.append($('<option>', {
								value: valorSelecionado,
								text: selecionado,
							}));

							if (item.valor != "") {

								var arrayValores = item.valor.split(';');
								$.each(arrayValores, function(index, value) {
									select.append($('<option>', {
										value: value,
										text: value,
										selected: 'selected',
									}));
								})
							}

							label.appendTo(div);
							select.appendTo(div);
							div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")


							$('#' + tarefaId).append(div)
							break;
							
					}
				}
			}
		});
	});

	populaSelectsConsultores();
	populaSelectsListaValores();
	
}

function populaSelectsConsultores() {

	$.when(solicitadoresConsultores()).done(function(consultores) {

		$(".consultores").each(function(i, obj) {
			var select = $(this);
			for (var i = 0; i < consultores.length; i++) {
				select.append($('<option>', {
					value: consultores[i].id,
					text: consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
		})
	});

}

function populaSelectsListaValores() {

	$.when(solicitadoresListaValores()).done(function(listaValores) {

		$(".lista").each(function(i, obj) {
			var select = $(this);

			for (var i = 0; i < listaValores.length; i++) {

				if (select.attr('name') == listaValores[i].item.id)
					select.append($('<option>', {
						value: listaValores[i].valor,
						text: listaValores[i].valor
					}));
			}
		})

		$(".listaM").each(function(i, obj) {
			var select = $(this);

			for (var i = 0; i < listaValores.length; i++) {
				var itemValor = listaValores[i]
				if (select.attr('name') == itemValor.item.id) {

					var opcoesSelecionadas = $(this).val();

					if (jQuery.inArray(itemValor.valor, opcoesSelecionadas) == -1) {
						select.append($('<option>', {
							value: itemValor.valor,
							text: itemValor.valor
						}));

					}
				}
			}
		})
	});
}


function solicitadoresItens() {

	var prodId = $("#idProd").val();

	var url = "/producoes/itemValores/" + prodId + ".json"

	return $.ajax({
		type: 'GET',
		url: url,
		async: false,
		success: function(data) { }
	});

}

function solicitadoresConsultores() {

	var url = "/producoes/consultores.json"

	return $.ajax({
		type: 'GET',
		url: url,
		async: false,
		success: function(data) { }
	});

}

function solicitadoresListaValores() {

	var url = "/producoes/listas.json"

	return $.ajax({
		type: 'GET',
		url: url,
		async: false,
		success: function(data) { }
	});

}

function inicializadores() {
	$('.select').material_select();
	$('.select').css("width", "300px");
	$('.select').siblings('input').css("width", "300px");
	$('.money').mask('000.000.000.000.000,00', { reverse: true });
}

function salvaItens() {
	
	
	var etapaFinalizada = false
	var checkeAnaliseRH = false;
	var checkeAnaliseNF =  false;
	var checkeConcluido = false;
	var checkeFinalizado= false;
	var selectedEstadoCorrecao = false;
	var selectedEstadoRevisao = false;
	var selectedEstadoAprovado = false;
	var selectedEstado = false;
	


	var valores = [];

	// PEGA VALOR DE TODOS OS DATEPICKERS
	$('.datepicker').each(function(i, obj) {
		var datepicker = {
			id: $(this).attr('id'),
			valor: $(this).val()
		};
		valores.push(datepicker);
	});
	
	
	// PEGA VALOR DE TODOS OS CHECKBOXS
	$('.filled-in').each(function(i, obj) {
		
		var nomeCampo = $(this).attr('name')
		console.log("nomeCampo",nomeCampo)
		
		var defineRealizacao = nomeCampo.substr(nomeCampo.lastIndexOf(".") + 1);
		console.log("defineRealizacao",defineRealizacao);
		
		var etapa =  $("#idEtapa").val();
		
		
		var idProd = $('#idProd').val()
		
		if($(this).is(":checked")){
			idItemValorAtual =  $(this).attr('id'),
			//console.log("idItemValorAtual",idItemValorAtual)
			 $.when(solicitadoresItens()).done(function(itemValores) {
				for (var i = 0; i < itemValores.length; i++) {
					var item = itemValores[i];
					//console.log("item.item",item)
					if (item.id == idItemValorAtual) {
						
						if(item.item.nome == "RECEBIMENTO E ANÁLISE RH"){
							console.log("entrou no RECEBIMENTO E ANÁLISE RH")
							checkeAnaliseRH = true;
							idAnaliseRH =  item.id;
							$.when(verificaInclusaoHoras(idProd,idItemValorAtual,etapa)).done(function(inclusaoHoras) {
								horasAnaliseRH = inclusaoHoras
							})
						}
						if(item.item.nome == "RECEBIMENTO E ANÁLISE NFS"){
							console.log("entrou no RECEBIMENTO E  ANÁLISE NFS")
							checkeAnaliseNF = true;
							idAnaliseNF =  item.id;
							$.when(verificaInclusaoHoras(idProd,idItemValorAtual,etapa)).done(function(inclusaoHoras) {
								horasAnaliseNF = inclusaoHoras
							})
						}
						if(item.item.nome == "CONCLUIDO"){
							console.log("entrou no CONCLUIDO")
							checkeConcluido = true;
							idConcluido =  item.id;
							$.when(verificaInclusaoHoras(idProd,idItemValorAtual,etapa)).done(function(inclusaoHoras) {
								horasConcluido = inclusaoHoras
								console.log("horasConcluido",horasConcluido)
								console.log("item.item.nomeo",item.item.nome)
							})
						}
						
						if(item.item.nome == "FINALIZADO?"){
							console.log("entrou no FINALIZADO")
							idFinalizado =  item.id;
							checkeFinalizado = true;
							idFinalizado =  item.id;
							$.when(verificaInclusaoHoras(idProd,idItemValorAtual,etapa)).done(function(inclusaoHoras) {
								horasFinalizado = inclusaoHoras
								console.log("horasFinalizado finalizado",horasFinalizado);
							})
						}
					}
				}
			})
			
			
		}
		if (defineRealizacao == "true" && $(this).is(":checked")) {
			etapaFinalizada = true
		}
		
		var check = {
			id: $(this).attr('id'),
			valor: $(this).is(":checked")
		};
		valores.push(check);
		console.log("id das checkbox", $(this).attr('id'));
		
	});

	// PEGA VALOR DE TODOS OS CAMPOS DE TEXTO
	$('.texto').each(function(i, obj) {
		var text = {
			id: $(this).attr('id'),
			valor: $(this).val()
		};
		valores.push(text);
	});

	// PEGA VALOR DE TODOS AS PORCENTAGENS
	$('.percent').each(function(i, obj) {
		var percent = {
			id: $(this).attr('id'),
			valor: $(this).val()
		};
		valores.push(percent);
	});

	// PEGA VALOR DE TODOS OS CAMPOS DE MONEY 
	$('.money').each(function(i, obj) {

		var money = {
			id: $(this).attr('id'),
			valor: $(this).val()
		};
		valores.push(money);
	});

	// PEGA VALOR DE TODOS OS SELECTS DE CONSULTORES 
	$('.consultores').each(function(i, obj) {
		var checker = $(this).attr('id');
		if (checker !== undefined) {
			var consultor = {
				id: $(this).attr('id'),
				valor: $(this).find(":selected").text()
			};
			valores.push(consultor);
		}
	});

	// PEGA VALOR DE TODOS OS SELECTS DE LISTA 
	$('.lista').each(function(i, obj) {
		
		var etapa =  $("#idEtapa").val();
		var idProd = $('#idProd').val()
		console.log($(this).find(":selected"));
		var itemValorSelected = $(this).find(":selected").text();
		if($(this).find(":selected")){
			var idItemValorAtualSelect =  $(this).attr('id');
			console.log("idItemValorAtualSelect",idItemValorAtualSelect);
			 $.when(solicitadoresItens()).done(function(itemValores) {
				for (var i = 0; i < itemValores.length; i++) {
					var item = itemValores[i];
					
					if (item.id == idItemValorAtualSelect) {
						console.log("entrou no if do selected");
						console.log("item.valor",item.valor);
						console.log("-",$(this).text())
						
							if(item.valor != itemValorSelected && itemValorSelected == "EM CORREÇÃO"){
								console.log("entrou em correção");
								selectedEstado  = true;
								idEstado =  item.id;
								horasEstado = false;
							}else{
								if(item.valor != itemValorSelected && itemValorSelected == "EM REVISÃO"){
									console.log("entrou em revisão");
									selectedEstado  = true;
									idEstado =  item.id;
									horasEstado = false;
								}else{
									if(item.valor != itemValorSelected && itemValorSelected == "APROVADO"){
										console.log("entrou em aprovado");
										selectedEstado  = true;
										idEstado =  item.id;
										horasEstado = false;
									}else{
										console.log("entrou no else  gera");
										selectedEstado  = true;
										idEstado =  item.id;
										horasEstado = true;
									}
								}
							}
							
					
					}
				}
			})
		
		}
		
		
		var checker = $(this).attr('id');
		if (checker !== undefined) {
			var lista = {
				id: $(this).attr('id'),
				valor: $(this).find(":selected").text()
			};
			valores.push(lista);
		}
	});

	// PEGA VALOR DE TODOS OS SELECTS MULTIPLOS
	$('.listaM').each(function(i, obj) {

		var checker = $(this).attr('id');
		if (checker !== undefined) {

			var array = $(this).val();
			var string = "";

			for (var i = 0; i < array.length; i++) {

				string += array[i] + ";"
			}

			string = string.slice(0, -1)

			var listaMulti = {
				id: $(this).attr('id'),
				valor: string
			};

			valores.push(listaMulti);
		}
	});

	var idEtapa = $("#idEtapa").val();
	var incluso = $("#inclusaoEficiencia").val()
	

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/" + idEtapa + "/salvaItens",
		data: JSON.stringify(valores),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Itens salvos com sucesso!',
						5000, 'rounded');
					console.log()
				if (checkeFinalizado  == true  && horasFinalizado  == false){
					Materialize
						.toast(
							'Etapa de Trabalho finalizada.',
							5000, 'rounded');
							selectedTarefaAtual(idFinalizado);
					   		$('#modalEficiencia').modal({ dismissible: false});
                		 	$('#modalEficiencia').modal('open');
				} else {
					if (checkeAnaliseRH == true && horasAnaliseRH == false){
					Materialize
						.toast(
							'Etapa de Trabalho finalizada.',
							5000, 'rounded');
							console.log("entrou no salva if")
							selectedTarefaAtual(idAnaliseRH);
					   		$('#modalEficiencia').modal({ dismissible: false});
                		 	$('#modalEficiencia').modal('open');
				}else {
					if (checkeAnaliseNF == true && horasAnaliseNF == false){
						
					Materialize
						.toast(
							'Etapa de Trabalho finalizada.',
							5000, 'rounded');
							selectedTarefaAtual(idAnaliseNF)
					   		$('#modalEficiencia').modal({ dismissible: false});
                		 	$('#modalEficiencia').modal('open');
					}else{
						if (checkeConcluido == true && horasConcluido == false){
							Materialize
							.toast(
								'Etapa de Trabalho finalizada.',
								5000, 'rounded');
								selectedTarefaAtual(idConcluido)
					   			$('#modalEficiencia').modal({ dismissible: false});
                		 		$('#modalEficiencia').modal('open');
                		 }else{
							if (selectedEstado  == true && horasEstado== false){
							Materialize
							.toast(
								'Etapa de Trabalho finalizada.',
								5000, 'rounded');
								selectedTarefaAtual(idEstado)
					   			$('#modalEficiencia').modal({ dismissible: false});
                		 		$('#modalEficiencia').modal('open');
                			 }
                		 }
					}
				}	
				}
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});

}


function inicializarDatePickers() {
	$('.datepicker').pickadate({
		monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
		weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		today: 'Hoje',
		clear: false,
		close: 'Pronto',
		labelMonthNext: 'Próximo mês',
		labelMonthPrev: 'Mês anterior',
		labelMonthSelect: 'Selecione um mês',
		labelYearSelect: 'Selecione um ano',
		selectMonths: true,
		selectYears: 15,
		closeOnSelect: false,
		format: 'dd/mm/yyyy'
	});
}

var i = 0

$('.add').click(

	function() {

		var divPai = $('#' + $(this).parent().attr("id"))

		var divRow = $("<div class='row'>")
		var divAtividade = $("<div class='input-field col s3'>")
		var divConsultor = $("<div class='input-field col s4'>")
		var divHoras = $("<div class='input-field col s2'>")
		var divTarefa = $("<div class='input-field col s2'>");
		var selectAtividade = $("<select id='atividade" + i + "' class='select'>");
		var labelAtividade = $("<label for='atividade'>").text(
			"Tipo de Atividade")
		selectAtividade.append($('<option>', {
			value: "",
			text: "Selecione",
			disabled: 'disabled',
			selected: 'selected',
		}));
		selectAtividade.append($('<option>', {
			value: "Feito Por",
			text: "Feito Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Validado Por",
			text: "Validado Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Revisado Por",
			text: "Revisado Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Gestão Por",
			text: "Gestão Por",
		}));

		var selectConsultor = $("<select id='consultor" + i + "' class='select consultores'>");
		var labelConsultor = $("<label for='consultor'>").text(
			"Consultor")
		selectConsultor.append($('<option>', {
			value: "",
			text: "Selecione",
			disabled: 'disabled',
			selected: 'selected',
		}));
			
		// SELECT DE TAREFAS //
		var selectTarefas = $("<select id='tarefas"+ idEtapa + i
					+ "' class='select tarefas'>");
		var labelTarefas = $("<label for='tarefas"+ idEtapa + i + "'>").text(
					"Tarefas")
		selectTarefas.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
		var inputHoras = $("<input type='time' required class='hora' id='horas"+ i
					+ "'  value='00:00' step='3600''>");
		
		selectTarefas.appendTo(divTarefa);
		labelTarefas.appendTo(divTarefa);
		divTarefa.appendTo(divRow);
		selectAtividade.appendTo(divAtividade);
		selectConsultor.appendTo(divConsultor);
		inputHoras.appendTo(divHoras)
		labelAtividade.appendTo(divAtividade);
		labelConsultor.appendTo(divConsultor);
		divAtividade.appendTo(divRow);
		divConsultor.appendTo(divRow);
		divHoras.appendTo(divRow);
		divRow.appendTo(divPai)

		populaSelects(selectConsultor);
		var idEtapa = $("#idEtapa").val();
		populaSelectsTarefas(selectTarefas,idEtapa);
		
		selectAtividade.material_select();
		selectConsultor.material_select();
		selectTarefas.material_select();
		i++;

	})


function handleChange(input) {
	if (input.value < 0) input.value = 0;
	if (input.value > 100) input.value = 100;
}

function populaSelects(selectConsultor) {

	$.when(solicitadoresConsultores()).done(function(consultores) {

		selectConsultor.find('option').remove()

		selectConsultor.each(function(i, obj) {
			var select = $(this);
			select.append($('<option>', {
				value: "",
				text: "Selecione",
				disabled: 'disabled',
				selected: 'selected',
			}));

			for (var i = 0; i < consultores.length; i++) {
				select.append($('<option>', {
					value: consultores[i].id,
					text: consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
		})
	});

}

$('.salvar').click(function() {
	var aux = 0;

	var idEtapa = $(this).parent().attr("id")
	var atividades = [];

	var divPai = $($(this).prev())
	var idDivPai = divPai.attr("id")

	var firstIteration = true;
	var j = 0

	$("#corpo > div").each(function() {
		
		var atividade = $('#atividade' + j).find(":selected").val();
		var consultorId = $('#consultor' + j).find(":selected").val();
		var horas = $('#horas' + j).val();
		var tarefa = $('#tarefas'+idEtapa+j).find(":selected").val();
		var itemTarefa = $('#idItemValorAtual').val();
		
		//input zero horas
		if(horas =='00:00'){
			$('#horas'+idEtapa+j).animate({width: "200px" }, 800)
			aux++;
		}
		//formatando hora
		var tempoAux = horas.split(":");
		var hora = tempoAux[0];
		var minutos = tempoAux[1];
		//criando nova variavel, formato float
		var horaFormatada  = hora+"."+minutos;
		
		
		var etapa = {
			id: $('#idEtapa').val()
		}

		var producao = {
			id: $('#idProd').val()
		}

		var consultor = {
			id: consultorId
		}
		
		 tarefa = {
			id :  tarefa
		} 
		
		 itemTarefa = {
			id :  itemTarefa
		} 
		
		var atividade = {
			atividade: atividade,
			consultor: consultor,
			horas: horaFormatada,
			etapa: etapa,
			producao: producao,
			tarefa: tarefa,
			itemTarefa: itemTarefa,
		};

		atividades.push(atividade)
		j++

	})
	if(atividades.length == 0 || aux != 0){
		Materialize.toast("Preencher campo horas", 5000, 'red');
		 $('#modalEficiencia').modal('open');
	}else{

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/eficiencia/salva",
		data: JSON.stringify(atividades),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Atividades salvas com sucesso!',
						5000, 'rounded');
				$('#modalEficiencia').modal('close');
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});
	}
})

//função para impedir inserção de zero horas
$(document).on("change", ".hora", function() {
	var hora = $(this);

	tempo = hora.val();
	if (tempo <= 0) {
		$(this).val('0.1')
	}
})

//function para preencher o select tarefas
function populaSelectsTarefas(selectTarefas,idEtapa) {
	console.log("idEtapa",idEtapa);
	

	$.when(solicitadoresTarefas(idEtapa)).done(function(tarefa) {

		selectTarefas.find('option').remove()

		selectTarefas.each(function(i, obj) {
		
	
			var select = $(this);
			select.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
			var tarefaId =$("#idTarefaAtual").val();
			console.log("tarefaId na construção do select",tarefaId)
			for (var i = 0; i < tarefa.length; i++) {
				if(tarefaId == tarefa[i].id){
					select.append($('<option>', {
					value :tarefa[i].id,
					text : tarefa[i].nome,
					selected : 'selected',
				}));
				}
				
			}
		})
	});

}
//function para pegar as tarefas(todas)
function solicitadoresTarefas(idEtapa){

	var url = "/producoes/"+idEtapa+"/tarefas.json"

	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			console.log("data",data);
		}
	});
}



//$(document).on("change",".filled-in",function() {
	// var ultimoItemTarefa = $(this).attr('id');
	// var url = "/producoes/"+ultimoItemTarefa+"/pegaTarefaByItemValores.json"

	//return $.ajax({
		//type : 'GET',
		//url : url,
		//async : false,
		//success : function(data) {
			//$("#idTarefaAtual").val(data);
		//}
	//});
//})


function verificaInclusaoHoras(idProd,idItemValorAtual,etapa){
console.log("etapa",etapa)
var url = "/producoes/" +  idProd + "/"+ idItemValorAtual+"/"+ etapa+ "/verificaInclusaoHoras.json"

			return $.ajax({
			type: 'GET',
			url: url,
			async: false,
				success: function(data) { 
			
			
			}
			});

}

function selectedTarefaAtual(idItemValor){
	
	selectedItemTarefaAtual(idItemValor);
	
	 var url = "/producoes/"+idItemValor+"/pegaTarefaByItemValores.json"

	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			$("#idTarefaAtual").val(data);
		}
	});
}

function selectedItemTarefaAtual(idItemValor){
	 var url = "/producoes/"+idItemValor+"/pegaItemTarefaByItemValores.json"

	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			$("#idItemValorAtual").val(data);
		}
	});
}


