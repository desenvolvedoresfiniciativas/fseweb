$("#atualizar-vigencia").click(testeWhile);

$(function(){
	identificador();
	 $('select').material_select();
	 $('.collapsible').collapsible({
		 accordion : true
		 });
	 sumCamps();
	 selectedSubmit();
	 selectedUpdateSubmit();
	 
})

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g,"");
    valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");
    return valor                    
}

function sumCamps(){
	var sum;
	var rh;
	var materiais;
	var terceiros;
	var outras;
	$('#somar').click(function(){
		if($('#valorRH').val() != null){rh = parseFloat($('#valorRH').val().replace(/\./g,'').replace(",","."));}else{rh = 0}
		if($('#valorMateriais').val() != null){materiais = parseFloat($('#valorMateriais').val().replace(/\./g,'').replace(",","."));}else{materiais = 0}
		if($('#valorServicosTerceiros').val() != null){terceiros = parseFloat($('#valorServicosTerceiros').val().replace(/\./g,'').replace(",","."));}else{terceiros = 0}
		if($('#valorOutrasDespesas').val() != null){outras =  parseFloat($('#valorOutrasDespesas').val().replace(/\./g,'').replace(",","."));}else{outras = 0}
		sum = rh + materiais + terceiros + outras;
		console.log(sum)
		sum = mascaraValor(sum.toFixed(2));
		$('#valorTotal').val(sum);
	});
}

function identificador(){
	var index = 0;
	$('.campo').each(function(i) {
		   if(!$(this).is('label') && !$(this).is('select')){ 
			   $(this).attr('id', 'valor' + index);
			   index++
		   }
		   if($(this).is("select")){
			   $(this).attr('id', 'valor' + index);
			   index++
		   }
		   if($(this).is('label')){
			   $(this).attr('for', 'valor'+(index-1))
		   }
	});
	
	$('.identificador').each(function(i) {
		   $(this).attr('id', 'id' + i);
		});
	$('.identificadorItem').each(function(i) {
		   $(this).attr('id', 'idItem' + i);
		});
	$('.identificadorTarefa').each(function(i) {
		   $(this).attr('id', 'idTarefa' + i);
		});
	$('.dataPrevisao').each(function(i) {
		   $(this).attr('id', 'dataP' + i);
		});
	$('.dataRealizado').each(function(i) {
		   $(this).attr('id', 'dataR' + i);
		});
}

function testeWhile(){
	
	var valores = [];
	
	var index = 0;
	
	$('.campo').each(function(i){
		index++
	});
	
	console.log(index);
	
	$('.identificador').each(function(i){
		console.log("entrou no identificador")
		
		var id = parseInt($("#id"+i).val());
		var idItem = parseInt($("#idItem"+i).val());
		var idTarefa = parseInt($("#idTarefa"+i).val());
		
		var idIt = {
			id: idItem,
		};
		
		var idTar = {
			id: idTarefa,
		};
		
		var valor;
		
		if($("#valor"+i).attr("type") == "checkbox"){
			console.log("Estou identificando o checkbox") 
			valor = $("#valor"+i).is(":checked");
		} else if($("#valor"+i).is("select")){
			valor = $("#valor"+i).find(":selected").text();
		} else {
			console.log("Não estou identificando nada") 
			 valor = $("#valor"+i).val();
		}
		
		item = {
		id: id,
		item: idIt,
		tarefa: idTar,
		valor: valor,
		dataPrevisao: $("#dataP"+i).val(),
		dataRealizado: $("#dataR"+i).val(),
		};
		
    	valores.push(item);

	});

	console.log(valores)
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/addItem",
		data: JSON.stringify(valores),
		dataType:"JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Itens salvos com sucesso!',
                        5000, 'rounded');
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }	
	});
	
}

$('#btn-cad-cliente').click(
	function() {
		//select do form add
	var optionSelect =$('#select-estado').find(":selected").text();
	$("#selectedSubmit").val(optionSelect);
	var paramentro = $("#selectedSubmit").val();
	sessionStorage.setItem("paramentro", paramentro);
	
	//select do update
	var optionSelectUpdate =$('#select-relatorioCalculo').find(":selected").text();
	var status = $("#statusEstado").val()
	if(optionSelectUpdate != status){
		$("#selectedUpdateSubmit").val(optionSelectUpdate);
		var paramentroUpdate = $("#selectedUpdateSubmit").val();
		sessionStorage.setItem("paramentroUpdate", paramentroUpdate);
	}else{
		console.log("mesmo status, não abre modal");
	}
})
	

function selectedSubmit(){
	 $("#selectedSubmit").val(sessionStorage.getItem("paramentro"));
	 var selected =   $("#selectedSubmit").val();
	 console.log("selected",selected);
	 if(selected == "APROVADO"){
		console.log("aprovado")
		var paramentro = ""
		sessionStorage.setItem("paramentro", paramentro);
		 $("#selectedSubmit").val(sessionStorage.getItem("paramentro"));
	 var selected =   $("#selectedSubmit").val();
	 console.log("selected",selected);
	 $('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
		
	}else{
		 if(selected == "EM CORREÇÃO"){
			console.log("EM CORREÇÃO")
			console.log("aprovado")
		var paramentro = ""
		sessionStorage.setItem("paramentro", paramentro);
		 $("#selectedSubmit").val(sessionStorage.getItem("paramentro"));
	 var selected =   $("#selectedSubmit").val();
	 console.log("selected",selected);
	 $('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
		 }else{
			if(selected == "EM REVISÃO"){
			console.log("EM REVISÃO");
			console.log("aprovado")
		var paramentro = ""
		sessionStorage.setItem("paramentro", paramentro);
		 $("#selectedSubmit").val(sessionStorage.getItem("paramentro"));
	 var selected =   $("#selectedSubmit").val();
	 console.log("selected",selected);
	 $('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
			}else{
				console.log("não entra nas opções");
			}
		}
	}
}

function selectedUpdateSubmit(){
	 $("#selectedUpdateSubmit").val(sessionStorage.getItem("paramentroUpdate"));
	 var selectedUpdate =   $("#selectedUpdateSubmit").val();
	 console.log("selectedUpdate",selectedUpdate);
	 if(selectedUpdate == "APROVADO"){
		
		var paramentroUpdate = ""
		sessionStorage.setItem("paramentroUpdate", paramentroUpdate);
		 $("#selectedUpdateSubmit").val(sessionStorage.getItem("paramentroUpdate"));
	 var selectedUpdate =   $("#selectedUpdateSubmit").val();
	 console.log("selectedUpdate",selectedUpdate);
	 $('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
		
	}else{
		 if(selectedUpdate == "EM CORREÇÃO"){
			console.log("EM CORREÇÃO")
			
		var paramentroUpdate = ""
		sessionStorage.setItem("paramentroUpdate", paramentroUpdate);
		 $("#selectedUpdateSubmit").val(sessionStorage.getItem("paramentroUpdate"));
	 var selectedUpdate =   $("#selectedUpdateSubmit").val();
	 console.log("selectedUpdate",selectedUpdate);
	 $('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
		 }else{
			if(selectedUpdate == "EM REVISÃO"){
			console.log("EM REVISÃO");
			
		var paramentroUpdate = ""
		sessionStorage.setItem("paramentroUpdate", paramentroUpdate);
		 $("#selectedUpdateSubmit").val(sessionStorage.getItem("paramentroUpdate"));
	 var selectedUpdate =   $("#selectedUpdateSubmit").val();
	 console.log("selectedUpdate",selectedUpdate);
	 $('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
			}else{
				console.log("não entra nas opções");
			}
		}
	}
}







var i = 0
$('.add').click(

	function() {

		var divPai = $('#' + $(this).parent().attr("id"))

		var divRow = $("<div class='row'>")
		var divAtividade = $("<div class='input-field col s3'>")
		var divConsultor = $("<div class='input-field col s4'>")
		var divHoras = $("<div class='input-field col s2'>")
		var divTarefa = $("<div class='input-field col s2'>");
		var selectAtividade = $("<select id='atividade" + i + "' class='select'>");
		var labelAtividade = $("<label for='atividade'>").text(
			"Tipo de Atividade")
		selectAtividade.append($('<option>', {
			value: "",
			text: "Selecione",
			disabled: 'disabled',
			selected: 'selected',
		}));
		selectAtividade.append($('<option>', {
			value: "Feito Por",
			text: "Feito Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Validado Por",
			text: "Validado Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Revisado Por",
			text: "Revisado Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Gestão Por",
			text: "Gestão Por",
		}));

		var selectConsultor = $("<select id='consultor" + i + "' class='select consultores'>");
		var labelConsultor = $("<label for='consultor'>").text(
			"Consultor")
		selectConsultor.append($('<option>', {
			value: "",
			text: "Selecione",
			disabled: 'disabled',
			selected: 'selected',
		}));
			
		// SELECT DE TAREFAS //
		var selectTarefas = $("<select id='tarefas"+ i
					+ "' class='select tarefas'>");
		var labelTarefas = $("<label for='tarefas" + i + "'>").text(
					"Tarefas")
		selectTarefas.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
		var inputHoras = $("<input type='time' required class='hora' id='horas"+ i
					+ "'  value='00:00' step='3600''>");
		
		selectTarefas.appendTo(divTarefa);
		labelTarefas.appendTo(divTarefa);
		divTarefa.appendTo(divRow);
		selectAtividade.appendTo(divAtividade);
		selectConsultor.appendTo(divConsultor);
		inputHoras.appendTo(divHoras)
		labelAtividade.appendTo(divAtividade);
		labelConsultor.appendTo(divConsultor);
		divAtividade.appendTo(divRow);
		divConsultor.appendTo(divRow);
		divHoras.appendTo(divRow);
		divRow.appendTo(divPai)

		populaSelects(selectConsultor);
		var idEtapa = 67;
		populaSelectsTarefas(selectTarefas,idEtapa);
		
		selectAtividade.material_select();
		selectConsultor.material_select();
		selectTarefas.material_select();
		i++;

	})
		
function populaSelects(selectConsultor) {

	$.when(solicitadoresConsultores()).done(function(consultores) {

		selectConsultor.find('option').remove()

		selectConsultor.each(function(i, obj) {
			var select = $(this);
			select.append($('<option>', {
				value: "",
				text: "Selecione",
				disabled: 'disabled',
				selected: 'selected',
			}));

			for (var i = 0; i < consultores.length; i++) {
				select.append($('<option>', {
					value: consultores[i].id,
					text: consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
		})
	});

}

$('.salvar').click(function() {
	var aux = 0;

	var idEtapa =  67
	var atividades = [];

	var divPai = $($(this).prev())
	var idDivPai = divPai.attr("id")

	var firstIteration = true;
	var j = 0

	$("#corpo > div").each(function() {
		
		var atividade = $('#atividade' + j).find(":selected").val();
		var consultorId = $('#consultor' + j).find(":selected").val();
		var horas = $('#horas' + j).val();
		var tarefa = $('#tarefas'+j).find(":selected").val();
		
		//input zero horas
		if(horas =='00:00'){
			$('#horas' + j).animate({width: "150px" }, 800)
			aux++;
		}
		//formatando tempo
		console.log("horas",horas)
		var tempoAux = horas.split(":");
		var hora = tempoAux[0];
		var minutos = tempoAux[1];
		//criando nova variavel, formato float
		var horaFormatada  = hora+"."+minutos;
		
		
		var etapa = {
			id:idEtapa
		}

		var producao = {
			id: $('#idProducao').val()
		}

		var consultor = {
			id: consultorId
		}
		
		 tarefa = {
			id :  tarefa
		} 
		
		var atividade = {
			atividade: atividade,
			consultor: consultor,
			horas: horaFormatada,
			etapa: etapa,
			producao: producao,
			tarefa: tarefa,
		};
		atividades.push(atividade)
		j++
	})
	if(atividades.length == 0 || aux != 0){
		Materialize.toast("Preencher campo horas", 5000, 'red');
	}else{

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/eficiencia/salva",
		data: JSON.stringify(atividades),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Atividades salvas com sucesso!',
						5000, 'rounded');
				$('#modalEficiencia').modal('close');
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});
	}
})

//função para impedir inserção de zero horas
$(document).on("change", ".hora", function() {
	var hora = $(this);

	tempo = hora.val();
	if (tempo <= 0) {
		$(this).val('0.1')
	}
})

//function para preencher o select tarefas
function populaSelectsTarefas(selectTarefas,idEtapa) {
	console.log("idEtapa",idEtapa);
	

	$.when(solicitadoresTarefas(idEtapa)).done(function(tarefa) {

		selectTarefas.find('option').remove()

		selectTarefas.each(function(i, obj) {
		
	
			var select = $(this);
			select.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
			var tarefaId =$("#idTarefaAtual").val();
			console.log("tarefaId na construção do select",tarefaId)
			for (var i = 0; i < tarefa.length; i++) {
				
					select.append($('<option>', {
					value :tarefa[1].id,
					text : tarefa[1].nome,
					selected : 'selected',
				}));
			
			}
		})
	});

}
//function para pegar as tarefas(todas)
function solicitadoresTarefas(idEtapa){

	var url = "/producoes/"+idEtapa+"/tarefas.json"

	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			console.log("data",data);
		}
	});
}

function solicitadoresConsultores() {

	var url = "/producoes/consultores.json"

	return $.ajax({
		type: 'GET',
		url: url,
		async: false,
		success: function(data) { }
	});

}




	
	
	



