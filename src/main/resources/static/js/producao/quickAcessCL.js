$(function() {
})

$("#buscarCodigo").on("click", function() {
	
	$("#corpo-tabela-prod").empty();

	var url = "/producoes/producoesCodigo.json"
	var codigo = $('#txt-pesq-codigo').val();

	$.ajax({
		type: 'GET',
		url: url,
		async: false,
		data: {
			codigo: codigo,
		},
		success: function(data) {
			for (var i = 0; i < data.length; i++) {
				var prod = data[i];
				var codigoProd;

				$.ajax({
					type: 'GET',
					url: '/producoes/getCodigoByProd.json',
					async: false,
					data: {
						id: prod.id
					},
					success: function(data) {
						codigoProd = data.valor
					}
					});

					$('#corpo-tabela-prod').append($("<tr><td>" + prod.cliente.razaoSocial + "</td><td>" + prod.produto.nome + "</td><td>"+codigoProd+"</td><td>" + prod.nomeProjeto + "</td><td>" + prod.situacao + "</td><td>" + prod.ano + "</td><td>" + prod.equipe.nome + "</td><td>" + prod.consultor.nome + "</td><td><a href='/producoes/update/" + prod.id + "' class=''btn light-blue darken-4'><i class='material-icons'>arrow_forward</i></a></td></tr>"))
				}
		}
		});

})