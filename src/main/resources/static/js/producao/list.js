$(function() {
	$(".select").material_select();
	$('.modal').modal();
	$('.modalD').modal();
	filtroTabelaProducao();
	filtroTabelaCliente();
	FiltroProd()
	
	$("#tabela-producao").DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			},
			"select": {
				"rows": {
					"_": "Selecionado %d linhas",
					"0": "Nenhuma linha selecionada",
					"1": "Selecionado 1 linha"
				}
			}
		},
		bFilter: false,
		iDisplayLength: 1000,
		columnDefs: [
			{
				targets: [0, 1, 2],
				className: 'mdl-data-table__cell--non-numeric'
			}
		]
	});
})



function filtroTabelaProducao() {
	var nomeFiltro = $("#combo-box :selected").val()
	console.log(nomeFiltro);
	$('#tabela-producao').find('tbody tr').each(
		function() {
			var conteudoCelula = $(this)
				.find('td:nth-child(3)').text();
			console.log(conteudoCelula);
			var corresponde = conteudoCelula
				.indexOf(nomeFiltro) >= 0;
			$(this).css('display', corresponde ? '' : 'none');
		});
	$("#combo-box").change(
		function() {
			var nomeFiltro = $("#combo-box :selected").val()
			console.log(nomeFiltro);

			if (nomeFiltro == "all") {
				$('#tabela-producao tr').show();
			} else {

				$('#tabela-producao').find('tbody tr').each(
					function() {
						var conteudoCelula = $(this)
							.find('td:nth-child(3)').text();
						console.log(conteudoCelula);
						var corresponde = conteudoCelula
							.indexOf(nomeFiltro) >= 0;
						$(this).css('display', corresponde ? '' : 'none');
					});
			}
		});
}

/*function filtroTabelaCliente() {
	var $rows = $('#tabela-empresas tbody tr');
	$('#txt-pesq-empresa').keyup(function() {
		var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
		console.log("val",val);

		$rows.show().filter(function() {
			var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
		
			return !~text.indexOf(val);
			
		}).hide();
	});
}*/

function listarAnosModal(idContrato) {
	zerarLista();
	var lista = $('#anoProducao');

	var url = '/producoes/' + idContrato + '/producoes.json';
	var link = '/producoes/form/' + idContrato;

	$.get(url, function(data) {

		$(data).each(function() {

			var producao = this.ano;
			var idContrato = this.id;

			var item = novoItemLista(producao, idContrato);
			lista.append(item);

		});

	});

	$('#modal1').modal('open');
	appendModal(link);
};

function appendModal(link) {

	var id1 = 'addProd';
	var id2 = 'closeMod';
	var footer = $('#botoesFooter');
	var classe1 = "'waves-effect waves-green btn-flat green white-text mouse-hand'"
	var classe2 = "'modal-action modal-close waves-effect waves-green btn-flat red white-text mouse-hand'"

	limpaBotoes();

	footer.append("<a id=" + id1 + " href=" + link + " class=" + classe1 + ">Novo</a>");
	footer.append(" ")
	footer.append("<a id=" + id2 + " class=" + classe2 + ">Fechar</a>");

}

function limpaBotoes() {
	$("#addProd").remove();
	$("#closeMod").remove();
}

function novoItemLista(item, idContrato) {
	var itemLista = $('<li>');
	var itemLista2 = $('<a>')
	var link = '/producoes/update/' + idContrato;

	itemLista.addClass('collection-item');

	var lista = itemLista.append("<a href=" + link + ">" + item + "</a>");

	return lista;
}

function zerarLista() {
	$('#anoProducao li').each(function() {
		$(this).remove();
	})
}

function balancete() {
    $("#TabelaBalancete").empty();
    $('#modalbalancete').modal('open');
    modalBalancete()
}

function modalBalancete() {
    cliente = $("#idcliente")

    $.ajax({
        type: 'GET',
        url: "/producoes/" + cliente.val() + "/listaBalancete.json",
        success: function (listaBalancete) {
            for (var i = 0; i < listaBalancete.length; i++) {
				
				var idProducao = listaBalancete[i].producao.id
                var idBalancete = listaBalancete[i].id;
                var gerouFaturamento = listaBalancete[i].gerouFaturamento;
                var reducaoImposto = "<td> R$ " + listaBalancete[i].reducaoImposto + "</td>";
                var valorTotal = "<td>  R$ " + listaBalancete[i].valorTotal + "</td>";
                var anoCampanha = "<td>" + listaBalancete[i].producao.ano + "</td>";
				var versao  = "<td>" + listaBalancete[i].versao + "</td>";
                if (gerouFaturamento == false) {
                    gerouFaturamento = "<td>" + "Não" + "</td>";
                } else {
                    gerouFaturamento = "<td>" + "Sim" + "</td>";
                }

               	buscaIdEtapasBalancete(idProducao)
				var idEtapas = $('#idDoEtapas').val();
				console.log("IdEtapas "+idEtapas)

                var botaoBalancete = "<td><button class='btn waves-effect light-blue darken-4 ' onclick= window.location.href='/producoes/balanceteUpdate/" + idBalancete + "?idEtapa=" + idEtapas + "'><i class='material-icons''>arrow_forward</i></button> </td>"
                $("#TabelaBalancete").append("<tr>" + anoCampanha + versao +valorTotal + reducaoImposto + gerouFaturamento + botaoBalancete + "</tr>")

            }
        }
    })

 	function buscaIdEtapasBalancete(idProducao) {

    	$.ajax({
        	type: 'GET',
        	url: "/producoes/" + idProducao + "/etapasBalancetes.json",
			async: false,
        	success: function (etapa) {
				 $('#idDoEtapas').val(etapa.id);
            }
    	})
	}

}

function relatorio() {
    $("#TabelaRelatorio").empty();
    $('#modalRelatorio').modal('open');
    modalRelatorio()
}

function modalRelatorio() {

    cliente = $("#idcliente")

    $.ajax({
        type: 'GET',
        url: "/producoes/" + cliente.val() + "/listaRelatorio.json",
        success: function (listaRelatorio) {
            for (var i = 0; i < listaRelatorio.length; i++) {
				console.log("informaçoes listaRelatorio ",listaRelatorio[i]);
				var idRelatorioTecnico = listaRelatorio[i].id ;
                var anoCampanha = "<td>" + listaRelatorio[i].producao.ano + "</td>";
                var numero = "<td>" + listaRelatorio[i].numero + "</td>";
                var nome = "<td>" + listaRelatorio[i].nome + "</td>";
				var idProducao = listaRelatorio[i].producao.id;

                if (listaRelatorio[i].estado == null) {
                    estado = "<td>" + "" + "</td>";
                } else {
                    estado = "<td>" + listaRelatorio[i].estado + "</td>";
                }

                if (listaRelatorio[i].feitoPor == null) {
                    feitoPor = "<td>" + "" + "</td>";
                } else {
                    feitoPor = "<td>" + listaRelatorio[i].feitoPor.nome + "</td>"
                }

                if (listaRelatorio[i].validadoPor == null) {
                    validadoPor = "<td>" + "" + "</td>";
                } else {
                    validadoPor = "<td>" + listaRelatorio[i].validadoPor.nome + "</td>";
                }
				
				buscaIdEtapasRelatorios(idProducao)
				var idEtapas = $('#idDoEtapas').val();
				console.log("IdEtapas "+idEtapas)

				var botaoRelatorio = "<td> <button class='btn waves-effect light-blue darken-4 ' onclick= window.location.href='/producoes/relatoriosUpdate/" + idRelatorioTecnico + "?idEtapa=" + idEtapas + "'><i class='material-icons'>arrow_forward</i></button> </td>"
                $("#TabelaRelatorio").append("<tr class='linhaRelatorio'>" + anoCampanha + numero + nome + estado + feitoPor + validadoPor + botaoRelatorio+ "</tr>")
            }
        }
    })

	function buscaIdEtapasRelatorios(idProducao) {

    	$.ajax({
        	type: 'GET',
        	url: "/producoes/" + idProducao + "/etapasRelatorios.json",
			async: false,
        	success: function (etapa) {
				 $('#idDoEtapas').val(etapa.id);
			}
		})
	}
}

//teste
function FiltroProd(){
	$("#txt-pesq-empresa").change(function () {
	var empresa = $.trim($("#txt-pesq-empresa").val())
	$("#txt-pesq-empresa").val(empresa);
	})
}






