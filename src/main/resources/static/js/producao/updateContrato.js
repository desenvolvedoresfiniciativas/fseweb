var idContratoInicial = document.querySelector('input[name="contrato.id"]:checked').value;

$('#btn-add-producao').click(
	function() {
		var botao = $(this);

		var idContrato = document.querySelector('input[name="contrato.id"]:checked').value;
		var idProducao = $('#producaoId').val()
		
		alteraFaturamentosDoContratoAlterado(idContrato, idProducao)
});

$(function(){
	console.log(idContratoInicial + " acho que é isso, fml")
})

$('#btnAlterar').click(function() {
	var idContrato = document.querySelector('input[name="contrato.id"]:checked').value;
	var idProducao = $('#producaoId').val()
	
	console.log(idContratoInicial, idContrato)
	
	var urlSalvarNovoContrato = '/producoes/salvaContrato/' + idProducao + '/' + idContrato;

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: urlSalvarNovoContrato,
		success: function(data) {
		}
	})
	
	var urlRecriarNovosFaturamentos = "/faturamentos/recriaFaturamentosPorContrato/" + idProducao + "/" + idContratoInicial + "/" + idContrato;
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: urlRecriarNovosFaturamentos,
		success: function(data) {
		}
	})
});

function alteraFaturamentosDoContratoAlterado(idContrato, idProducao) {
	console.log(idContratoInicial, 'o id do contrato que será alterado é: ' +idContrato)
	console.log('id prod ' + idProducao)
	
	if(idContratoInicial != idContrato) {
		$.ajax({
			type: 'GET',
			url: "/faturamentos/" + idProducao + "/buscaFaturamentosPorProducao.json",
			success: function (data) {
				var exibeModal = false;
				var possuiNotasGeradas = false;

				if(data.length <= 0) {
					exibeModal = false;
					possuiNotasGeradas = false;
					salvaProducao(idContrato, idProducao);
				} else if(data.length > 0) {
					exibeModal = true;
					for(var i = 0; i < data.length; i++) {
						var faturamento = data[i]
						
						if(faturamento.emitido) {
							possuiNotasGeradas = true;
						} else {
						}
					}
				}

				if(exibeModal == true && possuiNotasGeradas == false) {
					$('#modalDeleteFaturamentos').modal('open');
				} else if(possuiNotasGeradas == true) {
					Materialize.toast('Este Contrato possui Notas Fiscais. Não é possível alterar.', 5000, 'rounded');
				}
			}
		})
	} else {
		console.log("são iguais")
		salvaProducao(idContrato, idProducao);
	}
}

function salvaProducao(idContrato, idProducao) {
	var urlMontada = '/producoes/salvaContrato/' + idProducao + '/' + idContrato
	
	$.ajax({
	type: "POST",
	contentType: "application/json",
	url: urlMontada,
	success: function(data) {
		Materialize.toast('Contrato atualizado com Sucesso!', 5000, 'rounded');
		}
	})
}


