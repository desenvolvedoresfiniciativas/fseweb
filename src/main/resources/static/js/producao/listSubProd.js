$(function() {
})

$('#btn-salvaMapeamento').click(function() {

	var producao = {
		id: $("#idProd").val(),
		oppIdentificadas: $("#oppIdentificadas").is(':checked')
	}

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/mapeamento",
		data: JSON.stringify(producao),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Mapeamento atualizado com sucesso!',
						5000, 'rounded');
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	})
})