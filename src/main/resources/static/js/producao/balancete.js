$(function(){
	$('.modal').modal();
	$('#modalEficiencia').modal();
	 $('select').material_select();
	$("#form").submit(function () {
	console.log("AQUI");
        $("#btn-cad-cliente").attr("disabled", true);
        return true;
});

     $("#validadoEm").tooltipster({
         trigger: "custom"
     });

	 $('.collapsible').collapsible({
		 accordion : true
		 });
	 sumCamps();
	 calcReduction();
	 porcentagemRealizado();
	
	 blockBalanceteValidado()
	 verificaPendencias();
	 verificaGeraFaturamento();
	 verificaBalanceteGerouFat();
	 submitAdd();
	 submitSalva();
	
	 
	 
	 

	var rhMask = Number($("#rh").text()).toFixed(2)
	var terceirosMask = Number($("#terceiros").text()).toFixed(2)
	var materiaisMask = Number($("#materiais").text()).toFixed(2)
	var despesasMask = Number($("#despesas").text()).toFixed(2)
	var beneficioMask = Number($("#beneficio").text()).toFixed(2)
	
	$("#rh").text(mascaraValor(rhMask))
	$("#terceiros").text(mascaraValor(terceirosMask))
	$("#materiais").text(mascaraValor(materiaisMask))
	$("#despesas").text(mascaraValor(despesasMask))
	$("#beneficio").text(mascaraValor(beneficioMask))
	$("#btn-add-prejuizo").css('display', 'none')
	$("#btn-update-prejuizo").css('display', 'none')
	
	var checked = $('#prejuizo').is(":checked")
				if(checked == true){
					$("#validado :input").attr("disabled", true);
					$("#valoresBalancete :input").attr("disabled", true);
					$("somarDespesas :input").attr("disabled", true);
					$("#despesasTotais :input").attr("disabled", true);
					$("#observacoes :input").attr("disabled", true);
					$("#enviadoValidado :input").attr("disabled", true);
					$("#versoesFinais :input").attr("disabled", true);
					$('#somar').attr('disabled','disabled');
					$('#reduction').attr('disabled','disabled');
					//$("#btn-cad-cliente").action = '@{/producoes/balancete/addPrejuizo}';
					$("#btn-add-prejuizo").css('display', 'block')
					$("#btn-update-prejuizo").css('display', 'block')
					$("#btn-cad-cliente").css('display', 'none')
				}else{
					$("#validado :input").attr("disabled", false);
					$("#valoresBalancete :input").attr("disabled", false);
					$("somarDespesas :input").attr("disabled", false);
					$("#despesasTotais :input").attr("disabled", false);
					$("#observacoes :input").attr("disabled", false);
					$("#enviadoValidado :input").attr("disabled", false);
					$("#versoesFinais :input").attr("disabled", false);
					$('#somar').removeAttr('disabled');
					$('#reduction').removeAttr('disabled');
					$("#btn-cad-cliente").css('display', 'block')
					$("#btn-update-prejuizo").css('display', 'none')
					$("#btn-add-prejuizo").css('display', 'none')
				}

	$("#tabela-balancete").DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			},
			"select": {
				"rows": {
					"_": "Selecionado %d linhas",
					"0": "Nenhuma linha selecionada",
					"1": "Selecionado 1 linha"
				}
			}
		},
		bFilter: false,
		iDisplayLength: 1000,
		columnDefs: [
			{
				targets: [0, 1, 2],
				className: 'mdl-data-table__cell--non-numeric'
			}
		]
	});

})

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

$("#validadoEm").change(function() {
	$("#validado").prop('checked', true);
	
	var dataCliente = $('#validadoEm').val()
	var checked = $('#validado-check').is(":checked")
			
	if(dataCliente != 'Sem data' && checked == true){
		$('#btn-cad-cliente').attr("disabled", false);
		$("#validadoEm").prop("disabled", false);
		$("#validadoEm").tooltipster("close").tooltipster("content", "Data de validação não preenchida");
		$(".relatorioCalculo").trigger("change");
	}
})

function verificaPendencias(){
	var contrato = $("#statusContrato").val();
	var nf = $("#statusContatoNF").val();
	var anterior = $("#statusVersaoAnterior").val();
	
	var verifica = 0

	if(contrato=="false"){
		Materialize.toast('A produção não possui contrato vigente.', "stay on", 'rounded');
		verifica++
	}
	
	if(nf=="false"){
		Materialize.toast('O cliente não possui um contato para recebimento de Nota Fiscal.', "stay on", 'rounded');
		verifica++
	}
	
	if(anterior=="false"){
		Materialize.toast('A versão anterior de balancete precisa ser validada.', "stay on", 'rounded');
		verifica++
	}
	
	console.log(verifica)
	
	if(verifica>0){
		$("#validadoEm").attr("disabled", true)
		$("#validado").addClass("tooltipped")
		$('.tooltipped').tooltip();
	}
	
}

function handleChange(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 100) input.value = 100;
  }



$("#unlockBalanceteValidado").click(function() {
	
	
	$("#balanceteUpdateForm :input").prop("disabled", false);
	
})


function calcReduction() {
	$('#reduction').click(function() {
		var ali = $("#aliquota").val();
		var exclusao = $("#select-exclusaoUtilizada :selected").val();
		var total = $("#valorTotal").val();
		
		var count = 0;
		
		console.log("pre: "+ali)
		console.log("pre: "+exclusao)
		console.log("pre: "+total)
		
		if(ali=="" || ali=="NULL" || ali==null){
			Materialize.toast('Não é possível calcular a redução sem o valor de "Alí. IR/CSLL"',5000, 'rounded');
		} else {
			count++
		}
		if(exclusao=="" || exclusao=="NULL" || exclusao==null){
			Materialize.toast('Não é possível calcular a redução sem o valor de "Exclusão Utilizada"',5000, 'rounded');
		} else {
			count++
		}
		if(total=="" || total=="NULL" || total==null){
			Materialize.toast('Não é possível calcular a redução sem o valor de "Despesas totais"',5000, 'rounded');
		} else {
			count++
		}

		if(count==3){
			calculoTrue(total, ali, exclusao);
		}
		
	})
}

function calculoTrue(total, ali, exclusao){
	total = total.replace(/\./g,'')
	total = total.replace(",",".")
	total = parseFloat(total).toFixed(2)

	ali = "0." + ali

	if (exclusao == "SESSENTA") {
		exclusao = 0.60;
	}
	if (exclusao == "SETENTA") {
		exclusao = 0.70;
	}
	if (exclusao == "OITENTA") {
		exclusao = 0.80;
	}

	ali = parseFloat(ali);
	exclusao = parseFloat(exclusao);


	var reducao = (total) * (exclusao) * (ali);
	console.log(reducao)

	reducao = mascaraValor(reducao.toFixed(2));

	$("#reducaoImposto").val(reducao)
	$(".relatorioCalculo").trigger("change");
}




function blockBalanceteValidado(){
	if ($("#validado-check").is(':checked')) {
		$("#balanceteUpdateForm :input").not("#unlockBalanceteValidado").prop(
				"disabled", true);
		$("#blockMsgValidado").show();
		$("#unlockValidado").show();
	}

}


function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

function sumCamps() {
	var sum;
	var rh;
	var materiais;
	var terceiros;
	var outras;
	$('#somar').click(
			function() {
				if ($('#valorRH').val() != null) {
					rh = parseFloat($('#valorRH').val().replace(/\./g, '')
							.replace(",", "."));
				} else {
					rh = 0
				}
				if ($('#valorMateriais').val() != null) {
					materiais = parseFloat($('#valorMateriais').val().replace(
							/\./g, '').replace(",", "."));
				} else {
					materiais = 0
				}
				if ($('#valorServicosTerceiros').val() != null) {
					terceiros = parseFloat($('#valorServicosTerceiros').val()
							.replace(/\./g, '').replace(",", "."));
				} else {
					terceiros = 0
				}
				if ($('#valorOutrasDespesas').val() != null) {
					outras = parseFloat($('#valorOutrasDespesas').val()
							.replace(/\./g, '').replace(",", "."));
				} else {
					outras = 0
				}
				sum = rh + materiais + terceiros + outras;
				console.log(sum)
				sum = mascaraValor(sum.toFixed(2));
				$('#valorTotal').val(sum);
			});
}

$('#btn-cad-cliente').click(
		function() {
			if ($.trim($("#select-relatorioCalculo").find(":selected").val()) === "") {
				Materialize.toast('Preencha "Relatório cálculo".', 6000,
						'rounded');
			}
			if ($.trim($("#select-exclusaoUtilizada").find(":selected").val()) === "") {
				Materialize.toast('Preencha "Exclusão utilizada".', 6000,
						'rounded');
			}
			if ($.trim($("#select-feitoPor").find(":selected").val()) === "") {
				Materialize.toast('Preencha "Feito por".', 6000,
						'rounded');
			}
			if ($.trim($("#valorTotal").val()) === "") {
				Materialize.toast('Preencha "Valor total".', 6000,
						'rounded');
			}
			if ($.trim($("#porcentagemRealizado").val()) === "") {
				Materialize.toast('Preencha "Porcentagem Realizado".', 6000,
						'rounded');
			}
		
			});


$('#prejuizo').click(
		function() {
				var checked = $('#prejuizo').is(":checked")
				if(checked == true){
					$("#validado :input").attr("disabled", true);
					$("#valoresBalancete :input").attr("disabled", true);
					$("somarDespesas :input").attr("disabled", true);
					$("#despesasTotais :input").attr("disabled", true);
					$("#observacoes :input").attr("disabled", true);
					$("#enviadoValidado :input").attr("disabled", true);
					$("#versoesFinais :input").attr("disabled", true);
					$('#somar').attr('disabled','disabled');
					$('#reduction').attr('disabled','disabled');
					//$("#btn-cad-cliente").action = '@{/producoes/balancete/addPrejuizo}';
					$("#btn-add-prejuizo").css('display', 'block')
					$("#btn-update-prejuizo").css('display', 'block')
					$("#btn-cad-cliente").css('display', 'none')
				}else{
					$("#validado :input").attr("disabled", false);
					$("#valoresBalancete :input").attr("disabled", false);
					$("somarDespesas :input").attr("disabled", false);
					$("#despesasTotais :input").attr("disabled", false);
					$("#observacoes :input").attr("disabled", false);
					$("#enviadoValidado :input").attr("disabled", false);
					$("#versoesFinais :input").attr("disabled", false);
					$('#somar').removeAttr('disabled');
					$('#reduction').removeAttr('disabled');
					$("#btn-cad-cliente").css('display', 'block')
					$("#btn-update-prejuizo").css('display', 'none')
					$("#btn-add-prejuizo").css('display', 'none')
				}
			})			

			
$('#btn-add-prejuizo').click(
		function addPrejuizo() {
			console.log('função para adicionar balancete prejuizo')
			
			var urlMontada = "/producoes/balancete/addPrejuizo"
			var tipoApuracao = $('#tipoApuracao').val()
			var producaoId = $('#producaoId').val();
			var prejuizo = $('#prejuizo').is(":checked")
			var porcentagemRealizado = document.getElementById("porcentagemRealizado").value
			
			var producao = {
				id: producaoId
			}
			console.log('o tipo de apuração é: ', tipoApuracao)
			if(tipoApuracao === 'MENSAL'){
				var versao = $("#select-relatorioCalculo-mensal").val()	
			} if(tipoApuracao === 'TRIMESTRAL'){
				var versao = $("#select-relatorioCalculo-trimestral").val()	
				
			} if(tipoApuracao === 'ANUAL'){
				var versao = $("#select-relatorioCalculo-anual").val()	
			}
			console.log('os dados que serão enviados são: ' , producaoId,prejuizo,porcentagemRealizado,versao)
			
			var parametros = {
				versao: versao,
				producaoId: producaoId,
				prejuizo: prejuizo,
				porcentagemRealizado: porcentagemRealizado,
			
			}
			
			var balanceteCalculo = {
					
					versao: versao,
					feitoPor: null,
					validadoPor: null, 
					producao: producao,
					valorRH: null,
					valorMateriais: null,
					valorServicosTerceiros: null,
					valorOutrasDespesas: null,
					valorTotal: null,
					exclusaoUtilizada: null,
					reducaoImposto: null,
					enviadoEm: null,
					validadoEm: null,
					versaoFinal: null,
					versaoFinal: null,
					reunioesNecessarias: null,
					gerouFaturamento: false,
					validado: false,
					observacoes: null
					
			};
			//console.log('os dados de balancete são: ', balanceteCalculo)

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: urlMontada,
		data: JSON.stringify(parametros),
		headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
				},
		success: function() {
					Materialize
						.toast(
							'Balancete adicionado com sucesso',
							5000, 'rounded');
				}
		
		})	
	})	
	
	$('#btn-update-prejuizo').click(
		function updatePrejuizo() {
			console.log('função para atualizar balancete como prejuizo')
			var idBalancete = $('#id').val()
			
			var urlMontada = "/producoes/balancete/update" 
			var tipoApuracao = $('#tipoApuracao').val()
			var producaoId = $('#producaoId').val();
			var prejuizo = $('#prejuizo').is(":checked")
			var porcentagemRealizado = document.getElementById("porcentagemRealizado").value
			
			var producao = {
				id: producaoId
			}
			console.log('o tipo de apuração é: ', tipoApuracao)
			if(tipoApuracao === 'MENSAL'){
				var versao = $("#select-relatorioCalculo-mensal").val()	
			} if(tipoApuracao === 'TRIMESTRAL'){
				var versao = $("#select-relatorioCalculo-trimestral").val()	
				
			} if(tipoApuracao === 'ANUAL'){
				var versao = $("#select-relatorioCalculo-anual").val()	
			}
			console.log('os dados que serão enviados são: ' , producaoId,prejuizo,porcentagemRealizado,versao)
			
			var parametros = {
				versao: versao,
				producaoId: producaoId,
				prejuizo: prejuizo,
				porcentagemRealizado: porcentagemRealizado,
			
			}
			
			var balanceteCalculo = {
					
					versao: versao,
					feitoPor: $('#select-feitoPor').val(),
					validadoPor: $('#select-feitoPor').val(),
					valorRH: $('#valorRH').val(),
					valorMateriais: $('#valorMateriais').val(),
					valorServicosTerceiros: $('#valorServicosTerceiros').val(),
					valorTotal: $('#valorTotal').val(),
					exclusaoUtilizada: $('#select-exclusaoUtilizada').val(),
					reducaoImposto: $('#select-exclusaoUtilizada').val(),
					enviadoEm: $('#enviadoEm').val(),
					validadoEm: $('#validadoEm').val(),
					versaoFinal: $('#versaoFinal').val(),
					validado: $('#validado-check').val(),
					observacoes: $('#observacoes').val()
					
			};
			//console.log('os dados de balancete são: ', balanceteCalculo)
			var etapasId = $('#etapasId').val()

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: urlMontada,
		data: JSON.stringify(balanceteCalculo, etapasId),
		headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
				},
		success: function() {
					Materialize
						.toast(
							'Balancete adicionado com sucesso',
							5000, 'rounded');
				}
		
		})	
	})				

			
$(".relatorioCalculo").change(function () {

			var producaoId = $("#producaoId").val();
			var balanceteSelecionadoMensal = $("#select-relatorioCalculo-mensal").val()
			var balanceteSelecionadoTrimestral = $("#select-relatorioCalculo-trimestral").val()
			var balanceteSelecionadoAnual = $("#select-relatorioCalculo-anual").val()
			
			
			var reducaoImposto = $("#reducaoImposto");
			var reducao = (reducaoImposto.val() === "")
			
			var porcentagemRealializado =$("#porcentagemRealizado");
			var porcentagem = (porcentagemRealializado.val() === "");
			
			console.log("redução imposto",reducao);
			console.log("balanceteSelecionadoMensal",balanceteSelecionadoMensal);
			console.log("balanceteSelecionadoTrimestral",balanceteSelecionadoTrimestral);
			console.log("balanceteSelecionadoAnual",balanceteSelecionadoAnual);
			
    $.ajax({
        type: 'GET',
        url: "/producoes/" + producaoId + "/getBalanceteByProducao.json",
        success: function (listaBalancete) {
			console.log("entrou no sucess listaBalancete",listaBalancete)
			
            for (var i = 0; i < listaBalancete.length; i++) {
			
			//ANUAL
			if(listaBalancete[i].producao.tipoDeApuracao == 'ANUAL'){
				if(balanceteSelecionadoAnual == listaBalancete[i].versao || reducao == true || porcentagem == true){
					console.log("balancetes versao",listaBalancete[i].versao);
						console.log("bloqueio balancete if ");
					 	$('#btn-cad-cliente').attr("disabled", true);
						if(balanceteSelecionadoAnual == listaBalancete[i].versao){
							Materialize.toast('Já existe um balancete com essa versão', 3000,
							'rounded');
						}else{
							if(reducao == true){
								Materialize.toast('Preencha "Reducao Imposto".', 3000,
								'rounded');
							}else{
								if(porcentagem == true){
									Materialize.toast('Preencha "Porcentagem Realizado".', 3000,
								'rounded');
								}
							}
						}
						break;
				}
				else{
					if(balanceteSelecionadoAnual != listaBalancete[i].versao && reducao == false && porcentagem == false){
						console.log("else anual",listaBalancete[i].versao)
					$('#btn-cad-cliente').attr("disabled", false);
					
					}
					
				}
				
			}
			
			//TRIMESTRAL
			if(listaBalancete[i].producao.tipoDeApuracao == 'TRIMESTRAL'){
				if(balanceteSelecionadoTrimestral == listaBalancete[i].versao || reducao == true || porcentagem == true){
					console.log("balancetes versao",listaBalancete[i].versao);
						console.log("bloqueio balancete if reducao", reducao );
					 	$('#btn-cad-cliente').attr("disabled", true);
						if(balanceteSelecionadoTrimestral == listaBalancete[i].versao ){
							Materialize.toast('Já existe um balancete com essa versão', 3000,
							'rounded');
						}else{
							if(reducao == true){
								Materialize.toast('Preencha "Reducao Imposto".', 3000,
								'rounded');
							}else{
								if(porcentagem == true){
									Materialize.toast('Preencha "Porcentagem Redução".', 3000,
								'rounded');
								}
							}
						}
						break;
				}
				else{
					if(balanceteSelecionadoTrimestral != listaBalancete[i].versao && reducao == false && porcentagem == false){
						console.log("else trimestral",listaBalancete[i].versao)
					 
					$('#btn-cad-cliente').attr("disabled", false);
					}
					
				}
				
			}
			//MENSAL
			if(listaBalancete[i].producao.tipoDeApuracao == 'MENSAL'){
				if(balanceteSelecionadoMensal == listaBalancete[i].versao || reducao == true){
					console.log("balancetes versao",listaBalancete[i].versao);
					
					 	$('#btn-cad-cliente').attr("disabled", true);
					 	if(balanceteSelecionadoMensal == listaBalancete[i].versao ){
							Materialize.toast('Já existe um balancete com essa versão', 3000,
							'rounded');
						}else{
							if(reducao == true){
								Materialize.toast('Preencha "Reducao Imposto".', 3000,
								'rounded');
							}
						}
						break;
				}
				else{
					if(balanceteSelecionadoMensal != listaBalancete[i].versao && reducao == false && porcentagem == false){
						console.log("else mensal",listaBalancete[i].versao)
					$('#btn-cad-cliente').attr("disabled", false);
					}
				}
			}
			}						
		}
	});
})

$('#validado-check').click(
		function() {
			var dataCliente = $('#validadoEm').val()
			var checked = $('#validado-check').is(":checked")
			//$(".relatorioCalculo").trigger("change");
			if(dataCliente == 'Sem data' && checked == true){
				console.log("entroiu no validado check")
				$('#btn-cad-cliente').attr("disabled", true);
				$("#validadoEm").tooltipster("open").tooltipster("content", "Data de validação não preenchida");
				$("#validadoEm").tooltipster("open").tooltipster("content", "Data de validação não preenchida");
			}else{console.log("entroiu no validado check else")
				$('#btn-cad-cliente').attr("disabled", false);
				$(".relatorioCalculo").trigger("change");
			}
			
			});
$('#validado-check').change(function(){
	$('#validado-check').trigger("click");
})			
			
function verificaBalanceteGerouFat(){
	verificaGeraFaturamento();
// verifica se esse balancete já gerou faturamento, se sim o desbloqueio do botão(unlockBalanceteValidado) fica bloqueado
	var producaoId = $("#producaoId").val();
	var idBalancete = $("#idBalancete").val();
	
        $.ajax({
        type: 'GET',
        url: "/producoes/" + producaoId + "/getBalanceteByProducao.json",
        success: function (listaBalancete) {
		//recebe a lista de balacetes dessa prod, identifica o balancete acessado e se esse gerou fat
			   for (var i = 0; i < listaBalancete.length; i++) {
					if(listaBalancete[i].id == idBalancete && listaBalancete[i].gerouFaturamento == true){
						$("#unlockBalanceteValidado").prop("disabled", true);
					}else{
						if(listaBalancete[i].id == idBalancete && listaBalancete[i].gerouFaturamento == false){
						$("#unlockBalanceteValidado").prop("disabled", false);
						}
					}
				}
		}
		})
}	

$("#reducaoImposto").change(function() {
	$(".relatorioCalculo").trigger("change");
})	




function verificaGeraFaturamento(){
// verifica se esse balancete ainda tem faturamento(pode ter sido apagado), caso não tenha mais, o atributo geroufaturamento é alterado para  false.

console.log("verificaGeraFaturamento");
var producaoId = $("#producaoId").val();
var idBalancete = $("#idBalancete").val();

console.log("producaoId",producaoId);
console.log("idBalancete",idBalancete);
	
        $.ajax({
        type: 'POST',
        url: "/producoes/" + producaoId  + "/"+idBalancete+ "/getFaturamentoByBalancete.json",
        async: false,
        success: function (data) {
		}
		})
}

//teste
var i = 0
$('.add').click(

	function() {

		var divPai = $('#' + $(this).parent().attr("id"))

		var divRow = $("<div class='row'>")
		var divAtividade = $("<div class='input-field col s3'>")
		var divConsultor = $("<div class='input-field col s4'>")
		var divHoras = $("<div class='input-field col s2'>")
		var divTarefa = $("<div class='input-field col s2'>");
		var selectAtividade = $("<select id='atividade" + i + "' class='select'>");
		var labelAtividade = $("<label for='atividade'>").text(
			"Tipo de Atividade")
		selectAtividade.append($('<option>', {
			value: "",
			text: "Selecione",
			disabled: 'disabled',
			selected: 'selected',
		}));
		selectAtividade.append($('<option>', {
			value: "Feito Por",
			text: "Feito Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Validado Por",
			text: "Validado Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Revisado Por",
			text: "Revisado Por",
		}));
		selectAtividade.append($('<option>', {
			value: "Gestão Por",
			text: "Gestão Por",
		}));

		var selectConsultor = $("<select id='consultor" + i + "' class='select consultores'>");
		var labelConsultor = $("<label for='consultor'>").text(
			"Consultor")
		selectConsultor.append($('<option>', {
			value: "",
			text: "Selecione",
			disabled: 'disabled',
			selected: 'selected',
		}));
			
		// SELECT DE TAREFAS //
		var selectTarefas = $("<select id='tarefas"+ i
					+ "' class='select tarefas'>");
		var labelTarefas = $("<label for='tarefas" + i + "'>").text(
					"Tarefas")
		selectTarefas.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
		var inputHoras = $("<input type='time' required class='hora' id='horas"+ i
					+ "'  value='00:00' step='3600''>");
		
		selectTarefas.appendTo(divTarefa);
		labelTarefas.appendTo(divTarefa);
		divTarefa.appendTo(divRow);
		selectAtividade.appendTo(divAtividade);
		selectConsultor.appendTo(divConsultor);
		inputHoras.appendTo(divHoras)
		labelAtividade.appendTo(divAtividade);
		labelConsultor.appendTo(divConsultor);
		divAtividade.appendTo(divRow);
		divConsultor.appendTo(divRow);
		divHoras.appendTo(divRow);
		divRow.appendTo(divPai)

		populaSelects(selectConsultor);
		var idEtapa = 66;
		populaSelectsTarefas(selectTarefas,idEtapa);
		
		selectAtividade.material_select();
		selectConsultor.material_select();
		selectTarefas.material_select();
		i++;

	})
		
function populaSelects(selectConsultor) {

	$.when(solicitadoresConsultores()).done(function(consultores) {

		selectConsultor.find('option').remove()

		selectConsultor.each(function(i, obj) {
			var select = $(this);
			select.append($('<option>', {
				value: "",
				text: "Selecione",
				disabled: 'disabled',
				selected: 'selected',
			}));

			for (var i = 0; i < consultores.length; i++) {
				select.append($('<option>', {
					value: consultores[i].id,
					text: consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
		})
	});

}

$('.salvar').click(function() {
	var aux = 0;

	var idEtapa = 66;
	var atividades = [];

	var divPai = $($(this).prev())
	var idDivPai = divPai.attr("id")

	var firstIteration = true;
	var j = 0

	$("#corpo > div").each(function() {
		
		var atividade = $('#atividade' + j).find(":selected").val();
		var consultorId = $('#consultor' + j).find(":selected").val();
		var horas = $('#horas' + j).val();
		var tarefa = $('#tarefas'+j).find(":selected").val();
		
		//input zero horas
		if(horas =='00:00'){
			$('#horas' + j).animate({width: "150px" }, 800)
			aux++;
		}
		//formatando horas
		var tempoAux = horas.split(":");
		var hora = tempoAux[0];
		var minutos = tempoAux[1];
		//criando nova variavel, formato float
		var horaFormatada  = hora+"."+minutos;
		
		var etapa = {
			id:66
		}

		var producao = {
			id: $('#producaoId').val()
		}

		var consultor = {
			id: consultorId
		}
		
		 tarefa = {
			id :  tarefa
		} 
		
		var atividade = {
			atividade: atividade,
			consultor: consultor,
			horas: horaFormatada,
			etapa: etapa,
			producao: producao,
			tarefa: tarefa,
		};
		atividades.push(atividade)
		j++
	})

	console.log(atividades)
	if(atividades.length == 0 || aux != 0){
		Materialize.toast("Preencher campo horas", 5000, 'red');
	}else{

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/eficiencia/salva",
		data: JSON.stringify(atividades),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Atividades salvas com sucesso!',
						5000, 'rounded');
				$('#modalEficiencia').modal('close');
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});
	}
})

//função para impedir inserção de zero horas
$(document).on("change", ".hora", function() {
	var hora = $(this);

	tempo = hora.val();
	if (tempo <= 0) {
		$(this).val('0.1')
	}
})

//function para preencher o select tarefas
function populaSelectsTarefas(selectTarefas,idEtapa) {
	$.when(solicitadoresTarefas(idEtapa)).done(function(tarefa) {
		selectTarefas.find('option').remove()
		selectTarefas.each(function(i, obj) {
		
			var select = $(this);
			select.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
			var tarefaId =$("#idTarefaAtual").val();
			for (var i = 0; i < tarefa.length; i++) {
					select.append($('<option>', {
					value :tarefa[3].id,
					text : tarefa[3].nome,
					selected : 'selected',
				}));
			}
		})
	});
}
//function para pegar as tarefas(todas)
function solicitadoresTarefas(idEtapa){
	var url = "/producoes/"+idEtapa+"/tarefas.json"

	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
		}
	});
}

function solicitadoresConsultores() {
	var url = "/producoes/consultores.json"

	return $.ajax({
		type: 'GET',
		url: url,
		async: false,
		success: function(data) { }
	});
}

$('#btn-cad-cliente').click(function () {
	$("#submitAdd").val(true);
	var boolean = $("#submitAdd").val();
	sessionStorage.setItem("boolean", boolean);
	formAdd();
})

function submitAdd(){
	  $("#submitAdd").val(sessionStorage.getItem("boolean"));
	 var teste =   $("#submitAdd").val();
	 if(teste == 'true'){
		var boolean = ""
		sessionStorage.setItem("boolean", boolean);
		$("#submitAdd").val(sessionStorage.getItem("boolean"));
	 	var selected =   $("#submitAdd").val();
	 	$('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
	}else{
		console.log("ENTROU NO ELSE");
	}
}


$('#btn-update-cliente').click(function (){
	var checked = $('#versaoFinal').is(":checked");
	 var status = $("#statusVersaoFinal").val();
	//if(checked == true && status == 'false'){
		$("#submitSalva").val(true);
		var booleanSalva = $("#submitSalva").val();
		sessionStorage.setItem("booleanSalva", booleanSalva);
	//}else{
		//console.log("versão final não esta checked");
	//}
})

function submitSalva(){
	  $("#submitSalva").val(sessionStorage.getItem("booleanSalva"));
	 var teste =   $("#submitSalva").val();
	 if(teste == 'true'){
		var booleanSalva = ""
		sessionStorage.setItem("booleanSalva", booleanSalva);
		 $("#submitSalva").val(sessionStorage.getItem("booleanSalva"));
		 var selected = $("#submitSalva").val();
	 	$('#modalEficiencia').modal({ dismissible: false});
        $('#modalEficiencia').modal('open');
	}else{
		console.log("entrou no else");
	}
	 
}


function porcentagemRealizado(){
	var porcentagem = $("#porcentagemRealizado").val();
	if(porcentagem == ""){
		$("#btn-cad-cliente").attr("disabled", true);
		console.log("entrou no porcentagem == NULL")
	}else{
		$(".relatorioCalculo").trigger("change");
	}
}
	
$("#porcentagemRealizado").change(function(){
	var porcentagem = $("#porcentagemRealizado").val();
	if(porcentagem == ""){
		$("#btn-cad-cliente").attr("disabled", true);
		console.log("entrou no porcentagem.change  == NULL")
	}else{
		$(".relatorioCalculo").trigger("change");
	}
})

