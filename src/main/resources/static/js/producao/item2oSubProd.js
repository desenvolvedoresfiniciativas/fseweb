$("#salvar").click(salvaItens);

$(function() {
	populaTarefas();
	inicializadores();
	$('#modalEficiencia').modal();

})

function populaTarefas() {

	$.when(solicitadoresItens()).done(function(itemValores) {
		
		var idConclusao = $("#idConclusao").val();
		var idProd = $("#idProd").val();
		
		// Handler de páginas fixas.
		$('.corpo').each(function(i, obj) {
			
			var tarefaId = $(this).attr('id');			
			var tarefaAtual = $(this).attr('name');
			
			    // Tarefas fixas
				if(tarefaId == 149){
					var button = $("<a href='/producoes/balancete/"+idConclusao+"' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar balancetes</a>")
					$('#' + tarefaId).append(button)
				}
				
				if(tarefaId == 152){
					var button = $("<a href='/producoes/relatorios/"+idConclusao+"' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar relatórios</a>")
					$('#' + tarefaId).append(button)
				}
				
				if(tarefaId == 182){
					var button = $("<a href='/producoes/projetos/"+idConclusao+"' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Projetos</a>")
					$('#' + tarefaId).append(button)
				}
				
				if(tarefaId == 193){
					var button = $("<a href='/producoes/habilitacaoPortaria/"+idConclusao+"' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Habilitação de Portarias</a>")
					$('#' + tarefaId).append(button)
				}
				
				if(tarefaId == 174){
					var button = $("<a href='/producoes/auditoriaEconomica/"+idConclusao+"' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Auditoria Econômica</a>")
					$('#' + tarefaId).append(button)
				}
				
				if(tarefaId == 171){
					var button = $("<a href='/producoes/auditoriaProjetos/"+idConclusao+"' class='waves-effect waves-light btn'><i class='material-icons left'>arrow_forward</i>Acessar Auditoria de Projetos</a>")
					$('#' + tarefaId).append(button)
				}

			// Append de Item valores no Corpo HTML
			for(var i = 0; i<itemValores.length; i++){
				var item = itemValores[i];
				if (item.tarefa.id == tarefaId) {
					switch (item.item.tipo){
					
					// -- !! CAMPO PARA DATA !! -- //
					case 'DATA':
					var div = $("<div>")
					var label = $("<label>").text(item.item.nome)
					var input = $("<br/><input style='width: 100px; color: black;' type='text' placeholder='"+item.item.tipo+"' class='datepicker' id="+item.id+" value='"+item.valor+"'>");
					label.appendTo(div);
					input.appendTo(div)
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
					
					$('#' + tarefaId).append(div)
					break;
					
					// -- !! CAMPO PARA CHECK !! -- //
					case 'CHECK':
					var div = $("<div style='padding-bottom: 10px; 'padding-top: 10px;'>")
					var label = $("<label id="+item.item.id+" for="+item.id+">").text(item.item.nome)
					var input = $("<input type='checkbox' name='finalizador."+item.item.defineRealizacao+"' class='filled-in' id="+item.id+">");
					
					if(item.valor == "TRUE"){
						input.attr("checked", true);
					}else{input.attr("checked", false);}
					
					input.appendTo(div);
					label.appendTo(div);
					div.append("<br/>")
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
					
					$('#' + tarefaId).append(div)
					break;
					

					// -- !! CAMPO PARA TEXTO !! -- //
					case 'TEXTO':
					var div = $("<div>")
					var label = $("<label for="+item.id+">").text(item.item.nome)
					var holder = $("#cnpjHolder").val();
					var cnpjTester = "CNPJ";
					if(item.item.nome==cnpjTester){
					var input = $("<input id='"+item.id+"' style='width: 600px' type='text' class='texto' value='"+holder+"' />");	
					} else {
					var input = $("<input id='"+item.id+"' placeholder='"+item.item.tipo+"' style='width: 600px' type='text' class='texto' value='"+item.valor+"' />");
					}
					
					label.append("<br/>")
					label.appendTo(div);
					input.appendTo(div);
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
					
					$('#' + tarefaId).append(div)
					break;
					
					// -- !! CAMPO PARA TEXTO LONGO !! -- //
					case 'TEXTOLONGO':
					var div = $("<div>")
					var label = $("<label for="+item.id+">").text(item.item.nome)
					var input = $("<input id='"+item.id+"' placeholder='"+item.item.tipo+"' type='text' class='texto' value='"+item.valor+"' />");
					label.append("<br/>")
					label.appendTo(div);
					input.appendTo(div);
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
						
					$('#' + tarefaId).append(div)
					break;
					
					// -- !! CAMPO PARA DINHEIRO !! -- //
					case 'DINHEIRO':
					var div = $("<div>")
					var label = $("<label for="+item.id+">").text(item.item.nome)
					console.log(item.item.readOnly)
					if(item.item.readOnly){
						var input = $("<input id='"+item.id+"' readOnly disabled placeholder='"+item.item.tipo+"' style='width: 200px;cursor:not-allowed' type='text' class='money' value='"+item.valor+"' />");
					}else{
						var input = $("<input id='"+item.id+"' placeholder='"+item.item.tipo+"' style='width: 200px' type='text' class='money' value='"+item.valor+"' />");
					}
					label.append("<br/>")
					label.appendTo(div);
					input.appendTo(div);
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
					
					$('#' + tarefaId).append(div)
					break;
					
					// -- !! CAMPO PARA PORCENTAGEM !! -- //
					case 'PORCENTAGEM':
					console.log("ENRTROU NO PERCEEEEEEEEEEEEEEEEEENT")
					var div = $("<div>");
					var label = $("<label for="+item.id+">").text(item.item.nome)
					var input = $("<input id='"+item.id+"' placeholder='"+item.item.tipo+"' onchange='handleChange(this)' class='percent' style='width: 200px' type='text' value='"+item.valor+"' />");
					label.append("<br/>")
					label.appendTo(div);
					input.appendTo(div);
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
					
					$('#' + tarefaId).append(div)
					break;
					
					// -- !! CAMPO PARA CONSULTOR !! -- //
					case 'LISTA_CONSULTOR':
						
					var selecionado = 'Selecione';
					var valorSelecionado = '';
					if(item.valor != ""){
							selecionado = item.valor;
							valorSelecionado = item.valor;
					}
					
					var div = $("<div>")
					
					var select = $("<select id='"+item.id+"' class='consultores select'>");
					var label = $("<label for="+item.id+">").text(item.item.nome)
					select.append($('<option>', {
							value: valorSelecionado,
							text: selecionado,
							disabled: 'disabled',
							selected: 'selected',
						}));
					
					label.appendTo(div);
					select.appendTo(div);
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
					
					$('#' + tarefaId).append(div)

					break;
					
					// -- !! CAMPO PARA LISTA DE VALORES !! -- //
					case 'LISTA':
					
					var selecionado = 'Selecione';
					var valorSelecionado = '';
					if(item.valor != ""){
						selecionado = item.valor;
						valorSelecionado = item.valor;
					}
						
					var div = $("<div>")
					
					var select = $("<select id='"+item.id+"' name='"+item.item.id+"' class='lista select'>");
					var label = $("<label for="+item.id+">").text(item.item.nome)
					select.append($('<option>', {
							value: valorSelecionado,
							text: selecionado,
							disabled: 'disabled',
							selected: 'selected',
						}));
					
					label.appendTo(div);
					select.appendTo(div);
					div.append("<hr style='background-color: #f2f2f2;border-color: #cce6ff;  ; margin: 11px;'/>")
					
					$('#' + tarefaId).append(div)

					break;
					
					}
				}
			}
		});
		});
	
	populaSelectsConsultores();
	populaSelectsListaValores();
	
}

function populaSelectsConsultores(){
	
	$.when(solicitadoresConsultores()).done(function(consultores){
	
		$(".consultores").each(function(i, obj){
		var select = $(this);
			for(var i = 0; i<consultores.length; i++){
				select.append($('<option>', {
				    value: consultores[i].id,
				    text: consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
	})
	});
	
}

function populaSelectsListaValores(){
	
	$.when(solicitadoresListaValores()).done(function(listaValores){
		
		$(".lista").each(function(i, obj){
		var select = $(this);
		
			for(var i = 0; i<listaValores.length; i++){

				if(select.attr('name') == listaValores[i].item.id)
				select.append($('<option>', {
				    value: listaValores[i].valor,
				    text: listaValores[i].valor
				}));
			}
	})
	});
}

function solicitadoresItens() {

	var subProdId = $("#idSubProd").val();
	console.log(subProdId)

	var url = "/producoes/itemValoresSubProd/"+ subProdId + ".json"

	return $.ajax({
 		type: 'GET',
 		url: url,
 		async: false,
 		success:function(data){}
});

}

function solicitadoresConsultores() {

	var url = "/producoes/consultores.json"

	 return $.ajax({
         		type: 'GET',
         		url: url,
         		async: false,
         		success:function(data){}
     });
	
}

function solicitadoresListaValores() {

	var url = "/producoes/listas.json"

	 return $.ajax({
         		type: 'GET',
         		url: url,
         		async: false,
         		success:function(data){}
     });
	
}

function inicializadores(){
	$('.select').material_select();
	$('.select').css("width","300px");
	$('.select').siblings('input').css("width","300px");
	$('.money').mask('000.000.000.000.000,00', {reverse: true});
}

function salvaItens(){
	
	console.log("salvar")
	
	
	var etapaFinalizada = false
	
	var valores = [];
	
	// PEGA VALOR DE TODOS OS DATEPICKERS
	$('.datepicker').each(function(i, obj){
		var datepicker = {
				id: $(this).attr('id'),
				valor: $(this).val()
		};
		valores.push(datepicker);
	});
	
	// PEGA VALOR DE TODOS OS CHECKBOXS
	$('.filled-in').each(function(i, obj){
		
		var nomeCampo = $(this).attr('name')
		var defineRealizacao = nomeCampo.substr(nomeCampo.lastIndexOf(".") + 1);
		
		if(defineRealizacao=="true" && $(this).is(":checked")){
			etapaFinalizada = true
		} 
		
		var check = {
				id: $(this).attr('id'),
				valor: $(this).is(":checked")	
		};
		valores.push(check);
	});
	
	// PEGA VALOR DE TODOS OS CAMPOS DE TEXTO
	$('.texto').each(function(i, obj){
		var text = {
				id: $(this).attr('id'),
				valor: $(this).val()
		};
		valores.push(text);
	});
	
	// PEGA VALOR DE TODOS AS PORCENTAGENS
	$('.percent').each(function(i, obj){
		var percent = {
				id: $(this).attr('id'),
				valor: $(this).val()
		};
		valores.push(percent);
	});
	
	// PEGA VALOR DE TODOS OS CAMPOS DE MONEY 
	$('.money').each(function(i, obj){
		
		var money = {
				id: $(this).attr('id'),
				valor: $(this).val()
		};
		valores.push(money);
	});
	
	// PEGA VALOR DE TODOS OS SELECTS DE CONSULTORES 
	$('.consultores').each(function(i, obj){
		var checker = $(this).attr('id');
		if(checker !== undefined){
		var consultor = {
				id: $(this).attr('id'),
				valor: $(this).find(":selected").text()
		};
		valores.push(consultor);
		}
	});
	
	// PEGA VALOR DE TODOS OS SELECTS DE LISTA 
	$('.lista').each(function(i, obj){
		var checker = $(this).attr('id');
		if(checker !== undefined){
		var lista = {
				id: $(this).attr('id'),
				valor: $(this).find(":selected").text()
		};
		valores.push(lista);
		}
	});
	
	var idEtapa = $("#idEtapa").val();
	var incluso = $("#inclusaoEficiencia").val()
	
	console.log("e finalizada? "+etapaFinalizada)
	console.log("ja foi incluso? "+incluso)
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/"+idEtapa+"/salvaItens",
		data: JSON.stringify(valores),
		dataType:"JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Itens salvos com sucesso!',
                        5000, 'rounded');
                if(etapaFinalizada==true && incluso=="false"){
                	 Materialize
                     .toast(
                         'Etapa de Trabalho finalizada.',
                         5000, 'rounded');
                         $('#modalEficiencia').modal({ dismissible: false});
                		 $('#modalEficiencia').modal('open');
                	
					
                }else{

                }
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }	
	});
	
	
}

function inicializarDatePickers() {
	$('.datepicker').pickadate({
		monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
	    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
	    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
	    weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
	    today: 'Hoje',
	    clear: false,
	    close: 'Pronto',
	    labelMonthNext: 'Próximo mês',
	    labelMonthPrev: 'Mês anterior',
	    labelMonthSelect: 'Selecione um mês',
	    labelYearSelect: 'Selecione um ano',
	    selectMonths: true, 
	    selectYears: 15,
		closeOnSelect : false,
		format: 'dd/mm/yyyy'
	});
}

var i = 0

$('.add').click(
		
		function() {
			
			var divPai = $('#' + $(this).parent().attr("id"))

			var divRow = $("<div class='row'>")
			var divAtividade = $("<div class='input-field col s3'>")
			var divConsultor = $("<div class='input-field col s4'>")
			var divHoras = $("<div class='input-field col s1'>")

			var selectAtividade = $("<select id='atividade"+i+"' class='select'>");
			var labelAtividade = $("<label for='atividade'>").text(
					"Tipo de Atividade")
			selectAtividade.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			selectAtividade.append($('<option>', {
				value : "Feito Por",
				text : "Feito Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Validado Por",
				text : "Validado Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Revisado Por",
				text : "Revisado Por",
			}));

			var selectConsultor = $("<select id='consultor"+i+"' class='select consultores'>");
			var labelConsultor = $("<label for='consultor'>").text(
					"Consultor")
			selectConsultor.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));

			var inputHoras = $("<input type='number' id='horas"+i+"' placeholder='Horas'>");

			selectAtividade.appendTo(divAtividade);
			selectConsultor.appendTo(divConsultor);
			inputHoras.appendTo(divHoras)
			labelAtividade.appendTo(divAtividade);
			labelConsultor.appendTo(divConsultor);
			divAtividade.appendTo(divRow);
			divConsultor.appendTo(divRow);
			divHoras.appendTo(divRow);
			divRow.appendTo(divPai)

			populaSelects(selectConsultor);

			selectAtividade.material_select();
			selectConsultor.material_select();

			i++;

		})


function handleChange(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 100) input.value = 100;
  }

function populaSelects(selectConsultor) {

	$.when(solicitadoresConsultores()).done(function(consultores) {

		selectConsultor.find('option').remove()

		selectConsultor.each(function(i, obj) {
			var select = $(this);
			select.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));

			for (var i = 0; i < consultores.length; i++) {
				select.append($('<option>', {
					value : consultores[i].id,
					text : consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
		})
	});

}

$('.salvar').click(function() {
	

	
	var idEtapa = $(this).parent().attr("id")
	var atividades = [];

	var divPai = $($(this).prev())
	var idDivPai = divPai.attr("id")
	
	var firstIteration = true;
	var j = 0
	
	$("#corpo > div").each(function() {
		
		var atividade = $('#atividade'+j).find(":selected").val();
		var consultorId = $('#consultor'+j).find(":selected").val();
		var horas = $('#horas'+j).val();
		
		var etapa = {
				id: $('#idEtapa').val()
		}
		
		var producao = {
				id: $('#idProd').val()
		}
		
		var consultor = {
				id: consultorId
		}
		
		var atividade = {
				atividade: atividade,
				consultor: consultor,
				horas: horas,
				etapa: etapa,
				producao: producao
		};
		
		atividades.push(atividade)
		j++
		
	})
	
	console.log(atividades)
	
	if(atividades.length == 0){
		 $('#modalEficiencia').modal('open');
	}else{
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/eficiencia/salva",
		async: false,
		data: JSON.stringify(atividades),
		dataType:"JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Atividades salvas com sucesso!',
                        5000, 'rounded');
                        $('#modalEficiencia').modal('close');
                        
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }	
	});
	
	}
})


