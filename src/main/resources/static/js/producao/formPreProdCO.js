$(function() {
	$("#form").submit(function () {
        $("#btn-add-preProducao").attr("disabled", true);
        return true;
    });
	$('.select').material_select();
})

function checkPreProducaoUnique(input){
	var ano = $("#campanha").val();
	var cliente = $("#clienteId").val();

	var urlCheck = "/producoes/" + cliente + "/" + ano + ".json";

	$.ajax({
		url : urlCheck,
		type : 'GET',
		dataType : 'json',
		success : function(d) {
			if (d == "0") {
				$("#btn-add-preProducao").attr("disabled", false);
			} else {
				Materialize.toast(
						'Ya existe una preproducción para la campaña insertada..',
						10000, 'rounded');
				$("#btn-add-preProducao").attr("disabled", true);
			}
		},
	});
}
