
$(function(){
	
	carregaAtividades()
	
	
})


function carregaAtividades(){
	console.log("entrou aqui")
	var producao = $('#prodId').val();
	
	$.when(solicitadorAtividades()).done(function (atividades) {
		 for (var i = 0; i < atividades.length; i++) {
			 var atividade = atividades[i];
			console.log("")
			var idEtapa = atividade.etapa.id
			 
			var divPai = $('#corpo' + idEtapa )
			var divRow = $("<div class='row'>")
			var divAtividade = $("<div class='input-field col s2'>")
			var divConsultor = $("<div class='input-field col s4'>")
			var divHoras = $("<div class='input-field col s2'>")
			var divBtn = $("<div>")
			var divTarefa = $("<div class='input-field col s2'>")
		
		
			
			var inputId = $("<input hidden='hidden' class='id' type='text' value="+atividade.id+" id='id"+ idEtapa +i+ "'>");
			
			// SELECT DE ATIVIDADE //
			var selectAtividade = $("<select id='atividade"+ idEtapa + i + "' class='select'>");
			var labelAtividade = $("<label for='atividade"+ idEtapa + i + "'>").text(
					"Tipo de Atividade")
			selectAtividade.append($('<option>', {
				value : atividade.atividade,
				text : atividade.atividade,
				disabled : 'disabled',
				selected : 'selected',
			}));
			selectAtividade.append($('<option>', {
				value : "Feito Por",
				text : "Feito Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Validado Por",
				text : "Validado Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Revisado Por",
				text : "Revisado Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Gestão Por",
				text : "Gestão Por",
			}));

			// SELECT DE CONSULTOR //
			var selectConsultor = $("<select id='consultor"+ idEtapa + i
					+ "' class='select consultores'>");
			var labelConsultor = $("<label for='consultor"+ idEtapa + i + "'>").text(
					"Consultor")
			selectConsultor.append($('<option>', {
				value : atividade.consultor.id,
				text : atividade.consultor.sigla+', '+atividade.consultor.nome,
				disabled : 'disabled',
				selected : 'selected',
				class: "dbValue",
			}));

			// SELECT DE HORAS //
			//formatando tempo
			var horas = atividade.horas;
			console.log("horas",horas);
			var tempoAux = 	horas.toString().split(".");
			var hora = tempoAux[0];
			if (hora <10){
			 hora = "0"+tempoAux[0];
			}
			var minutos = tempoAux[1];
			if(minutos == undefined){
				minutos = "00"
			}
			var minutosAux = minutos.split("");
			if(minutosAux[1] == undefined){
				minutosAux[1] = "0";
				minutos = minutosAux[0] + minutosAux[1]
			}
			console.log("minutos",minutos);
			//criando nova variavel, formato float
			horas  = hora+":"+minutos;
			console.log("horas",horas);
			var inputHoras = $("<input type='time' class='hora' value="+horas+" id='horas"+ idEtapa + i
					+ "' placeholder='Horas' step='3600'>");
					
			
			// SELECT DE TAREFAS //
			try{
			var selectTarefas = $("<select id='tarefa"+ idEtapa + i
					+ "' class='select tarefas'>");
			var labelTarefas = $("<label for='tarefa"+ idEtapa + i + "'>").text(
					"Tarefas")
			selectTarefas.append($('<option>', {
				value : atividade.tarefa.id,
				text : atividade.tarefa.nome,
				disabled : 'disabled',
				selected : 'selected',
				class: "dbValue",
			}));
			}catch(e){
				
			}
			
			// BOTÃO DELET
			
			var botao = $("<a class='btn-floating btn-small waves-effect waves-light red botaoDelete' id='"+atividade.id+"' ><i class='large material-icons'>delete_forever</i></a>")

			inputId.appendTo(divRow)
			selectTarefas.appendTo(divTarefa);
			labelTarefas.appendTo(divTarefa);
			divTarefa.appendTo(divRow);
			selectAtividade.appendTo(divAtividade);
			selectConsultor.appendTo(divConsultor);
			inputHoras.appendTo(divHoras);
			botao.appendTo(divBtn);
			labelAtividade.appendTo(divAtividade);
			labelConsultor.appendTo(divConsultor);
			divAtividade.appendTo(divRow);
			divConsultor.appendTo(divRow);
			divHoras.appendTo(divRow);
			divBtn.appendTo(divRow);
			divRow.appendTo(divPai);
			
			populaSelectsConsultoresNotDBValue(selectConsultor, atividade)

			selectAtividade.material_select();
			selectConsultor.material_select();
			selectTarefas.material_select();
		 }
	})
}

var i = 0

$('.remove').click(function() {

	var divPai = $('#' + $(this).parent().attr("id"))
	var idDivPai = divPai.attr("id")
	if($('#'+idDivPai).children().last().find(".id").val()!=undefined){
		 Materialize.toast('Utilize o icone de lixeira para remover uma atividade salva.', 5000, 'rounded');
	} else {
		$('#'+idDivPai).children().last().remove()
	}
		
})

$('.hora').change(function(){
	console.log('entrou na função para bloquear zero horas')
	var tempo = document.getElementsByClassName('hora').val();
	
	if(tempo <= 0){
		
	}
	
})

$(document).on("change",".hora",function() {
	 var hora = $(this);
//	 let x = document.getElementsByClassName('.hora'+idEtapa+i);
		console.log('hora: ' + hora)
		
		console.log('entrou no método de bloquear zero horas');
//		 let hora = document.querySelector('#hora'+idEtapa+i);
		tempo = hora.val();
		console.log('o valor é' + tempo)	
		
		
		console.log(tempo)
	})

function formataHoras(i) {
	
	if($("#hora"+i) <= 0) {
	$("#hora"+i).replace('',"0,1")
}
}

					

$('.add').click(
		
		function() {
			
			var divPai = $('#' + $(this).parent().attr("id"))
			var idDivPai = divPai.attr("id")
			var lastDiv = $('#'+idDivPai).children().last()
			i = lastDiv.children().attr("id");
			if(i!=null){
				i = i.slice(-1)
				i++;
			} else {
				i = 0
			}

			var idEtapa = $(this).parent().attr("id").replace(/\D/g,'');
			var divRow = $("<div class='row'>")
			var divAtividade = $("<div class='input-field col s2'>")
			var divConsultor = $("<div class='input-field col s4'>")
			var divHoras = $("<div class='input-field col s2'>")
			var divTarefa = $("<div class='input-field col s2'>");
			var inputId = $("<input hidden='hidden' type='text' value='null' id='id"+ idEtapa +i+ "'>");
			var selectAtividade = $("<select id='atividade"+ idEtapa + i + "' class='select'>");
			var labelAtividade = $("<label for='atividade"+ idEtapa + i + "'>").text(
					"Tipo de Atividade")
			selectAtividade.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			selectAtividade.append($('<option>', {
				value : "Feito Por",
				text : "Feito Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Validado Por",
				text : "Validado Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Revisado Por",
				text : "Revisado Por",
			}));
			selectAtividade.append($('<option>', {
				value : "Gestão Por",
				text : "Gestão Por",
			}));


			var selectConsultor = $("<select id='consultor"+ idEtapa + i
					+ "' class='select consultores'>");
			var labelConsultor = $("<label for='consultor"+ idEtapa + i + "'>").text(
					"Consultor")
			selectConsultor.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
			// SELECT DE TAREFAS //
			var selectTarefas = $("<select id='tarefa"+ idEtapa + i
					+ "' class='tarefas'>");
			var labelTarefas = $("<label for='tarefa"+ idEtapa + i + "'>").text(
					"Tarefas")
			selectTarefas.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));
			
				var inputHoras = $("<input type='time' required class='hora' id='horas"+ idEtapa + i
					+ "'  value='00:00' step='3600''>");
			
			inputId.appendTo(divRow)
			selectTarefas.appendTo(divTarefa);
			labelTarefas.appendTo(divTarefa);
			divTarefa.appendTo(divRow);
			selectAtividade.appendTo(divAtividade);
			selectConsultor.appendTo(divConsultor);
			inputHoras.appendTo(divHoras);
			labelAtividade.appendTo(divAtividade);
			labelConsultor.appendTo(divConsultor);
			divAtividade.appendTo(divRow);
			divConsultor.appendTo(divRow);
			divHoras.appendTo(divRow);
			divRow.appendTo(divPai)

			populaSelectsConsultores(selectConsultor);
			populaSelectsTarefas(selectTarefas,idEtapa);
			
			selectAtividade.material_select();
			selectConsultor.material_select();
			selectTarefas.material_select();
		
			i++;

		})

function solicitadorAtividades() {

    var id = $("#prodId").val();

    var url = "/producoes/getEficiencia/" + id +  ".json"

    return $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data) {}
    });
}

$('.salvar').click(function() {
	var aux = 0;
	
	var idEtapa = $(this).attr("id")
	var atividades = [];

	var divPai = $($(this).prev())
	var idDivPai = divPai.attr("id")
	
	var lastDiv = $('#'+idDivPai).children().last()
	var i = lastDiv.children().attr("id");
	
	if(i!=null){
		i = i.slice(-1)
	}else {
		i = 0
	}

	var tamanhoEtapa = $("#corpo"+idEtapa+" > div").length
	
	var firstIteration = true;
	var j = 0
	
	
	$("#corpo"+idEtapa+" > div").each(function() {
		
		
		if(firstIteration==true){
		while(j!=i){
			j++;
		}
		firstIteration=false
		j = j-(tamanhoEtapa-1)
		}
		
		var atividade = $('#atividade'+idEtapa+j).find(":selected").val();
		var consultorId = $('#consultor'+idEtapa+j).find(":selected").val();
		var horas = $('#horas'+idEtapa+j).val();
		var tarefa = $('#tarefa'+idEtapa+j).find(":selected").val();
		
		//input zero horas
		if(horas =='00:00'){
			$('#horas'+idEtapa+j).animate({width: "200px" }, 800)
			aux++;
		}
		//formatando tempo
		var tempoAux = horas.split(":")
		var hora = tempoAux[0];
		var minutos = tempoAux[1];
		//criando nova variavel, formato float
		horas  = hora+"."+minutos;

		//tratando tarefa vazia
		if(tarefa ==  undefined){
			tarefa = 0;
		}
	
		var etapa = {
				id: idEtapa
		}
		
		var producao = {
				id: $('#prodId').val()
		}
		
		var consultor = {
				id: consultorId
		}
		
		var id = null;
		if($('#id'+idEtapa+j).val()!="null"){
			
			id = $('#id'+idEtapa+j).val();
			
		}
		 var tarefa = {
			id :  tarefa
		} 
		var atividade = {
				id: id,
				atividade: atividade,
				consultor: consultor,
				horas: horas,
				etapa: etapa,
				producao: producao,
				tarefa: tarefa,
				
		};
		console.log("atividade",atividade)
		atividades.push(atividade)
		j++
		
	
	})
	//input zero horas,não salva
	if(aux != 0){
		Materialize.toast("Preencher campo horas", 5000, 'red');
	}else{
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/eficiencia/salva",
		data: JSON.stringify(atividades),
		dataType:"JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Atividades salvas com sucesso!',
                        5000, 'rounded');
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }	
	});
	}
})

function populaSelectsConsultores(selectConsultor) {
	$.when(solicitadoresConsultores()).done(function(consultores) {

		selectConsultor.find('option').remove()

		selectConsultor.each(function(i, obj) {
			var select = $(this);
			select.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));

			for (var i = 0; i < consultores.length; i++) {
				select.append($('<option>', {
					value : consultores[i].id,
					text : consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
		})
	});

}

function populaSelectsConsultoresNotDBValue(selectConsultor, atividade) {
	$.when(solicitadoresConsultores()).done(function(consultores) {

		selectConsultor.find('option').remove()


		selectConsultor.each(function(i, obj) {
			var select = $(this);
			select.append($('<option>', {
				value : atividade.consultor.id,
				text : atividade.consultor.nome,
				disabled : 'disabled',
				selected : 'selected',
			}));

			for (var i = 0; i < consultores.length; i++) {
				select.append($('<option>', {
					value : consultores[i].id,
					text : consultores[i].sigla + ", " + consultores[i].nome
				}));
			}
		})
	});

}

function solicitadoresConsultores() {

	var url = "/producoes/consultores.json"

	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
		}
	});

}

$(document).on('click', "a.botaoDelete",function () {

	var idAtividade = $(this).attr('id')
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/producoes/eficiencia/deleta/"+idAtividade,
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Atividade removida com sucesso!',
                        5000, 'rounded');
                console.log($('#'+idAtividade).parent().parent().remove())
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }	
	});
})

//function para preencher o select tarefas
function populaSelectsTarefas(selectTarefas,idEtapa) {
	$.when(solicitadoresTarefas(idEtapa)).done(function(tarefa) {

		selectTarefas.find('option').remove()
		selectTarefas.each(function(i, obj) {
			var select = $(this);
			select.append($('<option>', {
				value : "",
				text : "Selecione",
				disabled : 'disabled',
				selected : 'selected',
			}));

			for (var i = 0; i < tarefa.length; i++) {
				select.append($('<option>', {
					value :tarefa[i].id,
					text : tarefa[i].nome
				}));
			}
		})
	});
}
//function para pegar as tarefas(todas)
function solicitadoresTarefas(idEtapa){
	var url = "/producoes/"+idEtapa+"/tarefas.json"

	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
		}
	});
}

$(document).on("change",".tarefas",function() {
	  var tarefaId = $(this).val();
	  $("#tarefaId").val(tarefaId);
})









