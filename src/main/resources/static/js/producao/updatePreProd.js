$(function() {
	$("#form").submit(function() {
		$("#btn-add-preProducao").attr("disabled", true);
		 let opcionAMandar = $("#select-estadoCliente").find("option:selected").attr("value");
		console.log("opcion en el submit -> " + opcionAMandar);
		if (opcionAMandar == "Inactivo") $('#items_previsiones_Or_activo').empty();
		if(!( opcionAMandar == "Inactivo"))  $('#items_inactivo').empty();
		return true;
	});
	
	$('.select').material_select();
	
	$('#items_previsiones_Or_activo').attr('hidden', 'true');
	$('#items_inactivo').attr('hidden', 'true');
	let opcionEstadoCliente = $("#select-estadoCliente").find("option:selected").attr("value");
	opcionesMotivoSituacion(opcionEstadoCliente);
	 
	
})

function checkPreProducaoUnique(input) {
	var ano = $("#campanha").val();
	var cliente = $("#clienteId").val();

	var urlCheck = "/producoes/" + cliente + "/" + ano + ".json";

	$.ajax({
		url: urlCheck,
		type: 'GET',
		dataType: 'json',
		success: function(d) {
			if (d == "0") {
				$("#btn-add-preProducao").attr("disabled", false);
			} else {
				Materialize.toast(
					'Ya existe una preproducción para la campaña insertada..',
					10000, 'rounded');
				$("#btn-add-preProducao").attr("disabled", true);
			}
		},
	});
}


// Dina'mica al hacer clic en el input de estado de cliente con sus options
$("#select-estadoCliente").change(function() {
	var optionValue = $(this).find("option:selected").attr("value");
	opcionesMotivoSituacion(optionValue);
});
function opcionesMotivoSituacion(optionValue){
	switch (optionValue) {
		case "Inactivo":
			$('#items_previsiones_Or_activo').attr('hidden', 'true');
			$('#items_inactivo').removeAttr('hidden');
			console.log(optionValue);
			break;
		case "Previsión Inactivo":
			$('#items_inactivo').attr('hidden', 'true');
			$('#items_previsiones_Or_activo').removeAttr('hidden');
			console.log(optionValue);
			break;
		case "Previsión Activo":
			$('#items_inactivo').attr('hidden', 'true');
			$('#items_previsiones_Or_activo').removeAttr('hidden');
			console.log(optionValue);
			break;
		case "Activo":
			$('#items_inactivo').attr('hidden', 'true');
			$('#items_previsiones_Or_activo').removeAttr('hidden');
			console.log(optionValue);
			break;
		default:
			console.log('default del input select-estadoCliente -> ', 'selected');
	}
}


