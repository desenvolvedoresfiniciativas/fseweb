$(function(){
	 $('select').material_select();
	 var toast = $('#toast').val()
		if(toast != null ){
		Materialize.toast(toast, 5000, 'rounded');
		}
});

$("#pesquisar-pontoSituacao").click(listarConsultoresModal)

function listarConsultoresModal() {

	var idProduto = $("#idProd").val();
	
    var url = '/producoes/' + idProduto + '/etapas.json';

    $.get(url, function (data) {

    	var valores = [];
    	
    	var id = $("#idProd").val();
    	
        $(data).each(function () {

        	var etapa = this;
        	var etapaNomeStr = etapa.nomeStr;
        	var etapaNome = etapa.nome;
        	
        console.log(etapaNome)
        	
        var etapaVal = $("#"+etapaNomeStr).find(":selected").val();
        console.log(etapaVal);
        
        var pontoSituacao = {
        	etapa: etapaNome,
        	valor: etapaVal,
        	produto: id
        };
        
        valores.push(pontoSituacao)
        
        });
        
        $.ajax({
    		type: "POST",
    		contentType: "application/json",
    		url: "/producoes/searchPontoSituacao",
    		data: JSON.stringify(valores),
    		dataType:"JSON",
            statusCode: {
                200: function (){
                	window.location.replace("/producoes/pontoSituacao/resultList");
                },
                500: function () {
                    Materialize.toast(
                        'Ops, houve um erro interno',
                        5000, 'rounded');
                },
                400: function () {
                    Materialize
                        .toast(
                            'Você deve estar fazendo algo de errado',
                            5000, 'rounded');
                },
                404: function () {
                    Materialize.toast('Url não encontrada',
                        5000, 'rounded');
                }
            }	
    	});

    });
};
