//var anoDigitado = $('#ano').val();
//var ano2 = '2019';
//console.log(anoDigitado);

//$('#ano').change(function() {
//if(anoDigitado == ano2){
//$('#ano').val(' ');
//Materialize.toast('O ano fiscal já é existente.',10000, 'rounded');
//}
//});

$(function() {
	$("#btn-add-producao").prop("disabled", true);

	$("#situacao").hide();
	$("#razaoInatividade").hide();
	
	$("#form").submit(function () {
        $("#btn-add-producao").attr("disabled", true);
        return true;
    });
    $('#equipe').attr("disabled", 'disabled');
    $('#equipe').material_select();
	
})

var timer;
$("#ano").on("blur", function() {
	if ($("#ano").val() == "") {
		console.log("ano",$("#ano").val())
		console.log("Trigger false")
		$("#btn-add-producao").prop("disabled", true);
	} else {
		console.log("entrou no else")
		var searchid = $(this).val().trim();
		clearInterval(timer);
		timer = setTimeout(function() {
			var disabled = checkVigencia($("#ano").val());
			checkForProducaoExistente(disabled);
		}, 500);
	}
});

function checkForProducaoExistente(disabled) {
	console.log('entrou no ajax')
	
	var ano = $("#ano").val();
	var cliente = $("#clienteId").val();
	var linha = $('input[name=contratoId]:checked')
	var produto = linha.parent().parent().parent().find('#produtoId').text();

	var urlCheck = "/producoes/" + cliente + "/" + produto + "/" + ano
			+ "/prod.json"

	$.ajax({
		url : urlCheck,
		type : 'GET',
		dataType : 'json',
		success : function(d) {
			if (d == "0") {
				if (disabled == false) {
					$("#btn-add-producao").attr("disabled", false);
				}
			} else {
				Materialize.toast(
						'Produção duplicada para o ano e produto informados.',
						10000, 'rounded');
				$("#btn-add-producao").attr("disabled", true);
			}
		},
	});

	// if( data !== "null" && data !== "undefined" ){
	// Materialize.toast('Produção existente encontrada para o ano/produto.',
	// 10000, 'rounded');
	// $("#btn-add-producao").attr("disabled", true);
	// } else {
	// Materialize.toast('OK', 10000, 'rounded');
	// $("#btn-add-producao").attr("disabled", false);
	// }

}

function checkVigencia(ano) {

	var selecionado = $('input[name=contratoId]:checked', '#form').parent()
			.parent().parent();

	if (selecionado != null) {
		if ($("#fim").val() == null) {
		} else {
			
			var iniVig = selecionado.children().eq(3).text().substr(6, 9);
			var fimVig = selecionado.children().eq(4).text().substr(6, 9);
			
			if (iniVig != "") {
					
				if (fimVig == "" || fimVig == 'RMINADA') {
					fimVig = 3000
					
				}
				if (ano <= fimVig && ano >= iniVig) {
					Materialize.toast(
							'Ano inserido dentro da vigência do contrato!',
							10000, 'rounded');
					
					$('#select-situacao').removeAttr('disabled');
					$('#select-situacao').material_select();
					
					$("#btn-add-producao").attr("disabled", false);
					
					return false;
				} else {

					Materialize.toast(
							'Ano inserido fora da vigência do contrato!',
							10000, 'rounded');
					Materialize
							.toast(
									'Travando a situação em "Negociação", especifique o motivo...',
									10000, 'rounded');
					$("#btn-add-producao").attr("disabled", false);

					$('#select-situacao option:eq(5)').prop('selected', true);
					$('#select-situacao').material_select();
					
					$('#select-situacao').prop("disabled", true)
					$('#select-situacao').material_select();	
					
					return true;
				}

				$('.tabs').tabs();
				$("#informacoes").show();

			}
		}
	}

}



$('.contratoId').click(
	function(){
		
		var contratoId = $(this).val()
		console.log('O VALOR DO CHECKBOX É: '+contratoId)
		
		$.ajax({
			type: 'GET',
			url: "/contratos/"+contratoId+"/VerificaPrincipal.json",
			success : function(data) {
				if(data == false){
					Materialize.toast(
							'O produto não é um produto Alvo!',
							5000, 'rounded');
				
				}
				
			}
		});
		
		
	}
);




$('#filial').change(function(){
				var filialId = $("#filial").val()
				console.log(filialId)
				var options = "<option selected='selected' value=''>Selecione</option>";
				var url = "/equipes/" + filialId + "/getEquipesByFilial.json"
				
				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {
						console.log(data)
						console.log("O TAMANHO É " + data.length)
						$(data).each(
								function(i) {
									var equipe = data[i];
									options += "<option value='" + equipe.id
											+ "'>" + equipe.nome + "</option>"
								});
						
						
											
						$("#equipe").empty();
						$("#equipe").append(options);
						$('#equipe').removeAttr('disabled');
						$("#equipe").material_select();			
						

						

		
					}
				});
	}
	
)


