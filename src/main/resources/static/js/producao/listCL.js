$(function(){
	$(".select").material_select();
	$('.modal').modal();
	$('.modalD').modal();
	filtroTabelaProducao();
	filtroTabelaCliente();
	filtroTabelaCodigo()
	sortTable();
})

function filtroTabelaProducao() {
	var nomeFiltro = $("#combo-box :selected").val()
	console.log(nomeFiltro);
	$('#tabela-producao').find('tbody tr').each(
			function() {
				var conteudoCelula = $(this)
						.find('td:nth-child(7)').text();
				console.log(conteudoCelula);
				var corresponde = conteudoCelula
						.indexOf(nomeFiltro) >= 0;
				$(this).css('display', corresponde ? '' : 'none');
			});
	$("#combo-box").change(
			function() {
				var nomeFiltro = $("#combo-box :selected").val()
				console.log(nomeFiltro);
				
				if(nomeFiltro=="all"){
					$('#tabela-producao tr').show();
				} else {
				
				$('#tabela-producao').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(7)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
				}
			});
}

function filtroTabelaCodigo() {
	var $rows = $('#tabela-producao tbody tr');
	$('#txt-pesq-codigo').keyup(function() {
	    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

	    $rows.show().filter(function() {
	        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
	        return !~text.indexOf(val);
	    }).hide();
	});
}

function listarAnosModal(idContrato) {
    zerarLista();
    var lista = $('#anoProducao');
  
    var url = '/producoes/' + idContrato + '/producoes.json';
    var link = '/producoes/form/' + idContrato;

    $.get(url, function (data) {

        $(data).each(function () {

            var producao = this.ano;
            var idContrato = this.id;
            
            var item = novoItemLista(producao, idContrato);
            lista.append(item);
            
        });

    });

    $('#modal1').modal('open');
    appendModal(link);
};

function appendModal(link){
	
	var id1 = 'addProd';
	var id2 = 'closeMod';
	var footer = $('#botoesFooter');
	var classe1 = "'waves-effect waves-green btn-flat green white-text mouse-hand'"
	var classe2 = "'modal-action modal-close waves-effect waves-green btn-flat red white-text mouse-hand'"
	
	limpaBotoes();
	
    footer.append("<a id="+id1+" href="+link+" class="+classe1+">Novo</a>");
    footer.append(" ")
    footer.append("<a id="+id2+" class="+classe2+">Fechar</a>");
    
	}

function limpaBotoes(){
	$("#addProd").remove();
	$("#closeMod").remove();
}

function novoItemLista(item,idContrato) {
    var itemLista = $('<li>');
    var itemLista2 = $('<a>')
    var link = '/producoes/update/'+idContrato;

    itemLista.addClass('collection-item');
    
    var lista = itemLista.append("<a href="+link+">"+item+"</a>");

    return lista;
}

function zerarLista(){
	$('#anoProducao li').each(function(){
		$(this).remove();
	})
}

function sortTable() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("tabela-preproducao");
  switching = true;
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[0];
      y = rows[i + 1].getElementsByTagName("TD")[0];
      // Check if the two rows should switch place:
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        // If so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}