$(function(){
	$('.modal').modal();
	$('#modal1').modal('open');
	$("#temporario").hide();
	$("#anteriorC").hide();
})

$("input[name='tipoContrato']").click(function() {
    if ($(this).val() === 'anterior') {
    	$("#temporario").hide();
    	$("#anteriorC").show();
    } else if ($(this).val() === 'temp') {
    	$("#anteriorC").hide();
    	$("#temporario").show();
    } 
  });

$("#cnpj").on("input", formatarCnpj);
$("#cnpj").on("input", validarCnpj);
$("#cnpj").on("focusout", validarCnpj);
$("#cnpjFaturar").on("input", formatarCnpj);
$("#selectProduto").material_select();

$('#btn-add-contrato').click(
		function() {
			if ($.trim($("#selectProduto").find(":selected").val()) === "") {
				Materialize.toast('O contrato necessita de um produto.', 6000,
						'rounded');
			}
			if ($.trim($("#cnpj").val()) === "") {
				Materialize.toast('O contrato necessita de um cliente.', 6000,
						'rounded');
			}
			if ($
					.trim($("#selectComercialResponsavel").find(":selected")
							.val()) === "") {
				Materialize.toast(
						'O contrato necessita de um comercial responsável.',
						6000, 'rounded');
			}
			if (!$("#vigenciaDeterminada").is(':checked')
					&& !$("#vigenciaIndeterminada").is(':checked')) {
				Materialize.toast(
						'O contrato necessita de um tipo de vigência.', 6000,
						'rounded');
			}
			if (!$("#valorFixo:checked").is(':checked')
					&& !$("#percentualFixo:checked").is(':checked')
					&& !$("#percentualEscalonado:checked").is(':checked')) {

			}

			validateFat();

		});

$(function() {
	$("select").material_select();
	$("#selectProduto").material_select();
	$("#selectGarantias").material_select();
	$("#selectComercialResponsavel").material_select();
	$("#cnpj").tooltipster({
		trigger : "custom"
	});
	$("#cnpjFaturar").tooltipster({
		trigger : "custom"
	});
	formatarCnpj();

});

function validateHon() {
	var status = false;
	if (!$("#valorFixo:checked").is(':checked')
			&& !$("#percentualFixo:checked").is(':checked')
			&& !$("#percentualEscalonado:checked").is(':checked')) {
		Materialize.toast('O contrato necessita de um honorário.', 6000,
				'rounded');
		return status;
	} else {
		status = true;
		console.log("true");
		return status;
	}
}

var timer;
$("#ano").on("keyup", function() {
	if ($("#ano").val() == "") {
		console.log("Trigger false")
	} else {
		var searchid = $(this).val().trim();
		clearInterval(timer);
		timer = setTimeout(function() {
			checkVigencia($("#ano").val());
			checkForProducaoExistenteTemporario();
		}, 1000);
	}
});

$("#selectProduto").change(function() {
		var searchid = $(this).val().trim();
		clearInterval(timer);
		timer = setTimeout(function() {
			checkVigencia($("#ano").val());
			checkForProducaoExistenteTemporario();
		}, 600);
});

var status = 0

function checkForProducaoExistenteTemporario() {
	
	status = 0
	
	if(status == 0){
		$("#btn-add-producao").attr("disabled", false);
	}	
	
	var ano = $("#ano").val();
	var produto = $("#selectProduto :selected").val();
	var cliente = $("#clienteId").val();
	
	console.log("checking")
	
	var urlCheck = "/producoes/"+cliente+"/"+produto+"/"+ano+"/prod.json"

	$.get(urlCheck, function(data) {

		$(data).each(function() {
			
			console.log(data.id)

			if( data !== "null" || data !== "undefined" ){
				Materialize.toast('Produção existente encontrada para o ano/produto.', 10000, 'rounded');
				$("#btn-add-producao").attr("disabled", true);
				status = 1
			} else {
				console.log("OK")
				Materialize.toast('OK', 10000, 'rounded');
				$("#btn-add-producao").attr("disabled", false);
				status = 0
			}
			
		});
	});

}