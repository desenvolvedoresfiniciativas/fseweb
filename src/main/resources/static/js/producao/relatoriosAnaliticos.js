$(function() {

	$('.collapsible').collapsible('open', 0);
	$('select').material_select();
	$('#tabela-analiticos').DataTable({
//	filtroAno();
//	filtroSetor();
//	filtroEspecialidade();
//	filtroEquipe();
//	filtroConsultor();
//	filtroFeitoPor();
//	filtroCorrigidoPor();
//	filtroEstado();
		 language: {
				    "sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_ resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": "Próximo",
				        "sPrevious": "Anterior",
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
				    },
				    "select": {
				        "rows": {
				            "_": "Selecionado %d linhas",
				            "0": "Nenhuma linha selecionada",
				            "1": "Selecionado 1 linha"
				        }
				    }
         },
	    	bFilter: false,

	    	iDisplayLength: 1000,

	        columnDefs: [
	            {
	                targets: [ 0, 1, 2 ],
	                className: 'mdl-data-table__cell--non-numeric'
	            }
	        ]
	    });
})

function filtroAno(){
	$("#ano").change(
			function() {
				var nomeFiltro = $("#ano :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(2)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function filtroSetor(){
	$("#setor").change(
			function() {
				var nomeFiltro = $("#setor :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(3)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function filtroEspecialidade(){
	$("#especialidade").change(
			function() {
				var nomeFiltro = $("#especialidade :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(4)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function filtroEquipe(){
	$("#equipe").change(
			function() {
				var nomeFiltro = $("#equipe :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(5)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function filtroConsultor(){
	$("#consultor").change(
			function() {
				var nomeFiltro = $("#consultor :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(6)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function filtroFeitoPor(){
	$("#feitoPor").change(
			function() {
				var nomeFiltro = $("#feitoPor :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(9)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function filtroCorrigidoPor(){
	$("#corrigidoPor").change(
			function() {
				var nomeFiltro = $("#corrigidoPor :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(11)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function filtroEstado(){
	$("#estado").change(
			function() {
				var nomeFiltro = $("#estado :selected").val()
				console.log(nomeFiltro);
				$('#tabela').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(12)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}

function fnExcelReport() {
    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Relatório Analítico</x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + $('#tabela-analiticos').html();
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, 'analitico.xls');
        }
    } else {
        $('#excelBtn').attr('href', data_type + ', ' + escape(tab_text));
        $('#excelBtn').attr('download', 'analitico.xls');
    }
}