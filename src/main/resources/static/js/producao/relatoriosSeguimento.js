$(function() {

	$('.collapsible').collapsible('open', 0);
	$('select').material_select();
	$(formataCnpjTabela())
	$('#tabela').DataTable({
		language : {
			"sEmptyTable" : "Nenhum registro encontrado",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ".",
			"sLengthMenu" : "_MENU_ resultados por página",
			"sLoadingRecords" : "Carregando...",
			"sProcessing" : "Processando...",
			"sZeroRecords" : "Nenhum registro encontrado",
			"sSearch" : "Pesquisar",
			"oPaginate" : {
				"sNext" : "Próximo",
				"sPrevious" : "Anterior",
				"sFirst" : "Primeiro",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			},
			"select" : {
				"rows" : {
					"_" : "Selecionado %d linhas",
					"0" : "Nenhuma linha selecionada",
					"1" : "Selecionado 1 linha"
				}
			}
		},
		bFilter : false,
		iDisplayLength : 5000,
		pageLength: 5000,
		columnDefs : [ {
			targets : [ 0, 1, 2 ],
			className : 'mdl-data-table__cell--non-numeric'
		} ]
	});
})

function fnExcelReport() {
	var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
	tab_text = tab_text
			+ '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

	tab_text = tab_text + '<x:Name>Relatório Seguimentos</x:Name>';

	tab_text = tab_text
			+ '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
	tab_text = tab_text
			+ '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

	tab_text = tab_text + "<table border='1px'>";
	tab_text = tab_text + $('#tabela').html();
	tab_text = tab_text + '</table></body></html>';

	var data_type = 'data:application/vnd.ms-excel';

	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
		if (window.navigator.msSaveBlob) {
			var blob = new Blob([ tab_text ], {
				type : "application/csv;charset=utf-8;"
			});
			navigator.msSaveBlob(blob, 'seguimentos.xls');
		}
	} else {
		$('#excelBtn').attr('href',
				data_type + ', ' + escape(tab_text));
		$('#excelBtn').attr('download', 'seguimentos.xls');
	}
}

function formataCnpjTabela() {
		$('.cnpj').each(function() {
			var cellText = $(this).html();
			cellText = cellText.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");
			$($(this)).html(cellText);
	})
}
