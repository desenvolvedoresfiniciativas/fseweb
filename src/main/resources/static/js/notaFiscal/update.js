var index = 1;

$('#addParcela').click(addLinhaParcela);
$('#removeParcela').click(apagaLinhaParcela);
$('#btn-cad-notaFiscal').click(salvaNF);
//$('#valorCobrarI').append("500");
$('.modal').modal();

$(function() {
	$(".select").material_select()
})

$("#cnpj").blur(buscaEmpresa);

$(function carregaParcelas(){
	
	var idNota = $("#idNota").val();
	console.log("id da nota que trará as parcelas é: " + idNota);
	$.getJSON("/notasFiscais/"  + idNota + "/parcelas.json", function(dados) {
		
		if (dados.erro != true) {
			for(var i = 1; i<=dados.length; i++){
			var parcela = dados[i-1];
			
			var parcelaId = $("<input type='text' hidden='hidden' value='"+parcela.id+"' id='id"+i+"' name='parcela'/>");

			var divRow = $("<div>").addClass("row");

			var tamanho = Number($('.valorParcela').length) + Number(1);

			var numeroParcela = $("<div class='col m1'>" +
				"<label for='parcela"+i+"'>N da Parcela</label> " +
				"<input type='text' value='"+parcela.nParcela+"' id='parcela"+i+"' name='parcela'/>  </div>");

			var valorParcela = $("<div class='col m2'>" +
				"<label for='valorParcela"+i+"'>Valor da Parcela</label> " +
				"<input type='text' value='"+mascaraValor(parcela.valorParcela.toFixed(2))+"' id='valorParcela"+i+"' class='valorParcela money' name='valorParcela'/></div>");

			var divDataCobranca = $("<div class='col m2'>" +
				"<label for='dataCobrancaParcela"+i+"'>Data de Cobrança</label> " +
				"<input type='text' value='"+parcela.dataCobranca+"' id='dataCobrancaParcela"+i+"' th:placeholder=" + 'DD/MM' +
				" name='dataCobrancaParcela' class='datepicker' />  </div>");

			var divDataVencimento = $("<div class='col m2'>" +
				"<label for='dataVencimentoParcela"+i+"'>Data de Vencimento</label> " +
				"<input type='text' value='"+parcela.dataVencimento+"' id='dataVencimentoParcela"+i+"' th:placeholder=" + 'DD/MM' +
				" name='dataVencimentoParcela' class='datepicker' />  </div>");

			var divComCheckbox = $("<p>" +
				" </p>");

			var checkbox = $(" <input name='parcelaPaga' type='checkbox' class='filled-in' id='parcelaPaga"+i+"'/>")
			checkbox.prop('checked', parcela.pago)
				
			var labelCheckbox = $("<label for='parcelaPaga"+i+"'>Pago</label>'")

			$.prop('checked', false);
			divComCheckbox.append(checkbox);
			divComCheckbox.append(labelCheckbox)

			divRow.append(parcelaId)
			divRow.append(numeroParcela);
			divRow.append(valorParcela);
			divRow.append(divDataCobranca);
			divRow.append(divDataVencimento);
			divRow.append(divComCheckbox);

			$("#parcela").append(divRow);
			}
			inicializarDatePickers();
			$('.money').mask('000.000.000.000.000,00', {reverse: true});
		} else {
			Materialize.toast('Parcelas não encontradas', 5000, 'rounded');
		}
	})
			
})

function buscaEmpresa() {

	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);

	if (!$(this).val().length == 0) {

		$.getJSON("/clientes/" + cnpj + ".json", function(dados) {

			if (dados.erro != true) {

				$("#nomeEmpresa").val(dados.razaoSocial);
				$("#nomeEmpresa").focusin();
				$("#idcliente").val(dados.id);
				$("#idcliente").focusin();
				$('#cnpjFaturar').val(cnpj);
				$('#cnpjFaturar').focusin();
				$('#razaoSocialFaturar').val(dados.razaoSocial)
				$('#razaoSocialFaturar').focusin();

			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})

	}

	function limparNomeEmpresa() {
		$("#nomeEmpresa").val("");
		$('#cnpjFaturar').val("");
		$('#razaoSocialFaturar').val("");
	}
}

function maskValor() {
	$(".valor").each(function() {
		var texto = $(this).text();
		texto = parseFloat(texto).toLocaleString('pt-BR')
		texto = "R$ " + texto
		$(this).text(texto);
		console.log(texto);
	})
}

$(function() {
	selectTipoNotaFiscal()
	var nomeFiltro = $("#select-tipoNotaFiscal :selected").val()
	console.log(nomeFiltro);
	if (nomeFiltro === 'SUBSTITUIDA') {
		$("#numeroNotaSubstituida").show();
	} else {
		$("#numeroNotaSubstituida").hide();
	}
})

function selectTipoNotaFiscal() {
	$("#select-tipoNotaFiscal").change(
		function() {
			var nomeFiltro = $("#select-tipoNotaFiscal :selected").val()
			console.log(nomeFiltro);
			if ($(this).val() === 'SUBSTITUIDA') {
				$("#numeroNotaSubstituida").show();
			} else {
				$("#numeroNotaSubstituida").hide();
			}
		});
}

function addLinhaParcela() {
	var lista = $("#parcela");

	lista.append(escreveLinhas(valorCobrar));
	index = $('.valorParcela').length;
	console.log("Quantidade de campos " + index)
	preencheValor(index);

}

function apagaLinhaParcela() {

	if (!$('#parcelaPaga' + index).is(':checked')) {
		$('#parcela').children().last().remove()
	} else {
		Materialize.toast('Você não pode remover parcelas já pagas.', 5000, 'rounded');
	}

	index = $('.valorParcela').length
	console.log("Quantidade de campos " + index)

	preencheValor(index)
}

function escreveLinhas() {

	var divRow = $("<div>").addClass("row");

	var tamanho = Number($('.valorParcela').length) + Number(1);

	console.log("Tamanho dentro da escreve linhas: " + tamanho)

	var divInputNParcela = $("<div>").addClass("col").addClass("m1").addClass("input-field");
	var inputNParcelas = $("<input>").attr("type", "text").attr("id", "parcela" + tamanho).val(tamanho).focusout();
	var label = $("<label>").attr("for", "parcela" + tamanho).text("N° Parcela").addClass('active');

	divInputNParcela.append(inputNParcelas).append(label)
	
	var parcelaId = $("<input type='text' hidden='hidden' id='id"+tamanho+"' name='parcela'/>");

	var divDataCobranca = $("<div class='col m2'>" +
		"<label for='dataCobrancaParcela" + tamanho + "'>Data de Cobrança</label> " +
		"<input type='text' class='datepicker' id='dataCobrancaParcela" + tamanho + "' th:placeholder=" + 'DD/MM' +
		" name='dataCobrancaParcela' class='datepicker' />  </div>");

	var divDataVencimento = $("<div class='col m2'>" +
		"<label for='dataVencimentoParcela" + tamanho + "'>Data de Vencimento</label> " +
		"<input type='text' class='datepicker' id='dataVencimentoParcela" + tamanho + "' th:placeholder=" + 'DD/MM' +
		" name='dataVencimentoParcela' class='datepicker' />  </div>");

	var divComCheckbox = $("<p>" +
		" </p>");

	var checkbox = $(" <input name='parcelaPaga' type='checkbox' class='filled-in' id='parcelaPaga" + tamanho + "' />" +
		" <label for='parcelaPaga" + tamanho + "'>Pago</label> ")

	$.prop('checked', false)
	divComCheckbox.append(checkbox)

	var divInputValor = $("<div>").addClass("col").addClass("m2").addClass("input-field");
	var inputValor = $("<input>").attr("type", "text").attr("id", "valorParcela" + tamanho + "").attr("class", "valorParcela");
	var labelValor = $("<label>").attr("for", "valorParcela" + tamanho + "").text("Valor da Parcela R$").addClass('active');

	divInputValor.append(inputValor).append(labelValor);

	divInputNParcela.append(inputNParcelas);
	divInputNParcela.append(label);

	divRow.append(parcelaId)
	divRow.append(divInputNParcela);
	divRow.append(divInputValor);
	divRow.append(divDataCobranca);
	divRow.append(divDataVencimento);
	divRow.append(divComCheckbox);
	
	inicializarDatePickers();
	$('.money').mask('000.000.000.000.000,00', {reverse: true});

	return divRow;
}

function calculaValor(index) {

	var valorPago = 0;

	var valorCobrarCheio = $("#cobrar").val().replace(/\./g, '').replace(",", ".");
	var texto = parseFloat(valorCobrarCheio);
	var valorSomado = texto;

	var i = Number($('.valorParcela').length);
	var j = 0
	
	console.log("cobrar cheio "+valorCobrarCheio)

	$.each($('.valorParcela'), function() {
		if ($('#parcelaPaga' + j).is(':checked')) {

			valorPago += parseFloat($('#valorParcela' + j).val().replace(/\./g, '').replace(",", "."));

			console.log('Valor pago ' + valorPago)

			valorSomado = Number(valorCobrarCheio) - Number(valorPago);

			i--

		}

		j++

	});


	console.log("vai ser " + valorSomado + "/" + i)

	valorSomado = valorSomado / i

	return valorSomado;
}

function preencheValor() {

	var valorDividido = calculaValor(index);
	valorDividido = mascaraValor(valorDividido.toFixed(2));

	var i = 1

	$.each($('.valorParcela'), function() {
		if (!$('#parcelaPaga' + i).is(':checked')) {
			$(this).val(valorDividido);
		}
		i++
	});
}

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

function salvaNF() {

	var parcelas = [];

	var index = 1;
	$.each($('.valorParcela'), function() {
		var parcela = {
			id: $('#id'+index).val(),
			nParcela: $('#parcela' + index).val(),
			valorParcela: $('#valorParcela' + index).val().replace(/\./g, '').replace(",", "."),
			dataCobranca: $('#dataCobrancaParcela' + index).val(),
			dataVencimento: $('#dataVencimentoParcela' + index).val(),
			pago: $("#parcelaPaga" + index).is(':checked'),
		}
		
		parcelas.push(parcela);
		index++;
	})
	
	var filial = {
		id: $("#filial").find(":selected").val(),
	}
	
	var faturamento = {
		id: $('#idFat').val()
	}
	
	var campanha = $('#ano').val()

	var nf = {
		id: $('#idNota').val(),
		cnpjFaturar: $('#cnpjFaturar').val(),
		razaoSocialFaturar: $('#razaoSocialFaturar').val(),
		numeroNF: $('#numeroNF').val(),
		tipoNotaFiscal: $('#selectTipoNotaFiscal').val(),
		numeroNotaSubstituida: $('#numeroNotaSubstituida').val(),
		faturamento: faturamento,
		cancelada: $("#cancelada").is(':checked'),
		cobranca: $("#cobranca").is(':checked'),
		atraso: $("#atraso").is(':checked'),
		dataEmissao: $('#dataEmissao').val(),
		dataVencimento: $('#dataVencimento').val(),
		dataCobranca: $('#dataCobranca').val(),
		ano: $('#ano').val(),
		motivoCancelamento: $("#selectMotivoCancelamento").find(":selected").val(),
		incidencia: $("#selectIncidencia").find(":selected").val(),
		obsNota: $('#obsNota').val(),
		obsCobranca: $('#obsCobranca').val(),
		faturar: $('#valorFat').val(),
		cobrar: $('#cobrar').val(),
		valorCobranca: $('#valorCobranca').val(),
		duplaTributacao: $('#duplaTributacao').val(),
		percentVariavel: $('#percentVariavel').val(),
		ISSvariavel: $('#ISSvariavel').val(),
		statusNF: $('#statusNF').val(),
		categoriaNF: $('#categoriaNF').val(),
		timing: $('#timing').val(),
		parcelas: parcelas,
		filial: filial,
		campanha: campanha
	};
	
	fat = $('#valorFat').val();
	console.log("fat",fat)
	
	var campanha = $('#ano').val()
	
	console.log()
	
	$.ajax({
					type: "POST",
					contentType: "application/json",
					url: "/notasFiscais/update",
					data: JSON.stringify(nf),
					dataType: 'JSON',
					statusCode: {
						200: function() {
							Materialize
								.toast(
									'Nota atualizada com sucesso!',
									5000, 'rounded');
						},
						500: function() {
							Materialize.toast(
								'Ops, houve um erro interno',
								5000, 'rounded');
						},
						400: function() {
							Materialize
								.toast(
									'Você deve estar fazendo algo de errado',
									5000, 'rounded');
						},
						404: function() {
							Materialize.toast('Url não encontrada',
								5000, 'rounded');
						}
					}
				});
				
				substituiNF();
	
	}
	

$(function atualizaValor() {

var fatId = $("#idFat").val()
console.log( "o id do faturamento é ",fatId)


			var url = "/faturamentos/" + fatId + "/fat.json"

			return $.ajax({
				type : 'GET',
				url : url,
				async : false,
				success : function(data) {

					$("#razaoSocial").val(data.cliente.razaoSocial)
					$("#razaoSocial").focusin()

					var cnpj = data.cliente.cnpj
					cnpj = cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,
							"\$1.\$2.\$3\/\$4\-\$5");

					$("#cnpj").val(cnpj)
					$("#cnpj").focusin()

					$("#produto").val(data.produto.nome)
					$("#produto").focusin()

					$("#categoriaProblematica").val(
							data.cliente.categoriaProblematica)
					$("#categoriaProblematica").focusin()

					var today = new Date();
					var dd = String(today.getDate()).padStart(2, '0');
					var mm = String(today.getMonth() + 1).padStart(2, '0');
					var yyyy = today.getFullYear();

					today = dd + '/' + mm + '/' + yyyy;

					//$("#dataEmissao").val(today)
					//$("#dataEmissao").focusin()
					try{
						$("#campanha option").each(function() {
						if ($(this).val() == data.producao.ano) {
							$(this).attr("selected", "selected");
						}
					});
					}catch{
						
					}
					
					
					$("#campanha").material_select();
					
					//$("#valorFat").material_select();

					/*$("#valorFat").val(
							data.valorEtapa.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							}));
					$("#valorFat").focusin()*/

					$("#formaPagamento").val(data.contrato.formaPagamento)
					$("#formaPagamento").focusin()

					var valorBruto = parseFloat(data.valorEtapa)
					var IRRF = valorBruto * (1.5 / 100)
					var CSSL = valorBruto * (1 / 100)
					var PIS = valorBruto * (0.65 / 100)
					var COFINS = valorBruto * (3 / 100)
					
					if((IRRF) < 10.0) {
						IRRF = 0.0;
						IRRF = parseFloat(IRRF).toFixed(2)
					}
					
					if((Number(CSSL) + Number(PIS) + Number(COFINS)) < 10.0) {
						CSSL = 0.0;
						PIS = 0.0;
						COFINS = 0.0;
						CSSL = parseFloat(CSSL).toFixed(2)
						PIS = parseFloat(PIS).toFixed(2)
						COFINS = parseFloat(COFINS).toFixed(2)
					}

					var valorLiquido = valorBruto
							- (IRRF + CSSL + PIS + COFINS)

//					$("#valorCobrar").material_select()

					$("#valorCobrarI").val(
							valorLiquido.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							})
							);
					$("#valorCobrarI").focusin();

					$("#IRRF").val(IRRF.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#IRRF").focusin();
					$("#CSSL").val(CSSL.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#CSSL").focusin();
					$("#PIS").val(PIS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PIS").focusin();
					$("#COFINS").val(COFINS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#COFINS").focusin();

					var PISdevido = valorBruto * (1.65 / 100)
					var COFINSdevido = valorBruto * (7.6 / 100)
					var ISSdevido = valorBruto * (5 / 100)

					$("#PISdevido").val(PISdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PISdevido").focusin();
					$("#COFINSdevido").val(
							COFINSdevido.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							}))
					$("#COFINSdevido").focusin();
					$("#ISSdevido").val(ISSdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#ISSdevido").focusin();

					$("#percentVariavel").val(0)
					$("#percentVariavel").focusin()

					var obs = 'Observações do Contrato: '
							+ data.contrato.obsContrato + "\n" + "\n"
							+ 'Observações do Faturamento: ' + data.observacao

					$("#obs").val(obs)
					$("#obs").focusin()
					
//					console.log(valorLiquido, " valor liquido")
//					$("#valorCobrar").append("teste")

				}
			});

		})

$(function maskValor(){
	$(".moneyClass").each(function(){
		var texto = $(this).text();
		texto = parseFloat(texto).toFixed(2).toLocaleString('pt-BR')
		texto = mascaraValor(texto)
		$(this).text("R$ " + texto);
	})
})



function substituiNF(){
//se checkbox cancelada for checked abre a modal

	if ($("#cancelada").is(':checked') == true){
		console.log("entrouno if");
		$('#modalSubstituiNF').modal('open');
	}
}


$('.subsNF').on("click", function(){
//entra nessa function se o botão de salvar na modal modalSubstituiNF for apertado
	console.log("entrou em subsNF");
	geraNFSubstituta();

})


		
function geraNFSubstituta(){
	
	var fatId = $('#idFat').val();
		var faturamento = {
			id : fatId
		}
			console.log(faturamento)
			var valorCobrar=  $("#cobrar").val()

			var valorFaturar = $("#valorFat").val()
			
			var valorCobranca = $("#valorCobrança").val();

			var duplaTributacao = $("#duplaTributacao :selected").val();

			if (duplaTributacao == "Sim") {
				duplaTributacao = true;
			} else {
				duplaTributacao = false;
			}
			var filial = {
		id: $("#filial").find(":selected").val(),
	}
			
			var data = new Date();
			
			var nf = {
				faturamento : faturamento,
				tipoNF : $("#tipoNF :selected").val(),
				categoriaNF : $("#categoriaNF :selected").val(),
				dataEmissao: data,
				dataVencimento :null,
				dataCobranca : null,
				numeroNF :null,  
				faturar : valorFaturar,
				cobrar : valorCobrar,
				valorCobranca : valorCobranca,
				obsNota : $("#obs").val(),
				parcela : $("#parcela").val(),
				duplaTributacao : duplaTributacao,
				percentVariavel : $("#percentVariavel").val(),
				ISSvariavel : $("#ISSVariavel").val(),
				numeroNotaSubstituida: $("#numeroNF").val(),
				filial :filial,
				  

			}

			console.log(nf)

			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/notasFiscais/geraNFSubstituta",
				data : JSON.stringify(nf),
				dataType : "JSON",
				statusCode : {
					200 : function() {
						console.log("nf",nf);
						Materialize.toast('Nota Fiscal substituta criada com sucesso!',
								5000, 'rounded');
								
						notaSubstituida(nf.numeroNotaSubstituida);
					},
					500 : function() {
						console.log("nf",nf)
						Materialize.toast('Ops, houve um erro interno', 5000,
								'rounded');
					},
					400 : function() {
						Materialize.toast(
								'Você deve estar fazendo algo de errado', 5000,
								'rounded');
					},
					404 : function() {
						Materialize
								.toast('Url não encontrada', 5000, 'rounded');
					}
				}
			});
			
}		
//passar a informação certa, tem que ser o id da nota
function notaSubstituida(idNotaSubstituida){
	console.log("fatId",idNotaSubstituida);
	
	var url = "/notasFiscais/" + idNotaSubstituida + "/notaSubstituida.json"
	
	
	
	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			var id = ("data",data.id);
			if(data.consolidado == true || data.consolidadoPorFat == true){
				 window.location.href = "/notaFiscal/detailsNotaConsolidada/"+ id;
			}
			else{
				window.location.href = "/notasFiscais/"+id;
			}
		}
	})
	
	
}


