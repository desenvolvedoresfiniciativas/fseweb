$('#addParcela').click(addLinhaParcela);
$('#removeParcela').click(apagaLinhaParcela);
$('#btn-cad-NotaFiscal').click(salvaNF);

$(function() {
	maskValor();
	$(".select").material_select();
	
	$('body').on('change', 'input[name="fatId"]:radio', function() {
     	var cobrar = $(this).parent().last().prev().text().substring(3);
		$("#faturar").val(mascaraValor(cobrar));
		$("#faturar").focusin();
		calculaDescontoDeImpostos(cobrar);
		})
})

function calculaDescontoDeImpostos(cobrar) {
	var valorACalcular = parseFloat(cobrar.replace(/\./g, '').replace(",", "."));

	var IRRF = (1.5 / 100);
	var CSSL = (1 / 100);
	var PIS = (0.65 / 100);
	var COFINS = (3 / 100);
	
	$("#IRRF").val("R$ " + parseFloat(valorACalcular * IRRF).toFixed(2).toLocaleString('pt-BR'));
	$("#IRRF").focusin();
	$("#CSSL").val("R$ " + parseFloat(valorACalcular * CSSL).toFixed(2).toLocaleString('pt-BR'));
	$("#CSSL").focusin();
	$("#PIS").val("R$ " + parseFloat(valorACalcular * PIS).toFixed(2).toLocaleString('pt-BR'));
	$("#PIS").focusin();
	$("#COFINS").val("R$ " + parseFloat(valorACalcular * COFINS).toFixed(2).toLocaleString('pt-BR'));
	$("#COFINS").focusin();

	valorACalcular -= valorACalcular * IRRF;
	valorACalcular -= valorACalcular * CSSL;
	valorACalcular -= valorACalcular * PIS;
	valorACalcular -= valorACalcular * COFINS;

	$("#cobrar").val(mascaraValor(valorACalcular.toFixed(2)));
	$("#cobrar").focusin();
}

$("#cnpjFaturar").on('input', function(e) {
	$("#cnpjFaturar").mask('00.000.000/0000-00', {
		reverse: true
	});
})

function maskValor(texto) {
	texto = parseFloat(texto).toFixed(2).toLocaleString('pt-BR')
	return texto;
}

$(function() {
	selectTipoNotaFiscal()
	var nomeFiltro = $("#select-tipoNotaFiscal :selected").val()
	console.log(nomeFiltro);
	if (nomeFiltro === 'SUBSTITUIDA') {
		$("#numeroNotaSubstituida").show();
	} else {
		$("#numeroNotaSubstituida").hide();
	}
})

function selectTipoNotaFiscal() {
	$("#select-tipoNotaFiscal").change(
		function() {
			var nomeFiltro = $("#select-tipoNotaFiscal :selected").val()
			if ($(this).val() === 'SUBSTITUIDA') {
				$("#numeroNotaSubstituida").show();
			} else {
				$("#numeroNotaSubstituida").hide();
			}
		});
}

function addLinhaParcela() {
	var lista = $("#parcela");

	lista.append(escreveLinhas());
	index = $('.valorParcela').length;
	console.log("Quantidade de campos " + index)
	preencheValor(index);
}

function apagaLinhaParcela() {

	if (!$('#parcelaPaga' + index).is(':checked')) {
		$('#parcela').children().last().remove()
	} else {
		Materialize.toast('Você não pode remover parcelas já pagas.', 5000, 'rounded');
	}

	index = $('.valorParcela').length
	console.log("Quantidade de campos " + index)

	preencheValor(index)
}

function escreveLinhas(valorCobrar) {

	var divRow = $("<div>").addClass("row");

	var tamanho = Number($('.valorParcela').length) + Number(1);

	console.log("Tamanho dentro da escreve linhas: " + tamanho)

	var divInputNParcela = $("<div>").addClass("col").addClass("m1").addClass("input-field");
	var inputNParcelas = $("<input>").attr("type", "text").attr("id", "parcela" + tamanho).val(tamanho).focusout();
	var label = $("<label>").attr("for", "parcela" + tamanho).text("N° Parcela").addClass('active');

	divInputNParcela.append(inputNParcelas).append(label)

	var divDataCobranca = $("<div class='col m2'>" +
		"<label for='dataCobrancaParcela" + tamanho + "'>Data de Cobrança</label> " +
		"<input type='text' id='dataCobrancaParcela" + tamanho + "' th:placeholder=" + 'DD/MM' +
		" name='dataCobrancaParcela' class='datepicker' />  </div>");

	var divDataVencimento = $("<div class='col m2'>" +
		"<label for='dataVencimentoParcela" + tamanho + "'>Data de Vencimento</label> " +
		"<input type='text' id='dataVencimentoParcela" + tamanho + "' th:placeholder=" + 'DD/MM' +
		" name='dataVencimentoParcela' class='datepicker' />  </div>");

	var divComCheckbox = $("<p>" +
		" </p>");

	var checkbox = $(" <input name='parcelaPaga' type='checkbox' class='filled-in' id='parcelaPaga" + tamanho + "' />" +
		" <label for='parcelaPaga" + tamanho + "'>Pago</label> ")

	$.prop('checked', false)
	divComCheckbox.append(checkbox)

	var divInputValor = $("<div>").addClass("col").addClass("m2").addClass("input-field");
	var inputValor = $("<input>").attr("type", "text").attr("id", "valorParcela" + tamanho + "").attr("class", "valorParcela");
	var labelValor = $("<label>").attr("for", "valorParcela" + tamanho + "").text("Valor da Parcela R$").addClass('active');

	divInputValor.append(inputValor).append(labelValor);

	divInputNParcela.append(inputNParcelas);
	divInputNParcela.append(label);

	divRow.append(divInputNParcela);
	divRow.append(divInputValor);
	divRow.append(divDataCobranca);
	divRow.append(divDataVencimento);
	divRow.append(divComCheckbox);
	$('.money').mask('000.000.000.000.000,00', { reverse: true });

	inicializarDatePickers();
	return divRow;
}

function calculaValor(index) {

	var valorPago = 0;

	var valorCobrarCheio = $("#cobrar").val().replace(/\./g, '').replace(",", ".");
	var texto = parseFloat(valorCobrarCheio);
	var valorSomado = texto;

	var i = Number($('.valorParcela').length);
	var j = 0

	$.each($('.valorParcela'), function() {
		if ($('#parcelaPaga' + j).is(':checked')) {

			valorPago += parseFloat($('#valorParcela' + j).val().replace(/\./g, '').replace(",", "."));

			console.log('Valor pago ' + valorPago)

			valorSomado = Number(valorCobrarCheio) - Number(valorPago);

			console.log('Valor somado ' + valorSomado)

			i--

		}

		j++

	});


	console.log("vai ser " + valorSomado + "/" + i)

	valorSomado = valorSomado / i

	return valorSomado;
}

function preencheValor() {

	var valorDividido = calculaValor(index);
	valorDividido = mascaraValor(valorDividido.toFixed(2));

	var i = 1

	$.each($('.valorParcela'), function() {
		if (!$('#parcelaPaga' + i).is(':checked')) {
			$(this).val(valorDividido);
		}
		i++
	});
}

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

function salvaNF() {

	var parcelas = [];

	var index = 1;
	$.each($('.valorParcela'), function() {
		var parcela = {
			id: $('#id' + index).val(),
			nParcela: $('#parcela' + index).val(),
			valorParcela: $('#valorParcela' + index).val().replace(/\./g, '').replace(",", "."),
			dataCobranca: $('#dataCobrancaParcela' + index).val(),
			dataVencimento: $('#dataVencimentoParcela' + index).val(),
			pago: $("#parcelaPaga" + index).is(':checked'),
		}

		parcelas.push(parcela);
		index++;
	})

	var cliente = {
		id: $("#idCliente").val()
	}
	
	var faturamento = {
		id: $('input[name=fatId]:checked').val()
	}
	
	var filial = {
		id: $("#filial").find(":selected").val(),
	}

	var nf = {
		id: $('#idNota').val(),
		cnpjFaturar: $('#cnpjFaturar').val(),
		razaoSocialFaturar: $('#razaoSocialFaturar').val(),
		cliente: cliente,
		numeroNF: $('#numeroNF').val(),
		tipoNotaFiscal: $('#select-tipoNotaFiscal').val(),
		numeroNotaSubstituida: $('#numeroNotaSubstituida').val(),
		fatIdHolder: $('#fatIdHolder').val(),
		cancelada: $("#cancelada").is(':checked'),
		cobranca: $("#cobranca").is(':checked'),
		atraso: $("#atraso").is(':checked'),
		dataEmissao: $('#dataEmissao').val(),
		dataVencimento: $('#dataVencimento').val(),
		dataCobranca: $('#dataCobranca').val(),
		ano: $('#ano').val(),
		motivoCancelamento: $("#selectMotivoCancelamento").find(":selected").val(),
		incidencia: $("#select-incidencia").find(":selected").val(),
		obsNota: $('#obsNota').val(),
		faturar: $('#faturar').val(),
		cobrar: $('#cobrar').val(),
		valorCobranca: $('#valorCobranca').val(),
		duplaTributacao: $('#duplaTributacao').val(),
		percentVariavel: $('#percentVariavel').val(),
		ISSvariavel: $('#ISSvariavel').val(),
		statusNF: $('#statusNF').val(),
		categoriaNF: $('#categoriaNF').val(),
		timing: $('#timing').val(),
		parcelas: parcelas,
		faturamento: faturamento,
		filial: filial
	};

	console.log(nf)

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/notasFiscais/add",
		data: JSON.stringify(nf),
		dataType: 'JSON',
		statusCode: {
			200: function(data) {
				Materialize
					.toast(
						'Nota criada com sucesso!',
						5000, 'rounded');
						$('#btn-cad-NotaFiscal').attr("disabled", true);
						window.location.replace("all")
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});
}