$("#cnpjFaturar").blur(buscaEmpresa);

function buscaEmpresa() {
	
	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);
	
	if ( !$(this).val().length == 0 ) {

		$.getJSON("/clientes/" + cnpj +  ".json", function(dados) {

			console.log(dados);  
			if (dados.erro != true) {
				
				populaTabelaFaturamentos(dados.id);

				$("#razaoSocialFaturar").val(dados.razaoSocial);
				$("#razaoSocialFaturar").focusin();
				$("#idCliente").val(dados.id);

			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})
	}
	}
	
function populaTabelaFaturamentos(idCliente){
			$.getJSON("/faturamentos/" + idCliente +  "/fatsCliente.json", function(dados) {

			if (dados.erro != true) {
				
				$("#corpo-faturamento").empty();
				
				console.log(dados)
				
			for(var i = 0; i<dados.length; i++){
			var faturamento = dados[i]
			
			var versao = "";
			var etapa = "";
			
			if(faturamento.balancete!=null){
				versao = faturamento.balancete.versao
			}
			if(faturamento.etapa!=null){
				etapa = faturamento.etapa.nome
			}

			$("#corpo-faturamento").append("<tr><td>"+faturamento.produto.nome+"</td>"
			+"<td>"+faturamento.campanha+"</td>"
			+"<td>"+versao+"</td>"
			+"<td>"+etapa+"</td>"
			+"<td> R$ "+mascaraValor(faturamento.valorEtapa.toFixed(2))+"</td>"
			+"<td><input name='fatId' class='fatId' type='radio' value='"+faturamento.id+"' id='"+faturamento.id+"' /><label for='"+faturamento.id+"'></label></td>")
			}

			} else {
				Materialize.toast('Nenhum faturamento encontrado para o cliente.', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})
}

function limparNomeEmpresa() {
		$("#nomeEmpresa").val("");
		$('#cnpjFaturar').val("");
		$('#razaoSocialFaturar').val("");
	}
	
function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

