$(function() {
	console.log("teste")
	teste();
	calculaImpostos();
	//simularConsolidacao();
	consolidado();
	calculaImpostosDevidosEmpresa();
//	carregaMemoriaDeCalculo();
	exibeValor();
})

function calculaImpostos() {
	
	var valorBruto = parseFloat($("#idValorEtapa").text());
	console.log(valorBruto, "valor bruto/etapa");

	var IRRF = valorBruto * (1.5 / 100)
	var CSSL = valorBruto * (1 / 100)
	var PIS = valorBruto * (0.65 / 100)
	var COFINS = valorBruto * (3 / 100)
	
	IRRF = parseFloat(IRRF).toFixed(2)
	CSSL = parseFloat(CSSL).toFixed(2)
	PIS = parseFloat(PIS).toFixed(2)
	COFINS = parseFloat(COFINS).toFixed(2)
	
	console.log(IRRF,CSSL,PIS,COFINS)
	console.log(IRRF)
	
	if(Number(IRRF) < 10.0) {
		IRRF = 0.0;
		IRRF = parseFloat(IRRF).toFixed(2)
	}
	
	if((Number(CSSL) + Number(PIS) + Number(COFINS)) < 10.0) {
		CSSL = 0.0;
		PIS = 0.0;
		COFINS = 0.0;
		CSSL = parseFloat(CSSL).toFixed(2)
		PIS = parseFloat(PIS).toFixed(2)
		COFINS = parseFloat(COFINS).toFixed(2)
	}
	
	var total = Number(IRRF) + Number(CSSL) + Number(PIS) + Number(COFINS);
	var valorLiquido = valorBruto - total;
	
	console.log(total,valorLiquido + "-------")
	
	total = parseFloat(total).toFixed(2)
	valorLiquido = parseFloat(valorLiquido).toFixed(2)
	
	console.log(total,valorLiquido)
	
	$("#idIRRF").text("R$ " + mascaraValor(IRRF))
	$("#idCSSL").text("R$ " + mascaraValor(CSSL))
	$("#idPIS").text("R$ " + mascaraValor(PIS))
	$("#idCOFINS").text("R$ " + mascaraValor(COFINS))
	$("#idTotal").text("R$ " + mascaraValor(total))
	$("#idValorLiquido").text("R$ " + mascaraValor(valorLiquido))
}

function calculaImpostosDevidosEmpresa() {
	
	var valorBruto = parseFloat($("#idValorEtapa").text());
	console.log(valorBruto, "valor bruto/etapa");

	var PISImpostosDevidos = valorBruto * (1.65 / 100)
	var COFINSImpostosDevidos = valorBruto * (7.6 / 100)
	var ISSImpostosDevidos = valorBruto * (3 / 100)
	//abaixo está com valor de 0
	var ISSVariavelImpostosDevidos = valorBruto * (0 / 100)
	var totalImpostosDevidos = PISImpostosDevidos + COFINSImpostosDevidos + ISSImpostosDevidos + ISSVariavelImpostosDevidos;
	
	PISImpostosDevidos = parseFloat(PISImpostosDevidos).toFixed(2)
	COFINSImpostosDevidos = parseFloat(COFINSImpostosDevidos).toFixed(2)
	ISSImpostosDevidos = parseFloat(ISSImpostosDevidos).toFixed(2)
	ISSVariavelImpostosDevidos = parseFloat(ISSVariavelImpostosDevidos).toFixed(2)
	totalImpostosDevidos = parseFloat(totalImpostosDevidos).toFixed(2)

	$("#idPISImpostosDevidos").text('R$ ' + mascaraValor(PISImpostosDevidos))
	$("#idCOFINSImpostosDevidos").text('R$ ' + mascaraValor(COFINSImpostosDevidos))
	$("#idISSImpostosDevidos").text('R$ ' + mascaraValor(ISSImpostosDevidos))
	$("#idISSVariavelImpostosDevidos").text('R$ ' + mascaraValor(ISSVariavelImpostosDevidos))

	$("#idTotalImpostosDevidos").text('R$ ' + mascaraValor(totalImpostosDevidos))
//	$("#idValorLiquido").text(valorLiquido)
}

function carregaMemoriaDeCalculo() {
	
	var fatId = $("#idFaturamento").text();	
	var url = "/faturamentos/" + fatId + "/fat.json";

	$.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			console.log("entrou no ajax novo")

			var i = 0
			
			console.log(data)
			var valorBeneficio = 0.0;
			
			if(data.consolidado == true) {
				simularConsolidacao();
				/*var url = 
				$.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {
						
					}
					})*/
			
			} else {
				$.each($('.valorBeneficio'), function () {
				var valorAtual = $(this).text().replaceAll(".", "");
				valorAtual = valorAtual.replace(",", ".");
				valorAtual = Number(valorAtual);
				
				valorBeneficio = valorBeneficio + valorAtual
				console.log(valorBeneficio, " vlbenef atual")
				})
			}
			// trazer unicamente o balancete que está no registro do fat (balanceteId)
			//se for consolidado, trago a soma da redução dos ids que estiverem na tabela if consolidado is true, trazer ids fats consolidados
			//tratar essa string para trazer cada balancete a ser considerado na soma do beneficio
			/*$.each($('.valorBeneficio'), function () {
				var valorAtual = $(this).text().replaceAll(".", "");
				valorAtual = valorAtual.replace(",", ".");
				valorAtual = Number(valorAtual);
				
				valorBeneficio = valorBeneficio + valorAtual
				console.log(valorBeneficio, " vlbenef atual")
				})*/

			for(i; i <= data.contrato.honorarios.length; i++) {
				
				var tipoDeHonorario;
				console.log(data.contrato.honorarios[i].nome)
				switch (data.contrato.honorarios[i].nome) {

				case "Percentual Escalonado":
				tipoDeHonorario = "PERCENTUAL ESCALONADO";
				var faixas = data.contrato.honorarios[i].faixasEscalonamento;
				var index = 0
				if (data.consolidado == true) {
					console.log($('#idBeneficioTotalSomatoriaReducaoBalancetes').text());
					var temp = parseFloat($('#idBeneficioTotalSomatoriaReducaoBalancetes').text()).toFixed(2);
				} else {
					var temp = parseFloat(valorBeneficio);
				}
				console.log(temp)

				var valorCalc = 0;
				var valorAplicado = 0;
				var valorTotalDosHonorarios = 0;
				var valorTotalAplicado = 0;
				
				for(var index in faixas) {
					console.log("entrou no for das faixas")

				var inicial = faixas[index].valorInicial;
				inicial = Number(Math.floor(inicial, -1).toFixed(2))
				inicial = parseFloat(inicial).toFixed(2)
				var finalF = faixas[index].valorFinal;
				finalF = Number(Math.floor(finalF, -1).toFixed(2))
				finalF = parseFloat(finalF).toFixed(2)
				var gap1 = finalF - inicial;
				var percentual = faixas[index].valorPercentual / 100;

				if (temp > 0) {

					if (temp >= gap1) {
						valorCalc = (gap1 * percentual);
						temp = temp - (gap1);
						valorAplicado = gap1;
						valorAplicado = parseFloat(valorAplicado)
						valorTotalDosHonorarios = valorTotalDosHonorarios + valorCalc
						valorTotalAplicado += valorAplicado
						} else if (temp < gap1) {
							console.log("Será calculado dentro dentro da faixa com o valor restante do temp...");
							valorCalc = (temp * percentual);
							valorAplicado = temp;
							valorAplicado = parseFloat(valorAplicado)
							valorTotalDosHonorarios = valorTotalDosHonorarios + valorCalc
							temp = temp - (gap1);
							valorTotalAplicado += valorAplicado
							valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						}
					} else {
						valorCalc = 0;
						valorAplicado = 0;
						valorTotalDosHonorarios = valorTotalDosHonorarios + valorCalc;
						valorTotalAplicado += valorAplicado
					}

				var tr = $("<tr>")
				var tds = $("<td>De R$ "+mascaraValor(inicial.toFixed(2))+" à R$ "+mascaraValor(finalF.toFixed(2))+"</td>" +
				"<td>R$ "+mascaraValor(valorAplicado.toFixed(2))+"</td>" +
				"<td>"+faixas[index].valorPercentual+"%</td>"+
				"<td>R$ "+mascaraValor(valorCalc.toFixed(2))+"</td>")
				tr.append(tds)
				
				$("#tabelaEscalonado").append(tr)

				index++
				}
				
				valorTotalDosHonorarios = parseFloat(valorTotalDosHonorarios).toFixed(2)
				
				if(temp > 0 || limitacao > 0) {
					console.log("temp > 0 || limitacao > 0")
					var valorAcimaDe = Number(data.contrato.honorarios[i].acimaDe)
					var percentualAcimaDe = Number((data.contrato.honorarios[i].percentAcimaDe) / 100);
					var honorarioAcimaDe = Number(percentualAcimaDe) * Number(temp);
					var limitacao = Number(data.contrato.honorarios[i].limitacao);
					console.log(data.contrato.honorarios[i].limitacao);
					var valorAplicadoAcimaDe = 0;
//					var valorTotalDosHonorariosAcimaDe = valorTotalDosHonorarios;

					if (limitacao > 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(valorAcimaDe.toFixed(2)))+"</td>" +
						"<td>R$ "+(mascaraValor(valorAplicadoAcimaDe.toFixed(2)))+"</td>" +
						"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
						"<td>R$ "+(limitacao.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						.append(trAcimaDe)
						valorTotalDosHonorarios = Number(valorTotalDosHonorarios) + Number(parseFloat(limitacao).toFixed(2))
						valorTotalAplicado += valorAplicadoAcimaDe
						valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						$("#tabelaEscalonado").append(trAcimaDe)
					} else if (temp > 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(valorAcimaDe.toFixed(2)))+"</td>" +
						"<td>R$ "+mascaraValor(temp.toFixed(2))+"</td>" +
						"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
						"<td>R$ "+mascaraValor(honorarioAcimaDe.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						.append(trAcimaDe)
						valorTotalDosHonorarios = Number(valorTotalDosHonorarios) + Number(parseFloat(honorarioAcimaDe).toFixed(2))
						
						valorAplicadoAcimaDe = parseFloat(temp).toFixed(2)
						valorTotalAplicado += valorAplicadoAcimaDe
						valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						console.log(valorAplicado, "vl aplicado")
						valorTotalAplicado = Number(parseFloat(valorAplicadoAcimaDe).toFixed(2)) + Number(parseFloat(valorTotalAplicado).toFixed(2))
						console.log("QQQQQQQq", valorTotalAplicado)
						$("#tabelaEscalonado").append(trAcimaDe)
					} else if (temp <= 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(parseFloat(valorAcimaDe).toFixed(2)))+"</td>" +
						"<td>R$ 0,00</td>" +
						"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
						"<td>R$ 0,00</td>")
						trAcimaDe.append(tdsAcimaDe)
						.append(trAcimaDe)
						valorTotalAplicado += valorAplicadoAcimaDe
						valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						$("#tabelaEscalonado").append(trAcimaDe)
					}
				} else {
					console.log("else")
					var valorAcimaDe = Number(data.contrato.honorarios[i].acimaDe)
					var percentualAcimaDe = Number((data.contrato.honorarios[i].percentAcimaDe) / 100);
					var honorarioAcimaDe = Number(percentualAcimaDe) * Number(temp);
					var limitacao = Number(data.contrato.honorarios[i].limitacao);
					console.log(data.contrato.honorarios[i].limitacao);
					var valorAplicadoAcimaDe = 0;
					
					var trAcimaDe = $("<tr>")
					var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(parseFloat(valorAcimaDe).toFixed(2)))+"</td>" +
					"<td>R$ 0,00</td>" +
					"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
					"<td>R$ 0,00</td>")
					trAcimaDe.append(tdsAcimaDe)
					.append(trAcimaDe)
					valorTotalAplicado += valorAplicadoAcimaDe
					valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
					$("#tabelaEscalonado").append(trAcimaDe)
				}
				
				$("#totalHonorarios").append("Total: R$ " + mascaraValor(valorTotalDosHonorarios));
				$("#totalHonorariosAplicados").append("Total Aplicado: R$ " + mascaraValor(valorTotalAplicado));
				$("#totalAFaturar").append("R$ " + mascaraValor(valorTotalDosHonorarios))
				exibeValor();

				break;

				case "Valor Fixo":
				tipoDeHonorario = "VALOR FIXO";
				var valorFixo = Number(data.contrato.honorarios[i].valor);
				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				$("#divCompleta").append("<b>Valor Fixo: </b> <p style='display:inline'>R$ "+mascaraValor(valorFixo.toFixed(2))+"</p><br /><hr />")
				exibeValor();
				
				break;

				case "Percentual Fixo":
				tipoDeHonorario = "PERCENTUAL FIXO";
				
				var valorAplicado = valorBeneficio;
				console.log(valorAplicado, " case percentual fixo ")
				valorAplicado = parseFloat(valorAplicado).toFixed(2)
				console.log(valorAplicado, " case percentual fixo com o tofixed2")
				valorAplicado = parseFloat(valorAplicado)
				var valorTotalDosHonorarios = 0;
				
				console.log(valorAplicado, " ++++++++++++++++++++++++")
				
				var percentualFixo = Number(data.contrato.honorarios[i].valorPercentual / 100);
				valorTotalDosHonorarios = percentualFixo * valorAplicado;
				valorTotalDosHonorarios = parseFloat(valorTotalDosHonorarios).toFixed(2)

				$("#idPercentualFixoHonorarioValorAplicado").text("R$ " + mascaraValor(valorAplicado))
				$("#idPercentualFixoHonorario").text("R$ " + mascaraValor(valorTotalDosHonorarios))
				$("#totalAFaturar").append("R$ " + mascaraValor(valorTotalDosHonorarios))
				exibeValor();
				
				break;
				
				default:
				$("#nfTipoDeHonorario").text("Sem Memória de Cálculo")
				break;
				}
			};
		}
	});
}

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

function maskValor(){
$(".money").each(function(){
	var texto = $(this).text();
	texto = parseFloat(texto).toLocaleString('pt-BR')
	texto = "R$ "+texto
	$(this).text(texto);
})
}

function exibeValor() {
	var valorFormatado = $("#idValorEtapa").text();
	console.log(mascaraValor(valorFormatado));
	$("#idValorEtapa").text("R$ " + mascaraValor(valorFormatado));
}

function simularConsolidacao() {
	
	 idFaturamento = $("#idFaturamento").text();
	 console.log($("#idFaturamento").text())
	
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
					ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
	
	console.log("fatsTeste",fats);
	
    var url = "/faturamentos/simulaFatConsolidado"
    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
			console.log("entrou no sucess")
            console.log("respondese",response)
            if (response.erro == "noBalancete") {
                Materialize.toast('Um ou mais faturamentos não possuem balancetes vinculados, não é possível consolidar o benefício', 5000, 'rounded');
            } else if (response.erro == "mesmoBalancete") {
                Materialize.toast('Dois ou mais faturamentos possuem a mesma versão de balancete, não é possível consolidar o benefício.', 5000, 'rounded');
            } else if (response.erro == "mesmaEtapa") {
                Materialize.toast('Dois ou mais faturamentos não possuem a mesma etapa de faturamento, não é possível consolidar o benefício.', 5000, 'rounded');
            } else {
				console.log("entrou no else");
				//teste
                $('#beneficioConsolidado').show();

                //CARREGA VALORES
                $("#cnpjFat").val(response.faturamentoConsolidado.cliente.cnpj)
                $("#cnpjFat").mask('00.000.000/0000-00', {
                    reverse: true
                });
                $("#cnpjFat").focusin();

                $("#razaoSocialFat").val(response.faturamentoConsolidado.cliente.razaoSocial)
                $("#razaoSocialFat").focusin();
                $("#idCliente").val(response.faturamentoConsolidado.cliente.id)

                $("#idProduto").val(response.faturamentoConsolidado.producao.produto.id)
                $("#produto").val(response.faturamentoConsolidado.producao.produto.nome)
                $("#produto").focusin();

				$("#idProducao").val(response.faturamentoConsolidado.producao.id)

				$("#idEtapa").val(response.faturamentoConsolidado.etapa.id)
                $("#etapa").val(response.faturamentoConsolidado.etapa.nome)
                $("#etapa").focusin();

				$('#observacao').val(response.faturamentoConsolidado.observacao)
				$('#observacao').focusin();

				$("#idsFatsConsolidados").val(response.faturamentoConsolidado.idsFaturamentosConsolidados)

                $("#percentEtapa").val(response.percent)
                $("#percentEtapa").focusin();

                $("#campanha").val(response.faturamentoConsolidado.producao.ano)
                $("#campanha").focusin();

                var valor = response.faturamentoConsolidado.valorEtapa

                if (Number.isInteger(valor)) {
                    valor = valor + ".00"
                }

                $("#valorEtapa").val(mascaraValor(valor))
                $("#valorEtapa").focusin();

                var divFats = $("#fatsConsolidados")
		//fim

                var divPai = $("<div'></div>")

                var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
//					+ '<th>Observações</th>'
                    + '</tr>'
                    + '</thead>')
                var tableBody = $('<tbody id="tbody-informacoes"></tbody>')

                for (var i = 0; i < response.fatsIds.length; i++) {
                    var fatC = response.fatsIds[i];

                    var valorEtapa = fatC.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatC.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatC.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatC.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatC.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatC.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatC.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(parseFloat(valorEtapa).toFixed(2)) + '</td>')
//					var observacoes = $('<td>' + fatC.balancete.observacoes + '</td>')
//					if (observacoes.text() == "null") {observacoes = $('<td>' + '</td>')}

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)
					tr.append(observacoes)

                    tableBody.append(tr)

                }

                var trTotal = $('<tr></tr>')

                var beneficioTotal = response.beneficioConsolidado

                if (Number.isInteger(beneficioTotal)) {
                    beneficioTotal = beneficioTotal + ".00"
                }

                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                var total = $('<td><b>Total: R$ ' + mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(total)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)

                tableBody.append(trTotal)

                table.append(tableBody)
                divPai.append(table)

                $("#corpoFats").append(divPai)
                console.log("divPai",divPai)

                var idContrato = response.faturamentoConsolidado.contrato.id

                getMemoriaDeCalculo(idContrato, beneficioTotal);

            }
        },
        error: function (xhr) {
        }
    });
    				
		}
		
	});

}

function getMemoriaDeCalculo(idContrato, beneficioTotal) {

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idContrato + "/honorarios.json",
        async: false,
        success: function (hon) {

            for (var i = 0; i < hon.length; i++) {
                var honorario = hon[i];
                console.log(honorario)

                if (honorario.nome == "Percentual Fixo") {

                    $('#percentFixo').show()

                    var trFixo = $("<tr></tr>");

                    var beneficioFixo = $('<td> R$ ' + mascaraValor(beneficioTotal) + '</td>')
                    var percentualFixo = $('<td>' + honorario.valorPercentual + '%</td>')

                    var percent = (honorario.valorPercentual / 100)
                    var resultado = (beneficioTotal * percent)

					console.log("RESULTADO "+resultado)

                        if (Number.isInteger(resultado)) {
                            resultado = resultado + ".00"
                        }

                    var valor = $('<td class="resultadoPF">R$ ' + mascaraValor(parseFloat(resultado).toFixed(2)) + '</td>')

                    trFixo.append(beneficioFixo)
                    trFixo.append(percentualFixo)
                    trFixo.append(valor)

                    $('#tbody-percentualFixo').append(trFixo);

                }
                if (honorario.nome == "Percentual Escalonado") {
                    $('#escalonado').show()

                    var temp = beneficioTotal;
                    var valorCalc = 0.00;
                    var total = 0.00;
					var totalAplicado = 0.00;

                    var faixas = honorario.faixasEscalonamento

                    for (var i = 0; i < faixas.length; i++) {

                        var faixa = faixas[i];

                        var linhaFaixa = $("<tr></tr>");

                        var inicio = faixa.valorInicial;
                        var fim = faixa.valorFinal;
                        var beneficioAtribuido = 0.00;

                        if (Number.isInteger(inicio)) {
                            inicio = inicio + ".00"
                        } else {
							inicio = Number(Math.floor(inicio, -1).toFixed(2))
							inicio = parseFloat(inicio).toFixed(2)
						}
						
                        if (Number.isInteger(fim)) {
                            fim = fim + ".00"
                        } else {
							fim = Number(Math.floor(fim, -1).toFixed(2))
							fim = parseFloat(fim).toFixed(2)
						}

                        var gap1 = fim - inicio;
                        if (Number.isInteger(gap1)) {
                            gap1 = gap1 + ".00"
                        }

                        var intervalo = $('<td style="white-space: nowrap;">R$ ' + mascaraValor(inicio) + ' - R$ ' + mascaraValor(fim) + '</td>')
                        var percent = $('<td>' + faixa.valorPercentual + '%</td>')

                        if (temp > 0) {

                            if (temp >= gap1) {
                                valorCalc = parseFloat(gap1).toFixed(2) * (parseFloat(faixa.valorPercentual).toFixed(2)/100);
                                temp = parseFloat(temp).toFixed(2) - parseFloat(gap1).toFixed(2);
                                beneficioAtribuido = parseFloat(gap1).toFixed(2);
								beneficioAtribuido = parseFloat(beneficioAtribuido).toFixed(2);
	                            totalAplicado = Number(parseFloat(totalAplicado).toFixed(2));
								totalAplicado += Number(parseFloat(beneficioAtribuido).toFixed(2));
                            } else if (temp < gap1) {
                                valorCalc = parseFloat(temp).toFixed(2) * (parseFloat(faixa.valorPercentual).toFixed(2)/100);
                                beneficioAtribuido = parseFloat(temp).toFixed(2);
								beneficioAtribuido = parseFloat(beneficioAtribuido).toFixed(2);
                                temp = parseFloat(temp).toFixed(2) - parseFloat(gap1).toFixed(2);
								totalAplicado = Number(parseFloat(totalAplicado).toFixed(2));
								totalAplicado += Number(parseFloat(beneficioAtribuido).toFixed(2));
                            }

                        } else {
                            valorCalc = 0;
                        }

                        var beneficio = $('<td> R$ ' + mascaraValor(parseFloat(beneficioAtribuido).toFixed(2)) + '</td>')
                        var honorarioCalc = $('<td class="valorHon"> R$ ' + mascaraValor(parseFloat(valorCalc).toFixed(2)) + '</td>')

                        linhaFaixa.append(intervalo)
                        linhaFaixa.append(beneficio)
                        linhaFaixa.append(percent)
                        linhaFaixa.append(honorarioCalc)

                        $('#tbody-escalonado').append(linhaFaixa);

                    }

                    if (temp > 0) {

                        var percentualAcima = honorario.percentAcimaDe;
                        var honorarioAcimaDe = parseFloat((percentualAcima / 100) * temp).toFixed(2);

                        var vlAcimaDe = honorario.acimaDe
                        if (Number.isInteger(vlAcimaDe)) {
                            vlAcimaDe = vlAcimaDe + ".00"
                        } else {
							vlAcimaDe = Number(Math.floor(vlAcimaDe, -1).toFixed(2))
							vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
						}

                        var limitacao = honorario.limitacao
                        if (Number.isInteger(limitacao)) {
                            limitacao = limitacao + ".00"
                        } else {
							limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
							limitacao = parseFloat(limitacao).toFixed(2)
						}

                        var linhaAcimaDe = $("<tr></tr>");

                        var acimaDe = $('<td>Acima de: R$ ' + mascaraValor(vlAcimaDe.toFixed(2)) + ' (Limitação: R$ ' + mascaraValor(limitacao) + ')</td>');
                        var percentAcima = $('<td>' + percentualAcima + '%</td>');
                        var beneficioAcima = $('<td> R$ ' + mascaraValor(parseFloat(temp).toFixed(2)) + '</td>');

                        var valorAcimaDe = $('<td class="valorHon"> R$ ' + mascaraValor(honorarioAcimaDe) + '</td>');
						var valorAplicadoAcimaDe = $('<td class="valorAplicadoAcimaDeClass"> R$ ' + mascaraValor(totalAplicado) + '</td>');

                        linhaAcimaDe.append(acimaDe)
                        linhaAcimaDe.append(beneficioAcima)
                        linhaAcimaDe.append(percentAcima)
                        linhaAcimaDe.append(valorAcimaDe)

						$('#tbody-escalonado').append(linhaAcimaDe);
						
						 }

                           $.each($('.valorHon'), function () {
                                var valorLinha = parseFloat($(this).text().replace("R", "").replace("$", ".").replace(/\./g, '').replace(",", ".")).toFixed(2);
                                console.log(valorLinha)
                                total = Number(parseFloat(total).toFixed(2)) + Number(parseFloat(valorLinha).toFixed(2))
                                console.log(total)
                            })
                            if (Number.isInteger(total)) {
                                total = total + ".00"
                            }

							$.each($('.valorAplicadoAcimaDeClass'), function () {
	                                var valorLinha = parseFloat($(this).text().replace("R", "").replace("$", ".").replace(/\./g, '').replace(",", ".")).toFixed(2);
	                                console.log(valorLinha)
	                                totalAplicado = Number(parseFloat(totalAplicado).toFixed(2)) + Number(parseFloat(valorLinha).toFixed(2))
	                                console.log(totalAplicado)
	                            })

						var trTotal = $("<tr></tr>")
						var vazio1 = $("<td> </td>")
						var vazio2 = $("<td><b>R$ " + mascaraValor(totalAplicado) + "</b></td>")
						var vazio3 = $("<td> </td>")
						
						trTotal.append(vazio1)
						trTotal.append(vazio2)
						trTotal.append(vazio3)
						
						if(total>limitacao){
							if (limitacao > 0) {
								var resultadoSomaLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+" (Limitado R$ "+mascaraValor(limitacao)+")</b></td>")
								trTotal.append(resultadoSomaLimit)
								$("#valorTotal").val(mascaraValor(limitacao))
                        		$("#valorTotal").focusin();
								$("#totalAFaturar").text("R$ " + mascaraValor(parseFloat(limitacao).toFixed(2)))
							} else {
								var resultadoSomaLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+"</b></td>")
								trTotal.append(resultadoSomaLimit)
								$("#valorTotal").val(mascaraValor(limitacao))
                        		$("#valorTotal").focusin();
								$("#totalAFaturar").text("R$ " + mascaraValor(parseFloat(limitacao).toFixed(2)))
							}
						} else {
							var resultadoSomaNoLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+"</b></td>")	
							trTotal.append(resultadoSomaNoLimit)
							$("#valorTotal").val(mascaraValor(parseFloat(total).toFixed(2)))
                        	$("#valorTotal").focusin();
							$("#totalAFaturar").text("R$ " + mascaraValor(parseFloat(total).toFixed(2)))
						}
						
						$('#tbody-escalonado').append(trTotal);

                       
                }
            }
        }
    })
}

function consolidado(){
		 idFaturamento = $("#idFaturamento").text();
	
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
			if(faturamento.consolidadoPorFat == true){
				console.log("entrou no consolidadoPorFat == true")
				simularConsolidacaoFaturamento();
			}
			
			if(faturamento.consolidado == true){
				console.log("entrou no consolidado == true")
				simularConsolidacao();
			}
		}
	})
}

function simularConsolidacaoFaturamento(){
		console.log("entrou no simularConsolidacaoFaturamento")
	 idFaturamento = $("#idFaturamento").text();
	
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
	ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
	
	console.log("fatsTeste",fats);
	
	url = "/faturamentos/simularConsolidacaoFaturamento/";
	console.log("teste antes ajax");
	
    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
			console.log("entrou no ajax");
            console.log(response)
			
			console.log("teste cnpj",response.faturamentoConsolidadoByFat.cliente.cnpj)
			console.log("teste razaoSocialFat",response.faturamentoConsolidadoByFat.cliente.razaoSocial)
	
		$('#fatConsolidado').show();
		
		$("#cnpjConsolidado").val(response.faturamentoConsolidadoByFat.cliente.cnpj)
        $("#cnpjConsolidado").mask('00.000.000/0000-00', {
                    reverse: true
                });
       $("#cnpjConsolidado").focusin();
       $("#idClienteConsolidado").val(response.faturamentoConsolidadoByFat.cliente.id)
	   $("#razaoSocialConsolidado").val(response.faturamentoConsolidadoByFat.cliente.razaoSocial)
       $("#razaoSocialConsolidado").focusin();
       $("#idProdutoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.id)
       $("#produtoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.nome)
       $("#produtoConsolidado").focusin();
  	   $("#campanhaConsolidado").val(response.faturamentoConsolidadoByFat.producao.ano)
       $("#campanhaConsolidado").focusin();
       
       $("#idProducaoConsolidado").val(response.faturamentoConsolidadoByFat.producao.id)
       $("#idsFatsConsolidado").val(response.faturamentoConsolidadoByFat.idsFaturamentosConsolidados)
       
      var divInfomacaoConsolidacao = $("<div'></div>")
	  
 	  var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
                    + '</tr>'
                    + '</thead>')
                    
        var tableBodyFatConsolidados = $('<tbody id="tbody-informacoesFatConsolidados"></tbody>')
        
		for (var i = 0; i < response.fatsConsolidadosIds.length; i++) {
                    var fatConsolidado = response.fatsConsolidadosIds[i];
					console.log("fatConsolidado",fatConsolidado)
                    var valorEtapa = fatConsolidado.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatConsolidado.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatConsolidado.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatConsolidado.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatConsolidado.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatConsolidado.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatConsolidado.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(valorEtapa) + '</td>')

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)

                    tableBodyFatConsolidados.append(tr)

                }
               
                 var trTotal = $('<tr></tr>')

                var valorEtapaConsolidado = response.faturamentoConsolidadoByFat.valorEtapa
				console.log("valorEtapaconsolidado",valorEtapaConsolidado)
				
                if (Number.isInteger(valorEtapaConsolidado)) {
                    valorEtapaConsolidado = valorEtapaConsolidado + ".00"
                }
                
                $("#valorTotalConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorTotalConsolidado").focusin();
       			
       				
       			$("#valorEtapaConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorEtapaConsolidado").focusin();
       			
                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                
                
                var total = $('<td><b>Total: R$' + mascaraValor(valorEtapaConsolidado) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)
                trTotal.append(total)

                tableBodyFatConsolidados.append(trTotal)
                
                
			table.append(tableBodyFatConsolidados)
            divInfomacaoConsolidacao.append(table)
             
            $("#totalAFaturar").append("R$ " + mascaraValor(valorEtapaConsolidado)) 
  			$("#corpoFatConsolidados").append(divInfomacaoConsolidacao)

       
       
		}
	})
   
   }
	
	
	})
			
}

function teste(){
	idNotaFiscal = $("#idNotaFiscal").val()
	console.log("idNotaFiscal",idNotaFiscal);
}
