$(function() {
	$('.collapsible').collapsible();
	$('.modal').modal();
	$(".select").material_select();
	filtroTabela($('#txt-pesq-notaFiscal'));
	filtroTabela2($('#txt-pesq-notaFiscal'));
	maskValor();
	$('#select-etapa').attr("disabled", 'disabled');
	$('#select-etapa').material_select();
//	$('#btn-enviadoAoCliente').click();
	filtro();
	 
	$("#tabela-fat").DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			},
			"select": {
				"rows": {
					"_": "Selecionado %d linhas",
					"0": "Nenhuma linha selecionada",
					"1": "Selecionado 1 linha"
				}
			}
		},
		bFilter: false,
		iDisplayLength: 1000,
		columnDefs: [
			{
				targets: [0, 1, 2],
				className: 'mdl-data-table__cell--non-numeric'
			}
		]
	});
	
	$("#tabela-notasFiscais").DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			},
			"select": {
				"rows": {
					"_": "Selecionado %d linhas",
					"0": "Nenhuma linha selecionada",
					"1": "Selecionado 1 linha"
				}
			}
		},
		bFilter: false,
		iDisplayLength: 1000,
		columnDefs: [
			{
				targets: [0, 1, 2],
				className: 'mdl-data-table__cell--non-numeric'
			}
		]
	});
	
})

function detailsFaturamento(fatId){

	var url = "/faturamentos/" + fatId + "/fat.json"
	
	console.log("faturamento Id ",fatId);
	
	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			
			console.log("data",data);
			console.log("data",data.consolidado);
			
			if(data.consolidado == true || data.consolidadoPorFat == true){
				 window.location.href = "/faturamentos/detailsFaturamentoConsolidado/"+ fatId;
			}
			else{
				window.location.href = "/faturamentos/details/"+fatId;
			}
		}
	})		
}


$(function verificaAoEntrarNaPagina(){
	
	//console.log( 'devo alterar a div principal?' + alterDiv)
	var queryTriggerNotas = document.getElementById("queryTriggerNotas");
	
	qt = queryTriggerNotas.value
	console.log("resultado do qt  " + qt)
	
	qt = null;
	
	if(qt != 'null'){
	
		console.log('entrou no if')

	}
})

function analyzeClicks(){
	//var alterDiv
	//console.log(  + alterDiv)
	var SearchByNotas = document.getElementById("btnNotas");
	SearchByNotas.addEventListener("click", carregaDivNotas());
}


function carregaDivNotas(){
	
	console.log('iniciar método para alterar div principal')
	
	/*var queryTriggerNotas = document.getElementById("queryTriggerNotas").value;
	var queryTriggerNotas = document.getElementById("queryTriggerNotas");
	queryTriggerNotas.addEventListener("click", console.log('clicou'));
	
	qt = queryTriggerNotas.value
	console.log("resultado do qt  " + qt)
	
	console.log(' fora do if')
	console.log(queryTriggerNotas)
	if(qt != 'null'){
	
		console.log('entrou no if')
		

	}*/
	
}

$('#selectProduto').change(
		function() {

			var prodId = $('#selectProduto :selected').val();
			console.log(prodId)

			if (prodId == "") {

				$('#select-etapa').attr("readonly", 'readonly');

				$('#select-etapa').find('option').remove().end().append(
						'<option value="" selected="selected">Selecione</option>').val(
						'whatever');

				$('#select-etapa').material_select();

			} else {

				var url = "/produtos/" + prodId + "/etapas.json"

				var options = "<option selected='selected' value=''>Selecione</option>";

				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {

						$(data).each(
								function(i) {
									var etapa = data[i];
									options += "<option value='" + etapa.id
											+ "'>" + etapa.nome + "</option>"
								});

						$("#select-etapa").empty();
						$("#select-etapa").append(options);
						$('#select-etapa').removeAttr('disabled');
						$("#select-etapa").material_select();

					}
				});
			}
		});

$("#test-swipe-2")

function filtroTabela(idInput) {
    $(idInput).on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-fat').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(1)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}
function filtroTabela2(idInput) {
    $(idInput).on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-notasFiscais').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(1)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}

function maskValor() {
	$(".valor").each(function() {
		var texto = $(this).text();
		$(this).text('R$'+mascaraValor(texto));
	})
}

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g, "");
    valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
    return valor
}

//$("#btn").click(function() {
//  alert( "Handler for .click() called." );
//	$("#txt-pesq-notaFiscal").val(" ");
//});
//
//$("#test-swipe-2").click(function() {
//  alert( "Handler for .click() called." );
//});
//
//function apagaInput(){
//	$("#txt-pesq-notaFiscal").val("");
//		console.log("metodo apagaInput on!")
//}

$('.btnRetornar').click(function() {
	$("#fatIdHolder").val($(this).attr("id"))
	console.log($(this).attr("id"))
})

$('.btnGerar').click(
		function() {
			
			limpaModal();
			$("#divCompleta").empty()
			$("#divContatoCliente").empty()
			$('#corpoFats').hide();
			$("#memoriaCalculoConsolidado").hide();
			$("#corpoFatConsolidados").hide();
			
			var fatIdRaw = $(this).attr("id")
			var fatId = fatIdRaw.substr(fatIdRaw.indexOf("a") + 1)
			$("#fatIdHolderNF").val(fatId)

			var url = "/faturamentos/" + fatId + "/fat.json"

			return $.ajax({
				type : 'GET',
				url : url,
				async : false,
				success : function(data) {
					
					if((data.consolidado == false && data.consolidadoPorFat == false )||( data.consolidadoPorFat == null && data.consolidado == false)){
						
					$("#tabBalancete").show();
					$("#razaoSocial").val(data.cliente.razaoSocial)
					$("#razaoSocial").focusin()
					try{
					$("#versaoBalancete").val(data.balancete != null ? data.balancete.versao : "")
					$("#versaoBalancete").focusin()
					$("#reducaoImposto").val(data.balancete != null ? data.balancete.reducaoImposto : "")
					$("#reducaoImposto").focusin()
					}catch(e){
					}
					$("#apuracaoProducao").val(data.producao != null ? data.producao.tipoDeApuracao : "")
					$("#apuracaoProducao").focusin()
					
					
					var cnpj = data.cliente.cnpj
					cnpj = cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,
							"\$1.\$2.\$3\/\$4\-\$5");

					$("#cnpj").val(cnpj)
					$("#cnpj").focusin()
					
					
					$("#idCliente").val(data.cliente.id);
					
					$("#produto").val(data.produto.nome)
					$("#produto").focusin()

					try {
						console.log("entrou no try")
						$("#filial option").each(function() {
							if ($(this).text() == data.producao.filial.nome) {
								console.log("entrou no if e os dados são: $(this).text()" + $(this).text() + " ----- e " + data.producao.filial.nome)
								$(this).attr("selected", "selected");
							}
						});
					} catch(e) {
						console.log("Não possui filial")
					}
					
					try {
						$("#filial").material_select();
					} catch {
						console.log("Setando valor vazio")
					}

					
					$("#categoriaProblematica").val(
							data.cliente.categoriaProblematica)
					$("#categoriaProblematica").focusin()

					
					informacaoContato()
					console.log("TESTE")

					var today = new Date();
					var dd = String(today.getDate()).padStart(2, '0');
					var mm = String(today.getMonth() + 1).padStart(2, '0');
					var yyyy = today.getFullYear();

					today = dd + '/' + mm + '/' + yyyy;

					$("#dataEmissao").val(today)
					$("#dataEmissao").focusin()

					try{
						$("#campanha option").each(function() {

						if ($(this).val() == data.producao.ano) {
							$(this).attr("selected", "selected");
						}
					});
					}catch(e){
						
					}
					try{
					$("#campanha").material_select();
					}catch{
						
					}
					
					
					var valorFat = $("#valorFat").val()
					valorFat = data.valorEtapa.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							})
					valorFat = valorFat.substr(3)
					$("#valorFat").val(valorFat)		

					//$("#valorFat").val(
					//		data.valorEtapa.toLocaleString("pt-BR", {
					//			style : "currency",
					//			currency : "BRL"
					//		}));
					$("#valorFat").focusin()

					try{
					$("#formaPagamento").val(data.contrato.formaPagamento)
					}catch(e){
						
					}
					$("#formaPagamento").focusin()

					var valorBruto = parseFloat(data.valorEtapa)
					var IRRF = valorBruto * (1.5 / 100)
					var CSSL = valorBruto * (1 / 100)
					var PIS = valorBruto * (0.65 / 100)
					var COFINS = valorBruto * (3 / 100)
					
					if(Number(IRRF) < 10.0) {
						IRRF = 0.0;
					}
	
					if((Number(CSSL) + Number(PIS) + Number(COFINS)) < 10.0) {
						CSSL = 0.0;
						PIS = 0.0;
						COFINS = 0.0;
					}

					var valorLiquido = valorBruto
							- (IRRF + CSSL + PIS + COFINS)
							
					var cobrar = $("#cobrar").val()
					console.log('o valor antes de formatar é: ', valorLiquido)		
					cobrar = valorLiquido.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							});
					cobrar = cobrar.substr(3)
					console.log('o valor após formatar é: ', cobrar)
					
				//	$("#cobrar").val(
				//			valorLiquido.toLocaleString("pt-BR", {
				//				style : "currency",
				//				currency : "BRL"
				//			}));
							$("#cobrar").val(cobrar)
					$("#cobrar").focusin();


					$("#IRRF").val(IRRF.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#IRRF").focusin();
					$("#CSSL").val(CSSL.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#CSSL").focusin();
					$("#PIS").val(PIS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PIS").focusin();
					$("#COFINS").val(COFINS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#COFINS").focusin();

					var PISdevido = valorBruto * (1.65 / 100)
					var COFINSdevido = valorBruto * (7.6 / 100)
					var ISSdevido = valorBruto * (5 / 100)

					$("#PISdevido").val(PISdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PISdevido").focusin();
					$("#COFINSdevido").val(
							COFINSdevido.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							}))
					$("#COFINSdevido").focusin();
					$("#ISSdevido").val(ISSdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#ISSdevido").focusin();

					$("#percentVariavel").val(0)
					$("#percentVariavel").focusin()
					
					var obs = 'Observações do Contrato: '
							+ data.contrato.obsContrato + "\n" + "\n"
							+ 'Observações do Faturamento: ' + data.observacao

					$("#obs").val(obs)
					$('#obs').trigger('autoresize');
					$("#obs").focusin()

					var i = 0
					var divCompleta = "<h5>Cálculo de Faturamento F.I.</h5> <hr />";
					$("#divCompleta").append(divCompleta)
					console.log(i, "i antes do for")

					for(i; i < data.contrato.honorarios.length; i++) {
						var tipoDeHonorario;
						var valorBeneficio = (data.balancete.reducaoImposto).replace(/\./g, '').replace(",",".");
						console.log("valorBeneficio var",valorBeneficio)
						
						console.log("depois eu tiro ", data.contrato.honorarios[i].nome, "e o i é: ", i)

						switch (data.contrato.honorarios[i].nome) {

						case "Percentual Escalonado":
						tipoDeHonorario = "PERCENTUAL ESCALONADO";
						var faixas = data.contrato.honorarios[i].faixasEscalonamento;
						var index = 0
						var temp = valorBeneficio;
						var valorCalc = 0;
						$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
						var cabecalhoTabela = $("<table class='responsive-table table-default bordered'><thead><tr><th>Intervalo de Faturamento</th><th>Valor Aplicado</th><th>Percentual %</th><th>Honorários</th></tr></thead>")

						for(var index in faixas) {

						var inicial = faixas[index].valorInicial;
						inicial = Number(Math.floor(inicial, -1).toFixed(2))
						inicial = parseFloat(inicial).toFixed(2)
						var finalF = faixas[index].valorFinal;
						finalF = Number(Math.floor(finalF, -1).toFixed(2))
						finalF = parseFloat(finalF).toFixed(2)
						var gap1 = finalF - inicial;
						var percentual = faixas[index].valorPercentual / 100;
						var valorAplicado = 0;

						if (temp > 0) {

							if (temp >= gap1) {
								valorCalc = (gap1 * percentual);
								temp = temp - (gap1);
								valorAplicado = gap1;
								console.log("if 1 valor aplicado atual " + valorAplicado)
							} else if (temp < gap1) {
									console.log("Será calculado dentro dentro da faixa com o valor restante do temp...");
									valorCalc = (temp * percentual);
									valorAplicado = temp;
									valorAplicado = parseFloat(valorAplicado)

									console.log("depois de setar o valor aplicado no else if 2   " + valorAplicado)
									temp = temp - (gap1);
									console.log("else if 2 valor aplicado atual " + valorAplicado)
								}
						} else {
							valorCalc = 0;
							valorAplicado = 0;
						}
						
						var tr = $("<tr>")
						var tds = $("<td>De R$ "+mascaraValor(inicial.toFixed(2))+" à R$ "+mascaraValor(finalF.toFixed(2))+"</td>" +
						"<td>R$ "+mascaraValor(valorAplicado.toFixed(2))+"</td>"+
						"<td>"+faixas[index].valorPercentual+"%</td>"+
						"<td>R$ "+mascaraValor(valorCalc.toFixed(2))+"</td>")
						tr.append(tds)

						index++
						cabecalhoTabela.append(tr)
						}
				
						$("#divCompleta").append(cabecalhoTabela)
				
						if(temp != 0) {
							var valorAcimaDe = Number(data.contrato.honorarios[i].acimaDe)
							var percentualAcimaDe = Number((data.contrato.honorarios[i].percentAcimaDe) / 100);
							var honorarioAcimaDe = Number(percentualAcimaDe) * Number(temp);

							var limitacao = Number(data.contrato.honorarios[i].limitacao);
							console.log(data.contrato.honorarios[i].limitacao);

							if (limitacao != 0) {
								var trAcimaDe = $("<tr>")
								var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
								"<td>R$ "+mascaraValor(temp.toFixed(2))+"</td>"+
								"<td>"+percentualAcimaDe*100+"%</td>"+
								"<td>R$ "+mascaraValor(limitacao.toFixed(2))+"</td>")
								trAcimaDe.append(tdsAcimaDe)
								cabecalhoTabela.append(trAcimaDe)
								$("#divCompleta").append(cabecalhoTabela)
							} else if (temp > 0) {
								var trAcimaDe = $("<tr>")
								var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
								"<td>R$ "+mascaraValor(temp.toFixed(2))+"</td>"+
								"<td>"+percentualAcimaDe*100+"%</td>"+
								"<td>R$ "+mascaraValor(honorarioAcimaDe.toFixed(2))+"</td>")
								trAcimaDe.append(tdsAcimaDe)
								cabecalhoTabela.append(trAcimaDe)
								$("#divCompleta").append(cabecalhoTabela)
							} else if (temp <= 0) {
								var trAcimaDe = $("<tr>")
								var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
								"<td>R$ 0,00</td>"+
								"<td>"+percentualAcimaDe*100+"%</td>"+
								"<td>R$ 0,00</td>")
								trAcimaDe.append(tdsAcimaDe)
								cabecalhoTabela.append(trAcimaDe)
								$("#divCompleta").append(cabecalhoTabela)
							}
						}

						break;

						case "Valor Fixo":
						tipoDeHonorario = "VALOR FIXO";
						var valorFixo = Number(data.contrato.honorarios[i].valor);
						$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
						$("#divCompleta").append("<b>Valor Fixo: </b> <p style='display:inline'>R$ "+mascaraValor(valorFixo.toFixed(2))+"</p><br /><hr />")
						break;

						case "Percentual Fixo":
						tipoDeHonorario = "PERCENTUAL FIXO";
						var percentualFixo = Number(data.contrato.honorarios[i].valorPercentual);
						var calculoPercentualFixo = (percentualFixo/100) * Number(valorBeneficio)

						$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
						$("#divCompleta").append("<b>Porcentagem Fixa: </b> <p style='display:inline'>"+percentualFixo+"%</p><br />")
						$("#divCompleta").append("<b>Valor Aplicado: </b> <p style='display:inline'>R$ "+mascaraValor(valorBeneficio)+"</p><br />")
						$("#divCompleta").append("<b>Valor: </b> <p style='display:inline'>R$ "+mascaraValor(calculoPercentualFixo.toFixed(2))+"</p><br /><hr />")
						break;
				
						default:
						$("#nfTipoDeHonorario").text("Sem Memória de Cálculo")
						break;
						}

						var value1 = data.contrato.honorarios[i].nome; // this value1 you can assign it into your textbox
						console.log(value1," nome dele")
					};
					
				}
				
						if(data.consolidado == false &&  data.consolidadoPorFat == true){
					
						$("#tabBalancete").hide();
						$("#tbody-escalonado").empty()
						
						$('#corpoFats').hide();
						$("#memoriaCalculoConsolidado").hide();
						$("#corpoFatConsolidados").show();
						$("#corpoFatConsolidados").empty();
						var titulo = "<h5>Informações da Consolidação</h5><hr/>";
						$("#corpoFatConsolidados").append(titulo)
						simularConsolidacaoFaturamento(fatId);
						 
						
						
						$("#razaoSocial").val(data.cliente.razaoSocial)
						$("#razaoSocial").focusin()
					
					var cnpj = data.cliente.cnpj
					cnpj = cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,
							"\$1.\$2.\$3\/\$4\-\$5");

					$("#cnpj").val(cnpj)
					$("#cnpj").focusin()
					
					
					$("#idCliente").val(data.cliente.id);
					
					$("#produto").val(data.produto.nome)
					$("#produto").focusin()
					
//					$("#filial").val(data.producao.id)
//					$("#filial").focusin()
					
					$("#categoriaProblematica").val(
							data.cliente.categoriaProblematica)
					$("#categoriaProblematica").focusin()

					
					informacaoContato()

					var today = new Date();
					var dd = String(today.getDate()).padStart(2, '0');
					var mm = String(today.getMonth() + 1).padStart(2, '0');
					var yyyy = today.getFullYear();

					today = dd + '/' + mm + '/' + yyyy;

					$("#dataEmissao").val(today)
					$("#dataEmissao").focusin()

					$("#campanha option").each(function() {
						if ($(this).val() == data.producao.ano) {
							$(this).attr("selected", "selected");
						}
					});
					$("#campanha").material_select();

					var valorFatu = mascaraValor(parseFloat(data.valorEtapa).toFixed(2))
					$("#valorFat").val(valorFatu);
					$("#valorFat").focusin()

					$("#formaPagamento").val(data.contrato.formaPagamento)
					$("#formaPagamento").focusin()

					var valorBruto = parseFloat(data.valorEtapa)
					var IRRF = valorBruto * (1.5 / 100)
					var CSSL = valorBruto * (1 / 100)
					var PIS = valorBruto * (0.65 / 100)
					var COFINS = valorBruto * (3 / 100)

					var valorLiquido = valorBruto
							- (IRRF + CSSL + PIS + COFINS)

					var valorCobrar = mascaraValor(parseFloat(valorLiquido).toFixed(2))
					$("#cobrar").val(valorCobrar);
					$("#cobrar").focusin();
					

					$("#IRRF").val(IRRF.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#IRRF").focusin();
					$("#CSSL").val(CSSL.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#CSSL").focusin();
					$("#PIS").val(PIS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PIS").focusin();
					$("#COFINS").val(COFINS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#COFINS").focusin();

					var PISdevido = valorBruto * (1.65 / 100)
					var COFINSdevido = valorBruto * (7.6 / 100)
					var ISSdevido = valorBruto * (5 / 100)

					$("#PISdevido").val(PISdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PISdevido").focusin();
					$("#COFINSdevido").val(
							COFINSdevido.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							}))
					$("#COFINSdevido").focusin();
					$("#ISSdevido").val(ISSdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#ISSdevido").focusin();

					$("#percentVariavel").val(0)
					$("#percentVariavel").focusin()
					
					var obs = 'Observações do Contrato: '
							+ data.contrato.obsContrato + "\n" + "\n"
							+ 'Observações do Faturamento: ' + data.observacao

					$("#obs").val(obs)
					$('#obs').trigger('autoresize');
					$("#obs").focusin()

					var i = 0
				}
				
				
				
					if(data.consolidado == true && data.consolidadoPorFat == false){
						
						$("#tabBalancete").hide();
						$("#tbody-escalonado").empty()
						$("#corpoFatConsolidados").hide()
						$('#corpoFats').show();
						$("#memoriaCalculoConsolidado").show();
						simularConsolidacao(fatId);
						
						teste=$('#corpoFats')
						console.log("'#teste'",teste);
						
						$("#razaoSocial").val(data.cliente.razaoSocial)
						$("#razaoSocial").focusin()
					
					var cnpj = data.cliente.cnpj
					cnpj = cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,
							"\$1.\$2.\$3\/\$4\-\$5");

					$("#cnpj").val(cnpj)
					$("#cnpj").focusin()
					
					
					$("#idCliente").val(data.cliente.id);
					
					$("#produto").val(data.produto.nome)
					$("#produto").focusin()
					
//					$("#filial").val(data.producao.id)
//					$("#filial").focusin()

					$("#categoriaProblematica").val(
							data.cliente.categoriaProblematica)
					$("#categoriaProblematica").focusin()

					
					informacaoContato()

					var today = new Date();
					var dd = String(today.getDate()).padStart(2, '0');
					var mm = String(today.getMonth() + 1).padStart(2, '0');
					var yyyy = today.getFullYear();

					today = dd + '/' + mm + '/' + yyyy;

					$("#dataEmissao").val(today)
					$("#dataEmissao").focusin()

					$("#campanha option").each(function() {
						if ($(this).val() == data.producao.ano) {
							$(this).attr("selected", "selected");
						}
					});
					$("#campanha").material_select();
					
					var valorFatu = mascaraValor(parseFloat(data.valorEtapa).toFixed(2))
					$("#valorFat").val(valorFatu);
					$("#valorFat").focusin()

					$("#formaPagamento").val(data.contrato.formaPagamento)
					$("#formaPagamento").focusin()

					var valorBruto = parseFloat(data.valorEtapa)
					var IRRF = valorBruto * (1.5 / 100)
					var CSSL = valorBruto * (1 / 100)
					var PIS = valorBruto * (0.65 / 100)
					var COFINS = valorBruto * (3 / 100)

					var valorLiquido = valorBruto
							- (IRRF + CSSL + PIS + COFINS)
							console.log("valorLiquido",valorLiquido);
					//arrumar aqui
					var valorCobrar = mascaraValor(parseFloat(valorLiquido).toFixed(2))
					$("#cobrar").val(valorCobrar);
					$("#cobrar").focusin();

					$("#IRRF").val(IRRF.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#IRRF").focusin();
					$("#CSSL").val(CSSL.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#CSSL").focusin();
					$("#PIS").val(PIS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PIS").focusin();
					$("#COFINS").val(COFINS.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#COFINS").focusin();

					var PISdevido = valorBruto * (1.65 / 100)
					var COFINSdevido = valorBruto * (7.6 / 100)
					var ISSdevido = valorBruto * (5 / 100)

					$("#PISdevido").val(PISdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#PISdevido").focusin();
					$("#COFINSdevido").val(
							COFINSdevido.toLocaleString("pt-BR", {
								style : "currency",
								currency : "BRL"
							}))
					$("#COFINSdevido").focusin();
					$("#ISSdevido").val(ISSdevido.toLocaleString("pt-BR", {
						style : "currency",
						currency : "BRL"
					}))
					$("#ISSdevido").focusin();

					$("#percentVariavel").val(0)
					$("#percentVariavel").focusin()
					
					var obs = 'Observações do Contrato: '
							+ data.contrato.obsContrato + "\n" + "\n"
							+ 'Observações do Faturamento: ' + data.observacao

					$("#obs").val(obs)
					$('#obs').trigger('autoresize');
					$("#obs").focusin()

					var i = 0
					}
				
				}
			})
		})

function handleChange(input) {
	if (input.value < 0)
		input.value = 0;
	if (input.value > 100)
		input.value = 100;
}

$('#retorna').click(
		function() {

			var fatIdRaw = $('#fatIdHolder').val()
			var fatId = fatIdRaw.substr(fatIdRaw.indexOf("a") + 1)

			console.log(fatId)

			var fat = {
				id : fatId
			}

			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/faturamentos/retorna",
				data : JSON.stringify(fat),
				dataType : "JSON",
				statusCode : {
					200 : function() {
						Materialize.toast('Faturamento retornado ao estoque!',
								5000, 'rounded');
						$("#lihha" + fatId).hide();
						$('#voltarEstoque').modal('close');
					},
					500 : function() {
						Materialize.toast('Ops, houve um erro interno', 5000,
								'rounded');
					},
					400 : function() {
						Materialize.toast(
								'Você deve estar fazendo algo de errado', 5000,
								'rounded');
					},
					404 : function() {
						Materialize
								.toast('Url não encontrada', 5000, 'rounded');
					}
				}
			});

		})

$("#gerar").click(
		function() {

			var fatIdRaw = $("#fatIdHolderNF").val()
			var fatId = fatIdRaw.substr(fatIdRaw.indexOf("a") + 1)

			console.log(fatId)

			var fat = {
				id : fatId
			}

			var valorCobrar=  $("#cobrar").val();
			console.log("valorCobrar",valorCobrar);

			var valorFaturar = $("#valorFat").val()
			
			var valorCobranca = $("#valorCobrança").val();

			var duplaTributacao = $("#duplaTributacao :selected").val();

			if (duplaTributacao == "Sim") {
				duplaTributacao = true;
			} else {
				duplaTributacao = false;
			}
			
			var filial = {
				id : $("#filial :selected").val()
			}

			var nf = {
				faturamento : fat,
				tipoNF : $("#tipoNF :selected").val(),
				categoriaNF : $("#categoriaNF :selected").val(),
				dataEmissao : $("#dataEmissao").val(),
				dataVencimento : $("#vencimento").val(),
				dataCobranca : $("#cobrança").val(),
				numeroNF : $("#numeroNota").val(),
				faturar : valorFaturar,
				cobrar : valorCobrar,
				valorCobranca : valorCobranca,
				obsNota : $("#obs").val(),
				parcela : $("#parcela").val(),
				duplaTributacao : duplaTributacao,
				percentVariavel : $("#percentVariavel").val(),
				ISSvariavel : $("#ISSVariavel").val(),
				filial : filial
			}

			console.log(nf)

			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/notasFiscais/geraNF",
				data : JSON.stringify(nf),
				dataType : "JSON",
				statusCode : {
					200 : function() {
						Materialize.toast('Nota Fiscal atualizada com sucesso!',
								5000, 'rounded');
						atualizarEmissaoFaturamento(fatId);
					},
					500 : function() {
						console.log("nf",nf)
						Materialize.toast('Ops, houve um erro interno', 5000,
								'rounded');
					},
					400 : function() {
						Materialize.toast(
								'Você deve estar fazendo algo de errado', 5000,
								'rounded');
					},
					404 : function() {
						Materialize
								.toast('Url não encontrada', 5000, 'rounded');
					}
				}
			});

		})
		
function atualizarEmissaoFaturamento(fatId) {
	console.log(fatId, " function fatid")
	
	var fat = {
		id : fatId
	};
	
	$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/faturamentos/updateEmissaoFat",
				data : JSON.stringify(fat),
				dataType : "JSON",
				statusCode : {
					200 : function() {
						location.reload();
					}
					}
	})
}

function limpaModal() {
	console.log("entrou na limpa")
	$("#razaoSocial").text("")
	$("#cnpj").text("")
	$("#produto").text("")
	$("#categoriaProblematica").text("")
	$("#dataEmissao").text("")
//	$("#campanha option").each(function() {
//		if ($(this).val() == data.producao.ano) {
//			$(this).attr("selected", "selected");
//		}
//	});
//	$("#campanha").text("")
	$("#valorFat").text("")
	$("#formaPagamento").text("")
	$("#valorCobrar").text("")
	$("#IRRF").text("")
	$("#CSSL").text("")
	$("#PIS").text("")
	$("#COFINS").text("")
	$("#PISdevido").text("")
	$("#COFINSdevido").text("")
	$("#ISSdevido").text("")
	$("#percentVariavel").text("")
	$("#obs").text("")
	$("#versaoBalancete").val('')
	$("#apuracaoProducao").val('')
	$("#reducaoImposto").val('')		
	//document.getElementById("#versaoBalancete").text('');
	//document.getElementById("#apuracaoProducao").text('');
	
	$('#corpoFats').text("")		
					
}

function email (){
	idCliente = $("#idCliente").val()
	
	
	 $.ajax({
        type: 'GET',
        url: "/clientes/contatos/" + idCliente + "/emailCliente.json" ,
        success: function(email) {
			for(var i = 0; i<email.length; i++){
				console.log("email",email);
				 /*$("#email").val(email[i]);
				 $("#email").focusin()*/
			}
		
		}
	})	
}

function endereco (){
	idCliente = $("#idCliente").val()
	console.log("idCliente",idCliente)
	
	 $.ajax({
        type: 'GET',
        url: "/clientes/" + idCliente + "/enderecoCliente.json" ,
        success: function(endereco) {
			for(var i = 0; i<endereco.length; i++){
				 $("#endereco").val(endereco[i]);
				 $("#endereco").focusin()
			}
		
		}
	})	
}

function informacaoContato(){
	
	idCliente = $("#idCliente").val()
	console.log("idCliente",idCliente);
	
		 $.ajax({
        	type: 'GET',
        	url:"/clientes/contatos/" + idCliente + "/idContatoCliente.json" ,
        	success: function(listIdContato) {
				console.log("entrou no metodos sucess IdContato")
				var titulo = "<h5>Informações Contato</h5> <hr />";
				$("#divContatoCliente").append(titulo)
				for(var i = 0 ; i<listIdContato.length; i++){
						if(listIdContato[i].pessoaNfBoleto == true){
							console.log("listIdContato",listIdContato[i]);
								
								var nome = listIdContato[i].nome;
								var email = listIdContato[i].email;
								var telefone1 = listIdContato[i].telefone1;
								var telefone2 = listIdContato[i].telefone2;
								
								$("#divContatoCliente").append("<b>Nome: </b> <p style='display:inline'>"+nome+"</p><br />");
								$("#divContatoCliente").append("<b>Email:  </b> <p style='display:inline'> "+email+" </p><br />");
								$("#divContatoCliente").append("<b>Telefone: </b> <p style='display:inline'> "+telefone1+" </p><br />");
								$("#divContatoCliente").append("<b>Telefone: </b> <p style='display:inline'> "+telefone2+" </p><br />");
								$("#divContatoCliente").append("<hr></hr><p style='display:inline'> </p><br />");  		
						}
				}
			}
		})	
					$.ajax({
        				type: 'GET',
        				url: "/clientes/" + idCliente + "/enderecoCliente.json" ,
        					success: function(enderecoContatoCliente) {
								for(var i = 0; i<enderecoContatoCliente.length; i++){
									console.log("tamanho vetor endereco",enderecoContatoCliente.length)
									console.log(" vetor endereco",enderecoContatoCliente[i])
									if(enderecoContatoCliente[i].enderecoNotaFiscalBoleto == true){
									
										if (enderecoContatoCliente[i].bairro == "" ){
											var bairro = "";
										}
										else{
											var bairro = enderecoContatoCliente[i].bairro;
										}
										if (enderecoContatoCliente[i].tipo == null){
											var tipo = " ";
										}
										else{
											var tipo = enderecoContatoCliente[i].tipo;
										}
									
										var endereco = enderecoContatoCliente[i].logradouro + ", "+ enderecoContatoCliente[i].numero + "-"+bairro + ", "+enderecoContatoCliente[i].cidade+ "-"+ enderecoContatoCliente[i].estado;
									
									
										$("#divContatoCliente").append("<b>Endereco: </b> <p style='display:inline'>"+endereco+"</p><br />"); 
										$("#divContatoCliente").append("<b>tipo: </b> <p style='display:inline'>"+tipo+"</p><br />"); 
									}
								}
							}
						})	
}

$(document).on("click",".validar",function setEnviadoAoCliente() {
	
	var botao = $(this);
	idBotao = botao.val();
	console.log('o idBotao é: ' +botao)
	
	console.log("entrou na função certa")
	
//amanhã faremos o seguinte:
//arrumar o ajax abaixo para salvar dinâmicamente o valor do checkbopx ao clicar no mesmo._
//o id do botao, já que teremos um por linha, sera o id próprio, mais id do fat selecionado
//exemplo: ''#btn-enviadoAoCliente1120''

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/faturamentos/setEnviado/" + idBotao,
		data: JSON.stringify(idBotao),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Registro de Envio ao Cliente com Sucesso!',
						5000, 'rounded');
						$(botao).attr("disabled", true);
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});	
})


function simularConsolidacao(idFaturamento) {
	
	
	console.log("idFaturamento",idFaturamento);
		
	 var titulo = ("<h5>Informações da Consolidação</h5> <hr/>")
	 $("#corpoFats").append(titulo)
	
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
					ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
		
	
	console.log("fatsTeste",fats);
	
    var url = "/faturamentos/simulaFatConsolidado"
     $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
            console.log(response)
            if (response.erro == "noBalancete") {
                Materialize.toast('Um ou mais faturamentos não possuem balancetes vinculados, não é possível consolidar o benefício', 5000, 'rounded');
            } else if (response.erro == "mesmoBalancete") {
                Materialize.toast('Dois ou mais faturamentos possuem a mesma versão de balancete, não é possível consolidar o benefício.', 5000, 'rounded');
            } else if (response.erro == "mesmaEtapa") {
                Materialize.toast('Dois ou mais faturamentos não possuem a mesma etapa de faturamento, não é possível consolidar o benefício.', 5000, 'rounded');
            } else {
                $('#beneficioConsolidado').show();

                //CARREGA VALORES
                $("#cnpjFat").val(response.faturamentoConsolidado.cliente.cnpj)
                $("#cnpjFat").mask('00.000.000/0000-00', {
                    reverse: true
                });
                $("#cnpjFat").focusin();

                $("#razaoSocialFat").val(response.faturamentoConsolidado.cliente.razaoSocial)
                $("#razaoSocialFat").focusin();
                $("#idCliente").val(response.faturamentoConsolidado.cliente.id)

                $("#idProduto").val(response.faturamentoConsolidado.producao.produto.id)
                $("#produto").val(response.faturamentoConsolidado.producao.produto.nome)
                $("#produto").focusin();

//				$("#filial").val(response.faturamentoConsolidado.producao.filial.id)
//				$("#filial").focusin();

				$("#idProducao").val(response.faturamentoConsolidado.producao.id)

				$("#idEtapa").val(response.faturamentoConsolidado.etapa.id)
                $("#etapa").val(response.faturamentoConsolidado.etapa.nome)
                $("#etapa").focusin();

				$('#observacao').val(response.faturamentoConsolidado.observacao)
				$('#observacao').focusin();

				$("#idsFatsConsolidados").val(response.faturamentoConsolidado.idsFaturamentosConsolidados)

                $("#percentEtapa").val(response.percent)
                $("#percentEtapa").focusin();

                $("#campanha").val(response.faturamentoConsolidado.producao.ano)
                $("#campanha").focusin();

                var valor = response.faturamentoConsolidado.valorEtapa

                if (Number.isInteger(valor)) {
                    valor = valor + ".00"
                }

                $("#valorEtapa").val(mascaraValor(valor))
                $("#valorEtapa").focusin();

                var divFats = $("#fatsConsolidados")


                var divPai = $("<div'></div>")

                var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
                    + '</tr>'
                    + '</thead>')
                var tableBody = $('<tbody id="tbody-informacoes"></tbody>')

                for (var i = 0; i < response.fatsIds.length; i++) {
                    var fatC = response.fatsIds[i];

                    var valorEtapa = fatC.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatC.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatC.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatC.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + mascaraValor(parseFloat(fatC.balancete.valorTotal).toFixed(2)) + '</td>')
                    var reducao = $('<td> R$ ' +mascaraValor(parseFloat(fatC.balancete.reducaoImposto ).toFixed(2)) + '</td>')
                    var etapa = $('<td>' + fatC.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(parseFloat(valorEtapa).toFixed(2)) + '</td>')

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)

                    tableBody.append(tr)

                }

                var trTotal = $('<tr></tr>')

                var beneficioTotal = response.beneficioConsolidado

                if (Number.isInteger(beneficioTotal)) {
                    beneficioTotal = beneficioTotal + ".00"
                }

                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                var total = $('<td><b>Total: R$' +mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(total)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)

                tableBody.append(trTotal)

                table.append(tableBody)
                divPai.append(table)

                $("#corpoFats").append(divPai)

                var idContrato = response.faturamentoConsolidado.contrato.id

                getMemoriaDeCalculo(idContrato, beneficioTotal);

            }
        },
        error: function (xhr) {

        }
    });

}
})
}

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g, "");
    valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
    return valor
}

function getMemoriaDeCalculo(idContrato, beneficioTotal) {

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idContrato + "/honorarios.json",
        async: false,
        success: function (hon) {

            for (var i = 0; i < hon.length; i++) {
                var honorario = hon[i];
                console.log(honorario)

                if (honorario.nome == "Percentual Fixo") {

                    $('#percentFixo').show()

                    var trFixo = $("<tr></tr>");

                    var beneficioFixo = $('<td> R$ ' + mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</td>')
                    var percentualFixo = $('<td>' + honorario.valorPercentual + '%</td>')

                    var percent = (honorario.valorPercentual / 100)
                    var resultado = (beneficioTotal * percent) * 100

                    var valor = $('<td class="resultadoPF">R$ ' + mascaraValor(parseFloat(resultado).toFixed(2)) + '</td>')

                    trFixo.append(beneficioFixo)
                    trFixo.append(percentualFixo)
                    trFixo.append(valor)

                    $('#tbody-percentualFixo').append(trFixo);

                }
                if (honorario.nome == "Percentual Escalonado") {
                    $('#escalonado').show()

                    var temp = beneficioTotal;
                    var valorCalc = 0.00;
                    var total = 0.00;

                    var faixas = honorario.faixasEscalonamento

                    for (var i = 0; i < faixas.length; i++) {

                        var faixa = faixas[i];

                        var linhaFaixa = $("<tr></tr>");

                        var inicio = faixa.valorInicial;
                        var fim = faixa.valorFinal;
                        var beneficioAtribuido = 0.00;

                        if (Number.isInteger(inicio)) {
                            inicio = inicio + ".00"
                        } else {
							inicio = Number(Math.floor(inicio, -1).toFixed(2))
							inicio = parseFloat(inicio).toFixed(2)
						}
						
                        if (Number.isInteger(fim)) {
                            fim = fim + ".00"
                        } else {
							fim = Number(Math.floor(fim, -1).toFixed(2))
							fim = parseFloat(fim).toFixed(2)
						}

                        var gap1 = fim - inicio;
                        if (Number.isInteger(gap1)) {
                            gap1 = gap1 + ".00"
                        }

                        var intervalo = $('<td style="white-space: nowrap;">R$ ' + mascaraValor(inicio) + ' - R$ ' + mascaraValor(fim) + '</td>')
                        var percent = $('<td>' + faixa.valorPercentual + '%</td>')

                        if (temp > 0) {

                            if (temp >= gap1) {
                                valorCalc = (gap1 * (faixa.valorPercentual/100));
                                temp = temp - (gap1);
                                beneficioAtribuido = gap1
                                valorCalc = Math.round(valorCalc);
                            } else if (temp < gap1) {
                                valorCalc = (temp * (faixa.valorPercentual/100));
                                beneficioAtribuido = temp;
                                temp = temp - (gap1);
                            }

                        } else {
                            valorCalc = 0;
                        }

                        var beneficio = $('<td> R$ ' + mascaraValor(parseFloat(beneficioAtribuido).toFixed(2)) + '</td>')
                        var honorarioCalc = $('<td class="valorHon"> R$ ' + mascaraValor(parseFloat(valorCalc).toFixed(2))+ '</td>')

                        linhaFaixa.append(intervalo)
                        linhaFaixa.append(beneficio)
                        linhaFaixa.append(percent)
                        linhaFaixa.append(honorarioCalc)

                        $('#tbody-escalonado').append(linhaFaixa);

                    }

                    if (temp > 0) {

                        var percentualAcima = honorario.percentAcimaDe;
                        var honorarioAcimaDe = parseFloat((percentualAcima / 100) * temp).toFixed(2);

                        var vlAcimaDe = honorario.acimaDe
                        if (Number.isInteger(vlAcimaDe)) {
                            vlAcimaDe = vlAcimaDe + ".00"
                        } else {
							vlAcimaDe = Number(Math.floor(vlAcimaDe, -1).toFixed(2))
							vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
						}

                        var limitacao = honorario.limitacao
                        if (Number.isInteger(limitacao)) {
                            limitacao = limitacao + ".00"
                        } else {
							limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
							limitacao = parseFloat(limitacao).toFixed(2)
						}

                        var linhaAcimaDe = $("<tr></tr>");

                        var acimaDe = $('<td>Acima de: R$ ' + mascaraValor(vlAcimaDe.toFixed(2)) + ' (Limitação: R$ ' + mascaraValor(limitacao) + ')</td>');
                        var percentAcima = $('<td>' + percentualAcima + '%</td>');
                        var beneficioAcima = $('<td> R$ ' + mascaraValor(parseFloat(temp).toFixed(2)) + '</td>');

                        var valorAcimaDe = $('<td class="valorHon"> R$ ' + mascaraValor(honorarioAcimaDe) + '</td>');

                        linhaAcimaDe.append(acimaDe)
                        linhaAcimaDe.append(beneficioAcima)
                        linhaAcimaDe.append(percentAcima)
                        linhaAcimaDe.append(valorAcimaDe)

						$('#tbody-escalonado').append(linhaAcimaDe);
						
						 }

                           $.each($('.valorHon'), function () {
                                var valorLinha = parseFloat($(this).text().replace("R", "").replace("$", ".").replace(/\./g, '').replace(",", ".")).toFixed(2);
                                console.log(valorLinha)
                                total = Number(parseFloat(total).toFixed(2)) + Number(parseFloat(valorLinha).toFixed(2))
                                console.log("total",total)
                            })
                            if (Number.isInteger(total)) {
                                total = total + ".00"
                            }

						var trTotal = $("<tr></tr>")
						var vazio1 = $("<td> </td>")
						var vazio2 = $("<td> </td>")
						var vazio3 = $("<td> </td>")
						
						trTotal.append(vazio1)
						trTotal.append(vazio2)
						trTotal.append(vazio3)
						
						if(total>limitacao){
							var resultadoSomaLimit = $("<td><b>Total R$"+mascaraValor(total)+" (Limitado R$ "+mascaraValor(limitacao)+")</b></td>")
							trTotal.append(resultadoSomaLimit)
							console.log("total bibs 123456",resultadoSomaLimit)
							 $("#valorTotal").val(mascaraValor(limitacao))
                        	 $("#valorTotal").focusin();
						} else {
							var resultadoSomaNoLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+"</b></td>")	
							trTotal.append(resultadoSomaNoLimit)
							console.log("total bibs 123456",total)
							$("#valorTotal").val(mascaraValor(total))
                        	$("#valorTotal").focusin();
						}
						
						$('#tbody-escalonado').append(trTotal);

                       
                }
            }
        }
    })
}


function simularConsolidacaoFaturamento(idFaturamento ){
	
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
	ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
	
	console.log("fatsTeste",fats);
	
	url = "/faturamentos/simularConsolidacaoFaturamento/";
	console.log("teste antes ajax");
	
    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
			console.log("entrou no ajax");
            console.log(response)
			
			console.log("teste cnpj",response.faturamentoConsolidadoByFat.cliente.cnpj)
			console.log("teste razaoSocialFat",response.faturamentoConsolidadoByFat.cliente.razaoSocial)
	
		$('#fatConsolidado').show();
		
		$("#cnpjConsolidado").val(response.faturamentoConsolidadoByFat.cliente.cnpj)
        $("#cnpjConsolidado").mask('00.000.000/0000-00', {
                    reverse: true
                });
       $("#cnpjConsolidado").focusin();
       $("#idClienteConsolidado").val(response.faturamentoConsolidadoByFat.cliente.id)
	   $("#razaoSocialConsolidado").val(response.faturamentoConsolidadoByFat.cliente.razaoSocial)
       $("#razaoSocialConsolidado").focusin();
       $("#idProdutoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.id)
       $("#produtoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.nome)
       $("#produtoConsolidado").focusin();
  	   $("#campanhaConsolidado").val(response.faturamentoConsolidadoByFat.producao.ano)
       $("#campanhaConsolidado").focusin();
       
       $("#idProducaoConsolidado").val(response.faturamentoConsolidadoByFat.producao.id)
       $("#idsFatsConsolidado").val(response.faturamentoConsolidadoByFat.idsFaturamentosConsolidados)
       
      var divInfomacaoConsolidacao = $("<div'></div>")

 	  var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
                    + '</tr>'
                    + '</thead>')
                    
        var tableBodyFatConsolidados = $('<tbody id="tbody-informacoesFatConsolidados"></tbody>')
        
		for (var i = 0; i < response.fatsConsolidadosIds.length; i++) {
                    var fatConsolidado = response.fatsConsolidadosIds[i];
					console.log("fatConsolidado",fatConsolidado)
                    var valorEtapa = fatConsolidado.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatConsolidado.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatConsolidado.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatConsolidado.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatConsolidado.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatConsolidado.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatConsolidado.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(valorEtapa) + '</td>')

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)

                    tableBodyFatConsolidados.append(tr)

                }
               
                 var trTotal = $('<tr></tr>')

                var valorEtapaConsolidado = response.faturamentoConsolidadoByFat.valorEtapa
				console.log("valorEtapaconsolidado",valorEtapaConsolidado)
				
                if (Number.isInteger(valorEtapaConsolidado)) {
                    valorEtapaConsolidado = valorEtapaConsolidado + ".00"
                }
                
                $("#valorTotalConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorTotalConsolidado").focusin();
       			
       				
       			$("#valorEtapaConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorEtapaConsolidado").focusin();
       			
                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                
                
                var total = $('<td><b>Total: R$' + mascaraValor(valorEtapaConsolidado) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)
                trTotal.append(total)

                tableBodyFatConsolidados.append(trTotal)
                
                
			table.append(tableBodyFatConsolidados)
            divInfomacaoConsolidacao.append(table)
  			$("#corpoFatConsolidados").append(divInfomacaoConsolidacao)

		}
	})
   
   }
	})
			
}

function parametrosFiltro(){
	//guarda a url(setItem) e armazena no input ("#currentParametros") 
	//para ser usada no botão voltar das págs details e update (para voltar a list fat liberados  sem perder os filtros)
	
	var url = window.location.href;
	$("#currentParametros").val(url);
	
	var params = $("#currentParametros").val();
	sessionStorage.setItem("params", params);
	
}


