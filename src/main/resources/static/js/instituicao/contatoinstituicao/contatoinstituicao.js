$("#tabela-contato-instituicao").click(exibirSectionTabela);
$("#tabela-contato-instituicao-hidden").click(esconderSectionTabela);
$("#add-contato-instituicao").click(exibeSectionContatoEmpresa);
$("#btn-cad-contato-instituicao").click(addContatoEmpresa);
$("#select-tipo-telefone").change(formataInputTelefone);
$("#fecharModalContato").click(fecharModalContato);

$(function () {

    incluirAContato();
    $("#select-tipo-telefone").material_select();
    $('.modal').modal();

});

function fecharModalContato() {
    $("#modalContatoinstituicao").modal("close");
}

function incluirAContato() {

    $("#add-contato-instituicao").text("+ Adicionar contato à ");

    var linkAddContato = $("#add-contato-instituicao").text();
    var empresaDigitada = $("#nome").val();

    if (empresaDigitada != '') {
        var stringConcatenada = linkAddContato + ' ' + empresaDigitada;
        $("#add-contato-instituicao").text(stringConcatenada);
    } else {
        $("#add-contato-instituicao").text("+ Adicionar contato à ");
    }

}

function exibeSectionContatoEmpresa() {
    $("#contato-instituicao").slideToggle(1200);
}

function formataInputTelefone() {
    var tipoTelefone = $("#select-tipo-telefone option:selected").val();

    if (tipoTelefone != "CELULAR") {
        $("#telefone").mask('(00) 0000-0000');
    } else {
        $("#telefone").mask('(00) 00000-0000');
    }

}

function exibirSectionTabela() {

    var btnExibirContatos = $("#tabela-contato-empresa");
    var btnOcultarContatos = $("#tabela-contato-empresa-hidden");
    var tabelaContatosEmpresa = $("#tabela-contatos");
    var idInstituicao = $("#idinstituicao").val();

    btnExibirContatos.stop().fadeOut(0); // Some
    btnOcultarContatos.stop().fadeIn(0); // Aparece
    tabelaContatosEmpresa.stop().slideToggle(0);

    var url = "http://localhost:9000/instituicoes/contatos/" + idInstituicao + ".json";

    $.get(url, function (data) {
        console.log(data);
        $(data).each(
            function () {
                var linha = novaLinha(this.id, this.nome, this.email,
                    this.cargo, this.telefone.tipoTelefone,
                    this.telefone.numero, this.pessoaNfBoleto,
                    this.referencia, this.pesquisaSatisfacao);
                $("#tbody-contatos").append(linha);
            });
    });

}

function zerarTabela() {
    $("#tabela-contatos #tbody-contatos tr").each(function () {
        $(this).remove();
    });
}

function esconderSectionTabela() {
    $("#tabela-contatos").stop().slideToggle(0);
    $("#tabela-contato-empresa").fadeIn(0);
    $("#tabela-contato-empresa-hidden").fadeOut(0);
    zerarTabela();
}

function novaLinha(id, nome, email, cargo, tipoTelefone, numeroTelefone,
    nfBoleto, referencia, pesqSatisfacao) {

    var icone = $("<i>");
    var linha = $("<tr>");
    var colunaId = $("<td>").text(id);
    colunaId.addClass("hidden");
    var colunaNome = $("<td>").text(nome);
    var colunaEmail = $("<td>").text(email);
    var colunaCargo = $("<td>").text(cargo);
    var colunaNumeroTelefone = $("<td>").text(numeroTelefone);
    var colunaIconeEditar = $("<td>").append(icone);
    var linkEditar = $("<a>").attr("href", "contatos/" + id);
    var colunaIconeView = $("<td>").append(icone);
    var linkVisualizar = $("<a>").attr("onclick",
        "modalContatoEmpresa(" + id + ");");

    var colunaDeletar = $("<td>");

    var formDeletar = $("<form>").attr("action", "contatos/" + $("#idInstituicao").val() + "/delete/" + id).attr("method", "POST");

    var inputSubmit = $("<input>").attr("type", "submit").addClass("btn").addClass("red").addClass("white-text").attr("value", "excluir");

    formDeletar.append(inputSubmit);

    colunaDeletar.append(formDeletar);

    linkVisualizar.addClass("mouse-hand");
    colunaIconeEditar.addClass("material-icons").text("edit");

    linkEditar.append(colunaIconeEditar);
    linkVisualizar.append(colunaIconeView);

    linha.append(colunaId);
    linha.append(colunaNome);
    linha.append(colunaEmail);
    linha.append(colunaCargo);
    linha.append(colunaNumeroTelefone);
    linha.append(linkVisualizar);
    linha.append(linkEditar);
    linha.append(colunaDeletar);

    return linha;

}

function modalContatoEmpresa(id) {

    $.get(
        "contatos/details/" + id + ".json",
        function (contato) {

            $("#nomeContatoModal").text(contato.nome);
            $("#emailContatoModal").text(
                "E-mail : " + contato.email);
            $("#cargoContatoModal")
                .text("Cargo : " + contato.cargo);
            $("#numeroTelefone").text(
                "Telefone : " + contato.telefone.numero);
            $("#modalContatoinstituicao").modal("close");
        }).fail(function () {
        console.log("erro");
    });

}

function addContatoEmpresa() {
    var contato = {};
    var idInstituicao = $("#idinstituicao").val();

    contato["nome"] = $("#contato-nome").val();
    contato["email"] = $("#contato-email").val();
    contato["cargo"] = $("#contato-cargo").val();
    telefone["numero"] = $("#telefone").val();
    contato["telefone"] = telefone;

    var urlMontada = "/instituicoes/contatos/add/" + idInstituicao;

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: urlMontada,
        data: JSON.stringify(contato) + cnpj,
        dataType: 'JSON',
        statusCode: {
            200: function () {
                Materialize.toast($("#contato-nome").val() +
                    ' Cadastrado com sucesso !', 5000, 'rounded');
                exibirSectionTabela();
                exibeSectionContatoEmpresa();
            },
            500: function () {
                Materialize
                    .toast('Ops, houve um erro interno', 5000, 'rounded');
            },
            400: function () {
                Materialize.toast('Você deve estar fazendo algo de errado',
                    5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada', 5000, 'rounded');
            }
        }
    });
}
