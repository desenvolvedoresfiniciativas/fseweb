$("#cep").blur(buscarEndereco);

$(function () {
    //carrega nada por enquanto
});


function limparFormEndereco() {
    $("#rua").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#estado").val("");
}

function buscarEndereco() {

    $(".progress").show();

    var cep = $(this).val().replace(/\D/g, "");

    if (cep != "") {

        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

            if (dados.erro != true) {

                $("#rua").val(dados.logradouro);
                $("#bairro").val(dados.bairro);
                $("#cidade").val(dados.localidade);
                $("#estado").val(dados.uf);

                $("#rua").focusin();

                $("#bairro").focusin();

                $("#cidade").focusin();
                $("#cidade").prop("readonly", true);

                $("#estado").focusin();
                $("#estado").prop("readonly", true);

            } else {
                Materialize.toast('CEP não encontrado', 5000, 'rounded');
                limparFormEndereco();
            }

        });

    }

    $(".progress").hide();
}
