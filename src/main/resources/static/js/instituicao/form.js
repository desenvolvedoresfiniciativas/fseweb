$("#cnpj").on("input", formatarCnpj);
$("#cnpj").on("input", validarCnpj);
$("#cnpj").on("focusout", validarCnpj);

$(function () {

    $("#cnpj").tooltipster({
        trigger: "custom"
    });

    formatarCnpj();
    $("#select-tipo").material_select();
    $("#select-tipoInstituicao").material_select();
    
    $("#cidade").prop("readonly", true);

    $("#estado").prop("readonly", true);

});

function formatarCnpj() {
    $("#cnpj").mask('00.000.000/0000-00', {
        reverse: true
    });
}

function validarCnpj() {
    var caracteresCnpj = $("#cnpj").val().length;
    if (caracteresCnpj < 18) {
        $("#cnpj").tooltipster("open").tooltipster("content", "CNPJ inválido");
    } else {
        $("#cnpj").tooltipster("close");
    }
}
