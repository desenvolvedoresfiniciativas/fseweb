$("#formDeleteInstituicao").submit(function () {
    console.log("submitou !")
});

$(function () {
    formatarCnpj();
    filtroTabela();
    $(".modal").modal();
})

function linkAlterar(cnpj) {
    var url = "/instituicoes/";

    window.location.replace(url + "'" + cnpj + "'");

    $("#alterar-" + cnpj).trigger("click");

}

function formatarCnpj() {
    $("#cnpj").mask('00.000.000/0000-00', {
        reverse: true
    });
}

function filtroTabela() {
    $('#txt-pesq-instituicao').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-instituicoes').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(2)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}
