$(".salvaMetrica").click(function(){
	
	var prodId = $(this).attr('id');
	$("."+prodId).each(function() {
		
		var ano = $("#combo-box :selected").val()
		var idEtapa = $(this).attr("id")
		var dataFin = $("#dataFinalizacao"+prodId+idEtapa).val()
		
		var valores = []
		valores.push(prodId)
		valores.push(idEtapa)
		valores.push(dataFin)
		valores.push(ano)
		
		$.ajax({
			type: "POST",
		contentType: "application/json",
			url: "/metricas/salvaParametros",
			data: JSON.stringify(valores),
			dataType:"JSON",
	        statusCode: {
	            200: function () {
	                Materialize
	                    .toast(
	                        'Métricas salvas com sucesso!',
	                        5000, 'rounded');
	            },
	            500: function () {
	                Materialize.toast(
	                    'Ops, houve um erro interno',
	                    5000, 'rounded');
	            },
	            400: function () {
	                Materialize
	                    .toast(
	                        'Você deve estar fazendo algo de errado',
	                        5000, 'rounded');
	            },
	            404: function () {
	                Materialize.toast('Url não encontrada',
	                    5000, 'rounded');
	            }
	        }	
		});
		
	})

	
});

$("#cnpj").keyup(function() {

	if (!this.value) {
		limpaTabela();
	}

});

$(function() {
	
	$('select').material_select();

})
