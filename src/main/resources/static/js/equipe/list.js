$(function(){
	$('.modal').modal();
	$('.modalD').modal();
	filtroTabela();
})

function listarConsultoresModal(idEquipe) {
    zerarLista();
    var lista = $('#lista-colaboradores');

    var url = '/equipes/' + idEquipe + '/consultores.json';

    $.get(url, function (data) {

        $(data).each(function () {

            var consultor = this;

            var consultorView = consultor.nome;

            if (this.cargo == 'CONSULTOR_LIDER_TECNICO') {
                consultorView += ' - Líder de Equipe';
            }

            var item = novoItemLista(consultorView);
            lista.append(item);
        });

    });

    $('#modal1').modal('open');

};

function novoItemLista(item) {
    var itemLista = $('<li>');

    itemLista.addClass('collection-item');

    itemLista.text(item);

    return itemLista;
}

function zerarLista(){
	$('#lista-colaboradores li').each(function(){
		$(this).remove();
	})
}

function filtroTabela() {
    $('#txt-pesq-equipe').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-equipes').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(2)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}

