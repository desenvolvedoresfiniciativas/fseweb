$(function(){
	$('.select').material_select();	
	$('.collapsible').collapsible('open', 0);
})

function validaConsultor(){
	if ($.trim($("#select-consultor").find(":selected").val()) == "undefined") {
		Materialize.toast('O prêmio precisa de um consultor.', 4000,'rounded');
	}
}

function validaVN(){
	if ($.trim($("#VNAtingido").val()) === "" || $.trim($("#VNPretendido").val()) === "") {
		Materialize.toast('Finalize o preenchimento dos Valores de negócio.', 4000, 'rounded');
	}
}

function validaPropostas(){
	if ($.trim($("#propostas").val()) === "" || $.trim($("#propostas").val()) == "0"  || $.trim($("#metaPropostas").val()) === "" || $.trim($("#metaPropostas").val()) == "0") {
		Materialize.toast('Finalize o preenchimento das propostas.', 4000, 'rounded');
	}
}
function validaContratos(){
	if ($.trim($("#contratos").val()) === "" || $.trim($("#contratos").val()) == "0" || $.trim($("#metaContratos").val()) === "" || $.trim($("#metaContratos").val()) == "0") {
		Materialize.toast('Finalize o preenchimento dos contratos.', 4000, 'rounded');
	}
}
function validaTicketMedio(){
	if ($.trim($("#ticketMedio").val()) === "" || $.trim($("#metaTicketMedio").val()) === "") {
		Materialize.toast('Finalize o preenchimento dos tickets médios.', 4000,	'rounded');
	}
}
function validaFluxoCaixa(){
	if ($.trim($("#fluxoCaixa").val()) === "" || $.trim($("#metaFluxoCaixa").val()) === "") {
		Materialize.toast('Finalize o preenchimento dos fluxos de caixa.', 4000, 'rounded');
	}
}
function validaTotalPremio(){
	if ($.trim($("#totalPremio").val()) === "") {
		Materialize.toast('O prêmio precisa de um valor.', 6000, 'rounded');
	}
}

function validaCampanha(){
	if ($.trim($("#select-campanha").find(":selected").val()) === "") {
		Materialize.toast('O prêmio precisa de uma campanha.', 4000,'rounded');
	}
}
	var junior = "JUNIOR";
	var pleno = "PLENO";
	var senior = "SENIOR"
	
$('#select-categoria').on('change', setarValoresPremio);
$('#select-consultor').on('change', setarValoresPremio);

function setarValoresPremio() {
	var nivelCargo = $('#select-categoria :selected').val();
	
	var idConsultor = $('#select-consultor :selected').val();
	
	$.getJSON("/consultores/" + idConsultor +  ".json", function(dados) {
		if (dados.erro != true) {
			$("#email").val(dados.email);
			$("#email").focusin();
			$("#cargo").val(dados.cargo);
			$("#cargo").focusin();
//			$("#nivelCargo").val(dados.categoriaConsultor);
//			$("#nivelCargo").focusin();
//			nivelCargo = dados.categoriaConsultor;
			
		} else {
			$("#email").val("");
			$('#cargo').val("");
			$('#nivelCargo').val("");
		}
		
		if(nivelCargo == junior){
			var VNPretendido = 600000;
			var metaPropostas = 52;
			var metaContratos = 12;
			var metaTicketMedio = 50000;
			var limitePremio = 21000;
						
			$("#VNPretendido").val(Number(VNPretendido).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
			$("#VNPretendido").focusin();
			
			$("#metaPropostas").val(metaPropostas);
			$("#metaPropostas").focusin();
			
			$("#metaContratos").val(metaContratos);
			$("#metaContratos").focusin();
			
			$("#metaTicketMedio").val(Number(metaTicketMedio).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
			$("#metaTicketMedio").focusin();

			$("#totalPremio").val("");

			$("#limitePremioCargo").val(Number(limitePremio).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
			$("#limitePremioCargo").focusin();
			
		}else if(nivelCargo == pleno){
			var VNPretendido = 1100000;
			var metaPropostas = 75;
			var metaContratos = 20;
			var metaTicketMedio = 55000;
			var limitePremio = 38500;
			
			$("#VNPretendido").val(Number(VNPretendido).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
			$("#VNPretendido").focusin();
			
			$("#metaPropostas").val(metaPropostas);
			$("#metaPropostas").focusin();
			
			$("#metaContratos").val(metaContratos);
			$("#metaContratos").focusin();
			
			$("#metaTicketMedio").val(Number(metaTicketMedio).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
			$("#metaTicketMedio").focusin();
			
			$("#totalPremio").val("");
			
			$("#limitePremioCargo").val(Number(limitePremio).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
			$("#limitePremioCargo").focusin();
			
		}else if(nivelCargo == senior) {
			var VNPretendido = 1900000;
			var metaPropostas = 112;
			var metaContratos = 134;
			var metaTicketMedio = 59375;
			var limitePremio = 42500;
			
			$("#VNPretendido").val(Number(VNPretendido).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
			$("#VNPretendido").focusin();
			
			$("#metaPropostas").val(metaPropostas);
			$("#metaPropostas").focusin();
			
			$("#metaContratos").val(metaContratos);
			$("#metaContratos").focusin();
			
			$("#metaTicketMedio").val(Number(metaTicketMedio).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
			$("#metaTicketMedio").focusin();
			
			$("#totalPremio").val("");
			
			$("#limitePremioCargo").val(Number(limitePremio).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
			$("#limitePremioCargo").focusin();
		}else {
			$("#VNPretendido").val("");
			$("#metaPropostas").val("");
			$("#metaContratos").val("");
			$("#metaTicketMedio").val("");
			$("#totalPremio").val("");
			$("#limitePremioCargo").val("");
		}
			
	})	 
};

//VALOR NEGOCIO
$('#VNAtingido').on('keyup', atingidoVN);
$('#VNPretendido').on('keyup', atingidoVN);

function atingidoVN() {
	var vnAtingido = parseFloat($("#VNAtingido").val().replace(",", "").replaceAll(".", ""));
	var vnPretendido = parseFloat($("#VNPretendido").val().replace(",", "").replaceAll(".", ""));
	var eficienciaVN = 0;
	var nan = NaN;
	
	if(vnAtingido > 0 && vnPretendido > 0 && eficienciaVN > -1 && vnAtingido !== nan && vnPretendido !== nan){
		eficienciaVN = (vnAtingido / vnPretendido * 100);
		$("#eficienciaVN").val(eficienciaVN.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 3}));
	}else{
		$("#eficienciaVN").val(0);
	}
	
	if(vnAtingido > 0 ){
		var metaFluxoCaixa = (vnAtingido / 100) * 0.70;
		$("#metaFluxoCaixa").val(Number(metaFluxoCaixa).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
		$("#metaFluxoCaixa").focusin();
	}else {
		$("#metaFluxoCaixa").val("");
	}
	totalPremio();
	
}

//PROPOSTAS
$('#propostas').on('keyup', funcPropostas);
$('#metaPropostas').on('keyup', funcPropostas);

function funcPropostas() {
	var propostas = $("#propostas").val();
	var metaPropostas = $("#metaPropostas").val();
	var eficienciaPropostas = 0;
	var nan = NaN;
	
	if(propostas > 0 && metaPropostas > 0 && eficienciaPropostas > -1 && propostas !== nan && metaPropostas !== nan){
		eficienciaPropostas = (propostas / metaPropostas) * 100;
		$("#eficienciaPropostas").val(eficienciaPropostas.toLocaleString('pt-br', {maximumFractionDigits: 1}))
		totalPremio();
	}else{
		$("#eficienciaPropostas").val(0)
	}
}

//CONTRATOS
$('#contratos').on('keyup', funcContratos);
$('#metaContratos').on('keyup', funcContratos);

function funcContratos() {
	var contratos = $("#contratos").val();
	var metaContratos = $("#metaContratos").val();
	var eficienciaContratos = 0;
	var nan = NaN;
	
	if(contratos > 0 && metaContratos > 0 && eficienciaContratos > -1 && contratos !== nan && metaContratos !== nan){
	
		eficienciaContratos = (contratos / metaContratos) * 100;
		$("#eficienciaContratos").val(eficienciaContratos.toLocaleString('pt-br', {maximumFractionDigits: 1}))
		totalPremio();
	}else{
		$("#eficienciaContratos").val(0)
	}
}

//TICKET MEDIO
$('#ticketMedio').on('keyup', ticketMedio);
$('#metaTicketMedio').on('keyup', ticketMedio);

function ticketMedio() {
	var ticketMedio = parseFloat($("#ticketMedio").val().replace(",", "").replaceAll(".", ""));
	var metaTicketMedio = parseFloat($("#metaTicketMedio").val().replace(",", "").replaceAll(".", ""));
	var eficienciaTicketMedio = 0;
	var nan = NaN;
	
	if(ticketMedio > 0 && metaTicketMedio > 0 && eficienciaTicketMedio > -1 && ticketMedio !== nan && metaTicketMedio !== nan){
		eficienciaTicketMedio = (ticketMedio / metaTicketMedio * 100);
		$("#eficienciaTicketMedio").val(eficienciaTicketMedio.toLocaleString('pt-br', {maximumFractionDigits: 1}));
		totalPremio();
	}else{
		$("#eficienciaTicketMedio").val(0);
	}
}

//FLUXO DE CAIXA
$('#fluxoCaixa').on('keyup', fluxoCaixa);
$('#metaFluxoCaixa').on('keyup', fluxoCaixa);

function fluxoCaixa() {
	var fluxoCaixa = parseFloat($("#fluxoCaixa").val().replace(",", "").replaceAll(".", ""));
	var metaFluxoCaixa = parseFloat($("#metaFluxoCaixa").val().replace(",", "").replaceAll(".", ""));
	var eficienciaFluxoCaixa = 0;
	var nan = NaN;
	
	if(fluxoCaixa > 0 && metaFluxoCaixa > 0 && eficienciaFluxoCaixa > -1 && fluxoCaixa !== nan && metaFluxoCaixa !== nan){
		eficienciaFluxoCaixa = (fluxoCaixa / metaFluxoCaixa * 100);
		$("#eficienciaFluxoCaixa").val(eficienciaFluxoCaixa.toLocaleString('pt-br', {maximumFractionDigits: 1}));
		totalPremio();
	}else{
		$("#eficienciaFluxoCaixa").val(0);
	}
	
	
}

//TOTAL PREMIO
function totalPremio() {
	var nivelCargo = $('#select-categoria :selected').val();
		
		var vnAtingido = parseFloat($("#VNAtingido").val().replace(",", "").replaceAll(".", ""));
		var vnPretendido = parseFloat($("#VNPretendido").val().replace(",", "").replaceAll(".", ""));
	
		var premioEficiencia;
		var premioServicos;
		var premioTicketMedio;
		var premioFluxoCaixa;
		
		var contratos = $("#contratos").val();
		var metaContratos = $("#metaContratos").val();
		
		var propostas = $("#propostas").val();
		
		var ticketMedio = parseFloat($("#ticketMedio").val().replace(",", "").replaceAll(".", ""));
		var metaTicketMedio = parseFloat($("#metaTicketMedio").val().replace(",", "").replaceAll(".", ""));
		
		var fluxoCaixa = parseFloat($("#fluxoCaixa").val().replace(",", "").replaceAll(".", ""));
		var metaFluxoCaixa = parseFloat($("#metaFluxoCaixa").val().replace(",", "").replaceAll(".", ""));
		//VN atingido maior que 70% da meta de VN
		if(vnAtingido >= (0.70 * vnPretendido)){
			if(nivelCargo == junior) {
				premioEficiencia = 3000;
				premioServicos = 3000;
				premioTicketMedio = 3000;
				premioFluxoCaixa = 3000;
				
			}else if(nivelCargo == pleno){
				premioEficiencia = 5500;
				premioServicos = 5500;
				premioTicketMedio = 5500;
				premioFluxoCaixa = 5500;
				
			}else if(nivelCargo == senior){ 
				premioEficiencia = 5500;
				premioServicos = 5500;
				premioTicketMedio = 5500;
				premioFluxoCaixa = 5500;
			}
		}
		//VN atingido maior que 80% da meta de VN
		if(vnAtingido >= (0.80 * vnPretendido)){
			if(nivelCargo == junior) {
				premioEficiencia = 6000;
				premioServicos = 6000;;
				premioTicketMedio = 6000;
				premioFluxoCaixa = 3000;
				
			}else if(nivelCargo == pleno){
				premioEficiencia = 11000;
				premioServicos = 11000;
				premioTicketMedio = 11000;
				premioFluxoCaixa = 5500;
				
			}else if(nivelCargo == senior){
				premioEficiencia = 11000;
				premioServicos = 11000;
				premioTicketMedio = 11000;
				premioFluxoCaixa = 9500;
			}
		}
		//EFICIENCIA
		if(contratos > 0 && propostas > 0){
			var porcentagemPropostasContratos;
			porcentagemPropostasContratos = contratos / propostas;
			
			var resultadoPremioEficiencia;
			resultadoPremioEficiencia = premioEficiencia * porcentagemPropostasContratos;
			
			if(resultadoPremioEficiencia > premioEficiencia){
				premioEficiencia = premioEficiencia;
			}else {
				premioEficiencia = resultadoPremioEficiencia;
			}
		}else{
			premioEficiencia = 0;
		}
		//SERVICOS
		if (contratos >= metaContratos) {
			premioServicos = premioServicos;
		}else {
			premioServicos = 0;
		}
		//TICKET MEDIO
		if(ticketMedio >= metaTicketMedio){
			premioTicketMedio = premioTicketMedio;
		}else{
			premioTicketMedio = 0;
		}
		//FLUXO DE CAIXA
		if(fluxoCaixa >= metaFluxoCaixa){
			var eficienciaFluxoCaixa = fluxoCaixa / vnAtingido;
			var resultPremioFluxoCaixa = eficienciaFluxoCaixa * premioFluxoCaixa;
			
			if(resultPremioFluxoCaixa >= premioFluxoCaixa) {
				premioFluxoCaixa = premioFluxoCaixa;
			}else{
				premioFluxoCaixa = resultPremioFluxoCaixa;				
			}
		}else {
			premioFluxoCaixa = 0;
		}
		
		//TOTAL PREMIO
		var totalPremio;
		totalPremio = premioEficiencia + premioServicos + premioTicketMedio + premioFluxoCaixa;
		
//		console.log("Premio KPI Eficiencia: " + premioEficiencia)
//		console.log("Premio KPI Servicos: " + premioServicos)
//		console.log("Premio KPI Ticket Medio: " + premioTicketMedio)
//		console.log("Premio KPI Fluxo de caixa: " + premioFluxoCaixa)
		
		$("#premioKPIEficiencia").val(premioEficiencia);
		$("#premioKPIServicos").val(premioServicos);
		$("#premioKPITicketMedio").val(premioTicketMedio);
		$("#premioKPIFluxoCaixa").val(premioFluxoCaixa);
		
//		console.log("Premio KPI Eficiencia variavel: " + ("#premioKPIEficiencia").val)
//		console.log("Premio KPI Servicos variavel: " + ("#premioKPIServicos").val)
//		console.log("remio KPI Ticket Medio variavel: " + $("#premioKPITicketMedio").val)
//		console.log("Premio KPI Fluxo de caixa variavel: " + $("#premioKPIFluxoCaixa").val)
		
		$("#totalPremio").val(Number(totalPremio).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2', maximumFractionDigits: '2'}));
		$("#totalPremio").focusin();
	}


$("#VNAtingido").mask('000.000.000.000,00', {reverse : true});
$("#VNPretendido").mask('000.000.000.000,00', {reverse : true});
$("#eficienciaVN").mask('000,00', {reverse : true});
$("#ticketMedio").mask('000.000.000.000,00', {reverse : true});
$("#metaTicketMedio").mask('000.000.000.000,00', {reverse : true});
$("#fluxoCaixa").mask('000.000.000.000,00', {reverse : true});
$("#metaFluxoCaixa").mask('000.000.000.000,00', {reverse : true});
$("#totalPremio").mask('000.000.000.000,00', {reverse : true});

function handleChange(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 1000) input.value = 1000;
  }
function handleChange2() {
	var premio = parseFloat($("#totalPremio").val().replaceAll(".", ""));
	var limitePremio = parseFloat($("#limitePremioCargo").val().replaceAll(".", ""));
	var nan = NaN;
	console.log(premio);
	console.log(limitePremio);
    if(premio == !nan || limitePremio !== nan){
		if (premio > limitePremio) {
			$("#totalPremio").val(Number(limitePremio).toLocaleString('pt-BR', { style: 'decimal' , minimumFractionDigits: '2'}));
		}else if(premio < 0){
			$("#totalPremio").val("0");
		}
	}else{
		
	}
}