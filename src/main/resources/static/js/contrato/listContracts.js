$(function () {
    filtroTabela();
})

function filtroTabela() {
    $('#txt-pesq-contratos').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-contratos').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(3)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}