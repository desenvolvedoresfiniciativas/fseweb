$('#btnEncerrarContrato').click(exibeDataEncerramento);
$('#addFaixaEscalonado').click(addLinhaEscalonado);
$('#removeFaixaEscalonado').click(apagaLinha);
$('#addFaixaEscalonadoModal').click(addLinhaModal);
$('#removeFaixaEscalonadoModal').click(apagaLinhaModal);
$('#btn-update-contrato').click(salvaTudo);
$('#salvaHonorario').click(atualizaHonorarios);
$('#atualizaVigencia').click(atualizaVigencia);
$('#honorarioBlocked').click(mensagemHonBlocked)
$('#salvaPEModal').click(salvaPEModal)
$('#combo').change(atualizarCombo);
$('#produtoPrincipal').click(atualizarCombo);

$(function () {

    $('.select').material_select();
    formatarCnpj();
    $('#dataCobrancaTrimestral1').mask('99/99');
    $('#dataCobrancaTrimestral2').mask('99/99');
    $('#dataCobrancaTrimestral3').mask('99/99');

    checkForPorcent();
    checkTimings()
    

	if($('#dateEncerramentoContrato').val()!=null || $('#dateEncerramentoContrato').val()!=""){
		$('#dateEncerramentoContrato').show();
	} else {
		$('#dateEncerramentoContrato').hide();
	}

    $('.modal').modal();

    $("#porcentagem1").on('keyup', function () {
		checkForPorcent();
        if ($("#porcentagem1").val() < 0) {
            $("#porcentagem1").val(0);
        }
    });

    $("#porcentagem2").on('keyup', function () {
        checkForPorcent();
        if ($("#porcentagem2").val() < 0) {
            $("#porcentagem2").val(0)
        }
    });

    $("#porcentagem3").on('keyup', function () {
        checkForPorcent();
        if ($("#porcentagem3").val() < 0) {
            $("#porcentagem3").val(0)
        }
    });

    $("#porcentagem4").on('keyup', function () {
        checkForPorcent();
        if ($("#porcentagem4").val() < 0) {
            $("#porcentagem4").val(0)
        }
    });

    $('.honorariosContrato').each(function () {
        if ($(this).val() == 'Percentual Escalonado') {
            $('#hon_percent_escalonado').attr('checked', true)
        }
        if ($(this).val() == 'Percentual Fixo') {
            $('#hon_percent_fixo').attr('checked', true)
        }
        if ($(this).val() == 'Valor Fixo') {
            $('#hon_valor_fixo').attr('checked', true)
        }
    })

    if ($("#vigContrato").val() == "Vigência Determinada") {
        $('#vig_determinada').attr('checked', true)
    } else {
        $('#vig_indeterminada').attr('checked', true)
    }

    if ($("#prejuizoFixo").is(":checked")) {
        $("#prejuizoValorFixo").show();
    } else {
        $("#prejuizoValorFixo").hide();
    }
    if ($("#prejuizoPercentual").is(":checked")) {
        $("#prejuizoPercentualFixo").show();
    } else {
        $("#prejuizoPercentualFixo").hide();
    }
});

function checkTimings() {
    if ($('#honorario1').val() == "Valor Fixo") {
        $("#divValor1").show();
        $("#divPorcent1").hide();
        $("#divEscalonado1").hide()

    }
    if ($('#honorario1').val() == "Percentual Fixo") {
        $("#divValor1").hide();
        $("#divPorcent1").show();
        $("#divEscalonado1").hide();

    }
    if ($('#honorario1').val() == "Isento" || $('#honorario1').val() == "") {
        $("#divValor1").hide();
        $("#divPorcent1").hide();
        $("#divEscalonado1").hide()

    }
    if ($('#honorario1').val() == "Percentual Escalonado") {
        $("#divValor1").hide();
        $("#divPorcent1").hide();
        $("#divEscalonado1").show();

    }
    if ($('#honorario2').val() == "Valor Fixo") {
        $("#divValor2").show();
        $("#divPorcent2").hide();
        $("#divEscalonado1").hide()

    }
    if ($('#honorario2').val() == "Percentual Fixo") {
        $("#divValor2").hide();
        $("#divPorcent2").show();
        $("#divEscalonado2").hide()

    }
    if ($('#honorario2').val() == "Percentual Escalonado") {
        $("#divValor2").hide();
        $("#divPorcent2").hide();
        $("#divEscalonado2").show();

    }
    if ($('#honorario2').val() == "Isento" || $('#honorario2').val() == "") {
        $("#divValor2").hide();
        $("#divPorcent2").hide();
        $("#divEscalonado2").css("display", "none")
    }
    if ($('#honorario3').val() == "Valor Fixo") {
        $("#divValor3").show();
        $("#divPorcent3").hide();
        $("#divEscalonado3").css("display", "none")

    }
    if ($('#honorario3').val() == "Percentual Fixo") {
        $("#divValor3").hide();
        $("#divPorcent3").show();
        $("#divEscalonado3").css("display", "none")

    }
    if ($('#honorario3').val() == "Percentual Escalonado") {
        $("#divValor3").hide();
        $("#divPorcent3").hide();
        $("#divEscalonado3").show();

    }
    if ($('#honorario3').val() == "Isento" || $('#honorario3').val() == "") {
        $("#divValor3").hide();
        $("#divPorcent3").hide();
        $("#divEscalonado3").hide();

    }
    if ($('#honorario4').val() == "Valor Fixo") {
        $("#divValor4").show();
        $("#divPorcent4").hide();
        $("#divEscalonado4").css("display", "none")

    }
    if ($('#honorario4').val() == "Percentual Fixo") {
        $("#divValor4").hide();
        $("#divPorcent4").show();
        $("#divEscalonado4").css("display", "none")

    }
    if ($('#honorario4').val() == "Percentual Escalonado") {
        $("#divValor4").hide();
        $("#divPorcent4").hide();
        $("#divEscalonado4").show();

    }
    if ($('#honorario4').val() == "Isento" || $('#honorario4').val() == "") {
        $("#divValor4").hide();
        $("#divPorcent4").hide();
        $("#divEscalonado4").hide();
    }
}

$('#honorario1').on('change', function () {
	checkTimings()
    if ($(this).val() == "Valor Fixo") {
        $("#divValor1").show();
        $("#divPorcent1").hide();
        $("#divEscalonado1").hide();

    }
    if ($(this).val() == "Percentual Fixo") {
        $("#divValor1").hide();
        $("#divPorcent1").show();
        $("#divEscalonado1").hide();

    }
    if ($(this).val() == "Percentual Escalonado") {
        $("#divValor1").hide();
        $("#divPorcent1").hide();
        $("#divEscalonado1").show();

    }
    if ($(this).val() == "Isento" || $(this).val() == "") {
        $("#divValor1").hide();
        $("#divPorcent1").hide();
        $("#divEscalonado1").hide();

    }
})

$('#honorario2').on('change', function () {
	checkTimings()
    if ($(this).val() == "Valor Fixo") {
        $("#divValor2").show();
        $("#divPorcent2").hide();
        $("#divEscalonado2").hide();

    }
    if ($(this).val() == "Percentual Fixo") {
        $("#divValor2").hide();
        $("#divPorcent2").show();
        $("#divEscalonado2").hide();

    }
    if ($(this).val() == "Percentual Escalonado") {
        $("#divValor2").hide();
        $("#divPorcent2").hide();
        $("#divEscalonado2").show();

    }
    if ($(this).val() == "Isento" || $(this).val() == "") {
        $("#divValor2").hide();
        $("#divPorcent2").hide();
        $("#divEscalonado2").hide();

    }
})

$('#honorario3').on('change', function () {
	checkTimings()
    if ($(this).val() == "Valor Fixo") {
        $("#divValor3").show();
        $("#divPorcent3").hide();
        $("#divEscalonado3").hide();

    }
    if ($(this).val() == "Percentual Fixo") {
        $("#divValor3").hide();
        $("#divPorcent3").show();
        $("#divEscalonado3").hide();

    }
    if ($(this).val() == "Percentual Escalonado") {
        $("#divValor3").hide();
        $("#divPorcent3").hide();
        $("#divEscalonado3").show();

    }
    if ($(this).val() == "Isento" || $(this).val() == "") {
        $("#divValor3").hide();
        $("#divPorcent3").hide();
        $("#divEscalonado3").hide();

    }
})

$('#honorario4').on('change', function () {
	checkTimings()
    if ($(this).val() == "Valor Fixo") {
        $("#divValor4").show();
        $("#divPorcent4").hide();
        $("#divEscalonado4").hide();
    }
    if ($(this).val() == "Percentual Fixo") {
        $("#divValor4").hide();
        $("#divPorcent4").show();
        $("#divEscalonado4").hide();
    }
    if ($(this).val() == "Percentual Escalonado") {
        $("#divValor4").hide();
        $("#divPorcent4").hide();
        $("#divEscalonado4").show();
    }
    if ($(this).val() == "Isento" || $(this).val() == "") {
        $("#divValor4").hide();
        $("#divPorcent4").hide();
        $("#divEscalonado4").hide();
    }
})


function loadSum() {

    var soma = porcent1 + porcent2 + porcent3 + porcent4

    $("#totalPorcent").text(soma + "%")

}

function apagaLinha() {
    $("#faixasEscalonamento li:last").remove();
}

function salvaTudo() {
    try {

        atualizarGerais();
        atualizarValorFixo();
        atualizarPercentualFixo();
        atualizarPercentualEscalonado();
        atualizarVigencia();
        atualizarEtapas();
        atualizarLBPrejuizo();
        atualizarCampanhasVigencia();
        atualizarCombo();
        produtosAlvo();
        atualizaNomeCombo();
        alteraNomeCombo();
        alteraPeriodoCobranca();

        Materialize.toast('Contrato atualizado com sucesso !', 5000, 'rounded');
    } catch (err) {
        Materialize.toast('Algo deu errado!', 5000, 'rounded');
        console.log(err);
    }
}

$(function zeroPorcent() {
    if ($("#porcentagem1").val() == null || $("#porcentagem1").val() == '') {
        $("#porcentagem1").val('0')
    }
    if ($("#porcentagem2").val() == null || $("#porcentagem2").val() == '') {
        $("#porcentagem2").val('0')
    }
    if ($("#porcentagem3").val() == null || $("#porcentagem3").val() == '') {
        $("#porcentagem3").val('0')
    }
    if ($("#porcentagem4").val() == null || $("#porcentagem4").val() == '') {
        $("#porcentagem4").val('0')
    }
});

function checkForPorcent() {

	var idProduto = $("#produtoid").val();
	console.log("produtoid",idProduto);

	var checkbox= $("#pagamentoData").is(":checked")
	console.log("produtoid",checkbox);

	if(idProduto == 23 || idProduto == 32 && checkbox == false) {
		console.log("entrou no IF----------------------")
		console.log("entrou no if do checkForPorcent");

	    var porcent1;
	    var porcent2;
	    var porcent3;
    	var porcent4;

    	if ($("#porcentagem1").val() == undefined || $("#porcentagem1").val() == "" || $("#porcentagem1").val() == "NaN") {
        	porcent1 = 0;
    	} else {
        	porcent1 = parseFloat($("#porcentagem1").val());
	    }
    	if ($("#porcentagem2").val() == undefined || $("#porcentagem2").val() == "" || $("#porcentagem2").val() == "NaN") {
        	porcent2 = 0;
	    } else {
    	    porcent2 = parseFloat($("#porcentagem2").val());
	    }
    	if ($("#porcentagem3").val() == undefined || $("#porcentagem3").val() == "" || $("#porcentagem3").val() == "NaN") {
        	porcent3 = 0;
	    } else {
    	    porcent3 = parseFloat($("#porcentagem3").val());
	    }
    	if ($("#porcentagem4").val() == undefined || $("#porcentagem4").val() == "" || $("#porcentagem4").val() == "NaN") {
        	porcent4 = 0;
	    } else {
    	    porcent4 = parseFloat($("#porcentagem4").val());
	    }

	    var soma = porcent1 + porcent2 + porcent3 + porcent4

    	$("#totalPorcent").text(soma + "%")
		console.log("totalporcent " + soma)

	    let comercialEntrada = $.trim($("#comercialResponsavelEntrada").find(":selected").val())
    	let comercialAtual = $.trim($("#comercialResponsavelEfetivo").find(":selected").val())

	    var checkBoxCombo = $('#combo').is(":checked")
		var nomeCombo = $("#nomeCombo").val();
		console.log("nomeCombo checkForPorcent ",nomeCombo)
    	
		if (checkBoxCombo == true) {
        	var valorCombo = (nomeCombo === "")
			console.log("valorCombo checkForPorcent ",valorCombo)

			if (soma === 100 & comercialEntrada != '' & comercialAtual != '' & valorCombo != false) {
	        	$("#btn-update-contrato").attr("disabled", false);
			} else {
    	    	$("#btn-update-contrato").attr("disabled", true);
	    	}
	    }

    if (soma === 100) {
        $("#totalPorcent").css("color", "#33cc33");
    } else {
        $("#totalPorcent").css("color", "#ff0000");
    }

    if (soma === 100 & comercialEntrada != '' & comercialAtual != '' ) {
        $("#btn-update-contrato").attr("disabled", false);
    } else {
        $("#btn-update-contrato").attr("disabled", true);
    }
} else {
	console.log("entrou no else")
}
}


function atualizarVigencia() {

    console.log($("#tipoVigencia").text())
    var vig = "vigência determinada"

    if ($("#tipoVigencia").text().toLowerCase() == vig) {
        atualizarVigenciaDeterminada();
    } else {
        atualizarVigenciaIndeterminada();
    }
}

$('#salvarEscalonamento')
    .click(
        function () {

            var valoresIniciais = $(".valorInicial");
            var valoresFinais = $(".valorFinal");
            var idContrato = $("#idContrato").val();
            var Cliente = new Object();
            var Produto = new Object();
            var ComercialResponsavel = new Object();
            var Vigencia = new Object();
            var FormaPagamento = new Object();
            var PrazoPagamento = new Object();
            var Reembolso = new Object();
            var Garantias = new Object();
            var PercentualEscalonado = new Object();
            var listGarantias = [];
            var PercentualFixo = {};
            var ValorFixo = {};
            var Honorario = {};

            Contrato["id"] = $("#idContrato").val();
            Produto["id"] = $("#id-produto :selected").val();
            Cliente["id"] = $("#id-cliente").val();
            ComercialResponsavel = $("#idComercialResponsavel");

            Contrato["vigencia"] = Vigencia;

            console.log(Contrato);

            var honorarios = [];
            var listFaixaEscalonado = [];

            $(".honorario").each(function () {
                if ($(this).text() == "Valor Fixo") {
                    atualizarValorFixo;
                }

                if ($(this).text() == "Percentual Fixo") {
                    atualizarValorFixo;
                }

                if ($(this).text() == "Percentual Escalonado") {
                    atualizarPercentualEscalonado;
                }
            })

            FormaPagamento = $('input[name=formaPagamento]:checked')
                .val();
            PrazoPagamento = $("#prazoPagamento").val();
            Reembolso = $("#reembolsoDespesas").is(":checked");

            var selecteds = [];
            $('#selectGarantias :selected').each(function (i, selected) {
                if ($(selected).text() != "Selecione") {
                    var garantiaContrato = new Object();
                    garantiaContrato["id"] = $(selected).val();
                    garantiaContrato["descricao"] = $(selected).text();
                    listGarantias.push(garantiaContrato);
                }
            });

            Contrato["reembolso"] = Reembolso;
            Contrato["produto"] = Produto;
            Contrato["cliente"] = Cliente;
            Contrato["garantias"] = listGarantias;
            Contrato["formaPagamento"] = FormaPagamento;
            Contrato["prazoPagamento"] = PrazoPagamento;
            // Contrato["honorarios"] = honorarios;

            var urlMontada = "/contratos/update";

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: urlMontada,
                data: JSON.stringify(Contrato) +
                    PercentualFixo + PercentualEscalonado +
                    ValorFixo,
                dataType: 'JSON',
                statusCode: {
                    200: function () {
                        Materialize
                            .toast(
                                'Contrato atualizado com sucesso !',
                                5000, 'rounded');
                    },
                    500: function () {
                        Materialize.toast(
                            'Ops, houve um erro interno',
                            5000, 'rounded');
                    },
                    400: function () {
                        Materialize
                            .toast(
                                'Você deve estar fazendo algo de errado',
                                5000, 'rounded');
                    },
                    404: function () {
                        Materialize.toast('Url não encontrada',
                            5000, 'rounded');
                    }
                }
            });

        })

function exibeDataEncerramento() {
    $('#btnEncerrarContrato').hide();
    $('#dateEncerramentoContrato').show();

}

function retornaFaixaEscalonamento(id, label, typeText, money, classe) {

    var divInput = $("<div>").addClass("col").addClass("m3").addClass(
        "input-field");

    var input = $("<input>").attr("type", typeText).attr("id", id).addClass(
        classe);

    var label = $("<label>").attr("for", id).text(label);

    if (money) {
        input.addClass("money");
    }

    divInput.append(input);
    divInput.append(label);

    return divInput;

}

var index = 1;

function retornaLinha() {
    var divRow = $("<div>").addClass("row");

    divRow.append(retornaFaixaEscalonamento("valorPercentual" + index, "Valor %",
        "number", false, "valorPercentual"));
    divRow.append(retornaFaixaEscalonamento("valorInicial" + index, "Valor Inicial R$",
        "text", true, "valorInicial"));
    divRow.append(retornaFaixaEscalonamento("valorFinal" + index, "Valor Final R$",
        "text", true, "valorFinal"));

    index++;

    return divRow;
}

function addLinhaEscalonado() {
    var lista = $("#faixasEscalonamento");
    var li = $("<li>");

    li.append(retornaLinha());

    lista.append(li);

    // para formatar os campos que ainda não foram carregados pelo main.js
    $('.money').mask('000.000.000.000.000,00', {
        reverse: true
    });

}

function atualizarEtapas() {

    var data = $("#dataFat").val();

    var etapa1 = {
        id: $("#etapa1").find(":selected").val(),
    }

    var etapa2 = {
        id: $("#etapa2").find(":selected").val(),
    }

    var etapa3 = {
        id: $("#etapa3").find(":selected").val(),
    }
    var etapa4 = {
        id: $("#etapa4").find(":selected").val(),
    }

    var contrato = {
        id: $("#idContrato").val(),
    }

    var valor1;
    var valor2;
    var valor3;
    var valor4;

    var produto = $('#id-produto').find(":selected").text();
    if (produto == "LEI DO BEM" || produto == "LB RETROATIVA") {
        valor1 = 0.0;
        valor2 = 0.0;
        valor3 = 0.0;
        valor4 = 0.0;
    } else {

        if ($("#valor1").val() != undefined) {
            valor1 = $("#valor1").val().replace(/\./g, '').replace(",", ".")
        }
        if ($("#valor2").val() != undefined) {
            valor2 = $("#valor2").val().replace(/\./g, '').replace(",", ".")
        }
        if ($("#valor3").val() != undefined) {
            valor3 = $("#valor3").val().replace(/\./g, '').replace(",", ".")
        }
        if ($("#valor4").val() != undefined) {
            valor4 = $("#valor4").val().replace(/\./g, '').replace(",", ".")
        }
    }

    var etapasPagamento = {
        id: $("#idEtapas").val(),
        etapa1: etapa1,
        honorario1: $("#honorario1").find(":selected").val(),
        valor1: valor1,
        porcentagem1: $("#porcentagem1").val(),
        parcela1: $("#parcela1").val(),
        etapa2: etapa2,
        honorario2: $("#honorario2").find(":selected").val(),
        valor2: valor2,
        parcela2: $("#parcela2").val(),
        porcentagem2: $("#porcentagem2").val(),
        etapa3: etapa3,
        honorario3: $("#honorario3").find(":selected").val(),
        valor3: valor3,
        parcela3: $("#parcela3").val(),
        porcentagem3: $("#porcentagem3").val(),
        etapa4: etapa4,
        honorario4: $("#honorario4").find(":selected").val(),
        valor4: valor4,
        parcela4: $("#parcela4").val(),
        porcentagem4: $("#porcentagem4").val(),
        contrato: contrato,
        dataFaturamento: data
    }

    console.log(etapasPagamento);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratos/atualizaEtapas",
        data: JSON.stringify(etapasPagamento),
        dataType: "JSON",
    });
}

function atualizarVigenciaDeterminada() {

    var dataDeterminada = {
        id: $("#idVigencia").val(),
        inicioVigencia: $("#inicioVigencia").val(),
        fimVigencia: $("#fimVigencia").val()
    }

    console.log(dataDeterminada)

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratos/vigencias/vigenciadeterminada/update",
        data: JSON.stringify(dataDeterminada),
        dataType: "JSON",
    });

}

function atualizarVigenciaIndeterminada() {
    var dataIndeterminada = {
        id: $("#idVigencia").val(),
        nome: $("#tipoVigencia").text(),
        inicioVigencia: $("#inicioVigencia").val(),
        dataEncerramentoContrato: $("#dataEncerramentoContrato").val()
    }

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratos/vigencias/vigenciaindeterminada/update",
        data: JSON.stringify(dataIndeterminada),
        dataType: "JSON",
    });

}

function atualizarValorFixo() {

    var valor = $("#valorFixo").val();
    if (valor == null) {
        console.log("Valor Fixo nulo");
    } else {
        if (valor != undefined) {
            valor = valor.replace(/\./g, '').replace(",", ".");
        }

        var valorFixo = {
            id: $("#idValorFixo").val(),
            nome: $("#nomeHonorario").text(),
            valor: valor,
            valorMensal: $("#isValorMensal").is(":checked"),
            valorAnual: $("#isValorAnual").is(":checked"),
            valorTrimestral: $("#isvalorTrimestral").is(":checked"),
            dataCobrancaAnual: $("#dataCobrancaAnual").val(),
            dataCobrancaMensal: $("#dataCobrancaMensal").val(),
            dataCobrancaTrimestral1: $("#dataCobrancaTrimestral1").val(),
            dataCobrancaTrimestral2: $("#dataCobrancaTrimestral2").val(),
            dataCobrancaTrimestral3: $("#dataCobrancaTrimestral3").val(),
            reajuste: $("#isReajuste").is(":checked"),
            obesrvacao: $("#obesrvacaoReajuste").val(),
            contratoId: $("#idContrato").val()
        }

        console.log(valorFixo)

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/contratos/honorarios/valorfixo/update",
            data: JSON.stringify(valorFixo),
            dataType: "JSON",
        });
    }
}

function atualizarPercentualFixo() {
    if ($("#idPercentualFixo").val() == null) {
        console.log("Percentual Fixo Nulo")
    } else {
        var valor = $("#valorPF").val();

        var percentualFixo = {
            nome: $("#nomePercentualFixo").text(),
            id: $("#idPercentualFixo").val(),
            contratoId: $("#idContrato").val(),
            valorPercentual: valor,
            limitacao: $("#limitacaoPercentualFixo").val().replace(/\./g, '').replace(",", "."),
        }

        console.log(percentualFixo);

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/contratos/honorarios/percentualfixo/update",
            data: JSON.stringify(percentualFixo),
            dataType: "JSON",
        });
    }
}

function redefineIdsValoresPercentuais() {
    var valoresPercentuais = $('.valorPercentual');
    index = 0;
    valoresPercentuais.each(function () {
        var id = this.id;
        id = id.replace(/\d+/g, '');
        console.log(id);
        $(this).attr('id', id + index);
        index++;
    });

}

function redefineIdsValoresIniciais() {
    var valorInicial = $('.valorInicial');
    index = 0;
    valorInicial.each(function () {
        var id = this.id;
        id = id.replace(/\d+/g, '');
        $(this).attr('id', id + index);
        index++;
    });

}

function redefineIdsValorFinal() {
    var valorFinal = $('.valorFinal');
    index = 0;
    valorFinal.each(function () {
        var id = this.id;
        id = id.replace(/\d+/g, '');
        $(this).attr('id', id + index);
        index++;
    });

}

function redefineIdsValoresPercentuais() {
    var valoresPercentuais = $('.valorPercentual');
    index = 0;
    valoresPercentuais.each(function () {
        var id = this.id;
        id = id.replace(/\d+/g, '');
        $(this).attr('id', id + index);
        index++;
    });

}

function atualizarPercentualEscalonado() {

    redefineIdsValoresIniciais();
    redefineIdsValorFinal();
    redefineIdsValoresPercentuais();

    if ($("#idPEscalonado").val() == null) {
        console.log("Percentual Escalonado nulo")
    } else {

        var valoresPercentuais = $('.valorPercentual');

        var faixas = [];

        var index = 0;

        valoresPercentuais.each(function () {

            if ($("#valorPercentual" + index) == undefined) {
                console.log("undefined")
            } else {

                var id = parseInt($("#idPercentualEscalonado").val());
                id = id + index;

                var percentual = $("#valorPercentual" + index).val();
                var final = $("#valorFinal" + index).val();
                var inicial = $("#valorInicial" + index).val();

                faixasEscalonadas = {
                    valorPercentual: percentual,
                    valorFinal: final.replace(/\./g, '').replace(",", "."),
                    valorInicial: inicial.replace(/\./g, '').replace(",", "."),
                };
                faixas.push(faixasEscalonadas);

                index = index + 1;
            }
        });

        var percentualEscalonado = {
            id: $("#idPEscalonado").val(),
            contratoId: $("#idContrato").val(),
            nome: $("#honorario").val(),
            limitacao: $("#limitacaoPercentualEscalonado").val().replace(/\./g, '').replace(",", "."),
            acimaDe: $("#acimaDe").val().replace(/\./g, '').replace(",", "."),
            percentAcimaDe: $("#percentAcimaDe").val(),
            faixasEscalonamento: faixas
        }

        console.log(percentualEscalonado)


        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/contratos/honorarios/percentualescalonado/update",
            data: JSON.stringify(percentualEscalonado),
            dataType: "JSON",
        });

    }
}

function atualizarGerais() {

    var comercialResponsavelEntrada = {
        id: $("#comercialResponsavelEntrada").find(":selected").val()
    }

    var comercialResponsavelEfetivo = {
        id: $("#comercialResponsavelEfetivo").find(":selected").val()
    }

    garantiasArray = $("#selectGarantias").val();
    garantiaList = []

    for (var i = 0; i < garantiasArray.length; i++) {
        var garantia = garantiasArray[i];
        var gar = {
            id: garantia
        }
        garantiaList.push(gar)
    }

    console.log($("#estimativaComercial").val())
    
    console.log('teste ' + $("#estimativaComercial").val())
    	

    var contrato = {
        id: $("#idContrato").val(),
        comercialResponsavelEntrada: comercialResponsavelEntrada,
        comercialResponsavelEfetivo: comercialResponsavelEfetivo,
        descricaoContratual: $("#descricaoContratual").val(),
        obsContrato: $("#obsContrato").val(),
        cnpjFaturar: $("#cnpjFaturar").val(),
        razaoSocialFaturar: $("#razaoSocialFaturar").val(),
        contratoFisico: $("#contratoFisico").is(':checked'),
        estimativaComercial: $("#estimativaComercial").val(),
        dataEntrada: $("#dataEntrada").val(),
        pedidoCompra: $("#pedidoCompra").is(':checked'),
        formaPagamento: $("input:radio[name ='formaPagamento']:checked").val(),
        prazoPagamento: $("#prazoPagamento").val(),
        reembolsoDespesas: $("#reembolsoDespesas").is(':checked'),
        reajuste: $("#reajuste").is(':checked'),
        dataReajuste: $("#dataReajuste").val(),
        obsContrato: $("#obsContrato").val(),
        dataReajuste: $("#dataReajuste").val(),
        manual:$("#manual").is(':checked'),
        garantias: garantiaList,
		nContrato:$("#nContrato").val(),
		variable:$("#variable").find(":selected").val(),
		nContrato:$("#nContrato").val(),
		variable:$("#variable").find(":selected").val()
    }

    console.log(contrato);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratosCO/atualizar",
        data: JSON.stringify(contrato),
        dataType: "JSON",
    });

}

function atualizarCampanhasVigencia() {

    var campanhaInicial = $("#campanhaInicial").val()
    var campanhaFinal = $("#campanhaFinal").val()
    var idContrato = $("#idContrato").val()    
        
    
    
		var url = "/contratos/vigencias/campanha/" + idContrato + "/" + campanhaInicial + "/" + campanhaFinal + "/.json"
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: url,
            dataType: "JSON",
        });

    }



function atualizarLBPrejuizo() {

    var idProduto = $("#id-produto :selected").val();

    if (idProduto == 23) {

        var contrato = {
            id: $("#idContrato").val(),
            prejuizoFixo: $("#prejuizoFixo").is(':checked'),
            prejuizoPercentual: $("#prejuizoPercentual").is(':checked'),
            prejuizoValorFixo: $("#prejuizoValorFixo").val(),
            prejuizoPercentualFixo: $("#prejuizoPercentualFixo").val()
        }

        console.log(contrato)

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/contratos/atualizarLBPreju",
            data: JSON.stringify(contrato),
            dataType: "JSON",
        });

    }

}


function atualizaHonorarios() {

    var isPercentualFixo = $("#hon_percent_fixo").is(':checked');
    var isValorFixo = $("#hon_valor_fixo").is(':checked');
    var isEscalonado = $("#hon_percent_escalonado").is(':checked');

    var honorarios = [];

    if (isEscalonado == true) {
        var PE = {
            nome: "Percentual Escalonado"
        }
        honorarios.push(PE)
    }
    if (isPercentualFixo == true) {
        var PF = {
            nome: "Percentual Fixo"
        }
        honorarios.push(PF)
    }
    if (isValorFixo == true) {
        var VF = {
            nome: "Valor Fixo"
        }
        honorarios.push(VF)
    }

    var contrato = {
        id: $("#idContrato").val(),
        honorarios: honorarios
    }

    console.log(contrato)

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratos/atualizarHonorarios",
        data: JSON.stringify(contrato),
        dataType: "JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Honorários atualizados com sucesso!',
                        5000, 'rounded');
                location.reload();
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }
    });

}

function mensagemHonBlocked() {
    Materialize
        .toast(
            'Não é possível atualizar os honorários quando o contrato já possui faturamento gerado.',
            5000, 'rounded');
}

function atualizaVigencia() {

    var vigencia = $("input:radio[name ='checkVigencia']:checked").val();

    console.log(vigencia)
    var vi = {}

    if (vigencia == "indet") {
        vi = {
            nome: "vigenciaIndeterminada"
        }
    }

    if (vigencia == "det") {
        vi = {
            nome: "vigenciaDeterminada"
        }
    }

    var contrato = {
        id: $("#idContrato").val(),
        vigencia: vi
    }

    console.log(contrato)

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratos/atualizarVigencia",
        data: JSON.stringify(contrato),
        dataType: "JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Vigência atualizada com sucesso!',
                        5000, 'rounded');
                location.reload();
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }
    });
}



function fixoPositivo() {
    $("#prejuizoPercentualFixo").hide();
    $("#prejuizoValorFixo").show();
    $("#prejuizoPercentual").prop("checked", false);
    $("#prejuizoPercentualFixo").val('');

    if ($("#prejuizoFixo").prop('checked') == false) {
        $("#prejuizoValorFixo").val('');
        $("#prejuizoValorFixo").hide();
    }
}

function percentualPositivo() {
    $("#prejuizoValorFixo").hide();
    $("#prejuizoPercentualFixo").show();
    $("#prejuizoFixo").prop("checked", false);
    $("#prejuizoValorFixo").val('');

    if ($("#prejuizoPercentual").prop('checked') == false) {
        $("#prejuizoPercentualFixo").val('');
        $("#prejuizoPercentualFixo").hide();
    }
}

function populaModal(i) {

    $('#corpoFaixas').empty()
    $('.PE').val("")
    $('#modalHeader').text("Escalonamento - Etapa " + i);

    var timingId = $("#idEtapas").val()
	console.log("timingId",timingId);

    return $.ajax({
        type: 'GET',
        url: "" + timingId + "/timings.json",
        async: false,
        success: function (data) {

            console.log(data)

            if ($('#modalHeader').text() == "Escalonamento - Etapa 1") {

                $("#idPE").val(data.escalonado1.id)
                $("#acima").val(mascaraValor(data.escalonado1.acimaDe.toFixed(2)))
                $("#percentAcima").val(data.escalonado1.percentAcimaDe)
                $("#limitação").val(mascaraValor(data.escalonado1.limitacao.toFixed(2)))

                $('.money').mask('000.000.000.000.000,00', {
                    reverse: true
                });

                $(data.escalonado1.faixasEscalonamento).each(function (index) {

                    console.log($(this))
                    var faixa = $(this)[0]

                    var div = $('#corpoFaixas')
                    var divRow = $("<div class='row linhaPE' id='linha" + index + "'>")

                    var idPE = $("<input type='text' hidden='hidden' value='" + faixa.id + "' id='" + 'id' + index + "' />")

                    // Percentual
                    var divInputPercent = $("<div class='input-field col s2'>")
                    var percent = $("<input type='number' value='" + faixa.valorPercentual + "' id='" + 'percent' + index + "' />")
                    var labelPercent = $("<label for='percent'" + index + "' class='active'>%</label>")

                    // Inicio
                    var divInputInicio = $("<div class='input-field col s5'>")
                    var inicio = $("<input type='text' class='money' value='" + faixa.valorInicial.toFixed(2) + "' id='" + 'inicio' + index + "'/>")
                    var labelInicio = $("<label for='inicio'" + index + "' class='active'>Início</label>")

                    // Fim
                    var divInputFim = $("<div class='input-field col s5'>")
                    var fim = $("<input type='text' class='money' value='" + faixa.valorFinal.toFixed(2) + "' id='" + 'fim' + index + "'/>")
                    var labelFim = $("<label for='fim'" + index + "' class='active'>Fim</label>")

                    // Append percent
                    divInputPercent.append(percent)
                    divInputPercent.append(labelPercent)
                    divRow.append(idPE)
                    divRow.append(divInputPercent)

                    // Append inicio
                    divInputInicio.append(inicio)
                    divInputInicio.append(labelInicio)
                    divRow.append(divInputInicio)

                    // Append fim
                    divInputFim.append(fim)
                    divInputFim.append(labelFim)
                    divRow.append(divInputFim)

                    div.append(divRow)

                    $('.money').mask('000.000.000.000.000,00', {
                        reverse: true
                    });

                })
            }
            if ($('#modalHeader').text() == "Escalonamento - Etapa 2") {

                $("#idPE").val(data.escalonado2.id)
                $("#acima").val(mascaraValor(data.escalonado2.acimaDe.toFixed(2)))
                $("#percentAcima").val(data.escalonado2.percentAcimaDe)
                $("#limitação").val(mascaraValor(data.escalonado2.limitacao.toFixed(2)))

                $(data.escalonado2.faixasEscalonamento).each(function (index) {

                    console.log($(this))
                    var faixa = $(this)[0]

                    var div = $('#corpoFaixas')
                    var divRow = $("<div class='row linhaPE' id='linha" + index + "'>")

                    // Percentual
                    var divInputPercent = $("<div class='input-field col s2'>")
                    var percent = $("<input type='number' value='" + faixa.valorPercentual + "' id='" + 'percent' + index + "' />")
                    var labelPercent = $("<label for='percent'" + index + "' class='active'>%</label>")

                    // Inicio
                    var divInputInicio = $("<div class='input-field col s5'>")
                    var inicio = $("<input type='text' class='money' value='" + faixa.valorInicial.toFixed(2) + "' id='" + 'inicio' + index + "'/>")
                    var labelInicio = $("<label for='inicio'" + index + "' class='active'>Início</label>")

                    // Fim
                    var divInputFim = $("<div class='input-field col s5'>")
                    var fim = $("<input type='text' class='money' value='" + faixa.valorFinal.toFixed(2) + "' id='" + 'fim' + index + "'/>")
                    var labelFim = $("<label for='fim'" + index + "' class='active'>Fim</label>")

                    // Append percent
                    divInputPercent.append(percent)
                    divInputPercent.append(labelPercent)
                    divRow.append(divInputPercent)

                    // Append inicio
                    divInputInicio.append(inicio)
                    divInputInicio.append(labelInicio)
                    divRow.append(divInputInicio)

                    // Append fim
                    divInputFim.append(fim)
                    divInputFim.append(labelFim)
                    divRow.append(divInputFim)

                    div.append(divRow)

                    $('.money').mask('000.000.000.000.000,00', {
                        reverse: true
                    });

                })
            }
            if ($('#modalHeader').text() == "Escalonamento - Etapa 3") {

                $("#idPE").val(data.escalonado3.id)
                $("#acima").val(mascaraValor(data.escalonado3.acimaDe.toFixed(2)))
                $("#percentAcima").val(data.escalonado3.percentAcimaDe)
                $("#limitação").val(mascaraValor(data.escalonado3.limitacao.toFixed(2)))

                $(data.escalonado3.faixasEscalonamento).each(function (index) {

                    console.log($(this))
                    var faixa = $(this)[0]

                    var div = $('#corpoFaixas')
                    var divRow = $("<div class='row linhaPE' id='linha" + index + "'>")

                    // Percentual
                    var divInputPercent = $("<div class='input-field col s2'>")
                    var percent = $("<input type='number' value='" + faixa.valorPercentual + "' id='" + 'percent' + index + "' />")
                    var labelPercent = $("<label for='percent'" + index + "' class='active'>%</label>")

                    // Inicio
                    var divInputInicio = $("<div class='input-field col s5'>")
                    var inicio = $("<input type='text' class='money' value='" + faixa.valorInicial.toFixed(2) + "' id='" + 'inicio' + index + "'/>")
                    var labelInicio = $("<label for='inicio'" + index + "' class='active'>Início</label>")

                    // Fim
                    var divInputFim = $("<div class='input-field col s5'>")
                    var fim = $("<input type='text' class='money' value='" + faixa.valorFinal.toFixed(2) + "' id='" + 'fim' + index + "'/>")
                    var labelFim = $("<label for='fim'" + index + "' class='active'>Fim</label>")

                    // Append percent
                    divInputPercent.append(percent)
                    divInputPercent.append(labelPercent)
                    divRow.append(divInputPercent)

                    // Append inicio
                    divInputInicio.append(inicio)
                    divInputInicio.append(labelInicio)
                    divRow.append(divInputInicio)

                    // Append fim
                    divInputFim.append(fim)
                    divInputFim.append(labelFim)
                    divRow.append(divInputFim)

                    div.append(divRow)

                    $('.money').mask('000.000.000.000.000,00', {
                        reverse: true
                    });

                })
            }
            if ($('#modalHeader').text() == "Escalonamento - Etapa 4") {

                $("#idPE").val(data.escalonado4.id)
                $("#acima").val(mascaraValor(data.escalonado4.acimaDe.toFixed(2)))
                $("#percentAcima").val(data.escalonado4.percentAcimaDe)
                $("#limitação").val(mascaraValor(data.escalonado4.limitacao.toFixed(2)))

                $(data.escalonado4.faixasEscalonamento).each(function (index) {

                    console.log($(this))
                    var faixa = $(this)[0]

                    var div = $('#corpoFaixas')
                    var divRow = $("<div class='row linhaPE' id='linha" + index + "'>")

                    // Percentual
                    var divInputPercent = $("<div class='input-field col s2'>")
                    var percent = $("<input type='number' value='" + faixa.valorPercentual + "' id='" + 'percent' + index + "' />")
                    var labelPercent = $("<label for='percent'" + index + "' class='active'>%</label>")

                    // Inicio
                    var divInputInicio = $("<div class='input-field col s5'>")
                    var inicio = $("<input type='text' class='money' value='" + faixa.valorInicial.toFixed(2) + "' id='" + 'inicio' + index + "'/>")
                    var labelInicio = $("<label for='inicio'" + index + "' class='active'>Início</label>")

                    // Fim
                    var divInputFim = $("<div class='input-field col s5'>")
                    var fim = $("<input type='text' class='money' value='" + faixa.valorFinal.toFixed(2) + "' id='" + 'fim' + index + "'/>")
                    var labelFim = $("<label for='fim'" + index + "' class='active'>Fim</label>")

                    // Append percent
                    divInputPercent.append(percent)
                    divInputPercent.append(labelPercent)
                    divRow.append(divInputPercent)

                    // Append inicio
                    divInputInicio.append(inicio)
                    divInputInicio.append(labelInicio)
                    divRow.append(divInputInicio)

                    // Append fim
                    divInputFim.append(fim)
                    divInputFim.append(labelFim)
                    divRow.append(divInputFim)

                    div.append(divRow)

                    $('.money').mask('000.000.000.000.000,00', {
                        reverse: true
                    });

                })
            }
        }
    });
}




function addLinhaModal() {

    i = $('.linhaPE').length

    var div = $('#corpoFaixas')
    var divRow = $("<div class='row linhaPE' id='linha" + i + "'>")

    // Percentual
    var divInputPercent = $("<div class='input-field col s2'>")
    var percent = $("<input type='number' id='" + 'percent' + i + "' />")
    var labelPercent = $("<label for='percent'" + i + "'>%</label>")

    // Inicio
    var divInputInicio = $("<div class='input-field col s5'>")
    var inicio = $("<input type='text' class='money' id='" + 'inicio' + i + "'/>")
    var labelInicio = $("<label for='inicio'" + i + "'>Início</label>")

    // Fim
    var divInputFim = $("<div class='input-field col s5'>")
    var fim = $("<input type='text' class='money' id='" + 'fim' + i + "'/>")
    var labelFim = $("<label for='fim'" + i + "'>Fim</label>")

    // Append percent
    divInputPercent.append(percent)
    divInputPercent.append(labelPercent)
    divRow.append(divInputPercent)

    // Append inicio
    divInputInicio.append(inicio)
    divInputInicio.append(labelInicio)
    divRow.append(divInputInicio)

    // Append fim
    divInputFim.append(fim)
    divInputFim.append(labelFim)
    divRow.append(divInputFim)

    div.append(divRow)
    i++;

    $('.money').mask('000.000.000.000.000,00', {
        reverse: true
    });

}

function apagaLinhaModal() {
    i = $('.linhaPE').length

    $("#corpoFaixas").children().last().remove();

    i--;
}

function salvaPEModal() {

    j = 0
    var faixas = []

    $('.linhaPE').each(function () {

        if ($("#valorPercentual" + index) == undefined) {
            console.log("undefined")
        } else {

            var id = parseInt($("#idPercentualEscalonado").val());
            id = id + index;

            var percentual = $("#percent" + j).val();
            var final = $("#fim" + j).val();
            var inicial = $("#inicio" + j).val();

            var faixasEscalonadas = {
                id: $('#id' + j).val(),
                valorPercentual: percentual,
                valorFinal: final.replace(/\./g, '').replace(",", "."),
                valorInicial: inicial.replace(/\./g, '').replace(",", "."),
            };
            faixas.push(faixasEscalonadas);

            j++
        }
    });

    var percentualEscalonado = {
        id: $("#idPE").val(),
        contratoId: $("#idContrato").val(),
        nome: $('#modalHeader').text(),
        limitacao: $("#limitação").val().replace(/\./g, '').replace(",", "."),
        acimaDe: $("#acima").val().replace(/\./g, '').replace(",", "."),
        percentAcimaDe: $("#percentAcima").val(),
        faixasEscalonamento: faixas
    }

    console.log(percentualEscalonado)

    var idTiming = $('#idEtapas').val()

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratos/updatePE/" + idTiming,
        data: JSON.stringify(percentualEscalonado),
        dataType: 'JSON',
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Contrato atualizado com sucesso !',
                        5000, 'rounded');
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }
    });


}

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g, "");
    valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
    return valor
}


function atualizarCombo() {


    var combo = $("#combo").is(":checked");
    console.log("O VALOR DO COMBO É", combo)


    var produtoPrincipal = $("#produtoPrincipal").is(":checked");
    console.log("O VALOR DO produtoPrincipal É", produtoPrincipal)

    /*var produtoAlvo =$("#produtoAlvo").is(":checked");
    console.log("O VALOR DO produtoAlvo É",produtoAlvo)*/


    var idContrato = $("#idContrato").val();
    var url = "/contratos/" + idContrato + "/" + combo + "/" + produtoPrincipal + "/.json"
    return $.ajax({
        type: 'PUT',
        url: url,
        success: function (data) {
            console.log("entrou no ajax", produtoPrincipal)
        }

    })
}




$('#combo').change(function () {
	
	 var checked = $('#combo').is(":checked")
	 var checked2 = $('#produtoPrincipal').is(":checked")
    console.log(checked)

    if (checked == true || checked == 'checked') {
			 bloqueiaNomeCombo()


        console.log("deu certo if ")
        $("#group_id").css("display", "block");
        $("#botao-produtoPrincipal").css("display", "block");
        $("#botao-principal").css("display", "block");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "none")
    }
    else {
        $("#group_id").css("display", "none");
        $("#botao-produtoPrincipal").css("display", "none");
        $("#botao-principal").css("display", "none");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "none")
        $("#separador").css("display", "none")
		$('#btn-update-contrato').attr("disabled", false);
    }
	if((checked == true || checked == 'checked') && (checked2 == true || checked2 == 'checked')){
		
		console.log("combo change e ambos checked")
		$("#botao-principal").css("display", "none");
		$("#botao-produtoPrincipal").css("display", "none");
        $("#textNomeCombo").css("display", "block")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "block")
	}else{
		if((checked == true || checked == 'checked') && (checked2 == false || checked2 != 'checked')){
			$("#group_id").css("display", "block");
        	$("#botao-produtoPrincipal").css("display", "block");
        	$("#botao-principal").css("display", "block");
        	$("#textNomeCombo").css("display", "none")
        	$("#selectCombo").css("display", "block")
        	$("#separador").css("display", "none")
		}
	}


})

$('#produtoPrincipal').change(function () {

    $("#botao-produtoPrincipal").css("display", "none")
    $("#botao-principal").css("display", "none")


    var checked2 = $('#produtoPrincipal').is(":checked")
    console.log(checked2)

    if (checked2 == true || checked2 == 'checked') {
        $("#botao-produtoPrincipal").css("display", "none");
        $("#textNomeCombo").css("display", "block")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "block")
    }
    else {
        $("#botao-produtoPrincipal").css("display", "block");
        $("#botao-principal").css("display", "block")
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "none")

    }
})

$(function () {
    $("#group_id").css("display", "none")
    $("#botao-produtoPrincipal").css("display", "none")
    $("#botao-principal").css("display", "none")
    $("#textNomeCombo").css("display", "none")
    $("#selectCombo").css("display", "none")
    $("#separador").css("display", "none")



    var checked = $('#combo').is(":checked")
    console.log(checked)

    if (checked == true || checked == 'checked') {
        console.log("deu certo if ")
        $("#group_id").css("display", "block");

        $("#botao-produtoPrincipal").css("display", "block");
        $("#botao-principal").css("display", "block");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "none")

    }
    else {
        $("#group_id").css("display", "none");
        $("#botao-produtoPrincipal").css("display", "none");
        $("#botao-principal").css("display", "none");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "none")
        $("#separador").css("display", "none")
    }

})

function botao() {
    $('#contratoCombo').modal('open');
}

$(function () {

    var checked = $('#combo').is(":checked")
    var checked2 = $('#produtoPrincipal').is(":checked")

    if (checked2 == true || checked2 == 'checked') {
        $("#botao-produtoPrincipal").css("display", "none");
        $("#botao-principal").css("display", "none")
        $("#textNomeCombo").css("display", "block")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "block")
    }
    else {
        if (checked == true || checked == 'checked') {
            $("#botao-principal").css("display", "block")
            $("#botao-produtoPrincipal").css("display", "block");
            $("#textNomeCombo").css("display", "none")
            $("#selectCombo").css("display", "block")
            $("#separador").css("display", "none")
        }
        else {
            $("#botao-produtoPrincipal").css("display", "none");
            $("#botao-principal").css("display", "none")
            $("#textNomeCombo").css("display", "none")
            $("#selectCombo").css("display", "none")
            $("#separador").css("display", "none")
        }
    }
})

function produtosAlvo() {

    var produtosAlvo = $("#produtosAlvo").is(":checked");
    var contratosPai = []
    var idContratoFilho = $("#idContrato").val()

    $('table [type="checkbox"]').each(function (j, checked) {

        if (checked.checked || checked == 'checked') {
            const empresa = checked.value;
            var contratoId = checked.value;
            var contrato = {
                id: contratoId
            }
            contratosPai.push(contrato)
        }
    })

    console.log(contratosPai)
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/contratos/" + idContratoFilho + "/salvaContratosPai",
        data: JSON.stringify(contratosPai),
        dataType: 'JSON'
    });
}

$(function checked() {

    var produtosAlvo = $("#produtosAlvo").is(":checked");
    var idContrato = $("#idContrato").val();
    var PegarListaContrato = "";

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/contratos/" + idContrato + "/PegarListaContrato.json",
        dataType: 'JSON',
        success: function (listContratosAlvos) {
         
            for (var i = 0; i < listContratosAlvos.length; i++) {
                var contratoPai = listContratosAlvos[i];
                $("#" + contratoPai.id).attr('checked', true)
            }
        }
    });
})

function atualizaNomeCombo() {

    var produtoPrincipal = $("#produtoPrincipal").is(":checked");
    var idContrato = $("#idContrato").val();
    var nomeCombo = $("#nomeCombo").val();
    var url = "/contratos/" + idContrato + "/" + nomeCombo + "/PegarNomeCombo.json"
    return $.ajax({
        type: 'POST',
        url: url,
        success: function (ListaNomeCombo) {
        }
    })
}

$(function selectNomeCombo() {
    $("#selectNomeCombo").material_select();
    var cnpj = $("#cnpj").val()
    var idcliente = $("#idcliente").val()
    var selectNomeCombo = $("#selectNomeCombo").val()
    var nomeCombo = $("#nomeCombo").val()

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idcliente + "/PegarListaNomeCombo.json",
        success: function (listNomeCombo) {
            var select = document.getElementById("selectNomeCombo");
            for (var i = 0; i < listNomeCombo.length; i++) {
                option = document.createElement('option');
                option.value = listNomeCombo[i];
                option.text = listNomeCombo[i];
                $("#selectNomeCombo").material_select();
                select.add(option)
                
                if (listNomeCombo[i] === nomeCombo) {
                    document.getElementById("selectNomeCombo").selectedIndex = [i + 1];                    
                }
            }
			 $("#selectNomeCombo").material_select();
        }
    })
})

function alteraNomeCombo() {

    var idContrato = $("#idContrato").val();
    var selectNomeCombo = $("#selectNomeCombo");
    var nomeCombo = $("#nomeCombo");
    var valorCombo = (nomeCombo.val() === "");

    if (selectNomeCombo.val() != "" && (valorCombo == false)) {
        var url = "/contratos/" + idContrato + "/" + nomeCombo.val() + "/NomeCombo.json"
        return $.ajax({
            type: 'POST',
            url: url,
            success: function (NomeCombo) {
            }
        })
    } else {
        if ((selectNomeCombo.val() == "") && (valorCombo == false)) {
            var url = "/contratos/" + idContrato + "/" + nomeCombo.val() + "/NomeCombo.json"
            return $.ajax({
                type: 'POST',
                url: url,
                success: function (NomeCombo) {
                }
            })
        } else {
            if ((selectNomeCombo.val() != "") && (valorCombo == true)) {
                var url = "/contratos/" + idContrato + "/" + selectNomeCombo.val() + "/NomeCombo.json"
                return $.ajax({
                    type: 'POST',
                    url: url,
                    success: function (NomeCombo2) {
                    }
                })
            }
        }
    }
}

$('#selectNomeCombo').change(function () {
	duplicidadeSelectNomeCombo();
	bloqueiaNomeCombo()	
})

$('#nomeCombo').change(function () {
	bloqueiaNomeCombo()	
	duplicidadeNomeCombo()   
	validaNomeCombo();

})

function duplicidadeNomeCombo() {

    selectNomeCombo = $("#selectNomeCombo").val('');
    $("#selectNomeCombo").material_select();
    selectNomeCombo = $("#selectNomeCombo").val();
}

function duplicidadeSelectNomeCombo() {
    nomeCombo = $("#nomeCombo").val('');
}

function validaNomeCombo() {
    var nomeCombo = $("#nomeCombo")
    var idcliente = $("#idcliente").val()
	var selectNomeCombo = $("#selectNomeCombo");

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idcliente + "/PegarListaNomeCombo.json",
        success: function (listNomeCombo) {

            for (var i = 0; i < listNomeCombo.length; i++) {           
                if (listNomeCombo[i] === nomeCombo.val()) {                    
                    Materialize.toast('Já existe um nome combo cadastrado com esse nome', 5000, 'rounded');
    				$('#btn-update-contrato').attr("disabled", true);
					console.log("entrou no validaNomeCombo if");
                    break;
                }
                else if (selectNomeCombo.val()!=null && listNomeCombo[i] != nomeCombo.val() ) {
                    $('#btn-update-contrato').attr("disabled", false);
					console.log("entrou no validaNomeCombo else");
					console.log("selectNomeCombo.val()",selectNomeCombo.val());
                }
            }
        }
    })
}

function bloqueiaNomeCombo() {
    var selectNomeCombo = $("#selectNomeCombo");
	var nomeCombo = $("#nomeCombo");
    var valorCombo = (nomeCombo.val() === "")
	
	console.log("selectNomeCombo",selectNomeCombo.val());
	console.log("valorCombo",valorCombo);
	console.log("entrou no bloqueia nome combo");

	if (valorCombo === true && selectNomeCombo.val() == null) {	
		console.log("entrou no if bloqueio");
		 Materialize.toast('O combo necessita de um nome', 5000, 'rounded');
   		 	$('#btn-update-contrato').attr("disabled", true);
	}
	else{
   		 $('#btn-update-contrato').attr("disabled", false);
		console.log("entrou no else bloqueio");
	}
}

$("#comercialResponsavelEntrada").change(function () {
    checkForPorcent();
})

$("#comercialResponsavelEfetivo").change(function () {
    checkForPorcent();
})


 
 function alteraPeriodoCobranca(){
	
	
	var idContrato = $("#idContrato").val();
	var periodo = $("#selectPeriodoCobranca").val();
	console.log("periodo",periodo,idContrato);
	
	$.ajax({
        type: 'POST',
        url: "/contratos/" + idContrato + "/"+periodo+"/alteraPeriodoCobranca.json",
        success: function (data) {
		}
	})
	
}
 
 
  
