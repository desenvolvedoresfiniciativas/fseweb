$("#nit").blur(buscaEmpresa);

function buscaEmpresa() {
	
	var nit = $(this).val();
	//var rut = $(this).val().replace(/\D/g, "");
	
	if ( !$(this).val().length == 0 ) {
		$.getJSON("/clientesCO/" + nit +  ".json", function(dados) {

			if (dados.erro != true) {

				$("#nomeEmpresa").val(dados.razaoSocial);
				$("#nomeEmpresa").focusin();
				$("#idcliente").val(dados.id);
				$("#idcliente").focusin();
				$('#nitFaturar').val(nit);
				$('#nitFaturar').focusin();
				$('#razaoSocialFaturar').val(dados.razaoSocial)
				$('#razaoSocialFaturar').focusin();

			} else {
				Materialize.toast('NIT não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
				console.log("erro")
			}
		})

	}

	function limparNomeEmpresa() {
		$("#nomeEmpresa").val("");
		$('#cnpjFaturar').val("");
		$('#razaoSocialFaturar').val("");
	}
}


