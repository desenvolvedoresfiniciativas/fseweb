$(function () {
    filtroTabela();
})

function filtroTabela() {
    $('#txt-pesq-cliente').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-clientes-contratos').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(1)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}