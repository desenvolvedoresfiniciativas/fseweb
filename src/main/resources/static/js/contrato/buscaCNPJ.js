$("#cnpj").blur(buscaEmpresa);

function buscaEmpresa() {

	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);

	if ( !$(this).val().length == 0 ) {

		$.getJSON("/clientes/" + cnpj +  ".json", function(dados) {

			if (dados.erro != true) {
				const cnpjFormatado = cnpj.replace(/^(\d{2})?(\d{3})?(\d{3})?(\d{4})?(\d{2})/, "$1.$2.$3/$4-$5")

				$("#nomeEmpresa").val(dados.razaoSocial);
				$("#nomeEmpresa").focusin();
				$("#idcliente").val(dados.id);
				$("#idcliente").focusin();
				$('#cnpjFaturar').val(cnpjFormatado);
				$('#cnpjFaturar').focusin();
				$('#razaoSocialFaturar').val(dados.razaoSocial)
				$('#razaoSocialFaturar').focusin();
				$('#produtosAlvo').val(dados.produtosAlvo)
			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})
	}

	function limparNomeEmpresa() {
		$("#nomeEmpresa").val("");
		$('#cnpjFaturar').val("");
		$('#razaoSocialFaturar').val("");
	}
}
