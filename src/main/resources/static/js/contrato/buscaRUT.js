$("#rut").blur(buscaEmpresa);

function buscaEmpresa() {
	
	var rut = $(this).val();
	//var rut = $(this).val().replace(/\D/g, "");
	console.log(rut);
	
	if ( !$(this).val().length == 0 ) {

		$.getJSON("/clientesCL/" + rut +  ".json", function(dados) {

			if (dados.erro != true) {

				$("#nomeEmpresa").val(dados.razaoSocial);
				$("#nomeEmpresa").focusin();
				$("#idcliente").val(dados.id);
				$("#idcliente").focusin();
				$('#rutFaturar').val(rut);
				$('#rutFaturar').focusin();
				$('#razaoSocialFaturar').val(dados.razaoSocial)
				$('#razaoSocialFaturar').focusin();

			} else {
				Materialize.toast('RUT não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})

	}

	function limparNomeEmpresa() {
		$("#nomeEmpresa").val("");
		$('#cnpjFaturar').val("");
		$('#razaoSocialFaturar').val("");
	}
}


