$("#cnpj").on("input", formatarCnpj);
$("#cnpj").on("input", validarCnpj);
$("#cnpj").on("focusout", validarCnpj);
$("#cnpjFaturar").on("change", formatarCnpj);
$("#selectProduto").material_select();

$('#btn-add-contrato').click(
		function() {
			if ($.trim($("#selectProduto").find(":selected").val()) === "") {
				Materialize.toast('O contrato necessita de um produto.', 6000,
						'rounded');
			}
			if ($.trim($("#rut").val()) === "") {
				Materialize.toast('O contrato necessita de um cliente.', 6000,
						'rounded');
			}
			if ($.trim($("#comercialResponsavelEntrada").find(":selected").val()) === "") {
				Materialize.toast(
						'O contrato necessita de um comercial responsável da entrada.',
						6000, 'rounded');
			}
			if ($.trim($("#comercialResponsavelEfetivo").find(":selected").val()) === "") {
				Materialize.toast(
						'O contrato necessita de um comercial responsável atual.',
						6000, 'rounded');
			}
			if (!$("#vigenciaDeterminada").is(':checked')
					&& !$("#vigenciaIndeterminada").is(':checked')) {
				Materialize.toast(
						'O contrato necessita de um tipo de vigência.', 6000,
						'rounded');
			}
			if (!$("#valorFixo:checked").is(':checked')
					&& !$("#percentualFixo:checked").is(':checked')
					&& !$("#percentualEscalonado:checked").is(':checked')) {

			}

			validateFat();

		});

$(function() {
	$('.modal').modal();
	$("select").material_select();
	$("#selectProduto").material_select();
	$("#selectGarantias").material_select();
	$("#selectComercialResponsavel").material_select();
	$("#cnpj").tooltipster({
		trigger : "custom"
	});
	$("#cnpjFaturar").tooltipster({
		trigger : "custom"
	});
	formatarCnpj();
	radioListener();
	//statusChecker();
});

function statusChecker(){
	
	if($("#status").val() == 'false'){
		$('#modalContrato').modal('open'); 
	}
}

function validateFat() {
	$("#form").submit(
			function() {
				if ($('#pagamentoEntrega:checked').is(":checked")
						|| $('#pagamentoData:checked').is(":checked")) {
					console.log("1")
					var status = validateHon();
					if (status == true) {
						return true;
					} else {
						return false;
					}
				} else {
					Materialize.toast(
							'O contrato necessita de uma forma de faturação.',
							6000, 'rounded');
					console.log("2")
					return false;
				}
			});
}

function validateHon() {
	var status = false;
	if (!$("#valorFixo:checked").is(':checked')
			&& !$("#percentualFixo:checked").is(':checked')
			&& !$("#percentualEscalonado:checked").is(':checked')) {
		Materialize.toast('O contrato necessita de um honorário.', 6000,
				'rounded');
		return status;
	} else {
		status = true;
		console.log("true");
		return status;
	}
}



function radioListener(){
	$('.contratoId').change(function(){
		console.log("ouvi")
		console.log($('.contratoId').parent());
	})
}