$("#cnpj").on("input", formatarCnpj);
$("#cnpj").on("input", validarCnpj);
$("#cnpj").on("focusout", validarCnpj);
$("#cnpjFaturar").on("change", formatarCnpj);
$("#selectProduto").material_select();



$('#btn-add-contrato').click(
    function () {
        if ($.trim($("#selectProduto").find(":selected").val()) === "") {
            Materialize.toast('O contrato necessita de um produto.', 6000,
                'rounded');
        }
        if ($.trim($("#cnpj").val()) === "") {
            Materialize.toast('O contrato necessita de um cliente.', 6000,
                'rounded');
        }
        if ($.trim($("#comercialResponsavelEntrada").find(":selected").val()) === "") {
            Materialize.toast(
                'O contrato necessita de um comercial responsável da entrada.',
                6000, 'rounded');
        }
        if ($.trim($("#comercialResponsavelEfetivo").find(":selected").val()) === "") {
            Materialize.toast(
                'O contrato necessita de um comercial responsável atual.',
                6000, 'rounded');
        }
        if (!$("#vigenciaDeterminada").is(':checked')
            && !$("#vigenciaIndeterminada").is(':checked')) {
            Materialize.toast(
                'O contrato necessita de um tipo de vigência.', 6000,
                'rounded');
        }
        if (!$("#valorFixo:checked").is(':checked')
            && !$("#percentualFixo:checked").is(':checked')
            && !$("#percentualEscalonado:checked").is(':checked')) {

        }

        validateFat();
        validaNomeCombo()

    });

$(function () {
    $('.modal').modal();
    $("select").material_select();
    $("#selectProduto").material_select();
    $("#selectGarantias").material_select();
    $("#selectComercialResponsavel").material_select();
    $("#cnpj").tooltipster({
        trigger: "custom"
    });
    $("#cnpjFaturar").tooltipster({
        trigger: "custom"
    });
    formatarCnpj();
    radioListener();

    /*visivel();*/
    //visivel();
    //statusChecker();
});

function statusChecker() {

    if ($("#status").val() == 'false') {
        $('#modalContrato').modal('open');
    }
}

function validateFat() {
    $("#form").submit(
        function () {
            if ($('#pagamentoEntrega:checked').is(":checked")
                || $('#pagamentoData:checked').is(":checked")) {
                console.log("1")
                var status = validateHon();
                if (status == true) {
                    return true;
                } else {
                    return false;
                }
            } else {
                Materialize.toast(
                    'O contrato necessita de uma forma de faturação.',
                    6000, 'rounded');
                console.log("2")
                return false;
            }
        });
}

function validateHon() {
    var status = false;
    if (!$("#valorFixo:checked").is(':checked')
        && !$("#percentualFixo:checked").is(':checked')
        && !$("#percentualEscalonado:checked").is(':checked')) {
        Materialize.toast('O contrato necessita de um honorário.', 6000,
            'rounded');
        return status;
    } else {
        status = true;
        console.log("true");
        return status;
    }
}

function radioListener() {
    $('.contratoId').change(function () {
        console.log("ouvi")
        console.log($('.contratoId').parent());
    })
}

$('#combo').change(function () {
    var checked = $('#combo').is(":checked")
	var checked2 = $('#produtoPrincipal').is(":checked")

	
    if (checked == true || checked == 'checked') {
		
       
		
        $("#group_id").css("display", "block");
        $("#botao-produtoPrincipal").css("display", "block");
        $("#botao-principal").css("display", "block");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "none")
		selectNomeCombo()
		bloqueiaNomeCombo()
    } else {
        $("#group_id").css("display", "none");
        $("#botao-produtoPrincipal").css("display", "none");
        $("#botao-principal").css("display", "none");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "none")
        $("#separador").css("display", "none")
		$('#btn-add-contrato').attr("disabled", false);
		 nomeCombo = $("#nomeCombo").val('');
    }

	if((checked == true || checked == 'checked') && (checked2 == true || checked2 == 'checked')){
		
		console.log("combo change e ambos checked")
		$("#botao-principal").css("display", "none");
		$("#botao-produtoPrincipal").css("display", "none");
        $("#textNomeCombo").css("display", "block")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "block")
	}else{
		if((checked == true || checked == 'checked') && (checked2 == false || checked2 != 'checked')){
			$("#group_id").css("display", "block");
        	$("#botao-produtoPrincipal").css("display", "block");
        	$("#botao-principal").css("display", "block");
        	$("#textNomeCombo").css("display", "none")
        	$("#selectCombo").css("display", "block")
        	$("#separador").css("display", "none")
		}
	}


})


$('#produtoPrincipal').change(function () {

    $("#botao-produtoPrincipal").css("display", "none")
    $("#botao-principal").css("display", "none")
    var checked2 = $('#produtoPrincipal').is(":checked")
    if (checked2 == true || checked2 == 'checked') {
        $("#botao-produtoPrincipal").css("display", "none");
        $("#textNomeCombo").css("display", "block")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "block")
    }
    else {
        $("#botao-produtoPrincipal").css("display", "block");
        $("#botao-principal").css("display", "block")
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "none")

    }
})

$(function () {

    $("#group_id").css("display", "none")
    $("#botao-produtoPrincipal").css("display", "none")
    $("#botao-principal").css("display", "none")
    $("#textNomeCombo").css("display", "none")
    $("#selectCombo").css("display", "none")
    $("#separador").css("display", "none")
    var checked = $('#combo').is(":checked")
	


    if (checked == true || checked == 'checked') {
        $("#group_id").css("display", "block");
        $("#botao-produtoPrincipal").css("display", "block");
        $("#botao-principal").css("display", "block");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "none")
		
		
    }
    else {
        $("#group_id").css("display", "none");
        $("#botao-produtoPrincipal").css("display", "none");
        $("#botao-principal").css("display", "none");
        $("#textNomeCombo").css("display", "none")
        $("#selectCombo").css("display", "none")
        $("#separador").css("display", "none")
    }
})

$(function () {

    var checked = $('#combo').is(":checked")
    var checked2 = $('#produtoPrincipal').is(":checked")

    if (checked2 == true || checked2 == 'checked') {
        $("#botao-produtoPrincipal").css("display", "none");
        $("#botao-principal").css("display", "none")
        $("#textNomeCombo").css("display", "block")
        $("#selectCombo").css("display", "block")
        $("#separador").css("display", "block")
    }
    else {
        if (checked == true || checked == 'checked') {
            $("#botao-produtoPrincipal").css("display", "block");
            $("#botao-principal").css("display", "block")
            $("#textNomeCombo").css("display", "none")
            $("#selectCombo").css("display", "block")
            $("#separador").css("display", "none")
			
			
        }
        else {
            $("#botao-produtoPrincipal").css("display", "none");
            $("#botao-principal").css("display", "none")
            $("#textNomeCombo").css("display", "none")
            $("#selectCombo").css("display", "none")
            $("#separador").css("display", "none")
        }
    }
})


function botao() {
    $("#TabelaForms").empty();
    $('#contratoCombo').modal('open');
    carregaInformacoes();
}

function carregaInformacoes() {
    var idCliente = $("#idcliente")
    var imprimeCNPJ = $("#cnpj")
    console.log(imprimeCNPJ.val(), "CNPJ")
    console.log(idCliente.val(), "ID CLIENTE")

    $.getJSON("/contrato/" + cnpj + ".json", function (dados) {

        if (dados.erro != true) {

            $("#idcliente").val(dados.id);
            $("#idcliente").focusin();

        } else {
            Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
            limparNomeEmpresa();
        }
    })

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idCliente.val() + "/PegarListaContratoForms.json",
        success: function (listContratosAlvosform) {

            console.log(listContratosAlvosform.length)
            for (var i = 0; i < listContratosAlvosform.length; i++) {
                console.log(listContratosAlvosform[i])

                var produtoNome = "<td>" + listContratosAlvosform[i].produto.nome + "</td>";
                var vigenciaInicio = "<td>" + listContratosAlvosform[i].vigencia.inicioVigencia + "</td>";
                var vigenciaFim = "<td>" + listContratosAlvosform[i].vigencia.fimVigencia + "</td>";
                var comercialResponsavelEfetivo = "<td>" + listContratosAlvosform[i].comercialResponsavelEfetivo.nome + "</td>";
                var checkbox = "<td><input  id='produtosAlvo" + i + "' name='produtosAlvo' type='checkbox' value='" + listContratosAlvosform[i].id +
                    "' class='filled-in'/><label for='produtosAlvo" + i + "'> </label> </td>";


                var y = listContratosAlvosform[i].vigencia.nome
                console.log("VIGENCIA inicio NOME É:" + y)
                var z = listContratosAlvosform[i].vigencia.nome.toString()
                console.log("VIGENCIA fim NOME É:" + z)

                if (listContratosAlvosform[i].vigencia.inicioVigencia == null) {
                    vigenciaInicio = ""
                    vigenciaInicio = "<td>" + vigenciaInicio + "</td>";
                    console.log("início vigencia  indetrminada = null")
                }
                else {
                    vigenciaInicio = "<td>" + listContratosAlvosform[i].vigencia.inicioVigencia + "</td>";
                    console.log("início vigencia  indetrminada != null")
                }

                if (listContratosAlvosform[i].vigencia.nome == 'VIGÊNCIA INDETERMINADA') {
                    if (listContratosAlvosform[i].vigencia.dataEncerramentoContrato != undefined) {
                        var teste = listContratosAlvosform[i].vigencia.dataEncerramentoContrato;
                        vigenciaFim = "<td>" + teste + "</td>";
                        console.log("fim vigencia  indetrminada != undefined")
                    }
                    else {
                        vigenciaFim = ""
                        vigenciaFim = "<td>" + vigenciaFim + "</td>";
                        console.log("fim vigencia indetrminada = undefined")
                    }
                }
                else if (listContratosAlvosform[i].vigencia.nome.toString() == 'VIGÊNCIA DETERMINADA') {
                    if (listContratosAlvosform[i].vigencia.fimVigencia != undefined) {
                        var teste = listContratosAlvosform[i].vigencia.fimVigencia;
                        console.log("teste  determinada != undefined")
                        vigenciaFim = "<td>" + teste + "</td>";
                    }
                    else {
                        console.log("vigencia determinada = undefined")
                        vigenciaFim = ""
                        vigenciaFim = "<td>" + vigenciaFim + "</td>";
                    }
                }
                $("#TabelaForms").append("<tr>" + produtoNome + vigenciaInicio + vigenciaFim + comercialResponsavelEfetivo + checkbox + "</tr>")
            }
        }
    });
}

function selectNomeCombo() {
	$("#selectNomeCombo").empty();
	
    $.getJSON("/contrato/" + cnpj + ".json", function (dados) {

        if (dados.erro != true) {

            $("#idcliente").val(dados.id);
            $("#idcliente").focusin();

        } else {
            Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
            limparNomeEmpresa();
        }
    })

    var idCliente = $("#idcliente")
    $.ajax({
        type: 'GET',
        url: "/contratos/" + idCliente.val() + "/PegarListaNomeCombo.json",
		async: false,
        success: function (listNomeCombo) {
			var select = document.getElementById("selectNomeCombo");
			option = document.createElement('option')
			option.value = '';
            option.text = '';
			select.add(option);
           
            for (var i = 0; i < listNomeCombo.length; i++) {
				console.log("a lista de nome combo é : ",listNomeCombo[i])
                option = document.createElement('option');
                option.value = listNomeCombo[i];
                option.text = listNomeCombo[i];
                $("#selectNomeCombo").material_select();
				select.add(option)
				console.log("option : ",option)
				console.log("select : ",select)
            }
			 $("#selectNomeCombo").material_select();
        }
    })
}

$('#nomeCombo').change(function () {
	console.log("nome change")
    bloqueiaNomeCombo()
    validaNomeCombo();
    duplicidadeNomeCombo();
	selectNomeCombo()
})

$('#selectNomeCombo').change(function () {
    bloqueiaNomeCombo()
    duplicidadeSelectNomeCombo()
	
})

function validaNomeCombo() {
    var nomeCombo = $("#nomeCombo")
    var valorCombo = (nomeCombo.val() === "");

    $.getJSON("/contrato/" + cnpj + ".json", function (dados) {

        if (dados.erro != true) {
            $("#idcliente").val(dados.id);
            $("#idcliente").focusin();
        } else {
            Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
            limparNomeEmpresa();
        }
    })
    var idCliente = $("#idcliente")
    if ((valorCombo == false)) {

        $.ajax({
            type: 'GET',
            url: "/contratos/" + idCliente.val() + "/PegarListaNomeCombo.json",
            success: function (listNomeCombo) {

                for (var i = 0; i < listNomeCombo.length; i++) {
                    if (listNomeCombo[i] === nomeCombo.val()) {
                        Materialize.toast('Já existe um nome combo cadastrado com esse nome', 5000, 'rounded');
                        $('#btn-add-contrato').attr("disabled", true);
						console.log("entrou no bloqueia")
                        break;
                    }
                    else if (listNomeCombo[i] != nomeCombo.val()) {
                        $("#btn-add-contrato").attr("disabled", false);
						console.log("entrou no desbloqueia")
                    }
                }
            }
        })
    }
}

function bloqueiaNomeCombo() {
    var selectNomeCombo = $("#selectNomeCombo");
    var nomeCombo = $("#nomeCombo");
    var valorCombo = (nomeCombo.val() === "")

    if (valorCombo === true && selectNomeCombo.val() == "") {
        Materialize.toast('O combo necessita de um nome', 5000, 'rounded');
		console.log("entrou no bloqueia")
        $('#btn-add-contrato').attr("disabled", true);
    }
    else {
        $('#btn-add-contrato').attr("disabled", false);
		console.log("entrou no desbloqueia")
    }
}

function duplicidadeNomeCombo() {

    selectNomeCombo = $("#selectNomeCombo").val('');
    $("#selectNomeCombo").material_select();
    selectNomeCombo = $("#selectNomeCombo").val();
}

function duplicidadeSelectNomeCombo() {

    nomeCombo = $("#nomeCombo").val('');
}

$("#form").submit(function () {
        $("#btn-add-contrato").attr("disabled", true);
        return true;
});