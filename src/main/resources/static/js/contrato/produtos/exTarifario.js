$(function () {
    checkTiming();
	$('#descontarValor').show();
})

function checkTiming(){
	
	if($('#semTiming').val()=='true'){
		$('#modalCarregaDefault').modal('open');	
	}
}

$('#btnCarregaDefault').click(function(){
	carregaConfigPadrao()
})

$('#carregDefault').click(function(){
	carregaConfigPadrao()
})

function carregaConfigPadrao(){
	
	$('#etapa1').val(78)
	$('#honorario1').val('Valor Fixo')
	$('#divValor1').show()
	
	$('#etapa2').val(79)
	$('#honorario2').val('Valor Fixo')
	$('#divValor2').show()
	
	$('#etapa3').val(108)
	$('#honorario3').val('Percentual Fixo')
	$('#divPorcent3').show()
	
	$('#etapa1').material_select();
	$('#honorario1').material_select();
	$('#etapa2').material_select();
	$('#honorario2').material_select();
	$('#etapa3').material_select();
	$('#honorario3').material_select();
	
	checkTimings()
	
}