$(function () {
	 $('select').material_select();
	 $("#divCliente").hide();
	 $("#divDireto").hide();
	 $("#recebidaFilial").hide();
	 $("#recebidaComercial").hide();
	 $("#enviadaConsultor").hide();
	 $("#enviadaFilial").hide();
 });

$("input[name='tipoReco']").click(function() {
    if ($(this).val() === 'cliente') {
    	$("#divDireto").hide();
    	$("#divCliente").show();
    } else if ($(this).val() === 'direto') {
    	$("#divCliente").hide();
    	$("#divDireto").show();
    } 
  });

$("input[name='statusReco']").click(function() {
    if ($(this).val() === 'recebida') {
    	$("#enviadaConsultor").hide();
    	$("#enviadaFilial").hide();
    	$("#recebidaFilial").show();
    	$("#recebidaComercial").show();
    } else if ($(this).val() === 'enviada') {
    	 $("#recebidaFilial").hide();
    	 $("#recebidaComercial").hide();
    	$("#enviadaConsultor").show();
    	$("#enviadaFilial").show();
    } 
  });

