$("#cnpj").blur(verificaEmpresaCadastrada);
$("#btn-cad-cliente").attr("disabled", true);

function buscaEmpresa() {
    var cnpj = $(this).val().replace(/\D/g, "");

    $.get("https://www.receitaws.com.br/v1/cnpj/" + cnpj, function (dados) {

        console.log(dados);

    })
}

function verificaEmpresaCadastrada() {
	
	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);
	
	if ( !$(this).val().length == 0 ) {

			var tiraBotao = false;

			$.getJSON("/clientes/" + cnpj +  ".json", function(dados) {

			console.log(dados);
			console.log("antes da condição")

			if (dados.erro != true) {
				console.log("if")
				$("#btn-cad-cliente").attr("disabled", true);
				Materialize.toast('CNPJ Já Cadastrado.', 5000, 'rounded');
				tiraBotao = true;

			} else if (dados.id == null) {
				console.log("else")
			}
		})
		
		if(cnpj != null && tiraBotao == false && cnpj.length == 14) {
			$("#btn-cad-cliente").attr("disabled", false);
		}
	}
}
