$("#tabela-contato-cliente").click(exibirSectionTabela);
$("#tabela-contato-cliente-hidden").click(esconderSectionTabela);
$("#add-contato-cliente").click(exibeSectionContatoEmpresa);
$("#btn-cad-contato-cliente").click(addContatoEmpresa);
$("#select-tipo-telefone").change(formataInputTelefone);
$("#fecharModalContato").click(fecharModalContato);

$(function () {

    incluirAContato();
    $("#select-tipo-telefone").material_select();
    $('.modal').modal();

});

function fecharModalContato() {
    $("#modalContatocliente").modal("close");
}

function incluirAContato() {

    $("#add-contato-cliente").text("+ Adicionar contato à ");

    var linkAddContato = $("#add-contato-cliente").text();
    var empresaDigitada = $("#nome").val();

    if (empresaDigitada != '') {
        var stringConcatenada = linkAddContato + ' ' + empresaDigitada;
        $("#add-contato-cliente").text(stringConcatenada);
    } else {
        $("#add-contato-cliente").text("+ Adicionar contato à ");
    }

}

function exibeSectionContatoEmpresa() {
    $("#contato-cliente").slideToggle(1200);
}

function formataInputTelefone() {
    var tipoTelefone = $("#select-tipo-telefone option:selected").val();

    if (tipoTelefone != "CELULAR") {
        $("#telefone").mask('(00) 0000-0000');
    } else {
        $("#telefone").mask('(00) 00000-0000');
    }

}

function exibirSectionTabela() {

    var btnExibirContatos = $("#tabela-contato-empresa");
    var btnOcultarContatos = $("#tabela-contato-empresa-hidden");
    var tabelaContatosEmpresa = $("#tabela-contatos");
    var idCliente = $("#idcliente").val();

    btnExibirContatos.stop().fadeOut(0); // Some
    btnOcultarContatos.stop().fadeIn(0); // Aparece
    tabelaContatosEmpresa.stop().slideToggle(0);

}

function zerarTabela() {
    $("#tabela-contatos #tbody-contatos tr").each(function () {
        $(this).remove();
    });
}

function esconderSectionTabela() {
    $("#tabela-contatos").stop().slideToggle(0);
    $("#tabela-contato-empresa").fadeIn(0);
    $("#tabela-contato-empresa-hidden").fadeOut(0);
    zerarTabela();
}

function novaLinha(id, nome, email, cargo, tipoTelefone, numeroTelefone,
    nfBoleto, referencia, pesqSatisfacao) {

    var icone = $("<i>");
    var linha = $("<tr>");
    var colunaId = $("<td>").text(id);
    colunaId.addClass("hidden");
    var colunaNome = $("<td>").text(nome);
    var colunaEmail = $("<td>").text(email);
    var colunaCargo = $("<td>").text(cargo);
    var colunatipoTelefone = $("<td>").text(tipoTelefone);
    var colunaNumeroTelefone = $("<td>").text(numeroTelefone);
    var colunaReceberNfBoleto = $("<td>").text((nfBoleto) ? "SIM" : "NÃO");
    var colunareferencia = $("<td>").text((referencia) ? "SIM" : "NÃO");
    var colunapesqSatisfacao = $("<td>").text((pesqSatisfacao) ? "SIM" : "NÃO");
    var colunaIconeEditar = $("<td>").append(icone);
    var linkEditar = $("<a>").attr("href", "contatos/" + id);
    var colunaIconeView = $("<td>").append(icone);
    var linkVisualizar = $("<a>").attr("onclick",
        "modalContatoEmpresa(" + id + ");");

    var colunaDeletar = $("<td>");

    var formDeletar = $("<form>").attr("action", "contatos/" + $("#idCliente").val() + "/delete/" + id).attr("method", "POST");

    var inputSubmit = $("<input>").attr("type", "submit").addClass("btn").addClass("red").addClass("white-text").attr("value", "excluir");

    formDeletar.append(inputSubmit);

    colunaDeletar.append(formDeletar);

    linkVisualizar.addClass("mouse-hand");
    colunaIconeEditar.addClass("material-icons").text("edit");
    colunaIconeView.addClass("material-icons").text("visibility");

    linkEditar.append(colunaIconeEditar);
    linkVisualizar.append(colunaIconeView);

    linha.append(colunaId);
    linha.append(colunaNome);
    linha.append(colunaEmail);
    linha.append(colunaCargo);
    linha.append(colunatipoTelefone);
    linha.append(colunaNumeroTelefone);
    linha.append(linkVisualizar);
    linha.append(linkEditar);
    linha.append(colunaDeletar);

    return linha;

}

function modalContatoEmpresa(id) {

    $
        .get(
            "contatos/details/" + id + ".json",
            function (contato) {

                var receptorNotaFiscalSim = "Receptor de nota fiscal / boleto ? : SIM";
                var receptorNotaFiscalNao = "Receptor de nota fiscal / boleto ? : NÃO";

                var referenciaSim = "Referência para novos contatos ? : SIM";
                var referenciaNao = "Referência para novos contatos ? : NÃO";

                var pesquisaSatisfacaoSim = "Participou da pesquisa de satisfação ? : SIM";
                var pesquisaSatisfacaoNao = "Participou da pesquisa de satisfação ? : NÃO";

                $("#nomeContatoModal").text(contato.nome);
                $("#emailContatoModal").text(
                    "E-mail : " + contato.email);
                $("#cargoContatoModal")
                    .text("Cargo : " + contato.cargo);
                $("#Telefone1").text(
                    "Telefone 1: " + contato.telefone1);
                $("#Telefone2").text(
                        "Telefone 2: " + contato.telefone2);
                $("#nfBoletoContatoModal").text(
                    contato.pessoaNfBoleto ? receptorNotaFiscalSim :
                    receptorNotaFiscalNao);
                $("#referenciaContatoModal").text(
                    contato.referencia ? referenciaSim :
                    referenciaNao);
                $("#pesquisaSatisfacaoContatoModal")
                    .text(
                        contato.pesquisaSatisfacao ? pesquisaSatisfacaoSim :
                        pesquisaSatisfacaoNao);
                $("#modalContatocliente").modal("open");
            }).fail(function () {
            console.log("erro");
        });

}

function addContatoEmpresa() {
    var contato = {};
    var idCliente = $("#idcliente").val();
    var cliente = {
    		id: $("#idcliente").val()
    }
    
    
    contato["nome"] = $("#contato-nome").val();
    contato["cliente"] = cliente;
    contato["email"] = $("#contato-email").val();
    contato["cargo"] = $("#contato-cargo").val();
    contato["telefone1"] = $("#telefone1").val();
    contato["telefone2"] = $("#telefone2").val();
    contato["pessoaNfBoleto"] = $("#checkPessoaNfBoleto").prop("checked") ? true :
        false;
    contato["referencia"] = $("#checkReferenciaNovosContatos").prop("checked") ? true :
        false;
    contato["pesquisaSatisfacao"] = $("#checkPesquisaSatisfacao").prop(
        "checked") ? true : false;
    
    console.log(contato);

    var urlMontada = "/clientes/contatos/add/" + idCliente;

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: urlMontada,
        data: JSON.stringify(contato) + cnpj,
        dataType: 'JSON',
        statusCode: {
            200: function () {
                Materialize.toast($("#contato-nome").val() +
                    ' Cadastrado com sucesso !', 5000, 'rounded');
                exibirSectionTabela();
                exibeSectionContatoEmpresa()
                location.reload();
            },
            500: function () {
                Materialize
                    .toast('Ops, houve um erro interno', 5000, 'rounded');
            },
            400: function () {
                Materialize.toast('Você deve estar fazendo algo de errado',
                    5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada', 5000, 'rounded');
            }
        }
    });
    
    
}
