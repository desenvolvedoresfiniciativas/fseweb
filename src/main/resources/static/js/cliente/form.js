$("#cnpj").on("input", formatarCnpj);
$("#cnpj").on("input", validarCnpj);
$("#cnpj").on("focusout", validarCnpj);
$('#site').on("focusout", function() {
	var myVariable = $("#site").val()
	if (/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(myVariable)) {
	} else {
		Materialize.toast("URL inválida no campo 'Site'.", 5000, 'rounded');
	}
});

$(function() {
	$("#divFidelizado").css("display", "none");
	$("#caminhoACT").css("display", "none");
	$("#caminhoRECO").css("display", "none");
	var changeVisibility = document.getElementById("fidelizado").innerText;
	console.log(changeVisibility);

	var x = $("#fidelizado").is(":checked");
	console.log(x)

	$('.modal').modal();

	if (x === true || x === 'checked') {
		$("#divFidelizado").css("display", "block");
		$("#dataFidelizacao").css("display", "block");
		$("#labDataFidelizacao").css("display", "block");
		$("#dataVencimentoFidelizacao").css("display", "block");
		$("#labDataVencimentoFidelizacao").css("display", "block");

		//var motivo = $("#motivoFidelizacao").text();
		/*console.log(motivo)*/
		var motivos = document.getElementById('motivoFidelizacao')
		console.log(motivos)
		var motivo = motivos.options[motivos.selectedIndex].text;
		console.log(motivo)

		if (motivo === 'ACT') {
			$("#caminhoACT").css("display", "block");
			$("#labelACT").css("display", "block");
		} else {
			$("#caminhoACT").css("display", "none");
			$("#labelACT").css("display", "none");
		}
		
		if (motivo === 'RECO') {
			$("#caminhoRECO").css("display", "block");
			$("#labelRECO").css("display", "block");
		} else {
			$("#caminhoRECO").css("display", "none");
			$("#labelRECO").css("display", "none");
		}
		
		if (motivo === 'ELOGIOS') {
			console.log("elogio teste")
			$("#fidelizacaoElogio").css("display", "block");
			$("#labelElogio").css("display", "block");
		} else {
			$("#fidelizacaoElogio").css("display", "none");
			$("#labelElogio").css("display", "none");	
		}
	} else {
		$("#divFidelizado").css("display", "none");
		$("#caminhoACT").css("display", "none");
		$("#labelACT").css("display", "none");
		$("#caminhoRECO").css("display", "none");
		$("#labelRECO").css("display", "none");
		$("#dataFidelizacao").css("display", "none");
		$("#labDataFidelizacao").css("display", "none");
		$("#dataVencimentoFidelizacao").css("display", "none");
		$("#labDataVencimentoFidelizacao").css("display", "none");
		$("#fidelizacaoElogio").css("display", "none");
		$("#labelElogio").css("display", "none");	
		var printDefault = document.getElementById("motivoFidelizacao").options[0].value
		console.log(printDefault)
		document.getElementById("motivoFidelizacao").selectedIndex = 0;

		/*$("#motivoFidelizacao").val("NONE")
		$("#motivoFidelizacao").text("")*/
		/*$("#motivoFidelizacao").css("display","none")*/
	}
})

$('#fidelizado').change(function() {
	var changeVisibility = document.getElementById("fidelizado");
	changeVisibility.addEventListener("click", console.log('clicou'))
	cv = changeVisibility.value

	var x = $("#fidelizado").is(":checked");
	console.log(x)
	var motivos = document.getElementById('motivoFidelizacao')
	console.log(motivos)

	if (x === true || x === 'checked') {
		$("#divFidelizado").css("display", "block");
		$("#dataFidelizacao").css("display", "block");
		$("#labDataFidelizacao").css("display", "block");
		$("#dataVencimentoFidelizacao").css("display", "block");
		$("#labDataVencimentoFidelizacao").css("display", "block");
	} else {
		var testeMotivo1 = $("#motivoFidelizacao option:selected").text()
		console.log('teste de valor ' + testeMotivo1)
		$('#motivoFidelizacao').val("")
		var testeMotivo = $("#motivoFidelizacao option:selected").text()
		console.log('teste de valor ' + testeMotivo)

		$("#motivoFidelizacao").material_select();
		$("#divFidelizado").css("display", "none");
		$("#caminhoACT").css("display", "none");
		$("#labelACT").css("display", "none");
		$("#caminhoRECO").css("display", "none");
		$("#labelRECO").css("display", "none");
		$("#dataFidelizacao").css("display", "none");
		$("#labDataFidelizacao").css("display", "none");
		$("#dataVencimentoFidelizacao").css("display", "none");
		$("#labDataVencimentoFidelizacao").css("display", "none");
		$("#fidelizacaoElogio").css("display", "none");
		$("#labelElogio").css("display", "none");
	}
	desbloqueiaBotão()
})

$('#motivoFidelizacao').change(function() {
	var motivos = document.getElementById('motivoFidelizacao')
	console.log(motivos)
	var motivo = motivos.options[motivos.selectedIndex].text;
	console.log(motivo)

	if (motivo === 'ACT') {
		$("#caminhoACT").css("display", "block");
		$("#labelACT").css("display", "block");
	} else {
		$("#caminhoACT").css("display", "none");
		$("#labelACT").css("display", "none");
	}
	
	if (motivo === 'RECO') {
		$("#caminhoRECO").css("display", "block");
		$("#labelRECO").css("display", "block");
	} else {
		$("#caminhoRECO").css("display", "none");
		$("#labelRECO").css("display", "none");
	}

	if (motivo === 'REFERÊNCIA') {
		openModalFidelizado();
	}

	if (motivo === 'ELOGIOS') {
			console.log("elogio teste")
			$("#fidelizacaoElogio").css("display", "block");
			$("#labelElogio").css("display", "block");
		} else {
			$("#fidelizacaoElogio").css("display", "none");
			$("#labelElogio").css("display", "none");	
		}
	desbloqueiaBotão();
})

$($("#caminhoACT")).change(function() {
	desbloqueiaBotão();
})	

$($("#caminhoRECO")).change(function() {
	desbloqueiaBotão();
})

$($("#fidelizacaoElogio")).change(function() {
	desbloqueiaBotão();
})	

$($("#dataFidelizacao")).change(function() {
	//dataFidadelização vencimento é a dataFidelização + 2 anos;
	var dataFidelizacao = $("#dataFidelizacao").val();
	//pegando a dataFidelização e separando cada informaçao
	var data = dataFidelizacao.split("/");
	var dia = data[0];
	var mes = data[1];
	var ano = data[2];
	
	//somando 2 anos ao ano
	ano = parseInt(ano);
	ano = ano+2;
	ano = ano.toString();
	//criando  dataVencimentoFidelizaçao 
	var dataVencimento = dia+"/"+mes+"/"+ano;
	//colocando no html dataVencimentoFidelizaçao
	$("#dataVencimentoFidelizacao").val(dataVencimento)
	$("#dataVencimentoFidelizacao").focusin();
	desbloqueiaBotão();
})

$(function() {
	$('.select').material_select();
	$("#cnpj").tooltipster({
		trigger: "custom"
	});

	formatarCnpj();
	

	//$("#cidade").prop("readonly", true);

	//$("#estado").prop("readonly", true);

});

function formatarCnpj() {
	$("#cnpj").mask('00.000.000/0000-00', {
		reverse: true
	});
}

function formatarRUT() {
	$("#rut").mask('00.000.000-0', {
		reverse: true
	});
}

function validarCnpj() {
	var caracteresCnpj = $("#cnpj").val().length;
	if (caracteresCnpj < 18) {
		$("#cnpj").tooltipster("open").tooltipster("content", "CNPJ inválido");
	} else {
		$("#cnpj").tooltipster("close");
	}

}

function addEnderecoFirst() {

	var clienteE = {
		id: $("#idcliente").val()
		
	};
	

	var endereco = {
		cep: $("#cep").val(),
		logradouro: $("#endereco").val(),
		numero: $("#numero").val(),
		cidade: $("#cidade").val(),
		tipo: $("#tipo").val(),
		bairro: $("#bairro").val(),
		estado: $("#estado").val(),
		complemento: $("#complemento").val(),
		tipo: $("#tipo :selected").val(),
		enderecoNotaFiscalBoleto: $("#checkEnderecoNotaFiscalBoleto").is(":checked")
	};

	console.log(endereco);

	var urlMontada = "/clientes/enderecoAdd"

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: urlMontada,
		data: JSON.stringify(endereco),
		dataType: 'JSON',
		statusCode: {
			200: function() {
				Materialize.toast('Cadastrado com sucesso !', 5000, 'rounded');
				exibirSectionTabela();
				exibeSectionContatoEmpresa();
			},
			500: function() {
				Materialize
					.toast('Ops, houve um erro interno', 5000, 'rounded');
			},
			400: function() {
				Materialize.toast('Você deve estar fazendo algo de errado',
					5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada', 5000, 'rounded');
			}
		}
	});
}

function openModalFidelizado() {
	let a = $("#contatoReferencia").is(":visible");
	console.log('método abre modal', a)
	$('#contatoReferencia').modal('open');
};

function salvarContatoReferencia() {
	console.log('entrou na função para salvar contato')

	$('table [type="checkbox"]').each(function(i, chk) {
		if (chk.checked) {
			console.log("o empresa selecionada foi", i, chk);

			const empresa = chk.value;
			console.log(empresa)

			var url = "/clientes/" + empresa + "/.json"

			return $.ajax({
				type: 'PUT',
				url: url,
				success: function(data) {
					console.log('ajax acionado' + data.nome)
					console.log(data.referencia)
					data.referencia = true;
					console.log(data.referencia)
					Materialize
						.toast(
							'Contato ' + data.nome + ' salvo com sucesso como uma referência',
							5000, 'rounded');
				}
			});
		}
	})
}

function fecharModalContato() {
	$('#contatoReferencia').modal('close');
	let a = $("#contatoReferencia").is(":visible");
	console.log('método fecha modal', a)
}

function updateDataFidelização(){
	console.log("entrou updateDataFidelização")
	
	var cnpj = $("#cnpj").val()
	console.log("cnpj",cnpj)
	 $.getJSON("/contrato/" + cnpj + ".json", function (dados) {

        if (dados.erro != true) {
			console.log("dados",dados)
            $("#idcliente").val(dados.id);
            $("#idcliente").focusin();
        } else {
            Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
            limparNomeEmpresa();
        }
    })
    var idCliente = $("#idcliente").val()
	console.log("id do cliente",idCliente);
	var data = $("#dataFidelizacao").val()
	console.log("dataFidelizacao",dataFidelizacao);
	
	 $.ajax({
        type: 'POST',
        url: "/clientes/" + idCliente + "/" + data+ "/updateDatafidelizacao.json",
        success: function (data) {

		}
	});
}

function bloqueiaBotão(){
		console.log("entrou no bloqueia botão");
	var checkFidelizado = $("#fidelizado").is(":checked");
	var caminhoAct = $("#caminhoACT").val();
	var caminhoReco = $("#caminhoRECO").val();
	var caminhoElogio = $("#fidelizacaoElogio").val();
	var motivos = document.getElementById('motivoFidelizacao')
	var motivo = motivos.options[motivos.selectedIndex].text;
	var emptySelecione = (motivo === "");
	var data = $("#dataFidelizacao").val()
	console.log("caminhoReco",caminhoReco);
		if(checkFidelizado === true){
			
			if (motivo === 'ACT' &&  caminhoAct == "") {
				Materialize.toast('Preencher caminho ACT ', 5000, 'rounded');
				$('#btn-att-cliente').attr("disabled", true);
			}
			if (motivo === 'RECO' &&  caminhoReco == "") {
				Materialize.toast('Preencher caminho RECO ', 5000, 'rounded');
				$('#btn-att-cliente').attr("disabled", true);
			}
			if (motivo === 'ELOGIOS' &&  caminhoElogio == "" ) {
				Materialize.toast('Preencher caminho ELOGIOS ', 5000, 'rounded');
				$('#btn-att-cliente').attr("disabled", true);
			}
			if (motivo == "Selecione") {
				Materialize.toast('Selecione Motivo Fidelização ', 5000, 'rounded');
				$('#btn-att-cliente').attr("disabled", true);
			}
			if(emptySelecione === true){
				Materialize.toast('Selecione Motivo Fidelização', 5000, 'rounded');
				$('#btn-att-cliente').attr("disabled", true);
			}
			
			if(data == ""){
				Materialize.toast('Selecione a Data de  Fidelização', 5000, 'rounded');
				$('#btn-att-cliente').attr("disabled", true);
			}
		}
		else{
				if(checkFidelizado === false){
				$('#btn-att-cliente').attr("disabled", false);
				}
		}	
}

function desbloqueiaBotão(){
	console.log("entrou no desbloqueia")
	var checkFidelizado = $("#fidelizado").is(":checked");
	var caminhoAct = $("#caminhoACT").val();
	var caminhoReco = $("#caminhoRECO").val();
	var caminhoElogio = $("#fidelizacaoElogio").val();
	var motivos = document.getElementById('motivoFidelizacao')
	var motivo = motivos.options[motivos.selectedIndex].text;
	var emptySelecione = (motivo === "")
	var data = $("#dataFidelizacao").val()

		if (motivo === 'ACT' &&  caminhoAct != "")  {
			$('#btn-att-cliente').attr("disabled", false);
		}
		if (motivo === 'RECO' &&  caminhoReco != "")  {
			$('#btn-att-cliente').attr("disabled", false);
		}
		if  (motivo === 'ELOGIOS' &&  caminhoElogio != "" ){
			$('#btn-att-cliente').attr("disabled", false);
		}
		if(checkFidelizado === false){
			$('#btn-att-cliente').attr("disabled", false);
		} else {
			if (motivo != "Selecione" && motivo !='ACT' && motivo !='RECO' && motivo !='ELOGIOS' && emptySelecione === false && data != "") {
				$('#btn-att-cliente').attr("disabled", false);
				console.log("entrou no if gigante true")
			} else {
				bloqueiaBotão()
			}
		}
}

$("#form").submit(function () {
        $("#btn-cad-cliente").attr("disabled", true);
        return true;
});