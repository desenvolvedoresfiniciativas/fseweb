$("#nit").on("input", formatarNIT);
$('#site').on("focusout", function() {
	var myVariable = $("#site").val()
	if(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(myVariable)){
	} else {
		Materialize.toast("URL inválida no campo 'Site'.", 5000, 'rounded');
	}
});

$(function () {
	
	$('.select').material_select();
	formatarNIT();

});

function formatarNIT() {
    $("#nit").mask('000.000.000-0', {
        reverse: true
    });
}


function addEnderecoFirst() {
    
    var clienteE = {
    		id: $("#idcliente").val()
    };
    
    
    var endereco = {
    		cep: $("#cep").val(),
    		logradouro: $("#endereco").val(),
    		numero: $("#numero").val(),
    		cidade: $("#cidade").val(),
    		tipo: $("#tipo").val(),
    		bairro: $("#bairro").val(),
    		estado: $("#estado").val(),
    		complemento: $("#complemento").val(),
    		tipo: $("#tipo :selected").val(),
    		enderecoNotaFiscalBoleto: $("#checkEnderecoNotaFiscalBoleto").is(":checked")
    		};
    
    console.log(endereco);

    var urlMontada = "/clientes/enderecoAdd"

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: urlMontada,
        data: JSON.stringify(endereco),
        dataType: 'JSON',
        statusCode: {
            200: function () {
                Materialize.toast('Cadastrado com sucesso !', 5000, 'rounded');
                exibirSectionTabela();
                exibeSectionContatoEmpresa();
            },
            500: function () {
                Materialize
                    .toast('Ops, houve um erro interno', 5000, 'rounded');
            },
            400: function () {
                Materialize.toast('Você deve estar fazendo algo de errado',
                    5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada', 5000, 'rounded');
            }
        }
    });
}
