$("#cnpj").on("input", formatarCnpj);
$("#cnpj").on("input", validarCnpj);
$("#cnpj").on("focusout", validarCnpj);
$('#site').on("focusout", function() {
	var myVariable = $("#site").val()
	if(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(myVariable)){
	} else {
		Materialize.toast("URL inválida no campo 'Site'.", 5000, 'rounded');
	}
});

$(function () {
	
	$('.tooltipped').tooltip({delay: 50,html: true,
	tooltip: "Micro 1: 0 a 200 UF Anuales"
+"<br/> Micro 2: 201 a 600 UF Anuales"
+"<br/> Micro 3: 601 a 2.400 UF Anuales"
+"<br/> Pequeña 1: 2.401 a 5.000 UF Anuales"
+"<br/> Pequeña 2: 5.001 a 10.000 UF Anuales"
+"<br/> Pequeña 3: 10.001 a 25.000 UF Anuales"
+"<br/> Mediana 1: 25.001 a 50.000 UF Anuales"
+"<br/> Mediana 2: 50.001 a 100.000 UF Anuales"
+"<br/> Grande 1: 100.001 a 200.000 UF Anuales"
+"<br/> Grande 2: 200.001 a 600.000 UF Anuales"
+"<br/> Grande 3: 600.001 a 1.000.000 UF Anuales"
+"<br/>Grande 4: más de 1.000.000 UF Anuales"});
	
	$('.select').material_select();
    $("#cnpj").tooltipster({
        trigger: "custom"
    });

    formatarCnpj();
    formatarRUT()

    //$("#cidade").prop("readonly", true);

    //$("#estado").prop("readonly", true);

});

function formatarCnpj() {
    $("#cnpj").mask('00.000.000/0000-00', {
        reverse: true
    });
}

function formatarRUT() {
    $("#rut").mask('00.000.000-0', {
        reverse: true
    });
}

function validarCnpj() {
    var caracteresCnpj = $("#cnpj").val().length;
    if (caracteresCnpj < 18) {
        $("#cnpj").tooltipster("open").tooltipster("content", "CNPJ inválido");
    } else {
        $("#cnpj").tooltipster("close");
    }
       
}

function addEnderecoFirst() {
    
    var clienteE = {
    		id: $("#idcliente").val()
    };
    
    
    var endereco = {
    		cep: $("#cep").val(),
    		logradouro: $("#endereco").val(),
    		numero: $("#numero").val(),
    		cidade: $("#cidade").val(),
    		tipo: $("#tipo").val(),
    		bairro: $("#bairro").val(),
    		estado: $("#estado").val(),
    		complemento: $("#complemento").val(),
    		tipo: $("#tipo :selected").val(),
    		enderecoNotaFiscalBoleto: $("#checkEnderecoNotaFiscalBoleto").is(":checked")
    		};
    
    console.log(endereco);

    var urlMontada = "/clientes/enderecoAdd"

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: urlMontada,
        data: JSON.stringify(endereco),
        dataType: 'JSON',
        statusCode: {
            200: function () {
                Materialize.toast('Cadastrado com sucesso !', 5000, 'rounded');
                exibirSectionTabela();
                exibeSectionContatoEmpresa();
            },
            500: function () {
                Materialize
                    .toast('Ops, houve um erro interno', 5000, 'rounded');
            },
            400: function () {
                Materialize.toast('Você deve estar fazendo algo de errado',
                    5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada', 5000, 'rounded');
            }
        }
    });
}
