$("#formDeleteEmpresa").submit(function() {
	console.log("submitou !")
});

$(function() {
	filtroTabela();
	$(".modal").modal();
	formataRUTTabela()
})


function formataRUTTabela() {
	$('.rut').each(function() {
		var cellText = $(this).html();
		cellText = cellText.replace( /^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
		$($(this)).html(cellText);
	})
}

function linkAlterar(cnpj) {
	var url = "/empresas/";

	window.location.replace(url + "'" + cnpj + "'");

	$("#alterar-" + cnpj).trigger("click");

}

function filtroTabela() {
	$('#txt-pesq-cliente').on(
			'keyup',
			function() {
				var nomeFiltro = $(this).val().toLowerCase();
				console.log(nomeFiltro);
				$('#tabela-clientes').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(2)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula.toLowerCase()
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}
