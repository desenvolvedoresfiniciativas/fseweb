$(function () {
	$('.progress').hide();
	$('.select').material_select();
})

$.each($('.valorClass'), function () {
	var valorAtual = $(this).text()
	if(valorAtual == 'R$ ,00') {
		$(this).text('R$ 0,00')
	}
})

//$.ajaxSetup({
//   beforeSend :$('.progress').show(),
//   complete: $('.progress').hide(),
//});

$(function maskValor(){
	$(".moneyClass").each(function(){
		var texto = $(this).text();
		texto = parseFloat(texto).toFixed(2).toLocaleString('pt-BR')
		texto = mascaraValor(texto)
		$(this).text("R$ " + texto);
	})
})

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

$(function formataCnpj() {
	$.each($('.cnpjClass'), function () {		
		var cnpjAtual = $(this).text().replaceAll();
		cnpjAtual = cnpjAtual.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");
		$(this).text(cnpjAtual);
})
})

function maskValor(){
$(".money").each(function(){
	var texto = $(this).text();
	texto = parseFloat(texto).toLocaleString('pt-BR')
	texto = "R$ "+texto
	$(this).text(texto);
})
}

$('#geraVN').click(function(){
	
var ano = $('#ano').find(":selected").val()
		var div = document.querySelector("div[class*='preloader-wrapper big active loader']");
		div.style.display = "block";
		/*div.style.display = "none";*/
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/relatorios/gerarVN",
			data: ano,
			dataType:"JSON",
	        statusCode: {
	            200: function () {
	                Materialize
	                    .toast(
	                        'Itens salvos com sucesso!',
	                        5000, 'rounded');
					window.location.replace("vNegocio?ano=" + ano)
	            },
	            500: function () {
	                Materialize.toast(
	                    'Ops, houve um erro interno',
	                    50000, 'rounded');
						div.style.display = "none";
	            },
	            400: function () {
	                Materialize
	                    .toast(
	                        'Você deve estar fazendo algo de errado',
	                        50000, 'rounded');
							div.style.display = "none";
	            },
	            404: function () {
	                Materialize.toast('Url não encontrada',
	                    50000, 'rounded');
					div.style.display = "none";
	            }
	        }	
		});
		
		
})

$('#loadBtn').click(function(){
	console.log('clicou');
	var ano = $("#ano").val()
	console.log('o ano é: ' + ano)
	
	window.location.href = "/relatorios/vNegocio?ano=" + ano;
})

function fnExcelReport() {
    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Valor de Negócio</x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + $('#tabela-vn').html();
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, 'vn.xls');
        }
    } else {
        $('#excelBtn').attr('href', data_type + ', ' + escape(tab_text));
        $('#excelBtn').attr('download', 'vn.xls');
    }
}

$(function trataPercentuais() {
	$.each($('.percentualClass'), function () {

		var percentualTexto = $(this).text().replaceAll("%", "");
		var percentualAtual = percentualTexto
		if(percentualAtual % 1 == 0) {
			percentualAtual = parseFloat(percentualAtual).toFixed(0)
		} else {
			//número com decimais
		}
		
		console.log(percentualAtual, " percentualAtual")
		$(this).text(percentualAtual + "%");
})
})
