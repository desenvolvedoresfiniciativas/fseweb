$()

$(function () {
	$('.modal').modal();
	formataVN()
})


function botaoEditar(idVN){
	var id = idVN;
	console.log("idVN",id);
	var url = "BuscaVNAtt/"+id+".json"

    return $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data) {
        	console.log(data)
        	
        	var valorBase = data.valorBase
			valorBase = valorBase.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
       	 	valorBase= valorBase.replace("R","")
    	 	valorBase= valorBase.replace('$',"")
    	 	valorBase = valorBase.replace(/\s/g, '');
        	
        	
        	$('#idVN').val(data.id)
        	$('#idVN').focusin();
        	$('#valor_base').val(valorBase)
        	$('#valor_base').focusin();
        	$('#modalAtt').modal('open');
        }
    });
}

$('.salvaVN').on("click", function(){

	var valorBase = $('#valor_base').val();
	valorBase = valorBase.replace(/\./g,'')
	valorBase = valorBase.replace(",",".")
	valorBase = parseFloat(valorBase).toFixed(2)
	
	var vn = {
			id: $('#idVN').val(),
			valorBase: valorBase,
	}
	
	$.ajax({
	type: "POST",
	contentType: "application/json",
	url: "/relatorios/updateVN",
	data: JSON.stringify(vn),
	dataType:"JSON",
    statusCode: {
        200: function () {
            Materialize
                .toast(
                    'Itens salvos com sucesso!',
                    5000, 'rounded');
            if(etapaFinalizada==true){
            	 Materialize
                 .toast(
                     'Etapa de Trabalho finalizada.',
                     5000, 'rounded');
            	$('#modalEficiencia').modal('open');
            }else{
            	alert("deu ruim")
            }
        },
        500: function () {
            Materialize.toast(
                'Ops, houve um erro interno',
                5000, 'rounded');
        },
        400: function () {
            Materialize
                .toast(
                    'Você deve estar fazendo algo de errado',
                    5000, 'rounded');
        },
        404: function () {
            Materialize.toast('Url não encontrada',
                5000, 'rounded');
        }
    }	
});

	
})

function formataVN(){
	
		$.each($('.vn'), function () {
			var vn = $(this).text();
			
			vn = vn.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
       	 	vn= vn.replace("R","")
    	 	vn= vn.replace('$',"")
    	 	vn = vn.replace(/\s/g, '');
    	 	
    	 	console.log("FORMATAÇÃO "+vn)
    	 	
    	 	$(this).text('R$ '+mascaraValor(parseFloat(vn).toFixed(2)))
			
		})
}

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g, "");
    valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
    return valor
}


