var etapasTrabalhoHolder = new Array();

let primeiroLoad = 1;

$(function paginaInicial() {
	
	var gif = document.querySelector("div[class*='preloader-wrapper big active loader']");
	gif.style.display = "none"

	$('.select').material_select();
	carregaPontoSituacao();
	filtrosEtapas();
	filtroNome();
	filtroAno();
	filtroColab();
	filtroEquipe();
	carregaIndicadores();
	


});

$(document).on("click",".buscar",function() {
	console.log('entrou')
	
	var gif = document.querySelector("div[class*='preloader-wrapper big active loader']");
	gif.style.display = "block"
	
	//totais
	var totalSimAbertura = 0;
	var totalNaoAbertura = 0;
	var totalSimIdentificacao = 0;
	var totalNaoIdentificacao = 0;
	var totalSimQuantificacao = 0;
	var totalNaoQuantificacao = 0;
	var totalSimDefesa = 0;
	var totalNaoDefesa = 0;
	var totalSimSubmissao = 0;
	var totalNaoSubmissao = 0;
	var totalSimEncerramento = 0;
	var totalNaoEncerramento = 0;
	var totalSimformCliente = 0;
	var totalNaoformCliente = 0;
	var totalSimEr = 0;
	var totalNaoEr = 0;
	
	//get filtros 
	var ano = ($('#campanha') != null ? $('#campanha').val() : "")
	var razaoSocial = $('#txt-pesq-cliente').val()
	var sigla = $('#selectColaboradores').val()
	var equipe = $('#equipes').val()
	var abertura = $('#abertura').val()
	var identificacao = $('#identificacao').val()
	var quantificacao = $('#quantificacao').val()
	var defesa = $('#defesa').val()
	var submissao = $('#submissao').val()
	var encerramento = $('#encerramento').val()
	var formCliente= $('#formCliente').val()
	var er = $('#Er').val()
	console.log('o form cliente é', formCliente)

	
	var parametros = {
		ano: ano,
		razaoSocial: razaoSocial,
		sigla: sigla,
		equipe:equipe,
		abertura: abertura,
		identificacao: identificacao,
		quantificacao: quantificacao,
		defesa:defesa,
		submissao:submissao,
		encerramento:encerramento,
		formCliente:formCliente,
		er:er
		
	}
	   
       $.ajax({
     			type: "post",
     			url: "/pontoSituacao/carrega",
     			data: JSON.stringify(parametros),
				headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
				},
				success : 
                    function(JSONObject) {
                        console.table(JSONObject)	
                       // $("#tabela-clientes").empty()
                       
                        $("#etapasTrabalho").empty()
                        $("#tabela").empty()
                        $('#tabela-clientes').empty();
						$('#tabela-clientes').append('<thead id="etapasTrabalho"> </thead> <tbody id="tabela">  </tbody>');
					//	$('#tabela-clientes').DataTable().draw();
                        
                    //    $("#tabela-clientes").append('<thead>');
                    //    $("#tabela-clientes").append('<tr id="etapasTrabalho"></tr>');
                        var trTitulo = $('<tr> </tr>')
                        
                        trTitulo.append('<th><b>Cliente</b></th>');
						trTitulo.append('<th><b>Ano</b></th>');
						trTitulo.append('<th><b>Equipe</b></th>');
						trTitulo.append('<th><b>Consultor</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>AM</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>DP</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>CALC</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>REL</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>FORM CLIENTE</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>SUBMISSÃO</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>ER</b></th>');
						trTitulo.append('<th style="text-align: center;"><b>DOSSIÊ FINAL</b></th>');
//						trTitulo.append('<th style="text-align: center;"><b>GEROU FATURAMENTO</b></th>');
						
						$("#etapasTrabalho").append(trTitulo);
						
						//$("#tabela-clientes").append('</thead>');
						
					//	$("#tabela-clientes").append('<tbody id="tabela">');
						//$("#tabela-clientes").append('</tbody>');
						
						var count = 0
						JSONObject.forEach(function(){
						
							var corAbertura = JSONObject[count].abertura == 'Sim' ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							JSONObject[count].abertura == 'Sim' ? totalSimAbertura++ : totalNaoAbertura++
							console.log('o total abertura sim é', totalSimAbertura, 'o total não é', totalNaoAbertura)
							var corIdentificacao = JSONObject[count].identificação == 'Sim' ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							JSONObject[count].identificação == 'Sim' ? totalSimIdentificacao++ : totalNaoIdentificacao++
							try{
								var primeiroValorQuantificacao = JSONObject[count].quantificação.substring(JSONObject[count].quantificação.indexOf('/') + 1);
							} catch(e){
								var primeiroValorQuantificacao = 0
							}
							try{
							var segundoValorQuantificacao = JSONObject[count].quantificação.split('/')[0];
							} catch(e){
							var segundoValorQuantificacao = 0
							}
							var corQuantificacao = primeiroValorQuantificacao == segundoValorQuantificacao ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							primeiroValorQuantificacao == segundoValorQuantificacao ? totalSimQuantificacao++ : totalNaoQuantificacao++
							if(primeiroValorQuantificacao == 0 && segundoValorQuantificacao == 0){
								corQuantificacao = bgcolor="#ef5350"
								totalSimQuantificacao--
								totalNaoQuantificacao++
							}
							var primeiro = JSONObject[count].defesa.substring(JSONObject[count].defesa.indexOf('/') + 1);
							var segundo = JSONObject[count].defesa.split('/')[0];
							var corDefesa = primeiro == segundo ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							primeiro == segundo ? totalSimDefesa++ : totalNaoDefesa++
							if(primeiro == 0 && segundo == 0){
								corDefesa = bgcolor="#ef5350"
								totalSimDefesa--
								totalNaoDefesa++
							}
							var corSubmissao = JSONObject[count].submissao == 'Sim' ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							JSONObject[count].submissao == 'Sim' ? totalSimSubmissao++ : totalNaoSubmissao++
							var corEncerramento = JSONObject[count].encerramento == 'Sim' ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							JSONObject[count].encerramento == 'Sim' ? totalSimEncerramento++ : totalNaoEncerramento++
							
							var corFormCliente = JSONObject[count].FormCliente == 'Sim' ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							JSONObject[count].FormCliente == 'Sim' ? totalSimformCliente++ : totalNaoformCliente++
							
							var corEr = JSONObject[count].ER === 'Sim' ? bgcolor="#2bbbad" : bgcolor="#ef5350"
							JSONObject[count].ER == 'Sim' ? totalSimEr++ : totalNaoEr++
							console.log("o valor do FormCliente é: " + JSONObject[count].formCliente)
							console.log("o valor do ER é: " + JSONObject[count].ER)
							
//							var corGerouFaturamento = JSONObject[count].gerouFaturamento == 'Sim' ? bgcolor="#2bbbad" : bgcolor="#ef5350"
//							JSONObject[count].gerouFaturamento == 'Sim' ? totalSimSubmissao++ : totalNaoSubmissao++
							
//							console.log(corGerouFaturamento)

//							tdGerouFaturamento = $('<td class="gerouFaturamento" '+ corGerouFaturamento +' >' + '' + '</td>') 
							 // $("#tabela-clientes").append('<tr>');
							  
							  var trLinha = $('<tr> </tr>')
							  console.log(JSONObject[count])
							  var tdRazaoSocial = $(' <td > <a href="/producoes/update/'+ JSONObject[count].idProducao+'"> ' + JSONObject[count].razaoSocial + '</a></td>')
							  var tdAno = $('<td >' + JSONObject[count].ano + '</td>')
							  var tdEquipe = $('<td >' + JSONObject[count].equipe + '</td>')
							  var tdConsultor = $('<td >' + JSONObject[count].consultor + '</td>') 
							  var tdAbertura = $('<td class="indicador " style="text:align:center;" bgcolor=' +  corAbertura + '> ' + JSONObject[count].abertura + '</td>') 
							  var tdIdentificacao = $('<td class="indicador" bgcolor=' + corIdentificacao + '>' + JSONObject[count].identificação + '</td>') 
							  var tdQuantificacao = $('<td class="indicador" bgcolor=' + corQuantificacao + '>' + JSONObject[count].quantificação + '</td>') 
							  var tdDefesa = $('<td class="indicador" bgcolor=' + corDefesa + '>' + JSONObject[count].defesa + '</td>') 
							  var tdformCliente = $('<td class="indicador" bgcolor=' + corFormCliente + '>' + JSONObject[count].FormCliente + '</td>') 
							  var tdSubmissao = $('<td class="indicador" bgcolor=' + corSubmissao + '>' + JSONObject[count].submissao + '</td>') 
							  var tdEr = $('<td class="indicador" bgcolor=' + corEr + '>' + JSONObject[count].ER + '</td>') 
							  var tdEncerramento = $('<td class="indicador" bgcolor=' + corEncerramento + '>' + JSONObject[count].encerramento + '</td>') 
//							  var tdGerouFaturamento = $('<td class="indicador" bgcolor=' + corGerouFaturamento + '>' + JSONObject[count].gerouFaturamento + '</td>') 

//		var tdGerouFaturamento = $('<td class="gerouFaturamento" bgcolor=#ef5350'  + '>' +  + '</td>')
							  
							  trLinha.append(tdRazaoSocial).append(tdAno).append(tdEquipe).append(tdConsultor).append(tdAbertura)
							  .append(tdIdentificacao).append(tdQuantificacao).append(tdDefesa).append(tdformCliente).append(tdSubmissao).append(tdEr)
							  .append(tdEncerramento)
//					.append(tdGerouFaturamento)
							  
							  $("#tabela").append(trLinha)
							  
							   // bgcolor="#ef5350" vermelho
							   //bgcolor="#2bbbad" verde
					
					/*		 
    						  $("#tabela").append('<td >' + JSONObject[count].razaoSocial + '</td>');
    						  $("#tabela").append('<td">' + JSONObject[count].ano + '</td>');
    						  $("#tabela").append('<td>' + JSONObject[count].equipe + '</td>');
    						  $("#tabela").append('<td>' + JSONObject[count].consultor + '</td>');
    						  if(JSONObject[count].abertura == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad"class="indicador">' + JSONObject[count].abertura + '</td>');
							  }else{
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].abertura + '</td>');
							  }
							  if(JSONObject[count].identificação == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].identificação + '</td>');
							  }else{
								$("#tabela").append('<td  bgcolor="#ef5350" class="indicador">' + JSONObject[count].identificação + '</td>');
							  }
							  if(JSONObject[count].quantificação == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].quantificação + '</td>');
							  }else{
								$("#tabela").append('<td  bgcolor="#ef5350" class="indicador">' + JSONObject[count].quantificação + '</td>');
							  }
							  if(JSONObject[count].defesa == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].defesa + '</td>');
							  }else{
								$("#tabela").append('<td  bgcolor="#ef5350"class="indicador">' + JSONObject[count].defesa + '</td>');
							  }
							  if(JSONObject[count].submissao == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].submissao + '</td>');
							  }else{
								$("#tabela").append('<td  bgcolor="#ef5350" class="indicador">' + JSONObject[count].submissao + '</td>');
							  }
							  if(JSONObject[count].encerramento == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].encerramento + '</td>');
							  }else{
								$("#tabela").append('<td  bgcolor="#ef5350" class="indicador">' + JSONObject[count].encerramento + '</td>');
							  }
							  if(JSONObject[count].mcti == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].mcti + '</td>');
							  }else{
								$("#tabela").append('<td  bgcolor="#ef5350" class="indicador">' + JSONObject[count].mcti + '</td>');
							  }
							  if(JSONObject[count].rfb == 'sim'){
								$("#tabela").append('<td bgcolor="#2bbbad" class="indicador">' + JSONObject[count].rfb + '</td>');
							  }else{
								$("#tabela").append('<td  bgcolor="#ef5350" class="indicador">' + JSONObject[count].rfb + '</td>');
							  }
    						  
    						  $("#tabela").append('</tr>');
    						  
    					*/	  
    						  
    						  console.log(count)
    						  count ++;
						});
					
			//			linebreak = document.createElement("br");
			//			var totaisAbertura = 'o total de sim é:' +totalSimAbertura   + 
			//			'O total não é:' + totalNaoAbertura + linebreak
			//			$("#countAbertura").text(totaisAbertura)

			/*			
						var totaisAbertura = '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						$("#aberturaMissao").append(totaisAbertura)
						
						var totaisIdentificacao = '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						$("#identificacaoTecnica").append(totaisIdentificacao)
						
						var totaisQuantificacao = '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						$("#quantificacaoCard").append(totaisQuantificacao)
						
						var totaisDefesa = '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						$("#defesaTecnica").append(totaisDefesa)
						
						var totaisSubmissao = '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						$("#submissaoMCTI").append(totaisSubmissao)
						
						var totaisEncerramento= '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						$("#submissaoMCTI").append(totaisEncerramento)
						
						var totaisMcti = '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						$("#submissaoMCTI").append(totaisMcti)
						
						var totaisRfb = '</b> </span></br>  <br> <span class="white-text" style="font-size: 1.2em"> <b> Total finalizados: </b> <b>' + totalSimAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Total de não finalizados: ' + totalNaoAbertura + 
						'</b> </span> <br> <span class="white-text" style="font-size: 1.2em"> <b>Avanço total: ' + (totalSimAbertura/JSONObject.length) * 100 + '%'  + '</b> </span></br>'
						
						$("#submissaoMCTI").append(totaisRfb)
				*/		
				
				
						
						$("#totalSimAbertura").text(totalSimAbertura)
						$("#totalNaoAbertura").text(totalNaoAbertura)
						$("#totalSimAberturaPercent").text(((totalSimAbertura/JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoAberturaPercent").text(((totalNaoAbertura/JSONObject.length) * 100).toFixed(2) + '%')
						
						$("#totalSimIdentificacao").text(totalSimIdentificacao)
						$("#totalNaoIdentificacao").text(totalNaoIdentificacao)
						$("#totalSimIdentificacaoPercent").text(((totalSimIdentificacao/JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoIdentificacaoPercent").text(((totalNaoIdentificacao/JSONObject.length) * 100).toFixed(2) + '%')
						
						$("#totalSimQuantificacao").text(totalSimQuantificacao)
						$("#totalNaoQuantificacao").text(totalNaoQuantificacao )
						$("#totalSimQuantificacaoPercent").text(((totalSimQuantificacao/JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoQuantificacaoPercent").text(((totalNaoQuantificacao /JSONObject.length) * 100).toFixed(2) + '%')
						
						$("#totalSimDefesa").text(totalSimDefesa )
						$("#totalNaoDefesa").text(totalNaoDefesa  )
						$("#totalSimDefesaPercent").text(((totalSimDefesa /JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoDefesaPercent").text(((totalNaoDefesa  /JSONObject.length) * 100).toFixed(2) + '%')
						
						$("#totalSimSubmissao").text(totalSimSubmissao )
						$("#totalNaoSubmissao").text(totalNaoSubmissao  )
						$("#totalSimSubmissaoPercent").text(((totalSimSubmissao /JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoSubmissaoPercent").text(((totalNaoSubmissao  /JSONObject.length) * 100).toFixed(2) + '%')
						
						$("#totalSimEncerramento").text(totalSimEncerramento )
						$("#totalNaoEncerramento").text(totalNaoEncerramento  )
						$("#totalSimEncerramentoPercent").text(((totalSimEncerramento /JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoEncerramentoPercent").text(((totalNaoEncerramento  /JSONObject.length) * 100).toFixed(2) + '%')
						
						$("#totalSimEr").text(totalSimEr )
						$("#totalNaoEr").text(totalNaoEr  )
						$("#totalSimErPercent").text(((totalSimEr /JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoErPercent").text(((totalNaoEr  /JSONObject.length) * 100).toFixed(2) + '%')
						
						$("#totalSimformCliente").text(totalSimformCliente )
						$("#totalNaoFormCliente").text(totalNaoformCliente  )
						$("#totalSimFormClientePercent").text(((totalSimformCliente /JSONObject.length) * 100).toFixed(2) + '%')
						$("#totalNaoFormClientePercent").text(((totalNaoformCliente  /JSONObject.length) * 100).toFixed(2) + '%')
						
						if(primeiroLoad == 1){
						
						$('#tabela-clientes').DataTable({
								language: {
									"sEmptyTable": "Nenhum registro encontrado",
									"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
									"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
									"sInfoFiltered": "(Filtrados de _MAX_ registros)",
									"sInfoPostFix": "",
									"sInfoThousands": ".",
									"sLengthMenu": "_MENU_ resultados por página",
									"sLoadingRecords": "Carregando...",
									"sProcessing": "Processando...",
									"sZeroRecords": "Nenhum registro encontrado",
									"sSearch": "Pesquisar",
									"oPaginate": {
										"sNext": "Próximo",
										"sPrevious": "Anterior",
										"sFirst": "Primeiro",
										"sLast": "Último"
									},
									"oAria": {
										"sSortAscending": ": Ordenar colunas de forma ascendente",
										"sSortDescending": ": Ordenar colunas de forma descendente"
									},
									"select": {
										"rows": {
											"_": "Selecionado %d linhas",
											"0": "Nenhuma linha selecionada",
											"1": "Selecionado 1 linha"
										}
									}
								},
								bFilter: false,
								iDisplayLength: 5000,
								columnDefs: [
									{
										targets: [0, 1, 2],
										className: 'mdl-data-table__cell--non-numeric'
									}
								]
							});
							primeiroLoad++
						}
					//	$("#totais").append('<div id="' + 'box" class="etapa card-panel grey lighten-4 z-depth-3"><span>'  + '</span></br></div>&nbsp&nbsp&nbsp&nbsp&nbsp');

					//	$("#aberturaMissao").append('<div id="totalSimAbertura' + 'sim" style="max-height:40px; border-radius: 4px 0px 0px 4px; float: left; width: 24%; height: 34px; background-color: #2bbbad; border-right-color: coral;" class="indicadores card-panel z-depth-1">0')
					//	$("#aberturaMissao").append('<div id="totalSimAbertura' + 'simPercent" style="margin-right: 2px;max-height:38px; border-radius: 0px 4px 4px 0px; float: left; width: 24%; height: 34px; background-color: #2bbbad;" class="indicadores cartao card-panel ">')

					//	$("#aberturaMissao").append('<div id="totalNaoAbertura'+ 'nao" style="max-height:40px; border-radius: 4px 0px 0px 4px; float: left; width: 23%; height: 34px;" class="indicadores cartao card-panel red lighten-1 ">0')
					//	$("#aberturaMissao").append('<div id="totalNaoAbertura' + 'naoPercent" style="max-height:38px; border-radius: 0px 4px 4px 0px; float: left; width: 24%; height: 34px;" class="indicadores card-panel red lighten-1 z-depth-1">')
//	}
						
						gif.style.display = "none"
						//var table = $('#tabela-clientes').DataTable();
						
                    }
                    
	})

})

function filtroGeral() {
	//var $filterableRows = $('#tabela-clientes').find('tr').not(':first'),
	//	inputs = $('.search-key');

	//inputs.on('change', function() {

	//	var filterVal = $(this).val()

	//	$filterableRows.hide().filter(function() {
	//		return $(this).find('td').filter(function() {

	//		var tdText = $(this).text().toLowerCase(),
	//				inputValue = $('#' + $(this).data('input')).val();

	//				return tdText.indexOf(inputValue) != -1;

	//		}).length == $(this).find('td').length;
	//	}).show();

	//});
}

function filtrosEtapas() {
//	$(".filtro").change(
//		function() {

//			var index = $(this).attr('class');
//			index = index.replace(/\D/g, '');
//			index = parseInt(index);

//			var indexSearch = 5 + index
//			var selectAtual = $(this).find("select")
//			var nomeFiltro = $("#" + selectAtual.attr("id") + " :selected").val();

	//		$('#tabela-clientes').find('tbody tr').each(
	//			function() {
	//				var conteudoCelula = $(this)
	//					.find('td:nth-child(' + indexSearch + ')').text();
	//				var corresponde = conteudoCelula
	//					.indexOf(nomeFiltro) >= 0;
	//				$(this).css('display', corresponde ? '' : 'none');
	//			});
	//		calculaIndicadores();
	//	});
}

function filtroAno() {
//	$("#combo-box").change(
//		function() {
//			var nomeFiltro = $("#combo-box :selected").val()
//			$('#tabela-clientes').find('tbody tr').each(
//				function() {
//					var conteudoCelula = $(this)
//						.find('td:nth-child(2)').text();
//					var corresponde = conteudoCelula
//						.indexOf(nomeFiltro) >= 0;
//					$(this).css('display', corresponde ? '' : 'none');
//				});
//			calculaIndicadores();
//		});
}

function filtroEquipe() {
//	if ($("#liderFilter").val() == "1") {
//		var nomeFiltro = $("#selectEquipes :selected").val()
//		$('#tabela-clientes').find('tbody tr').each(
//			function() {
//				var conteudoCelula = $(this)
//					.find('td:nth-child(3)').text();
//				var corresponde = conteudoCelula
//					.indexOf(nomeFiltro) >= 0;
//				$(this).css('display', corresponde ? '' : 'none');
//			});
//		calculaIndicadores()
//	}
//	$("#selectEquipes").change(
//		function() {
//			var nomeFiltro = $("#selectEquipes :selected").val()
//			$('#tabela-clientes').find('tbody tr').each(
//				function() {
//					var conteudoCelula = $(this)
//						.find('td:nth-child(3)').text();
//					var corresponde = conteudoCelula
//						.indexOf(nomeFiltro) >= 0;
//					$(this).css('display', corresponde ? '' : 'none');
//				});
//			calculaIndicadores();
//		});
}

function filtroColab() {
	//if ($("#consultorFilter").val() == "1") {
	//	var nomeFiltro = $("#selectColaboradores :selected").val()
	//	$('#tabela-clientes').find('tbody tr').each(
	//		function() {
	//			var conteudoCelula = $(this)
	//				.find('td:nth-child(4)').text();
	//			var corresponde = conteudoCelula
	//				.indexOf(nomeFiltro) >= 0;
	//			$(this).css('display', corresponde ? '' : 'none');
	//		});
	//	calculaIndicadores()
	//}
	//$("#selectColaboradores").change(
	//	function() {
	//		var nomeFiltro = $("#selectColaboradores :selected").val()
	//		$('#tabela-clientes').find('tbody tr').each(
	//			function() {
	//				var conteudoCelula = $(this)
	//					.find('td:nth-child(4)').text();
	//				var corresponde = conteudoCelula
	//					.indexOf(nomeFiltro) >= 0;
	//				$(this).css('display', corresponde ? '' : 'none');
	//			});
	//		calculaIndicadores()
	//	});
}

function filtroNome() {

//	$('#txt-pesq-cliente').on(
//		'keyup',
//		function() {
//			var nomeFiltro = $(this).val().toLowerCase();
//			$('#tabela-clientes').find('tbody tr').each(
//				function() {
//					var conteudoCelula = $(this)
//						.find('td:nth-child(1)').text();
//					var corresponde = conteudoCelula.toLowerCase()
//						.indexOf(nomeFiltro) >= 0;
//					$(this).css('display', corresponde ? '' : 'none');
//				});
//			calculaIndicadores()
//		});
}

var etapasConclusaoGlobal;

function carregaPontoSituacao() {

/*
	$("#etapasTrabalho").append('<th>Cliente</th>');
	$("#etapasTrabalho").append('<th>Ano</th>');
	$("#etapasTrabalho").append('<th>Equipe</th>');
	$("#etapasTrabalho").append('<th>Consultor</th>');
	var etapasTrabalho = [];
	var producoes = [];
	var contrato;

	$.when(solicitadorEtapas()).done(function(etapasConclusao) {
		etapasConclusaoGlobal = etapasConclusao;

		for (var i = 0; i < etapasConclusaoGlobal.length; i++) {
			var etapas = etapasConclusaoGlobal[i];

			var etapaTrabalho = {
				id: etapas.etapa.id,
				nome: etapas.etapa.nome,
				produto: etapas.producao.produto.id
			}


			var producao = {
				id: etapas.producao.id,
				ano: etapas.producao.ano,
				equipe: etapas.producao.equipe.nome,
				cliente: etapas.producao.cliente.razaoSocial,
				clienteId: etapas.producao.cliente.id,
				consultor: etapas.producao.consultor.sigla
			}

			etapasTrabalho.push(etapaTrabalho)
			producoes.push(producao)

		}

		etapasTrabalho = unique(etapasTrabalho);
		etapasSize = etapasTrabalho.length;
		producoes = uniqueProducao(producoes);
		etapasTrabalhoHolder = etapasTrabalho;

		for (var i = 0; i < etapasTrabalho.length; i++) {
			var etapa = etapasTrabalho[i];


			var nomeCru = etapa.nome;
			var nome = nomeCru.substring(nomeCru.indexOf(".") + 1);

			$("#etapasTrabalho").append('<th>' + nome + '</th>');

			$("#filtroEtapas").append('<div class="input-field col m2">'
				+ '<select id="' + i + '" class="' + i + ' filtro select search-key">'
				+ '<option value="">Sem filtro</option>'
				+ '<option value="Sim" >Sim</option>'
				+ '<option value="Não" >Não</option>'
				+ '</select>'
				+ '<label>' + nome + '</label>'
				+ '</div>')
		}

		$('.filtro').material_select();

		for (var i = 0; i < producoes.length; i++) {

			var producao = producoes[i];
			$("#tabela").append('<tr id="prod' + producao.id + '">'
				+ '<td style="max-width: 230px" class="cliente"><a href="/producoes/update/' + producao.id + '">' + producao.cliente + '</a></td>'
				+ '<td>' + producao.ano + '</td>'
				+ '<td>' + producao.equipe + '</td>'
				+ '<td>' + producao.consultor + '</td>'
				+ '</tr>')
		}

		var dataInput = 0

		for (var i = 0; i < etapasConclusao.length; i++) {

			var etapas = etapasConclusao[i];

			if (etapas.concluido == true) {
				$('#prod' + etapas.producao.id).append('<td data-input="' + dataInput + '" class="indicador coluna' + etapas.etapa.id + '" bgcolor="#2bbbad" >Sim</td>');
			} else {
				$('#prod' + etapas.producao.id).append('<td data-input="' + dataInput + '" class="indicador coluna' + etapas.etapa.id + '" bgcolor="#ef5350">Não</td>');
			}

			dataInput = dataInput + 1;

			if (dataInput == etapasSize) {
				dataInput = 0
			}

		}
	});
	*/
}

function solicitadorEtapas() {



	var id = $("#prodId").val();
	var equipe = $("#equipeId").val();

	console.log("Id da equipe: " + equipe)

	if (equipe != null || equipe != undefined || equipe != "undefined") {

	var url = "/pontoSituacao/" + id + "/" + equipe + "/etapas.json"

	} else {
		var url = "/pontoSituacao/" + id + "/etapas.json"
	}


	return $.ajax({

		type: 'GET',
		url: url,
		async: false,
		beforeSend: function() {
			$('#loader').show();
		},
		complete: function() {
			$('#loader').hide();
		},
		success: function(data) { }
	});

}

function solicitadorContratos() {

   var url = "/contratos/all.json"

   return $.ajax({
       type: 'GET',
       url: url,
       async: false,
       success: function (data) {}
    });

}

function unique(list) {

	var result = [];
	$.each(list, function(i, e) {
		var matchingItems = $.grep(result, function(item) {
			return item.nome === e.nome;
		});
		if (matchingItems.length === 0) {
			result.push(e);
		}
	});
	return result;
}

function uniqueProducao(list) {

	var result = [];
	$.each(list, function(i, e) {
		var matchingItems = $.grep(result, function(item) {
			return item.id === e.id;
		});
		if (matchingItems.length === 0) {
			result.push(e);
		}
	});
	return result;
}

function uniqueET(list) {
	var result = [];
	$.each(list, function(i, e) {
		var matchingItems = $.grep(result, function(item) {
			return item.nome === e.nome;
		});
		if (matchingItems.length === 0) {
			result.push(e);
		}
	});
	return result;
}

var etapasDeTrabalho = [];

function carregaIndicadores() {

	var index = 0
	var etapasTrabalho = []
	var indicadores = []
	var valorSim = 0;
	var valorNao = 0;
	var total = 0;
	var percentSim = 0;
	var percentNao = 0;
	//var etapasC;

//	for (var i = 0; i < etapasConclusaoGlobal.length; i++) {
//		var etapas = etapasConclusaoGlobal[i];

		//etapasC = etapasConclusaoGlobal; ja estava comentado

//		var etapaTrabalho = {
//			id: etapas.etapa.id,
//			nome: etapas.etapa.nome,
//			produto: etapas.producao.produto.id
//		}

//		etapasTrabalho.push(etapaTrabalho)

//	}

//	etapasTrabalho = uniqueET(etapasTrabalho);
//	etapasDeTrabalho = etapasTrabalho;

//	for (var i = 0; i < etapasTrabalho.length; i++) {
//		var etapaT = etapasTrabalho[i];

//		$("#totais").append('<div id="' + etapaT.id + 'box" class="etapa card-panel grey lighten-4 z-depth-3"><span>' + etapaT.nome + '</span></br></div>&nbsp&nbsp&nbsp&nbsp&nbsp');

//		$("#" + etapaT.id + "box").append('<div id="' + etapaT.id + 'sim" style="max-height:40px; border-radius: 4px 0px 0px 4px; float: left; width: 24%; height: 34px; background-color: #2bbbad; border-right-color: coral;" class="indicadores card-panel z-depth-1">0')
//		$("#" + etapaT.id + "box").append('<div id="' + etapaT.id + 'simPercent" style="margin-right: 2px;max-height:38px; border-radius: 0px 4px 4px 0px; float: left; width: 24%; height: 34px; background-color: #2bbbad;" class="indicadores cartao card-panel ">')

//		$("#" + etapaT.id + "box").append('<div id="' + etapaT.id + 'nao" style="max-height:40px; border-radius: 4px 0px 0px 4px; float: left; width: 23%; height: 34px;" class="indicadores cartao card-panel red lighten-1 ">0')
//		$("#" + etapaT.id + "box").append('<div id="' + etapaT.id + 'naoPercent" style="max-height:38px; border-radius: 0px 4px 4px 0px; float: left; width: 24%; height: 34px;" class="indicadores card-panel red lighten-1 z-depth-1">')
//	}

//	calculaIndicadores();
}

function calculaIndicadores() {

	for (var i = 0; i < etapasDeTrabalho.length; i++) {
		var etapa = etapasDeTrabalho[i];

		var sumSim = 0;
		var sumNao = 0;
		$('.coluna' + etapa.id).each(function() {
			if ($(this).is(":visible")) {
				var value = $(this).text();
				if (value == "Sim") {
					sumSim += 1;
					$('#' + etapa.id + 'sim').text(sumSim)
				} else {
					sumNao += 1;
					$('#' + etapa.id + 'nao').text(sumNao)
				}
			}
		});
	}

	for (var i = 0; i < etapasDeTrabalho.length; i++) {
		var etapa = etapasDeTrabalho[i];

		total = parseInt($('#' + etapa.id + 'sim').text()) + parseInt($('#' + etapa.id + 'nao').text())

		percentSim = ($('#' + etapa.id + 'sim').text() / total) * 100;
		percentNao = ($('#' + etapa.id + 'nao').text() / total) * 100;

		$('#' + etapa.id + 'simPercent').text(Math.round(percentSim) + "%");
		$('#' + etapa.id + 'naoPercent').text(Math.round(percentNao) + "%");

	}
}

function fnExcelReport() {

	var tab_text = '\uFEFF<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
	tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

	tab_text = tab_text + '<x:Name>Ponto Situação</x:Name>';

	tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
	tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

	tab_text = tab_text + "<table border='1px'>";
	tab_text = tab_text + $('#tabela-clientes').html();
	tab_text = tab_text + '</table></body></html>';

	var data_type = 'data:application/vnd.ms-excel';

	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
		if (window.navigator.msSaveBlob) {
			var blob = new Blob([tab_text], {
				type: "application/csv;charset=utf-8"
			});
			navigator.msSaveBlob(blob, 'pontoSituacao.xls');
		}
	} else {
		$('#excelBtn').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
		$('#excelBtn').attr('download', 'pontoSituacao.xls');
	}
}


function htmlTableToExcel(tableName, name){
	//const tableRows = document.querySelectorAll('tr')
	const tableRows = document.getElementById(tableName).querySelectorAll('tr')
	console.log(tableRows)
	var universalBOM = "\uFEFF";
	const CSVString = 
		Array.from(tableRows)
			.map(row => Array.from(row.cells)
				.map(cell => cell.textContent)
				.join(';').replaceAll('/', ` de `)
			)
			.join('\n').replace('/', ' ')
			console.log('o texto ficou: ',CSVString)
		const exportBtn = document.getElementById('excelBtn')
		//exportBtn.setAttribute('href', `data:text/csvcharset=utf-8,${encodeURIComponent(CSVString)}`)
		exportBtn.setAttribute('href', 'data:text/csv; charset=utf-8,' + encodeURIComponent(universalBOM+CSVString));
		exportBtn.setAttribute('download', name +'.csv')
		console.log('o botão é', exportBtn)
}	