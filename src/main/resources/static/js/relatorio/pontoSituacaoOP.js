var etapasTrabalhoHolder = new Array();

$(function() {

	$('.select').material_select();
	carregaPontoSituacao();
	filtrosEtapas();
	filtroNome();
	filtroAno();
	filtroColab();
	filtroEquipe();
	carregaIndicadores()
		$('#tabela-clientes').DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			},
			"select": {
				"rows": {
					"_": "Selecionado %d linhas",
					"0": "Nenhuma linha selecionada",
					"1": "Selecionado 1 linha"
				}
			}
		},
		bFilter: false,
		iDisplayLength: 5000,
		columnDefs: [
			{
				targets: [0, 1, 2],
				className: 'mdl-data-table__cell--non-numeric'
			}
		]
	});


});

function filtroGeral() {
	var $filterableRows = $('#tabela-clientes').find('tr').not(':first'),
		inputs = $('.search-key');

	inputs.on('change', function() {

		var filterVal = $(this).val()
		console.log(filterVal)

		$filterableRows.hide().filter(function() {
			return $(this).find('td').filter(function() {

				var tdText = $(this).text().toLowerCase(),
					inputValue = $('#' + $(this).data('input')).val();

				//console.log($(this));

				return tdText.indexOf(inputValue) != -1;

			}).length == $(this).find('td').length;
		}).show();

	});
}

function filtrosEtapas() {
	$(".filtro").change(
		function() {

			var index = $(this).attr('class');
			index = index.replace(/\D/g, '');
			index = parseInt(index);

			var indexSearch = 5 + index
			console.log("O index de pesquisa é : " + indexSearch)
			var selectAtual = $(this).find("select")
			var nomeFiltro = $("#" + selectAtual.attr("id") + " :selected").val();

			console.log(nomeFiltro);

			$('#tabela-clientes').find('tbody tr').each(
				function() {
					var conteudoCelula = $(this)
						.find('td:nth-child(' + indexSearch + ')').text();
					var corresponde = conteudoCelula
						.indexOf(nomeFiltro) >= 0;
					$(this).css('display', corresponde ? '' : 'none');
				});
			calculaIndicadores();
		});
}

function filtroAno() {
	$("#combo-box").change(
		function() {
			var nomeFiltro = $("#combo-box :selected").val()
			console.log(nomeFiltro);
			$('#tabela-clientes').find('tbody tr').each(
				function() {
					var conteudoCelula = $(this)
						.find('td:nth-child(2)').text();
					console.log(conteudoCelula);
					var corresponde = conteudoCelula
						.indexOf(nomeFiltro) >= 0;
					$(this).css('display', corresponde ? '' : 'none');
				});
			calculaIndicadores();
		});
}

function filtroEquipe() {
	if ($("#liderFilter").val() == "1") {
		var nomeFiltro = $("#selectEquipes :selected").val()
		console.log(nomeFiltro);
		$('#tabela-clientes').find('tbody tr').each(
			function() {
				var conteudoCelula = $(this)
					.find('td:nth-child(3)').text();
				console.log(conteudoCelula);
				var corresponde = conteudoCelula
					.indexOf(nomeFiltro) >= 0;
				$(this).css('display', corresponde ? '' : 'none');
			});
		calculaIndicadores()
	}
	$("#selectEquipes").change(
		function() {
			var nomeFiltro = $("#selectEquipes :selected").val()
			console.log(nomeFiltro);
			$('#tabela-clientes').find('tbody tr').each(
				function() {
					var conteudoCelula = $(this)
						.find('td:nth-child(3)').text();
					console.log(conteudoCelula);
					var corresponde = conteudoCelula
						.indexOf(nomeFiltro) >= 0;
					$(this).css('display', corresponde ? '' : 'none');
				});
			calculaIndicadores();
		});
}

function filtroColab() {
	if ($("#consultorFilter").val() == "1") {
		var nomeFiltro = $("#selectColaboradores :selected").val()
		console.log("VALOR PARA FILTRAR: " + nomeFiltro);
		$('#tabela-clientes').find('tbody tr').each(
			function() {
				var conteudoCelula = $(this)
					.find('td:nth-child(4)').text();
				console.log(conteudoCelula);
				var corresponde = conteudoCelula
					.indexOf(nomeFiltro) >= 0;
				$(this).css('display', corresponde ? '' : 'none');
			});
		calculaIndicadores()
	}
	$("#selectColaboradores").change(
		function() {
			var nomeFiltro = $("#selectColaboradores :selected").val()
			console.log(nomeFiltro);
			$('#tabela-clientes').find('tbody tr').each(
				function() {
					var conteudoCelula = $(this)
						.find('td:nth-child(4)').text();
					console.log(conteudoCelula);
					var corresponde = conteudoCelula
						.indexOf(nomeFiltro) >= 0;
					$(this).css('display', corresponde ? '' : 'none');
				});
			calculaIndicadores()
		});
}

function filtroNome() {
	$('#txt-pesq-cliente').on(
		'keyup',
		function() {
			var nomeFiltro = $(this).val().toLowerCase();
			console.log(nomeFiltro);
			$('#tabela-clientes').find('tbody tr').each(
				function() {
					var conteudoCelula = $(this)
						.find('td:nth-child(1)').text();
					console.log(conteudoCelula);
					var corresponde = conteudoCelula.toLowerCase()
						.indexOf(nomeFiltro) >= 0;
					$(this).css('display', corresponde ? '' : 'none');
				});
			calculaIndicadores()
		});
}

var etapasConclusaoGlobal;

function carregaPontoSituacao() {

	$("#etapasTrabalho").append('<th>Cliente</th>');
	if ($("#pais").val() == "CHILE") {
		$("#etapasTrabalho").append('<th>Nombre Proyecto</th>');
	}
	$("#etapasTrabalho").append('<th>Ano</th>');
	$("#etapasTrabalho").append('<th>Equipe</th>');
	$("#etapasTrabalho").append('<th>Consultor</th>');
	var etapasTrabalho = [];
	var producoes = [];
	var subProducoes = [];
	var contrato;

	$.when(solicitadorEtapas()).done(function(etapasConclusao) {
		etapasConclusaoGlobal = etapasConclusao;

		for (var i = 0; i < etapasConclusaoGlobal.length; i++) {
			var etapas = etapasConclusaoGlobal[i];

			var etapaTrabalho = {
				id: etapas.etapa.id,
				nome: etapas.etapa.nome,
				produto: etapas.producao.produto.id
			}


			var producao = {
				id: etapas.producao.id,
				ano: etapas.producao.ano,
				nomeProjeto: etapas.producao.nomeProjeto,
				equipe: etapas.producao.equipe.nome,
				cliente: etapas.producao.cliente.razaoSocial,
				clienteId: etapas.producao.cliente.id,
				consultor: etapas.producao.consultor.sigla
			}

			etapasTrabalho.push(etapaTrabalho)
			producoes.push(producao)

		}

		etapasTrabalho = unique(etapasTrabalho);
		etapasSize = etapasTrabalho.length;
		producoes = uniqueProducao(producoes);
		etapasTrabalhoHolder = etapasTrabalho;

		for (var i = 0; i < etapasTrabalho.length; i++) {
			var etapa = etapasTrabalho[i];

			var nomeCru = etapa.nome;
			var nome = nomeCru.substring(nomeCru.indexOf(".") + 1);

			$("#etapasTrabalho").append('<th>' + nome + '</th>');

		}

		$('.filtro').material_select();

		for (var i = 0; i < producoes.length; i++) {

			var producao = producoes[i];
			var nombreProyecto = producao.nomeProjeto;
			
			if(nombreProyecto==null){
				nombreProyecto=="";
			}
			
			// SE USUÁRIO FOR DO CHILE, CARREGA COM NOME DO PROJETO.
			if ($("#pais").val() == "CHILE") {
				$("#tabela").append('<tr id="' + producao.id + '">'
					+ '<td style="max-width: 230px" class="cliente"><a href="/producoes/update/' + producao.id + '">' + producao.cliente + '</a></td>'
					+ '<td>' + nombreProyecto + '</td>'
					+ '<td>' + producao.ano + '</td>'
					+ '<td>' + producao.equipe + '</td>'
					+ '<td>' + producao.consultor + '</td>'
					+ '</tr>')
			} else {
				$("#tabela").append('<tr id="' + producao.id + '">'
					+ '<td style="max-width: 230px" class="cliente"><a href="/producoes/update/' + producao.id + '">' + producao.cliente + '</a></td>'
					+ '<td>' + producao.ano + '</td>'
					+ '<td>' + producao.equipe + '</td>'
					+ '<td>' + producao.consultor + '</td>'
					+ '</tr>')
			}
		}

		var dataInput = 0
		
		for (var i = 0; i < etapasConclusao.length; i++) {
			var url = "";
			var etapas = etapasConclusao[i];
			if (etapas.etapa.subProducao == false) {
				if (etapas.concluido === true) {
					$('#' + etapas.producao.id).append('<td data-input="' + dataInput + '" class="indicador coluna' + etapas.etapa.id + '" bgcolor="#2bbbad" >Sim</td>');
				} else {
					$('#' + etapas.producao.id).append('<td data-input="' + dataInput + '" class="indicador coluna' + etapas.etapa.id + '" bgcolor="#ef5350">Não</td>');
				}
			} else{
			url = "/pontoSituacao/" + etapas.etapa.id+ "/"+etapas.producao.id+"/subProds.json";
			console.log(url)
			$.ajax({
				type: 'GET',
				url: url,
				async: false,
				success: function(data) {
					console.log(data.idProducao+" + "+data.idEtapa)
					if(data.done=="false"){
						$('#' + data.idProducao).append('<td data-input="' + dataInput + '" class="indicador coluna' + data.idEtapa + '" bgcolor="#ef5350">'+data.scts+'</td>');
				} else {
						$('#' + data.idProducao).append('<td data-input="' + dataInput + '" class="indicador coluna' + data.idEtapa + '" bgcolor="#2bbbad">'+data.scts+'</td>');
				}
				},
				error: function(xhr, status, erro) {
					alert(xhr.responseText);
				}
			});
			}

			dataInput = dataInput + 1;

			if (dataInput == etapasSize) {
				dataInput = 0
			}
		}	
	});
}

function solicitadorEtapas() {

	var id = $("#prodId").val();
	var equipe = $("#equipeId").val();

	if (equipe == null || equipe == undefined || equipe == "undefined") {

		var url = "/pontoSituacao/" + id + "/" + equipe + "/etapas.json"

	} else {
		var url = "/pontoSituacao/" + id + "/etapas.json"
	}


	return $.ajax({
		type: 'GET',
		url: url,
		async: false,
		beforeSend: function() {
			$('#loader').show();
		},
		complete: function() {
			$('#loader').hide();
		},
		success: function(data) { }
	});

}

//function solicitadorContratos() {
//
//    var url = "/contratos/all.json"
//
//    return $.ajax({
//        type: 'GET',
//        url: url,
//        async: false,
//        success: function (data) {}
//    });
//
//}

function unique(list) {
	var result = [];
	$.each(list, function(i, e) {
		var matchingItems = $.grep(result, function(item) {
			return item.nome === e.nome;
		});
		if (matchingItems.length === 0) {
			result.push(e);
		}
	});
	return result;
}

function uniqueProducao(list) {
	var result = [];
	$.each(list, function(i, e) {
		var matchingItems = $.grep(result, function(item) {
			return item.id === e.id;
		});
		if (matchingItems.length === 0) {
			result.push(e);
		}
	});
	return result;
}

function uniqueET(list) {
	var result = [];
	$.each(list, function(i, e) {
		var matchingItems = $.grep(result, function(item) {
			return item.nome === e.nome;
		});
		if (matchingItems.length === 0) {
			result.push(e);
		}
	});
	return result;
}

function carregaTotais() {
	$.when(solicitadorEtapas()).done(function(etapasConclusao) {
		for (var i = 0; i < etapasConclusao.length; i++) {

		}
	});
}

var etapasDeTrabalho = [];

function carregaIndicadores() {

	var index = 0
	var etapasTrabalho = []
	var indicadores = []
	var valorSim = 0;
	var valorNao = 0;
	var total = 0;
	var percentSim = 0;
	var percentNao = 0;
	//var etapasC;

}

function calculaIndicadores() {

	for (var i = 0; i < etapasDeTrabalho.length; i++) {
		var etapa = etapasDeTrabalho[i];

		var sumSim = 0;
		var sumNao = 0;
		$('.coluna' + etapa.id).each(function() {
			if ($(this).is(":visible")) {
				var value = $(this).text();
				if (value == "Sim") {
					sumSim += 1;
					$('#' + etapa.id + 'sim').text(sumSim)
				} else {
					sumNao += 1;
					$('#' + etapa.id + 'nao').text(sumNao)
				}
			}
		});
	}

	for (var i = 0; i < etapasDeTrabalho.length; i++) {
		var etapa = etapasDeTrabalho[i];

		total = parseInt($('#' + etapa.id + 'sim').text()) + parseInt($('#' + etapa.id + 'nao').text())

		percentSim = ($('#' + etapa.id + 'sim').text() / total) * 100;
		percentNao = ($('#' + etapa.id + 'nao').text() / total) * 100;

		$('#' + etapa.id + 'simPercent').text(Math.round(percentSim) + "%");
		$('#' + etapa.id + 'naoPercent').text(Math.round(percentNao) + "%");

	}
}

function fnExcelReport() {
	var tab_text = '\uFEFF<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
	tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

	tab_text = tab_text + '<x:Name>Ponto Situação</x:Name>';

	tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
	tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

	tab_text = tab_text + "<table border='1px'>";
	tab_text = tab_text + $('#tabela-clientes').html();
	tab_text = tab_text + '</table></body></html>';

	var data_type = 'data:application/vnd.ms-excel';

	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
		if (window.navigator.msSaveBlob) {
			var blob = new Blob([tab_text], {
				type: "application/csv;charset=utf-8"
			});
			navigator.msSaveBlob(blob, 'pontoSituacao.xls');
		}
	} else {
		$('#excelBtn').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
		$('#excelBtn').attr('download', 'pontoSituacao.xls');
	}
}

