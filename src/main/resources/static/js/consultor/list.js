$(function(){
	$('#modal-exlcuir').modal();
	filtroTabela('#txt-pesq-consultor');
})

$(function () {
    $(".modal").modal();
})

function removerConsultor(id) {

    $.post("/consultores/delete/" + id, function () {
            location.reload();
        })
        .fail(function () {
            alert("deu ruim");
        });
};

function filtroConsultores(){
	$("#select-estado").change(
			function() {
				var nomeFiltro = $("#select-estado :selected").val()
				console.log(nomeFiltro);
				$('#tabela-fat').find('tbody tr').each(
						function() {
							var conteudoCelula = $(this)
									.find('td:nth-child(5)').text();
							console.log(conteudoCelula);
							var corresponde = conteudoCelula
									.indexOf(nomeFiltro) >= 0;
							$(this).css('display', corresponde ? '' : 'none');
						});
			});
}


function fnExcelReport() {
    var tab_text = '\uFEFF<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Lista Consultores/x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + $('#tabela-consultor').html();
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8"
            });
            navigator.msSaveBlob(blob, 'Consultores.xls');
        }
    } else {
        $('#excelBtn').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
        $('#excelBtn').attr('download', 'Consultores.xls');
    }
}

