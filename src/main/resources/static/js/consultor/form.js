 $("#email").blur(concatenaDominio);
 $("#confirmSenha").on("input", confirmarSenha);
 $("#senha").on("input", desbloquearConfirmarSenha);

 
 $(function () {
     $("#confirmSenha").prop("disabled", true);
     $(".select").material_select();
     $("#confirmSenha").tooltipster({
         trigger: "custom"
     });
     $("#btn-editar-consultor").tooltipster();
 });

 function concatenaDominio() {
     if ($(this).val() != '') {
         $("#username").val($(this).val());
         $("#username").focusin();
         $(this).val($(this).val() + "@fi-group.com");
     }
 };

 function confirmarSenha() {
     var senha = $("#senha").val();
     var senha2 = $("#confirmSenha").val();

     if (senha != '') {
         if (senha != senha2) {
             console.log("!=")
             $("#confirmSenha").tooltipster("open").tooltipster("content", "Senha diferente");
         } else {
             $("#confirmSenha").tooltipster("close");
         }
     }

 };

 function desbloquearConfirmarSenha() {
     var senha = $("#senha").val();
     if (senha != '') {
         $("#confirmSenha").prop("disabled", false);
     } else {
         $("#confirmSenha").prop("disabled", true);
     }
 };
 
 $(function () {
		selectCargos()
		var nomeFiltro = $("#select-cargos :selected").val()
		console.log(nomeFiltro);
		    if (nomeFiltro === 'CONSULTOR_TECNICO') {
		    	$("#categoria").show();
		    } else {
		    	$("#categoria").hide();
		    } 
	})
 
function selectCargos(){
		$("#select-cargos").change(
				function() {
					var nomeFiltro = $("#select-cargos :selected").val()
					console.log(nomeFiltro);
					    if ($(this).val() === 'CONSULTOR_TECNICO') {
					    	$("#categoria").show();
					    } else {
					    	$("#categoria").hide();
					    } 
				});
	}
	
	
	 $(function () {
		selectDivisão()
		var departamento = $("#select-departamento :selected").val()
		console.log("entrou no select departamento",departamento);
		    if (departamento === 'CONSULTORIA_TECNICA') {
		    	$("#setorAtividade").show();
		    } else {
		    	$("#setorAtividade").hide();
		    } 
	})
 
function selectDivisão(){
		$("#select-departamento").change(
				function() {
					var departamento = $("#select-setorAtividade :selected").val()
					console.log("entrou no select departamento",departamento);
					    if ($(this).val() === 'CONSULTORIA_TECNICA') {
					    	$("#setorAtividade").show();
					    } else {
					    	$("#setorAtividade").hide();
					    } 
				});
	}
	
	
	