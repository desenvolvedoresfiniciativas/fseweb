$(function() {
	console.log("js carregado")
	ativaMenu();
	filtroTabelaCliente();
	filtroTabelaCampanha();
	filtroTabelaEtapa();
	filtroTabelaEstado();
	$('.datepicker').on('mousedown',function(event){
	    event.preventDefault();
	})
	$('.select-dropdown').on('mousedown',function(event){
	    event.preventDefault();
	})
	$('.modal').modal()
//	$('#modalRedefinirSenha').click(showModal)

	try{
	$('.money').mask('000.000.000.000.000,00', {
        reverse : true
    });
	} catch(err) {
  		console.log("Não foi possível iniciar a máscara.")
	}
	
});

$('#enviaReport').click(function(){

	var msg = $("#descricao").val()
	var tipo = $("#select-tipoReport").find(":selected").val()
	
	var report = {
		tipoMensagem: tipo,
		mensagem: msg
	}
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/report",
		data: JSON.stringify(report),
		dataType:"JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Mensagem enviada com sucesso!',
                        5000, 'rounded');
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }	
	});
	
})

function ativaMenu() {
	$(".button-collapse").sideNav({
		menuWidth : 300,
		edge : 'left',
		closeOnClick : true,
		draggable : true,
		onOpen : function(el) { /* Do Stuff */
		},
		onClose : function(el) { /* Do Stuff */
		}
	});
}

function convertStringToDate(mmDDYYY) {
	var arr = mmDDYYY.split("/");
	return new Date(arr[2], arr[0] - 1, arr[1]);
}

function filtroTabelaCliente() {
    $('#txt-pesq-cliente').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        console.log(nomeFiltro);
        $('#tabela-fat').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(1)').text();
            console.log(conteudoCelula);
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}

function filtroTabelaCampanha() {
    $('#txt-pesq-campanha').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        console.log(nomeFiltro);
        $('#tabela-fat').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(2)').text();
            console.log(conteudoCelula);
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}

function filtroTabelaEtapa() {
    $('#txt-pesq-etapa').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        console.log(nomeFiltro);
        $('#tabela-fat').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(3)').text();
            console.log(conteudoCelula);
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}

function filtroTabelaEstado() {
    $('#txt-pesq-estado').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        console.log(nomeFiltro);
        $('#tabela-fat').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(4)').text();
            console.log(conteudoCelula);
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}
