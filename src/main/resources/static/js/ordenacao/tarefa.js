

$(function(){
	$('.collapsible').collapsible();
	identificarLinhas()
})

function identificarLinhas(){
	var index = 0
	$(".linha").each(function(){
		$(this).attr("id", index)
		index++
	})
}

$(".sobe").click(function(){
	
	//Atual
	var divAtual = $('#'+$(this).parent().attr('id'))
	var divContent = divAtual.children()
	
	//Destino
	var divTarget = divAtual.prev();
	var divTargetContent = divTarget.children()
	
	if(divTarget.length){
	
	divTarget.append(divContent)
	divAtual.append(divTargetContent)
	
	var ordemOriginal = divTarget.find(".ordem")
	var ordemTarget = divAtual.find(".ordem")
	
	console.log("A div original tem a ordem de "+ordemOriginal.val()+" e deve ser trocada com a ordem do target "+ordemTarget.val())
	
	var ordemOriginalNumero = ordemTarget.val()
	var ordemTargetNumero = ordemOriginal.val()
	
	ordemOriginal.val(ordemOriginalNumero)
	ordemTarget.val(ordemTargetNumero)	
	
	}
	
})

$(".desce").click(function(){
	
	//Atual
	var divAtual = $('#'+$(this).parent().attr('id'))
	var divContent = divAtual.children()
	
	//Destino
	var divTarget = divAtual.next();
	var divTargetContent = divTarget.children()
	
	if(divTarget.length){
	
	divTarget.append(divContent)
	divAtual.append(divTargetContent)
	
	var ordemOriginal = divTarget.find(".ordem")
	var ordemTarget = divAtual.find(".ordem")
	
	console.log("A div original tem a ordem de "+ordemOriginal.val()+" e deve ser trocada com a ordem do target "+ordemTarget.val())
	
	var ordemOriginalNumero = ordemTarget.val()
	var ordemTargetNumero = ordemOriginal.val()
	
	
	ordemOriginal.val(ordemOriginalNumero)
	ordemTarget.val(ordemTargetNumero)
	}
	
})

$('.salvar').click(function(){
	
	var ordenacao = [];
	
	// Para cada Linha
	$('.linha').each(function(){
		
		var ordemItem = [];
		var idOrdem;
		var idItem;
		var ordemNumero;
		
		var campos = $(this).children();
		
		//Para cada campo da Linha
		campos.each(function(){

			 if($(this).find('> input').hasClass('idOrdem')){
				 idOrdem = $(this).find('> input').val();
			 }
			 if($(this).find('> input').hasClass('idItem')){
				 idItem = $(this).find('> input').val();
			 }
			 if($(this).find('> input').hasClass('ordem')){
				 ordemNumero = $(this).find('> input').val();
			 }
		})
		
		var item = {
			id: idItem
		}
		
		ordemItem = {
			id: idOrdem,
			item: item,
			numero: ordemNumero
		}
		
		ordenacao.push(ordemItem)
		
	})
	
	console.log(ordenacao)
	
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/ordenacoes/salvaOrdem",
		data: JSON.stringify(ordenacao),
		dataType:"JSON",
        statusCode: {
            200: function () {
                Materialize
                    .toast(
                        'Ordenação salva com sucesso!',
                        5000, 'rounded');
            },
            500: function () {
                Materialize.toast(
                    'Ops, houve um erro interno',
                    5000, 'rounded');
            },
            400: function () {
                Materialize
                    .toast(
                        'Você deve estar fazendo algo de errado',
                        5000, 'rounded');
            },
            404: function () {
                Materialize.toast('Url não encontrada',
                    5000, 'rounded');
            }
        }	
	});
	
})
