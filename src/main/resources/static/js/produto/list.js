$(function(){
	filtroTabela();
})

function filtroTabela() {
    $('#txt-pesq-produto').on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-produtos').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(2)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}