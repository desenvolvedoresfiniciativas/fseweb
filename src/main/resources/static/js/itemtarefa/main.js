$("#tabela-lista-exibe").click(exibirSectionTabela);
$("#tabela-lista-hidden").click(esconderSectionTabela);
$("#selectTipoItem").on('change', function() {
	bloqueiaDefinidor()
	});

$(function () {
    $("#selectTipoItem").material_select();
    $('.tooltipped').tooltip();
    bloqueiaDefinidor()
});

function exibirSectionTabela() {
	var btnExibirContatos = $("#tabela-lista-exibe");
    var btnOcultarContatos = $("#tabela-lista-hidden");
    var tabelaContatosEmpresa = $("#tabela-listas");

    btnExibirContatos.stop().fadeOut(0); // Some
    btnOcultarContatos.stop().fadeIn(0); // Aparece
    tabelaContatosEmpresa.stop().slideToggle(0);

}

function esconderSectionTabela() {
    $("#tabela-listas").stop().slideToggle(0);
    $("#tabela-lista-exibe").fadeIn(0);
    $("#tabela-lista-hidden").fadeOut(0);
}

function bloqueiaDefinidor(){
	var status = $("#selectTipoItem :selected").text();
	if(status!="CHECK"){
		$("#definidor").attr("disabled", true);
	} else {
		$("#definidor").removeAttr("disabled");
	}
}