 $('#combo-box').bind("change keyup",
        function () {
	 carregaIndicadores()
	 filtroTabela()
	 carregaClientes()
});
 
 $('#select-tipoDeNegocio').bind("change keyup",
	        function () {
		 carregaIndicadores()
		 //filtroTabela1()
		 carregaClientes()
});

$(function () {

    $('.tabs').tabs();
    $("#informacoes").hide();
    $('.select').material_select();
    $('.tooltipped').tooltip();
    $('.modal').modal();
    filtroTabela();
    carregaIndicadores();
    carregaClientes();
    verificaPrimeiroLogin();
    
    //filtroTabela1();
    $("#pagina2").hide();
    $("#anterior2").hide();;
    $("#proximo2").hide();
    $("#pagina3").hide();
    $("#anterior3").hide();
    
	// Ir para pagina 2
	$("#proximo1").click(function(){
		$("#pagina1").hide();
		$("#proximo1").hide();
		$("#pagina2").show();
		$("#anterior2").show();
		$("#proximo2").show();
	});
	
	// voltar para pagina 1
	$("#anterior2").click(function(){
		$("#pagina1").show();
		$("#proximo1").show();
		$("#pagina2").hide();
		$("#anterior2").hide();
		$("#proximo2").hide();
	});
	
	// Ir para pagina 3
	$("#proximo2").click(function(){
		$("#pagina2").hide();
		$("#proximo2").hide();
		$("#anterior2").hide();
		$("#pagina3").show();
		$("#anterior3").show();
		$("#fechar").show();
	});
	
	// voltar para pagina 2
	$("#anterior3").click(function(){
		$("#pagina2").show();
		$("#anterior2").show();
		$("#proximo2").show();
		$("#pagina3").hide();
		$("#anterior3").hide();
		$("#fechar").hide();
	});
	
	// Fechar modal
	$("#fechar").click(function(){
		$('#modal1').modal('close');
		
	});		
});


function verificaPrimeiroLogin(){
	var primeiroLogin = $("#primeiroLogin").val();
	if(primeiroLogin=="true"){	
		$('#modalAviso').modal('open');
	}		
}

function carregaIndicadores() {

            limpaTudo();

            var ano = $('#combo-box').val();
            var index = 0
            var etapasTrabalho = []
            var indicadores = []
            var valorSim = 0;
            var valorNao = 0;
            var total = 0;
            var percentSim = 0;
            var percentNao = 0;
            var etapasC;
            solicitadorEtapas
            $.when(solicitadorEtapas(ano)).done(function (etapasConclusao) {
               
            	etapasConclusaoGlobal = etapasConclusao;
            	
            	for (var i = 0; i < etapasConclusao.length; i++) {
                    var etapas = etapasConclusao[i];
                    
                    console.log(etapas.producao.produto.id)
                                        
                    etapasC = etapasConclusao;
                    
                    var etapaTrabalho = {
                    	id: etapas.etapa.id,
                        nome: etapas.etapa.nome,
                        produto: etapas.producao.produto.id
                    }

                    etapasTrabalho.push(etapaTrabalho)

                }

                etapasTrabalho = unique(etapasTrabalho);
                
                $(".corpo").each(function () {
                    var prodId = $(this).attr('id');

                    for (var i = 0; i < etapasTrabalho.length; i++) {
                        var etapaT = etapasTrabalho[i];                       
                        
                        var nomeCru = etapaT.nome;
                        var nome = nomeCru.substring(nomeCru.indexOf(".") + 1);             
                        
                        if (etapaT.produto == prodId) {
                        $("#"+prodId+"").append('<div id="'+etapaT.id+'" class="etapa card-panel grey lighten-4 z-depth-3"><span style="font-size: 13px">'+nome+'</span></br></div>&nbsp&nbsp&nbsp&nbsp&nbsp');
                        
                        $("#"+etapaT.id+"").append('<div id="'+prodId+etapaT.id+'sim" style="max-height:38px; border-radius: 4px 0px 0px 4px; float: left; width: 24%; height: 38px; background-color: #2bbbad; border-right-color: coral;" class="card-panel z-depth-1">')
                        $("#"+etapaT.id+"").append('<div id="'+prodId+etapaT.id+'simPercent" style="margin-right: 2px;max-height:38px; border-radius: 0px 4px 4px 0px; float: left; width: 24%; height: 38px; background-color: #2bbbad;" class="cartao card-panel ">')
                        
                        $("#"+etapaT.id+"").append('<div id="'+prodId+etapaT.id+'nao" style="max-height:38px; border-radius: 4px 0px 0px 4px; float: left; width: 23%; height: 38px;" class="cartao card-panel red lighten-1 ">')
                        $("#"+etapaT.id+"").append('<div id="'+prodId+etapaT.id+'naoPercent" style="max-height:38px; border-radius: 0px 4px 4px 0px; float: left; width: 24%; height: 38px;" class="card-panel red lighten-1 z-depth-1">')
                        }
                    }
                })
                 
            });

            	 for (var i = 0; i < etapasConclusaoGlobal.length; i++) {
                     var etapas = etapasConclusaoGlobal[i];
                    
                     if($("#"+etapas.producao.produto.id+etapas.etapa.id+"sim").find('span').length == 0){
                     $("#"+etapas.producao.produto.id+etapas.etapa.id+"sim").append('<span class="indicador" id="sim'+etapas.etapa.id+'">0</span>');
                     $("#"+etapas.producao.produto.id+etapas.etapa.id+"simPercent").append('<span class="indicadorPercentSim" id="simPercent'+etapas.etapa.id+'">0</span>');
                     }
                     
                    
                     if($("#"+etapas.producao.produto.id+etapas.etapa.id+"nao").find('span').length == 0){
                     $("#"+etapas.producao.produto.id+etapas.etapa.id+"nao").append('<span class="indicador" id="nao'+etapas.etapa.id+'">0</span>');	
                     $("#"+etapas.producao.produto.id+etapas.etapa.id+"naoPercent").append('<span class="indicadorPercentNao" id="naoPercent'+etapas.etapa.id+'">0</span>');	 
                     }
                     
                     if(etapas.concluido===true){
                    	 	valorSim = $('#sim'+etapas.etapa.id+'').text();
                    	 	valorSim = parseInt(valorSim);
                    	 	valorSim = valorSim+1
                         $('#sim'+etapas.etapa.id+'').text(valorSim);
                    	 } else {
                    		 valorNao = $('#nao'+etapas.etapa.id+'').text();
                        	 valorNao = parseInt(valorNao);
                        	 valorNao = valorNao+1
                           	 $('#nao'+etapas.etapa.id+'').text(valorNao);
                    	 }

                     }
            	 
            	 	for (var i = 0; i < etapasConclusaoGlobal.length; i++) {
                     var etapas = etapasConclusaoGlobal[i];
            	 	
                     total = parseInt($('#sim'+etapas.etapa.id+'').text())+parseInt($('#nao'+etapas.etapa.id+'').text())
                     
                     percentSim = ($('#sim'+etapas.etapa.id+'').text()/total)*100;
                     percentNao = ($('#nao'+etapas.etapa.id+'').text()/total)*100;
                     
                     $('#simPercent'+etapas.etapa.id+'').text(Math.round(percentSim)+"%");
                     $('#naoPercent'+etapas.etapa.id+'').text(Math.round(percentNao)+"%");
            	 	
            	 	}
}

function limpaTudo(){
	$('#indicadores').find('*')
    .not('#list')
    .not('#produtos')
    .not('#prodBtn')
    .not('#divisor')
    .not('#psBtn')
    .not('.collapsible-header')
    .not('.material-icons')
    .not('.filho')
    .not('.collapsible-body').remove();
}

function unique(list) {
    var result = [];
    $.each(list, function (i, e) {
        var matchingItems = $.grep(result, function (item) {
        	return item.id === e.id;
        });
        if (matchingItems.length === 0) {
            result.push(e);
        }
    });
    return result;
}

function solicitadorEtapas(ano) {

    var url = "/" + ano + "/ano.json"

    return $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data) {}
    });

}

function mostraDetalhes(id) {
	
	console.log(id)
	
    var url = '/' + id + '/contatos.json';
    var contatos = [];

    $('#informacoes').find('*').not('#tabs-aqui').remove();
    $('#pontoSituacao').find('*').remove();

    $("#tabs-aqui").append('<ul id="tabs-contatos" class="tabs"></ul>');

    var index = 1

    // Variável combo-box
    var ano = $("#combo-box option:selected").val();
    
    var produto = 

    // Função Ajax
    $.get(url, function (data) {

        $(data).each(function () {
            var contato = this;

            $("#tabs-contatos").append('<li class="tab s3"><a href="#test-swipe-' + index + '">' + index + '</a></li>');
            $("#informacoes").append(' <div style="display: none; font-size: 13px; " id="test-swipe-' + index + '" class="col s12"> <b>Nome</b>: ' + contato.nome +
                ' &nbsp <b>Cargo</b>: ' + contato.cargo + '</br> <b>E-mail</b>: ' + contato.email + '</br> <b>Telefone</b>: ' + contato.telefone1 + '</div>')

            index++;

        });
    });

    var urlEtapas = "/" +id+ "/prodAno.json"

    $.get(urlEtapas, function (data) {

        $(data).each(function () {
            var etapas = this;
            if (etapas.concluido == true) {
                $("#pontoSituacao").append(
                    '<a href="producoes/' + etapas.id + '/v2' + '" style="box-shadow: 1px;border-radius: 0px;width: 100%; height: auto;" class="waves-effect waves-light btn"><i class="material-icons left">check</i>' + etapas.etapa.nome + '</a>'
                );
            } else if (etapas.concluido == false) {
                $("#pontoSituacao").append(
                    '<a href="producoes/' + etapas.id + '/v2' + '" id="negativo" style="box-shadow: 1px;border-radius: 0px;width: 100%; height: auto;" class="red lighten-1 waves-effect waves-light btn"><i class="material-icons left">close</i>' + etapas.etapa.nome + '</a>'
                );
            }
        });
    });
    $('.tabs').tabs();
    $("#informacoes").show();

}

function filtroTabela() {
            var nomeFiltro = $("#combo-box").val().toLowerCase();
            $('#tabela-seguimento').find('tbody tr').each(
                function () {
                    var conteudoCelula = $(this)
                        .find('td:nth-child(3)').text();
                    var corresponde = conteudoCelula.toLowerCase()
                        .indexOf(nomeFiltro) >= 0;
                    $(this).css('display', corresponde ? '' : 'none');
                });
}

function filtroTabela1() {
    var nomeFiltro = $("#select-tipoDeNegocio").val().toLowerCase();
    $('#tabela-seguimento').find('tbody tr').each(
        function () {
            var conteudoCelula = $(this)
                .find('td:nth-child(3)').text();
            var corresponde = conteudoCelula.toLowerCase()
                .indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
}


function carregaClientes() {
	
	var ano = $("#combo-box").val();
	var ativos = 0;
	var intativos = 0;
	var prevA = 0;
	var prevI = 0;
	var negociacoes = 0;
	
	$("#countAtivos").text("0")
    $("#countInativos").text("0")
    $("#countPAtivos").text("0")
    $("#countPInativos").text("0")
    $("#countNegociacoes").text("0")

	
	var url = "/" + ano + "/producoes.json"

    $.get(url, function (data) {
    	$(data).each(function () {
    			
            var producao = this;
			console.log('a produção é:',producao)
            
            if(producao.situacao=="ATIVA"){
            	ativos++;
            }
            
            if(producao.situacao=="INATIVA"){
            	intativos++;
            }
            
            if(producao.situacao=="PREVISAO_ATIVA"){
            	prevA++;
            }
            
            if(producao.situacao=="PREVISAO_INATIVA"){
            	prevI++;
            }
            
            if(producao.situacao=="NEGOCIACAO"){
            	negociacoes++;
            }
            
            $("#countAtivos").text(ativos)
            $("#countInativos").text(intativos)
            $("#countPAtivos").text(prevA)
            $("#countPInativos").text(prevI)
            $("#countNegociacoes").text(negociacoes)
    		
    });
});
}

