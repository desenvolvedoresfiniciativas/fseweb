
$(function(){
	$('.tabs').tabs({ 'swipeable': true });
	$("#itensTarefa").material_select();
	
	$(".cartao").change( function(e){
		 console.log(e.target)
		  var id = e.target.id;
		 e.preventDefault();
		 
		 if($(e.target).is(":checked")){
		
			  $('#itensTarefa option[value=' + id + ']').attr('selected', true);
			  $('#itensTarefa').material_select();
			  
			 } else {
				 $('#itensTarefa option[value=' + id + ']').attr('selected', false);
				  $('#itensTarefa').material_select();
			 }
		  
		  //1. pegar id do item de tarefa
		  //2. procurar o id na lista de itens do select
		  //3. marcar o item no select
		  
		  //Precisa também fazer um alg para deselect.
		  
	})
	
})

populaCadProd();

function catchItemTerefaToSelect(item){
	
	console.log(item);
	
}

function populaCadProd(){ 
	
	 var indexText = 0;
	 var indexMoney = 0;
	 var indexData = 0;
	 var indexCheck = 0;
	 var indexLista = 0;
	 var indexListaConsul = 0;
	 var indexTextLong = 0;
	 var indexPercent
	
	 $.when(solicitadorItens()).done(function (itensTarefa) {
		 for (var i = 0; i < itensTarefa.length; i++) {
			 var item = itensTarefa[i];
			 
			 if(item.tipo=="TEXTO"){
				 if(indexText & 1){
					 $("#txtLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>text_fields</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				"<p>"+
                             "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                             "<label for="+item.id+"></label>"+
                             "</p></div></div>")
					}else{
					 $("#txtRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>text_fields</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 				"<p>"+
					 				"<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                    "<label for="+item.id+"></label>"+
                                    "</p></div></div>")
					} 
				 indexText = indexText+1;
			 }
			 
			if(item.tipo=="DINHEIRO"){
							 
				 if(indexMoney & 1){
					 $("#mnyLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>attach_money</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				"<p>"+
                             "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                             "<label for="+item.id+"></label>"+
                             "</p></div></div>")
					}else{
					 $("#mnyRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>attach_money</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 				"<p>"+
					 				"<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                    "<label for="+item.id+"></label>"+
                                    "</p></div></div>")
					} 
				 indexMoney = indexMoney+1;	
			}
			
			if(item.tipo=="DATA"){
				 
				if(indexData & 1){
					 $("#dataLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>date_range</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				"<p>"+
                            "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                            "<label for="+item.id+"></label>"+
                            "</p></div></div>")
					}else{
					 $("#dataRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>date_range</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 				"<p>"+
					 				"<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                   "<label for="+item.id+"></label>"+
                                   "</p></div></div>")
					} 
				 indexData = indexData+1;
				 
			}
			
			if(item.tipo=="CHECK"){
				 
				if(indexCheck & 1){
					 $("#chkLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>check_box</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				"<p>"+
                           "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                           "<label for="+item.id+"></label>"+
                           "</p></div></div>")
					}else{
					 $("#chkRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>check_box</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 				"<p>"+
					 			  "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                  "<label for="+item.id+"></label>"+
                                  "</p></div></div>")
					} 
				indexCheck = indexCheck+1;
				 
			}
			
			if(item.tipo=="PORCENTAGEM"){
				 
				if(indexPercent & 1){
					 $("#pctLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>check_box</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				"<p>"+
                           "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                           "<label for="+item.id+"></label>"+
                           "</p></div></div>")
					}else{
					 $("#pctRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>check_box</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 				"<p>"+
					 			  "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                  "<label for="+item.id+"></label>"+
                                  "</p></div></div>")
					} 
				indexPercent = indexPercent+1;
				 
			}
			
			if(item.tipo=="LISTA"){
				 
				if(indexLista & 1){
					 $("#lstLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>format_list_bulleted</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				"<p>"+
                          "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                          "<label for="+item.id+"></label>"+
                          "</p></div></div>")
					}else{
					 $("#lstRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>format_list_bulleted</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 				"<p>"+
					 			  "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                 "<label for="+item.id+"></label>"+
                                 "</p></div></div>")
					} 
				indexLista = indexLista+1;
				 
			}
			
			if(item.tipo=="LISTA_CONSULTOR"){
				 
				if(indexListaConsul & 1){
					 $("#lstCLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>contacts</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				"<p>"+
                         "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                         "<label for="+item.id+"></label>"+
                         "</p></div></div>")
					}else{
					 $("#lstCRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>contacts</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 				"<p>"+
					 			  "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                "<label for="+item.id+"></label>"+
                                "</p></div></div>")
					} 
				indexListaConsul = indexListaConsul+1;
				 
			}

			if(item.tipo=="TEXTO_LONGO"){
				 
				if(indexTextLong & 1){
					 $("#txtLLft").append("<div class='card-panel  white z-depth-1'>" +
							 "<i class='material-icons icone'>format_align_left</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
				 				  "<p>"+
		                          "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
		                          "<label for="+item.id+"></label>"+
		                          "</p></div></div>")
					}else{
					 $("#txtLRgt").append("<div class='card-panel  white z-depth-1'>" +
					 		"<i class='material-icons icone'>format_align_left</i><span class='spanInterior'>&nbsp"+item.nome+"</span><div class='insider' style='float: right;'>" +
					 			 "<p>"+
					 			 "<input id="+item.id+" type='checkbox' class='filled-in cartao' />"+
                                 "<label for="+item.id+"></label>"+
                                 "</p></div></div>")
					} 
				indexTextLong = indexTextLong+1;
				 
			}
						 
					 }
				 })	
			}

function solicitadorItens() {

    var id = $("#prodId").val();

    var url = "/itensTarefas/itensT.json"

    return $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data) {}
    });

}