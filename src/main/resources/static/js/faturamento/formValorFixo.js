$("#cnpj").blur(buscaEmpresa);
$("#btnGera").click(geraFats);
//$('.datepicker').datepicker()

$(document).on("change", '#idEmpresa', function() {

	var id = $("#idEmpresa").val();
	buscaContratos(id);

});

$("#cnpj").keyup(function() {

	if (!this.value) {
		limpaTabela();
	}

});

$(function() {
	formatarCnpj();
	buscaEmpresa();
	validarCnpj();
	maskValor();
	$("#corpoSimulacao").toggle();
	$("#simulacaoFalse").toggle();
})

function limpaTabela() {
	$("#tabela > tbody").empty();
	$("#tabelaFats > tbody").empty();
}

function buscaContratos(id) {

	limpaTabela();
	var url = '/contratos/' + id + '.json';
	$.get(url, function(data) {
		$(data).each(function() {

			var contrato = this;

			var fimVigencia;

			if (contrato.vigencia.nome == "VIGÊNCIA INDETERMINADA") {
				fimVigencia = "Sem Vigência"
			} else {
				fimVigencia = contrato.vigencia.fimVigencia
			}

			$("#corpo").append(
				"<tr>" + "<td>"
				+ contrato.produto.nome
				+ "</td>"
				+ "<td>"
				+ contrato.vigencia.inicioVigencia
				+ "</td>"
				+ "<td>"
				+ fimVigencia
				+ "</td>"
				+ "<td>"
				+ contrato.comercialResponsavelEfetivo.nome
				+ "</td>"
				+ "<td>"
				+ "<p>"
				+ "<input name='contratoId' type='radio' value='"
				+ contrato.id
				+ "' id='"
				+ contrato.id
				+ "' />"
				+ "<label for='"
				+ contrato.id
				+ "'></label>"
				+ "</p>"
				+ "</td>"
				+ "</tr>")

		});

	});
}

$(document).on('change', 'input:radio[name="contratoId"]', function(event) {
	var id = $(this).val();

	$("#simulacao").empty();
	$("#dataInicialContratoDiv").focusin()

	var url = '/contratos/unique/' + id + '.json';
	$.get(url, function(data) {

		$(data).each(function() {

			var contrato = this;
			
			$("#dataInicialContratoDiv").append("<input disabled='disabled' name='dataInicialContrato' value='Data Inicial do Contrato: " + 
			contrato.etapasPagamento.dataFaturamento + "' id='dataInicialContrato'/> " +
			"")
			$("#dataInicialContratoDiv").focusin()

			console.log(contrato)

			var honorarios = contrato.honorarios
			for (i = 0; i < honorarios.length; i++) {
				var hon = honorarios[i]
				if (contrato.pagamentoData == true) {
					if (i === honorarios.length - 1 && hon.nome != "Valor Fixo") {
						Materialize.toast('O contrato selecionado não possui Valor Fixo.', 5000, 'rounded');
						$("#simulacaoFalse").append("O contrato selecionado não possui Valor Fixo.")
					} else {

						if (hon.valorAnual == true) {
							console.log(hon)

							$("#simulacaoFalse").toggle();
							$("#corpoFats").append("<tr>" +
								"<td>" + contrato.cliente.razaoSocial + "</td>" +
								"<td>Valor Fixo</td>" +
								"<td>Anual</td>" +
								"<td> R$: " + (hon.valor).toLocaleString('pt-BR') + "</td>" +
								"<td> <input name='dataCobrancaAnual' class='datepicker' value='" + hon.dataCobrancaAnual + "' id='dataCobrancaAnual'</td>")
						}

						if (hon.valorMensal == true) {

							var mes;
							console.log(contrato.etapasPagamento.dataFaturamento)
							var mesAtual = (contrato.etapasPagamento.dataFaturamento.substring(3,5));

							for (i = 0; i < 12; i++) {

								switch (mesAtual) {
									case "01":
										mes = "jan"
										mesAtual = "02"
										break;
									case "02":
										mes = "fev"
										mesAtual = "03"
										break;
									case "03":
										mes = "mar"
										mesAtual = "04"
										break;
									case "04":
										mes = "abr"
										mesAtual = "05"
										break;
									case "05":
										mes = "mai"
										mesAtual = "06"
										break;
									case "06":
										mes = "jun"
										mesAtual = "07"
										break;
									case "07":
										mes = "jul"
										mesAtual = "08"
										break;
									case "08":
										mes = "ago"
										mesAtual = "09"
										break;
									case "09":
										mes = "set"
										mesAtual = "10"
										break;
									case "10":
										mes = "out"
										mesAtual = "11"
										break;
									case "11":
										mes = "nov"
										mesAtual = "12"
										break;
									case "12":
										mes = "dez"
										mesAtual = "01"
										break;
								}
								$("#simulacaoFalse").toggle();
								$("#corpoFats").append("<tr>" +
									"<td>" + contrato.cliente.razaoSocial + "</td>" +
									"<td>Valor Fixo</td>" +
									"<td>Mensal</td>" +
									"<td> R$: " + (hon.valor).toLocaleString('pt-BR') + "</td>" +
									"<td> <input name='dataCobrancaMensal' class='datepicker' value='" + hon.dataCobrancaMensal + "/" + mes + "' id='dataCobrancaMensal'</td>"
								)

							}
						}

						if (hon.valorTrimestral == true) {

							var dataCobranca;
							
							var mes;
							console.log(contrato.etapasPagamento.dataFaturamento)
							var mesAtual = (contrato.etapasPagamento.dataFaturamento.substring(3,5));
							console.log(mesAtual, " mesatual")

							for (i = 0; i < 4; i++) {

								console.log(i)
								switch (i) {
									case 0:
										dataCobranca = "<td> <input name='dataCobrancaTrimestral1' id='dataCobrancaTrimestral1' class='datepicker' value='" + hon.dataCobrancaTrimestral1 + "'</td>";
										break;
									case 1:
										dataCobranca = "<td> <input name='dataCobrancaTrimestral2' id='dataCobrancaTrimestral2' class='datepicker' value='" + hon.dataCobrancaTrimestral2 + "'</td>";
										break;
									case 2:
										dataCobranca = "<td> <input name='dataCobrancaTrimestral3' id='dataCobrancaTrimestral3' class='datepicker' value='" + hon.dataCobrancaTrimestral3 + "'</td>";
										break;
									case 3:
										dataCobranca = "<td> <input name='dataCobrancaTrimestral4' id='dataCobrancaTrimestral4' class='datepicker' value='" + hon.dataCobrancaTrimestral4 + "'</td>";
										break;
								}

								$("#simulacaoFalse").toggle();
								$("#corpoFats").append("<tr>" +
									"<td>" + contrato.cliente.razaoSocial + "</td>" +
									"<td>Valor Fixo</td>" +
									"<td>Trimestral</td>" +
									"<td> R$: " + (hon.valor).toLocaleString('pt-BR') + "</td>" +
									dataCobranca
								)

							}
						}
					}
				} else {
					Materialize.toast('O contrato selecionado é vinculado a entrega de tarefas.', 5000, 'rounded');
				}
			}
		});
	});
});

function maskValor() {
	$(".valor").each(function() {
		var texto = $(this).text();
		texto = parseFloat(texto).toLocaleString('pt-BR')
		texto = "R$ " + texto
		$(this).text(texto);
	})
}

function formatarCnpj() {
	$("#cnpj").mask('00.000.000/0000-00', {
		reverse: true
	});

}

function validarCnpj() {
	var caracteresCnpj = $("#cnpj").val().length;
	if (caracteresCnpj < 18) {
		$("#cnpj").tooltipster("open").tooltipster("content", "CNPJ inválido");
	} else if (caracteresCnpj == 18) {
		$("#cnpj").tooltipster("close");
	}

}

function geraFats(id) {

	var id = $("input:radio[name ='contratoId']:checked").val();
	var ano = $("#campanha").val();

	var dataCobrancaAnual = $("#dataCobrancaAnual").val();
	var dataCobrancaMensal = $("#dataCobrancaMensal").val();
	if($("#dataCobrancaTrimestral1").val() != null &&
		$("#dataCobrancaTrimestral2").val() != null &&
		$("#dataCobrancaTrimestral3").val() != null &&
		$("#dataCobrancaTrimestral4").val() != null) {
	var datasCobrancasTrimestrais = $("#dataCobrancaTrimestral1").val()+""+
									$("#dataCobrancaTrimestral2").val()+""+
									$("#dataCobrancaTrimestral3").val()+""+
									$("#dataCobrancaTrimestral4").val();
	if(datasCobrancasTrimestrais == "undefinedundefinedundefinedundefined" || datasCobrancasTrimestrais == "undefinedundefinedundefinedund/ef/ined") {
		datasCobrancasTrimestrais == null;
	}
	} else {
		var datasCobrancasTrimestrais = null;
	}
	
	var dataCobranca;
	
	if(dataCobrancaAnual != null) {
		dataCobranca = dataCobrancaAnual.replaceAll('/', '');
		dataCobranca.replace(/(\d{ 2 })(\d{ 2 })(\d{ 4 })/, "$1-$2-$3");
	}
	console.log("dataCobrancaMensal",dataCobrancaMensal);
	if(dataCobrancaMensal != null) {
		dataCobranca = dataCobrancaMensal.replaceAll('/', '');
		dataCobranca = dataCobranca.toString();
		dataCobranca.replace(/(\d{ 2 })(\d{ 2 })(\d{ 4 })/, "$1-$2-$3");
		console.log("entrou no if cobrancamensal e o  data é: " + dataCobranca)
	}
	if(datasCobrancasTrimestrais != null) {
		dataCobranca = datasCobrancasTrimestrais.replaceAll('/', '');
		dataCobranca.replace(/(\d{ 2 })(\d{ 2 })(\d{ 4 })/, "$1-$2-$3");
		console.log("entrou no if cobrancatrimestral e o data é: " + dataCobranca)
	}

	console.log("o id é: " + id + " e a data formatada é: " + dataCobranca)

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/faturamentos/generateFixed/" + ano + "/" + dataCobranca,
		data: JSON.stringify(id),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Faturamentos gerados com sucesso!',
						5000, 'rounded');
				$("#btnGera").attr("disabled", true);
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});

}

function buscaEmpresa() {

	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);

	if (!$(this).val().length == 0) {

		$.getJSON("/clientes/" + cnpj + ".json", function(dados) {

			if (dados.erro != true) {

				$("#razaoSocial").val(dados.razaoSocial);
				$("#razaoSocial").focusin();
				$("#idEmpresa").val(dados.id).trigger("change");

			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})

	}

}

function setaData() {

	var idContrato = $("input:radio[name ='contratoId']:checked").val();
//	var dataCobrancaAnual = $("#dataCobrancaAnual").val();
//	dataCobrancaAnual = dataCobrancaAnual.replaceAll('/', '')
//	dataCobrancaAnual.replace(/(\d{ 2 })(\d{ 2 })(\d{ 4 })/, "$1-$2-$3");

	console.log(idContrato, " idContrato")
//	console.log(dataCobrancaAnual, " datacobAnual")

// colocar no ajax os campos com ids diferentes
// trimestre 1, 2, 3 e 4. certo? cada um com id próprio.
// aqui embaixo no ajax, salvar esses campos cada um no seu lugar (1, 2, 3 e 4)
// criar na controller e conseguir resolver pq falhar não é uma opção
// vou criar um baita textão com as 4 datas e usar substring pra separar cada uma. um dia isso vai ser uma história divertida kkkkkk
// envio as 4 datas para a controller que está esperando uma string, dentro da condição do trimestral, eu crio 4 strings
// nelas eu pego partes diferentes da string que chegou no meu método. exemplo:
// string 1 = casas do 0 até o 5
// string 2 = 6 até o 10 e assim por diante
// depois disso, dou um fixo.setvalorttrimestral1 com a string

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/faturamentos/setaData/" + dataCobrancaAnual,
		data: JSON.stringify(idContrato),
		dataType: "JSON",
		statusCode: {
			200: function() {
				Materialize
					.toast(
						'Sucesso!',
						5000, 'rounded');
			},
			500: function() {
				Materialize.toast(
					'Ops',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('não encontrada',
					5000, 'rounded');
			}
		}
	});
}