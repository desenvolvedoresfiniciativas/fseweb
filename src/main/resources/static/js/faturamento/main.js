$('#modalStart').click(showModal)
$('#cadRec').click(cadastraReceptores)
$("#releaseNF").click(function(){
	liberaFaturamento()
	$(this).prop("disabled", true)
})
$("#gerarNF").click()

$(function() {
	$('.collapsible').collapsible();
	$(".modal").modal();
	$(".select").material_select();
	$('#select-etapa').attr("disabled", 'disabled');
	$('#select-etapa').material_select();
	$("#tabela-fat").DataTable({
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			},
			"select": {
				"rows": {
					"_": "Selecionado %d linhas",
					"0": "Nenhuma linha selecionada",
					"1": "Selecionado 1 linha"
				}
			}
		},
		bFilter: false,
		iDisplayLength: 1000,
		columnDefs: [
			{
				targets: [0, 1, 2],
				className: 'mdl-data-table__cell--non-numeric'
			}
		]
	});
	maskValor();
	calculoHonorario();
//	formataCnpjGeral();
	calculoValorPercentualFixo();
	faturamentoManual();
	
	var params = window.location.href
	params = params.slice(params.lastIndexOf('?'));
	
	$("#currentParam").val(params)
	console.log("currentParam",$("#currentParam").val());
	
});

$('#selectProduto').change(
		function() {

			var prodId = $('#selectProduto :selected').val();
			console.log(prodId)

			if (prodId == "") {

				$('#select-etapa').attr("readonly", 'readonly');

				$('#select-etapa').find('option').remove().end().append(
						'<option value="" selected="selected">Selecione</option>').val(
						'whatever');

				$('#select-etapa').material_select();

			} else {

				var url = "/produtos/" + prodId + "/etapas.json"

				var options = "<option selected='selected' value=''>Selecione</option>";

				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {

						$(data).each(
								function(i) {
									var etapa = data[i];
									console.log(etapa.id + " loop antes")
									options += "<option value='" + etapa.id
											+ "'>" + etapa.nome + "</option>"
								});
								
						if(prodId == 23) {
							options += "<option value='" + 156 
											+ "'>NIT</option>"
						}
						console.log(options)

						$("#select-etapa").empty();
						$("#select-etapa").append(options);
						$('#select-etapa').removeAttr('disabled');
						$("#select-etapa").material_select();

					}
				});
			}
		});

function cadastraReceptores() {
	var consults = $("#receptores").val()

	console.log(consults)

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/faturamentos/addReceptores",
		data : JSON.stringify(consults),
		dataType : "JSON",
		statusCode : {
			200 : function() {
				Materialize.toast('Receptores atualizados com sucesso!', 5000,
						'rounded');
			},
			500 : function() {
				Materialize
						.toast('Ops, houve um erro interno', 5000, 'rounded');
			},
			400 : function() {
				Materialize.toast('Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404 : function() {
				Materialize.toast('Url não encontrada', 5000, 'rounded');
			}
		}
	});

}

function showModal() {
	$('#modal').modal('open');
	$("#divCompleta").replace($("#divCompleta"), "")
}

function maskValor() {
	$(".money").each(function() {
		var texto = $(this).text();
//		texto = parseFloat(texto).toLocaleString('pt-BR')
		texto = "R$ " + texto
		$(this).text(texto);
	})
}

$('.btnLiberar').click(function(){
	
	console.log("clicou o botão")
	
	
	var fatId = $(this).attr('id')
	var url = "/faturamentos/" + fatId + "/fat.json"
	console.log("id fo faturamento",fatId);
	
	
	
	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
				console.log("data botão liberar faturamento info",data);
		if(data.consolidado == true){
			console.log("entrou no consolidado = true")
			$('#fatIdHolder').val(fatId);
			$("#divCompleta").empty()
			$("#tbody-escalonado").empty()
			limpaModal();
			$('#corpoFats').show();
			$("#tabBalancete").hide();
			$("#memoriaCalculoConsolidado").show();
			console.log("consolidado")
			simularConsolidacao(fatId);
			
			console.log("fatIdHolder",fatId);
			
			var cnpj = data.cliente.cnpj
			cnpj = cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");


			$("#nfRazaoSocial").text(data.cliente.razaoSocial)
			$("#nfCnpj").text(cnpj)
			$("#nfRazaoSocialFaturar").text(data.contrato.razaoSocialFaturar)
			$("#nfCnpjFaturar").text(data.contrato.cnpjFaturar)
			$("#nfProduto").text(data.produto.nome)
			$("#nfValorEtapa").text("R$ "+mascaraValor(data.valorEtapa))
			$("#nfFormaPagemento").text(data.contrato.formaPagamento)
			
			try{
			$("#nfEtapaFaturar").text(data.etapa.nome)
			$("#nfPorcentEtapa").text(data.percetEtapa)
				if(data.etapas.concluido==true){
					$("#nfConclusao").text("Etapa concluída")
				}
			
		}catch(e){
			
		}
		
		console.log("data.contrato.formaPagamento",data.contrato.formaPagamento)
			var i = 0
			var divCompleta = "<h5>Cálculo de Faturamento F.I.</h5> <hr />";
			$("#divCompleta").append(divCompleta)

			for(i; i <= data.contrato.honorarios.length; i++) {
				var tipoDeHonorario;
				var valorBeneficio = (data.balancete.reducaoImposto).replace(/\./g, '').replace(",",".");
				console.log("valorBeneficio var",valorBeneficio)

				switch (data.contrato.honorarios[i].nome) {

				case "Percentual Escalonado":
				tipoDeHonorario = "PERCENTUAL ESCALONADO";
				var faixas = data.contrato.honorarios[i].faixasEscalonamento;
				var index = 0
				var temp = valorBeneficio;
				var valorCalc = 0;
				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				var cabecalhoTabela = $("<table class='responsive-table table-default bordered'><thead><tr><th>Intervalo de Faturamento</th><th>Percentual %</th><th>Honorários</th></tr></thead>")

				for(var index in faixas) {

				var inicial = faixas[index].valorInicial;
				inicial = Number(Math.floor(inicial, -1).toFixed(2))
				inicial = parseFloat(inicial).toFixed(2)
				var finalF = faixas[index].valorFinal;
				finalF = Number(Math.floor(finalF, -1).toFixed(2))
				finalF = parseFloat(finalF).toFixed(2)
				var gap1 = finalF - inicial;
				var percentual = faixas[index].valorPercentual / 100;

				if (temp > 0) {

					if (temp >= gap1) {
						valorCalc = (gap1 * percentual);
						temp = temp - (gap1);
//						valorCalc = Math.round(valorCalc);
//						console.log(valorCalc, " valorcalc com round")
						} else if (temp < gap1) {
							console.log("Será calculado dentro dentro da faixa com o valor restante do temp...");
							valorCalc = (temp * percentual);
							temp = temp - (gap1);
//							valorCalc = Math.round(valorCalc);
						}
					} else {
						valorCalc = 0;
					}

				var tr = $("<tr>")
				var tds = $("<td>De R$ "+mascaraValor(inicial)+" à R$ "+mascaraValor(finalF)+"</td>" +
				"<td>"+faixas[index].valorPercentual+"%</td>"+
				"<td>R$ "+mascaraValor(valorCalc.toFixed(2))+"</td>")
				tr.append(tds)

				index++
				cabecalhoTabela.append(tr)
				}
				
				$("#divCompleta").append(cabecalhoTabela)
				
				if(temp != 0) {
					var valorAcimaDe = Number(data.contrato.honorarios[i].acimaDe)
					var percentualAcimaDe = Number((data.contrato.honorarios[i].percentAcimaDe) / 100);
					var honorarioAcimaDe = Number(percentualAcimaDe) * Number(temp);

					var limitacao = Number(data.contrato.honorarios[i].limitacao);
					console.log(data.contrato.honorarios[i].limitacao);

					if (limitacao != 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
						"<td>"+percentualAcimaDe*100+"%</td>"+
						"<td>R$ "+mascaraValor(limitacao.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						cabecalhoTabela.append(trAcimaDe)
						$("#divCompleta").append(cabecalhoTabela)
					} else if (temp > 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
						"<td>"+percentualAcimaDe*100+"%</td>"+
						"<td>R$ "+mascaraValor(honorarioAcimaDe.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						cabecalhoTabela.append(trAcimaDe)
						$("#divCompleta").append(cabecalhoTabela)
					} else if (temp <= 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
						"<td>"+percentualAcimaDe*100+"%</td>"+
						"<td>R$ 0,00</td>")
						trAcimaDe.append(tdsAcimaDe)
						cabecalhoTabela.append(trAcimaDe)
						$("#divCompleta").append(cabecalhoTabela)
					}
				}

				break;

				case "Valor Fixo":
				tipoDeHonorario = "VALOR FIXO";
				var valorFixo = Number(data.contrato.honorarios[i].valor);
				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				$("#divCompleta").append("<b>Valor Fixo: </b> <p style='display:inline'>R$ "+mascaraValor(valorFixo.toFixed(2))+"</p><br /><hr />")
				break;

				case "Percentual Fixo":
				tipoDeHonorario = "PERCENTUAL FIXO";
				var percentualFixo = Number(data.contrato.honorarios[i].valorPercentual);
				var calculoPercentualFixo = (percentualFixo/100) * Number(valorBeneficio)

				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				$("#divCompleta").append("<b>Porcentagem Fixa: </b> <p style='display:inline'>"+percentualFixo+"%</p><br />")
				$("#divCompleta").append("<b>Valor: </b> <p style='display:inline'>R$ "+mascaraValor(calculoPercentualFixo.toFixed(2))+"</p><br /><hr />")
				break;
				
				default:
				$("#nfTipoDeHonorario").text("Sem Memória de Cálculo")
				break;
				}

				var value1 = data.contrato.honorarios[i].nome; // this value1 you can assign it into your textbox
				console.log(value1," nome dele")
			};
		}
			
		if(data.consolidado == false &&  data.consolidadoPorFat == true){
			console.log("entrou no consolidado por faturamento")
			$('#corpoFats').hide();
			$("#memoriaCalculoConsolidado").hide();
			$('#fatIdHolder').val(fatId)
			$("#corpoFatConsolidados").empty()
			$("#tabBalancete").hide();
			limpaModal();
			
			console.log("entrou no btl leberar");
			var cnpj = data.cliente.cnpj
			cnpj = cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");

			
			try{
			$("#nfRazaoSocial").text(data.cliente.razaoSocial)
			$("#nfCnpj").text(cnpj)
			$("#nfRazaoSocialFaturar").text(data.contrato.razaoSocialFaturar)
			$("#nfCnpjFaturar").text(data.contrato.cnpjFaturar)
			$("#nfProduto").text(data.produto.nome)
			$("#nfValorEtapa").text("R$ "+mascaraValor(data.valorEtapa))
			$("#nfApuracao").text(data.producao.tipoDeApuracao)
			$("#nfVersao").text(data.balancete.versao);
			$("#nfDispendio").text("R$ "+mascaraValor(data.balancete.valorTotal))
			$("#nfReducao").text("R$ "+mascaraValor(data.balancete.reducaoImposto))
			}catch(e){
				
			}
			
			try{
			$("#nfFormaPagemento").text(data.contrato.formaPagamento)
			$("#nfEtapaFaturar").text(data.etapa.nome)
			$("#nfPorcentEtapa").text(data.percetEtapa)
				if(data.etapas.concluido==true){
					$("#nfConclusao").text("Etapa concluída")
				}
			
			}catch(e){
				
			}
			var titulo = "<h5>Informações da Consolidação</h5><hr/>";
			$("#corpoFatConsolidados").append(titulo)
			 simularConsolidacaoFaturamento(fatId);
		}
		
		if((data.consolidado == false && data.consolidadoPorFat == false )||( data.consolidadoPorFat == null && data.consolidado == false)){
			
			$('#corpoFats').hide();
			$("#memoriaCalculoConsolidado").hide();
			$('#fatIdHolder').val(fatId)
			$("#divCompleta").empty()
			$("#tabBalancete").show();
			limpaModal();
			
			console.log("entrou no btl leberar");
			var cnpj = data.cliente.cnpj
			cnpj = cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");

			
			try{
			$("#nfRazaoSocial").text(data.cliente.razaoSocial)
			$("#nfCnpj").text(cnpj)
			$("#nfRazaoSocialFaturar").text(data.contrato.razaoSocialFaturar)
			$("#nfCnpjFaturar").text(data.contrato.cnpjFaturar)
			$("#nfProduto").text(data.produto.nome)
			$("#nfValorEtapa").text("R$ "+mascaraValor(data.valorEtapa))
			$("#nfApuracao").text(data.producao.tipoDeApuracao)
			$("#nfVersao").text(data.balancete.versao);
			$("#nfDispendio").text("R$ "+mascaraValor(data.balancete.valorTotal))
			$("#nfReducao").text("R$ "+mascaraValor(data.balancete.reducaoImposto))
			}catch(e){
				
			}
			
			try{
			$("#nfFormaPagemento").text(data.contrato.formaPagamento)
			$("#nfEtapaFaturar").text(data.etapa.nome)
			$("#nfPorcentEtapa").text(data.percetEtapa)
				if(data.etapas.concluido==true){
					$("#nfConclusao").text("Etapa concluída")
				}
			
			}catch(e){
				
			}
				
				
			var i = 0
			var divCompleta = "<h5>Cálculo de Faturamento F.I.</h5> <hr />";
			$("#divCompleta").append(divCompleta)
			
			try{
			for(i; i <= data.contrato.honorarios.length; i++) {
				
				var tipoDeHonorario;
					
					var valorBeneficio = (data.balancete.reducaoImposto).replace(/\./g, '').replace(",",".");
					console.log("valorBeneficio var",valorBeneficio)
					
					
				switch (data.contrato.honorarios[i].nome) {
				
				case "Percentual Escalonado":
				tipoDeHonorario = "PERCENTUAL ESCALONADO";
				
				var faixas = data.contrato.honorarios[i].faixasEscalonamento;
				var index = 0
				var temp = valorBeneficio;
				var valorCalc = 0;
				var totalHonorariosAplicados = 0.0;
				var totalHonorarios = 0.0;
				
				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				var cabecalhoTabela = $("<table class='responsive-table table-default bordered'><thead><tr><th>Intervalo de Faturamento</th><th>Percentual %</th><th>Honorários</th></tr></thead>")
				
				for(var index in faixas) {

				var inicial = faixas[index].valorInicial;
				inicial = Number(Math.floor(inicial, -1).toFixed(2))
				inicial = parseFloat(inicial).toFixed(2)
				var finalF = faixas[index].valorFinal;
				finalF = Number(Math.floor(finalF, -1).toFixed(2))
				finalF = parseFloat(finalF).toFixed(2)
				var gap1 = finalF - inicial;
				var percentual = faixas[index].valorPercentual / 100;

				if (temp > 0) {
					if (temp >= gap1) {
						valorCalc = (gap1 * percentual);
						console.log(valorCalc)
						temp = temp - (gap1);
						} else if (temp < gap1) {
							console.log("Será calculado dentro dentro da faixa com o valor restante do temp...");
							valorCalc = (temp * percentual);
							temp = temp - (gap1);
						}
					} else {
						valorCalc = 0;
					}

				var tr = $("<tr>")
				var tds = $("<td>De R$ "+mascaraValor(inicial)+" à R$ "+mascaraValor(finalF)+"</td>" +
				"<td>"+faixas[index].valorPercentual+"%</td>"+
				"<td>R$ "+mascaraValor(valorCalc.toFixed(2))+"</td>")
				tr.append(tds)

				index++
				cabecalhoTabela.append(tr)
				}
				
				$("#divCompleta").append(cabecalhoTabela)
				
				if(temp != 0) {
					var valorAcimaDe = Number(data.contrato.honorarios[i].acimaDe)
					var percentualAcimaDe = Number((data.contrato.honorarios[i].percentAcimaDe) / 100);
					var honorarioAcimaDe = Number(percentualAcimaDe) * Number(temp);

					var limitacao = Number(data.contrato.honorarios[i].limitacao);
					console.log(data.contrato.honorarios[i].limitacao);

					if (limitacao != 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
						"<td>"+percentualAcimaDe*100+"%</td>"+
						"<td>R$ "+mascaraValor(limitacao.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						cabecalhoTabela.append(trAcimaDe)
						$("#divCompleta").append(cabecalhoTabela)
					} else if (temp > 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
						"<td>"+percentualAcimaDe*100+"%</td>"+
						"<td>R$ "+mascaraValor(honorarioAcimaDe.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						cabecalhoTabela.append(trAcimaDe)
						$("#divCompleta").append(cabecalhoTabela)
					} else if (temp <= 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+mascaraValor(valorAcimaDe.toFixed(2))+"</td>" +
						"<td>"+percentualAcimaDe*100+"%</td>"+
						"<td>R$ 0,00</td>")
						trAcimaDe.append(tdsAcimaDe)
						cabecalhoTabela.append(trAcimaDe)
						$("#divCompleta").append(cabecalhoTabela)
					}
					
				}
				
				break;
				
				case "Valor Fixo":
				console.log("valorfixo");
				
				tipoDeHonorario = "VALOR FIXO";
				
				var valorFixo = Number(data.contrato.honorarios[i].valor);
				console.log("teste valorFixo",valorFixo)
				
				console.log("entrou no catch do valorFixo")
				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				
				$("#divCompleta").append("<b>Valor Fixo: </b> <p style='display:inline'>R$ "+mascaraValor(valorFixo.toFixed(2))+"</p><br /><hr />")
				
					
				
					
				break;
				
				case "Percentual Fixo":
				tipoDeHonorario = "PERCENTUAL FIXO";
				var percentualFixo = Number(data.contrato.honorarios[i].valorPercentual);
				var calculoPercentualFixo = (percentualFixo/100) * Number(valorBeneficio)
				console.log("entrou no catch do PERCENTUAL FIXO calculoPercentualFixo",calculoPercentualFixo);
				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				$("#divCompleta").append("<b>Porcentagem Fixa: </b> <p style='display:inline'>"+percentualFixo+"%</p><br />")
				$("#divCompleta").append("<b>Valor: </b> <p style='display:inline'>R$ "+mascaraValor(calculoPercentualFixo.toFixed(2))+"</p><br /><hr />")
				break;
				
				default:
				$("#nfTipoDeHonorario").text("Sem Memória de Cálculo")
				break;
				}
				
				
				var value1 = data.contrato.honorarios[i].nome; // this value1 you can assign it into your textbox
				console.log(value1," nome dele")
				
			};
			}catch(e){
				
			}
			
			
			}
			
		
		}
	});
	
})

function liberaFaturamento(){
	
	var fatId = $('#fatIdHolder').val()
	console.log("teste fatId LiberaFaturamento",fatId);
	var fat = {
		id: fatId
	}
	var estadoFat;
	/*teste*/
	$.ajax({
		type : 'GET',
		url : "/faturamentos/" + fatId + "/fat.json",
		async : false,
		success : function(fat) {
			console.log("fat",fat.id);
			console.log("fat",fat.estado);
			estadoFat = fat.estado;
			console.log("fat",fat.estado);
		}
	})
	
	if(estadoFat == "CANCELADO" || estadoFat =="PERDIDO" ){
		console.log("faturamento cancelado ou perdido");
		console.log("nÃO LIBERA O FATURAMENTO");
		Materialize.toast('Não é possível liberar o faturamento com estado: '+ estadoFat, 5000,
						'rounded');
	}else{
	/*fimTeste*/
	
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/faturamentos/libera",
		data : JSON.stringify(fat),
		dataType : "JSON",
		statusCode : {
			200 : function() {
				Materialize.toast('Faturamento liberado com sucesso!', 5000,
						'rounded');
				$("#lihha"+fatId).hide();
				$('#modalLiberarNF').modal('close');
			},
			500 : function() {
				Materialize
						.toast('Ops, houve um erro interno', 5000, 'rounded');
			},
			400 : function() {
				Materialize.toast('Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404 : function() {
				Materialize.toast('Url não encontrada', 5000, 'rounded');
			}
		}
	});
	}
}

function calculoHonorario(){

	var balancete = $("#stringValorBalancete").val();
	var temp = $("#stringValorBalancete").val();

	var idContrato = $("#idContrato").val();
	var reducaoImposto = $("#reducaoImposto").val();

	if ($('#reducaoImposto').val() != null) {
					reducaoImposto = parseFloat($('#reducaoImposto').val().replace(/\./g, '')
							.replace(",", "."));
				} else {
					reducaoImposto = 0
				}

	var numeroDeFaixas = document.getElementsByClassName("calc");

	$.ajax({
		type : 'GET',
		url : "/faturamentos/" + idContrato + "/pe.json",
		async : false,
		success : function(pe) {

			var faixas = pe.faixasEscalonamento;
			var campo;

			var index = 0;
			var temp = parseFloat($('#beneficio').text().replace("R","").replace("$","").replace(/\./g, '').replace(",", "."));
			var valorCalc = 0;
			var valorTotalAplicado = 0.0;
			var valorTotalHonorarios = 0.0;

			for(var index in faixas) {

				var inicial = faixas[index].valorInicial;
				inicial = Number(Math.floor(inicial, -1).toFixed(2))
				inicial = parseFloat(inicial).toFixed(2)
				var finalF = faixas[index].valorFinal;
				finalF = Number(Math.floor(finalF, -1).toFixed(2))
				finalF = parseFloat(finalF).toFixed(2)
				var gap1 = finalF - inicial;
				var percentual = faixas[index].valorPercentual / 100;
				var valorAplicado = 0;

				if (temp > 0) {

					if (temp >= gap1) {
						console.log("Passa direto da faixa, calcula com o valor final da faixa...");
						valorCalc = (gap1 * percentual);
						valorTotalHonorarios += valorCalc
						temp = temp - (gap1);
						valorAplicado = gap1
						valorTotalAplicado += valorAplicado
						} else if (temp < gap1) {
//							console.log("Será calculado dentro dentro da faixa com o valor restante do temp...");
							valorCalc = (temp * percentual);
							valorTotalHonorarios += valorCalc
							valorAplicado = temp
							valorTotalAplicado += valorAplicado
							temp = temp - (gap1);
						}
					} else {
						valorCalc = 0;
						valorAplicado = 0;
						valorTotalHonorarios += valorCalc
						valorTotalAplicado += valorAplicado
					}

				$("#calc"+index).append("R$ " + mascaraValor(valorCalc.toFixed(2)));
				$("#valoresAplicados"+index).append("R$ " + mascaraValor(valorAplicado.toFixed(2)));
				$("#valores"+index).append("De R$ " + mascaraValor(inicial) + " à R$ " + mascaraValor(finalF))
				index++;
			}

			if (temp >= 0) {
				var percentualAcima = Number(pe.percentAcimaDe) / 100;
				var honorarioAcimaDe = Number(percentualAcima) * Number(temp);
				valorTotalHonorarios = parseFloat(valorTotalHonorarios.toFixed(2));

				var vlAcimaDe = $('#idValorAcimaDe').val();
				vlAcimaDe = Number(Math.floor(vlAcimaDe, -1).toFixed(2))
				vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
				console.log(vlAcimaDe)

				var limitacao = Number($('#idLimitacao').val());
				console.log(limitacao)
				limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
				limitacao = parseFloat(limitacao).toFixed(2)

				if (limitacao > 0) {
					if (temp > 0 && temp < limitacao) {
						$("#valorAcimaDe").append("Acima de R$ " + mascaraValor(vlAcimaDe) + " (Limitação de R$ " + mascaraValor(limitacao) + ")");
						$("#valorAplicadoAcimaDe").append("R$ " + mascaraValor(temp.toFixed(2)));
						$("#honorarioAcimaDe").append("R$ " + mascaraValor(temp.toFixed(2)) + " Valor Limitado");
						valorAplicado = temp;
						valorTotalAplicado = parseFloat(valorTotalAplicado)
						valorTotalAplicado += parseFloat(valorAplicado)
						valorTotalHonorarios = parseFloat(valorTotalHonorarios)
						valorTotalHonorarios += parseFloat(honorarioAcimaDe)
					} else if (temp > 0 && temp >= limitacao) {
						$("#valorAcimaDe").append("Acima de R$ " + mascaraValor(vlAcimaDe) + " (Limitação de R$ " + mascaraValor(limitacao) + ")");
						$("#valorAplicadoAcimaDe").append("R$ " + mascaraValor(limitacao));
						var valorAplicadoAcimaDe = limitacao * percentualAcima;
						$("#honorarioAcimaDe").append("R$ " + mascaraValor(valorAplicadoAcimaDe.toFixed(2)) + " Valor Limitado");
						valorAplicado = limitacao;
						valorTotalAplicado = parseFloat(valorTotalAplicado)
						valorTotalAplicado += parseFloat(valorAplicado)
						valorTotalHonorarios = parseFloat(valorTotalHonorarios)
						valorTotalHonorarios += parseFloat(valorAplicadoAcimaDe)
					} else {
						$("#valorAcimaDe").append("Acima de R$ " + mascaraValor(vlAcimaDe) + " (Limitação de R$ " + mascaraValor(limitacao) + ")");
						$("#valorAplicadoAcimaDe").append("R$ 0,00");
						$("#honorarioAcimaDe").append("R$ 0,00");
					}
				} else if (temp > 0) {
					$("#valorAcimaDe").text("Acima de R$ " + mascaraValor(vlAcimaDe) + " (Limitação de R$ " + mascaraValor(limitacao) + ")");
					$("#valorAplicadoAcimaDe").text("R$ " +mascaraValor(temp.toFixed(2)));
					$("#honorarioAcimaDe").text("R$ " + mascaraValor(honorarioAcimaDe.toFixed(2)));
					valorAplicado = temp;
					valorTotalAplicado = parseFloat(valorTotalAplicado)
					valorTotalAplicado += parseFloat(valorAplicado)
					valorTotalHonorarios = parseFloat(valorTotalHonorarios)
					valorTotalHonorarios += parseFloat(honorarioAcimaDe)
				} else if (temp <= 0) {
					$("#valorAcimaDe").append("Acima de R$ " + mascaraValor(vlAcimaDe) + " (Limitação de R$ " + mascaraValor(limitacao) + ")");
					$("#valorAplicadoAcimaDe").append("R$ 0,00");
					$("#honorarioAcimaDe").append("R$ 0,00");
				}

			valorTotalHonorarios = parseFloat(valorTotalHonorarios).toFixed(2)
			valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
			$("#totalHonorariosAplicados").text("Total Aplicado: R$ " + mascaraValor(valorTotalAplicado));
			$("#totalHonorarios").text("Total: R$ " + mascaraValor(valorTotalHonorarios));
		} else if (temp < 0) {

			var percentualAcima = Number(pe.percentAcimaDe) / 100;
			var honorarioAcimaDe = Number(percentualAcima) * Number(temp);
			valorTotalHonorarios = parseFloat(valorTotalHonorarios.toFixed(2));

			var vlAcimaDe = $('#idValorAcimaDe').val();
			vlAcimaDe = Number(Math.floor(vlAcimaDe, -1).toFixed(2))
			vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
			console.log(vlAcimaDe)
			var limitacao = Number($('#idLimitacao').val());
			limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
			limitacao = parseFloat(limitacao).toFixed(2)
			
			if (limitacao > 0) {
				$("#valorAcimaDe").append("Acima de R$ " + mascaraValor(vlAcimaDe) + " (Limitação de R$ " + mascaraValor(limitacao) + ")");
				$("#valorAplicadoAcimaDe").append("R$ 0,00");
				$("#honorarioAcimaDe").append("R$ 0,00");
			} else {
				$("#valorAcimaDe").append("Acima de R$ " + mascaraValor(vlAcimaDe));
				$("#valorAplicadoAcimaDe").append("R$ 0,00");
				$("#honorarioAcimaDe").append("R$ 0,00");
			}

			valorTotalHonorarios = parseFloat(valorTotalHonorarios).toFixed(2)
			valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
			$("#totalHonorariosAplicados").text("Total Aplicado: R$ " + mascaraValor(valorTotalAplicado));
			$("#totalHonorarios").text("Total: R$ " + mascaraValor(valorTotalHonorarios));
		}
	}
	});
}

function calculoValorPercentualFixo(){

	var fatId = $("#fatId").val();
	console.log("fatId",fatId)
	$.ajax({
		type : 'GET',
		url : "/faturamentos/" + fatId + "/fat.json",
		async : false,
		success : function(pf) {

			var percentualFixo = parseFloat($('#idPercentualFixo').text().replace(",", "."));
			var valorASerCalculado = parseFloat($('#beneficio').text().replace("R","").replace("$","").replace(/\./g, '').replace(",", "."));
			console.log("pF", percentualFixo)
			console.log("valorasercalc", valorASerCalculado)

			var valorDoPercentualFixo = Number(percentualFixo / 100) * Number(valorASerCalculado)
			
			$('#idPercentualFixoHonorarioValorAplicado').append("R$ " + mascaraValor(valorASerCalculado.toFixed(2)))
			$('#idPercentualFixoHonorario').append("R$ " + mascaraValor(valorDoPercentualFixo.toFixed(2)))
		}
	});
}

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

function formataCnpjGeral(){
    var cellText = $(".cnpj").html();
    cellText = cellText.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");
    $($(".cnpj")).html(cellText);
}

function limpaModal() {
			$("#nfRazaoSocial").text("")
			$("#nfCnpj").text("")
			$("#nfRazaoSocialFaturar").text("")
			$("#nfCnpjFaturar").text("")
			$("#nfProduto").text("")
			$("#nfValorEtapa").text("")
			$("#nfApuracao").text("")
			$("#nfVersao").text("")
			$("#nfDispendio").text("")
			$("#nfReducao").text("")
			$("#nfFormaPagemento").text("")
			$("#nfEtapaFaturar").text("")
			$("#nfPorcentEtapa").text("")
			$("#nfConclusao").text("")
			$('#corpoFats').text("")
	
}

function balancete() {
    $("#TabelaBalancete").empty();
    $('#modalbalancete').modal('open');
    modalBalancete()
}

function modalBalancete() {
    cliente = $("#idcliente")
	var idProducao = $("#idProducao").val();
	console.log("idProducao",idProducao);

    $.ajax({
        type: 'GET',
        url: "/producoes/" + idProducao + "/listaBalanceteFaturamento.json",
        success: function (listaBalanceteFaturamento) {
            for (var i = 0; i < listaBalanceteFaturamento.length; i++) {
					
					var idBalancete = listaBalanceteFaturamento[i].id;
					var gerouFaturamento = listaBalanceteFaturamento[i].gerouFaturamento;
                	var reducaoImposto = "<td> R$ " + listaBalanceteFaturamento[i].reducaoImposto + "</td>";
                	var valorTotal = "<td>  R$ " + listaBalanceteFaturamento[i].valorTotal + "</td>";
                	var anoCampanha = "<td>" + listaBalanceteFaturamento[i].producao.ano + "</td>";
					var versao  = "<td>" + listaBalanceteFaturamento[i].versao + "</td>";
                	if (gerouFaturamento == false) {
                    	gerouFaturamento = "<td>" + "Não" + "</td>";
                	} else {
                    	gerouFaturamento = "<td>" + "Sim" + "</td>";
                	}

               		buscaIdEtapasBalancete(idProducao)
					var idEtapas = $('#idDoEtapas').val();
					console.log("IdEtapas "+idEtapas)
					
                	var botaoBalancete = "<td> <button class='waves-effect light-blue darken-4 ' style='height:20px;width:25px' onclick= window.location.href='/producoes/balanceteUpdate/" + idBalancete + "?idEtapa=" + idEtapas + "'></button> </td>"
                	$("#TabelaBalancete").append("<tr>" + anoCampanha + versao +valorTotal + reducaoImposto + gerouFaturamento + botaoBalancete + "</tr>")
	
			}
        }
    })

 	function buscaIdEtapasBalancete(idProducao) {

    	$.ajax({
        	type: 'GET',
        	url: "/producoes/" + idProducao + "/etapasBalancetes.json",
			async: false,
        	success: function (etapa) {
				 $('#idDoEtapas').val(etapa.id);
            }
    	})
	}

}

function detailsFaturamento(fatId){
	parametrosFiltro();
	

	var url = "/faturamentos/" + fatId + "/fat.json"
	
	
	
	console.log("faturamento Id ",fatId);
	
	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			
			console.log("data",data);
			console.log("data",data.consolidado); 
			
			
			if(data.consolidado == true || data.consolidadoPorFat){
				 window.location.href = "/faturamentos/detailsFaturamentoConsolidado/"+ fatId;
			}
			else{
				window.location.href = "/faturamentos/details/"+fatId;
			}
		}
	})		
}

function updateFaturamento(fatId){

	var url = "/faturamentos/" + fatId + "/fat.json"
	
	console.log("faturamento Id para updateFaturamento ",fatId);
	
	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			
			console.log("data",data);
			console.log("data",data.consolidado);
			
			if(data.consolidado == true|| data.consolidadoPorFat){
				//window.location.href ="https://www.google.com/"
				window.location.href = "/faturamentos/faturamentoConsolidadoUpdate/"+fatId;
			}
			else{
				window.location.href = "/faturamentos/faturamentoUpdate/"+fatId;
			}
		}
	})		
}

function simularConsolidacao(idFaturamento) {
	
	
	console.log("idFaturamento",idFaturamento);
		
	 var titulo = ("<h5>Informações da Consolidação</h5> <hr/>")
	 $("#corpoFats").append(titulo)
	
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
					ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
		
	
	console.log("fatsTeste",fats);
	
    var url = "/faturamentos/simulaFatConsolidado"
     $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
            console.log(response)
            if (response.erro == "noBalancete") {
                Materialize.toast('Um ou mais faturamentos não possuem balancetes vinculados, não é possível consolidar o benefício', 5000, 'rounded');
            } else if (response.erro == "mesmoBalancete") {
                Materialize.toast('Dois ou mais faturamentos possuem a mesma versão de balancete, não é possível consolidar o benefício.', 5000, 'rounded');
            } else if (response.erro == "mesmaEtapa") {
                Materialize.toast('Dois ou mais faturamentos não possuem a mesma etapa de faturamento, não é possível consolidar o benefício.', 5000, 'rounded');
            } else {
                $('#beneficioConsolidado').show();

                //CARREGA VALORES
                $("#cnpjFat").val(response.faturamentoConsolidado.cliente.cnpj)
                $("#cnpjFat").mask('00.000.000/0000-00', {
                    reverse: true
                });
                $("#cnpjFat").focusin();

                $("#razaoSocialFat").val(response.faturamentoConsolidado.cliente.razaoSocial)
                $("#razaoSocialFat").focusin();
                $("#idCliente").val(response.faturamentoConsolidado.cliente.id)

                $("#idProduto").val(response.faturamentoConsolidado.producao.produto.id)
                $("#produto").val(response.faturamentoConsolidado.producao.produto.nome)
                $("#produto").focusin();

				$("#idProducao").val(response.faturamentoConsolidado.producao.id)

				$("#idEtapa").val(response.faturamentoConsolidado.etapa.id)
                $("#etapa").val(response.faturamentoConsolidado.etapa.nome)
                $("#etapa").focusin();

				$('#observacao').val(response.faturamentoConsolidado.observacao)
				$('#observacao').focusin();

				$("#idsFatsConsolidados").val(response.faturamentoConsolidado.idsFaturamentosConsolidados)

                $("#percentEtapa").val(response.percent)
                $("#percentEtapa").focusin();

                $("#campanha").val(response.faturamentoConsolidado.producao.ano)
                $("#campanha").focusin();

                var valor = response.faturamentoConsolidado.valorEtapa

                if (Number.isInteger(valor)) {
                    valor = valor + ".00"
                }

                $("#valorEtapa").val(mascaraValor(valor))
                $("#valorEtapa").focusin();

                var divFats = $("#fatsConsolidados")


                var divPai = $("<div'></div>")

                var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
                    + '</tr>'
                    + '</thead>')
                var tableBody = $('<tbody id="tbody-informacoes"></tbody>')

                for (var i = 0; i < response.fatsIds.length; i++) {
                    var fatC = response.fatsIds[i];

                    var valorEtapa = fatC.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatC.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatC.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatC.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + mascaraValor(parseFloat(fatC.balancete.valorTotal).toFixed(2)) + '</td>')
                    var reducao = $('<td> R$ ' +mascaraValor(parseFloat(fatC.balancete.reducaoImposto ).toFixed(2)) + '</td>')
                    var etapa = $('<td>' + fatC.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(parseFloat(valorEtapa).toFixed(2)) + '</td>')

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)

                    tableBody.append(tr)

                }

                var trTotal = $('<tr></tr>')

                var beneficioTotal = response.beneficioConsolidado

                if (Number.isInteger(beneficioTotal)) {
                    beneficioTotal = beneficioTotal + ".00"
                }

                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
console.log(" incineroar")
                var total = $('<td><b>Total: R$' +mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(total)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)

                tableBody.append(trTotal)

                table.append(tableBody)
                divPai.append(table)

                $("#corpoFats").append(divPai)

                var idContrato = response.faturamentoConsolidado.contrato.id

                getMemoriaDeCalculo(idContrato, beneficioTotal);

            }
        },
        error: function (xhr) {

        }
    });

}
})
}

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g, "");
    valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
    return valor
}

function getMemoriaDeCalculo(idContrato, beneficioTotal) {

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idContrato + "/honorarios.json",
        async: false,
        success: function (hon) {

            for (var i = 0; i < hon.length; i++) {
                var honorario = hon[i];
                console.log(honorario)

                if (honorario.nome == "Percentual Fixo") {

                    $('#percentFixo').show()

                    var trFixo = $("<tr></tr>");

                    var beneficioFixo = $('<td> R$ ' + mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</td>')
                    var percentualFixo = $('<td>' + honorario.valorPercentual + '%</td>')

                    var percent = (honorario.valorPercentual / 100)
                    var resultado = (beneficioTotal * percent) * 100

                    var valor = $('<td class="resultadoPF">R$ ' + mascaraValor(parseFloat(resultado).toFixed(2)) + '</td>')

                    trFixo.append(beneficioFixo)
                    trFixo.append(percentualFixo)
                    trFixo.append(valor)

                    $('#tbody-percentualFixo').append(trFixo);

                }
                if (honorario.nome == "Percentual Escalonado") {
                    $('#escalonado').show()

                    var temp = beneficioTotal;
                    var valorCalc = 0.00;
                    var total = 0.00;

                    var faixas = honorario.faixasEscalonamento

                    for (var i = 0; i < faixas.length; i++) {

                        var faixa = faixas[i];

                        var linhaFaixa = $("<tr></tr>");

                        var inicio = faixa.valorInicial;
                        var fim = faixa.valorFinal;
                        var beneficioAtribuido = 0.00;

                        if (Number.isInteger(inicio)) {
                            inicio = inicio + ".00"
                        } else {
							inicio = Number(Math.floor(inicio, -1).toFixed(2))
							inicio = parseFloat(inicio).toFixed(2)
						}

                        if (Number.isInteger(fim)) {
                            fim = fim + ".00"
                        } else {
							fim = Number(Math.floor(fim, -1).toFixed(2))
							fim = parseFloat(fim).toFixed(2)
						}

                        var gap1 = fim - inicio;
                        if (Number.isInteger(gap1)) {
                            gap1 = gap1 + ".00"
                        }

                        var intervalo = $('<td style="white-space: nowrap;">R$ ' + mascaraValor(inicio) + ' - R$ ' + mascaraValor(fim) + '</td>')
                        var percent = $('<td>' + faixa.valorPercentual + '%</td>')

                        if (temp > 0) {

                            if (temp >= gap1) {
                                valorCalc = (gap1 * (faixa.valorPercentual/100));
                                temp = temp - (gap1);
                                beneficioAtribuido = gap1
                                valorCalc = Math.round(valorCalc);
                            } else if (temp < gap1) {
                                valorCalc = (temp * (faixa.valorPercentual/100));
                                beneficioAtribuido = temp;
                                temp = temp - (gap1);
                            }

                        } else {
                            valorCalc = 0;
                        }

                        var beneficio = $('<td> R$ ' + mascaraValor(parseFloat(beneficioAtribuido).toFixed(2)) + '</td>')
                        var honorarioCalc = $('<td class="valorHon"> R$ ' + mascaraValor(parseFloat(valorCalc).toFixed(2))+ '</td>')

                        linhaFaixa.append(intervalo)
                        linhaFaixa.append(beneficio)
                        linhaFaixa.append(percent)
                        linhaFaixa.append(honorarioCalc)

                        $('#tbody-escalonado').append(linhaFaixa);

                    }

                    if (temp > 0) {

                        var percentualAcima = honorario.percentAcimaDe;
                        var honorarioAcimaDe = parseFloat((percentualAcima / 100) * temp).toFixed(2);

                        var vlAcimaDe = honorario.acimaDe
                        if (Number.isInteger(vlAcimaDe)) {
                            vlAcimaDe = vlAcimaDe + ".00"
                        } else {
							vlAcimaDe = Number(Math.floor(vlAcimaDe, -1).toFixed(2))
							vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
						}

                        var limitacao = honorario.limitacao
                        if (Number.isInteger(limitacao)) {
                            limitacao = limitacao + ".00"
                        } else {
							limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
							limitacao = parseFloat(limitacao).toFixed(2)
						}

                        var linhaAcimaDe = $("<tr></tr>");

                        var acimaDe = $('<td>Acima de: R$ ' + mascaraValor(vlAcimaDe) + ' (Limitação: R$ ' + mascaraValor(limitacao) + ')</td>');
                        var percentAcima = $('<td>' + percentualAcima + '%</td>');
                        var beneficioAcima = $('<td> R$ ' + mascaraValor(parseFloat(temp).toFixed(2)) + '</td>');

                        var valorAcimaDe = $('<td class="valorHon"> R$ ' + mascaraValor(honorarioAcimaDe) + '</td>');

                        linhaAcimaDe.append(acimaDe)
                        linhaAcimaDe.append(beneficioAcima)
                        linhaAcimaDe.append(percentAcima)
                        linhaAcimaDe.append(valorAcimaDe)

						$('#tbody-escalonado').append(linhaAcimaDe);
						
						 }

                           $.each($('.valorHon'), function () {
                                var valorLinha = parseFloat($(this).text().replace("R", "").replace("$", ".").replace(/\./g, '').replace(",", ".")).toFixed(2);
                                console.log(valorLinha)
                                total = Number(parseFloat(total).toFixed(2)) + Number(parseFloat(valorLinha).toFixed(2))
                                console.log("total",total)
                            })
                            if (Number.isInteger(total)) {
                                total = total + ".00"
                            }

						var trTotal = $("<tr></tr>")
						var vazio1 = $("<td> </td>")
						var vazio2 = $("<td> </td>")
						var vazio3 = $("<td> </td>")
						
						trTotal.append(vazio1)
						trTotal.append(vazio2)
						trTotal.append(vazio3)
						
						if(total>limitacao){
							var resultadoSomaLimit = $("<td><b>Total R$"+mascaraValor(total)+" (Limitado R$ "+mascaraValor(limitacao)+")</b></td>")
							trTotal.append(resultadoSomaLimit)
							console.log("total bibs 123456",resultadoSomaLimit)
							 $("#valorTotal").val(mascaraValor(limitacao))
                        	 $("#valorTotal").focusin();
						} else {
							var resultadoSomaNoLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+"</b></td>")	
							trTotal.append(resultadoSomaNoLimit)
							console.log("total bibs 123456",total)
							$("#valorTotal").val(mascaraValor(total))
                        	$("#valorTotal").focusin();
						}
						
						$('#tbody-escalonado').append(trTotal);

                       
                }
            }
        }
    })
}


function simularConsolidacaoFaturamento(idFaturamento ){
	
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
	ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
	
	console.log("fatsTeste",fats);
	
	url = "/faturamentos/simularConsolidacaoFaturamento/";
	console.log("teste antes ajax");
	
    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
			console.log("entrou no ajax");
            console.log(response)
			
			console.log("teste cnpj",response.faturamentoConsolidadoByFat.cliente.cnpj)
			console.log("teste razaoSocialFat",response.faturamentoConsolidadoByFat.cliente.razaoSocial)
	
		$('#fatConsolidado').show();
		
		$("#cnpjConsolidado").val(response.faturamentoConsolidadoByFat.cliente.cnpj)
        $("#cnpjConsolidado").mask('00.000.000/0000-00', {
                    reverse: true
                });
       $("#cnpjConsolidado").focusin();
       $("#idClienteConsolidado").val(response.faturamentoConsolidadoByFat.cliente.id)
	   $("#razaoSocialConsolidado").val(response.faturamentoConsolidadoByFat.cliente.razaoSocial)
       $("#razaoSocialConsolidado").focusin();
       $("#idProdutoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.id)
       $("#produtoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.nome)
       $("#produtoConsolidado").focusin();
  	   $("#campanhaConsolidado").val(response.faturamentoConsolidadoByFat.producao.ano)
       $("#campanhaConsolidado").focusin();
       
       $("#idProducaoConsolidado").val(response.faturamentoConsolidadoByFat.producao.id)
       $("#idsFatsConsolidado").val(response.faturamentoConsolidadoByFat.idsFaturamentosConsolidados)
       
      var divInfomacaoConsolidacao = $("<div'></div>")

 	  var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
                    + '</tr>'
                    + '</thead>')
                    
        var tableBodyFatConsolidados = $('<tbody id="tbody-informacoesFatConsolidados"></tbody>')
        
		for (var i = 0; i < response.fatsConsolidadosIds.length; i++) {
                    var fatConsolidado = response.fatsConsolidadosIds[i];
					console.log("fatConsolidado",fatConsolidado)
                    var valorEtapa = fatConsolidado.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatConsolidado.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatConsolidado.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatConsolidado.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatConsolidado.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatConsolidado.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatConsolidado.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(valorEtapa) + '</td>')

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)

                    tableBodyFatConsolidados.append(tr)
                    
                   
                   

                }
               
                 var trTotal = $('<tr></tr>')

                var valorEtapaConsolidado = response.faturamentoConsolidadoByFat.valorEtapa
				console.log("valorEtapaconsolidado",valorEtapaConsolidado)
				
                if (Number.isInteger(valorEtapaConsolidado)) {
                    valorEtapaConsolidado = valorEtapaConsolidado + ".00"
                }
                
                $("#valorTotalConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorTotalConsolidado").focusin();
       			
       				
       			$("#valorEtapaConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorEtapaConsolidado").focusin();
       			
                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                
                
                var total = $('<td><b>Total: R$' + mascaraValor(valorEtapaConsolidado) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)
                trTotal.append(total)

                tableBodyFatConsolidados.append(trTotal)
                
                
			table.append(tableBodyFatConsolidados)
            divInfomacaoConsolidacao.append(table)
  			$("#corpoFatConsolidados").append(divInfomacaoConsolidacao)

		}
	})
   
   }
	})
			
}


function parametrosFiltro(){
	//guarda a url(setItem) e armazena no input ("#currentParametros") 
	//para ser usada no botão voltar das págs details e update (para voltar a list sem perder os filtros)
	
	var url = window.location.href;
	$("#currentParametros").val(url);
	
	var params = $("#currentParametros").val();
	sessionStorage.setItem("params", params);
	
}

$(function formataCnpj() {
	console.log("entrou na func")
	$.each($('.cnpjClass'), function () {
		
		var cnpjAtual = $(this).text().replaceAll();
		cnpjAtual = cnpjAtual.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");
		console.log(cnpjAtual, " cnpjAtual")
})
})

function botaoVoltar(){
	//botão de voltar sem perder os filtros da página anterior
	// na páginda list.html  foi armazenado o valor da url
	//pega url armazenada(getItem),coloca o valor em um input(#currentParametros) e retorna para ela.
	
	 $("#currentParametros").val(sessionStorage.getItem("params"));
	 window.location.href =   $("#currentParametros").val();
	
	 if( $("#currentParametros").val()  == ""  ){
		 window.location.href = "/faturamentos/all";
	}  
}

$(function carregaCobranca() {
	console.log("entrou na carrega cobrança")
	$.each($('.tdDataCobrar'), function () {
		var fatId = $(this).text()
		
		var url = "/faturamentos/" + fatId + "/buscaDataACobrar.json"

		return $.ajax({
			type : 'GET',
			url : url,
			async : false,
			success : function(data) {
				$(this).text("wasko of gamar")
				console.log($(this).text())
	}})
})
})

//caso o faturamento seja manual e etapa faturar = int então,havera limitação de informações na tela
function faturamentoManual(){
	var fatId = $("#id").val();
	var url = "/faturamentos/" + fatId + "/fat.json"
	
	return $.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			if(data.etapa.id == 156){
				console.log("é faturamento manual")
				$("#containerFatManual").hide();
			}
		}
		})
}




