let primeiroLoad = 1;
$(function() {
	var gif = document.querySelector("div[class*='preloader-wrapper big active loader']");
	gif.style.display = "none"
	
	$('.collapsible').collapsible('open', 0);
	$(".modal").modal();
	$(".select").material_select();
	$('#select-etapa').attr("disabled", 'disabled');
	$('#select-etapa').material_select();
});

$('#selectProduto').change(
		function() {

			var prodId = $('#selectProduto :selected').val();
			console.log(prodId)

			if (prodId == "") {

				$('#select-etapa').attr("readonly", 'readonly');

				$('#select-etapa').find('option').remove().end().append(
						'<option value="" selected="selected">Selecione</option>').val(
						'whatever');

				$('#select-etapa').material_select();

			} else {

				var url = "/produtos/" + prodId + "/etapas.json"

				var options = "<option selected='selected' value=''>Selecione</option>";

				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {

						$(data).each(
								function(i) {
									var etapa = data[i];
									options += "<option value='" + etapa.id
											+ "'>" + etapa.nome + "</option>"
								});

						$("#select-etapa").empty();
						$("#select-etapa").append(options);
						$('#select-etapa').removeAttr('disabled');
						$("#select-etapa").material_select();

					}
				});
			}
		});

$(function filialToSigla() {
	$.each($('.filialClass'), function () {
		var filialAtual = $(this).text();

		switch (filialAtual) {
			case 'RIO DE JANEIRO':
			$(this).text('RJ')
			break;
			
			case 'CURITIBA':
			$(this).text('PR')
			break;
			
			case 'SÃO PAULO':
			$(this).text('SP')
			break;
			
			case 'BRASÍLIA':
			$(this).text('DF')
			break;
			
			case 'BELO HORIZONTE':
			$(this).text('MG')
			break;
			
			case 'PERNAMBUCO':
			$(this).text('RE')
			break;
			
			case 'PORTO ALEGRE':
			$(this).text('RS')
			break;
			
			case 'FLORIANÓPOLIS':
			$(this).text('SC')
			break;
			
			case 'MANAUS':
			$(this).text('AM')
			break;
			
			default:
			$(this).text('')
			break;
		}
	})
});		

$(document).on("click",".buscar",function() {
	console.log('entrou no método de buscar')
	var gif = document.querySelector("div[class*='preloader-wrapper big active loader']");
	gif.style.display = "block"
	
	//totais
	var totalSimQuantificacao = 0;
	var totalNaoQuantificacao = 0;
	var totalSimSubsmissao = 0;
	var totalNaoSubmissao = 0;
	var totalSimFatQuantificacao = 0;
	var totalSimFatSubmissao = 0;
	var countEmpresasSubmissao = 0;
	var countEmpresasQuantificacao = 0;
	
	//get filtros 
	var ano = ($('#select-campanha') != null ? $('#select-campanha').val() : "")
	var razaoSocial = $('#txt-pesq-empresa').val()
	var produto = $('#selectProduto').val()
	var etapa = $('#select-etapa').val()
	var filial = $('#select-filial').val()
	var tipoNegocio = $('#tipoNegocio').val()
	var submissao = $('#submissao').val()
	var quantificacao = $('#quantificacao').val()
	
	var parametros = {
		ano: ano,
		razaoSocial: razaoSocial,
		produto:produto,
		etapa:etapa,
		filial:filial,
		tipoNegocio:tipoNegocio,
		submissao:submissao,
		quantificacao:quantificacao
		
	}
	
	console.log(parametros)
	
	 $.ajax({
     			type: "post",
     			url: "/relatorios/carrega.jsonObject",
     			data: JSON.stringify(parametros),
				headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
				},
				success : 
                    function(JSONObject) {
					//console.log(JSONObject)
					$("#corpo-tabela").empty()
					
					var count = 0
					JSONObject.forEach(function(){
						
					//contabilizar totalizadores
					totalSimFatQuantificacao += JSONObject[count].qtdQuantificacao;
					totalSimFatSubmissao += JSONObject[count].qtdSubmissao;
					JSONObject[count].submissao == 'Sim' ? totalSimSubsmissao++ : totalNaoSubmissao++
					if(JSONObject[count].qtdSubmissao > 0){
						countEmpresasSubmissao++;
					}
					if(JSONObject[count].qtdQuantificacao > 0){
						countEmpresasQuantificacao++
					}	
					
					try{
							var primeiroValorQuantificacao = JSONObject[count].quantificacao.substring(JSONObject[count].quantificacao.indexOf('/') + 1);
							} catch(e){
								var primeiroValorQuantificacao = 0
							}
							try{
							var segundoValorQuantificacao = JSONObject[count].quantificacao.split('/')[0];
							} catch(e){
							var segundoValorQuantificacao = 0
							}
							primeiroValorQuantificacao == segundoValorQuantificacao ? totalSimQuantificacao++ : totalNaoQuantificacao++
							if(primeiroValorQuantificacao == 0 && segundoValorQuantificacao == 0){
								totalSimQuantificacao--
								totalNaoQuantificacao++
							}
						
						 var trLinha = $('<tr> </tr>')
							  
							  var tdRazaoSocial = $(' <td > <a href="/producoes/update/'+ JSONObject[count].idProducao+'"> ' + JSONObject[count].razaoSocial + '</a></td>')
							  var tdCampanha = $('<td >' + JSONObject[count].campanha + '</td>')
							  var tdEquipe = $('<td >' + JSONObject[count].equipe + '</td>')
							  var tdVn = $('<td >' + 'R$ ' + mascaraValor(parseFloat(JSONObject[count].vn).toFixed(2)) + '</td>')
							  var tdApuracao = $('<td >' + JSONObject[count].apuracao + '</td>')
							  var tdModo = $('<td >' + JSONObject[count].modo + '</td>')
							  var tdQuantificacao = $('<td >' + JSONObject[count].quantificacao + '</td>')
							  var tdQtdQuantificacao = $('<td >' + JSONObject[count].qtdQuantificacao + '</td>')
							  var tdSubmissao = $('<td >' + JSONObject[count].submissao + '</td>')
							  var tdQtdSubmissao = $('<td >' + JSONObject[count].qtdSubmissao + '</td>')
							  var tdSubmissaoPercent = $('<td >' + JSONObject[count].submissaoPercent + ' %' + '</td>')
							  var tdQuantificacaoPercent = $('<td >' + JSONObject[count].quantificacaoPercent + ' %' + '</td>')
							  var tdGerouFaturamento = $('<td >' + JSONObject[count].gerouFaturamento + '</td>')
					
							  trLinha.append(tdRazaoSocial).append(tdCampanha).append(tdEquipe).append(tdApuracao).append(tdQuantificacao).append(tdSubmissao).append(tdQuantificacaoPercent).append(tdSubmissaoPercent)
							  .append(tdQtdQuantificacao).append(tdQtdSubmissao).append(tdVn)
							  .append(tdModo).append(tdGerouFaturamento)
							  
							  $("#corpo-tabela").append(trLinha)
							  
							  
							  
						count++;
					})
					
					//popular totalizadores
					$("#totalSimQuantificacao").text(totalSimQuantificacao)
					$("#totalNaoQuantificacao").text(totalNaoQuantificacao )
					$("#totalSimQuantificacaoPercent").text(((totalSimQuantificacao/JSONObject.length) * 100).toFixed(2) + '%')
					$("#totalNaoQuantificacaoPercent").text(((totalNaoQuantificacao /JSONObject.length) * 100).toFixed(2) + '%')
					
					$("#totalSimSubmissao").text(totalSimSubsmissao )
					$("#totalNaoSubmissao").text(totalNaoSubmissao  )
					$("#totalSimSubmissaoPercent").text(((totalSimSubsmissao /JSONObject.length) * 100).toFixed(2) + '%')
					$("#totalNaoSubmissaoPercent").text(((totalNaoSubmissao  /JSONObject.length) * 100).toFixed(2) + '%')
					
					$("#totalSimFatsQuanficacao").text('Empresas')
					$("#totalNaoFatsQuanficacaoPercent").text(totalSimFatQuantificacao)
					$("#totalNaoFatsQuanficacao").text('Faturamentos')
					$("#totalSimFatsQuanficacaoPercent").text(countEmpresasQuantificacao)
					
					
					$("#totalSimFatsSubmissao").text('Empresas')
					$("#totalNaoFatsSubmissaoPercent").text(totalSimFatSubmissao)
					$("#totalNaoFatsSubmissao").text('Faturamentos')
					$("#totalSimFatsSubmissaoPercent").text(countEmpresasSubmissao)
					
					if(primeiroLoad == 1){
						
						$('#tabela-faturamentos').DataTable({
								language: {
									"sEmptyTable": "Nenhum registro encontrado",
									"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
									"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
									"sInfoFiltered": "(Filtrados de _MAX_ registros)",
									"sInfoPostFix": "",
									"sInfoThousands": ".",
									"sLengthMenu": "_MENU_ resultados por página",
									"sLoadingRecords": "Carregando...",
									"sProcessing": "Processando...",
									"sZeroRecords": "Nenhum registro encontrado",
									"sSearch": "Pesquisar",
									"oPaginate": {
										"sNext": "Próximo",
										"sPrevious": "Anterior",
										"sFirst": "Primeiro",
										"sLast": "Último"
									},
									"oAria": {
										"sSortAscending": ": Ordenar colunas de forma ascendente",
										"sSortDescending": ": Ordenar colunas de forma descendente"
									},
									"select": {
										"rows": {
											"_": "Selecionado %d linhas",
											"0": "Nenhuma linha selecionada",
											"1": "Selecionado 1 linha"
										}
									}
								},
								bFilter: false,
								iDisplayLength: 5000,
								columnDefs: [
									{
										targets: [0, 1, 2],
										className: 'mdl-data-table__cell--non-numeric'
									}
								]
							});
							primeiroLoad++
							gif.style.display = "none"
						}
				}
	
	})
	
})

function mascaraValor(valor) {
	if(valor === undefined){
		
	}else{
		console.log(valor)
		valor = valor.toString().replace(/\D/g, "");
		valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
		valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
		valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	}
	return valor
}
