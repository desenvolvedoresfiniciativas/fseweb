$("#cnpj").blur(buscaEmpresa);
$('#btn').click(buscaFaturamentos)
$('#btnBuscaProducoes').click(buscaProducoes)

$("#btn-add-beneficioConsolidado").click(salvaBeneficioConsolidado)

$("#btn-add-faturamentoConsolidado").click(salvaFaturamentoConsolidado)

$(function () {
    formatarCnpj();
    $('.select').material_select();
    $('#beneficioConsolidado').hide();
    $('#fatConsolidado').hide();

	if($("#preLoaded").val()=="s"){
		preLoad();
	}	
})

function preLoad(){
	
		var idProducao = $("#idProdLoaded").val()
	
	    $.ajax({
        type: 'GET',
        url: "/producoes/" + idProducao + "/prod.json",
        async: false,
        success: function (prod) {
			
			$('#idcliente').val(prod.cliente.id)
			$('#cnpj').val(prod.cliente.cnpj)
			$('#razaoSocial').val(prod.cliente.razaoSocial)
			$('#selectProduto').val(prod.produto.nome)
			$('#selectProduto').text(prod.produto.nome)
			$('#select-ano').val(prod.ano)
			$('#select-ano').text(prod.ano)
			$('#selectProduto option:contains("'+prod.produto.nome+'")').attr('selected', true);
			$('#select-ano option:contains("'+prod.ano+'")').attr('selected', true);
		
			$("#corpoTabelaProducoes").append(
                    "<tr><td>" + prod.produto.nome + "</td>"
                    + "<td>" + prod.ano + "</td>"
                    + "<td>" + prod.tipoDeApuracao + "</td>"
                    + "<td class='check'><p><input name='prod' id='" + prod.id + "' type='radio' /><label for='" + prod.id + "'></label></p></td></tr > ")
		
	
	buscaFaturamentos()

	}
    })
	
}

function expand(){
  $(".fatsCollapsabler").addClass("active");
  $(".fatsCollapsable").collapsible({accordion: false});
}

function simularConsolidacao() {
			$('#corpoFats').text("")
			$("#divCompleta").empty()
			$("#tbody-escalonado").empty()
    var url = 'simulaFatConsolidado';

    var fats = []

    $("input:checkbox[name='faturamentos']:checked").each(function () {
        var fat = {
            id: $(this).attr('id')
        }
        fats.push(fat)
    })

    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
            console.log(response)
            if (response.erro == "noBalancete") {
                Materialize.toast('Um ou mais faturamentos não possuem balancetes vinculados, não é possível consolidar o benefício', 5000, 'rounded');
            } else if (response.erro == "mesmoBalancete") {
                Materialize.toast('Dois ou mais faturamentos possuem a mesma versão de balancete, não é possível consolidar o benefício.', 5000, 'rounded');
            } else if (response.erro == "mesmaEtapa") {
                Materialize.toast('Dois ou mais faturamentos não possuem a mesma etapa de faturamento, não é possível consolidar o benefício.', 5000, 'rounded');
            } else {
                $('#beneficioConsolidado').show();

				console.log(response)

                //CARREGA VALORES
                $("#cnpjFat").val(response.faturamentoConsolidado.cliente.cnpj)
                $("#cnpjFat").mask('00.000.000/0000-00', {
                    reverse: true
                });
                $("#cnpjFat").focusin();

                $("#razaoSocialFat").val(response.faturamentoConsolidado.cliente.razaoSocial)
                $("#razaoSocialFat").focusin();
                $("#idCliente").val(response.faturamentoConsolidado.cliente.id)

                $("#idProduto").val(response.faturamentoConsolidado.producao.produto.id)
                $("#produto").val(response.faturamentoConsolidado.producao.produto.nome)
                $("#produto").focusin();

				$("#idProducao").val(response.faturamentoConsolidado.producao.id)

				$("#idEtapa").val(response.faturamentoConsolidado.etapa.id)
                $("#etapa").val(response.faturamentoConsolidado.etapa.nome)
                $("#etapa").focusin();

				$('#observacao').val(response.faturamentoConsolidado.observacao)
				$('#observacao').focusin();

				$("#idsFatsConsolidados").val(response.faturamentoConsolidado.idsFaturamentosConsolidados)

                $("#percentEtapa").val(response.percent)
                $("#percentEtapa").focusin();

                $("#campanha").val(response.faturamentoConsolidado.producao.ano)
                $("#campanha").focusin();

                var valor = response.faturamentoConsolidado.valorEtapa

                if (Number.isInteger(valor)) {
                    valor = valor + ".00"
                }

                $("#valorEtapa").val(mascaraValor(parseFloat(valor).toFixed(2)))
                $("#valorEtapa").focusin();

                var divFats = $("#fatsConsolidados")


                var divPai = $("<div'></div>")

                var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
                    + '</tr>'
                    + '</thead>')
                var tableBody = $('<tbody id="tbody-informacoes"></tbody>')

                for (var i = 0; i < response.fatsIds.length; i++) {
                    var fatC = response.fatsIds[i];

                    var valorEtapa = fatC.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatC.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatC.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatC.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatC.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatC.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatC.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(valorEtapa) + '</td>')

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)

                    tableBody.append(tr)

                }

                var trTotal = $('<tr></tr>')

                var beneficioTotal = response.beneficioConsolidado

                if (Number.isInteger(beneficioTotal)) {
                    beneficioTotal = beneficioTotal + ".00"
                }

                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                var total = $('<td><b>Total: R$' + mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(total)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)

                tableBody.append(trTotal)

                table.append(tableBody)
                divPai.append(table)

                $("#corpoFats").append(divPai)

                var idContrato = response.faturamentoConsolidado.contrato.id

                getMemoriaDeCalculo(idContrato, beneficioTotal);

            }
        },
        error: function (xhr) {

        }
    });

}

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g, "");
    valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
    return valor
}

function getMemoriaDeCalculo(idContrato, beneficioTotal) {

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idContrato + "/honorarios.json",
        async: false,
        success: function (hon) {

            for (var i = 0; i < hon.length; i++) {
                var honorario = hon[i];
                console.log(honorario)

                if (honorario.nome == "Percentual Fixo") {

                    $('#percentFixo').show()

                    var trFixo = $("<tr></tr>");

                    var beneficioFixo = $('<td> R$ ' + mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</td>')
                    var percentualFixo = $('<td>' + honorario.valorPercentual + '%</td>')

                    var percent = (honorario.valorPercentual / 100)
                    var resultado = (beneficioTotal * percent) * 100

                    var valor = $('<td class="resultadoPF">R$ ' + mascaraValor(parseFloat(resultado).toFixed(2)) + '</td>')

                    trFixo.append(beneficioFixo)
                    trFixo.append(percentualFixo)
                    trFixo.append(valor)

                    $('#tbody-percentualFixo').append(trFixo);

                }
                if (honorario.nome == "Percentual Escalonado") {
                    $('#escalonado').show()

                    var temp = beneficioTotal;
                    var valorCalc = 0.00;
                    var total = 0.00;

                    var faixas = honorario.faixasEscalonamento

                    for (var i = 0; i < faixas.length; i++) {

                        var faixa = faixas[i];

                        var linhaFaixa = $("<tr></tr>");

                        var inicio = faixa.valorInicial;
                        var fim = faixa.valorFinal;
                        var beneficioAtribuido = 0.00;

                        if (Number.isInteger(inicio)) {
                            inicio = inicio + ".00"
                        } else {
							inicio = Number(Math.floor(inicio, -1).toFixed(2))
							inicio = parseFloat(inicio).toFixed(2)
						}
						
                        if (Number.isInteger(fim)) {
                            fim = fim + ".00"
                        } else {
							fim = Number(Math.floor(fim, -1).toFixed(2))
							fim = parseFloat(fim).toFixed(2)
						}

                        var gap1 = fim - inicio;
                        if (Number.isInteger(gap1)) {
                            gap1 = gap1 + ".00"
                        }

                        var intervalo = $('<td style="white-space: nowrap;">R$ ' + mascaraValor(inicio) + ' - R$ ' + mascaraValor(fim) + '</td>')
                        var percent = $('<td>' + faixa.valorPercentual + '%</td>')

                        if (temp > 0) {

                            if (temp >= gap1) {
                                valorCalc = parseFloat(gap1).toFixed(2) * (parseFloat(faixa.valorPercentual).toFixed(2)/100);
                                temp = parseFloat(temp).toFixed(2) - parseFloat(gap1).toFixed(2);
                                beneficioAtribuido = parseFloat(gap1).toFixed(2)
                                valorCalc = Math.round(valorCalc);
                            } else if (temp < gap1) {
                                valorCalc = parseFloat(temp).toFixed(2) * (parseFloat(faixa.valorPercentual).toFixed(2)/100);
                                beneficioAtribuido = temp;
                                temp = parseFloat(temp).toFixed(2) - parseFloat(gap1).toFixed(2);
                            }

                        } else {
                            valorCalc = 0;
                        }

                        var beneficio = $('<td> R$ ' + mascaraValor(parseFloat(beneficioAtribuido).toFixed(2)) + '</td>')
                        var honorarioCalc = $('<td class="valorHon"> R$ ' + mascaraValor(parseFloat(valorCalc).toFixed(2)) + '</td>')

                        linhaFaixa.append(intervalo)
                        linhaFaixa.append(beneficio)
                        linhaFaixa.append(percent)
                        linhaFaixa.append(honorarioCalc)

                        $('#tbody-escalonado').append(linhaFaixa);

                    }

                    if (temp > 0) {

                        var percentualAcima = honorario.percentAcimaDe;
                        var honorarioAcimaDe = parseFloat((percentualAcima / 100) * temp).toFixed(2);

                        var vlAcimaDe = honorario.acimaDe
                        if (Number.isInteger(vlAcimaDe)) {
                            vlAcimaDe = vlAcimaDe + ".00"
                        } else {
							vlAcimaDe = Number(Math.floor(vlAcimaDe, -1).toFixed(2))
							vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
						}

                        var limitacao = honorario.limitacao
                        if (Number.isInteger(limitacao)) {
                            limitacao = limitacao + ".00"
                        } else {
							limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
							limitacao = parseFloat(limitacao).toFixed(2)
						}

                        var linhaAcimaDe = $("<tr></tr>");

                        var acimaDe = $('<td>Acima de: R$ ' + mascaraValor(vlAcimaDe.toFixed(2)) + ' (Limitação: R$ ' + mascaraValor(limitacao) + ')</td>');
                        var percentAcima = $('<td>' + percentualAcima + '%</td>');
                        var beneficioAcima = $('<td> R$ ' + mascaraValor(parseFloat(temp).toFixed(2)) + '</td>');

                        var valorAcimaDe = $('<td class="valorHon"> R$ ' + mascaraValor(parseFloat(honorarioAcimaDe).toFixed(2)) + '</td>');

                        linhaAcimaDe.append(acimaDe)
                        linhaAcimaDe.append(beneficioAcima)
                        linhaAcimaDe.append(percentAcima)
                        linhaAcimaDe.append(valorAcimaDe)

						$('#tbody-escalonado').append(linhaAcimaDe);
						
						 }

                           $.each($('.valorHon'), function () {
                                var valorLinha = parseFloat($(this).text().replace("R", "").replace("$", ".").replace(/\./g, '').replace(",", ".")).toFixed(2);
                                console.log(valorLinha)
                                total = Number(parseFloat(total).toFixed(2)) + Number(parseFloat(valorLinha).toFixed(2))
                                console.log(total)
                            })
                            if (Number.isInteger(total)) {
                                total = total + ".00"
                            }

						var trTotal = $("<tr></tr>")
						var vazio1 = $("<td> </td>")
						var vazio2 = $("<td> </td>")
						var vazio3 = $("<td> </td>")
						
						trTotal.append(vazio1)
						trTotal.append(vazio2)
						trTotal.append(vazio3)
						
						if(total>limitacao){
							var resultadoSomaLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+" (Limitado R$ "+mascaraValor(limitacao)+")</b></td>")
							trTotal.append(resultadoSomaLimit)
							 $("#valorTotal").val(mascaraValor(limitacao))
                        	 $("#valorTotal").focusin();
						} else {
							var resultadoSomaNoLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+"</b></td>")	
							trTotal.append(resultadoSomaNoLimit)
							$("#valorTotal").val(mascaraValor(parseFloat(total).toFixed(2)))
                        	$("#valorTotal").focusin();
						}
						
						$('#tbody-escalonado').append(trTotal);

                       
                }
            }
        }
    })
}

function buscaProducoes() {
    emptyTable();

    var cnpj = $('#cnpj').val().replace(/\D/g, "");
    var produto = $('#selectProduto').find(":selected").val();
    var ano = $('#select-ano').find(":selected").val();

    var url = '/producoes/' + cnpj + '/' + produto + '/' + ano + '/producoes.json';
    $.get(url, function (data) {

        if (data.length == 0) {
            Materialize.toast('Nenhuma produção encontrada.', 5000, 'rounded');
        } else {

            $(data).each(function () {

                var producao = this;

                $("#corpoTabelaProducoes").append(
                    "<tr><td>" + producao.produto.nome + "</td>"
                    + "<td>" + producao.ano + "</td>"
                    + "<td>" + producao.tipoDeApuracao + "</td>"
                    + "<td class='check'><p><input name='producao' id='" + producao.id + "' type='radio' /><label for='" + producao.id + "'></label></p></td></tr > ")

            });
        }
    });
}


function emptyTable() {
    $('#tabelaProducao tbody').empty();
}

function buscaFaturamentos() {
	
	if($("#preLoaded").val()=="n"){
    var idProd = $("input:radio[name ='producao']:checked").attr("id");
	} else {
	var idProd = $("#idProdLoaded").val()
	}

    var url = idProd + '/fatsProd.json';
    $.get(url, function (data) {

        if (data.length == 0) {
            Materialize.toast('Nenhum faturamento encontrato.', 5000, 'rounded');
        } else {
		
   		$('#tabela > tbody').empty();
   		
   		

            $(data).each(function () {

                var fat = this;

                var balancete;

                if (fat.balancete == null || fat.balancete == undefined) {
                    balancete = "Sem versão";
                } else {
                    balancete = fat.balancete.versao
                }
                
                console.log('o faturamento é: ' + fat)
                console.log('o tipo de apuração é: ' + fat.producao.tipoDeApuracao)

                $("#corpoTabela").append(
                    "<tr>"
                    + "<td>" + fat.produto.nome + "</td>"
                    + "<td>" + fat.producao.ano + "</td>"
                    + "<td>" + fat.producao.tipoDeApuracao + "</td>"
                    + "<td>" + balancete + "</td>"
                    + "<td>" + fat.etapa.nome + "</td>"
                    + "<td>" + fat.estado + "</td>"
                    + "<td><p>" + maskValor(fat.valorEtapa) + "</p></td>"
                    + "<td class='check center'><p><input name='faturamentos' id='" + fat.id + "' type='checkbox'/><label for='" + fat.id + "'></label></p></td>"
                    + "</tr>")

            });
         console.log('o id da produção é' + idProd)   
        var url = "/producoes/getBalancetesPrejuizoByProd/"+idProd+"/.json"
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: url,
            dataType: "JSON",
            success : function(listaBalancete){
			console.log('entrou no método que lista balancetes no prejuízo')
				$(listaBalancete).each(function () {
					console.log('entrou no método que lista balancetes no prejuízo')
					balancete = this;
					 $("#corpoTabela").append(
                    "<tr>"
                    + "<td>" + balancete.producao.produto.nome + "</td>"
                    + "<td>" + balancete.producao.ano + "</td>"
                    + "<td>" + balancete.producao.tipoDeApuracao + "</td>"
                    + "<td>" + balancete.versao + "</td>"
                    + "<td>" + '' + "</td>"
                    + "<td>" + '' + "</td>"
                    + "<td><p>" + 'R$ 0,00' + "</p></td>"
                    + "<td>" +'PREJUIZO' + "</td>"
                    + "</tr>")
					
				})	
			}
        });
        }
    });

    maskValor();

}

function maskValor(valor) {

    var texto = valor;
    texto = parseFloat(texto).toLocaleString('pt-BR')
    texto = "R$ " + texto
    return texto;
}

function formatarCnpj() {
    $("#cnpj").mask('00.000.000/0000-00', {
        reverse: true
    });

}

function limpaTabela() {
    $("#tabela > tbody").empty();
}

function buscaEmpresa() {

    var cnpj = $(this).val().replace(/\D/g, "");

    if (!$(this).val().length == 0) {

        $.getJSON("/clientes/" + cnpj + ".json", function (dados) {

            if (dados.erro != true) {

                $("#razaoSocial").val(dados.razaoSocial);
                $("#razaoSocial").focusin();
                $("#idcliente").val(dados.id);

            } else {
                Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
                limparNomeEmpresa();
            }
        })

    }

}

function salvaBeneficioConsolidado(){
	
	var cliente = {
		id: $('#idCliente').val()
	}
	
	var etapa = {
		id: $("#idEtapa").val()
	}
	
	var producao = {
		id: $("#idProducao").val()
	}
	
	var produto = {
		id: $("#idProduto").val()
	}
	
	var fat = {
		anoFiscal: $("#anoFiscal").val(),
		balancete: null,
		campanha: $("#campanha").val(),
		cliente: cliente, 
		cnpjFaturar: null,
		razaoSocialFaturar: null,
		consolidado: true,
		contrato: null,
		data: null,
		dataCriacao: null,
		emitido: false,
		estado: $('#select-estado').find(":selected").val(),
		etapa: etapa,
		etapaTrabalho: null,
		idsFaturamentosConsolidados: $("#idsFatsConsolidados").val(),
		liberado: false,
		manual: false,
		numeroNF: null,
		obsContrato: null,
		observacao: $("#observacao").val(),
		percentualE: null,
		percentualF: null,
		percetEtapa: $('#percentEtapa').val(),
		producao: producao,
		produto: produto,
		subProducao: null,
		valorEtapa: $("#valorEtapa").val().replace(/\./g, '').replace(",","."),
		valorF: null,
		valorTotal: $("#valorTotal").val().replace(/\./g, '').replace(",","."),
	};
	
	console.log(fat)
	
		$.ajax({
					type: "POST",
					contentType: "application/json",
					url: "/faturamentos/salvaBeneficioConsolidado",
					data: JSON.stringify(fat),
					dataType: 'JSON',
					statusCode: {
						200: function() {
							Materialize.toast(
								'Faturamento consolidado criado com sucesso!',
								5000, 'rounded');
								$("#btn-add-beneficioConsolidado").attr("disabled", true)
								Location.reload()
						},
						500: function() {
							Materialize.toast(
								'Ops, houve um erro interno',
								5000, 'rounded');
						},
						400: function() {
							Materialize.toast(
									'Você deve estar fazendo algo de errado',
									5000, 'rounded');
						},
						404: function() {
							Materialize.toast('Url não encontrada',
								5000, 'rounded');
						}
					}
				});
	
}

$('#btnSimular').click(
	function (){
		if (document.querySelector('#balanceteConsolidado').checked == true) {
		console.log("balConsolidado checked");
		simularConsolidacao();
		}
	
	if (document.querySelector('#faturamentoConsolidado').checked == true) {
		console.log("faturamentoConsolidado checked");
		simularConsolidacaoFaturamento()
	
	}
})


function simularConsolidacaoFaturamento() {
	$("#corpoFatConsolidados").empty()
	   
	   var fats = []
	   
	   $("input:checkbox[name='faturamentos']:checked").each(function () {
        var fat = {
            id: $(this).attr('id')
        }
        fats.push(fat)
    })

	url = "/faturamentos/simularConsolidacaoFaturamento/";
	
    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
			console.log("entrou no ajax");
            console.log(response)
			
			console.log("teste cnpj",response.faturamentoConsolidadoByFat.cliente.cnpj)
			console.log("teste razaoSocialFat",response.faturamentoConsolidadoByFat.cliente.razaoSocial)
	
		$('#fatConsolidado').show();
		
		$("#cnpjConsolidado").val(response.faturamentoConsolidadoByFat.cliente.cnpj)
        $("#cnpjConsolidado").mask('00.000.000/0000-00', {
                    reverse: true
                });
       $("#cnpjConsolidado").focusin();
       $("#idClienteConsolidado").val(response.faturamentoConsolidadoByFat.cliente.id)
	   $("#razaoSocialConsolidado").val(response.faturamentoConsolidadoByFat.cliente.razaoSocial)
       $("#razaoSocialConsolidado").focusin();
       $("#idProdutoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.id)
       $("#produtoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.nome)
       $("#produtoConsolidado").focusin();
  	   $("#campanhaConsolidado").val(response.faturamentoConsolidadoByFat.producao.ano)
       $("#campanhaConsolidado").focusin();
       
       $("#idProducaoConsolidado").val(response.faturamentoConsolidadoByFat.producao.id)
       $("#idsFatsConsolidado").val(response.faturamentoConsolidadoByFat.idsFaturamentosConsolidados)
       
      var divInfomacaoConsolidacao = $("<div'></div>")

 	  var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
                    + '</tr>'
                    + '</thead>')
                    
        var tableBodyFatConsolidados = $('<tbody id="tbody-informacoesFatConsolidados"></tbody>')
        
		for (var i = 0; i < response.fatsConsolidadosIds.length; i++) {
                    var fatConsolidado = response.fatsConsolidadosIds[i];
					console.log("fatConsolidado",fatConsolidado)
                    var valorEtapa = fatConsolidado.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatConsolidado.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatConsolidado.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatConsolidado.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatConsolidado.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatConsolidado.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatConsolidado.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(valorEtapa) + '</td>')

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)

                    tableBodyFatConsolidados.append(tr)
                    
                }
               
                 var trTotal = $('<tr></tr>')

                var valorEtapaConsolidado = response.faturamentoConsolidadoByFat.valorEtapa
				console.log("valorEtapaconsolidado",valorEtapaConsolidado)
				
                if (Number.isInteger(valorEtapaConsolidado)) {
                    valorEtapaConsolidado = valorEtapaConsolidado + ".00"
                }
                
                $("#valorTotalConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorTotalConsolidado").focusin();
       			
       				
       			$("#valorEtapaConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorEtapaConsolidado").focusin();
       			
                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                
                
                var total = $('<td><b>Total: R$' + mascaraValor(valorEtapaConsolidado) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)
                trTotal.append(total)

                tableBodyFatConsolidados.append(trTotal)
                
                
			table.append(tableBodyFatConsolidados)
            divInfomacaoConsolidacao.append(table)
  			$("#corpoFatConsolidados").append(divInfomacaoConsolidacao)

       
       
       
		}
	})
   
   }
   
   
   
   function salvaFaturamentoConsolidado(){
	
	var cliente = {
		id: $('#idClienteConsolidado').val()
	}
	
	/*var etapa = {
		id: $("#idEtapaConsolidado").val()
	}*/
	
	var producao = {
		id: $("#idProducaoConsolidado").val()
	}
	
	var produto = {
		id: $("#idProdutoConsolidado").val()
	}
	
	var fatConsolidado = {
		anoFiscal: $("#anoFiscalConsolidado").val(),
		balancete: null,
		campanha: $("#campanhaConsolidado").val(),
		cliente: cliente, 
		cnpjFaturar: null,
		razaoSocialFaturar: null,
		consolidado: true,
		contrato: null,
		data: null,
		dataCriacao: null,
		emitido: false,
		estado: $('#select-estadoConsolidado').find(":selected").val(),
		etapa: null,
		etapaTrabalho: null,
		idsFaturamentosConsolidados: $("#idsFatsConsolidado").val(),
		liberado: false,
		manual: false,
		numeroNF: null,
		obsContrato: null,
		observacao: $("#observacaoConsolidado").val(),
		percentualE: null,
		percentualF: null,
		percetEtapa: null,
		producao: producao,
		produto: produto,
		subProducao: null,
		valorEtapa: $("#valorTotalConsolidado").val().replace(/\./g, '').replace(",","."),
		valorF: null,
		valorTotal: $("#valorTotalConsolidado").val().replace(/\./g, '').replace(",","."),
	};
	
	console.log("salvaFaturamentoConsolidado fatConsolidado",fatConsolidado);
	///console.log("salvaFaturamentoConsolidado etapa",etapa);
	
		$.ajax({
					type: "POST",
					contentType: "application/json",
					url: "/faturamentos/salvaFaturamentoConsolidado",
					data: JSON.stringify(fatConsolidado),
					dataType: 'JSON',
					statusCode: {
						200: function() {
							Materialize
								.toast(
									'Faturamento consolidado criado com sucesso!',
									5000, 'rounded');
									$("#btn-add-faturamentoConsolidado").attr("disabled", true)
									Location.reload()
						},
						500: function() {
							Materialize.toast(
								'Ops, houve um erro interno',
								5000, 'rounded');
						},
						400: function() {
							Materialize
								.toast(
									'Você deve estar fazendo algo de errado',
									5000, 'rounded');
						},
						404: function() {
							Materialize.toast('Url não encontrada',
								5000, 'rounded');
						}
					}
				});
	
}

    

 



