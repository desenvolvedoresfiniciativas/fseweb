$(function() {

consolidado();

});

function simularConsolidacao() {
	
	idFaturamento = $("#id").val();
	
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
					ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
	
    var url = "/faturamentos/simulaFatConsolidado"
    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
            if (response.erro == "noBalancete") {
                Materialize.toast('Um ou mais faturamentos não possuem balancetes vinculados, não é possível consolidar o benefício', 5000, 'rounded');
            } else if (response.erro == "mesmoBalancete") {
                Materialize.toast('Dois ou mais faturamentos possuem a mesma versão de balancete, não é possível consolidar o benefício.', 5000, 'rounded');
            } else if (response.erro == "mesmaEtapa") {
                Materialize.toast('Dois ou mais faturamentos não possuem a mesma etapa de faturamento, não é possível consolidar o benefício.', 5000, 'rounded');
            } else {
				//teste
                $('#beneficioConsolidado').show();

                //CARREGA VALORES
                $("#cnpjFat").val(response.faturamentoConsolidado.cliente.cnpj)
                $("#cnpjFat").mask('00.000.000/0000-00', {
                    reverse: true
                });
                $("#cnpjFat").focusin();

                $("#razaoSocialFat").val(response.faturamentoConsolidado.cliente.razaoSocial)
                $("#razaoSocialFat").focusin();
                $("#idCliente").val(response.faturamentoConsolidado.cliente.id)

                $("#idProduto").val(response.faturamentoConsolidado.producao.produto.id)
                $("#produto").val(response.faturamentoConsolidado.producao.produto.nome)
                $("#produto").focusin();

				$("#idProducao").val(response.faturamentoConsolidado.producao.id)

				$("#idEtapa").val(response.faturamentoConsolidado.etapa.id)
                $("#etapa").val(response.faturamentoConsolidado.etapa.nome)
                $("#etapa").focusin();

				$('#observacao').val(response.faturamentoConsolidado.observacao)
				$('#observacao').focusin();

				$("#idsFatsConsolidados").val(response.faturamentoConsolidado.idsFaturamentosConsolidados)

                $("#percentEtapa").val(response.percent)
                $("#percentEtapa").focusin();

                $("#campanha").val(response.faturamentoConsolidado.producao.ano)
                $("#campanha").focusin();

                var valor = response.faturamentoConsolidado.valorEtapa

                if (Number.isInteger(valor)) {
                    valor = valor + ".00"
                }

                $("#valorEtapa").val(mascaraValor(valor))
                $("#valorEtapa").focusin();

                var divFats = $("#fatsConsolidados")
		//fim

                var divPai = $("<div'></div>")

                var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
					+ '<th>Observações</th>'
                    + '</tr>'
                    + '</thead>')
                var tableBody = $('<tbody id="tbody-informacoes"></tbody>')

                for (var i = 0; i < response.fatsIds.length; i++) {
                    var fatC = response.fatsIds[i];

                    var valorEtapa = fatC.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatC.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatC.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatC.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatC.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatC.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatC.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(parseFloat(valorEtapa).toFixed(2)) + '</td>')
					var observacoes = $('<td>' + fatC.balancete.observacoes + '</td>')
					if (observacoes.text() == "null") {observacoes = $('<td>' + '</td>')}

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)
					tr.append(observacoes)

                    tableBody.append(tr)

                }

                var trTotal = $('<tr></tr>')

                var beneficioTotal = response.beneficioConsolidado

                if (Number.isInteger(beneficioTotal)) {
                    beneficioTotal = beneficioTotal + ".00"
                }

                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                var total = $('<td><b>Total: R$' + mascaraValor(parseFloat(beneficioTotal).toFixed(2)) + '</b></td></tr></tr>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(total)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)

                tableBody.append(trTotal)

                table.append(tableBody)
                divPai.append(table)

                $("#corpoFats").append(divPai)

                var idContrato = response.faturamentoConsolidado.contrato.id

                getMemoriaDeCalculo(idContrato, beneficioTotal);

            }
        },
        error: function (xhr) {
        }
    });
    				
		}
		
	});

}

function mascaraValor(valor) {
    valor = valor.toString().replace(/\D/g, "");
    valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
    return valor
}

function getMemoriaDeCalculo(idContrato, beneficioTotal) {

    $.ajax({
        type: 'GET',
        url: "/contratos/" + idContrato + "/honorarios.json",
        async: false,
        success: function (hon) {

            for (var i = 0; i < hon.length; i++) {
                var honorario = hon[i];

                if (honorario.nome == "Percentual Fixo") {

                    $('#percentFixo').show()

                    var trFixo = $("<tr></tr>");

                    var beneficioFixo = $('<td> R$ ' + mascaraValor(beneficioTotal) + '</td>')
                    var percentualFixo = $('<td>' + honorario.valorPercentual + '%</td>')

                    var percent = (honorario.valorPercentual / 100)
                    var resultado = (beneficioTotal * percent)

                        if (Number.isInteger(resultado)) {
                            resultado = resultado + ".00"
                        }

                    var valor = $('<td class="resultadoPF">R$ ' + mascaraValor(parseFloat(resultado).toFixed(2)) + '</td>')

                    trFixo.append(beneficioFixo)
                    trFixo.append(percentualFixo)
                    trFixo.append(valor)

                    $('#tbody-percentualFixo').append(trFixo);

                }
                if (honorario.nome == "Percentual Escalonado") {
				
                    $('#escalonado').show()

                    var temp = beneficioTotal;
                    var valorCalc = 0.00;
                    var total = 0.00;
					var totalAplicado = 0.00;

                    var faixas = honorario.faixasEscalonamento

                    for (var i = 0; i < faixas.length; i++) {

                        var faixa = faixas[i];

                        var linhaFaixa = $("<tr></tr>");

                        var inicio = faixa.valorInicial;
                        var fim = faixa.valorFinal;
                        var beneficioAtribuido = 0.00;

                        if (Number.isInteger(inicio)) {
                            inicio = inicio + ".00"
                        } else {
							inicio = Number(Math.floor(inicio, -1).toFixed(2))
							inicio = parseFloat(inicio).toFixed(2)
						}
						
                        if (Number.isInteger(fim)) {
                            fim = fim + ".00"
                        } else {
							fim = Number(Math.floor(fim, -1).toFixed(2))
							fim = parseFloat(fim).toFixed(2)
						}

                        var gap1 = fim - inicio;
                        if (Number.isInteger(gap1)) {
                            gap1 = gap1 + ".00"
                        }

                        var intervalo = $('<td style="white-space: nowrap;">R$ ' + mascaraValor(inicio) + ' - R$ ' + mascaraValor(fim) + '</td>')
                        var percent = $('<td>' + faixa.valorPercentual + '%</td>')

                        if (temp > 0) {

                            if (temp >= gap1) {
                                valorCalc = parseFloat(gap1).toFixed(2) * (parseFloat(faixa.valorPercentual).toFixed(2)/100);
                                temp = parseFloat(temp).toFixed(2) - parseFloat(gap1).toFixed(2);
                                beneficioAtribuido = parseFloat(gap1).toFixed(2)
								totalAplicado += beneficioAtribuido;
								totalAplicado = parseFloat(totalAplicado).toFixed(2)
                            } else if (temp < gap1) {
                                valorCalc = parseFloat(temp).toFixed(2) * (parseFloat(faixa.valorPercentual).toFixed(2)/100);
                                beneficioAtribuido = parseFloat(temp).toFixed(2);
                                temp = parseFloat(temp).toFixed(2) - parseFloat(gap1).toFixed(2);
								totalAplicado += beneficioAtribuido;
								totalAplicado = parseFloat(totalAplicado).toFixed(2)
                            }

                        } else {
                            valorCalc = 0;
                        }

                        var beneficio = $('<td> R$ ' + mascaraValor(parseFloat(beneficioAtribuido).toFixed(2)) + '</td>')
                        var honorarioCalc = $('<td class="valorHon"> R$ ' + mascaraValor(parseFloat(valorCalc).toFixed(2)) + '</td>')

                        linhaFaixa.append(intervalo)
                        linhaFaixa.append(beneficio)
                        linhaFaixa.append(percent)
                        linhaFaixa.append(honorarioCalc)

                        $('#tbody-escalonado').append(linhaFaixa);

                    }

                    if (temp > 0) {

                        var percentualAcima = honorario.percentAcimaDe;
                        var honorarioAcimaDe = parseFloat((percentualAcima / 100) * temp).toFixed(2);

                        var vlAcimaDe = honorario.acimaDe
                        if (Number.isInteger(vlAcimaDe)) {
                            vlAcimaDe = vlAcimaDe + ".00"
							vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
                        } else {
							vlAcimaDe = Number(Math.floor(vlAcimaDe, -1).toFixed(2))
							vlAcimaDe = parseFloat(vlAcimaDe).toFixed(2)
						}

                        var limitacao = honorario.limitacao
                        if (Number.isInteger(limitacao)) {
                            limitacao = limitacao + ".00"
                        } else {
							limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
							limitacao = parseFloat(limitacao).toFixed(2)
						}

                        var linhaAcimaDe = $("<tr></tr>");

                        var acimaDe = $('<td>Acima de: R$ ' + mascaraValor(vlAcimaDe) + ' (Limitação: R$ ' + mascaraValor(limitacao) + ')</td>');
                        var percentAcima = $('<td>' + percentualAcima + '%</td>');
                        var beneficioAcima = $('<td class="valorAplicadoAcimaDe"> R$ ' + mascaraValor(parseFloat(temp).toFixed(2)) + '</td>');

                        var valorAcimaDe = $('<td class="valorHon"> R$ ' + mascaraValor(honorarioAcimaDe) + '</td>');
						var valorAplicadoAcimaDe = $('<td class="valorAplicadoAcimaDe"> R$ ' + mascaraValor(totalAplicado) + '</td>');

                        linhaAcimaDe.append(acimaDe)
                        linhaAcimaDe.append(beneficioAcima)
                        linhaAcimaDe.append(percentAcima)
                        linhaAcimaDe.append(valorAcimaDe)

						$('#tbody-escalonado').append(linhaAcimaDe);
					}

                    $.each($('.valorHon'), function () {
                    		var valorLinha = parseFloat($(this).text().replace("R", "").replace("$", ".").replace(/\./g, '').replace(",", ".")).toFixed(2);
                            total = Number(parseFloat(total).toFixed(2)) + Number(parseFloat(valorLinha).toFixed(2))
                    	})
                            if (Number.isInteger(total)) {
                                total = total + ".00"
                            }

					$.each($('.valorAplicadoAcimaDe'), function () {
                        var valorLinhaAcimaDe = parseFloat($(this).text().replace("R", "").replace("$", ".").replace(/\./g, '').replace(",", ".")).toFixed(2);
						totalAplicado = Number(parseFloat(totalAplicado).toFixed(2)) + Number(parseFloat(valorLinhaAcimaDe).toFixed(2))
                    })

						var trTotal = $("<tr></tr>")
						var vazio1 = $("<td> </td>")
						var vazio2 = $("<td><b>R$ " + mascaraValor(totalAplicado) + "</b></td>")
						var vazio3 = $("<td> </td>")
						
						trTotal.append(vazio1)
						trTotal.append(vazio2)
						trTotal.append(vazio3)
						
						if(total>limitacao){
							var resultadoSomaLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+" (Limitado R$ "+mascaraValor(limitacao)+")</b></td>")
							
							trTotal.append(resultadoSomaLimit)
							 $("#valorTotal").val(mascaraValor(limitacao))
                        	 $("#valorTotal").focusin();
						} else {
							var resultadoSomaNoLimit = $("<td><b>Total R$"+mascaraValor(parseFloat(total).toFixed(2))+"</b></td>")	
							trTotal.append(resultadoSomaNoLimit)
							$("#valorTotal").val(mascaraValor(parseFloat(total).toFixed(2)))
                        	$("#valorTotal").focusin();
						}
						
						$('#tbody-escalonado').append(trTotal);

                       
                }
            }
        }
    })
}


function balancete() {
    $("#TabelaBalancete").empty();
    $('#modalbalancete').modal('open');
    modalBalancete()
}

function modalBalancete() {
    
	var idProducao = $("#idProducao").val();

    $.ajax({
        type: 'GET',
        url: "/producoes/" + idProducao + "/listaBalanceteFaturamento.json",
        success: function (listaBalanceteFaturamento) {
            for (var i = 0; i < listaBalanceteFaturamento.length; i++) {
					
					var idBalancete = listaBalanceteFaturamento[i].id;
					var gerouFaturamento = listaBalanceteFaturamento[i].gerouFaturamento;
                	var reducaoImposto = "<td> R$ " + listaBalanceteFaturamento[i].reducaoImposto + "</td>";
                	var valorTotal = "<td>  R$ " + listaBalanceteFaturamento[i].valorTotal + "</td>";
                	var anoCampanha = "<td>" + listaBalanceteFaturamento[i].producao.ano + "</td>";
					var versao  = "<td>" + listaBalanceteFaturamento[i].versao + "</td>";
                	if (gerouFaturamento == false) {
                    	gerouFaturamento = "<td>" + "Não" + "</td>";
                	} else {
                    	gerouFaturamento = "<td>" + "Sim" + "</td>";
                	}

               		buscaIdEtapasBalancete(idProducao)
					var idEtapas = $('#idDoEtapas').val();
					
                	var botaoBalancete = "<td> <button class='waves-effect light-blue darken-4 ' style='height:20px;width:25px' onclick= window.location.href='/producoes/balanceteUpdate/" + idBalancete + "?idEtapa=" + idEtapas + "'></button> </td>"
                	$("#TabelaBalancete").append("<tr>" + anoCampanha + versao +valorTotal + reducaoImposto + gerouFaturamento + botaoBalancete + "</tr>")
            	
			}
        }
    })

 	function buscaIdEtapasBalancete(idProducao) {

    	$.ajax({
        	type: 'GET',
        	url: "/producoes/" + idProducao + "/etapasBalancetes.json",
			async: false,
        	success: function (etapa) {
				 $('#idDoEtapas').val(etapa.id);
            }
    	})
	}
}



function consolidado(){
	
	idFaturamento = $("#id").val();
	
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
	
			console.log(faturamento)
	
			if(faturamento.consolidadoPorFat == true){
				simularConsolidacaoFaturamento();
			}
			if(faturamento.consolidado == true){
				simularConsolidacao();
			}
		}
	})
}

function simularConsolidacaoFaturamento(){
	idFaturamento = $("#id").val();
	 var fats = []
	 
	 $.ajax({
        type: 'GET',
        url: "/faturamentos/" + idFaturamento + "/faturamentoConsolidado.json",
        success: function (faturamento) {
			ids = faturamento.idsFaturamentosConsolidados.split(";");
					
					for(var i=0 ; i<ids.length; i++){
						
						var fat = {
            				idFaturamento: ids[i]
        				}
        				fats.push(fat);	
					}	
	
	url = "/faturamentos/simularConsolidacaoFaturamento/";
	
    $.ajax({
        url: url,
        type: "post",
        data: JSON.stringify(fats),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (response) {
	
		$('#fatConsolidado').show();
		
		$("#cnpjConsolidado").val(response.faturamentoConsolidadoByFat.cliente.cnpj)
        $("#cnpjConsolidado").mask('00.000.000/0000-00', {
                    reverse: true
                });
       $("#cnpjConsolidado").focusin();
       $("#idClienteConsolidado").val(response.faturamentoConsolidadoByFat.cliente.id)
	   $("#razaoSocialConsolidado").val(response.faturamentoConsolidadoByFat.cliente.razaoSocial)
       $("#razaoSocialConsolidado").focusin();
       $("#idProdutoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.id)
       $("#produtoConsolidado").val(response.faturamentoConsolidadoByFat.producao.produto.nome)
       $("#produtoConsolidado").focusin();
  	   $("#campanhaConsolidado").val(response.faturamentoConsolidadoByFat.producao.ano)
       $("#campanhaConsolidado").focusin();
       
       $("#idProducaoConsolidado").val(response.faturamentoConsolidadoByFat.producao.id)
       $("#idsFatsConsolidado").val(response.faturamentoConsolidadoByFat.idsFaturamentosConsolidados)
       
      var divInfomacaoConsolidacao = $("<div'></div>")
	  
 	  var table = $('<table class="responsive-table table-default bordered">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Versão</th>'
                    + '<th>Apuração</th>'
                    + '<th>Dispêndio</th>'
                    + '<th>Redução</th>'
                    + '<th>Etapa Trabalho</th>'
                    + '<th>% da Etapa</th>'
                    + '<th>Valor Faturamento</th>'
					+ '<th>Observações</th>'
                    + '</tr>'
                    + '</thead>')
                    
        var tableBodyFatConsolidados = $('<tbody id="tbody-informacoesFatConsolidados"></tbody>')
        
		for (var i = 0; i < response.fatsConsolidadosIds.length; i++) {
                    var fatConsolidado = response.fatsConsolidadosIds[i];
                    var valorEtapa = fatConsolidado.valorEtapa

                    if (Number.isInteger(valorEtapa)) {
                        valorEtapa = valorEtapa + ".00"
                    }

                    var tr = $('<tr></tr>')

                    var percent = fatConsolidado.percetEtapa;
                    if (percent == "1") {
                        percent = "100"
                    }

                    var versao = $('<td>' + fatConsolidado.balancete.versao + '</td>')
                    var apuracao = $('<td>' + fatConsolidado.balancete.producao.tipoDeApuracao + '</td>')
                    var dispendio = $('<td> R$ ' + fatConsolidado.balancete.valorTotal + '</td>')
                    var reducao = $('<td> R$ ' + fatConsolidado.balancete.reducaoImposto + '</td>')
                    var etapa = $('<td>' + fatConsolidado.etapa.nome + '</td>')
                    var percent = $('<td>' + percent + '%</td>')
                    var valor = $('<td> R$ ' + mascaraValor(valorEtapa) + '</td>')
					var observacoes = $('<td>' + fatConsolidado.balancete.observacoes + '</td>')
					if (observacoes.text() == "null") {observacoes = $('<td>' + '</td>')}

                    tr.append(versao)
                    tr.append(apuracao)
                    tr.append(dispendio)
                    tr.append(reducao)
                    tr.append(etapa)
                    tr.append(percent)
                    tr.append(valor)
					tr.append(observacoes)

                    tableBodyFatConsolidados.append(tr)

                }
               
                 var trTotal = $('<tr></tr>')

                var valorEtapaConsolidado = response.faturamentoConsolidadoByFat.valorEtapa
				
                if (Number.isInteger(valorEtapaConsolidado)) {
                    valorEtapaConsolidado = valorEtapaConsolidado + ".00"
                }
                
                $("#valorTotalConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorTotalConsolidado").focusin();
       			
       				
       			$("#valorEtapaConsolidado").val(mascaraValor(valorEtapaConsolidado))
       			$("#valorEtapaConsolidado").focusin();
       			
                var vazio1 = $('<td> </td>')
                var vazio2 = $('<td> </td>')
                var vazio3 = $('<td> </td>')
                var vazio4 = $('<td> </td>')
                var vazio5 = $('<td> </td>')
                var vazio6 = $('<td> </td>')
                
                
                var total = $('<td><b>Total: R$' + mascaraValor(valorEtapaConsolidado) + '</b></td>')

                trTotal.append(vazio1)
                trTotal.append(vazio2)
                trTotal.append(vazio3)
                trTotal.append(vazio4)
                trTotal.append(vazio5)
                trTotal.append(vazio6)
                trTotal.append(total)

                tableBodyFatConsolidados.append(trTotal)
                
                
			table.append(tableBodyFatConsolidados)
            divInfomacaoConsolidacao.append(table)
             
  			$("#corpoFatConsolidados").append(divInfomacaoConsolidacao)

		}
	})
   
   }
	
	
	})
			
}


function botaoVoltar(){
	//botão de voltar sem perder os filtros da página anterior
	// na páginda list.html  foi armazenado o valor da url
	//pega url armazenada(getItem),coloca o valor em um input(#currentParametros) e retorna para ela.
	
	 $("#currentParametros").val(sessionStorage.getItem("params"));
	 window.location.href =   $("#currentParametros").val();
	
	 if( $("#currentParametros").val()  == ""  ){
		 window.location.href = "/faturamentos/all";
	}
	  
}



