$("#cnpj").blur(buscaEmpresa);
$('#btnBuscaProducoes').click(buscaProducoes);
$('#addFaturamento').click(addLinhaFaturamento);
$('#removeFaturamento').click(apagaLinhaFaturamento);
$('#btn-add-faturamento').click(salvaFaturamentos);
var idProdManual = 0;
var idBalManual = 0;

$('#selectProduto').change(
		function() {
			var prodId = $('#selectProduto :selected').val();
			console.log(prodId)

			if (prodId == "") {

				$('#select-etapa').attr("readonly", 'readonly');
				$('#select-etapa').find('option').remove().end().append(
						'<option value="" selected="selected">Selecione</option>').val(
						'whatever');
				$('#select-etapa').material_select();
			} else {
				var url = "/produtos/" + prodId + "/etapas.json"
				var options = "<option selected='selected' value=''>Selecione</option>";

				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {
						console.log(data)

						$(data).each(
								function(i) {
									var etapa = data[i];
									options += "<option value='" + etapa.id
											+ "'>" + etapa.nome + "</option>"
								});

						$("#select-etapa").empty();
						$("#select-etapa").append(options);
						$('#select-etapa').removeAttr('disabled');
						$("#select-etapa").material_select();
						
						console.log($("#select-etapa").innerText)
					}
				});
			}
		});

$(function () {
	formatarCnpj();
	buscaEmpresa();
	validarCnpj();
	maskValor();
})

function maskValor(){
$(".valor").each(function(){
	var texto = $(this).text();
	texto = parseFloat(texto).toLocaleString('pt-BR')
	texto = "R$ "+texto
	$(this).text(texto);
})
}

function formatarCnpj() {
	$("#cnpj").mask('00.000.000/0000-00', {
		reverse : true
	});
}

function validarCnpj() {
    var caracteresCnpj = $("#cnpj").val().length;
    if (caracteresCnpj < 18) {
        $("#cnpj").tooltipster("open").tooltipster("content", "CNPJ inválido");
    } else if (caracteresCnpj == 18) {
        $("#cnpj").tooltipster("close");
    }
}

function buscaEmpresa() {
	
	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);
	
	if ( !$(this).val().length == 0 ) {

		$.getJSON("/clientes/" + cnpj +  ".json", function(dados) {

			if (dados.erro != true) {

				$("#razaoSocial").val(dados.razaoSocial);
				$("#razaoSocial").focusin();
				$("#idcliente").val(dados.id);
				
			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})
	}
}

function buscaProducoes() {
	limpaTabela();

	var cnpj = $('#cnpj').val().replace(/\D/g, "");
	var produto = $('#selectProduto').find(":selected").val();
	var campanha = $('#select-ano').find(":selected").val();
	
	var url = '/producoes/' + cnpj + '/' + produto + '/' + campanha +'/producoes.json';
	$.get(url, function(data) {
		
		if(data.length == 0){
			Materialize.toast('Nenhuma produção encontrada.', 5000, 'rounded');
		} else {
		$(data).each(function() {
			
			var producao = this;
			console.log(producao)
			$("#corpoTabelaProducoes").append(
				"<tr><td>"+producao.produto.nome+"</td>"
				+"<td>"+producao.ano+"</td>"
				+"<td>"+producao.tipoDeApuracao+"</td>"
				+"<td>"+producao.situacao+"</td>"
				+"<td><p><input id='"+producao.id+"' name='idProducao' value='"+producao.id+"' type='radio'/><label for='"+producao.id+"'></label></p></td></tr>")
		});
	}
	});
}

$(document).on('change', 'input:radio[name="idProducao"]', function(event) {
	var idProducao = $(this).val();
	idProdManual = idProducao

	var url = '/producoes/balancetes/' + idProducao + '.json';
	$.get(url, function(data) {

			$(data).each(function() {
				
				var balanceteAtual = this;
				console.log(balanceteAtual)
				
				var stringValidado;
				var stringGerouFat;
				
				if(balanceteAtual.validado == true) {
					stringValidado = "Sim"
				} else {
					stringValidado = "Não"
				}
				
				if(balanceteAtual.gerouFaturamento == true) {
					stringGerouFat = "Sim"
				} else {
					stringGerouFat = "Não"
				}
				
				$("#corpoTabelaBalancetes").append(
				"<tr><td>"+balanceteAtual.versao+"</td>"
				+"<td>"+balanceteAtual.porcentagemRealizado+"</td>"
				+"<td>"+balanceteAtual.valorTotal+"</td>"
				+"<td>"+balanceteAtual.reducaoImposto+"</td>"
				+"<td>"+stringValidado+"</td>"
				+"<td>"+stringGerouFat+"</td>"
				+"<td><p><input id='"+balanceteAtual.id+"' name='idBalancete' value='"+balanceteAtual.id+"' type='radio'/><label for='"+balanceteAtual.id+"'></label></p></td></tr>")
			}
		);
});
})

$(document).on('change', 'input:radio[name="idBalancete"]', function(event) {
	var idBalancete = $(this).val();
	idBalManual = idBalancete
})

function limpaTabela() {
	$("#corpoTabelaProducoes").empty()
}

function buscaFaturamentos() {
	if($("#preLoaded").val()=="n"){
    var idProd = $("input:radio[name ='producao']:checked").attr("id");
	} else {
	var idProd = $("#idProdLoaded").val()
	}

    var url = idProd + '/fatsProd.json';
    $.get(url, function (data) {

        if (data.length == 0) {
            Materialize.toast('Nenhum faturamento encontrato.', 5000, 'rounded');
        } else {
		
   		$('#tabela > tbody').empty();

            $(data).each(function () {

                var fat = this;

                var balancete;

                if (fat.balancete == null || fat.balancete == undefined) {
                    balancete = "Sem versão";
                } else {
                    balancete = fat.balancete.versao
                }
                
                console.log('o faturamento é: ' + fat)
                console.log('o tipo de apuração é: ' + fat.producao.tipoDeApuracao)

                $("#corpoTabela").append(
                    "<tr>"
                    + "<td>" + fat.produto.nome + "</td>"
                    + "<td>" + fat.producao.ano + "</td>"
                    + "<td>" + fat.producao.tipoDeApuracao + "</td>"
                    + "<td>" + balancete + "</td>"
                    + "<td>" + fat.etapa.nome + "</td>"
                    + "<td>" + fat.estado + "</td>"
                    + "<td><p>" + maskValor(fat.valorEtapa) + "</p></td>"
                    + "<td class='check center'><p><input name='faturamentos' id='" + fat.id + "' type='checkbox'/><label for='" + fat.id + "'></label></p></td>"
                    + "</tr>")

            });
         console.log('o id da produção é' + idProd)   
        var url = "/producoes/getBalancetesPrejuizoByProd/"+idProd+"/.json"
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: url,
            dataType: "JSON",
            success : function(listaBalancete){
			console.log('entrou no método que lista balancetes no prejuízo')
				$(listaBalancete).each(function () {
					console.log('entrou no método que lista balancetes no prejuízo')
					balancete = this;
					 $("#corpoTabela").append(
                    "<tr>"
                    + "<td>" + balancete.producao.produto.nome + "</td>"
                    + "<td>" + balancete.producao.ano + "</td>"
                    + "<td>" + balancete.producao.tipoDeApuracao + "</td>"
                    + "<td>" + balancete.versao + "</td>"
                    + "<td>" + '' + "</td>"
                    + "<td>" + '' + "</td>"
                    + "<td><p>" + 'R$ 0,00' + "</p></td>"
                    + "<td>" +'PREJUIZO' + "</td>"
                    + "</tr>")
				})	
			}
        });
        }
    });
    maskValor();
}

function addLinhaFaturamento() {
	var lista = $("#divFaturamentosManuais");

	lista.append(escreveLinhas());
	index = $('.indexFaturamentoManual').length;
	console.log("Quantidade de campos " + index)
	preencheValor(index);
}

function apagaLinhaFaturamento() {

	$('#divFaturamentosManuais').children().last().remove()

	index = $('.indexFaturamentoManual').length
	console.log("Quantidade de campos " + index)

	preencheValor(index)
}

function preencheValor() {

//	var i = 1

	$.each($('.valorParcela'), function() {
		if (!$('#parcelaPaga' + i).is(':checked')) {
			$(this).val(valorDividido);
		}
		i++
	});
}

function escreveLinhas() {
	var divRow = $("<div>").addClass("row");
	var tamanho = Number($('.indexFaturamentoManual').length) + Number(1);

	var divIndexFaturamentoManual = $("<div class='col m1'>" +
		"<label for='indexFaturamentoManual'>Número</label> " +
		"<input type='text' class='indexFaturamentoManual' value='"+tamanho+"' name='indexFaturamentoManual'/></div>");
	
	var divInputTotalFaturar = $("<div class='col m2'>" +
		"<label for='totalFaturarFat" + tamanho + "'>Total Faturar R$</label> " +
		"<input type='text' id='totalFaturarFat" + tamanho + "' " + " name='totalFaturarFat'/></div>");
	
	var divValorEtapa = $("<div class='col m2'>" +
		"<label for='valorEtapaFat" + tamanho + "'>Valor Etapa R$</label> " +
		"<input type='text' id='valorEtapaFat" + tamanho + "' " + " name='valorEtapaFat'/></div>");
	
	var divDiferenca = $("<div class='col m2'>" +
		"<label for='diferencaFat" + tamanho + "'>Diferença R$</label> " +
		"<input type='text' id='diferencaFat" + tamanho + "'" +
		" name='diferencaFat'/></div>");

	divRow.append(divIndexFaturamentoManual);
	divRow.append(divInputTotalFaturar);
	divRow.append(divValorEtapa);
	divRow.append(divDiferenca);

	return divRow;
}

function salvaFaturamentos() {
	var razaoSocial = $("#razaoSocial").val();
	var cnpj = $("#cnpj").val();
	var idProducao = idProdManual;
	var idBalancete = idBalManual;
	var idProduto = $("#selectProduto").val();
	var etapa = $("#select-etapa").val();
	var campanha = $("#select-ano").val();
	var anoFiscal = $("#anoFiscal").val();
	var obsConsolidacao = $("#observacao").val();
	var index = 1;
	
	$.each($('.indexFaturamentoManual'), function() {
		var faturamentoManualAtual = {
				razaoSocialFaturar : razaoSocial,
				cnpjFaturar : cnpj,
				produto : idProduto,
				etapa : etapa,
				campanha : campanha,
				anoFiscal : anoFiscal,
				valorT : $("#totalFaturarFat"+[index]).val(),
				valorE : $("#valorEtapaFat"+[index]).val(),
				valorDiferencaAtual : $("#diferencaFat"+[index]).val(),
				idProducao : idProducao,
				idBalancete : idBalancete
		}
		console.log(faturamentoManualAtual)
		
		$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/faturamentos/addManuaisConsolidados",
		data: JSON.stringify(faturamentoManualAtual),
		dataType: 'JSON',
		statusCode: {
			200: function(data) {
				Materialize
					.toast(
						'Nota criada com sucesso!',
						5000, 'rounded');
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});
		index++;
	});
	
/*	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/faturamentos/addFaturamentoConsolidadoManual",
		data: JSON.stringify(faturamentoAtual,valorT,valorE,idProducao,idBalancete),
		dataType: 'JSON',
		statusCode: {
			200: function(data) {
				Materialize
					.toast(
						'Nota criada com sucesso!',
						5000, 'rounded');
						$('#btn-cad-NotaFiscal').attr("disabled", true);
						window.location.replace("all")
			},
			500: function() {
				Materialize.toast(
					'Ops, houve um erro interno',
					5000, 'rounded');
			},
			400: function() {
				Materialize
					.toast(
						'Você deve estar fazendo algo de errado',
						5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada',
					5000, 'rounded');
			}
		}
	});*/
	
	//ajax para buscar os ids novos e consolidar em um só e salvar
}
