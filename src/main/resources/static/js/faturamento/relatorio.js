$(function() {
	$('.collapsible').collapsible('open', 0);
	$(".modal").modal();
	$(".select").material_select();
	$('#select-etapa').attr("disabled", 'disabled');
	$('#select-etapa').material_select();
	$('#tabela-faturamentos').DataTable({
		language : {
			"sEmptyTable" : "Nenhum registro encontrado",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ".",
			"sLengthMenu" : "_MENU_ resultados por página",
			"sLoadingRecords" : "Carregando...",
			"sProcessing" : "Processando...",
			"sZeroRecords" : "Nenhum registro encontrado",
			"sSearch" : "Pesquisar",
			"oPaginate" : {
				"sNext" : "Próximo",
				"sPrevious" : "Anterior",
				"sFirst" : "Primeiro",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			},
			"select" : {
				"rows" : {
					"_" : "Selecionado %d linhas",
					"0" : "Nenhuma linha selecionada",
					"1" : "Selecionado 1 linha"
				}
			}
		},
		bFilter : false,
		iDisplayLength : 50000,
		columnDefs : [ {
			targets : [ 0, 1, 2 ],
			className : 'mdl-data-table__cell--non-numeric'
		} ]
	});
});

$('#selectProduto').change(
		function() {

			var prodId = $('#selectProduto :selected').val();
			console.log(prodId)

			if (prodId == "") {

				$('#select-etapa').attr("readonly", 'readonly');

				$('#select-etapa').find('option').remove().end().append(
						'<option value="" selected="selected">Selecione</option>').val(
						'whatever');

				$('#select-etapa').material_select();

			} else {

				var url = "/produtos/" + prodId + "/etapas.json"

				var options = "<option selected='selected' value=''>Selecione</option>";

				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {

						$(data).each(
								function(i) {
									var etapa = data[i];
									options += "<option value='" + etapa.id
											+ "'>" + etapa.nome + "</option>"
								});

						$("#select-etapa").empty();
						$("#select-etapa").append(options);
						$('#select-etapa').removeAttr('disabled');
						$("#select-etapa").material_select();

					}
				});
			}
		});

function fnExcelReport() {
    var tab_text = '\uFEFF<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Relatório Faturamentos/x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + $('#tabela-faturamentos').html();
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8"
            });
            navigator.msSaveBlob(blob, 'faturamentos.xls');
        }
    } else {
        $('#excelBtn').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
        $('#excelBtn').attr('download', 'faturamentos.xls');
    }
}

$(function filialToSigla() {
	$.each($('.filialClass'), function () {
		var filialAtual = $(this).text();

		switch (filialAtual) {
			case 'RIO DE JANEIRO':
			$(this).text('RJ')
			break;
			
			case 'CURITIBA':
			$(this).text('PR')
			break;
			
			case 'SÃO PAULO':
			$(this).text('SP')
			break;
			
			case 'BRASÍLIA':
			$(this).text('DF')
			break;
			
			case 'BELO HORIZONTE':
			$(this).text('MG')
			break;
			
			case 'PERNAMBUCO':
			$(this).text('RE')
			break;
			
			case 'PORTO ALEGRE':
			$(this).text('RS')
			break;
			
			case 'FLORIANÓPOLIS':
			$(this).text('SC')
			break;
			
			case 'MANAUS':
			$(this).text('AM')
			break;
			
			default:
			$(this).text('')
			break;
		}
	})
});

$(function maskValor(){
	
	$(".moneyClass").each(function(){
		var texto = $(this).text();
		if(texto == ""){
			$(this).text(texto);
		}else{
			texto = parseFloat(texto).toFixed(2).toLocaleString('pt-BR')
			texto = mascaraValor(texto)
			$(this).text("R$ " + texto);
		}
	})
})

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}


	
	
	
