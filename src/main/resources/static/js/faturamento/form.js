$("#cnpj").blur(buscaEmpresa);
$('#btnBuscaProducoes').click(buscaProducoes);
$("#form").submit(function () {
        $("#btn-add-faturamento").attr("disabled", true);
        return true;
});

$('#selectProduto').change(
		function() {

			var prodId = $('#selectProduto :selected').val();
			console.log(prodId)

			if (prodId == "") {

				$('#select-etapa').attr("readonly", 'readonly');

				$('#select-etapa').find('option').remove().end().append(
						'<option value="" selected="selected">Selecione</option>').val(
						'whatever');

				$('#select-etapa').material_select();

			} else {

				var url = "/produtos/" + prodId + "/etapas.json"

				var options = "<option selected='selected' value=''>Selecione</option>";

				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {
						console.log(data)

						$(data).each(
								function(i) {
									var etapa = data[i];
									options += "<option value='" + etapa.id
											+ "'>" + etapa.nome + "</option>"
								});

						$("#select-etapa").empty();
						$("#select-etapa").append(options);
						$('#select-etapa').removeAttr('disabled');
						$("#select-etapa").material_select();
						
						console.log($("#select-etapa").innerText)

					}
				});
			}
		});


$(function () {
	formatarCnpj();
	buscaEmpresa();
	validarCnpj();
	maskValor();
})

function maskValor(){
$(".valor").each(function(){
	var texto = $(this).text();
	texto = parseFloat(texto).toLocaleString('pt-BR')
	texto = "R$ "+texto
	$(this).text(texto);
})
}

function formatarCnpj() {
	$("#cnpj").mask('00.000.000/0000-00', {
		reverse : true
	});

}

function validarCnpj() {
    var caracteresCnpj = $("#cnpj").val().length;
    if (caracteresCnpj < 18) {
        $("#cnpj").tooltipster("open").tooltipster("content", "CNPJ inválido");
    } else if (caracteresCnpj == 18) {
        $("#cnpj").tooltipster("close");
    }
    
}

function buscaEmpresa() {
	
	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);
	
	if ( !$(this).val().length == 0 ) {

		$.getJSON("/clientes/" + cnpj +  ".json", function(dados) {

			if (dados.erro != true) {

				$("#razaoSocial").val(dados.razaoSocial);
				$("#razaoSocial").focusin();
				$("#idcliente").val(dados.id);
				
			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})
	}
}

function buscaProducoes() {
	limpaTabela();

	var cnpj = $('#cnpj').val().replace(/\D/g, "");
	var produto = $('#selectProduto').find(":selected").val();
	var campanha = $('#select-ano').find(":selected").val();
	
	var url = '/producoes/' + cnpj + '/' + produto + '/' + campanha +'/producoes.json';
	$.get(url, function(data) {
		
		if(data.length == 0){
			Materialize.toast('Nenhuma produção encontrada.', 5000, 'rounded');
		} else {

		$(data).each(function() {
			
			var producao = this;
			console.log(producao)

			$("#corpoTabelaProducoes").append(
				"<tr><td>"+producao.produto.nome+"</td>"
				+"<td>"+producao.ano+"</td>"
				+"<td>"+producao.tipoDeApuracao+"</td>"
				+"<td>"+producao.situacao+"</td>"
				+"<td><p><input id='"+producao.id+"' name='idProducao' value='"+producao.id+"' type='radio'/><label for='"+producao.id+"'></label></p></td></tr>")				
		});
	}
	});
}

$(document).on('change', 'input:radio[name="idProducao"]', function(event) {
	var idProducao = $(this).val();

	var url = '/producoes/balancetes/' + idProducao + '.json';
	$.get(url, function(data) {

			$(data).each(function() {
				
				var balanceteAtual = this;
				console.log(balanceteAtual)
				
				var stringValidado;
				var stringGerouFat;
				
				if(balanceteAtual.validado == true) {
					stringValidado = "Sim"
				} else {
					stringValidado = "Não"
				}
				
				if(balanceteAtual.gerouFaturamento == true) {
					stringGerouFat = "Sim"
				} else {
					stringGerouFat = "Não"
				}
				
				$("#corpoTabelaBalancetes").append(
				"<tr><td>"+balanceteAtual.versao+"</td>"
				+"<td>"+balanceteAtual.porcentagemRealizado+"</td>"
				+"<td>"+balanceteAtual.valorTotal+"</td>"
				+"<td>"+balanceteAtual.reducaoImposto+"</td>"
				+"<td>"+stringValidado+"</td>"
				+"<td>"+stringGerouFat+"</td>"
				+"<td><p><input id='"+balanceteAtual.id+"' name='idBalancete' value='"+balanceteAtual.id+"' type='radio'/><label for='"+balanceteAtual.id+"'></label></p></td></tr>")
			}
		);
});
})

function limpaTabela() {
	$("#corpoTabelaProducoes").empty()
}

//function preenche valores
//vai pegar os dados da produção pra setar os campos
function preencheValores() {
	
}
