$(function() {
	calculaImpostos();
	formataValores();
})

function formataValores(){
	var etapa = $('#idValorEtapa').text()
	var idTotal = $('#idTotal').text()
	var valorBruto = $('#idFatValorEtapa').text()
//	$('#idValorEtapa').text("R$ " + mascaraValor(etapa))
	$('#idTotal').text('R$ '+mascaraValor(idTotal))
	$('#idFatValorEtapa').text('R$ '+mascaraValor(valorBruto))
}

function calculaImpostos() {
	
	var valorBruto = $("#idFatValorEtapa").text();
	valorBruto = valorBruto.replaceAll("R","")
	valorBruto = valorBruto.replaceAll("$","")
	valorBruto = valorBruto.replaceAll(" ","")
	
	valorBruto = parseFloat(valorBruto)
	console.log(valorBruto, " transformar em número")


	var IRRF = valorBruto * (1.5 / 100)
	var CSSL = valorBruto * (1 / 100)
	var PIS = valorBruto * (0.65 / 100)
	var COFINS = valorBruto * (3 / 100)
	
	IRRF = parseFloat(IRRF).toFixed(2)
	CSSL = parseFloat(CSSL).toFixed(2)
	PIS = parseFloat(PIS).toFixed(2)
	COFINS = parseFloat(COFINS).toFixed(2)
	
	if((IRRF) < 10.0) {
		IRRF = 0.0;
		IRRF = parseFloat(IRRF).toFixed(2)
	}
	
	if((Number(CSSL) + Number(PIS) + Number(COFINS)) < 10.0) {
		CSSL = 0.0;
		PIS = 0.0;
		COFINS = 0.0;
		CSSL = parseFloat(CSSL).toFixed(2)
		PIS = parseFloat(PIS).toFixed(2)
		COFINS = parseFloat(COFINS).toFixed(2)
	}
	
	var total = Number(IRRF) + Number(CSSL) + Number(PIS) + Number(COFINS);
	var valorLiquido = valorBruto - total;
	
	total = parseFloat(total).toFixed(2)
	valorLiquido = parseFloat(valorLiquido).toFixed(2)
	
	console.log(total,valorLiquido)
	
	$("#idIRRF").text("R$ " + mascaraValor(IRRF))
	$("#idCSSL").text("R$ " + mascaraValor(CSSL))
	$("#idPIS").text("R$ " + mascaraValor(PIS))
	$("#idCOFINS").text("R$ " + mascaraValor(COFINS))
	$("#idTotalImpostos").text("R$ " + mascaraValor(total))
	$("#idValorLiquido").text("R$ " + mascaraValor(valorLiquido))
}

function calculaImpostosDevidosEmpresa() {
	
	var valorBruto = parseFloat($("#idValorEtapa").text());
	console.log(valorBruto, "valor bruto/etapa");

	var PISImpostosDevidos = valorBruto * (1.65 / 100)
	var COFINSImpostosDevidos = valorBruto * (7.6 / 100)
	var ISSImpostosDevidos = valorBruto * (3 / 100)
	//abaixo tá com valor de 0
	var ISSVariavelImpostosDevidos = valorBruto * (0 / 100)
	
	console.log("5",PISImpostosDevidos)
	console.log("6",COFINSImpostosDevidos)
	console.log("7",ISSImpostosDevidos)
	console.log("8",ISSVariavelImpostosDevidos)
	
	$("#idPISImpostosDevidos").text(PISImpostosDevidos)
	$("#idCOFINSImpostosDevidos").text(COFINSImpostosDevidos)
	$("#idISSImpostosDevidos").text(ISSImpostosDevidos)
	$("#idISSVariavelImpostosDevidos").text(ISSVariavelImpostosDevidos)
	
	var totalImpostosDevidos = PISImpostosDevidos + COFINSImpostosDevidos + ISSImpostosDevidos + ISSVariavelImpostosDevidos;
	
	var valorLiquido = valorBruto - total;
	
	$("#idTotalImpostosDevidos").text(total)
	$("#idValorLiquido").text(valorLiquido)
}

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}

function maskValor(){
$(".money").each(function(){
	var texto = $(this).text();
	texto = parseFloat(texto).toLocaleString('pt-BR')
//	texto = "R$ "+texto
	$(this).text(texto);
})
}

/*function carregaMemoriaDeCalculo() {
	$("#totalHonorarios").append("Total: R$ " );
	$("#totalHonorariosAplicados").append("Total Aplicado: R$ " );
}*/

