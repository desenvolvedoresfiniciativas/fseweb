function formatarCnpj() {
	$("#cnpj").mask('00.000.000/0000-00', {
		reverse : true
	});
	$("#cnpjFaturar").mask('00.000.000/0000-00', {
		reverse : true
	});
}

function formatarRUT() {
    $("#rut").mask('00.000.000-a', {
        reverse: true
    });
}

function validarCnpj() {
    var caracteresCnpj = $("#cnpj").val().length;
    if (caracteresCnpj < 18) {
        $("#cnpj").tooltipster("open").tooltipster("content", "CNPJ inválido");
    } else if (caracteresCnpj == 18) {
        $("#cnpj").tooltipster("close");
    }
    
}

$(function formatarCnpjClass() {
	$.each($('.cnpjClass'), function () {
		var cnpjAtual = $(this).val();
		console.log(cnpjAtual)
		cnpjAtual = cnpjAtual.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3\/\$4\-\$5");
		
		$(this).val(cnpjAtual);
})
})