$("#cnpjFaturar").blur(buscaEmpresa);

function buscaEmpresa() {
	
	var cnpj = $(this).val().replace(/\D/g, "");
	console.log(cnpj);
	
	if ( !$(this).val().length == 0 ) {

		$.getJSON("/clientes/" + cnpj +  ".json", function(dados) {

			console.log(dados);  
			if (dados.erro != true) {
				
				populaTabelaNotasFiscais(dados.id);

				/*$("#razaoSocialFaturar").val(dados.razaoSocial);
				$("#razaoSocialFaturar").focusin();
				$("#idCliente").val(dados.id);*/

			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
			}
		})
	}
	}
	
function populaTabelaNotasFiscais(idCliente){
			$.getJSON("/notasFiscais/" + idCliente +  "/notasCliente.json", function(dados) {
			console.log("entrou no ajax de notas fiscais")
			if (dados.erro != true) {
				
				$("#corpo-nota-fiscal").empty();
				
				console.log(dados)
				
			for(var i = 0; i<dados.length; i++){
				console.log('entrou no loop: ' +dados[i].id)
			var notaFiscal = dados[i]
			
			var versao = "";
			var etapa = "";
			
			/*if(NotaFiscal.balancete!=null){
				versao = NotaFiscal.balancete.versao
			}
			if(faturamento.etapa!=null){
				etapa = NotaFiscal.etapa.nome
			}*/
		try{
			$("#corpo-nota-fiscal").append("<tr><td>"+notaFiscal.numeroNF+"</td>"
			+"<td>"+notaFiscal.faturamento.produto.nome+"</td>"
			+"<td>"+notaFiscal.valorCobranca+"</td>"			
			+"<td>"+notaFiscal.faturamento.campanha+"</td>"
		/*	+"<td>"+versao+"</td>"
			+"<td>"+etapa+"</td>"*/
			
			+"<td><input name='notaFiscal.id' class='nota' type='radio' value='"+notaFiscal.id+"' id='"+notaFiscal.id+"' /><label for='"+notaFiscal.id+"'></label></td>")
			}catch(e){
				console.log('deu erro na nota fiscal: ',notaFiscal.id)
				console.log('o erro é: ', e)
			}
			}

			} else {
				Materialize.toast('Nenhuma nota fiscal encontrada para o cliente.', 5000, 'rounded');
				limparNomeEmpresa();
			}
		
		})
}

function limparNomeEmpresa() {
		//$("#nomeEmpresa").val("");
		$('#cnpjFaturar').val("");
		//$('#razaoSocialFaturar').val("");
	}
	
function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}
