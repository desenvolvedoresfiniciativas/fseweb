$(function(){
	$('select').material_select();
	$('.collapsible').collapsible('open', 0);
	porcentagem();
})

var idProducao;

$("#cnpj").mask('00.000.000/0000-00', {reverse : true});

$('#select-consultor').change(function () {
	
	var idConsultor = $('#select-consultor :selected').val();
	
	$.getJSON("/consultores/" + idConsultor +  ".json", function(dados) {

			if (dados.erro != true) {
				$("#email").val(dados.email);
				$("#email").focusin();
				$("#cargo").val(dados.cargo);
				$("#cargo").focusin();
				
			} else {
				$("#email").val("");
				$('#cargo').val("");
			}
		})	 

});

$(document).on("click",".nota",function() {
	 var botao = $(this);

		id = botao.val();
		console.log('o id é: ' +id)
		
		var url = '/notasFiscais/' + id + '/getNota.json'

	$.ajax({
		type: "GET",
		contentType: "application/json",
		url: url,
		dataType: 'JSON',
		success:function(nota){
			console.log(nota.valorCobranca)
			
			var valorCobrancaCorrigido    = nota.valorCobranca.replace(".", "");
			valorCobrancaCorrigido    = nota.valorCobranca.replace(",", ".");
			console.log('o valor corrigido é:', valorCobrancaCorrigido)
			//$("#valorLiquido").val(parseFloat(nota.valorCobranca).toFixed(2).toLocaleString('pt-BR'));


			//var valorLiquido = $('#valorLiquido').val(nota.valorCobranca)
			//$('#semImposto').val( parseFloat($(1.5 / 100) * valorCobrancaCorrigido));
			//$("#semImposto").val(parseFloat(valorLiquido).toFixed(2).toLocaleString('pt-BR'));
		}
	});
	})
	
/*$('#porcemtagemComissao').change(function(){
	var percentual = $('#porcemtagemComissao').val()
	console.log('o  percentual digitado foi: ' + percentual)
	var valorLiquido = $('#valorLiquido').val()
	
	var resultado = parseFloat($(percentual / 100) * valorLiquido);
	console.log('o valor da comissão sem o IRPJ deve ser: ' + resultado)	
	$('#comissaoValor').val(resultado)
	
	})	*/


$('#cnpj').change(
	
	function () {
	
	var cnpj = $('#cnpj').val().replace(/\D/g, "");
	console.log('CNPJ: ' + cnpj);
		
	if ( !$(this).val().length == 0 ) {
		
		
		$.getJSON("/clientes/" + cnpj +  ".json", function(dados) {
			console.log(dados);
			if (dados != null) {
					
				$("#cliente").val(dados.razaoSocial);
				$("#cliente").focusin();
				$('#razaoSocial').val(dados.razaoSocial);
				$('#razaoSocial').focusin();
				$('#razaoSocialFaturar').val(dados.razaoSocial)
				$('#razaoSocialFaturar').focusin();
									
				var id = dados.id;
				var url = "/producoes/clientes/" + id + ".json"
				var options = "<option value='' disabled='disabled' selected='selected'>Selecione a produto</option>";
				
				
				return $.ajax({
					type: 'GET',
					url: url,
					async: false,
					success: function (data) {
					
						if(data.length != 0){
													
							$(data).each(function(i) {
								var producao = data[i];
								options+="<option value='"+producao.id+"'>"+producao.produto.nome+"</option>"
							});
							
							$("#select-producao").empty();
							$("#select-producao").append(options);
							$("#select-producao").material_select();
							$('#porcemtagemComissao').attr('readonly', false);
						}else{
							Materialize.toast('Este cliente não possui produções', 2000, 'rounded');
							$('#selected').prop('selectedIndex',0)
							$('#selected option:first').prop('selected',true);
							$('#select-producao option:first').prop('selected',true);
							
							$("#valorContrato").val("");
							$("#anoCampanha").val("");
							$("#valorBruto").val("");
							$("#valorLiquido").val("");
							$("#porcemtagemComissao").val("");
							$("#totalReceber").val("");
							$('#porcemtagemComissao').attr('readonly', true);
							options = "<option value='' disabled='disabled' selected='selected'>Selecione a produto</option>"
							$("#select-producao").empty();
							$("#select-producao").append(options);
							$("#select-producao").material_select();
//							$('#select-producao :selected').val('0');
						} 
					}
				})
				
			} else {
				Materialize.toast('CNPJ não encontrado', 5000, 'rounded');
				limparNomeEmpresa();
				limparSelectProducao();
				console.log("sem producoes")
			}
			
		})
		
	}
	
	
	
	function limparNomeEmpresa(){
		$("#razaoSocial").val("");
		$('#cliente').val("");
		$('#idcliente').val("");
	}
	
	function limparSelectProducao(){
		$('#select-consultor :selected').val('1');
		console.log("limparSelectProducao")
		$("#select-producao").empty();
	}
});

$('#cnpj').change(function () {
		
	$('#select-producao').change(function () {
	
		var id = $('#select-producao :selected').val();
		
	
		$.getJSON("/comissoes/producao/" + id + "/valorNegocio.json", function(dados) {

			idProducao = dados.producao.id;
			console.log("id da producao: " + dados.producao.id);
//			console.log("Etapa de pagamento 1: " + dados.producao.contrato.etapasPagamento.etapa1.nome);
//			console.log("Id do contrato: " + dados.producao.contrato.id);
			
			$.getJSON("/comissoes/producao/" + idProducao + "/etapaPagamento.json", function(dados2) {
				
				if(dados2.etapa1 == null){
					console.log("Etapa 1 nula")
				}else{
					console.log("Etapa 1 nome: " + dados2.etapa1.nome);
					console.log("Porcentagem 1 valor: " + dados2.porcentagem1);
					
					options+="<option value='"+producao.id+"'>"+producao.produto.nome+"</option>"
					$("#select-producao").append(options);
					$("#select-producao").material_select();
				}

				if(dados2.etapa2 == null){
					console.log("Etapa 2 nula")
				}else{
					console.log("Etapa 2 nome: " + dados2.etapa2.nome);
					console.log("Porcentagem 2 valor: " + dados2.porcentagem2);
					options+="<option value='"+producao.id+"'>"+producao.produto.nome+"</option>"
					
					$("#select-producao").append(options);
					$("#select-producao").material_select();
				}

				if(dados2.etapa3 == null){
					console.log("Etapa 3 nula")
				}else{
					console.log("Etapa 3 nome: " + dados2.etapa3.nome);
					console.log("Porcentagem 3 valor: " + dados2.porcentagem3);
					
					options+="<option value='"+producao.id+"'>"+producao.produto.nome+"</option>"
					$("#select-producao").append(options);
					$("#select-producao").material_select();
				}
				
				
			})

			if (dados.erro != true) {
				var valorBruto;
				var valorLiquido;
				
				if (dados.estComercial == 0){
					valorBruto = dados.valorBase;
					$("#valorContrato").val(Number(valorBruto).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
					$("#valorContrato").focusin();
					$("#valorBruto").val(Number(valorBruto).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
					$("#valorBruto").focusin();
					Materialize.toast('Estimativa comercial vazia, utilizando Valor base', 5000, 'rounded');
					
				}else if(dados.valorBase == 0){
					valorBruto = dados.estComercial;
					$("#valorContrato").val(Number(valorBruto).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
					$("#valorContrato").focusin();
					$("#valorBruto").val(Number(valorBruto).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
					$("#valorBruto").focusin();
					Materialize.toast('Valor base do VN vazio, utilizando Estimativa comercial', 5000, 'rounded');
					
				}
				$("#totalReceber").focusin();
				
				var totalImpostos = $("#totalImpostos").val();
				valorLiquido = valorBruto - (totalImpostos / 100) * valorBruto;
				
				$("#totalReceber").val("R$0,00");
				
				$("#porcemtagemComissao").on('keyup', function(){
						var porcentagem = $("#porcemtagemComissao").val();
						var total = (porcentagem / 100) * valorLiquido;
						total = total.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
						$("#totalReceber").val(total);
				}).change();
				
				$("#valorBruto").val(Number(valorBruto).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
				
				$("#valorLiquido").val(Number(valorLiquido).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
				$("#valorLiquido").focusin();
				
				$("#anoCampanha").val(dados.producao.ano);
				$("#anoCampanha").focusin();
			} else {
				$("#valorContrato").val("");
				$("#anoCampanha").val("");
				$("#valorBruto").val("");
				$("#valorLiquido").val("");
				$("#porcemtagemComissao").val("");
				$("#totalReceber").val("R$0,00");
			}
		})	 
	
	});
})

//$('#select-producao').change(function () {
//	
//	var id = $('#select-producao :selected').val();
//	
//	$.getJSON("/comissoes/producao/" + id + "/etapaPagamento.json", function(dados) {
//		console.log(dados);
//		if (dados.erro != true) {
//			
//			if(data.length != 0){
//													
//				$(data).each(function(i) {
//					var producao = data[i];
//					options+="<option value='"+producao.id+"'>"+producao.produto.nome+"</option>"
//				});
//				
//				$("#select-producao").empty();
//				$("#select-producao").append(options);
//				$("#select-producao").material_select();
//				$('#porcemtagemComissao').attr('readonly', false);
//			
//				var options = "<option value='' disabled='disabled' selected='selected'>Selecione a produto</option>";
//			}
//		}
//		
//	})
//	
//})


function porcentagem(){
	var iss = $("#iss").val();
	var cofins = $("#cofins").val();
	var pis = $("#pis").val();
	var csll = $("#csll").val();
	var irpj = $("#irpj").val();
	
	$("#iss").val(iss + "%");
	$("#cofins").val(cofins + "%");
	$("#pis").val(pis + "%");
	$("#csll").val(csll + "%");
	$("#irpj").val(irpj + "%");
	
}

function handleChange(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 20) input.value = 20;
  }
