$(function(){
	filtroTabela($('#txt-pesq-consultor'));
	filtroTabelaCargo($('#cargos'));
	$('select').material_select();
    $(".modal").modal();
})

$('#geraCOMISSOES').click(function(){
	
var campanha = $('#campanhaComissao').find(":selected").val();
console.log(campanha)
		
		
		
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/comissoes/calculaComissaoByAno/" + campanha,
			dataType:"JSON",
	        statusCode: {
	            200: function () {
	                Materialize
	                    .toast(
	                        'Itens salvos com sucesso!',
	                        5000, 'rounded');
	            },
	            500: function () {
	                Materialize.toast(
	                    'Ops, houve um erro interno',
	                    5000, 'rounded');
	            },
	            400: function () {
	                Materialize
	                    .toast(
	                        'Você deve estar fazendo algo de errado',
	                        5000, 'rounded');
	            },
	            404: function () {
	                Materialize.toast('Url não encontrada',
	                    5000, 'rounded');
	            }
	        }	
		});
			
})

$('#geraPREMIACOES').click(function(){
	
var campanha = $('#campanhaPremiacao').find(":selected").val()
console.log(campanha)
		
	$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/premios/gerarPremiacoes/" + campanha,
			dataType:"JSON",
	        statusCode: {
	            200: function () {
	                Materialize
	                    .toast(
	                        'Itens salvos com sucesso!',
	                        5000, 'rounded');
	            },
	            500: function () {
	                Materialize.toast(
	                    'Ops, houve um erro interno',
	                    5000, 'rounded');
	            },
	            400: function () {
	                Materialize
	                    .toast(
	                        'Você deve estar fazendo algo de errado',
	                        5000, 'rounded');
	            },
	            404: function () {
	                Materialize.toast('Url não encontrada',
	                    5000, 'rounded');
	            }
	        }	
		});
			
})

function filtroTabela(idInput) {
    $(idInput).on('keyup', function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-consultor').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(2)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}
//##1 Consertar filtro de cargo
//document.querySelector('option[value=" + value +"]').selected = true
function filtroTabelaCargo(idInput) {
	
    $(idInput).on('change', function () {
	var cargos = $('#cargos option:selected').text();
		
		console.log(cargos)
		console.log(this)
		
        var nomeFiltro = $(this).val().toLowerCase();
        $('#tabela-consultor').find('tbody tr').each(function () {
            var conteudoCelula = $(this).find('td:nth-child(3)').text();
            var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).css('display', corresponde ? '' : 'none');
        });
    });
}

$('#lerExcelFuncionarios').click(function(){
	console.log('click')
	

		
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/premios/lerExcel",
			dataType:"JSON",
	        statusCode: {
	            200: function () {
	                Materialize
	                    .toast(
	                        'Itens salvos com sucesso!',
	                        5000, 'rounded');
	            },
	            500: function () {
	                Materialize.toast(
	                    'Ops, houve um erro interno',
	                    5000, 'rounded');
	            },
	            400: function () {
	                Materialize
	                    .toast(
	                        'Você deve estar fazendo algo de errado',
	                        5000, 'rounded');
	            },
	            404: function () {
	                Materialize.toast('Url não encontrada',
	                    5000, 'rounded');
	            }
	        }
		});
			
})
