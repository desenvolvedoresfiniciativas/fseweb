$(document).on("click",".validar",function() {
	 var botao = $(this);

		id = botao.val();
		console.log('o id é: ' +id)
		
		var url = "/comissoes/validar/" + id

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: url,
		dataType: 'JSON',
		statusCode: {
			200: function() {
				Materialize.toast('Comissão validada com sucesso !', 5000, 'rounded');
				
			},
			500: function() {
				Materialize
					.toast('Ops, houve um erro interno', 5000, 'rounded');
			},
			400: function() {
				Materialize.toast('Você deve estar fazendo algo de errado',
					5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada', 5000, 'rounded');
			}
		}
	});
	})
	
$(function calculaImpostos() {
	
	var valorBruto = ($("#idValorEtapa").text());
	console.log(valorBruto, " -------valor bruto/etapa recebido");
	valorBruto = valorBruto.replaceAll("R", "")
	valorBruto = valorBruto.replaceAll("$", "")
	valorBruto = valorBruto.replaceAll(" ", "")
	valorBruto = valorBruto.replaceAll(".", "")
	valorBruto = valorBruto.replaceAll(",", ".")
	console.log(valorBruto, " -------valor bruto/etapa numerico");
	valorBruto = parseFloat(valorBruto)

	var IRRF = valorBruto * (1.5 / 100)
	var CSSL = valorBruto * (1 / 100)
	var PIS = valorBruto * (0.65 / 100)
	var COFINS = valorBruto * (3 / 100)
	
	IRRF = parseFloat(IRRF).toFixed(2)
	CSSL = parseFloat(CSSL).toFixed(2)
	PIS = parseFloat(PIS).toFixed(2)
	COFINS = parseFloat(COFINS).toFixed(2)
	
	if(Number(IRRF) < 10.0) {
		IRRF = 0.0;
		IRRF = parseFloat(IRRF).toFixed(2)
	}
	
	if((Number(CSSL) + Number(PIS) + Number(COFINS)) < 10.0) {
		CSSL = 0.0;
		PIS = 0.0;
		COFINS = 0.0;
		CSSL = parseFloat(CSSL).toFixed(2)
		PIS = parseFloat(PIS).toFixed(2)
		COFINS = parseFloat(COFINS).toFixed(2)
	}
	
	var total = Number(IRRF) + Number(CSSL) + Number(PIS) + Number(COFINS);
	var valorLiquido = valorBruto - total;
	
	console.log(total,valorLiquido + "-------")
	
	total = parseFloat(total).toFixed(2)
	valorLiquido = parseFloat(valorLiquido).toFixed(2)
	
	console.log(total,valorLiquido)
	
	$("#idIRRF").text("R$ " + mascaraValor(IRRF))
	$("#idCSSL").text("R$ " + mascaraValor(CSSL))
	$("#idPIS").text("R$ " + mascaraValor(PIS))
	$("#idCOFINS").text("R$ " + mascaraValor(COFINS))
	$("#idTotal").text("R$ " + mascaraValor(total))
	$("#idValorLiquido").text("R$ " + mascaraValor(valorLiquido))
})

/*$(function calculaImpostosDevidosEmpresa() {
	
	var valorBruto = ($("#idValorEtapa").text());
	console.log(valorBruto, " -------valor bruto/etapa recebido");
	valorBruto = valorBruto.replaceAll("R", "")
	valorBruto = valorBruto.replaceAll("$", "")
	valorBruto = valorBruto.replaceAll(" ", "")
	valorBruto = valorBruto.replaceAll(".", "")
	valorBruto = valorBruto.replaceAll(",", ".")
	console.log(valorBruto, " -------valor bruto/etapa numerico");
	valorBruto = parseFloat(valorBruto)

	var PISImpostosDevidos = valorBruto * (1.65 / 100)
	var COFINSImpostosDevidos = valorBruto * (7.6 / 100)
	var ISSImpostosDevidos = valorBruto * (3 / 100)
	//abaixo está com valor de 0
	var ISSVariavelImpostosDevidos = valorBruto * (0 / 100)
	var totalImpostosDevidos = PISImpostosDevidos + COFINSImpostosDevidos + ISSImpostosDevidos + ISSVariavelImpostosDevidos;
	
	PISImpostosDevidos = parseFloat(PISImpostosDevidos).toFixed(2)
	COFINSImpostosDevidos = parseFloat(COFINSImpostosDevidos).toFixed(2)
	ISSImpostosDevidos = parseFloat(ISSImpostosDevidos).toFixed(2)
	ISSVariavelImpostosDevidos = parseFloat(ISSVariavelImpostosDevidos).toFixed(2)
	totalImpostosDevidos = parseFloat(totalImpostosDevidos).toFixed(2)

	$("#idPISImpostosDevidos").text('R$ ' + mascaraValor(PISImpostosDevidos))
	$("#idCOFINSImpostosDevidos").text('R$ ' + mascaraValor(COFINSImpostosDevidos))
	$("#idISSImpostosDevidos").text('R$ ' + mascaraValor(ISSImpostosDevidos))
	$("#idISSVariavelImpostosDevidos").text('R$ ' + mascaraValor(ISSVariavelImpostosDevidos))

	$("#idTotalImpostosDevidos").text('R$ ' + mascaraValor(totalImpostosDevidos))
//	$("#idValorLiquido").text(valorLiquido)
})*/

$(function carregaMemoriaDeCalculo() {
	
	var fatId = $("#idFaturamento").text();	
	var url = "/faturamentos/" + fatId + "/fat.json";

	$.ajax({
		type : 'GET',
		url : url,
		async : false,
		success : function(data) {
			console.log("entrou no ajax novo")

			var i = 0
			
			console.log(data)
			var valorBeneficio = 0.0;
			
			if(data.consolidado == true) {
				simularConsolidacao();
				/*var url = 
				$.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {
						
					}
					})*/
			
			} else {
				$.each($('.valorBeneficio'), function () {
				var valorAtual = $(this).text().replaceAll(".", "");
				valorAtual = valorAtual.replace(",", ".");
				valorAtual = Number(valorAtual);
				
				valorBeneficio = valorBeneficio + valorAtual
				console.log(valorBeneficio, " vlbenef atual")
				})
			}
			// trazer unicamente o balancete que está no registro do fat (balanceteId)
			//se for consolidado, trago a soma da redução dos ids que estiverem na tabela if consolidado is true, trazer ids fats consolidados
			//tratar essa string para trazer cada balancete a ser considerado na soma do beneficio
			/*$.each($('.valorBeneficio'), function () {
				var valorAtual = $(this).text().replaceAll(".", "");
				valorAtual = valorAtual.replace(",", ".");
				valorAtual = Number(valorAtual);
				
				valorBeneficio = valorBeneficio + valorAtual
				console.log(valorBeneficio, " vlbenef atual")
				})*/
			console.table('o tamanho do objeto é: ' + data.contrato.honorarios.length)
			for(i; i <= data.contrato.honorarios.length; i++) {
				
				var tipoDeHonorario;
				console.log(data.contrato.id)
				switch (data.contrato.honorarios[i].nome) {

				case "Percentual Escalonado":	
				
				console.log('entrou no percentual escalonado')
				tipoDeHonorario = "PERCENTUAL ESCALONADO";
				var faixas = data.contrato.honorarios[i].faixasEscalonamento;
				var index = 0
				if (data.consolidado == true) {
					console.log($('#idBeneficioTotalSomatoriaReducaoBalancetes').text());
					var temp = parseFloat($('#idBeneficioTotalSomatoriaReducaoBalancetes').text()).toFixed(2);
				} else {
					var temp = ($('#idReducaoImposto').text());
					temp = temp.replaceAll('R','')
					temp = temp.replaceAll('$','')
					temp = temp.replaceAll(' ','')
					temp = temp.replaceAll('.','')
					temp = temp.replaceAll(',','.')
					
					temp = parseFloat(temp).toFixed(2);
				}
				console.log(temp)

				var valorCalc = 0;
				var valorAplicado = 0;
				var valorTotalDosHonorarios = 0;
				var valorTotalAplicado = 0;
				
				for(var index in faixas) {
					console.log("entrou no for das faixas")

				var inicial = faixas[index].valorInicial;
				inicial = Number(Math.floor(inicial, -1).toFixed(2))
				inicial = parseFloat(inicial).toFixed(2)
				var finalF = faixas[index].valorFinal;
				finalF = Number(Math.floor(finalF, -1).toFixed(2))
				finalF = parseFloat(finalF).toFixed(2)
				var gap1 = finalF - inicial;
				var percentual = faixas[index].valorPercentual / 100;

				if (temp > 0) {

					if (temp >= gap1) {
						valorCalc = (gap1 * percentual);
						temp = temp - (gap1);
						valorAplicado = gap1;
						valorAplicado = parseFloat(valorAplicado)
						valorTotalDosHonorarios = valorTotalDosHonorarios + valorCalc
						valorTotalAplicado += valorAplicado
						} else if (temp < gap1) {
							console.log("Será calculado dentro dentro da faixa com o valor restante do temp...");
							valorCalc = (temp * percentual);
							valorAplicado = temp;
							valorAplicado = parseFloat(valorAplicado)
							valorTotalDosHonorarios = valorTotalDosHonorarios + valorCalc
							temp = temp - (gap1);
							valorTotalAplicado += valorAplicado
							valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						}
					} else {
						valorCalc = 0;
						valorAplicado = 0;
						valorTotalDosHonorarios = valorTotalDosHonorarios + valorCalc;
						valorTotalAplicado += valorAplicado
					}

				var tr = $("<tr>")
				var tds = $("<td>De R$ "+mascaraValor(inicial)+" à R$ "+mascaraValor(finalF)+"</td>" +
				"<td>R$ "+mascaraValor(valorAplicado.toFixed(2))+"</td>" +
				"<td>"+faixas[index].valorPercentual+"%</td>"+
				"<td>R$ "+mascaraValor(valorCalc.toFixed(2))+"</td>")
				tr.append(tds)
				
				$("#tabelaEscalonado").append(tr)

				index++
				}
				
				valorTotalDosHonorarios = parseFloat(valorTotalDosHonorarios).toFixed(2)
				
				if(temp > 0 || limitacao > 0) {
					console.log("temp > 0 || limitacao > 0")
					var valorAcimaDe = Number(data.contrato.honorarios[i].acimaDe)
					valorAcimaDe = Number(Math.floor(valorAcimaDe, -1).toFixed(2))
					valorAcimaDe = parseFloat(valorAcimaDe).toFixed(2)
					var percentualAcimaDe = Number((data.contrato.honorarios[i].percentAcimaDe) / 100);
					var honorarioAcimaDe = Number(percentualAcimaDe) * Number(temp);
					var limitacao = Number(data.contrato.honorarios[i].limitacao);
					limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
					limitacao = parseFloat(limitacao).toFixed(2)
					console.log(data.contrato.honorarios[i].limitacao);
					var valorAplicadoAcimaDe = 0;
//					var valorTotalDosHonorariosAcimaDe = valorTotalDosHonorarios;

					if (limitacao > 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(valorAcimaDe.toFixed(2)))+"</td>" +
						"<td>R$ "+(mascaraValor(valorAplicadoAcimaDe.toFixed(2)))+"</td>" +
						"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
						"<td>R$ "+(limitacao.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						.append(trAcimaDe)
						valorTotalDosHonorarios = Number(valorTotalDosHonorarios) + Number(parseFloat(limitacao).toFixed(2))
						valorTotalAplicado += valorAplicadoAcimaDe
						valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						$("#tabelaEscalonado").append(trAcimaDe)
					} else if (temp > 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(valorAcimaDe))+"</td>" +
						"<td>R$ "+mascaraValor(temp.toFixed(2))+"</td>" +
						"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
						"<td>R$ "+mascaraValor(honorarioAcimaDe.toFixed(2))+"</td>")
						trAcimaDe.append(tdsAcimaDe)
						.append(trAcimaDe)
						valorTotalDosHonorarios = Number(parseFloat(valorTotalDosHonorarios)) + Number(parseFloat(honorarioAcimaDe))
						valorTotalDosHonorarios = parseFloat(valorTotalDosHonorarios).toFixed(2)
						
						valorAplicadoAcimaDe = parseFloat(temp).toFixed(2)
						valorTotalAplicado = Number(parseFloat(valorTotalAplicado)) + Number(parseFloat(valorAplicadoAcimaDe))
						valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						$("#tabelaEscalonado").append(trAcimaDe)
					} else if (temp <= 0) {
						var trAcimaDe = $("<tr>")
						var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(parseFloat(valorAcimaDe).toFixed(2)))+"</td>" +
						"<td>R$ 0,00</td>" +
						"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
						"<td>R$ 0,00</td>")
						trAcimaDe.append(tdsAcimaDe)
						.append(trAcimaDe)
						valorTotalAplicado += valorAplicadoAcimaDe
						valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
						$("#tabelaEscalonado").append(trAcimaDe)
					}
				} else {
					console.log("else")
					var valorAcimaDe = Number(data.contrato.honorarios[i].acimaDe)
					valorAcimaDe = Number(Math.floor(valorAcimaDe, -1).toFixed(2))
					valorAcimaDe = parseFloat(valorAcimaDe).toFixed(2)
					var percentualAcimaDe = Number((data.contrato.honorarios[i].percentAcimaDe) / 100);
					var honorarioAcimaDe = Number(percentualAcimaDe) * Number(temp);
					var limitacao = Number(data.contrato.honorarios[i].limitacao);
					limitacao = Number(Math.floor(limitacao, -1).toFixed(2))
					limitacao = parseFloat(limitacao).toFixed(2)
					console.log(data.contrato.honorarios[i].limitacao);
					var valorAplicadoAcimaDe = 0;
					
					var trAcimaDe = $("<tr>")
					var tdsAcimaDe = $("<td>Acima de R$ "+(mascaraValor(parseFloat(valorAcimaDe).toFixed(2)))+"</td>" +
					"<td>R$ 0,00</td>" +
					"<td>"+data.contrato.honorarios[i].percentAcimaDe+"%</td>"+
					"<td>R$ 0,00</td>")
					trAcimaDe.append(tdsAcimaDe)
					.append(trAcimaDe)
					valorTotalAplicado += valorAplicadoAcimaDe
					valorTotalAplicado = parseFloat(valorTotalAplicado).toFixed(2)
					$("#tabelaEscalonado").append(trAcimaDe)
				}
				
				$("#totalHonorarios").append("Total: R$ " + mascaraValor(valorTotalDosHonorarios));
				$("#totalHonorariosAplicados").append("Total Aplicado: R$ " + mascaraValor(valorTotalAplicado));
				$("#totalAFaturar").append("R$ " + mascaraValor(valorTotalDosHonorarios))
				exibeValor();

				break;

				case "Valor Fixo":
				tipoDeHonorario = "VALOR FIXO";
				var valorFixo = Number(data.contrato.honorarios[i].valor);
				$("#divCompleta").append("<b>Tipo de Honorário: </b> <p style='display:inline'>"+tipoDeHonorario+"</p><br />")
				$("#divCompleta").append("<b>Valor Fixo: </b> <p style='display:inline'>R$ "+mascaraValor(valorFixo.toFixed(2))+"</p><br /><hr />")
				exibeValor();
				
				break;

				case "Percentual Fixo":
				tipoDeHonorario = "PERCENTUAL FIXO";
				
				var valorAplicado = valorBeneficio;
				console.log(valorAplicado, " case percentual fixo ")
				valorAplicado = parseFloat(valorAplicado).toFixed(2)
				console.log(valorAplicado, " case percentual fixo com o tofixed2")
				valorAplicado = parseFloat(valorAplicado)
				var valorTotalDosHonorarios = 0;
				
				console.log(valorAplicado, " ++++++++++++++++++++++++")
				
				var percentualFixo = Number(data.contrato.honorarios[i].valorPercentual / 100);
				valorTotalDosHonorarios = percentualFixo * valorAplicado;
				valorTotalDosHonorarios = parseFloat(valorTotalDosHonorarios).toFixed(2)

				$("#idPercentualFixoHonorarioValorAplicado").text("R$ " + mascaraValor(valorAplicado))
				$("#idPercentualFixoHonorario").text("R$ " + mascaraValor(valorTotalDosHonorarios))
				$("#totalAFaturar").append("R$ " + mascaraValor(valorTotalDosHonorarios))
				exibeValor();
				
				break;
				
				default:
				$("#nfTipoDeHonorario").text("Sem Memória de Cálculo")
				break;
				}
			};
		}
	});
})

function exibeValor() {
	var valorFormatado = $("#idValorEtapa").text();
	console.log(mascaraValor(valorFormatado));
	$("#idValorEtapa").text("R$ " + mascaraValor(valorFormatado));
}