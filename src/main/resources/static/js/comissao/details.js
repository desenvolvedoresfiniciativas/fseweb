$(function(){
	$(".select").material_select();
	$('.modal').modal();
	$('.modalD').modal();
	$(".openModal").material_select();
	calculaIndicadores()
	filtroTabelaProducao();
})

function filtroTabelaProducao() {
	var nomeFiltro = $("#select-ano :selected").val()
	console.log(nomeFiltro);
	$('#tbody-comissoes').find('tbody tr').each(
			function() {
				var conteudoCelula = $(this)
						.find('td:nth-child(1)').text();
				console.log(conteudoCelula);
				var corresponde = conteudoCelula
						.indexOf(nomeFiltro) >= 0;
				$(this).css('display', corresponde ? '' : 'none');
			});
	$("#select-ano").change(
			function() {
				//var nomeFiltro = $("#select-ano :selected").val()
				//console.log(nomeFiltro);
				
				$('#tbody-comissoes').find('tbody tr').each(
					function() {
						var conteudoCelula = $(this).find('td:nth-child(1)').text();
						console.log(conteudoCelula);
						var corresponde = conteudoCelula.indexOf(nomeFiltro) >= 0;
						$(this).css('display', corresponde ? '' : 'none');
				});
			});
}

function fnExcelReport() {
    var tab_text = '\uFEFF<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Comissoes /x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + $('#tabela-comissoes').html();
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8"
            });
            navigator.msSaveBlob(blob, 'comissoes.xls');
        }
    } else {
        $('#excelBtn').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
        $('#excelBtn').attr('download', 'comissoes.xls');
    }
}

$('#selectProduto').change(
		function() {

			var prodId = $('#selectProduto :selected').val();
			console.log(prodId)

			if (prodId == "") {

				$('#select-etapa').attr("readonly", 'readonly');

				$('#select-etapa').find('option').remove().end().append(
						'<option value="" selected="selected">Selecione</option>').val(
						'whatever');

				$('#select-etapa').material_select();

			} else {

				var url = "/produtos/" + prodId + "/etapas.json"

				var options = "<option selected='selected' value=''>Selecione</option>";

				return $.ajax({
					type : 'GET',
					url : url,
					async : false,
					success : function(data) {

						$(data).each(
								function(i) {
									var etapa = data[i];
									options += "<option value='" + etapa.id
											+ "'>" + etapa.nome + "</option>"
								});

						$("#select-etapa").empty();
						$("#select-etapa").append(options);
						$('#select-etapa').removeAttr('disabled');
						$("#select-etapa").material_select();

					}
				});
			}
		});
		
$('#atualizaComissoes').click(
	function() {
			var idConsultor = $('#consultorId').val();
			console.log(idConsultor)
			
			$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/comissoes/calculaComissaoByConsultor/" + idConsultor,
			dataType:"JSON",
	        statusCode: {
	            200: function () {
	                Materialize
	                    .toast(
	                        'Itens salvos com sucesso!',
	                        5000, 'rounded');
	            },
	            500: function () {
	                Materialize.toast(
	                    'Ops, houve um erro interno',
	                    5000, 'rounded');
	            },
	            400: function () {
	                Materialize
	                    .toast(
	                        'Você deve estar fazendo algo de errado',
	                        5000, 'rounded');
	            },
	            404: function () {
	                Materialize.toast('Url não encontrada',
	                    5000, 'rounded');
	            }
	        }	
		});
	});		


$(document).on("click",".validar",function() {
	 var botao = $(this);

		id = botao.val();
		console.log('o id é: ' +id)
		
		var url = "/comissoes/validar/" + id

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: url,
		dataType: 'JSON',
		statusCode: {
			200: function() {
				Materialize.toast('Comissão validada com sucesso !', 5000, 'rounded');
				
			},
			500: function() {
				Materialize
					.toast('Ops, houve um erro interno', 5000, 'rounded');
			},
			400: function() {
				Materialize.toast('Você deve estar fazendo algo de errado',
					5000, 'rounded');
			},
			404: function() {
				Materialize.toast('Url não encontrada', 5000, 'rounded');
			}
		}
	});
	})
	

function calculaIndicadores() {
	

	var consultor = $("#consultorId").val();
	var receber = 0.0;
	var recebido = 0.0;
	var simulado = 0.0;
	var real = 0.0;
	
	$("#countReceber").text(mascaraValor(receber))
    $("#countRecebido").text(mascaraValor(recebido))
    $("#countSimulado").text(mascaraValor(simulado))
    $("#countReal").text(mascaraValor(real))
	
	console.log('o consultor a ser pesquisado é: ' + consultor)
	
	var url = "/comissoes" +"/" + consultor + "/comissoesAtivas.json"
	
	console.log(url)
	
    $.get(url, function (data) {

	console.log('o tamanho do objeto é: ' + data.length)
    	$(data).each(function () {
	
	console.log(data)

            var comissao = this;
			var receberAtual
			var recebidoAtual
			var simuladoAtual
			var realAtual
			
			console.log("essa comissão é simulada?" + comissao.comissaoSimulada)
			console.log('o objeto é:', comissao)
			console.log(comissao.comissaoValor + " -----")

            receberAtual = (comissao.comissaoValor).replaceAll(".", "");
			receberAtual = receberAtual.replaceAll(",",".")
			receberAtual = Number(parseFloat(receberAtual).toFixed(2))
			receber += parseFloat(receberAtual)

            if(comissao.pagamento === true) {
				recebidoAtual = (comissao.comissaoValor).replaceAll(".", "");
				recebidoAtual = recebidoAtual.replaceAll(",",".")
				recebidoAtual = Number(parseFloat(recebidoAtual).toFixed(2))
				recebido += parseFloat(recebidoAtual)
            }
            
            if(comissao.comissaoSimulada === true) {
				simuladoAtual = (comissao.comissaoValor).replaceAll(".", "");
				simuladoAtual = simuladoAtual.replaceAll(",",".")
				simuladoAtual = Number(parseFloat(simuladoAtual).toFixed(2))
				simulado += parseFloat(simuladoAtual)
            }
            
            if(comissao.comissaoSimulada === false) {
				realAtual = (comissao.comissaoValor).replaceAll(".", "");
				realAtual = realAtual.replaceAll(",",".")
				realAtual = Number(parseFloat(realAtual).toFixed(2))
				real += parseFloat(realAtual)
            }

            
          //  $("#countReceber").text('R$ '+ mascaraValor(parseFloat(receber).toFixed(2))
           // $("#countRecebido").text('R$ '+ mascaraValor(parseFloat(recebido).toFixed(2))
            //$("#countSimulado").text('R$ '+mascaraValor(parseFloat(simulado).toFixed(2))
           // $("#countReal").text('R$ '+ mascaraValor(parseFloat(real).toFixed(2))
    		

    });

	receber = (receber).toFixed(2)
	recebido = (recebido).toFixed(2)
	simulado = (simulado).toFixed(2)
	real = (real).toFixed(2)

	$("#countReceber").text('R$ '+ mascaraValor(receber))
    $("#countRecebido").text('R$ '+ mascaraValor(recebido))
    $("#countSimulado").text('R$ '+ mascaraValor(simulado))
    $("#countReal").text('R$ '+ mascaraValor(real))
});
}

function mascaraValor(valor) {
	valor = valor.toString().replace(/\D/g, "");
	valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
	valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
	return valor
}
