package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Categoria;
import br.com.finiciativas.fseweb.enums.CategoriaProblematica;
import br.com.finiciativas.fseweb.enums.Divisao;
import br.com.finiciativas.fseweb.enums.MotivoFidelizacao;
import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.TipoEndereco;
import br.com.finiciativas.fseweb.enums.TipoTelefone;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.services.ClienteService;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EnderecoClienteServiceImpl;

@Controller
@RequestMapping(path = "/clientes")
public class ClienteController {

	@Autowired
	private ClienteServiceImpl clienteService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private EnderecoClienteServiceImpl enderecoClienteService;

	@Autowired
	private ContatoClienteServiceImpl ContatoClienteService;
	

	@GetMapping("/form")
	public ModelAndView form() {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("divisao",Divisao.values());
		modelAndView.addObject("cargos", Cargo.values());
		modelAndView.addObject("tipoTelefone", TipoTelefone.values());
		modelAndView.addObject("setorAtividade", SetorAtividade.values());
		modelAndView.addObject("categoriaProblematica", CategoriaProblematica.values());
		modelAndView.addObject("categoria", Categoria.values());
		modelAndView.addObject("consultoresComerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		modelAndView.addObject("tipo", TipoEndereco.values());
		modelAndView.addObject("motivoFidelizacao", MotivoFidelizacao.values());
		modelAndView.setViewName("/cliente/form");

		return modelAndView;
	}

	@GetMapping("/all")
	public ModelAndView getAll() {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("clientes", clienteService.getAll());
		modelAndView.setViewName("/cliente/list");

		return modelAndView;
	}

	@Transactional
	@PostMapping("/add")
	public ModelAndView add(@Valid Cliente empresa,
			@RequestParam(value = "cep") String cep,
			@RequestParam(value = "logradouro") String logradouro,
			@RequestParam(value = "numero") String numero,
			@RequestParam(value = "complemento", required = false) String complemento,
			@RequestParam(value = "tipoEnder", required = false) String tipo,
			@RequestParam(value = "bairro") String bairro,
			@RequestParam(value = "cidade") String cidade,
			@RequestParam(value = "estado") String estado,
			@RequestParam(value = "enderecoNotaFiscalBoleto", required = false) Boolean enderecoNotaFiscalBoleto,
			@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "cargo", required = false) String cargo,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "telefone1", required = false) String telefone1,
			@RequestParam(value = "telefone2", required = false) String telefone2,
			@RequestParam(value = "nfBoleto", required = false) Boolean nfBoleto,
			@RequestParam(value = "novosContatos", required = false) Boolean novosContatos,
			@RequestParam(value = "fidelizado", required = false) Boolean fidelizado,
			@RequestParam(value = "dataFidelizacao", required = false) String dataFidelizacao,
			@RequestParam(value = "dataVencimentoFidelizacao", required = false) String dataVencimentoFidelizacao,
			@RequestParam(value = "motivoFidelizacao", required = false) MotivoFidelizacao motivoFidelizacao) {
		
		System.out.println("Chegou");
		System.out.println(empresa.getRut());
		System.out.println(fidelizado);
		System.out.println(dataVencimentoFidelizacao);
		System.out.println(fidelizado);
		
		ModelAndView modelAndView = new ModelAndView();

		if(empresa.getCnpj()!=null) {
		empresa.setCnpj(empresa.getCnpj().replaceAll("[^a-zA-Z0-9]+",""));
		}
		
		if(empresa.getRut()!=null){
		empresa.setRut(empresa.getRut().replaceAll("[^a-zA-Z0-9]+",""));
		}
		
		if(novosContatos==null){novosContatos=false;}
		if(nfBoleto==null){nfBoleto=false;}
		if(fidelizado==null){fidelizado=false;}
		
		Cliente cliente = clienteService.create(empresa);
		
		ContatoCliente contato = new ContatoCliente();
		
		contato.setAtivo(true);
		contato.setNome(nome);
		contato.setCargo(cargo);
		contato.setEmail(email);
		contato.setTelefone1(telefone1);
		contato.setTelefone2(telefone2);
		contato.setPessoaNfBoleto(nfBoleto);
		contato.setReferencia(novosContatos);
		contato.setPesquisaSatisfacao(fidelizado);
		
		ContatoClienteService.create(contato, cliente.getId());
		
		EnderecoCliente endereco = new EnderecoCliente();
		
		if(enderecoNotaFiscalBoleto == null) {
			enderecoNotaFiscalBoleto = false;
		} else {
			enderecoNotaFiscalBoleto = true;
		}
		
		if(nfBoleto == null) {
			nfBoleto = false;
		} else {
			nfBoleto = true;
		}
		
		if(novosContatos == null) {
			novosContatos = false;
		} else {
			novosContatos = true;
		}
		
		if(fidelizado == null) {
			fidelizado = false;
		} else {
			fidelizado = true;
			empresa.setMotivoFidelizacao(motivoFidelizacao);
			empresa.setDataFidelizacao(dataFidelizacao);
			empresa.setDataVencimentoFidelizacao(dataVencimentoFidelizacao);
		}
		
		endereco.setCep(cep);
		endereco.setLogradouro(logradouro);
		endereco.setNumero(numero);
		endereco.setComplemento(complemento);
		endereco.setTipo(tipo);
		endereco.setBairro(bairro);
		endereco.setCidade(cidade);
		endereco.setEstado(estado);
		endereco.setCliente(cliente);
		endereco.setEnderecoNotaFiscalBoleto(enderecoNotaFiscalBoleto);
		
		enderecoClienteService.create(endereco);

		modelAndView.setViewName("redirect:/clientes/details/"+cliente.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		clienteService.delete(id);

		modelAndView.setViewName("redirect:/clientes/all");

		return modelAndView;

	}

	@Transactional
	@PostMapping("/update")
	public ModelAndView update(@Valid Cliente empresa) {

		ModelAndView modelAndView = new ModelAndView();
		
		System.out.println("Is clean"+empresa.isClean());
		
		if(empresa.getCnpj()!=null) {
			empresa.setCnpj(empresa.getCnpj().replaceAll("[^a-zA-Z0-9]+",""));
		} else if(empresa.getRut()!=null){
			empresa.setRut(empresa.getRut().replaceAll("[^a-zA-Z0-9]+",""));
		}

		Cliente empresaAtt = clienteService.update(empresa);
		
		//modelAndView.addObject("dataFidelizacao", empresaAtt.getDataFidelizacao());
		modelAndView.setViewName("redirect:/clientes/" + empresaAtt.getId());
		
		modelAndView.addObject("tipo", TipoEndereco.values());
		modelAndView.addObject("contato", empresaAtt.getContatos());
		

		return modelAndView;
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Cliente cliente = clienteService.findById(id);

		modelAndView.addObject("cliente", cliente);
		modelAndView.addObject("enderecos", enderecoClienteService.getAllById(id));
		modelAndView.addObject("contatos", ContatoClienteService.getAllById(id));
		modelAndView.addObject("cargos", Cargo.values());
		modelAndView.addObject("divisao", Divisao.values());
		modelAndView.addObject("tipoTelefone", TipoTelefone.values());
		modelAndView.addObject("setorAtividade", SetorAtividade.values());
		modelAndView.addObject("categoriaProblematica", CategoriaProblematica.values());
		modelAndView.addObject("consultoresComerciais",
				consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		modelAndView.addObject("categoria", Categoria.values());
		modelAndView.addObject("motivoFidelizacao", MotivoFidelizacao.values());

		modelAndView.addObject("dataFidelizacao", cliente.getDataFidelizacao());
		modelAndView.addObject("dataVencimentoFidelizacao", cliente.getDataVencimentoFidelizacao());
		
		modelAndView.setViewName("/cliente/update");

		return modelAndView;
	}

	@GetMapping("/details/{id}")
	public ModelAndView details(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Cliente cliente = clienteService.loadFullEmpresa(id);
		
		List<EnderecoCliente> enderecos = enderecoClienteService.getAllById(id);
		System.out.println(enderecos.size());

		modelAndView.addObject("cliente", cliente);
		modelAndView.addObject("enderecos", enderecoClienteService.getAllById(id));
		modelAndView.addObject("contatos", ContatoClienteService.getAllById(id));

		modelAndView.setViewName("/cliente/details");

		return modelAndView;
	}

	@GetMapping("/{cnpj}.json")
	public @ResponseBody Cliente clienteJson(@PathVariable("cnpj") String cnpj) {
		
		System.out.println(cnpj);

		Cliente cliente = this.clienteService.findByCnpj(cnpj);

		//cliente.setComercialResponsavel(new ConsultorPresenter(cliente.getComercialResponsavel()).converter());

		return cliente;
	}
	@GetMapping("/nome/{id}.json")
	public @ResponseBody Cliente buscaClientePorIdJson(@PathVariable("id") Long id) {
		
		System.out.println(id);

		Cliente clienteNome = this.clienteService.findById(id);
		
		System.out.println(clienteNome.getCnpj());
		//cliente.setComercialResponsavel(new ConsultorPresenter(cliente.getComercialResponsavel()).converter());

		return clienteNome;
	}

	@PostMapping("/enderecoAdd")
	@ResponseBody
	public void enderAdd(@RequestBody EnderecoCliente endereco) {
		enderecoClienteService.create(endereco);
	}

	@GetMapping("/all.json")
	public @ResponseBody List<Cliente> empresasJson() {

		return clienteService.getAll();

	}

	@GetMapping("/enderecoform/{id}")
	public ModelAndView enderForm(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("cliente", clienteService.findById(id));
		modelAndView.addObject("tipo", TipoEndereco.values());

		modelAndView.setViewName("/cliente/endereco/form");

		return modelAndView;
	}

	@Transactional
	@PostMapping("/enderecoFormAdd")
	public ModelAndView enderecoAdd(@Valid EnderecoCliente ender) {

		ModelAndView modelAndView = new ModelAndView();

		enderecoClienteService.create(ender);

		modelAndView.setViewName("redirect:/clientes/"+ender.getCliente().getId());

		return modelAndView;

	}

	@Transactional
	@RequestMapping("/deleteEndereco/{id}")
	public ModelAndView deleteEnder(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		EnderecoCliente ender = enderecoClienteService.findById(id);
		enderecoClienteService.delete(ender);
		modelAndView.setViewName("redirect:/clientes/" + ender.getCliente().getId());

		return modelAndView;

	}
	
	@Transactional
	@RequestMapping(value = "/updateEndereco/{id}")
	public ModelAndView updateEnder(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("tipo", TipoEndereco.values());
		modelAndView.addObject("ender", enderecoClienteService.findById(id));
		modelAndView.setViewName("/cliente/endereco/update");

		return modelAndView;
	}
	
	@Transactional
	@PostMapping("/updateAtualizar")
	public ModelAndView atualizarEndereco(@Valid EnderecoCliente enderecoCliente, @RequestParam("clienteId") Long id) {
		ModelAndView mav = new ModelAndView();
		
		System.out.println(id);
		
		System.out.println(enderecoCliente.isEnderecoNotaFiscalBoleto());

		Cliente cliente = clienteService.findById(id);
		enderecoCliente.setCliente(cliente);
		enderecoClienteService.update(enderecoCliente);
		mav.setViewName("redirect:/clientes/" + enderecoCliente.getCliente().getId());
		return mav;
	}
	
	@Transactional
	@RequestMapping("/deleteContato/{id}")
	public ModelAndView deleteContato(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		ContatoCliente contatoEmpresa = ContatoClienteService.findById(id);
		ContatoClienteService.remove(contatoEmpresa);
		
		
		modelAndView.setViewName("redirect:/clientes/" + contatoEmpresa.getCliente().getId());

		return modelAndView;

	}
	
	//@GetMapping("/{id}/.json")
	@RequestMapping(value = "/{id}/.json", method = RequestMethod.PUT)
	
	public @ResponseBody ContatoCliente updateIsReferenciaById(@PathVariable("id") Long id) {
		System.out.println("ID " + id + " recebido");
		//Buscar o contato do cliente e o cliente atrav�s do id enviado via ajax
		ContatoCliente contato = ContatoClienteService.findById(id);
		Cliente cliente = contato.getCliente();
		
		//Atualizar informa��o de que o cliente � fidelizado
		cliente.setFidelizado(true);
		cliente.setMotivoFidelizacao(MotivoFidelizacao.REFERENCIA);
		clienteService.update(cliente);
		
		
		System.out.println(contato.isReferencia());
		//Atualizar a informa��o de que o contato � uma refer�ncia
		contato.setReferencia(true);
		ContatoClienteService.update(contato);
		System.out.println(contato.isReferencia());
		 return ContatoClienteService.findById(id);
	}
	
	  @PostMapping("/deleteCliente/{id}")
	   public ModelAndView deleteCliente(@PathVariable("id") Long id) {
		  System.out.println("entrou no metodo deleteCliente");
		  System.out.println("IdCliente"+ id);
	  ModelAndView modelAndView = new ModelAndView();
	  
	  Cliente cliente = clienteService.findById(id); 
	  cliente.setAtivo(false);
	  clienteService.update(cliente);
	  
	  modelAndView.setViewName("redirect:/clientes/all");
	 
	  return modelAndView; 
	  }

	  @GetMapping("/{idCliente}/enderecoCliente.json")
		public @ResponseBody  List<EnderecoCliente> getEnderecoClienteNF(@PathVariable("idCliente") Long idCliente) {
			List <EnderecoCliente> enderecoCliente = enderecoClienteService.getAllById(idCliente);
			
			return enderecoCliente;
		} 
		 
	@Transactional
	@PostMapping("/atualizarPropriedadesPorContrato")
	public ModelAndView atualizarPropriedadesPorContrato(@RequestBody Cliente cliente) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		if(cliente != null) {
			Cliente clienteAtt = clienteService.findById(cliente.getId());
			String cnpj = cliente.getCnpj().replaceAll("[^a-zA-Z0-9]+","");	
			clienteAtt.setCnpj(cnpj);
			clienteAtt.setRazaoSocial(cliente.getRazaoSocial());
		}
		
		modelAndView.setViewName("redirect:/contratos/all");
		
		return modelAndView; 
	}
	
	@GetMapping("/filtroClientes")
	public ModelAndView filtroClientes(@RequestParam(value = "empresa", required = false) String empresa,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger) {

		System.out.println("empresa" + empresa);

		ModelAndView mav = new ModelAndView();

		if (queryTrigger != null) {
			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}

			if (queryTrigger.equalsIgnoreCase("true")) {

				List<Cliente> cliente = clienteService.getFiltroCliente(empresa);
				mav.addObject("clientes", cliente);

			}
		}

		mav.addObject("empresa", empresa);
		mav.setViewName("/cliente/list");
		return mav;

	}
}
	
