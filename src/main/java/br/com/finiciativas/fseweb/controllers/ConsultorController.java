package br.com.finiciativas.fseweb.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.conf.GenericResponse;
import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.CategoriaConsultor;
import br.com.finiciativas.fseweb.enums.Departamento;
import br.com.finiciativas.fseweb.enums.Divisao;
import br.com.finiciativas.fseweb.enums.Pais;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Role;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FilialServiceImpl;
import br.com.finiciativas.fseweb.services.impl.RoleServiceImpl;

@Controller
@RequestMapping(path = "/consultores")
public class ConsultorController {

	@Autowired
	private ConsultorServiceImpl consultorServiceImpl;

	@Autowired
	private RoleServiceImpl roleServiceImpl;
	
	@Autowired
	private FilialServiceImpl filialServiceImpl;
	
	@Autowired
	private EquipeServiceImpl equipeServiceImpl;

	@GetMapping(path = "/form")
	public String form(Model model) {
		
		List<Equipe> equipes = this.equipeServiceImpl.getAll();
		List<Filial> filiais = this.filialServiceImpl.getAll();
		List<Cargo> cargos = Arrays.asList(Cargo.values());
		List<CategoriaConsultor> categoriaConsultor = Arrays.asList(CategoriaConsultor.values());
		List<Departamento> departamentos = Arrays.asList(Departamento.values());
		List<Divisao> setorAtividade = Arrays.asList(Divisao.values());
		
		model.addAttribute("setorAtividade", setorAtividade);
		model.addAttribute("departamentos", departamentos);
		model.addAttribute("equipes", equipes);
		model.addAttribute("filiais", filiais);
		model.addAttribute("cargos", cargos);
		model.addAttribute("roles", roleServiceImpl.getAll());
		model.addAttribute("paises", Pais.values());
		model.addAttribute("categoriaConsultor", categoriaConsultor);

		return "consultor/form";
	}

	@ResponseBody
	@GetMapping(path = "/{id}")
	public ModelAndView findByUsername(@PathVariable("id") Long id) {
		
		ModelAndView modelAndView = new ModelAndView();

		List<Cargo> cargos = Arrays.asList(Cargo.values());
		List<Departamento> departamentos = Arrays.asList(Departamento.values());
		List<Role> roles = roleServiceImpl.getAll();
		List<CategoriaConsultor> categoriaConsultor = Arrays.asList(CategoriaConsultor.values());
		List<Divisao> setorAtividade = Arrays.asList(Divisao.values());
		
		Consultor consultor = consultorServiceImpl.findById(id);
			
		modelAndView.addObject("setorAtividade", setorAtividade);
		modelAndView.addObject("cargos", cargos);
		modelAndView.addObject("departamentos", departamentos);
		modelAndView.addObject("roles",
				roles.stream().filter(role -> !consultor.getRoles().contains(role)).collect(Collectors.toList()));
		modelAndView.addObject("consultor", consultor);
		modelAndView.addObject("categoriaConsultor", categoriaConsultor);
		modelAndView.addObject("paises", Pais.values());
		
		modelAndView.setViewName("/consultor/update");

		return modelAndView;

	}
	
	@GetMapping(path = "/updateSenha/{id}")
	public ModelAndView updateSenha(@PathVariable("id") Long id) {
		
		ModelAndView modelAndView = new ModelAndView();

		Consultor consultor = consultorServiceImpl.findById(id);

		modelAndView.addObject("consultor", consultor);
		
		modelAndView.setViewName("/consultor/updateSenha");

		return modelAndView;

	}
	
	@Transactional
	@PostMapping(path = "/updatePassword")
	public ModelAndView updatePassword(@Valid Consultor consultor) {

		ModelAndView modelAndView = new ModelAndView();
			
		System.out.println(consultor.getSenha());
		System.out.println(consultor.getId());
		
		consultor = consultorServiceImpl.updateSenha(consultor);
		
		modelAndView.setViewName("redirect:/consultores/" + consultor.getId());
		
		return modelAndView;

	}

	@Transactional
	@PostMapping(path = "/update")
	public ModelAndView update(@Valid Consultor consultor) {

		ModelAndView modelAndView = new ModelAndView();
		System.out.println(consultor.getCategoriaConsultor());
		
		consultor = consultorServiceImpl.update(consultor);
		
		modelAndView.setViewName("redirect:/consultores/" + consultor.getId());
		
		return modelAndView;

	}
	@GetMapping(path = "/all")
	public ModelAndView listCollaborator() {
		
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("consultores", consultorServiceImpl.getAll());
		modelAndView.setViewName("consultor/lista");
		
		return modelAndView;

	}
	
	@GetMapping(path = "/listAll")
	public ModelAndView listAtivos() {
		
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("consultores", consultorServiceImpl.getAtivo());
		modelAndView.setViewName("consultor/inativo");
		
		return modelAndView;

	}
	
	@Transactional
    @PostMapping("/addConsultor")
	public ModelAndView createConsultor(@Valid Consultor consultor) {

		ModelAndView modelAndView = new ModelAndView();

		if (consultorServiceImpl.findByUsername(consultor.getUsername()) != null) {
			modelAndView.addObject("errorMessage", "Ops, este usu�rio j� consta em nosso sistema");
			modelAndView.addObject("consultores", consultorServiceImpl.getAll());
			modelAndView.addObject("categoriaConsultor", CategoriaConsultor.values());
			return modelAndView;
		}

		//consultorServiceImpl.enviarEmailBoasVindas(consultor);

		consultor.criptarSenha();

		consultorServiceImpl.create(consultor);
		
		modelAndView.setViewName("redirect:/consultores/all");

		return modelAndView;

	}
	
	@Transactional
    @PostMapping("/add")
	public ModelAndView addConsultor(@Valid Consultor consultor) {

		ModelAndView modelAndView = new ModelAndView();

		//consultorServiceImpl.enviarEmailBoasVindas(consultor);

		consultor.criptarSenha();

		consultorServiceImpl.create(consultor);
		
		modelAndView.setViewName("redirect:/consultores/all");

		return modelAndView;

	}

	@PostMapping(path = "/delete/{id}")
	@Transactional
	public String delete(@PathVariable("id") Long id) {

		consultorServiceImpl.delete(id);

		return "redirect:/consultores/all";

	}
	
	@GetMapping("/{id}.json")
	public @ResponseBody Consultor buscaConsultorporIdJson(@PathVariable("id")Long id) {
		
		System.out.println("Id do consultor: " + id);
		
		Consultor consultor = this.consultorServiceImpl.findById(id);

		System.out.println("Nome consultor: " + consultor.getNome());
		System.out.println("Consultor cargo: " + consultor.getCargo());
		System.out.println("Consultor email: " + consultor.getEmail());
		
		return consultor;
	}

	@GetMapping("/email/{email}.json")
	public @ResponseBody Consultor emailConsultorJson(@PathVariable("email") String email) {

		Consultor consultor = this.consultorServiceImpl.findUserByEmail(email);

	    if (consultor == null) {
	        System.out.println("Usu�rio n�o encontrado");
	        return null;
	    } else {
	    	System.out.println("o e-mail do consultor encontrado �: " + email);
	    	String token = UUID.randomUUID().toString();
	        constructResetTokenEmail(token, consultor);
	        return consultor;
	    }
	}

	private void constructResetTokenEmail(String token, Consultor consultor) {
		System.out.println("entrou no m�todo para enviar o e-mail");
		try {
			HtmlEmail email = new HtmlEmail();
			email.setHostName("SMTP.office365.com");
			email.setSmtpPort(587);
			email.setSSLOnConnect(false);
			email.setStartTLSEnabled(true);
			email.setStartTLSRequired(true);
			email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@@fi*1303"));
			email.setFrom("fseweb@f-iniciativas.com.br");
			email.setSubject("TROCA DE SENHA FSEWEB");
			email.addTo(consultor.getEmail());
			StringBuffer msg = new StringBuffer();
			msg.append("<html><body>");
			msg.append("<b>Aten��o! Envio autom�tico pelo FSEWeb, n�o responder.</b><br/>");
			msg.append("<br/>");
			msg.append("Prezado(a), " + consultor.getNome() + "<br/>");
			msg.append("<br/>");
			msg.append("Link de acesso: <a th:href=\"@{/login}\"></a> <b>" + token + "</b><br/>");
			msg.append("<br/>");
			msg.append("Obrigado.");

			email.setHtmlMsg(msg.toString());

			// set the alternative message
			email.setTextMsg("Your email client does not support HTML messages");
			// send the email
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor rolesCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					Role role = new Role();
					role.setNome(element.toString());

					return role;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};
		
		binder.registerCustomEditor(List.class, "roles", rolesCollector);

	}
}
