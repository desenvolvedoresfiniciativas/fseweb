package br.com.finiciativas.fseweb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.services.impl.OportunidadeServiceImpl;

@Controller
@RequestMapping("/oportunidades")
public class OportunidadeController {

	@Autowired
	private OportunidadeServiceImpl oportunidadeService;
	
	@GetMapping("/all")
	public ModelAndView getAll(){
		ModelAndView mav = new ModelAndView();

		mav.addObject("oportunidades", oportunidadeService.getAll());
		
		mav.setViewName("/CRM/list");
		
		return mav;
	}
	
	
}
