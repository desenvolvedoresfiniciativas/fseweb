package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;

@Controller
@RequestMapping(path = "/clientes/contatos")
public class ContatoClienteController {

	@Autowired
	private ContatoClienteServiceImpl contatoClienteService;
	
	@Autowired
	private ClienteServiceImpl clienteService;

	@GetMapping("/{id}.json")
	public @ResponseBody List<ContatoCliente> getContatos(@PathVariable("id") Long id) {
		return contatoClienteService.getContatosCliente(id);
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();

		ContatoCliente contato = contatoClienteService.findById(id);

		modelAndView.addObject("contatoCliente", contato);

		modelAndView.setViewName("cliente/contatocliente/update");

		return modelAndView;
	}

	@GetMapping("details/{id}.json")
	public @ResponseBody ContatoCliente findByIdJson(@PathVariable("id") Long id) {
		return contatoClienteService.findById(id);
	}

	@Transactional
	@PostMapping("/add/{id}")
	public void add(@RequestBody ContatoCliente contatoCliente, @PathVariable Long id,
			HttpServletResponse response) {
		contatoClienteService.create(contatoCliente, id);
		response.setStatus(200);
	}

	@Transactional
	@PostMapping("/update")
	public String update(@Valid ContatoCliente contatoCliente) {
		contatoCliente.setCliente(clienteService.findById(contatoCliente.getCliente().getId()));
		contatoClienteService.update(contatoCliente);
		return "redirect:/clientes/" + contatoCliente.getCliente().getId();
	}

	@Transactional
	@PostMapping("{idCliente}/delete/{id}")
	public String delete(@PathVariable("idCliente") Long idCliente, @PathVariable("id") Long id) {
		ContatoCliente contatoCliente = new ContatoCliente();
		contatoCliente = contatoClienteService.findById(id);
		contatoClienteService.remove(contatoCliente);
		return "redirect:/clientes/" + idCliente;
	}
	
	
	@GetMapping("/{idCliente}/idContatoCliente.json")
	public @ResponseBody List<ContatoCliente> getContatoCliente(@PathVariable("idCliente") Long idCliente) {
		
		List<ContatoCliente> contato = contatoClienteService.getContatosCliente(idCliente);
		List<Long> idContato = new ArrayList<Long>();
		
		for(ContatoCliente info : contato) {
			//idContato.add(info.getId());
			System.out.println("info: "+info);
		}
		
		return  contato;
	}
	
	
	
	
	
	
}
