package br.com.finiciativas.fseweb.controllers;

import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;

@Controller
@RequestMapping(path = "/metricas")
public class MetricasController {

	@Autowired
	private ProdutoServiceImpl produtoService;
	
	@Autowired
	private ItemValoresServiceImpl itemValoresService;

	@GetMapping("/update")
	public ModelAndView form() {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("produtos", produtoService.getAll());
		modelAndView.setViewName("/metricas/update");

		return modelAndView;
	}

	@PostMapping("/salvaParametros")
	@ResponseBody
	public void gerador(@RequestBody List<String> parametros) {

		Long idProd = null;
		Long idEtapa = null;
		String data  = null;
		String ano  = null;

		for (int i = 0; i < parametros.size(); i++) {
			switch (i) {
			case 0:
				idProd = Long.parseLong(parametros.get(i));
				break;
			case 1:
				idEtapa = Long.parseLong(parametros.get(i));
				break;
			case 2:
				data = parametros.get(i);
				break;
			case 3:
				ano = parametros.get(i);
				break;
			}

		}
		
		itemValoresService.atualizaDataFinalizacao(idProd, idEtapa, data, ano);

	}

}
