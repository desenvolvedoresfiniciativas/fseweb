package br.com.finiciativas.fseweb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.honorario.FaixasEscalonado;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.repositories.impl.HonorarioRepositoryImpl;

@RequestMapping("/contratos/honorarios")
@Controller
public class HonorarioController {
	
	@Autowired
	private HonorarioRepositoryImpl honorarioRepository;
	
	@PostMapping("/percentualfixo/update")
	@ResponseBody	           
	public Honorario updatePercentualFixo(@RequestBody PercentualFixo percentualFixo){
		
		if(percentualFixo.getId()!=null) {
		return this.honorarioRepository.updatePercentualFixo(percentualFixo);
		}else {
			return percentualFixo;
		}
		
	}
	
	@PostMapping("/valorfixo/update")
	@ResponseBody	           
	public Honorario updateValorFixo(@RequestBody ValorFixo valorFixo){
		if(valorFixo.getId()!=null) {
		return this.honorarioRepository.update(valorFixo);
		}else {
			return valorFixo;
		}
	}
	
	@PostMapping("/percentualescalonado/update")
	@ResponseBody	           
	public Honorario updatePercentualEscalonado(@RequestBody PercentualEscalonado percentualescalonado){
		if(percentualescalonado.getId()!=null) {
		percentualescalonado.setNome("Percentual Escalonado");
		
		return this.honorarioRepository.update(percentualescalonado);

		} else {
			return percentualescalonado;
		}
	}

}
