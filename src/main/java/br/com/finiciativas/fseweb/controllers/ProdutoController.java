package br.com.finiciativas.fseweb.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;

@Controller
@RequestMapping("/produtos")
public class ProdutoController {
	
	@Autowired
	private ProdutoServiceImpl produtoService;
	
	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;
	
	@Autowired
	private EtapasConclusaoServiceImpl conclusaoServiceImpl;
	
	private Long produtoAtual;
	
	@GetMapping("/form")
	public ModelAndView form(){
		ModelAndView mav = new ModelAndView();
		mav.addObject("etapasTrabalho", this.etapaTrabalhoService.getAll());
		mav.setViewName("/produto/form");
		return mav;
	}
	
	@PostMapping("/add")
	public String create(Produto produto){
		
		this.produtoService.create(produto);
		return "/produto/form";
	}
	
	@GetMapping("/all")
	public ModelAndView getAll() {
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("produtos", this.produtoService.getAll());
		
		mav.setViewName("/produto/list");
		
		return mav;
	}
	
	@GetMapping("/{id}")
	public ModelAndView details(@PathVariable("id") Long id){
		ModelAndView mav = new ModelAndView();
		
		List<EtapaTrabalho> etapasTrabalho = this.etapaTrabalhoService.getAll();
		Produto produto = this.produtoService.findById(id);
		List<EtapaTrabalho> etapasNotCotainsInProduct = etapasTrabalho.stream().filter(etapa -> !produto.getEtapasTrabalho().contains(etapa)).collect(Collectors.toList());
		
		mav.addObject("produto", produto);
		
		mav.addObject("etapasTrabalhoRestantes", etapasNotCotainsInProduct);
		
		mav.setViewName("/produto/update");
		
		return mav;
	}
	
	@GetMapping("detalhes/{id}")
	public ModelAndView detalhe(@PathVariable("id") Long id){
		ModelAndView mav = new ModelAndView();
		Produto produto = this.produtoService.findById(id);
		
		produtoAtual = produto.getId();
		
		mav.addObject("produto", produto);
		mav.addObject("etapas", produto.getEtapasTrabalho());
		
		mav.setViewName("/produto/detalhe");
		
		return mav;
	}
	
	@GetMapping("tarefas/{id}")
	public ModelAndView detalheTarefas(@PathVariable("id") Long id){
		ModelAndView mav = new ModelAndView();
		EtapaTrabalho etapa = this.etapaTrabalhoService.findById(id);
		
		mav.addObject("etapa", etapa);
		mav.addObject("produto", produtoService.findById(produtoAtual));
		mav.addObject("tarefas", etapa.getTarefas());
		
		mav.setViewName("/produto/detalheTarefas");
		
		return mav;
	}
	
	@PostMapping("/update")
	public ModelAndView update(Produto produto){
		ModelAndView mav = new ModelAndView();
		conclusaoServiceImpl.addEtapaInProductions();
		List<EtapaTrabalho> etapasNotCotainsInProduct = produto.getEtapasTrabalho()
				.stream().filter(etapa -> !produto.getEtapasTrabalho().contains(etapa))
				.collect(Collectors.toList());
		conclusaoServiceImpl.addEtapaInProductions();
		mav.addObject("produto", this.produtoService.update(produto));
		mav.addObject("etapasTrabalhoRestantes", etapasNotCotainsInProduct);
		mav.setViewName("redirect:/produtos/" + produto.getId().toString());
		
		return mav;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor etapasTrabalhoCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					EtapaTrabalho etapaTrabalho = new EtapaTrabalho();
					etapaTrabalho.setId(Long.parseLong(element.toString()));

					return etapaTrabalho;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};

		binder.registerCustomEditor(List.class, "etapasTrabalho", etapasTrabalhoCollector);

	}
	
	@GetMapping("/{idProduto}/etapas.json")
	public @ResponseBody List<EtapaTrabalho> getEtapasOfProduto(@PathVariable("idProduto") Long id) {

		System.out.println("chegou");
		
		List<EtapaTrabalho> etapas = produtoService.findById(id).getEtapasTrabalho();
		System.out.println(etapas);

		return etapas;
	}
	
}
