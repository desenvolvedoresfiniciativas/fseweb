package br.com.finiciativas.fseweb.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.vigencia.Vigencia;
import br.com.finiciativas.fseweb.models.vigencia.VigenciaDeterminada;
import br.com.finiciativas.fseweb.models.vigencia.VigenciaIndeterminada;
import br.com.finiciativas.fseweb.services.ContratoService;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.VigenciaServiceImpl;

@RequestMapping("/contratos/vigencias")
@Controller
public class VigenciaController {
	
	@Autowired
	private VigenciaServiceImpl service;
	
	@Autowired
	private ContratoServiceImpl contratoService;
	
	@PostMapping("/vigenciadeterminada/update")
	@ResponseBody
	public Vigencia update(@RequestBody VigenciaDeterminada vigenciaDeterminada){
		return this.service.update(vigenciaDeterminada);
	}
	
	@PostMapping("/vigenciaindeterminada/update")
	@ResponseBody
	public Vigencia update(@RequestBody VigenciaIndeterminada vigenciaIndeterminada){
		return this.service.update(vigenciaIndeterminada);
	}
	
	@RequestMapping(value = "/campanha/{id}/{campanhaInicial}/{campanhaFinal}/.json", method = RequestMethod.POST)
	public @ResponseBody Vigencia updateCampanha(@PathVariable("id") Long idContrato, @PathVariable("campanhaInicial") String campanhaInicial, @PathVariable("campanhaFinal") String campanhaFinal) {
		Contrato contrato = contratoService.findById(idContrato);
		Vigencia vigencia = contrato.getVigencia();
		if(campanhaInicial != null) {
			vigencia.setCampanhaInicial(campanhaInicial);
		}
		if(campanhaFinal != null) {
			vigencia.setCampanhaFinal(campanhaFinal);
		}
		
		return this.service.update(vigencia);
	}
	
}
