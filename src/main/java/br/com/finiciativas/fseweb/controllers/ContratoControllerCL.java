package br.com.finiciativas.fseweb.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.FormaPagamento;
import br.com.finiciativas.fseweb.enums.FormaPagamentoCL;
import br.com.finiciativas.fseweb.factory.HonorarioFactory;
import br.com.finiciativas.fseweb.factory.VigenciaFactory;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.contrato.GarantiaContrato;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaPagamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.GarantiaContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;

@Controller
@RequestMapping(path = "/contratosCL")
public class ContratoControllerCL {

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private GarantiaContratoServiceImpl garantiaContratoService;

	@Autowired
	private HonorarioServiceImpl HonorarioService;

	@Autowired
	private ClienteServiceImpl ClienteService;

	@Autowired
	private EtapaPagamentoServiceImpl EtapaPagamentoService;

	@GetMapping("/formCL")
	public ModelAndView formCL() {
		ModelAndView mav = new ModelAndView("/contrato/chile/form");

		mav.addObject("produtos", this.produtoService.getAll());
		mav.addObject("comerciaisAtivos", this.consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("comerciaisTodos", this.consultorService.getConsultorByCargoAll(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("formasPagamento", FormaPagamentoCL.values());
		mav.addObject("garantias", this.garantiaContratoService.getAll());

		return mav;
	}

	@PostMapping("/add")
	public ModelAndView create(@Valid Contrato contrato, @RequestParam("vigenciaradio") String vigenciaradio,
			@RequestParam(value = "valorfixocheck", required = false) String valorfixocheck,
			@RequestParam(value = "percentualfixocheck", required = false) String percentualfixocheck,
			@RequestParam(value = "percentualescalonadocheck", required = false) String percentualescalonadocheck) {

		ModelAndView mav = new ModelAndView();
		contrato.setVigencia(new VigenciaFactory().retornaVigencia(vigenciaradio));
		contrato.setHonorarios(new HonorarioFactory().retornarHonorario(valorfixocheck, percentualfixocheck,
				percentualescalonadocheck));
		

		for (Honorario h : contrato.getHonorarios()) {
			h.setContrato(contrato);
		}

			EtapasPagamento etapas = new EtapasPagamento();
			contrato.setEtapasPagamento(etapas);

		mav.setViewName("redirect:/contratos/" + this.contratoService.create(contrato));

		if (contrato.isPagamentoEntrega()) {
			EtapasPagamento et = EtapaPagamentoService.getLastAdded();
			et.setContrato(contrato);
			EtapaPagamentoService.update(et);
		}
		return mav;
	}
	
	@PostMapping("/addCL")
	public ModelAndView createCL(@Valid Contrato contrato, @RequestParam("vigenciaradio") String vigenciaradio,
			@RequestParam(value = "valorfixocheck", required = false) String valorfixocheck,
			@RequestParam(value = "percentualfixocheck", required = false) String percentualfixocheck,
			@RequestParam(value = "percentualescalonadocheck", required = false) String percentualescalonadocheck) {
		
		System.out.println(percentualescalonadocheck);
		System.out.println(percentualfixocheck);
		System.out.println(valorfixocheck);

		ModelAndView mav = new ModelAndView();
		contrato.setVigencia(new VigenciaFactory().retornaVigencia(vigenciaradio));
		contrato.setHonorarios(new HonorarioFactory().retornarHonorario(valorfixocheck, percentualfixocheck,
				percentualescalonadocheck));

			EtapasPagamento etapas = new EtapasPagamento();
			contrato.setEtapasPagamento(etapas);

		mav.setViewName("redirect:/contratos/chile/" + this.contratoService.create(contrato));
		
		for (Honorario h : contrato.getHonorarios()) {
			System.out.println("Adicionado ao contrato: "+h.getNome());
			h.setContrato(contrato);
			HonorarioService.update(h);
		}

		if (contrato.isPagamentoEntrega()) {
			EtapasPagamento et = EtapaPagamentoService.getLastAdded();
			et.setContrato(contrato);
			EtapaPagamentoService.update(et);
		}
		return mav;
	}

	@PostMapping("/atualizaEtapas")
	@ResponseBody
	public EtapasPagamento atualizaEtapas(@RequestBody EtapasPagamento etapas) {
		EtapasPagamento etapa = EtapaPagamentoService.findByContratoId(etapas.getContrato().getId());
		etapa.setContrato(etapas.getContrato());
		
		if (etapas.getEtapa1().getId() != null && etapas.getEtapa1().getId() != 0l) {
			etapa.setEtapa1(etapas.getEtapa1());
		} else {
			etapa.setEtapa1(null);
		}
		if (etapas.getEtapa2().getId() != null && etapas.getEtapa2().getId() != 0l) {
			etapa.setEtapa2(etapas.getEtapa2());
		} else {
			etapa.setEtapa2(null);
		}
		if (etapas.getEtapa3().getId() != null && etapas.getEtapa3().getId() != 0l) {
			etapa.setEtapa3(etapas.getEtapa3());
		} else {
			etapa.setEtapa3(null);
		}

		if (etapas.getPorcentagem1() == 0.0f) {
			etapa.setPorcentagem1(0.0f);
		} else {
			etapa.setPorcentagem1(etapas.getPorcentagem1());
		}
		if (etapas.getPorcentagem2() == 0.0f) {
			etapa.setPorcentagem2(0.0f);
		} else {
			etapa.setPorcentagem2(etapas.getPorcentagem2());
		}
		if (etapas.getPorcentagem3() == 0.0f) {
			etapa.setPorcentagem3(0.0f);
		} else {
			etapa.setPorcentagem3(etapas.getPorcentagem3());
		}

		etapa.setParcela1(etapas.getParcela1());
		etapa.setParcela2(etapas.getParcela2());
		etapa.setParcela3(etapas.getParcela3());

		if (etapas.getDataFaturamento() != null) {
			etapa.setDataFaturamento(etapas.getDataFaturamento());
		}

		EtapaPagamentoService.update(etapa);
		return etapas;
	}

	@GetMapping("/cliente/{id}")
	public ModelAndView findClienteId(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/contrato/cliente");

		System.out.println(this.contratoService.getContratoByCliente(id));
		mav.addObject("contratos", this.contratoService.getContratoByCliente(id));

		return mav;
	}
	
	@GetMapping("/clienteCL/{id}")
	public ModelAndView findClienteIdCL(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/contrato/chile/cliente");

		System.out.println(this.contratoService.getContratoByCliente(id));
		mav.addObject("contratos", this.contratoService.getContratoByCliente(id));

		return mav;
	}
	
	@GetMapping("/chile/{id}")
	public ModelAndView findByIdChile(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/contrato/chile/update");

		List<GarantiaContrato> garantias = garantiaContratoService.getAll();
		Contrato contrato = contratoService.findById(id);
		List<EtapaTrabalho> etapasTrabalho = contrato.getProduto().getEtapasTrabalho();

		List<Honorario> honorarios = contrato.getHonorarios();

		for (Honorario honorario : honorarios) {
			System.out.println("Honor�rios do Contrato acessado: ");
			System.out.println("Nome: " + honorario.getNome());
			System.out.println("Contrato: " + honorario.getContrato());
		}

		mav.addObject("contrato", contrato);
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("comerciaisAtivos", this.consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("comerciaisTodos", this.consultorService.getConsultorByCargoAll(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("formasPagamentoCL", FormaPagamentoCL.values());
		mav.addObject("etapasTrabalho", etapasTrabalho);
		mav.addObject("etapasPagamento", contrato.getEtapasPagamento());
		mav.addObject("garantias", garantias);
		mav.addObject("honorarios", honorarios);

		mav.addObject("garantiasNo", garantias.stream().filter(garan -> !contrato.getGarantias().contains(garan))
				.collect(Collectors.toList()));

		return mav;
	}
	
	@GetMapping("/allCL")
	public ModelAndView getAllCL() {

		ModelAndView modelAndView = new ModelAndView();

		List<Cliente> clientes = ClienteService.getAll();

		modelAndView.setViewName("/contrato/chile/list");
		modelAndView.addObject("clientes", clientes);
		return modelAndView;
	}

	@GetMapping("/listAll")
	public ModelAndView getAllContracts() {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/contrato/chile/listAll");
		modelAndView.addObject("contratos", contratoService.getAll());
		return modelAndView;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor garantiasContratoCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					GarantiaContrato garantiaContrato = new GarantiaContrato();
					garantiaContrato.setId(Long.parseLong(element.toString()));

					return garantiaContrato;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};

		binder.registerCustomEditor(List.class, "garantias", garantiasContratoCollector);

	}

	@Transactional
	@PostMapping("/update")
	public void add(@RequestBody @Valid Contrato contrato, @RequestBody PercentualFixo pf,
			@RequestBody PercentualEscalonado pe, @RequestBody ValorFixo vf, HttpServletResponse response) {

		List<Honorario> honorarios = new ArrayList<>();

		if (pf != null) {
			System.out.println("TEM PF");
			honorarios.add(pf);
		}

		if (pe != null) {
			System.out.println("TEM PE");
			honorarios.add(pe);
		}

		if (vf != null) {
			System.out.println("TEM VF");
			honorarios.add(vf);
		}

		contrato.setHonorarios(honorarios);

//		if (this.contratoService.update(contrato) != null) {
//			response.setStatus(200);
//		}
//		response.setStatus(400);
	}

	@GetMapping("/all.json")
	public @ResponseBody List<Contrato> contratoJson() {

		return contratoService.getAll();

	}
	
	@GetMapping("/{id}.json")
	public @ResponseBody List<Contrato> contratosByIDJson(@PathVariable("id") Long id) {

		return contratoService.getContratoByCliente(id);

	}
	
	@GetMapping("/unique/{id}.json")
	public @ResponseBody Contrato contratoByIdJson(@PathVariable("id") Long id) {

		return contratoService.findById(id);

	}

	@GetMapping("/honorario.json")
	public @ResponseBody List<Honorario> honorarioJson() {

		return HonorarioService.getAll();

	}
	
	@PostMapping("/atualizar")
	@ResponseBody
	public void atualizarContrato(@RequestBody Contrato contrato) {
		
		System.out.println(contrato.getCnpjFaturar());
		System.out.println(contrato.getRazaoSocialFaturar());
		System.out.println(contrato.isContratoFisico());
		
		
		Contrato contratoAtt = contratoService.findById(contrato.getId());
		contratoAtt.setComercialResponsavelEntrada(consultorService.findById(contrato.getComercialResponsavelEntrada().getId()));
		contratoAtt.setComercialResponsavelEfetivo(consultorService.findById(contrato.getComercialResponsavelEfetivo().getId()));
		contratoAtt.setDescricaoContratual(contrato.getDescricaoContratual());
		contratoAtt.setObsContrato(contrato.getObsContrato());
		contratoAtt.setContratoFisico(contrato.isContratoFisico());
		contratoAtt.setCnpjFaturar(contrato.getCnpjFaturar());
		contratoAtt.setRazaoSocialFaturar(contrato.getRazaoSocialFaturar());
				
		contratoService.update(contratoAtt);
		
	}
}
