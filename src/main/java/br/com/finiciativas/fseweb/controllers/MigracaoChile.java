package br.com.finiciativas.fseweb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.services.impl.MigracaoServiceImpl;

@Controller
@RequestMapping(path = "/migraCL")
public class MigracaoChile {

	@Autowired
	private MigracaoServiceImpl migracaoService;
	
	@GetMapping("/migra")
	public ModelAndView form() {

		ModelAndView modelAndView = new ModelAndView();

		//migracaoService.lerExcel();
		//migracaoService.populaProducoes();
		//migracaoService.criarFiscalizacoes();
		//migracaoService.populaFiscalizacao();
		//migracaoService.identificaPostulacoes();
		//migracaoService.identificaFiscalizacoes();
		//migracaoService.atualizarTipoContrato();
		//migracaoService.procuraProds();
		migracaoService.geraModificaiones();
		
		modelAndView.setViewName("/migracao/main");

		return modelAndView;
	}
	
}
