package br.com.finiciativas.fseweb.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Especialidade;
import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.enums.EstadoEtapa;
import br.com.finiciativas.fseweb.enums.ExclusaoUtilizada;
import br.com.finiciativas.fseweb.enums.MotivoInatividade;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeAtivoIntangivel;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeCVM;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeExTarifario;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeFiname;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeFinanciamento;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeFinanciamentoSub;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeImportInov;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeLB;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeLI;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeLeiDoBem;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeRotaII;
import br.com.finiciativas.fseweb.enums.MotivoInatividadeRotaIII;
import br.com.finiciativas.fseweb.enums.Pais;
import br.com.finiciativas.fseweb.enums.RelatorioCalculo;
import br.com.finiciativas.fseweb.enums.Resultado;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.enums.SituacaoCL;
import br.com.finiciativas.fseweb.enums.SituacaoCO;
import br.com.finiciativas.fseweb.enums.TipoDeApuracao;
import br.com.finiciativas.fseweb.enums.TipoProjeto;
import br.com.finiciativas.fseweb.models.Eficiencia;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.lista.ValoresLista;
import br.com.finiciativas.fseweb.models.producao.PreProducao;
import br.com.finiciativas.fseweb.models.producao.PreProducaoCO;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Acompanhamento;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.AcompanhamentoExTarifario;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.AuditoriaEconomica;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.AuditoriaProjetos;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculoRota2030;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.FNDCT;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.HabilitacaoPortaria;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.ObrigacoesPeriodicas;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Projetos;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.produto.ordenacao.OrdemItensTarefa;
import br.com.finiciativas.fseweb.models.vigencia.VigenciaIndeterminada;
import br.com.finiciativas.fseweb.services.TarefaService;
import br.com.finiciativas.fseweb.services.ProducaoService;
import br.com.finiciativas.fseweb.services.impl.AcompanhamentoExTarifarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.AcompanhamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.AuditoriaEconomicaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.AuditoriaProjetosServiceImpl;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoROTAServiceImpl;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EficienciaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FNDCTserviceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FilialServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HabilitacaoPortariaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ObrigacoesPeriodicasServiceImpl;
import br.com.finiciativas.fseweb.services.impl.OrdemItensTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PreProducaoCOServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PreProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProjetosServiceImpl;
import br.com.finiciativas.fseweb.services.impl.RelatorioTecnicoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.TarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValoresListaServiceImpl;

@Controller
@RequestMapping("/producoes")
public class ProducaoController {

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ClienteServiceImpl ClienteService;

	@Autowired
	private ContratoServiceImpl ContratoService;

	@Autowired
	private ProdutoServiceImpl ProdutoService;

	@Autowired
	private ItemTarefaServiceImpl ItemTarefaService;

	@Autowired
	private ConsultorServiceImpl ConsultorService;

	@Autowired
	private ValoresListaServiceImpl ValoresListaService;

	@Autowired
	private ItemValoresServiceImpl itemValoresService;

	@Autowired
	private TarefaServiceImpl tarefaService;

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Autowired
	private ObrigacoesPeriodicasServiceImpl obrigacoesPeriodicasService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private FNDCTserviceImpl fndcTservice;

	@Autowired
	private ProjetosServiceImpl projetosService;

	@Autowired
	private BalanceteCalculoServiceImpl balanceteCalculoService;

	@Autowired
	private RelatorioTecnicoServiceImpl relatorioTecnicoService;

	@Autowired
	private AcompanhamentoServiceImpl acompanhamentoService;

	@Autowired
	private EquipeServiceImpl equipeService;

	@Autowired
	private FilialServiceImpl filialService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private EtapasConclusaoServiceImpl etapasConclusaoService;

	@Autowired
	private ContatoClienteServiceImpl contatoClienteService;

	@Autowired
	private HabilitacaoPortariaServiceImpl habilitacaoPortariaService;

	@Autowired
	private AuditoriaEconomicaServiceImpl auditoriaEconomicaService;

	@Autowired
	private AuditoriaProjetosServiceImpl auditoriaProjetosService;

	@Autowired
	private EficienciaServiceImpl eficienciaService;

	@Autowired
	private SubProducaoServiceImpl subProducaoService;

	@Autowired
	private OrdemItensTarefaServiceImpl ordemService;

	@Autowired
	private BalanceteCalculoROTAServiceImpl balanceteCalculoRotaService;

	@Autowired
	private PreProducaoServiceImpl preProducaoService;

	@Autowired
	private AcompanhamentoExTarifarioServiceImpl acompanhamentoExService;

	@Autowired
	private PreProducaoCOServiceImpl preProducaoCOService;

	private Long idProd;

	private Cliente clienteDaProd;

	private Producao prod;

	private long etapaId;

	@GetMapping("/all")
	public ModelAndView getAll() {
		ModelAndView mav = new ModelAndView();

		// pegar equipe do consultor.
		// query para pegar

		mav.addObject("clientes", ClienteService.getAll());
		mav.setViewName("/producao/list");

		return mav;
	}

	@GetMapping("/form/{id}")
	public ModelAndView form(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		List<Contrato> contratos = ContratoService.getContratoByCliente(id);

		mav.addObject("situacao", Situacao.values());
		mav.addObject("contratosAtual", contratos);
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.setViewName("/producao/form");
		mav.addObject("cliente", ClienteService.findById(id));
		return mav;
	}

	@GetMapping("/formCL/{id}")
	public ModelAndView formCL(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		PreProducao preProducao = preProducaoService.findById(id);
		List<Contrato> contratos = ContratoService.getContratoByCliente(preProducao.getCliente().getId());

		mav.addObject("preProducao", preProducao);
		mav.addObject("situacao", SituacaoCL.values());
		mav.addObject("contratosAtual", contratos);
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.setViewName("/producao/formCL");
		mav.addObject("cliente", preProducao.getCliente());
		return mav;
	}
	
	@GetMapping("/formCO/{id}")
	public ModelAndView formCO(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		PreProducaoCO preProducaoCO = preProducaoCOService.findById(id);
		List<Contrato> contratos = ContratoService.getContratoByCliente(preProducaoCO.getCliente().getId());

		mav.addObject("preProducao", preProducaoCO);
		mav.addObject("situacao", Situacao.values());
		mav.addObject("contratosAtual", contratos);
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.setViewName("/producao/formCO");
		mav.addObject("cliente", preProducaoCO.getCliente());
		return mav;
	}

	@GetMapping("/formPre/{id}")
	public ModelAndView formPre(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		Cliente cliente = ClienteService.findById(id);

		mav.addObject("situacao", SituacaoCL.values());
		mav.addObject("cliente", cliente);
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.addObject("cliente", ClienteService.findById(id));

		mav.setViewName("/producao/formPreProducao");

		return mav;
	}

	@GetMapping("/formPreCO/{id}")
	public ModelAndView formPreCO(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		Cliente cliente = ClienteService.findById(id);

		mav.addObject("situacao", SituacaoCO.values());
		mav.addObject("cliente", cliente);
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.addObject("cliente", ClienteService.findById(id));

		mav.setViewName("/producao/formPreProducaoCO");

		return mav;
	}

	@GetMapping("/preProducao/{id}")
	public ModelAndView updatePreProducao(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		PreProducao preProducao = preProducaoService.findById(id);

		mav.addObject("situacao", SituacaoCL.values());
		mav.addObject("preProducao", preProducao);
		mav.addObject("motivo", preProducao.getExplicacaoMotivo().toString());
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.addObject("cliente", ClienteService.findById(id));

		mav.setViewName("/producao/updatePreProducao");

		return mav;
	}
	
	@GetMapping("/preProducaoCO/{id}")
	public ModelAndView updatePreProducaoCO(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		PreProducaoCO preProducaoCO = preProducaoCOService.findById(id);

		mav.addObject("situacao", SituacaoCO.values());
		mav.addObject("preProducao", preProducaoCO);
		mav.addObject("motivo", preProducaoCO.getExplicacaoMotivo().toString());
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.addObject("cliente", ClienteService.findById(id));

		mav.setViewName("/producao/updatePreProducaoCO");

		return mav;
	}
	
	@PostMapping("/linkPre/{id}")
	public ModelAndView linkPre(@Valid Long pre,@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		
		
		System.out.println(pre);
		System.out.println(id);
		PreProducao pr = preProducaoService.findById(pre);
		Producao p = producaoService.findById(id);
		
		p.setPreProducao(pr);
		producaoService.update(p);
		modelAndView.setViewName("redirect:/producoes/clienteCL/" + p.getCliente().getId());

		return modelAndView;
	}

	@PostMapping("/addPre")
	public ModelAndView addPre(@Valid PreProducao preProd) {
		ModelAndView modelAndView = new ModelAndView();

		preProducaoService.create(preProd);

		modelAndView.setViewName("redirect:/producoes/clienteCL/" + preProd.getCliente().getId());
		return modelAndView;
	}
	@PostMapping("/addPreCO")
	public ModelAndView addPreCO(@Valid PreProducaoCO preProd) {
		ModelAndView modelAndView = new ModelAndView();
		preProd.setMotivoSituacao(preProd.getMotivoSituacao().replaceAll(",", "").trim());
		preProducaoCOService.create(preProd);
		modelAndView.setViewName("redirect:/producoes/clienteCO/" + preProd.getCliente().getId());
		return modelAndView;
	}

	@PostMapping("/updatePre")
	public ModelAndView updatePre(@Valid PreProducao preProd) {
		ModelAndView modelAndView = new ModelAndView();

		preProducaoService.update(preProd);

		modelAndView.setViewName("redirect:/producoes/clienteCL/" + preProd.getCliente().getId());
		return modelAndView;
	}
	@PostMapping("/updatePreCO")
	public ModelAndView updatePreCO(@Valid PreProducaoCO preProd) {
		ModelAndView modelAndView = new ModelAndView();
		preProducaoCOService.update(preProd);
		modelAndView.setViewName("redirect:/producoes/clienteCO/" + preProd.getCliente().getId());
		return modelAndView;
	}
	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Producao producao = producaoService.findById(id);
		producaoService.deleteProducao(id);

		modelAndView.setViewName("redirect:/producoes/cliente/" + producao.getCliente().getId());

		return modelAndView;

	}

	@GetMapping("/formSemContrato/{id}")
	public ModelAndView formSemContrato(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		List<Contrato> contratosAnteriores = ContratoService.getContratoByCliente(id);

		mav.addObject("filiais", filialService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("tipoDeApuracao", TipoDeApuracao.values());
		mav.addObject("comerciaisAtivos", this.consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("comerciaisTodos", this.consultorService.getConsultorByCargoAll(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("contratos", contratosAnteriores);
		mav.addObject("cliente", ClienteService.findById(id));
		mav.addObject("produtos", ProdutoService.getAll());
		mav.setViewName("/producao/formSemContrato");

		return mav;

	}

	@GetMapping("{idProd}/{idEtapa}/{idTar}")
	public ModelAndView formTest(@PathVariable("idProd") Long idProd, @PathVariable("idEtapa") Long idEtapa,
			@PathVariable("idTar") Long idTar) {
		ModelAndView mav = new ModelAndView();

		List<ItemTarefa> itens = new ArrayList<>();
		itens.addAll(ItemTarefaService.getItensByProducao(producaoService.findById(idProd)));
		for (ItemTarefa itemTarefa : itens) {
			System.out.println("----------------------------------------------------" + itemTarefa.getNome());
		}
		List<ValoresLista> lista = new ArrayList<>();
		for (ItemTarefa itemTarefa : itens) {
			lista.addAll(ValoresListaService.findByItemId(itemTarefa.getId()));
		}

		Producao producao = producaoService.findById(idProd);
		Tarefa tar = tarefaService.findById(idTar);

		mav.setViewName("/producao/item");
		mav.addObject("producao", producao);
		mav.addObject("cliente", producao.getContrato().getCliente());
		mav.addObject("itens", itens);
		mav.addObject("consultores", ConsultorService.getAll());
		mav.addObject("valoreslista", lista);
		mav.addObject("etapa", etapaTrabalhoService.findById(idEtapa));
		mav.addObject("valores", itemValoresService.findByTarefaAndProducao(idTar, idProd));
		mav.addObject("tarefa", tar);

		return mav;
	}

	@Transactional
	@ResponseBody
	@GetMapping("/{idEtapas}/v2")
	public ModelAndView form2o(@PathVariable("idEtapas") Long idEtapas) {

		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);
		Producao producao = producaoService.findById(etapas.getProducao().getId());

		List<ItemTarefa> itens = new ArrayList<>();
		itens.addAll(ItemTarefaService.getItensByProducao(producao));

		List<ValoresLista> lista = new ArrayList<>();
		for (ItemTarefa itemTarefa : itens) {
			lista.addAll(ValoresListaService.findByItemId(itemTarefa.getId()));
		}

		EtapaTrabalho etapa = etapaTrabalhoService.findById(etapas.getEtapa().getId());
		List<Tarefa> tarefas = etapa.getTarefas();

		mav.setViewName("/producao/item2o/item");
		mav.addObject("producao", producao);
		mav.addObject("cliente", producao.getContrato().getCliente());
		mav.addObject("itens", itens);
		mav.addObject("consultores", ConsultorService.getAll());
		mav.addObject("valoreslista", lista);
		mav.addObject("etapa", etapa);
		mav.addObject("tarefas", tarefas);
		mav.addObject("conclusao", etapas);

		return mav;
	}

	@GetMapping("/cliente/{id}")
	public ModelAndView findClienteId(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/producao/cliente1");

		List<Producao> producao = producaoService.getProducoesByCliente(id);
//		List<Contrato> contrato = new ArrayList<Contrato>();
//		contrato.addAll(ContratoService.getContratoByCliente(id));
//
//		if (contrato.size() > 0) {
//			mav.addObject("possuiContrato", "true");
//		} else {
//			mav.addObject("possuiContrato", "false");
//		}
//
//		List<Producao> producao2 = new ArrayList<>();
//
//		for (Contrato contrato2 : contrato) {
//			producao2.addAll(producaoService.getProducaoByContrato(contrato2.getId()));
//		}

		clienteDaProd = ClienteService.findById(id);

//		List<Contrato> contratosCliente = ContratoService.getContratoByCliente(id);
//		List<Produto> produtos = new ArrayList<Produto>();
//
//		for (Contrato contratoTemp : contratosCliente) {
//			if (!produtos.contains(contratoTemp.getProduto())) {
//				produtos.add(contratoTemp.getProduto());
//			}
//		}

		mav.addObject("cliente", ClienteService.findById(id));
		mav.addObject("producao", producao);
		// mav.addObject("contratos", contrato);
		// mav.addObject("produtos", produtos);

		return mav;
	}

	@GetMapping("/clienteCL/{id}")
	public ModelAndView findClienteChileId(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/producao/clienteCL");

		List<Producao> producao = producaoService.getProducoesByCliente(id);
		List<Contrato> contrato = new ArrayList<Contrato>();
		contrato.addAll(ContratoService.getContratoByCliente(id));

		if (contrato.size() > 0) {
			mav.addObject("possuiContrato", "true");
		} else {
			mav.addObject("possuiContrato", "false");
		}

		List<Producao> producao2 = new ArrayList<>();

		for (Contrato contrato2 : contrato) {
			producao2.addAll(producaoService.getProducaoByContrato(contrato2.getId()));
		}

		for (Producao producao3 : producao2) {
			System.out.println(producao3.getAno());
		}

		clienteDaProd = ClienteService.findById(id);

		List<Contrato> contratosCliente = ContratoService.getContratoByCliente(id);
		List<Produto> produtos = new ArrayList<Produto>();

		for (Contrato contratoTemp : contratosCliente) {
			if (!produtos.contains(contratoTemp.getProduto())) {
				produtos.add(contratoTemp.getProduto());
			}
		}

		List<PreProducao> preProducoes = preProducaoService.getAllByCliente(id);

		List<ItemValores> codigos = itemValoresService.getCodigoProyectoByCliente(id);
		List<ItemValores> estados = itemValoresService.getEstadoProyectoByCliente(id);
		List<ItemValores> fechaInicio = itemValoresService.getFechaInicioByCliente(id);
		List<ItemValores> fechaTermino = itemValoresService.getFechaTerminoByCliente(id);
		List<ItemValores> nombreProyecto = itemValoresService.getNombreProyectoByCliente(id);
		List<ItemValores> nombre = new ArrayList<ItemValores>();
		List<Long> done = new ArrayList<Long>();
		List<ItemValores> codigos2 = new ArrayList<ItemValores>();
		List<ItemValores> fechaTermino2 = new ArrayList<ItemValores>();
		List<ItemValores> fechaInicio2 = new ArrayList<ItemValores>();
		
		for (int i = 0; i < fechaInicio.size(); i+=2) {
			fechaInicio2.add(fechaInicio.get(i));
		}
		for (int i = 0; i < fechaTermino.size(); i+=2) {
			fechaTermino2.add(fechaTermino.get(i));
		}
		for (int i = 0; i < codigos.size(); i+=2) {
			codigos2.add(codigos.get(i));
		}
		for(ItemValores i : nombreProyecto) {
			System.out.println(i);
			
			if(!done.contains(i.getProducao().getId())) {
				done.add(i.getProducao().getId());
				nombre.add(i);
			}
		}

		mav.addObject("fechaInicio", fechaInicio2);
		mav.addObject("fechaTermino", fechaTermino2);
		mav.addObject("nombre", nombre);
		mav.addObject("codigos", codigos2);
		mav.addObject("estados", estados);
		mav.addObject("cliente", ClienteService.findById(id));
		mav.addObject("producao", producao);
		mav.addObject("contratos", contrato);
		mav.addObject("preProducao", preProducoes);
		mav.addObject("produtos", produtos);
		
		
		return mav;
	}

	@GetMapping("/clienteCO/{id}")
	public ModelAndView findClienteColombiaId(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/producao/clienteCO");

		List<Producao> producao = producaoService.getProducoesByCliente(id);
		List<Contrato> contrato = new ArrayList<Contrato>();
		contrato.addAll(ContratoService.getContratoByCliente(id));

		if (contrato.size() > 0) {
			mav.addObject("possuiContrato", "true");
		} else {
			mav.addObject("possuiContrato", "false");
		}

		List<Producao> producao2 = new ArrayList<>();

		for (Contrato contrato2 : contrato) {
			producao2.addAll(producaoService.getProducaoByContrato(contrato2.getId()));
		}

		for (Producao producao3 : producao2) {
			System.out.println(producao3.getAno());
		}

		clienteDaProd = ClienteService.findById(id);

		List<Contrato> contratosCliente = ContratoService.getContratoByCliente(id);
		List<Produto> produtos = new ArrayList<Produto>();

		for (Contrato contratoTemp : contratosCliente) {
			if (!produtos.contains(contratoTemp.getProduto())) {
				produtos.add(contratoTemp.getProduto());
			}
		}

		List<PreProducaoCO> preProducoes = preProducaoCOService.getAllByCliente(id);
		List<ItemValores> codigos = itemValoresService.getCodigoProyectoByCliente(id);
		List<ItemValores> estados = itemValoresService.getEstadoProyectoByCliente(id);

		mav.addObject("codigos", codigos);
		mav.addObject("estados", estados);
		mav.addObject("cliente", ClienteService.findById(id));
		mav.addObject("producao", producao);
		mav.addObject("contratos", contrato);
		mav.addObject("preProducao", preProducoes);
		mav.addObject("produtos", produtos);

		return mav;
	}

	@GetMapping("/{cliente}/{produto}/{ano}/prod.json")
	public @ResponseBody String checkForProducao(@PathVariable("cliente") Long idCliente,
			@PathVariable("produto") Long idProduto, @PathVariable("ano") String ano) {

		String possui;

		Producao producao = producaoService.checkForProducao(idCliente, idProduto, ano);
		if (Objects.nonNull(producao)) {
			System.out.println("Possui");
			possui = "1";
		} else {
			System.out.println("N�o Possui");
			possui = "0";
		}

		return possui;
	}

	@GetMapping("/{cliente}/{ano}.json")
	public @ResponseBody String checkForPreProducao(@PathVariable("cliente") Long idCliente,
			@PathVariable("ano") String ano) {

		String possui;

		PreProducao preProd = producaoService.checkForPreProducao(idCliente, ano);
		if (Objects.nonNull(preProd)) {
			System.out.println("Possui");
			possui = "1";
		} else {
			System.out.println("N�o Possui");
			possui = "0";
		}

		return possui;
	}

	@GetMapping("/{idContrato}/producoes.json")
	public @ResponseBody List<Producao> getProducoesByContrato(@PathVariable("idContrato") Long idContrato) {

		Contrato contrato = this.ContratoService.findById(idContrato);
		System.out.println(contrato + " cult of personality");

		List<Producao> producoes = producaoService.getProducaoByContrato(contrato.getId());

		return producoes;
	}

	@GetMapping("/producoesCodigo.json")
	public @ResponseBody List<Producao> getProducoesByCodigo(@RequestParam("codigo") String codigo) {

		List<Producao> prods = producaoService.getProducaoByCodigo(codigo);

		return prods;
	}

	@GetMapping("/getCodigoByProd.json")
	public @ResponseBody ItemValores getCodigoByProducao(@RequestParam("id") Long id) {

		return itemValoresService.getCodigoProyectoByProducao(id);
	}

	@GetMapping("/clientes/{id}.json")
	public @ResponseBody List<Producao> getProducoesByCliente(@PathVariable("id") Long id) {

		List<Producao> producoes = producaoService.getProducoesByCliente(id);

		return producoes;
	}

	@Transactional
	@PostMapping("/add")
	public ModelAndView add(@Valid Producao producao, @RequestParam("contratoId") Long idContrato) {
		System.out.println(" entrou aqui");

		ModelAndView modelAndView = new ModelAndView();

		Contrato contrato = ContratoService.findById(idContrato);

		if (producao.getSituacao() == null) {
			producao.setSituacao(Situacao.NEGOCIACAO);
		}
		System.out.println("find");
		producao.setContrato(contrato);
		producao.setProduto(contrato.getProduto());
		producao.setCliente(contrato.getCliente());

		Producao prod = producaoService.create(producao);

		etapasConclusaoService.createEtapasConclusaoByProd(prod);

		producaoService.createItemValores(prod);

		List<OrdemItensTarefa> ordenacao = ordemService.getOrdenacaoByProduto(prod.getProduto().getId());

		for (OrdemItensTarefa ordemItem : ordenacao) {
			List<ItemValores> itens = itemValoresService.findByItemTarefaAndProducao(ordemItem.getItem().getId(),
					ordemItem.getTarefa().getId(), prod.getId());
			for (ItemValores item : itens) {
				item.setNumeroOrdem(ordemItem.getNumero());

				itemValoresService.update(item);
			}
		}

		modelAndView.setViewName("redirect:/producoes/update/" + prod.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/addSemContrato")
	public ModelAndView addSemContrato(@Valid Producao producao,
			@RequestParam(value = "produtoId", required = false) Long idProduto,
			@RequestParam(value = "clienteId") Long clienteId,
			@RequestParam(value = "comercialResponsavelEntrada", required = false) Long idComEntry,
			@RequestParam(value = "comercialResponsavelEfetivo", required = false) Long idComEffect,
			@RequestParam(value = "estimativaComercial", required = false) String estimativa,
			@RequestParam("tipoContrato") String tipoContrato,
			@RequestParam(value = "contratoId", required = false) Long contratoId,
			@RequestParam(value = "vigenciaInit", required = false) String vigenciaInit) {

		ModelAndView modelAndView = new ModelAndView();

		if (tipoContrato.equalsIgnoreCase("temp")) {

			Contrato contratoTemp = new Contrato();

			VigenciaIndeterminada vigencia = new VigenciaIndeterminada();
			vigencia.setNome("Vig�ncia Indeterminada");
			vigencia.setInicioVigencia(vigenciaInit);
			vigencia.setDataEncerramentoContrato(null);

			Cliente cliente = ClienteService.findById(clienteId);

			contratoTemp.setCnpjFaturar(cliente.getCnpj());
			contratoTemp.setRazaoSocialFaturar(cliente.getRazaoSocial());
			contratoTemp.setCliente(cliente);
			contratoTemp.setProduto(ProdutoService.findById(idProduto));
			contratoTemp.setComercialResponsavelEntrada(consultorService.findById(idComEntry));
			contratoTemp.setComercialResponsavelEfetivo(consultorService.findById(idComEffect));
			contratoTemp.setEstimativaComercial(estimativa);
			contratoTemp.setVigencia(vigencia);
			contratoTemp.setTemporario(true);

			ContratoService.create(contratoTemp);

			producao.setContrato(ContratoService.getLastAdded());

		} else if (tipoContrato.equalsIgnoreCase("anterior")) {

			producao.setContrato(ContratoService.findById(contratoId));

		}

		producao.setProduto(ProdutoService.findById(producao.getContrato().getProduto().getId()));
		producao.setContratoTemporario(true);

		producaoService.create(producao);

		Producao prod = new Producao();
		prod = producaoService.getLastItemAdded();

		etapasConclusaoService.createEtapasConclusaoByProd(prod);
		producaoService.createItemValores(prod);

		modelAndView.setViewName("redirect:/producoes/update/" + prod.getId());

		return modelAndView;

	}

	@GetMapping("/listProducoes/{id}")
	public ModelAndView findProducoesId(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/producao/listProducoes");

		mav.addObject("producao", this.producaoService.findById(id));

		return mav;
	}

	@GetMapping("update/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Producao producao = producaoService.findById(id);

		Produto produto = new Produto();
		produto = producao.getProduto();

		List<EtapaTrabalho> etapaTrabalho = new ArrayList<EtapaTrabalho>();
		etapaTrabalho = produto.getEtapasTrabalho();

		Collections.sort(etapaTrabalho, new Comparator<EtapaTrabalho>() {
			@Override
			public int compare(EtapaTrabalho o1, EtapaTrabalho o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});

		/*
		 * List<ItemTarefa> itens = new ArrayList<>();
		 * itens.addAll(ItemTarefaService.getItensByTarefaId(6l)); for (ItemTarefa
		 * itemTarefa : itens) { System.out.println(itemTarefa.getNome()); }
		 */
		
		Cliente cliente = producao.getContrato().getCliente();
		List<PreProducao> preProd = preProducaoService.getAllByCliente(cliente.getId());
		modelAndView.addObject("preProd",preProd);
		modelAndView.addObject("etapa", etapaTrabalho);
		modelAndView.addObject("produto", produto);
		modelAndView.addObject("producao", producao);
		modelAndView.addObject("cliente", cliente);

		modelAndView.setViewName("/producao/update");

		return modelAndView;
	}

	@GetMapping("updateContrato/{id}")
	public ModelAndView changeContratoById(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Producao producao = producaoService.findById(id);

		Produto produto = new Produto();
		produto = producao.getProduto();

		List<EtapaTrabalho> etapaTrabalho = new ArrayList<EtapaTrabalho>();
		etapaTrabalho = produto.getEtapasTrabalho();

		Collections.sort(etapaTrabalho, new Comparator<EtapaTrabalho>() {
			@Override
			public int compare(EtapaTrabalho o1, EtapaTrabalho o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});

		/*
		 * List<ItemTarefa> itens = new ArrayList<>();
		 * itens.addAll(ItemTarefaService.getItensByTarefaId(6l)); for (ItemTarefa
		 * itemTarefa : itens) { System.out.println(itemTarefa.getNome()); }
		 */

		Cliente cliente = producao.getContrato().getCliente();

		List<Contrato> contratos = ContratoService.getContratosByProdutoAndCliente(producao.getProduto().getId(),
				producao.getAno(), producao.getCliente().getId());

		modelAndView.addObject("etapa", etapaTrabalho);
		modelAndView.addObject("produto", produto);
		modelAndView.addObject("producao", producao);
		modelAndView.addObject("cliente", cliente);
		modelAndView.addObject("contratos", contratos);

		modelAndView.setViewName("/producao/updateContrato");

		return modelAndView;
	}

	@PostMapping("salvaContrato/{idProducao}/{idContrato}")
	public ModelAndView updateContrato(@PathVariable("idProducao") Long producaoId,
			@PathVariable("idContrato") Long contratoId) {

		ModelAndView modelAndView = new ModelAndView();

		System.out.println("o id do contrato �: " + contratoId);
		Producao producao = producaoService.findById(producaoId);
		Contrato contrato = ContratoService.findById(contratoId);
		List<Faturamento> faturamentos = faturamentoService.getAllByProducao(producaoId);
		// if(Objects.isNull(faturamentos)) {
		producao.setContrato(contrato);

		producaoService.update(producao);
		// modelAndView.setViewName("redirect:/producoes/updateContrato/" +
		// producao.getId());
		// return modelAndView;
		// }else {
		modelAndView.setViewName("redirect:/producoes/updateContrato/" + producao.getId());

		return modelAndView;
		// }

	}

	@GetMapping("/gerencial/{id}")
	public ModelAndView findgerencialId(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Producao producao = producaoService.findById(id);

		modelAndView.addObject("producao", producao);
		modelAndView.addObject("contrato", producao.getContrato());
		modelAndView.addObject("filiais", filialService.getAll());
		modelAndView.addObject("equipe", equipeService.getAll());
		modelAndView.addObject("consultores", consultorService.getConsultores());
		modelAndView.setViewName("/producao/gerencial");

		return modelAndView;

	}

	@PostMapping("/gerencial/update")
	public ModelAndView updateGerencial(@Valid Producao gerencial) {

		ModelAndView modelAndView = new ModelAndView();

		producaoService.update(gerencial);

		modelAndView.setViewName("redirect:/producoes/gerencial/" + gerencial.getId());

		return modelAndView;

	}

	@GetMapping("/operacional/{id}")
	public ModelAndView findOperacionalId(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		BalanceteCalculo balancete = balanceteCalculoService.getLastBalanceteCalculoByProducao(id);

		Boolean status = false;

		Producao producao = producaoService.findById(id);

		if (Objects.nonNull(balancete)) {
			status = true;
		}
		// EX-TARIF�RIO
		if (producao.getProduto().getId() == 28) {
			modelAndView.addObject("motivo", MotivoInatividadeExTarifario.values());
		}
		// LEI DO BEM || LB RETROATIVA
		if (producao.getProduto().getId() == 23 || producao.getProduto().getId() == 32) {
			modelAndView.addObject("motivo", MotivoInatividadeLeiDoBem.values());
		}
		// ROTA 2030 II
		if (producao.getProduto().getId() == 25) {
			modelAndView.addObject("motivo", MotivoInatividadeRotaII.values());
		}
		// ROTA 2030 III
		if (producao.getProduto().getId() == 38) {
			modelAndView.addObject("motivo", MotivoInatividadeRotaIII.values());
		}
		// LEI DA INFORM�TICA || LI RETROATIVA
		if (producao.getProduto().getId() == 24 || producao.getProduto().getId() == 40) {
			modelAndView.addObject("motivo", MotivoInatividadeLI.values());
		}
		// AUDITORIA CVM
		if (producao.getProduto().getId() == 31) {
			modelAndView.addObject("motivo", MotivoInatividadeCVM.values());
		}
		// F.REEMBOSAV�L
		if (producao.getProduto().getId() == 29) {
			modelAndView.addObject("motivo", MotivoInatividadeFinanciamento.values());
		}
		// FINANCIAMENTO SUBVEN��O
		if (producao.getProduto().getId() == 39) {
			modelAndView.addObject("motivo", MotivoInatividadeFinanciamentoSub.values());
		}
		// LB PREJUIZO || LB DEFESA || AUDITORIA LB
		if (producao.getProduto().getId() == 26 || producao.getProduto().getId() == 27
				|| producao.getProduto().getId() == 37) {
			modelAndView.addObject("motivo", MotivoInatividadeLB.values());
		}
		// ATIVO INTANGIVEL
		if (producao.getProduto().getId() == 41) {
			modelAndView.addObject("motivo", MotivoInatividadeAtivoIntangivel.values());
		}
		// IMPORTA��O � INOVA��O
		if (producao.getProduto().getId() == 33) {
			modelAndView.addObject("motivo", MotivoInatividadeImportInov.values());
		}
		// FINAME
		if (producao.getProduto().getId() == 30) {
			modelAndView.addObject("motivo", MotivoInatividadeFiname.values());
		}

		modelAndView.addObject("producao", producao);
		modelAndView.addObject("balanceteChecker", status);
		modelAndView.addObject("contrato", producao.getContrato());
		modelAndView.addObject("situacao", Situacao.values());
		modelAndView.addObject("tipoDeApuracao", TipoDeApuracao.values());
		modelAndView.setViewName("/producao/operacional");

		return modelAndView;

	}

	@PostMapping("/operacional/update")
	public ModelAndView updateOperacional(@Valid Producao operacional,
			@RequestParam("caminhoPasta") String caminhoPasta) {

		ModelAndView modelAndView = new ModelAndView();

		Producao prodAtt = producaoService.findById(operacional.getId());

		Cliente clienteAtt = ClienteService.findById(prodAtt.getCliente().getId());
		clienteAtt.setCaminhoPasta(caminhoPasta);

		ClienteService.update(clienteAtt);

		prodAtt.setSituacao(operacional.getSituacao());
		 prodAtt.setMotivoInatividade(operacional.getMotivoInatividade());
		prodAtt.setTipoDeNegocio(operacional.getTipoDeNegocio());
		if (operacional.getTipoDeApuracao() != null) {
			prodAtt.setTipoDeApuracao(operacional.getTipoDeApuracao());
		}
		prodAtt.setRazaoInatividade(operacional.getRazaoInatividade());
		prodAtt.setUltimoContato(operacional.getUltimoContato());
		prodAtt.setObservacoes(operacional.getObservacoes());
		producaoService.updateOperacional(prodAtt);

		modelAndView.setViewName("redirect:/producoes/operacional/" + operacional.getId());

		return modelAndView;

	}

	@GetMapping("{idProd}/{idEtapa}")
	public ModelAndView findTarefaId(@PathVariable("idEtapa") Long idEtapa, @PathVariable("idProd") Long idProd) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.getEtapasConclusaoByProducaoAndEtapa(idProd, idEtapa);
		System.out.println(etapas.getId());
		modelAndView.setViewName("redirect:/producoes/" + etapas.getId() + "/v2");

		return modelAndView;
	}

	@GetMapping("/{idEtapa}/{idProducao}/etapas.json")
	public @ResponseBody EtapasConclusao sendEtapas(@PathVariable("idEtapa") Long idEtapa,
			@PathVariable("idProducao") Long id) {
		return etapasConclusaoService.getEtapasConclusaoByProducaoAndEtapa(idProd, idEtapa);
	}

	@GetMapping("/{idProducao}/prod.json")
	public @ResponseBody Producao getProducao(@PathVariable("idProducao") Long id) {
		return producaoService.findById(id);
	}

	@GetMapping("/all.json")
	public @ResponseBody List<Producao> producaoJson() {

		return producaoService.getAll();

	}

	@GetMapping("/listas.json")
	public @ResponseBody List<ValoresLista> listaJson() {

		return ValoresListaService.getAll();

	}

	@GetMapping("/valores.json")
	public @ResponseBody List<ItemValores> valoresJson() {
		return itemValoresService.getAll();

	}

	@GetMapping("/consultores.json")
	public @ResponseBody List<Consultor> consultoresJson() {
		System.out.println("Server-side Alert: Recebi a solicita��o de consultores");
		return consultorService.getConsultores();

	}

	@GetMapping("/itemValores/{id}.json")
	public @ResponseBody List<ItemValores> valoresJsonProducao(@PathVariable("id") String id) {
		System.out.println("Server-side Alert: Recebi a solicita��o de itens de tarefa");
		return itemValoresService.getAllbyProducao(Long.parseLong(id));

	}

	@GetMapping("/itemValoresSubProd/{id}.json")
	public @ResponseBody List<ItemValores> valoresJsonSubProducao(@PathVariable("id") String id) {
		return itemValoresService.getAllbySubProducao(Long.parseLong(id));

	}

	@GetMapping("/balancete.json")
	public @ResponseBody List<BalanceteCalculo> balancete() {

		return balanceteCalculoService.getAll();

	}

	@Transactional
	@PostMapping("/{idEtapa}/salvaItens")
	@ResponseBody
	public void salvaItens(@RequestBody List<ItemValores> ItemValores, @PathVariable("idEtapa") Long idEtapa) {
		for (ItemValores item : ItemValores) {

			// ! -- SALVA O ITEM ITERADO -- ! //
			ItemValores itemAtt = itemValoresService.findById(item.getId());
			itemAtt.setValor(item.getValor());
			if (item.getValor() != null && !item.getValor().isEmpty()) {
				System.out.println("ITEM PREENCHIDO - CATCHING DATE");
				itemAtt.setDataRealizado(java.time.LocalDate.now().toString());
			}

			itemValoresService.update(itemAtt);
			// valor para actualizar en punto de situacion
			String valor = itemAtt.getValor();
			// tarefa_id = 15, 15 es punto situacion.
			Long tarefa_id_PuntoSituacion = 15L;
			// item_id relacion de campos con punto de situacion su refeljo
			Long item_id = itemAtt.getItem().getId();
			// producao_id producto al que le vamos a reflejar el campo en punto de situacion
			Long producao_id = itemAtt.getProducao().getId();
			//actualizar en punto situacion
			itemValoresService.updatePuntoSituacion(valor, producao_id, item_id, tarefa_id_PuntoSituacion);

			Producao producao = itemAtt.getProducao();
			SubProducao subProducao = itemAtt.getSubProducaoIndex();
			Contrato contrato = producao.getContrato();
			EtapasConclusao etapaConclusao = etapasConclusaoService
					.getEtapasConclusaoByProducaoAndEtapa(itemAtt.getProducao().getId(), idEtapa);

			// GERA FATURAMENTO OUTROS PRODUTOS
			switch (producao.getProduto().getNome()) {

			case "LEI DE INFORM�TICA":
				if (itemAtt.getItem().getId() == 560l && itemAtt.getValor() != "") {
					if (contrato.isPagamentoEntrega()) {
						if (contrato.getEtapasPagamento().getEtapa1() != null) {
							faturamentoService.geraFaturamentoLI(producao,
									contrato.getEtapasPagamento().getPorcentagem1(), etapaConclusao.getEtapa(),
									etapaConclusao);
						}
						if (contrato.getEtapasPagamento().getEtapa2() != null) {
							faturamentoService.geraFaturamentoLI(producao,
									contrato.getEtapasPagamento().getPorcentagem2(), etapaConclusao.getEtapa(),
									etapaConclusao);
						}
						if (contrato.getEtapasPagamento().getEtapa3() != null) {
							faturamentoService.geraFaturamentoLI(producao,
									contrato.getEtapasPagamento().getPorcentagem3(), etapaConclusao.getEtapa(),
									etapaConclusao);
						}

//						faturamentoService.aplicaLimitacao(producao.getId());

					}
				}
				break;
			case "EX-TARIF�RIO":
				// Trigger Mapeamento
//				if (itemAtt.getTarefa().getId() == 232l && itemAtt.getItem().getId() == 363l
//						&& itemAtt.getValor() == "true") {
//					faturamentoService.geraFaturamentoExTarifario("Mapeamento", itemAtt.getProducao(), subProducao,
//							null);
//				}
				// Trigger Envio do pleito
				if (itemAtt.getTarefa().getId() == 236l && itemAtt.getItem().getId() == 667l
						&& itemAtt.getValor() == "true") {
					faturamentoService.geraFaturamentoExTarifario("Envio do pleito", itemAtt.getProducao(), subProducao,
							null);
				}
				// Trigger Acompanhamento
				if (itemAtt.getTarefa().getId() == 240l && itemAtt.getItem().getId() == 363l
						&& itemAtt.getValor() == "true") {
					faturamentoService.geraFaturamentoExTarifario("Acompanhamento", itemAtt.getProducao(), subProducao,
							null);
				}
				// Trigger Aprova��o
				if (itemAtt.getTarefa().getId() == 240l && itemAtt.getItem().getId() == 674l
						&& itemAtt.getValor().equalsIgnoreCase("APROVADO")) {
					faturamentoService.geraFaturamentoExTarifario("Aprova��o", itemAtt.getProducao(), subProducao,
							null);
				}
				break;
			case "FINANCIAMENTO REEMBOLS�VEL":
				// Trigger Mapeamento
				if (itemAtt.getTarefa().getId() == 247l && itemAtt.getItem().getId() == 363l
						&& itemAtt.getValor() == "true") {
					faturamentoService.geraFaturamentoFinanciamento("Mapeamento", itemAtt.getProducao(), subProducao);
				}
				// Trigger Envio do pleito
				if (itemAtt.getTarefa().getId() == 250l && itemAtt.getItem().getId() == 703l
						&& itemAtt.getValor() != "") {
					faturamentoService.geraFaturamentoFinanciamento("Envio do pleito", itemAtt.getProducao(),
							subProducao);
				}
				// Trigger Aprova��o/libera��o
				if (itemAtt.getTarefa().getId() == 241l && itemAtt.getItem().getId() == 363l
						&& itemAtt.getValor() == "true") {
					faturamentoService.geraFaturamentoFinanciamento("Aprova��o/libera��o", itemAtt.getProducao(),
							subProducao);
				}
				// Trigger Acompanhamento/parcelas
				if (itemAtt.getTarefa().getId() == 240l && itemAtt.getItem().getId() == 674l
						&& itemAtt.getValor().equalsIgnoreCase("APROVADO")) {
					faturamentoService.geraFaturamentoFinanciamento("Acompanhamento/parcelas", itemAtt.getProducao(),
							subProducao);
				}
				break;
			}

			EtapasPagamento etapasPagamento = etapaConclusao.getProducao().getContrato().getEtapasPagamento();

			Long etapa = idEtapa;
			Long contratoId = etapaConclusao.getProducao().getContrato().getId();

			// ! -- ATUALIZA CONCULUS�O DE ETAPA -- ! //
			if (itemAtt.getItem().isDefineRealizacao() && item.getValor() == "true") {
				etapaConclusao.setConcluido(true);

				try {
					if (etapasPagamento.getEtapa1() != null) {
						if (etapasPagamento.getEtapa1().getId() == idEtapa) {
							List<Faturamento> fats = faturamentoService
									.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
							if (fats != null) {
								for (Faturamento faturamentoAtt : fats) {
									if (!faturamentoAtt.isLiberado()) {
										faturamentoAtt.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
										faturamentoService.update(faturamentoAtt);
									}
								}
							}
						}
					}
					if (etapasPagamento.getEtapa2() != null) {
						if (etapasPagamento.getEtapa2().getId() == idEtapa) {
							List<Faturamento> fats = faturamentoService
									.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
							if (fats != null) {
								for (Faturamento faturamentoAtt : fats) {
									if (!faturamentoAtt.isLiberado()) {
										faturamentoAtt.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
										faturamentoService.update(faturamentoAtt);
									}
								}
							}
						}
					}
					if (etapasPagamento.getEtapa3() != null) {
						if (etapasPagamento.getEtapa3().getId() == idEtapa) {
							List<Faturamento> fats = faturamentoService
									.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
							if (fats != null) {
								for (Faturamento faturamentoAtt : fats) {
									if (!faturamentoAtt.isLiberado()) {
										faturamentoAtt.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
										faturamentoService.update(faturamentoAtt);
									}
								}
							}
						}
					}
					if (etapasPagamento.getEtapa4() != null) {
						if (etapasPagamento.getEtapa4().getId() == idEtapa) {
							List<Faturamento> fats = faturamentoService
									.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
							if (fats != null) {
								for (Faturamento faturamentoAtt : fats) {
									if (!faturamentoAtt.isLiberado()) {
										faturamentoAtt.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
										faturamentoService.update(faturamentoAtt);
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				etapasConclusaoService.update(etapaConclusao);

				if (itemAtt.isSubProducao()) {
					SubProducao subProd = itemAtt.getSubProducaoIndex();
					if (itemAtt.getValor() == "true") {
						subProd.setFinalizado(true);
						subProducaoService.update(subProd);
					} else {
						subProd.setFinalizado(false);
						subProducaoService.update(subProd);
					}
				}
				// DESCONCLUI ETAPA E ATUALIZA FATURAMENTO
			} else if (itemAtt.getItem().isDefineRealizacao() && item.getValor() == "false") {

				System.out.println(etapaConclusao.getId());

				etapaConclusao.setConcluido(false);

				System.out.println(etapaConclusao.isConcluido());

				etapasConclusaoService.update(etapaConclusao);
				System.out.println("Entrou e desconcluiu");

				if (etapasPagamento.getEtapa1() != null) {
					if (etapasPagamento.getEtapa1().getId() == idEtapa) {
						List<Faturamento> fats = faturamentoService
								.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
						if (fats != null) {
							for (Faturamento faturamentoAtt : fats) {
								if (!faturamentoAtt.isLiberado()) {
									faturamentoAtt.setEstado(EstadoEtapa.AGUARDANDO_ETAPA.toString());
									faturamentoService.update(faturamentoAtt);
								}
							}
						}
					}
				}
				if (etapasPagamento.getEtapa2() != null) {
					if (etapasPagamento.getEtapa2().getId() == idEtapa) {
						List<Faturamento> fats = faturamentoService
								.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
						if (fats != null) {
							for (Faturamento faturamentoAtt : fats) {
								if (!faturamentoAtt.isLiberado()) {
									faturamentoAtt.setEstado(EstadoEtapa.AGUARDANDO_ETAPA.toString());
									faturamentoService.update(faturamentoAtt);
								}
							}
						}
					}
				}
				if (etapasPagamento.getEtapa3() != null) {
					if (etapasPagamento.getEtapa3().getId() == idEtapa) {
						List<Faturamento> fats = faturamentoService
								.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
						if (fats != null) {
							for (Faturamento faturamentoAtt : fats) {
								if (!faturamentoAtt.isLiberado()) {
									faturamentoAtt.setEstado(EstadoEtapa.AGUARDANDO_ETAPA.toString());
									faturamentoService.update(faturamentoAtt);
								}
							}
						}
					}
				}
				if (etapasPagamento.getEtapa4() != null) {
					if (etapasPagamento.getEtapa4().getId() == idEtapa) {
						List<Faturamento> fats = faturamentoService
								.getFaturamentoByProducaoEtapaContrato(producao.getId(), etapa, contratoId);
						if (fats != null) {
							for (Faturamento faturamentoAtt : fats) {
								if (!faturamentoAtt.isLiberado()) {
									faturamentoAtt.setEstado(EstadoEtapa.AGUARDANDO_ETAPA.toString());
									faturamentoService.update(faturamentoAtt);
								}
							}
						}
					}
				}
			}

			// CONCLUI SUBPRODUCAO CASO FINALIZADO
			if (itemAtt.isSubProducao()) {
				SubProducao subProd = itemAtt.getSubProducaoIndex();
				if (itemAtt.getItem().getNome().equalsIgnoreCase("FINALIZADO")) {
					if (itemAtt.getValor() == "true") {
						subProd.setFinalizado(true);
						subProducaoService.update(subProd);
					} else {
						subProd.setFinalizado(false);
						subProducaoService.update(subProd);
					}
				}
			}
		}
	}

	@GetMapping("/obrigacoes/{id}")
	public ModelAndView obrigacoes(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("cliente", clienteDaProd);
		mav.addObject("etapa", etapaTrabalhoService.findById(etapaId));
		mav.addObject("obrigacoes", obrigacoesPeriodicasService.getObrigacoesByProducao(id));
		mav.addObject("FNDCTCondic", obrigacoesPeriodicasService.getFNDCTbyProducaoId(id));
		mav.addObject("producao", producaoService.findById(id));
		mav.addObject("consultores", consultorService.getConsultorByCargo(Cargo.CONSULTOR_TECNICO));
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("FNDCT", fndcTservice.getFNDCTByProducao(id));
		mav.setViewName("/producao/obrigacoes");
		return mav;
	}

	@GetMapping("/obrigacoesUpdate/{id}")
	public ModelAndView obrigacoesUpdate(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("obrigacao", obrigacoesPeriodicasService.findById(id));
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.setViewName("/producao/obrigacoesUpdate");
		return mav;
	}

	@Transactional
	@PostMapping("/obrigacoes/add")
	public ModelAndView addObrigacoes(@Valid ObrigacoesPeriodicas obrigacao, @RequestParam("idProducao") Long idProd) {

		ModelAndView modelAndView = new ModelAndView();

		obrigacao.setProducao(producaoService.findById(idProd));
		System.out.println(obrigacao.getFeitoPor());

		obrigacoesPeriodicasService.create(obrigacao);

		modelAndView.setViewName("redirect:/producoes/obrigacoes/" + obrigacao.getProducao().getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/obrigacoes/update")
	public ModelAndView updateObrigacoes(@Valid ObrigacoesPeriodicas obrigacao) {

		ModelAndView modelAndView = new ModelAndView();
		ObrigacoesPeriodicas obri = obrigacoesPeriodicasService.update(obrigacao);

		modelAndView.setViewName("redirect:/producoes/obrigacoes/" + obri.getProducao().getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/FNDCT/add")
	public ModelAndView addFNDCT(@Valid FNDCT fndct, @RequestParam("idProducao") Long idProd) {

		ModelAndView modelAndView = new ModelAndView();
		fndct.setProducao(producaoService.findById(idProd));
		fndcTservice.update(fndct);
		modelAndView.setViewName("redirect:/producoes/obrigacoes/" + fndct.getProducao().getId());

		return modelAndView;
	}

	@GetMapping("/balancete/{id}")
	public ModelAndView balancete(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(id);
		boolean temFinal = false;

		float rh = 0;
		float terceiros = 0;
		float materiais = 0;
		float despesas = 0;
		float beneficio = 0;

		List<BalanceteCalculo> balancetes = balanceteCalculoService
				.getBalanceteCalculoByProducao(etapas.getProducao().getId());

		for (BalanceteCalculo balanceteCalculo : balancetes) {
			if (balanceteCalculo.isVersaoFinal()) {
				temFinal = true;
			}

			float balanceteRH = 0;
			float balanceteTerceiros = 0;
			float balanceteMateriais = 0;
			float balanceteDespesas = 0;
			float balanceteBeneficio = 0;

			try {

				if (!balanceteCalculo.getValorServicosTerceiros().isEmpty()) {
					balanceteTerceiros = Float.parseFloat(
							balanceteCalculo.getValorServicosTerceiros().replace(".", "").replace(",", "."));
				}
				if (!balanceteCalculo.getValorMateriais().isEmpty()) {
					balanceteMateriais = Float
							.parseFloat(balanceteCalculo.getValorMateriais().replace(".", "").replace(",", "."));
				}
				if (!balanceteCalculo.getValorTotal().isEmpty()) {
					balanceteDespesas = Float
							.parseFloat(balanceteCalculo.getValorTotal().replace(".", "").replace(",", "."));
				}
				if (!balanceteCalculo.getReducaoImposto().isEmpty()) {
					balanceteBeneficio = Float
							.parseFloat(balanceteCalculo.getReducaoImposto().replace(".", "").replace(",", "."));
				}
				if (!balanceteCalculo.getValorRH().isEmpty()) {
					balanceteRH = Float.parseFloat(balanceteCalculo.getValorRH().replace(".", "").replace(",", "."));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			rh += balanceteRH;
			terceiros += balanceteTerceiros;
			materiais += balanceteMateriais;
			despesas += balanceteDespesas;
			beneficio += balanceteBeneficio;

		}

		ItemValores itemValor = itemValoresService.getAliquotaByProducao(etapas.getProducao().getId());

		BalanceteCalculo balancete = balanceteCalculoService
				.getLastBalanceteCalculoByProducao(etapas.getProducao().getId());

		Boolean statusContatoNF = false;
		Boolean statusContrato = true;
		Boolean versaoAnterior = false;

		if (Objects.nonNull(balancete)) {
			if (!balancete.isValidado()) {
				System.out.println("O balancete anterior n�o foi validado.");
			} else {
				System.out.println("Balancete anterior foi validado.");
				versaoAnterior = true;
			}
		} else {
			versaoAnterior = true;
		}

		List<ContatoCliente> contatos = contatoClienteService
				.getContatosCliente(etapas.getProducao().getCliente().getId());
		for (ContatoCliente contatoCliente : contatos) {
			if (contatoCliente.isPessoaNfBoleto()) {
				statusContatoNF = true;
			}
		}

		mav.addObject("rh", rh);
		mav.addObject("terceiros", terceiros);
		mav.addObject("materiais", materiais);
		mav.addObject("despesas", despesas);
		mav.addObject("beneficio", beneficio);
		mav.addObject("temFinal", temFinal);
		mav.addObject("etapasId", etapas.getId());
		mav.addObject("statusContrato", statusContrato);
		mav.addObject("statusContatoNF", statusContatoNF);
		mav.addObject("versaoAnterior", versaoAnterior);
		mav.addObject("cliente", etapas.getProducao().getCliente());
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("aliquota", itemValor.getValor());
		mav.addObject("tiposProjeto", TipoProjeto.values());
		mav.addObject("producao", etapas.getProducao());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("balancete", balanceteCalculoService.getBalanceteCalculoByProducao(etapas.getProducao().getId()));
		mav.addObject("relatorioCalculo", RelatorioCalculo.values());
		mav.addObject("exclusaoUtilizada", ExclusaoUtilizada.values());
		mav.setViewName("/producao/balancete");
		return mav;
	}

	@GetMapping("/balanceteUpdate/{id}")
	public ModelAndView balanceteUpdate(@PathVariable("id") String idBalStr, @RequestParam("idEtapa") Long idEtapa) {
		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapa);

		Long id = Long.parseLong(idBalStr);

		BalanceteCalculo balanceteAtt = balanceteCalculoService.findById(id);

		Producao prod = balanceteAtt.getProducao();
		ItemValores itemValor = itemValoresService.getAliquotaByProducao(prod.getId());

		BalanceteCalculo balancete = balanceteCalculoService.getLastBalanceteCalculoByProducao(prod.getId());

		boolean temFinal = false;

		List<BalanceteCalculo> balancetes = balanceteCalculoService.getBalanceteCalculoByProducao(prod.getId());
		for (BalanceteCalculo balanceteCalculo : balancetes) {
			if (balanceteCalculo.isVersaoFinal()) {
				temFinal = true;
			}
		}

		Boolean statusContatoNF = false;
		Boolean statusContrato = true;
		Boolean versaoAnterior = false;

		if (Objects.nonNull(balancete)) {
			if (balanceteAtt.getId() == balancete.getId()) {
				versaoAnterior = true;
			}
			if (!balancete.isValidado()) {
				System.out.println("O balancete anterior n�o foi validado.");
			} else {
				System.out.println("Balancete anterior foi validado.");
				versaoAnterior = true;
			}
		} else {
			versaoAnterior = true;
		}

		List<ContatoCliente> contatos = contatoClienteService.getContatosCliente(prod.getCliente().getId());
		for (ContatoCliente contatoCliente : contatos) {
			if (contatoCliente.isPessoaNfBoleto()) {
				statusContatoNF = true;
			}
		}

		System.out.println(temFinal);

		mav.addObject("temFinal", temFinal);
		mav.addObject("etapasId", idEtapa);
		mav.addObject("statusContrato", statusContrato);
		mav.addObject("statusContatoNF", statusContatoNF);
		mav.addObject("versaoAnterior", versaoAnterior);
		mav.addObject("balancete", balanceteAtt);

		mav.addObject("cliente", etapas.getProducao().getCliente());
		mav.addObject("etapa", etapas.getEtapa());

		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("relatorioCalculo", RelatorioCalculo.values());
		mav.addObject("exclusaoUtilizada", ExclusaoUtilizada.values());
		mav.addObject("producao", prod);
		mav.addObject("aliquota", itemValor.getValor());
		mav.setViewName("/producao/balanceteUpdate");
		return mav;

	}

	@Transactional
	@PostMapping("/balancete/add")
	public ModelAndView addBalancete(@Valid BalanceteCalculo balancete, @RequestParam("etapasId") Long etapasId) {
		
		EtapasConclusao etapas = etapasConclusaoService.findById(etapasId);

		ModelAndView mav = new ModelAndView();

		balancete.setProducao(etapas.getProducao());
		BalanceteCalculo balanceteCriado = balanceteCalculoService.create(balancete);

		Contrato contrato = etapas.getProducao().getContrato();

		if (balanceteCriado.isValidado()) {
			System.out.println("Entrou validado");
			if (contrato.isPagamentoEntrega()) {
				System.out.println("Entrou pagamento");
				if (contrato.getEtapasPagamento().getEtapa1() != null) {
					System.out.println("Entrou etapa 1");
					faturamentoService.geraFaturamento(balanceteCriado, contrato.getEtapasPagamento().getPorcentagem1(),
							contrato.getEtapasPagamento().getEtapa1(), etapas);
					//balanceteCriado.setGerouFaturamento(true);
				}
				if (contrato.getEtapasPagamento().getEtapa2() != null) {
					System.out.println("Entrou etapa 2");
					faturamentoService.geraFaturamento(balanceteCriado, contrato.getEtapasPagamento().getPorcentagem2(),
							contrato.getEtapasPagamento().getEtapa2(), etapas);
					//balanceteCriado.setGerouFaturamento(true);
				}
				if (contrato.getEtapasPagamento().getEtapa3() != null) {
					System.out.println("Entrou etapa 3");
					faturamentoService.geraFaturamento(balanceteCriado, contrato.getEtapasPagamento().getPorcentagem3(),
							contrato.getEtapasPagamento().getEtapa3(), etapas);
					//balanceteCriado.setGerouFaturamento(true);
				}

				balanceteCalculoService.update(balanceteCriado);
				//faturamentoService.aplicaLimitacao(etapas.getProducao().getId());

			}
		}

		if (balanceteCriado.isValidado()) {
			etapas.setConcluido(true);
			etapasConclusaoService.update(etapas);
		}
		
		
		mav.addObject("etapas", etapas);
		
		mav.setViewName("redirect:/producoes/FatLimiter?etapasId=" + etapas.getId());

		return mav;

	}

	@Transactional
	@PostMapping("/balancete/addPrejuizo")
	public @ResponseBody void addBalancetePrejuizo(@RequestBody JSONObject parametros) {

		Long producaoId = Long.parseLong(parametros.get("producaoId").toString());
		int versao = Integer.parseInt(parametros.get("versao").toString());
		int porcentagemRealizado = Integer.parseInt(parametros.get("porcentagemRealizado").toString());
		boolean prejuizo = Boolean.parseBoolean(parametros.get("prejuizo").toString());

		System.out.println("entrou no m�todo prejuizo");
		System.out.println("a vers�o �" + versao);
		System.out.println("o prejuizo �: " + prejuizo);
		System.out.println("o percentual realizado �: " + porcentagemRealizado);
		System.out.println("o id de produ��o �: " + producaoId);
		Producao producao = producaoService.findById(producaoId);

		BalanceteCalculo balancete = new BalanceteCalculo();
		balancete.setPrejuizo(prejuizo);
		balancete.setPorcentagemRealizado(porcentagemRealizado);
		balancete.setVersao(versao);
		balancete.setProducao(producao);
		balancete.setValidado(true);
		balancete.setValorTotal("0,00");
		balancete.setReducaoImposto("0,00");

		balanceteCalculoService.create(balancete);

	}

	@Transactional
	@PostMapping("/balancete/update")
	public ModelAndView balanceteProjetos(@Valid BalanceteCalculo balancete,
			@RequestParam(value = "etapasId") Long idEtapas,
			@RequestParam(value = "fatBoolean", required = false) boolean gerouFat) {

		ModelAndView modelAndView = new ModelAndView();
		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);
		BalanceteCalculo balanceteCriado = balanceteCalculoService.update(balancete);

		if (balancete.isPrejuizo()) {
			balancete.setValorTotal("0.0");
			balancete.setReducaoImposto("0,00");
			balancete.setValidado(true);
			balanceteCalculoService.update(balancete);
		} else {

			if (balanceteCriado.isValidado()) {
				etapas.setConcluido(true);
				etapasConclusaoService.update(etapas);
			}

			Contrato contrato = etapas.getProducao().getContrato();

			if (balanceteCriado.isValidado()) {
				etapas.setConcluido(true);
				etapasConclusaoService.update(etapas);
			} else {
				etapas.setConcluido(false);
				etapasConclusaoService.update(etapas);
			}

			if (balancete.isGerouFaturamento() == false) {
				if (balanceteCriado.isValidado()) {
					System.out.println("Entrou validado");
					if (contrato.isPagamentoEntrega()) {
						System.out.println("Entrou pagamento");
						if (contrato.getEtapasPagamento().getEtapa1() != null) {
							System.out.println("Entrou etapa 1");
							Faturamento fat = faturamentoService.geraFaturamento(balanceteCriado,
									contrato.getEtapasPagamento().getPorcentagem1(),
									contrato.getEtapasPagamento().getEtapa1(), etapas);
							if (Objects.nonNull(fat)) {
								balanceteCriado.setGerouFaturamento(true);
							}
						}
						if (contrato.getEtapasPagamento().getEtapa2() != null) {
							System.out.println("Entrou etapa 2");
							Faturamento fat = faturamentoService.geraFaturamento(balanceteCriado,
									contrato.getEtapasPagamento().getPorcentagem2(),
									contrato.getEtapasPagamento().getEtapa2(), etapas);
							if (Objects.nonNull(fat)) {
								balanceteCriado.setGerouFaturamento(true);
							}
						}
						if (contrato.getEtapasPagamento().getEtapa3() != null) {
							System.out.println("Entrou etapa 3");
							Faturamento fat = faturamentoService.geraFaturamento(balanceteCriado,
									contrato.getEtapasPagamento().getPorcentagem3(),
									contrato.getEtapasPagamento().getEtapa3(), etapas);
							if (Objects.nonNull(fat)) {
								balanceteCriado.setGerouFaturamento(true);
							}
						}
						if (contrato.getEtapasPagamento().getEtapa4() != null) {
							System.out.println("Entrou etapa 4");
							Faturamento fat = faturamentoService.geraFaturamento(balanceteCriado,
									contrato.getEtapasPagamento().getPorcentagem3(),
									contrato.getEtapasPagamento().getEtapa3(), etapas);
							if (Objects.nonNull(fat)) {
								balanceteCriado.setGerouFaturamento(true);
							}
						}

						balanceteCriado = balanceteCalculoService.update(balanceteCriado);

					}
				}
			}
		}

		modelAndView.setViewName("redirect:/producoes/FatLimiter?etapasId=" + etapas.getId());

		return modelAndView;

	}

	@GetMapping("/FatLimiter")
	public ModelAndView limitacao(@RequestParam("etapasId") Long EtapasId) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapa = etapasConclusaoService.findById(EtapasId);
//		faturamentoService.aplicaLimitacao(etapa.getProducao().getId());

		modelAndView.setViewName("redirect:/producoes/balancete/" + etapa.getId());
		return modelAndView;

	}

	@GetMapping("/projetos/{id}")
	public ModelAndView projetos(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(id);
		Cliente cliente = etapas.getProducao().getContrato().getCliente();

		mav.addObject("cliente", cliente);
		mav.addObject("etapa", etapaTrabalhoService.findById(etapas.getEtapa().getId()));
		mav.addObject("etapas", etapas);
		mav.addObject("tiposProjeto", TipoProjeto.values());
		mav.addObject("producao", producaoService.findById(etapas.getProducao().getId()));
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("projetos", projetosService.getProjetosByProducao(etapas.getProducao().getId()));
		mav.addObject("estado", Estado.values());
		mav.addObject("projetos", projetosService.getProjetosByProducao(etapas.getProducao().getId()));
		mav.setViewName("/producao/projetos");
		return mav;
	}

	@GetMapping("/projetosUpdate/{id}")
	public ModelAndView projetosUpdate(@PathVariable("id") Long id, @RequestParam("idEtapas") Long idEtapas) {
		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("producao", etapas.getProducao());
		mav.addObject("cliente", etapas.getProducao().getCliente());
		mav.addObject("projeto", projetosService.findById(id));
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("tiposProjeto", TipoProjeto.values());
		mav.addObject("estado", Estado.values());
		mav.addObject("etapas", etapas);
		mav.setViewName("/producao/projetosUpdate");
		return mav;
	}

	@Transactional
	@PostMapping("/projetos/add")
	public ModelAndView addProjeto(@Valid Projetos projeto, @RequestParam("idEtapas") Long idEtapa) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapa);

		projeto.setProducao(producaoService.findById(etapas.getProducao().getId()));
		projetosService.create(projeto);
		modelAndView.setViewName("redirect:/producoes/projetos/" + etapas.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/projetos/update")
	public ModelAndView updateProjetos(@Valid Projetos projetos, @RequestParam("idEtapas") Long idEtapa) {

		ModelAndView modelAndView = new ModelAndView();
		projetosService.update(projetos);

		modelAndView.setViewName("redirect:/producoes/projetos/" + idEtapa);

		return modelAndView;

	}

	@GetMapping("/relatorios/{id}")
	public ModelAndView relatorios(@PathVariable("id") Long id) {
		
		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(id);

		System.out.println(etapas.getProducao());
		System.out.println(etapas.getProducao().getContrato());
		System.out.println(etapas.getProducao().getContrato().getCliente());

		mav.addObject("cliente", etapas.getProducao().getContrato().getCliente());
		mav.addObject("etapa", etapaTrabalhoService.findById(etapas.getEtapa().getId()));
		mav.addObject("especialidade", Especialidade.values());
		mav.addObject("estado", Estado.values());
		mav.addObject("etapasId", etapas.getId());
		mav.addObject("producao", producaoService.findById(etapas.getProducao().getId()));
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("relatorios",
				relatorioTecnicoService.getRelatorioTecnicoByProducao(etapas.getProducao().getId()));
		mav.setViewName("/producao/relatoriosTecnicos");

		return mav;
	}

	@GetMapping("/relatoriosUpdate/{id}")
	public ModelAndView relatoriosUpdate(@PathVariable("id") Long id, @RequestParam("idEtapa") Long idEtapas) {
		ModelAndView mav = new ModelAndView();
		
		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);
		
		mav.addObject("etapasId", idEtapas);
		mav.addObject("etapa", etapaTrabalhoService.findById(etapas.getEtapa().getId()));
		mav.addObject("especialidade", Especialidade.values());
		mav.addObject("estado", Estado.values());
		mav.addObject("relatorio", relatorioTecnicoService.findById(id));
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("cliente", etapas.getProducao().getContrato().getCliente());
		mav.addObject("producao", producaoService.findById(etapas.getProducao().getId()));
		mav.setViewName("/producao/relatoriosUpdate");

		return mav;
	}

	@Transactional
	@RequestMapping("/relatoriosDelete/{id}")
	public ModelAndView relatorioDelete(@PathVariable("id") Long id, @RequestParam("etapasId") Long etapasId) {

		ModelAndView modelAndView = new ModelAndView();

		RelatorioTecnico relatorio = relatorioTecnicoService.findById(id);
		relatorioTecnicoService.delete(relatorio);
		modelAndView.setViewName("redirect:/producoes/relatorios/" + etapasId);

		return modelAndView;

	}

	@Transactional
	@PostMapping("/relatorios/add")
	public ModelAndView addrelatorios(@Valid RelatorioTecnico relatorio, @RequestParam("etapasId") Long etapaId) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(etapaId);

		relatorio.setProducao(etapas.getProducao());
		relatorioTecnicoService.create(relatorio);

		modelAndView.setViewName("redirect:/producoes/relatorios/" + etapas.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/relatorios/update")
	public ModelAndView updaterelatorios(@Valid RelatorioTecnico relatorio, @RequestParam("etapasId") Long etapasId) {

		ModelAndView modelAndView = new ModelAndView();
		relatorioTecnicoService.update(relatorio);

		modelAndView.setViewName("redirect:/producoes/relatorios/" + etapasId);

		return modelAndView;

	}

	// >> ------------------------------- ACOMPANHAMENTO
	// ------------------------------- <<\\

	@GetMapping("/acompanhamentos/{id}")
	public ModelAndView acompanhamentos(@PathVariable("id") Long id) {

		Producao producao = new Producao();
		producao = prod;
		etapaId = id;

		Produto produto = new Produto();
		produto = producao.getProduto();

		Cliente cliente = clienteDaProd;

		ModelAndView mav = new ModelAndView();
		mav.addObject("producao", producaoService.findById(id));
		mav.addObject("cliente", cliente);
		mav.addObject("produto", produto);
		mav.addObject("acompanhamentos", acompanhamentoService.getAcompanhamentoByProducao(id));
		mav.setViewName("/producao/acompanhamento");
		return mav;
	}

	@Transactional
	@PostMapping("/acompanhamentos/add")
	public ModelAndView addAcompanhamentos(@Valid Acompanhamento acompanhamento,
			@RequestParam("idProducao") Long idProd) {

		ModelAndView modelAndView = new ModelAndView();
		acompanhamento.setProducao(producaoService.findById(idProd));
		acompanhamentoService.create(acompanhamento);
		modelAndView.setViewName("redirect:/producoes/acompanhamentos/" + acompanhamento.getProducao().getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/acompanhamentos/update")
	public ModelAndView updatedAcompanhamentos(@Valid Acompanhamento acompanhamento) {

		ModelAndView modelAndView = new ModelAndView();
		acompanhamentoService.update(acompanhamento);
		modelAndView.setViewName("redirect:/producoes/acompanhamentos/" + acompanhamento.getProducao().getId());

		return modelAndView;

	}

	@GetMapping("/acompanhamentoTarefas/{id}")
	public ModelAndView acompanhamentosTarefas(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("acompanhamento", acompanhamentoService.findById(id));
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("estado", Estado.values());
		mav.addObject("resultado", Resultado.values());
		mav.setViewName("/producao/acompanhamentoTarefas");
		return mav;
	}

	@GetMapping("/{id}/etapas.json")
	public @ResponseBody List<EtapaTrabalho> getAllByProduto(@PathVariable("id") Long id) {

		System.out.println("entrou na controller");

		Produto prod = ProdutoService.findById(id);
		List<EtapaTrabalho> etapas = prod.getEtapasTrabalho();

		return etapas;

	}

	@GetMapping("/habilitacaoPortaria/{id}")
	public ModelAndView habilitacaoPortaria(@PathVariable("id") Long id) {

		EtapasConclusao etapas = etapasConclusaoService.findById(id);

		Producao producao = etapas.getProducao();
		Produto produto = producao.getProduto();

		Cliente cliente = producao.getContrato().getCliente();

		ModelAndView mav = new ModelAndView();
		mav.addObject("producao", producao);
		mav.addObject("etapas", etapas);
		mav.addObject("cliente", cliente);
		mav.addObject("produto", produto);
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("habilitacoes", habilitacaoPortariaService.getHabilitacaoPortariaByProducao(producao.getId()));
		mav.setViewName("/producao/habilitacaoPortaria");
		return mav;
	}

	@Transactional
	@PostMapping("/habilitacaoPortaria/add")
	public ModelAndView addHabilitacao(@Valid HabilitacaoPortaria habilitacao,
			@RequestParam("idEtapas") Long idEtapas) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		habilitacao.setProducao(etapas.getProducao());
		habilitacaoPortariaService.create(habilitacao);
		modelAndView.setViewName("redirect:/producoes/habilitacaoPortaria/" + etapas.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/habilitacaoPortaria/update")
	public ModelAndView updatedHabilitacao(@Valid HabilitacaoPortaria habilitacao,
			@RequestParam("idEtapas") Long idEtapas) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		habilitacaoPortariaService.update(habilitacao);
		modelAndView.setViewName("redirect:/producoes/habilitacaoPortaria/" + etapas.getId());

		return modelAndView;

	}

	@GetMapping("/habilitacaoUpdate/{id}")
	public ModelAndView habilitacaoUpdate(@PathVariable("id") Long id, @RequestParam("idEtapas") Long idEtapas) {
		ModelAndView mav = new ModelAndView();

		HabilitacaoPortaria habilitacao = habilitacaoPortariaService.findById(id);
		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		mav.addObject("cliente", habilitacao.getProducao().getContrato().getCliente());
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("etapas", etapas);
		mav.addObject("habilitacao", habilitacao);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("estado", Estado.values());
		mav.addObject("resultado", Resultado.values());
		mav.setViewName("/producao/habilitacaoPortariaUpdate");
		return mav;
	}

	@GetMapping("/auditoriaEconomica/{id}")
	public ModelAndView auditoriaEconomica(@PathVariable("id") Long id) {

		EtapasConclusao etapas = etapasConclusaoService.findById(id);

		Producao producao = etapas.getProducao();
		Produto produto = producao.getProduto();

		Cliente cliente = producao.getContrato().getCliente();

		ModelAndView mav = new ModelAndView();
		mav.addObject("producao", producao);
		mav.addObject("etapas", etapas);
		mav.addObject("cliente", cliente);
		mav.addObject("produto", produto);
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("auditorias", auditoriaEconomicaService.getAuditoriaEconomicaByProducao(producao.getId()));
		mav.setViewName("/producao/auditoriaEconomica");
		return mav;

	}

	@Transactional
	@PostMapping("/auditoriaEconomica/add")
	public ModelAndView addauditoriaEconomica(@Valid AuditoriaEconomica audit,
			@RequestParam("idEtapas") Long idEtapas) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		audit.setProducao(etapas.getProducao());
		auditoriaEconomicaService.create(audit);
		modelAndView.setViewName("redirect:/producoes/auditoriaEconomica/" + etapas.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/auditoriaEconomica/update")
	public ModelAndView updatedauditoriaEconomica(@Valid AuditoriaEconomica audit,
			@RequestParam("idEtapas") Long idEtapas) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);
		audit.setProducao(etapas.getProducao());

		auditoriaEconomicaService.update(audit);

		modelAndView.setViewName("redirect:/producoes/auditoriaEconomica/" + etapas.getId());

		return modelAndView;

	}

	@GetMapping("/auditoriaEconomicaUpdate/{id}")
	public ModelAndView auditoriaEconomicaUpdate(@PathVariable("id") Long id, @RequestParam("idEtapas") Long idEtapas) {
		ModelAndView mav = new ModelAndView();

		AuditoriaEconomica audit = auditoriaEconomicaService.findById(id);
		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		mav.addObject("cliente", audit.getProducao().getContrato().getCliente());
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("producao", audit.getProducao());
		mav.addObject("etapas", etapas);
		mav.addObject("auditoria", audit);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("estado", Estado.values());
		mav.addObject("resultado", Resultado.values());
		mav.setViewName("/producao/auditoriaEconomicaUpdate");
		return mav;
	}

	@GetMapping("/auditoriaProjetos/{id}")
	public ModelAndView auditoriaProjetos(@PathVariable("id") Long id) {

		EtapasConclusao etapas = etapasConclusaoService.findById(id);

		Producao producao = etapas.getProducao();
		Produto produto = producao.getProduto();

		Cliente cliente = producao.getContrato().getCliente();

		ModelAndView mav = new ModelAndView();
		mav.addObject("producao", producao);
		mav.addObject("etapas", etapas);
		mav.addObject("cliente", cliente);
		mav.addObject("produto", produto);
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("auditorias", auditoriaProjetosService.getAuditoriaProjetosByProducao(producao.getId()));
		mav.setViewName("/producao/auditoriaProjetos");
		return mav;

	}

	@Transactional
	@PostMapping("/auditoriaProjetos/add")
	public ModelAndView addAuditoriaProjetos(@Valid AuditoriaProjetos audit, @RequestParam("idEtapas") Long idEtapas) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		audit.setProducao(etapas.getProducao());
		auditoriaProjetosService.create(audit);
		modelAndView.setViewName("redirect:/producoes/auditoriaProjetos/" + etapas.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/auditoriaProjetos/update")
	public ModelAndView updatedAuditoriaProjetos(@Valid AuditoriaProjetos audit,
			@RequestParam("idEtapas") Long idEtapas) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);
		audit.setProducao(etapas.getProducao());

		auditoriaProjetosService.update(audit);

		modelAndView.setViewName("redirect:/producoes/auditoriaProjetos/" + etapas.getId());

		return modelAndView;

	}

	@GetMapping("/auditoriaProjetosUpdate/{id}")
	public ModelAndView AuditoriaProjetosUpdate(@PathVariable("id") Long id, @RequestParam("idEtapas") Long idEtapas) {
		ModelAndView mav = new ModelAndView();

		AuditoriaProjetos audit = auditoriaProjetosService.findById(id);
		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		mav.addObject("cliente", audit.getProducao().getContrato().getCliente());
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("producao", audit.getProducao());
		mav.addObject("etapas", etapas);
		mav.addObject("auditoria", audit);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("estado", Estado.values());
		mav.addObject("resultado", Resultado.values());
		mav.setViewName("/producao/auditoriaProjetosUpdate");
		return mav;
	}

	@GetMapping("/eficiencia/{id}")
	public ModelAndView eficienciaOperacao(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();

		Producao producao = producaoService.findById(id);
		List<EtapasConclusao> etapas = etapasConclusaoService.getAllEtapasConclusaoByProducao(id);

		mav.addObject("producao", producao);
		mav.addObject("etapas", etapas);

		mav.setViewName("/producao/eficiencia");

		return mav;
	}

	@PostMapping("/eficiencia/salva")
	@ResponseBody
	public void eficienciaSalvar(@RequestBody List<Eficiencia> atividades) {
		
		for (Eficiencia eficiencia : atividades) {
			EtapasConclusao etapas = etapasConclusaoService.getEtapasConclusaoByProducaoAndEtapa(
					eficiencia.getProducao().getId(), eficiencia.getEtapa().getId());
			etapas.setInclusaoHoras(true);
			etapasConclusaoService.update(etapas);

			Consultor consultor = eficiencia.getConsultor();
			float hora = (eficiencia.getHoras());
			Date dataApontamento = new Date();
			String data = new SimpleDateFormat("yyyy-MM-dd").format(dataApontamento);
			
			Eficiencia novaAtividade = new Eficiencia();
			
			novaAtividade.setId(eficiencia.getId());
			novaAtividade.setAtividade(eficiencia.getAtividade());
			novaAtividade.setConsultor(consultor);
			novaAtividade.setProducao(eficiencia.getProducao());
			novaAtividade.setEtapa(eficiencia.getEtapa());
			novaAtividade.setHoras(hora);
			novaAtividade.setDataApontamento(data);
			
			
			try {
				long id = eficiencia.getItemTarefa().getId();
				ItemTarefa itemTarefa = ItemTarefaService.findById(id);
				novaAtividade.setItemTarefa(itemTarefa);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(eficiencia.getTarefa().getId() == 0) {
				System.out.println("tarefa esta vazia, n�o seta nada");
			}else {
				novaAtividade.setTarefa(eficiencia.getTarefa());
			}
			eficienciaService.update(novaAtividade);
		}

	}

	@PostMapping("/eficiencia/deleta/{id}")
	@ResponseBody
	public void eficienciaDeletar(@PathVariable("id") Long id) {

		Eficiencia atividade = eficienciaService.findById(id);
		eficienciaService.delete(atividade);

	}

	@GetMapping("/getEficiencia/{id}.json")
	public @ResponseBody List<Eficiencia> getAllItensTarefa(@PathVariable("id") Long id) {

		return eficienciaService.getEficienciaByProducao(id);

	}

	@GetMapping("/subProd/{id}")
	public ModelAndView subProducoes(@PathVariable("id") Long id, @RequestParam("idEtapa") Long idEtapa) {
		ModelAndView mav = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Consultor consultor = ((Consultor) auth.getPrincipal());

		EtapasConclusao etapa = etapasConclusaoService.getEtapasConclusaoByProducaoAndEtapa(id, idEtapa);
		List<SubProducao> subs = subProducaoService.getSubProducaobyProducaoAndEtapa(etapa.getProducao().getId(),
				idEtapa);

		mav.addObject("etapas", etapa);
		mav.addObject("cliente", etapa.getProducao().getCliente());
		mav.addObject("producao", etapa.getProducao());
		mav.addObject("subs", subs);

		if (consultor.getPais() == Pais.CHILE) {

			List<ItemValores> itensFecha = new ArrayList<ItemValores>();

			for (SubProducao sub : subs) {
				itensFecha.add(itemValoresService.getFechaPrevistaBySubProducao(sub.getId()));
			}

			mav.addObject("fechasPrevistas", itensFecha);

		}

		mav.setViewName("/producao/listSubProducao");

		return mav;
	}
	@GetMapping("/updateSubProd/{id}")
	public ModelAndView updateSubProd(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();
		SubProducao sub = subProducaoService.findById(id);
		mav.addObject("sub", sub);
		
		mav.setViewName("/producao/updateSubProducao");
		return mav;
	}
	
	@PostMapping("/atualizarSubProd")
	public ModelAndView atualizarSubProd(@Valid SubProducao sub) {
		ModelAndView mav = new ModelAndView();
		SubProducao subs = subProducaoService.findById(sub.getId());
		sub.setEtapaTrabalho(subs.getEtapaTrabalho());
		sub.setProducao(subs.getProducao());
		subProducaoService.update(sub);
		System.out.println(sub.getId());
		mav.setViewName("redirect:/producoes/subProducao/"+sub.getId());
		return mav;
	}
	
	
	@GetMapping("/formSubProd/{id}")
	public ModelAndView formSubProd(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapa = etapasConclusaoService.findById(id);
		mav.addObject("etapa", etapa);
		mav.addObject("cliente", etapa.getProducao().getCliente());

		mav.setViewName("/producao/formSubProducao");

		return mav;
	}

	@PostMapping("/addSubProd")
	public ModelAndView addSubProd(@Valid SubProducao subProducao, @RequestParam("etapaId") Long etapaId) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(etapaId);
		SubProducao lastSubProd = new SubProducao();

		if (etapas.getProducao().getProduto().getNome().equalsIgnoreCase("EX-TARIF�RIO")) {

			for (int i = 0; i < 4; i++) {

				SubProducao subProdCreate = new SubProducao();
				List<OrdemItensTarefa> ordenacao = new ArrayList<OrdemItensTarefa>();

				switch (i) {
				case 0:

					subProdCreate.setAno(subProducao.getAno());
					subProdCreate.setDescricao(subProducao.getDescricao());
					subProdCreate.setEtapaTrabalho(etapaTrabalhoService.findById(78l));

					System.out.println("Entrou no caso 0: " + subProdCreate.getEtapaTrabalho().getNome());

					subProdCreate.setProducao(etapas.getProducao());

					subProducaoService.create(subProdCreate);
					lastSubProd = subProducaoService.getLastAddedByProducao(subProdCreate.getProducao().getId());

					etapaTrabalhoService.createItemValoresSubProducao(subProdCreate.getEtapaTrabalho(), subProdCreate);

					ordenacao = ordemService.getOrdenacaoByProduto(lastSubProd.getProducao().getProduto().getId());

					for (OrdemItensTarefa ordemItem : ordenacao) {
						List<ItemValores> itens = itemValoresService.findByItemTarefaAndProducao(
								ordemItem.getItem().getId(), ordemItem.getTarefa().getId(),
								lastSubProd.getProducao().getId());
						for (ItemValores item : itens) {
							item.setNumeroOrdem(ordemItem.getNumero());

							itemValoresService.update(item);
						}
					}

					break;
				case 1:

					subProdCreate.setAno(subProducao.getAno());
					subProdCreate.setDescricao(subProducao.getDescricao());
					subProdCreate.setEtapaTrabalho(etapaTrabalhoService.findById(79l));

					System.out.println("Entrou no caso 1: " + subProdCreate.getEtapaTrabalho().getNome());

					subProdCreate.setProducao(etapas.getProducao());

					subProducaoService.create(subProdCreate);

					etapaTrabalhoService.createItemValoresSubProducao(subProdCreate.getEtapaTrabalho(), subProdCreate);

					ordenacao = ordemService.getOrdenacaoByProduto(lastSubProd.getProducao().getProduto().getId());

					for (OrdemItensTarefa ordemItem : ordenacao) {
						List<ItemValores> itens = itemValoresService.findByItemTarefaAndProducao(
								ordemItem.getItem().getId(), ordemItem.getTarefa().getId(),
								lastSubProd.getProducao().getId());
						for (ItemValores item : itens) {
							item.setNumeroOrdem(ordemItem.getNumero());

							itemValoresService.update(item);
						}
					}

					break;
				case 2:

					subProdCreate.setAno(subProducao.getAno());
					subProdCreate.setDescricao(subProducao.getDescricao());
					subProdCreate.setEtapaTrabalho(etapaTrabalhoService.findById(107l));

					System.out.println("Entrou no caso 2: " + subProdCreate.getEtapaTrabalho().getNome());

					subProdCreate.setProducao(etapas.getProducao());

					subProducaoService.create(subProdCreate);

					etapaTrabalhoService.createItemValoresSubProducao(subProdCreate.getEtapaTrabalho(), subProdCreate);

					ordenacao = ordemService.getOrdenacaoByProduto(lastSubProd.getProducao().getProduto().getId());

					for (OrdemItensTarefa ordemItem : ordenacao) {
						List<ItemValores> itens = itemValoresService.findByItemTarefaAndProducao(
								ordemItem.getItem().getId(), ordemItem.getTarefa().getId(),
								lastSubProd.getProducao().getId());
						for (ItemValores item : itens) {
							item.setNumeroOrdem(ordemItem.getNumero());

							itemValoresService.update(item);
						}
					}

					break;
				case 3:

					subProdCreate.setAno(subProducao.getAno());
					subProdCreate.setDescricao(subProducao.getDescricao());
					subProdCreate.setEtapaTrabalho(etapaTrabalhoService.findById(108l));

					System.out.println("Entrou no caso 3: " + subProdCreate.getEtapaTrabalho().getNome());

					subProdCreate.setProducao(etapas.getProducao());

					subProducaoService.create(subProdCreate);

					etapaTrabalhoService.createItemValoresSubProducao(subProdCreate.getEtapaTrabalho(), subProdCreate);

					ordenacao = ordemService.getOrdenacaoByProduto(lastSubProd.getProducao().getProduto().getId());

					for (OrdemItensTarefa ordemItem : ordenacao) {
						List<ItemValores> itens = itemValoresService.findByItemTarefaAndProducao(
								ordemItem.getItem().getId(), ordemItem.getTarefa().getId(),
								lastSubProd.getProducao().getId());
						for (ItemValores item : itens) {
							item.setNumeroOrdem(ordemItem.getNumero());

							itemValoresService.update(item);
						}
					}

					break;
				}

			}

		} else {

			subProducao.setEtapaTrabalho(etapas.getEtapa());
			subProducao.setProducao(etapas.getProducao());

			subProducaoService.create(subProducao);
			lastSubProd = subProducaoService.getLastAddedByProducao(subProducao.getProducao().getId());

			etapaTrabalhoService.createItemValoresSubProducao(subProducao.getEtapaTrabalho(), subProducao);

			List<OrdemItensTarefa> ordenacao = ordemService
					.getOrdenacaoByProduto(lastSubProd.getProducao().getProduto().getId());

			for (OrdemItensTarefa ordemItem : ordenacao) {
				List<ItemValores> itens = itemValoresService.findByItemTarefaAndProducao(ordemItem.getItem().getId(),
						ordemItem.getTarefa().getId(), lastSubProd.getProducao().getId());
				for (ItemValores item : itens) {
					item.setNumeroOrdem(ordemItem.getNumero());

					itemValoresService.update(item);
				}
			}

		}

		modelAndView.setViewName("redirect:/producoes/subProducao/" + lastSubProd.getId());

		return modelAndView;

	}

	@GetMapping("/subProducao/{id}")
	public ModelAndView subProducaoSeguimento(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();

		SubProducao subProd = subProducaoService.findById(id);

		EtapasConclusao etapas = etapasConclusaoService.getEtapasConclusaoByProducaoAndEtapa(
				subProd.getProducao().getId(),
				subProd.getEtapaTrabalho().getId());
		Producao producao = producaoService.findById(etapas.getProducao().getId());

		List<ItemTarefa> itens = new ArrayList<>();
		itens.addAll(ItemTarefaService.getItensByProducao(producao));

		List<ValoresLista> lista = new ArrayList<>();
		for (ItemTarefa itemTarefa : itens) {
			lista.addAll(ValoresListaService.findByItemId(itemTarefa.getId()));
		}

		EtapaTrabalho etapa = etapaTrabalhoService.findById(etapas.getEtapa().getId());
		List<Tarefa> tarefas = etapa.getTarefas();

		mav.setViewName("/producao/item2o/itemSubProd");
		mav.addObject("producao", producao);
		mav.addObject("subProd", subProd);
		mav.addObject("cliente", producao.getContrato().getCliente());
		mav.addObject("itens", itens);
		mav.addObject("consultores", ConsultorService.getAll());
		mav.addObject("valoreslista", lista);
		mav.addObject("etapa", etapa);
		mav.addObject("tarefas", tarefas);
		mav.addObject("conclusao", etapas);

		return mav;
	}

	@GetMapping("/balanceteROTA/{id}")
	public ModelAndView balanceteROTA(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(id);

		mav.addObject("etapasId", etapas.getId());
		mav.addObject("cliente", etapas.getProducao().getCliente());
		mav.addObject("etapa", etapas.getEtapa());
		mav.addObject("producao", etapas.getProducao());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("balancete",
				balanceteCalculoRotaService.getBalanceteCalculoRota2030ByProducao(etapas.getProducao().getId()));

		mav.setViewName("/producao/balanceteROTA");
		return mav;
	}

	@GetMapping("/balanceteROTAUpdate/{id}")
	public ModelAndView balanceteROTAUpdate(@PathVariable("id") String idBalStr,
			@RequestParam("idEtapa") Long idEtapa) {
		ModelAndView mav = new ModelAndView();

		Long id = Long.parseLong(idBalStr);

		BalanceteCalculoRota2030 balanceteAtt = balanceteCalculoRotaService.findById(id);
		Producao prod = balanceteAtt.getProducao();

		Boolean statusContatoNF = false;
		Boolean statusContrato = true;
		Boolean versaoAnterior = false;

		mav.addObject("etapasId", idEtapa);
		mav.addObject("statusContrato", statusContrato);
		mav.addObject("statusContatoNF", statusContatoNF);
		mav.addObject("versaoAnterior", versaoAnterior);
		mav.addObject("balancete", balanceteAtt);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("producao", prod);
		mav.setViewName("/producao/balanceteROTAUpdate");
		return mav;

	}

	@Transactional
	@PostMapping("/balanceteROTA/add")
	public ModelAndView addBalanceteROTA(@Valid BalanceteCalculoRota2030 balancete,
			@RequestParam("etapasId") Long etapasId) {

		EtapasConclusao etapas = etapasConclusaoService.findById(etapasId);

		ModelAndView mav = new ModelAndView();

		System.out.println(balancete.getPorcentagemRealizado());

		if (balancete.getValidadoCliente() == null) {
			balancete.setValidadoCliente("Sem data");
		}

		balancete.setProducao(etapas.getProducao());
		BalanceteCalculoRota2030 balanceteCriado = balanceteCalculoRotaService.create(balancete);

		if (!balanceteCriado.getValidadoCliente().equalsIgnoreCase("Sem data")) {
			etapas.setConcluido(true);
			etapasConclusaoService.update(etapas);
		}

		mav.setViewName("redirect:/producoes/balanceteROTA/" + etapas.getId());

		return mav;

	}

	@Transactional
	@PostMapping("/balanceteROTA/update")
	public ModelAndView balanceteROTAupdate(@Valid BalanceteCalculoRota2030 balancete,
			@RequestParam(value = "etapasId") Long idEtapas) {

		ModelAndView modelAndView = new ModelAndView();

		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		System.out.println(balancete.getVersao());

		if (balancete.getValidadoCliente() == null) {
			balancete.setValidadoCliente("Sem data");
		}

		BalanceteCalculoRota2030 balanceteCriado = balanceteCalculoRotaService.update(balancete);

		if (!balanceteCriado.getValidadoCliente().equalsIgnoreCase("Sem data")) {
			etapas.setConcluido(true);
			etapasConclusaoService.update(etapas);
		}

		modelAndView.setViewName("redirect:/producoes/balanceteROTA/" + etapas.getId());

		return modelAndView;

	}

	@GetMapping("/geraProducoes2021")
	public ModelAndView geraProducoes() {

		ModelAndView modelAndView = new ModelAndView();

		producaoService.geraProducoes("2021");

		modelAndView.setViewName("/migracao/main");
		return modelAndView;

		// producaoService

	}

	@GetMapping("/acompanhamentoEx/{id}")
	public ModelAndView acompanhamentoExList(@PathVariable("id") Long idSub, @RequestParam("idEtapas") Long idEtapas) {

		ModelAndView mav = new ModelAndView();

		SubProducao sub = subProducaoService.findById(idSub);
		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		List<AcompanhamentoExTarifario> acompanhamentos = acompanhamentoExService
				.getAcompanhamentoExTarifarioBySubProducao(idSub);

		mav.addObject("subProducao", sub);
		mav.addObject("cliente", sub.getProducao().getCliente());
		mav.addObject("producao", sub.getProducao());
		mav.addObject("etapa", sub.getEtapaTrabalho());
		mav.addObject("etapas", etapas);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("acompanhamentos", acompanhamentos);

		mav.setViewName("/producao/acompanhamentoEx");

		return mav;

	}

	@GetMapping("/acompanhamentoExUpdate/{id}")
	public ModelAndView acompanhamentoExUpdate(@PathVariable("id") Long idAcomp,
			@RequestParam("idEtapas") Long idEtapas) {

		ModelAndView mav = new ModelAndView();

		AcompanhamentoExTarifario acomp = acompanhamentoExService.findById(idAcomp);
		SubProducao sub = acomp.getSubProducao();
		EtapasConclusao etapas = etapasConclusaoService.findById(idEtapas);

		mav.addObject("subProducao", sub);
		mav.addObject("cliente", sub.getProducao().getCliente());
		mav.addObject("producao", sub.getProducao());
		mav.addObject("etapa", sub.getEtapaTrabalho());
		mav.addObject("etapas", etapas);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("acomp", acomp);

		mav.setViewName("/producao/acompanhamentoExUpdate");

		return mav;

	}

	@PostMapping("/acompanhamentoEx/add")
	public ModelAndView addAcompanhamentoEx(@Valid AcompanhamentoExTarifario acompanhamento,
			@RequestParam("idEtapas") Long etapasId) {

		ModelAndView mav = new ModelAndView();

		EtapasConclusao et = etapasConclusaoService.findById(etapasId);
		SubProducao sub = subProducaoService.findById(acompanhamento.getSubProducao().getId());

		acompanhamentoExService.create(acompanhamento);

		if (acompanhamento.isAprovado()) {
			faturamentoService.geraFaturamentoExTarifario("AT", et.getProducao(), sub, acompanhamento);
		}

		mav.setViewName("redirect:/producoes/acompanhamentoEx/" + acompanhamento.getSubProducao().getId() + "?idEtapas="
				+ etapasId);

		return mav;
	}

	@PostMapping("/acompanhamentoEx/update")
	public ModelAndView updateAcompanhamentoEx(@Valid AcompanhamentoExTarifario acompanhamento,
			@RequestParam("idEtapas") Long etapasId) {

		ModelAndView mav = new ModelAndView();

		EtapasConclusao et = etapasConclusaoService.findById(etapasId);
		SubProducao sub = subProducaoService.findById(acompanhamento.getSubProducao().getId());

		acompanhamentoExService.update(acompanhamento);

		if (acompanhamento.isAprovado()) {
			faturamentoService.geraFaturamentoExTarifario("AT", et.getProducao(), sub, acompanhamento);
		}

		mav.setViewName(
				"redirect:/producoes/acompanhamentoExUpdate/" + acompanhamento.getId() + "?idEtapas=" + etapasId);

		return mav;
	}

	@RequestMapping(value = "/{cliente}/listaBalancete.json", method = RequestMethod.GET)
	public @ResponseBody List<Producao> getListaBalancete(@PathVariable("cliente") Long id) {

		List<Producao> listBalancete = new ArrayList<Producao>();
		listBalancete = producaoService.getListaBalancete(id);

		return listBalancete;
	}

	@RequestMapping(value = "/{cliente}/listaRelatorio.json", method = RequestMethod.GET)
	public @ResponseBody List<Producao> getListaRelatorioTecnico(@PathVariable("cliente") Long id) {

		List<Producao> listRelatorioTecnico = new ArrayList<Producao>();
		listRelatorioTecnico = producaoService.getListaRelatorioTecnico(id);

		return listRelatorioTecnico;
	}

	@RequestMapping(value = "/{idProdu��o}/etapasBalancetes.json", method = RequestMethod.GET)
	public @ResponseBody EtapasConclusao getEtapasBalancete(@PathVariable("idProdu��o") Long id) {

		EtapasConclusao etapas = etapasConclusaoService.getEtapasBalanceteProducao(id);

		return etapas;

	}

	@RequestMapping(value = "/{idProdu��o}/etapasRelatorios.json", method = RequestMethod.GET)
	public @ResponseBody EtapasConclusao getEtapasRelatorios(@PathVariable("idProdu��o") Long id) {

		EtapasConclusao etapas = etapasConclusaoService.getEtapasRelatorioProducao(id);

		System.out.println(etapas.getProducao().getAno());

		return etapas;

	}

	@PostMapping("/mapeamento")
	@ResponseBody
	public void mapeamento(@RequestBody @Valid Producao prod) {

		Producao prodAtt = producaoService.findById(prod.getId());

		prodAtt.setOppIdentificadas(prod.isOppIdentificadas());

		producaoService.update(prodAtt);

		if (prod.isOppIdentificadas() == true) {
			faturamentoService.geraFaturamentoExTarifario("Mapeamento", prodAtt, null, null);
		}

	}

	@RequestMapping(value = "/{idProducao}/listaBalanceteFaturamento.json", method = RequestMethod.GET)
	public @ResponseBody List<Producao> getListaBalanceteFaturamento(@PathVariable("idProducao") Long idProducao) {

		List<Producao> listBalanceteFaturamento = new ArrayList<Producao>();
		listBalanceteFaturamento = producaoService.getListaBalanceteFaturamento(idProducao);

		return listBalanceteFaturamento;
	}

	@GetMapping("/{cnpj}/{produto}/{ano}/producoes.json")
	public @ResponseBody List<Producao> getProducoesByClienteEProduto(@PathVariable("cnpj") String cnpj,
			@PathVariable("produto") Long produto, @PathVariable("ano") String ano) {

		Cliente cliente = ClienteService.findByCnpj(cnpj);

		List<Producao> producoes = producaoService.getProducoesByClienteProdutoCampanha(cliente.getId(), produto, ano);
		System.out.println(producoes.size() + " tamanho da lista na controller");

		return producoes;
	}

	// a lista abaixo foi substitu�da para incluir campanha na busca.
	// Estarei a mantendo caso haja uma necessidade de fazer busca somente com
	// produto e cliente
	// ou complementar com mais par�metros.
	@GetMapping("/{cnpj}/{produto}/producoes.json")
	public @ResponseBody List<Producao> getProducoesByCnpjEProduto(@PathVariable("cnpj") String cnpj,
			@PathVariable("produto") Long produto) {

		List<Producao> producoes = producaoService.getProducoesByCnpjEProduto(cnpj, produto);

		return producoes;
	}

	@GetMapping("/balancetes/{idProducao}.json")
	public @ResponseBody List<BalanceteCalculo> getBalanceteCalculos(@PathVariable("idProducao") Long idProducao) {

		List<BalanceteCalculo> balancetes = balanceteCalculoService.getBalancetesByProducao(idProducao);
		System.out.println(balancetes.size() + " tamanho da lista na controller");

		return balancetes;
	}

	@GetMapping("/{idProducao}/getBalanceteByProducao.json")
	public @ResponseBody List<BalanceteCalculo> getBalanceteByProducao(@PathVariable("idProducao") Long idProducao) {

		List<BalanceteCalculo> balancetes = balanceteCalculoService.getBalancetesByProducao(idProducao);
		System.out.println(balancetes.size() + " tamanho da lista na controller");

		return balancetes;
	}

	@RequestMapping(value = "/getBalancetesPrejuizoByProd/{id}/.json", method = RequestMethod.GET)
	public @ResponseBody List<BalanceteCalculo> getBalancetesPrejuizoByProd(@PathVariable("id") Long id) {
		// System.out.println("o id da produ��o � " + id);
		// Long idProducao = Long.parseLong(id);
		List<BalanceteCalculo> balancetes = balanceteCalculoService.getBalancetesPrejuizoByProducao(id);
		System.out.println("o tamanho da lista �" + balancetes.size());

		return balancetes;
	}

	@GetMapping("/{idProducao}/pegaEficiencia.json")
	public @ResponseBody List<Eficiencia> getEficienciaByProdAndEtapa(@PathVariable("idProducao") Long idProducao) {
		List<Eficiencia> eficiencia = eficienciaService.getEficienciaByProdAndEtapa(idProducao);
		return eficiencia;
	}

	@GetMapping("/filtroProducao")
	public ModelAndView filtroProducao(@RequestParam(value = "empresa", required = false) String empresa,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger) {

		System.out.println("empresa" + empresa);

		ModelAndView mav = new ModelAndView();

		if (queryTrigger != null) {
			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}

			if (queryTrigger.equalsIgnoreCase("true")) {

				List<Cliente> cliente = ClienteService.getFiltroProducao(empresa);
				mav.addObject("clientes", cliente);

			}
		}

		mav.addObject("empresa", empresa);
		mav.setViewName("/producao/list");
		return mav;

	}

	@PostMapping("/subProducao/delete/{id}")
	public ModelAndView deleteSubProducao(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		SubProducao subProducao = subProducaoService.findById(id);
		subProducaoService.deleteSubProducao(subProducao.getId());

		modelAndView.setViewName("redirect:/producoes/cliente/" + subProducao.getProducao().getCliente().getId());

		return modelAndView;

	}
	
	/* teste */
	@GetMapping("/{idEtapa}/tarefas.json")
	public @ResponseBody List<Tarefa> listaTarefaByEtapa (@PathVariable("idEtapa") Long idEtapa){
		
		EtapaTrabalho etapa = etapaTrabalhoService.findById(idEtapa);
		List<Tarefa> listaTarefa = etapa.getTarefas();
		
		return listaTarefa;
	}
	

	@GetMapping("/{idEtapa}/itensTarefas.json")
	public @ResponseBody List<ItemTarefa> listaItensTarefas (@PathVariable("idEtapa") Long idEtapa){
		System.out.println("solicita��o tarefas por id idEtapa itensTarefas "+idEtapa);
		
		EtapaTrabalho etapa = etapaTrabalhoService.findById(idEtapa);
		List<Tarefa> listaTarefa = etapa.getTarefas();
		List<ItemTarefa> itens = new ArrayList<ItemTarefa>();
		
		for (Tarefa tarefa : listaTarefa) {
			itens.addAll(ItemTarefaService.getItemByTarefa(tarefa.getId()));
		}
		
		
		for (ItemTarefa itemTarefa2 : itens) {
			System.out.println("item tarefa  "+ itemTarefa2.getNome());
		}
		
		return itens;
	}
	
	@GetMapping("/{idItemValor}/pegaTarefaByItemValores.json")
	public @ResponseBody Long  pegaTarefaByItemValores (@PathVariable("idItemValor") Long idItemValor){
		System.out.println("pegou o ultimoItemTarefa  "+idItemValor);
		
		ItemValores item = itemValoresService.findById(idItemValor);
		System.out.println(" item  "+item.getTarefa().getId());
		Long tarefaId = item.getTarefa().getId();
		
		
		return tarefaId;
	}
	
	@GetMapping("/{idProd}/{idItemValorAtual}/{etapa}/verificaInclusaoHoras.json")
	public @ResponseBody Boolean verificaInclusaoHoras (@PathVariable("idProd") Long idProd,@PathVariable("idItemValorAtual") Long idItemValorAtual,@PathVariable("etapa") Long etapa){
		System.out.println("idProd  "+idProd);
		System.out.println("idItemValorAtual  "+idItemValorAtual);
		System.out.println("etapa  "+etapa);
		
		Boolean verificaInclusaoHoras = false;
		List<Eficiencia> eficiencia = eficienciaService.getEficienciaByProducao(idProd);
		ItemValores item = itemValoresService.findById(idItemValorAtual);
		Long itemTarefaId = item.getItem().getId();
		Long tarefaId = item.getTarefa().getId();
		System.out.println("itemTarefaId  "+itemTarefaId);
		
		for (Eficiencia eficiencia2 : eficiencia) {
			try {
				if(eficiencia2.getItemTarefa().getId() == itemTarefaId && eficiencia2.getEtapa().getId() == etapa && eficiencia2.getTarefa().getId() == tarefaId ) {
					System.out.println("eficiencia2  "+eficiencia2.getItemTarefa().getId());
					System.out.println("eficiencia2  "+eficiencia2.getTarefa().getId());
					 verificaInclusaoHoras = true;
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		System.out.println("verificaInclusaoHoras"+verificaInclusaoHoras);
		return verificaInclusaoHoras;
	}
	
	@GetMapping("/{idItemValor}/pegaItemTarefaByItemValores.json")
	public @ResponseBody Long  pegaItemTarefaByItemValores (@PathVariable("idItemValor") Long idItemValor){
		
		ItemValores item = itemValoresService.findById(idItemValor);
		System.out.println(" item  "+item.getTarefa().getId());
		Long ItemTarefa = item.getItem().getId();
		
		
		return ItemTarefa;
	}
	
}
