package br.com.finiciativas.fseweb.controllers;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.ScoreCL;
import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.TamanhoEmpresa;
import br.com.finiciativas.fseweb.enums.TipoEndereco;
import br.com.finiciativas.fseweb.enums.TipoTelefone;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EnderecoClienteServiceImpl;

@Controller
@RequestMapping(path = "/clientesCL")
public class ClienteControllerCL {

	@Autowired
	private ClienteServiceImpl clienteService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private EnderecoClienteServiceImpl enderecoClienteService;

	@Autowired
	private ContatoClienteServiceImpl ContatoClienteService;

	@GetMapping("/form")
	public ModelAndView form() {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("cargos", Cargo.values());
		modelAndView.addObject("tipoTelefone", TipoTelefone.values());
		modelAndView.addObject("setorAtividade", SetorAtividade.values());
		modelAndView.addObject("categoria", TamanhoEmpresa.values());
		modelAndView.addObject("consultoresComerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		modelAndView.addObject("tipo", TipoEndereco.values());
		modelAndView.addObject("score",ScoreCL.values());

		modelAndView.setViewName("/cliente/chile/form");

		return modelAndView;
	}

	@GetMapping("/all")
	public ModelAndView getAll() {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("clientes", clienteService.getAll());
		modelAndView.setViewName("/cliente/chile/list");

		return modelAndView;
	}

	@Transactional
	@PostMapping("/add")
	public ModelAndView add(@Valid Cliente empresa,
			@RequestParam(value = "cep") String cep,
			@RequestParam(value = "logradouro") String logradouro,
			@RequestParam(value = "numero") String numero,
			@RequestParam(value = "complemento") String complemento,
			@RequestParam(value = "tipoEnder") String tipo,
			@RequestParam(value = "bairro") String bairro,
			@RequestParam(value = "cidade") String cidade,
			@RequestParam(value = "estado") String estado,
			@RequestParam(value = "enderecoNotaFiscalBoleto", required = false) Boolean enderecoNotaFiscalBoleto,
			@RequestParam(value = "nome") String nome,
			@RequestParam(value = "cargo") String cargo,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "telefone1") String telefone1,
			@RequestParam(value = "telefone2") String telefone2,
			@RequestParam(value = "nfBoleto", required = false) Boolean nfBoleto,
			@RequestParam(value = "novosContatos", required = false) Boolean novosContatos,
			@RequestParam(value = "fidelizado", required = false) Boolean fidelizado) {
		
		System.out.println("Chegou");
		System.out.println(empresa.getRut());
		
		ModelAndView modelAndView = new ModelAndView();

		
		if(empresa.getRut()!=null){
		empresa.setRut(empresa.getRut().replaceAll("[^a-zA-Z0-9]+",""));
		}
		
		if(novosContatos==null){novosContatos=false;}
		if(nfBoleto==null){nfBoleto=false;}
		if(fidelizado==null){fidelizado=false;}
		
		Cliente cliente = clienteService.create(empresa);
		
		ContatoCliente contato = new ContatoCliente();
		
		contato.setAtivo(true);
		contato.setNome(nome);
		contato.setCargo(cargo);
		contato.setEmail(email);
		contato.setTelefone1(telefone1);
		contato.setTelefone2(telefone2);
		contato.setPessoaNfBoleto(nfBoleto);
		contato.setReferencia(novosContatos);
		contato.setPesquisaSatisfacao(fidelizado);
		
		ContatoClienteService.create(contato, cliente.getId());
		
		EnderecoCliente endereco = new EnderecoCliente();
		
		if(enderecoNotaFiscalBoleto == null) {
			enderecoNotaFiscalBoleto = false;
		} else {
			enderecoNotaFiscalBoleto = true;
		}
		
		if(nfBoleto == null) {
			nfBoleto = false;
		} else {
			nfBoleto = true;
		}
		
		if(novosContatos == null) {
			novosContatos = false;
		} else {
			novosContatos = true;
		}
		
		if(fidelizado == null) {
			fidelizado = false;
		} else {
			fidelizado = true;
		}
		
		endereco.setCep(cep);
		endereco.setLogradouro(logradouro);
		endereco.setNumero(numero);
		endereco.setComplemento(complemento);
		endereco.setTipo(tipo);
		endereco.setBairro(bairro);
		endereco.setCidade(cidade);
		endereco.setEstado(estado);
		endereco.setCliente(cliente);
		endereco.setEnderecoNotaFiscalBoleto(enderecoNotaFiscalBoleto);
		
		enderecoClienteService.create(endereco);

		modelAndView.setViewName("redirect:/clientesCL/all");

		return modelAndView;

	}

	@Transactional
	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		clienteService.delete(id);

		modelAndView.setViewName("redirect:/clientesCL/all");

		return modelAndView;

	}

	@Transactional
	@PostMapping("/update")
	public ModelAndView update(@Valid Cliente empresa) {

		ModelAndView modelAndView = new ModelAndView();
		
		empresa.setRut(empresa.getRut().replaceAll("[^a-zA-Z0-9]+",""));
		
		Cliente empresaAtt = clienteService.updateCL(empresa);
		
		modelAndView.setViewName("redirect:/clientesCL/" + empresaAtt.getId());
		
		modelAndView.addObject("tipo", TipoEndereco.values());

		return modelAndView;
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Cliente cliente = clienteService.findById(id);

		modelAndView.addObject("cliente", cliente);
		modelAndView.addObject("enderecos", enderecoClienteService.getAllById(id));
		modelAndView.addObject("contatos", ContatoClienteService.getAllById(id));
		modelAndView.addObject("cargos", Cargo.values());
		modelAndView.addObject("tipoTelefone", TipoTelefone.values());
		modelAndView.addObject("setorAtividade", SetorAtividade.values());
		modelAndView.addObject("consultoresComerciais",
				consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		modelAndView.addObject("categoria", TamanhoEmpresa.values());
		modelAndView.addObject("score", ScoreCL.values());

		modelAndView.setViewName("/cliente/chile/update");

		return modelAndView;
	}

	@GetMapping("/details/{id}")
	public ModelAndView details(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Cliente cliente = clienteService.loadFullEmpresa(id);
		
		List<EnderecoCliente> enderecos = enderecoClienteService.getAllById(id);
		System.out.println(enderecos.size());

		modelAndView.addObject("cliente", cliente);
		modelAndView.addObject("enderecos", enderecoClienteService.getAllById(id));
		modelAndView.addObject("contatos", ContatoClienteService.getAllById(id));

		modelAndView.setViewName("/cliente/chile/details");

		return modelAndView;
	}

	@GetMapping("/{rut}.json")
	public @ResponseBody Cliente clienteJson(@PathVariable("rut") String rut) {
		
		System.out.println(rut);

		Cliente cliente = this.clienteService.findByRUT(rut);

		//cliente.setComercialResponsavel(new ConsultorPresenter(cliente.getComercialResponsavel()).converter());

		return cliente;
	}

	@PostMapping("/enderecoAdd")
	@ResponseBody
	public void enderAdd(@RequestBody EnderecoCliente endereco) {
		enderecoClienteService.create(endereco);
	}

	@GetMapping("/all.json")
	public @ResponseBody List<Cliente> empresasJson() {

		return clienteService.getAll();

	}

	@GetMapping("/enderecoform/{id}")
	public ModelAndView enderForm(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("cliente", clienteService.findById(id));
		modelAndView.addObject("tipo", TipoEndereco.values());

		modelAndView.setViewName("/cliente/endereco/form");

		return modelAndView;
	}

	@Transactional
	@PostMapping("/enderecoFormAdd")
	public ModelAndView enderecoAdd(@Valid EnderecoCliente ender) {

		ModelAndView modelAndView = new ModelAndView();

		enderecoClienteService.create(ender);

		modelAndView.setViewName("redirect:/clientes/"+ender.getCliente().getId());

		return modelAndView;

	}

	@Transactional
	@RequestMapping("/deleteEndereco/{id}")
	public ModelAndView deleteEnder(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		EnderecoCliente ender = enderecoClienteService.findById(id);
		enderecoClienteService.delete(ender);
		modelAndView.setViewName("redirect:/clientes/" + ender.getCliente().getId());

		return modelAndView;

	}
	
	@Transactional
	@RequestMapping(value = "/updateEndereco/{id}")
	public ModelAndView updateEnder(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("tipo", TipoEndereco.values());
		modelAndView.addObject("ender", enderecoClienteService.findById(id));
		modelAndView.setViewName("/cliente/endereco/update");

		return modelAndView;
	}
	
	@Transactional
	@PostMapping("/updateAtualizar")
	public ModelAndView atualizarEndereco(@Valid EnderecoCliente enderecoCliente, @RequestParam("clienteId") Long id) {
		ModelAndView mav = new ModelAndView();
		
		System.out.println(id);
		
		System.out.println(enderecoCliente.isEnderecoNotaFiscalBoleto());

		Cliente cliente = clienteService.findById(id);
		enderecoCliente.setCliente(cliente);
		enderecoClienteService.update(enderecoCliente);
		mav.setViewName("redirect:/clientes/" + enderecoCliente.getCliente().getId());
		return mav;
	}
	
	@Transactional
	@RequestMapping("/deleteContato/{id}")
	public ModelAndView deleteContato(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		ContatoCliente contatoEmpresa = ContatoClienteService.findById(id);
		ContatoClienteService.remove(contatoEmpresa);
		
		
		modelAndView.setViewName("redirect:/clientes/" + contatoEmpresa.getCliente().getId());

		return modelAndView;

	}
	
}
	
