package br.com.finiciativas.fseweb.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.TarefaServiceImpl;

@Controller
@RequestMapping("/etapasTrabalho")
public class EtapaTrabalhoController {

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Autowired
	private TarefaServiceImpl tarefaService;

	@Autowired
	private ProdutoServiceImpl produtoService;
	
	@Autowired
	private EtapasConclusaoServiceImpl conclusaoServiceImpl;

	@GetMapping("/form")
	public ModelAndView form() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("tarefasList", tarefaService.getAll());
		mav.setViewName("/etapatrabalho/form");
		return mav;
	}

	@PostMapping("/update")
	public ModelAndView update(EtapaTrabalho etapaTrabalho) {
		ModelAndView mav = new ModelAndView();

		this.etapaTrabalhoService.update(etapaTrabalho);

		mav.setViewName("redirect:/etapasTrabalho/" + etapaTrabalho.getId());

		return mav;
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		EtapaTrabalho etapaTrabalho = this.etapaTrabalhoService.findById(id);
		List<Tarefa> tarefas = this.tarefaService.getAll();

		mav.addObject("etapaTrabalho", etapaTrabalho);
		mav.addObject("tarefasRestante", tarefas.stream().filter(tarefa -> !etapaTrabalho.getTarefas().contains(tarefa))
				.collect(Collectors.toList()));

		mav.setViewName("/etapatrabalho/update");

		return mav;
	}

	@PostMapping("/add")
	public String create(EtapaTrabalho etapaTrabalho) {

		etapaTrabalhoService.create(etapaTrabalho);
		EtapaTrabalho etapa = etapaTrabalhoService.getLastAdded();
		etapaTrabalhoService.bindEtapaToTarefas(etapa);
		conclusaoServiceImpl.addEtapaInProductions();

		return "/etapatrabalho/form";
	}

	@GetMapping("/produtos/{idProduto}")
	public ModelAndView etapasTrabalhoProduto(@PathVariable("idProduto") Long idProduto) {
		ModelAndView mav = new ModelAndView();

		List<EtapaTrabalho> etapas = this.produtoService.getEtapasTrabalho(idProduto);

		mav.addObject("produto", this.produtoService.findById(idProduto));

		mav.addObject("etapasTrabalhoProduto", etapas);

		mav.setViewName("/etapatrabalho/list");

		return mav;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor tarefasCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					Tarefa tarefa = new Tarefa();
					tarefa.setId(Long.parseLong(element.toString()));

					return tarefa;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};

		binder.registerCustomEditor(List.class, "tarefas", tarefasCollector);

	}

}
