package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.presenters.ConsultorPresenter;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FilialServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Controller
@RequestMapping("/equipes")
public class EquipeController {

	@Autowired
	private EquipeServiceImpl equipeService;

	@Autowired
	private ConsultorServiceImpl consultorService;
	
	@Autowired
	private ValorNegocioServiceImpl valorNegocioService;
	
	@Autowired
	private FilialServiceImpl filialService;
	
	@GetMapping("/form")
	public ModelAndView form() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("lideres", consultorService.getLideres());
		mav.addObject("coordenadores", consultorService.getCoordenadores());
		mav.addObject("gerentes", consultorService.getGerentes());
		mav.addObject("consultores", consultorService.getConsultoresSemEquipe());

		mav.setViewName("equipe/form");

		return mav;
	}

	@PostMapping("/add")
	public String create(Equipe equipe) {

		this.equipeService.create(equipe);

		return "redirect:/equipes/all";
	}

	@GetMapping("/all")
	public ModelAndView getAll() {
		ModelAndView mav = new ModelAndView();
	
		mav.addObject("equipes", this.equipeService.getAll());
		mav.setViewName("/equipe/list");
		return mav;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor consultoresCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					Consultor consultor = new Consultor();
					consultor.setId(Long.parseLong(element.toString()));

					return consultor;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};

		binder.registerCustomEditor(List.class, "consultores", consultoresCollector);

	}

	@GetMapping("/{idEquipe}/consultores.json")
	public @ResponseBody List<Consultor> getConsultoresByFilial(@PathVariable("idEquipe") Long idEquipe) {
		
		Equipe equipe = this.equipeService.findById(idEquipe);

		List<Consultor> consultores = this.equipeService.getConsultores(idEquipe);

		ArrayList<Consultor> consultoresPresenter = new ArrayList<>();

		if (consultores != null && !consultores.isEmpty()) { 
			consultoresPresenter
					.addAll(consultores.stream().map(c -> new ConsultorPresenter(c).converter()).collect(Collectors.toList()));
		}
		
		consultoresPresenter.add(0, equipe.getLider());

		return consultoresPresenter;
	}

	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		equipeService.remove(id);

		modelAndView.setViewName("redirect:/equipes/all");

		return modelAndView;

	}

	@PostMapping("/update")
	public ModelAndView update(@Valid Equipe equipe) {
		
		ModelAndView modelAndView = new ModelAndView("redirect:/equipes/" + equipe.getId());
		
			equipeService.update(equipe);

		return modelAndView;
	}

	@GetMapping("/{id}")
	public ModelAndView updateForm(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView("/equipe/update");
		
		List<Consultor> consultoresSemEquipe = consultorService.getConsultoresSemEquipe();
		Equipe equipe = equipeService.findById(id);
		List<Filial> filiais = filialService.getAll();
		

		modelAndView.addObject("lideres", consultorService.getLideres());
		modelAndView.addObject("coordenadores", consultorService.getCoordenadores());
		modelAndView.addObject("gerentes", consultorService.getGerentes());
		modelAndView.addObject("equipe", equipe);
		modelAndView.addObject("filiais", filialService.getAll());
		modelAndView.addObject("consultoresNoEquipe", consultoresSemEquipe);
		return modelAndView;

	}
	
	@GetMapping("/{idEquipe}/getFilial.json")
	public @ResponseBody Filial getFilial(@PathVariable("idEquipe") Long id) {
		Equipe equipe = equipeService.findById(id);
		
		Filial filial = equipe.getFilial();
		
		return filial;
	}
	
	@GetMapping("/{idFilial}/getEquipesByFilial.json")
	public @ResponseBody List<Equipe> getEquipesByFilial(@PathVariable("idFilial") Long id) {
		System.out.println("o id enviado foi " + id);
		List<Equipe> equipes = equipeService.getAllEquipesByFilial(id);
		
		return equipes;
	}

}
