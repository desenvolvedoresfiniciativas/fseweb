package br.com.finiciativas.fseweb.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.TipoTelefone;
import br.com.finiciativas.fseweb.models.instituicao.ContatoInstituicao;
import br.com.finiciativas.fseweb.services.impl.ContatoInstituicaoServiceImpl;

@Controller
@RequestMapping(path = "/instituicoes/contatos")
public class ContatoInstituicaoController {

	@Autowired
	private ContatoInstituicaoServiceImpl contatoInstituicaoService;

	@GetMapping("/{id}.json")
	public @ResponseBody List<ContatoInstituicao> getContatos(@PathVariable("id") Long id) {
		return contatoInstituicaoService.getContatosInstituicao(id);
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();

		ContatoInstituicao contato = contatoInstituicaoService.findById(id);

		modelAndView.addObject("tipoTelefone", TipoTelefone.values());
		modelAndView.addObject("contatoInstituicao", contato);
		modelAndView.addObject("idInstituicao", contatoInstituicaoService.getIdInstituicaoOfContact(id));

		modelAndView.setViewName("instituicao/contatoinstituicao/update");

		return modelAndView;
	}

	@GetMapping("details/{id}.json")
	public @ResponseBody ContatoInstituicao findByIdJson(@PathVariable("id") Long id) {
		return contatoInstituicaoService.findById(id);
	}

	@Transactional
	@PostMapping("/add/{id}")
	public void add(@RequestBody ContatoInstituicao contatoInstituicao, @PathVariable("id") Long id,
			HttpServletResponse response) {
		contatoInstituicaoService.create(contatoInstituicao, id);
		response.setStatus(200);
	}

	@Transactional
	@PostMapping("/update")
	public String update(@Valid ContatoInstituicao contatoInstituicao) {
		contatoInstituicaoService.update(contatoInstituicao);
		return "redirect:/instituicoes/" + contatoInstituicaoService.getIdInstituicaoOfContact(contatoInstituicao.getId());
	}
	
	@Transactional
	@PostMapping("{idInstituicao}/delete/{id}")
	public String delete(@PathVariable Long idInstituicao, @PathVariable("id") Long id) {
		ContatoInstituicao contatoIntituicao = new ContatoInstituicao();
		contatoIntituicao = contatoInstituicaoService.findById(id);
		contatoInstituicaoService.remove(contatoIntituicao);
		return "redirect:/instituicoes/" + idInstituicao;
	}

}
