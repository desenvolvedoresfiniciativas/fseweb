package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.FITechReport;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FITechReportServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.VersaoServiceImpl;

@Controller
public class HomeController {

	@Autowired
	private ClienteServiceImpl ClienteService;

	@Autowired
	private ProducaoServiceImpl ProducaoService;

	@Autowired
	private ContatoClienteServiceImpl contatoCliente;

	@Autowired
	private EtapasConclusaoServiceImpl etapasService;
	
	@Autowired
	private VersaoServiceImpl versaoService;
	
	@Autowired
	private EquipeServiceImpl equipeService;
	
	@Autowired
	private FITechReportServiceImpl FITechReportService;
	
	@RequestMapping("/")
	public ModelAndView index(HttpServletRequest request) {

		ModelAndView mav = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		Consultor consultor = ((Consultor) auth.getPrincipal());
		
		System.out.println(consultor.getCargo());
		
		if(consultor.getCargo()==Cargo.CONSULTOR_TECNICO || consultor.getCargo()==Cargo.CONSULTOR_TRAINEE  || consultor.getCargo()==Cargo.CONSULTOR_LIDER_TECNICO){
			List<Producao> producoes = ProducaoService.getProducaoByConsultor(consultor.getId());
			mav.addObject("producoes", producoes);
			
			List<Produto> produtos = new ArrayList<Produto>();
			
			for (Producao producao : producoes) {
				if(!produtos.contains(producao.getProduto())){
					produtos.add(producao.getProduto());
				}
			}

			mav.addObject("produtos", produtos);
			
		}
		
		if(consultor.getCargo()==Cargo.LIDER_TECNICO){
			List<Producao> producoes = ProducaoService.getProducaoByEquipe(consultor.getId());
			mav.addObject("producoes", producoes);
		
			List<Produto> produtos = new ArrayList<Produto>();

			for (Producao producao : producoes) {
				if(!produtos.contains(producao.getProduto())){
					produtos.add(producao.getProduto());
				}
			}

			mav.addObject("produtos", produtos);
		
		}
		
		if(consultor.getCargo()==Cargo.COORDENADOR_TECNICO){
			List<Producao> producoes = ProducaoService.getProducaoByCoordenador(consultor.getId());
			mav.addObject("producoes", producoes);

			List<Produto> produtos = new ArrayList<Produto>();

			for (Producao producao : producoes) {
				if(!produtos.contains(producao.getProduto())){
					produtos.add(producao.getProduto());
				}
			}

			mav.addObject("produtos", produtos);
		
		}
		
		if(consultor.getCargo()==Cargo.GERENTE_CONSULTORIA_TECNICA){
			List<Producao> producoes = ProducaoService.getProducaoByGerente(consultor.getId());
			mav.addObject("producoes", producoes);
		
			List<Produto> produtos = new ArrayList<Produto>();

			for (Producao producao : producoes) {
				if(!produtos.contains(producao.getProduto())){
					produtos.add(producao.getProduto());
				}
			}
			mav.addObject("produtos", produtos);
		}
		
		if(consultor.getCargo()==Cargo.ESTAGIARIO_TECNICO){
			
			List<Producao> producoes;
			
			try {
				Long equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId())).getId();
				producoes = ProducaoService.getProducaoDaEquipe(equipe);
			} catch (Exception e) {
				producoes = ProducaoService.getProducaoByConsultor(consultor.getId());
				
			}

			
			mav.addObject("producoes", producoes);

			List<Produto> produtos = new ArrayList<Produto>();

			for (Producao producao : producoes) {
				if(!produtos.contains(producao.getProduto())){
					produtos.add(producao.getProduto());
				}
			}
			mav.addObject("produtos", produtos);
		}

		if(consultor.getCargo()==Cargo.GERENTE_GERAL){
			List<Producao> producoes = ProducaoService.getAll();
			mav.addObject("producoes", producoes);

			List<Produto> produtos = new ArrayList<Produto>();

			for (Producao producao : producoes) {
				if(!produtos.contains(producao.getProduto())){
					produtos.add(producao.getProduto());
				}
			}
			mav.addObject("produtos", produtos);
		}
		
//		if(consultor.getCargo()==Cargo.DIRETORIA){
//			List<Producao> producoes = ProducaoService.getProducaoByDiretor();
//			mav.addObject("producoes", producoes);
//		
//			List<Produto> produtos = new ArrayList<Produto>();
//
//			for (Producao producao : producoes) {
//				if(!produtos.contains(producao.getProduto())){
//					produtos.add(producao.getProduto());
//				}
//			}	
//				
//			mav.addObject("produtos", produtos);
//		
//		}
		
		mav.addObject("versao", versaoService.getVersaoAtual());
		mav.addObject("primeiroLogin", consultor.isPrimeiroLogin());
		mav.addObject("consultor", consultor);
		mav.addObject("acessoConsultor", consultor.getId());
		mav.setViewName("/index");
		return mav;
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accessDenied(Consultor user) {

		ModelAndView model = new ModelAndView("exceptions/403");

		if (user != null) {
			model.addObject("msg", "Ol� " + user.getNome() + ", voc� n�o tem permiss�o para isto.");
		} else {
			model.addObject("msg", "Voc� n�o tem permiss�o para isto.");
		}

		return model;
	}

	@GetMapping("/{idProducao}/contatos.json")
	public @ResponseBody List<ContatoCliente> getConsultoresByFilial(@PathVariable("idProducao") Long id) {
		
		Producao producao = ProducaoService.findById(id);

		List<ContatoCliente> contatos = contatoCliente.getAllById(producao.getCliente().getId());

		return contatos;
	}

	@GetMapping("/{idProd}/prodAno.json")
	public @ResponseBody List<EtapasConclusao> getProducaoByAno(@PathVariable("idProd") Long id) {

		Producao producao = ProducaoService.findById(id);
		List<EtapasConclusao> etapas = etapasService.getAllEtapasConclusaoByProducao(producao.getId());

		return etapas;

	}

	@GetMapping("/{idAno}/ano.json")
	public @ResponseBody List<EtapasConclusao> getAllProducaoByAno(@PathVariable("idAno") String ano) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Consultor consultor = ((Consultor) auth.getPrincipal());
		
		List<EtapasConclusao> etapas = new ArrayList<EtapasConclusao>();

		if(consultor.getCargo()==Cargo.CONSULTOR_TRAINEE){
			etapas = etapasService.getAllEtapasOfConsultorByAno(ano, consultor.getId());
		}
		
		if(consultor.getCargo()==Cargo.CONSULTOR_TECNICO || consultor.getCargo()==Cargo.CONSULTOR_LIDER_TECNICO){
			etapas = etapasService.getAllEtapasOfConsultorByAno(ano, consultor.getId());
		}
		
		if(consultor.getCargo()==Cargo.LIDER_TECNICO){
			etapas = etapasService.getAllEtapasOfEquipeByAno(ano, consultor.getId());
		}
		
		if(consultor.getCargo()==Cargo.ESTAGIARIO_TECNICO){
			Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));
			etapas = etapasService.getAllEtapasOfEquipeByAno(ano, equipe.getLider().getId());
		}
		
		if(consultor.getCargo()==Cargo.GERENTE_GERAL){
			etapas = etapasService.getAllEtapasByAno(ano);
		}
		
		if(consultor.getCargo()==Cargo.DIRETORIA){
			etapas = etapasService.getAllEtapasOfDiretorByAno(ano);
		}

		return etapas;
	}
	
	@GetMapping("/{ano}/producoes.json")
	public @ResponseBody List<Producao> getAllProducaoByAnoAndConsultor(@PathVariable("ano") String ano) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Consultor consultor = ((Consultor) auth.getPrincipal());
		
		List<Producao> producoes = new ArrayList<Producao>();
		
		if(consultor.getCargo()==Cargo.CONSULTOR_TECNICO || consultor.getCargo()==Cargo.CONSULTOR_LIDER_TECNICO){
			producoes = ProducaoService.getAllProducaoByConsultorAndAno(ano, consultor.getId());
		}
		
		if(consultor.getCargo()==Cargo.CONSULTOR_TRAINEE) {
			producoes = ProducaoService.getAllProducaoByConsultorAndAno(ano, consultor.getId());
		}
		
		if(consultor.getCargo()==Cargo.ESTAGIARIO_TECNICO){
			Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));
			producoes = ProducaoService.getProducaoDaEquipe(equipe.getId());
		}
		
		if(consultor.getCargo()==Cargo.LIDER_TECNICO){
			producoes = ProducaoService.getAllProducaoByEquipeAndAno(ano, consultor.getId());
		}
		
		if(consultor.getCargo()==Cargo.GERENTE_GERAL){
			producoes = ProducaoService.getAllByAno(ano);
		}
		
		if(consultor.getCargo()==Cargo.DIRETORIA){
			producoes = ProducaoService.getAllProducaoByDiretorAndAno(ano);
		}
		
		return producoes;
	}
	
	@PostMapping("/report")
	@ResponseBody
	public void gerador(@RequestBody FITechReport msg) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Consultor consultor = ((Consultor) auth.getPrincipal());

		FITechReport mensagem = new FITechReport();
		mensagem.setTipoMensagem(msg.getTipoMensagem());
		mensagem.setConsultor(consultor);
		mensagem.setMensagem(msg.getMensagem());
		mensagem.setAtendido(false);
		
		FITechReportService.create(mensagem);	
	}	
}
