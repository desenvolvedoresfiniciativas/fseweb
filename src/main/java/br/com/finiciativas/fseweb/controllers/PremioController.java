package br.com.finiciativas.fseweb.controllers;

import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Premio;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PremioServiceImpl;

@Controller
@RequestMapping(path = "/premios")
public class PremioController {
		
		@Autowired
		private PremioServiceImpl premioService;
		
		@Autowired
		private ConsultorServiceImpl consultorService;
		
		
		@GetMapping(path = "/form")
		public ModelAndView form() {
			ModelAndView mav = new ModelAndView("/premio/form");
			mav.addObject("consultores", consultorService.getAllComercialAndConsultor());
					
			mav.setViewName("/premio/form");
			
			return mav;
		}
		
		
		@PostMapping("/lerExcel")
		public @ResponseBody void lerExcel() {
			System.out.println("entrou no m�todo");
			premioService.readExcel();
			
			
		}
		
		@PostMapping("/gerarPremiacoes/{campanha}")
		public @ResponseBody void gerarPremiacoesByAno(@PathVariable("campanha") String campanha) {
			System.out.println(campanha);
			
			switch(campanha){
				case "2021":
					List<JSONObject> jsonObjects = premioService.readExcel();
					System.out.println("o tamanho do objeto recebido �: " + jsonObjects.size());
					
					premioService.calculaPremio2021(jsonObjects);
					break;
				default:
					System.out.println("n�o existe um calculo para essa premia��o");;	
			}
			
		}

		@Transactional
		@PostMapping(path = "/add")
		public ModelAndView add(
				@RequestParam(value = "consultor", 				required=true)	Long idConsultor,
				@RequestParam(value = "VNAtingido", 			required=true) 	String StringVNAtingido,
				@RequestParam(value = "VNPretendido", 			required=true) 	String StringVNPretendido,
				@RequestParam(value = "propostas",  			required=false) Long propostas,
				@RequestParam(value = "metaPropostas", 			required=false) Long metaPropostas,
				@RequestParam(value = "contratos",  			required=false) Long contratos,
				@RequestParam(value = "metaContratos", 			required=false) Long metaContratos,
				@RequestParam(value = "ticketMedio",  			required=false) String StringTicketMedio,
				@RequestParam(value = "metaTicketMedio", 		required=false) String StringMetaTicketMedio,
				@RequestParam(value = "fluxoCaixa",  			required=false) String StringFluxoCaixa,
				@RequestParam(value = "metaFluxoCaixa", 		required=false) String StringMetaFluxoCaixa,
				@RequestParam(value = "premioKPIEficiencia", 	required=true) 	String premioKPIEficiencia,
				@RequestParam(value = "premioKPIServicos", 		required=true) 	String premioKPIServicos,
				@RequestParam(value = "premioKPITicketMedio", 	required=true) 	String premioKPITicketMedio,
				@RequestParam(value = "premioKPIFluxoCaixa", 	required=true) 	String premioKPIFluxoCaixa,
				@RequestParam(value = "totalPremio", 			required=true) 	String totalPremio,
				@RequestParam(value = "dataEntradaCliente", 	required=true) 	String dataEntradaCliente,	
				@RequestParam(value = "dataEntradaFuncionario", required=true) 	String dataEntradaFuncionario,
				@RequestParam(value = "campanha", 				required=true) 	String campanha,
				@RequestParam(value = "obsPremio", 			required=false) String observacoes) {	
			
			Consultor consultor = consultorService.findById(idConsultor);
			
			System.out.println(consultor.getNome());
			System.out.println("VN Atingido: " + StringVNAtingido);
			System.out.println("VN Pretendido: " + StringVNPretendido);
			System.out.println("Propostas Atingidas: " + propostas);
			System.out.println("Propostas pretendidas: " + metaPropostas);
			System.out.println("Contratos atingidos: " + contratos);
			System.out.println("Contratos pretendidos: " + metaContratos);
			System.out.println("Ticket Medio atingido: " + StringTicketMedio);
			System.out.println("Ticket Medio pretendido: " + StringMetaTicketMedio);
			System.out.println("Fluxo de caixa atingido: " + StringFluxoCaixa);
			System.out.println("Fluxo de caixa pretendido: " + StringMetaFluxoCaixa);
			System.out.println("Premio KPI Eficiencia: " + premioKPIEficiencia);
			System.out.println("Premio KPI Servicos: " + premioKPIServicos);
			System.out.println("Premio KPI Ticket Medio: " + premioKPITicketMedio);
			System.out.println("Premio KPI Fluxo de caixa: " + premioKPIFluxoCaixa);
			
			System.out.println("Total premio: " + totalPremio);
			System.out.println("Data de entrada do cliente: " + dataEntradaCliente);
			System.out.println("Data de entrada do funcionario: " + dataEntradaFuncionario);
			System.out.println("Campanha: " + campanha);
			System.out.println("Observacoes: " + observacoes);
			
			ModelAndView mav = new ModelAndView();
			
			Premio premio = new Premio();

			if(StringVNAtingido != null) {
				StringVNAtingido = StringVNAtingido.replaceAll("\\.", "");
				StringVNAtingido = StringVNAtingido.replaceAll("\\,", ".");
				StringVNAtingido = StringVNAtingido.replaceAll("[^0-9\\.]","");
				System.out.println("VN atingido float: " + StringVNAtingido);
			}
			if(StringVNPretendido != null) {
				StringVNPretendido = StringVNPretendido.replaceAll("\\.", "");
				StringVNPretendido = StringVNPretendido.replaceAll("\\,", ".");
				StringVNPretendido = StringVNPretendido.replaceAll("[^0-9\\.]","");
				System.out.println("VN pretendido float: " + StringVNPretendido);
			}
			if(StringTicketMedio != null) {
				StringTicketMedio = StringTicketMedio.replaceAll("\\.", "");
				StringTicketMedio = StringTicketMedio.replaceAll("\\,", ".");
				StringTicketMedio = StringTicketMedio.replaceAll("[^0-9\\.]","");
				System.out.println("Ticket medio atingido float: " + StringTicketMedio);
			}
			if(StringMetaTicketMedio != null) {
				StringMetaTicketMedio = StringMetaTicketMedio.replaceAll("\\.", "");
				StringMetaTicketMedio = StringMetaTicketMedio.replaceAll("\\,", ".");
				StringMetaTicketMedio = StringMetaTicketMedio.replaceAll("[^0-9\\.]","");
				System.out.println("Ticket medio pretendido float: " + StringMetaTicketMedio);
			}
			if(StringFluxoCaixa != null) {
				StringFluxoCaixa = StringFluxoCaixa.replaceAll("\\.", "");
				StringFluxoCaixa = StringFluxoCaixa.replaceAll("\\,", ".");
				StringFluxoCaixa = StringFluxoCaixa.replaceAll("[^0-9\\.]","");
				System.out.println("Fluxo de caixa atingido float: " + StringFluxoCaixa);
			}
			if(StringMetaFluxoCaixa != null) {
				StringMetaFluxoCaixa = StringMetaFluxoCaixa.replaceAll("\\.", "");
				StringMetaFluxoCaixa = StringMetaFluxoCaixa.replaceAll("\\,", ".");
				StringMetaFluxoCaixa = StringMetaFluxoCaixa.replaceAll("[^0-9\\.]","");
				System.out.println("Fluxo de caixa pretendido float: " + StringMetaFluxoCaixa);
			}
			
//			float VNAtingido = Float.parseFloat(StringVNAtingido);
//			float VNPretendido = Float.parseFloat(StringVNPretendido);
//			float ticketMedio = Float.parseFloat(StringTicketMedio);
//			float metaTicketMedio = Float.parseFloat(StringMetaTicketMedio);
//			float fluxoCaixa = Float.parseFloat(StringFluxoCaixa);
//			float metaFluxoCaixa = Float.parseFloat(StringMetaFluxoCaixa);
			
			premio.setConsultor(consultor);
			premio.setValorNegocioRealizado(Float.parseFloat(StringVNAtingido));
			premio.setValorNegocioPretendido(Float.parseFloat(StringVNPretendido));
			premio.setEficienciaPropostasRealizado(propostas);
			premio.setEficienciaPropostasPretendido(metaPropostas);
			premio.setEficienciaContratosRealizado(contratos);
			premio.setEficienciaContratosPretendido(metaContratos);
			premio.setTicketMedioPretendido(Float.parseFloat(StringTicketMedio));
			premio.setTicketMedioRealizado(Float.parseFloat(StringMetaTicketMedio));
			premio.setFaturamentoRealizado(Float.parseFloat(StringFluxoCaixa));
			premio.setFaturamentoPretendido(Float.parseFloat(StringMetaFluxoCaixa));
			premio.setPremioKPIEficiencia(Float.parseFloat(premioKPIEficiencia));
			premio.setPremioKPIServicos(Float.parseFloat(premioKPIServicos));
			premio.setPremioKPIFluxoDeCaixa(Float.parseFloat(premioKPIFluxoCaixa));
			premio.setPremioKPITicketMedio(Float.parseFloat(premioKPITicketMedio));
			//premio.setValorPremio(totalPremio);
			premio.setDataDeRecebimentoCliente(dataEntradaCliente);
			premio.setDataRecebimentoFuncionario(dataEntradaFuncionario);
			premio.setObservacoes(observacoes);
			
			//premio = premioService.create(premio);
			
			mav.setViewName("redirect:/comissoes/all");
			
			return mav;
		}
		
		@GetMapping(path = "/info/{id}")
		public ModelAndView findByIdPremio(@PathVariable("id") Long id) {
			ModelAndView mav = new ModelAndView();
			
			Premio premio = premioService.findById(id);
			
			System.out.println("Id premio: " + premio.getId());
			System.out.println("Consultor: " + premio.getConsultor().getNome());
			System.out.println("Valor Premio: " + premio.getValorPremio());
			
			mav.addObject("premio", premio);
			
			mav.setViewName("premio/info");
			
			return mav;
			
		}
		
		@GetMapping(path = "/all")
		public ModelAndView getAll() {
			
			ModelAndView mav = new ModelAndView();
			//List<Premio> premios = premioService.getAll();
			mav.setViewName("/premio/list");
			//mav.addObject("premios", premios);
			return mav;
		}
		
//		@GetMapping(path = "/listAll")
		public ModelAndView getAllContracts() {

			ModelAndView mav = new ModelAndView();
			
			return mav;
		}
		
		@Transactional
		@PostMapping("/delete/{id}")
		public ModelAndView delete(@PathVariable("id") Long id) {

			ModelAndView modelAndView = new ModelAndView();

			Premio premio = premioService.findById(id);
			premioService.delete(premio);
			modelAndView.setViewName("redirect:/comissoes/all");

			return modelAndView;
		}
		
		@Transactional
		@PostMapping(path = "/update")
		public ModelAndView add(@Valid Premio premio) {

			ModelAndView mav = new ModelAndView();
			return mav;
		}
		
		@PostMapping(value = "/gerarPremiacoesCampanha2020")
		@ResponseBody
		public void gerarPremiacoes2020() {
		
			premioService.gerarPremioByCampanha2020();
		
		}
}