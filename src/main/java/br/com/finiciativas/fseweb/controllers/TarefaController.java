package br.com.finiciativas.fseweb.controllers;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.produto.ordenacao.OrdemItensTarefa;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.OrdemItensTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.TarefaServiceImpl;

@Controller
@RequestMapping("/tarefas")
public class TarefaController {
	
	@Autowired
	private TarefaServiceImpl tarefaService;
	
	@Autowired
	private ItemTarefaServiceImpl itemTarefaService;
	
	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;
	
	@Autowired
	private ProdutoServiceImpl produtoService;
	
	@Autowired
	private OrdemItensTarefaServiceImpl ordemService;
	
	@GetMapping("/form")
	public ModelAndView form(){
		ModelAndView mav= new ModelAndView();
		mav.addObject("itensTarefa", itemTarefaService.getAll());
		mav.setViewName("/tarefa/form2");
		return mav;
	}
	
	@PostMapping("/add")
	public ModelAndView add(Tarefa tarefa){
		ModelAndView mav = new ModelAndView();
		tarefaService.create(tarefa);
		mav.setViewName("redirect:/tarefas/form");
		return mav;
	}
	
	@GetMapping("/{id}/{idProduto}")
	public ModelAndView findById(@PathVariable("id") Long id, @PathVariable("idProduto") Long idProduto, @RequestParam(value = "idEtapa") Long etapaId){
		ModelAndView mav = new ModelAndView();
		
		Tarefa tarefa = this.tarefaService.findById(id);
		List<ItemTarefa> iTarefa = this.itemTarefaService.getAll();
		
		mav.addObject("tarefa", tarefa);
		mav.addObject("etapaId", etapaId);
		mav.addObject("produto", produtoService.findById(idProduto));
		mav.addObject("itensTarefasRestantes", iTarefa.stream().filter(itaref -> !tarefa.getItensTarefa().contains(itaref)).collect(Collectors.toList()));
		
		mav.setViewName("/tarefa/update");
		
		return mav;
	}
	
	@PostMapping("/update")
	public ModelAndView update(Tarefa tarefa, @RequestParam(value = "produto") Long produtoId, @RequestParam(value = "idEtapa") Long etapaId){
		ModelAndView mav = new ModelAndView();
		
		List<ItemTarefa> itens = tarefa.getItensTarefa();
		for (ItemTarefa itemTarefa : itens) {
		
		OrdemItensTarefa ordem = ordemService.checkOrdenacaoExists(itemTarefa.getId(), tarefa.getId(), produtoId);
		
		if(!Objects.nonNull(ordem)){
			System.out.println("Odene��o desse item para essa tarefa ainda n�o existe...");
			OrdemItensTarefa novaOrdem = new OrdemItensTarefa();
			novaOrdem.setItem(itemTarefaService.findById(itemTarefa.getId()));
			novaOrdem.setTarefa(tarefaService.findById(tarefa.getId()));
			novaOrdem.setProduto(produtoService.findById(produtoId));
			
			List<OrdemItensTarefa> lastOrdem = ordemService.getOrdenacaoByTarefa(tarefa.getId());
			
			int lastNumero = 0;
			for (OrdemItensTarefa order : lastOrdem) {
				if(order.getNumero()>lastNumero){
					lastNumero = order.getNumero();
				}
			}
			
			novaOrdem.setNumero(lastNumero+1);
			ordemService.create(novaOrdem);
		}	
		}
		
		tarefaService.updateTarefaItens(tarefa, produtoId, etapaId);
		
		mav.setViewName("redirect:/tarefas/" + tarefa.getId() + "/" +produtoId+"?idEtapa="+etapaId);
		
		return mav;
	}
	
	@GetMapping("/etapasTrabalho/{idEtapaTrabalho}")
	public ModelAndView getTarefasCotainsInEtapaTrabalho(@PathVariable("idEtapaTrabalho") Long idEtapaTrabalho){
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("etapaTrabalho", this.etapaTrabalhoService.getTarefas(idEtapaTrabalho));
		
		mav.setViewName("/tarefa/list");
		
		return mav;
	}
	
	@GetMapping("/{idTarefa}/itens")
	public ModelAndView getItens(@PathVariable("idTarefa") Long id){
		ModelAndView mav = new ModelAndView();
		
		Tarefa tarefa = this.tarefaService.findById(id);
		
		mav.addObject("tarefa", tarefa);
		
		mav.setViewName("/itemtarefa/list");
		
		return mav;		
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor itensTarefaCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					ItemTarefa itemTarefa = new ItemTarefa();
					itemTarefa.setId(Long.parseLong(element.toString()));

					return itemTarefa;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};

		binder.registerCustomEditor(List.class, "itensTarefa", itensTarefaCollector);

	}
	
	
}
