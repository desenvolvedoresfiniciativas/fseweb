package br.com.finiciativas.fseweb.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.TipoInstituicao;
import br.com.finiciativas.fseweb.models.instituicao.Instituicao;
import br.com.finiciativas.fseweb.services.impl.InstituicaoServiceImpl;

@Controller
@RequestMapping("/instituicoes")
public class InstituicaoController {

	@Autowired
	private InstituicaoServiceImpl instituicaoService;

	@GetMapping("/all")
	public ModelAndView getAll() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("instituicoes", this.instituicaoService.getAll());

		mav.setViewName("/instituicao/list");

		return mav;
	}

	@GetMapping("/form")
	public ModelAndView form() {
		ModelAndView mav = new ModelAndView("/instituicao/form");

		mav.addObject("tiposInstituicao", TipoInstituicao.values());

		return mav;
	}

	@PostMapping("/add")
	public ModelAndView create(Instituicao instituicao) {
		ModelAndView mav = new ModelAndView("redirect:/instituicoes/all");

		instituicaoService.create(instituicao);

		return mav;
	}

	@Transactional
	@PostMapping("/update")
	public ModelAndView update(@Valid Instituicao instituicao) {

		ModelAndView modelAndView = new ModelAndView();

		Instituicao instituicaoAtt = instituicaoService.update(instituicao);

		modelAndView.setViewName("redirect:/instituicoes/" + instituicaoAtt.getId());

		return modelAndView;
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView("/instituicao/update");

		modelAndView.addObject("instituicao", instituicaoService.findById(id));
		modelAndView.addObject("tipo", TipoInstituicao.values());

		return modelAndView;
	}

	@GetMapping("/details/{id}")
	public ModelAndView details(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("instituicao", instituicaoService.loadFullEmpresa(id));

		modelAndView.setViewName("/instituicao/details");

		return modelAndView;
	}

	@Transactional
	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		instituicaoService.delete(id);

		modelAndView.setViewName("redirect:/instituicoes/all");

		return modelAndView;

	}

}
