package br.com.finiciativas.fseweb.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.FormaPagamento;
import br.com.finiciativas.fseweb.factory.HonorarioFactory;
import br.com.finiciativas.fseweb.factory.VigenciaFactory;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.contrato.GarantiaContrato;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.vigencia.Vigencia;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaPagamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.GarantiaContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PercentualEscalonadoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.VigenciaServiceImpl;

@Controller
@RequestMapping(path = "/contratos")
public class ContratoController {

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private GarantiaContratoServiceImpl garantiaContratoService;

	@Autowired
	private HonorarioServiceImpl HonorarioService;

	@Autowired
	private ClienteServiceImpl ClienteService;

	@Autowired
	private EtapaPagamentoServiceImpl EtapaPagamentoService;

	@Autowired
	private VigenciaServiceImpl vigenciaService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private PercentualEscalonadoServiceImpl percentualEscalonadoService;

	@GetMapping("/form")
	public ModelAndView form() {
		ModelAndView mav = new ModelAndView("/contrato/form");

		mav.addObject("produtos", this.produtoService.getAll());
		mav.addObject("comerciaisAtivos", this.consultorService.getComerciais(Cargo.CONSULTOR_COMERCIAL, Cargo.ESPECIALISTA_COMERCIAL, Cargo.SUPERVISOR_ANALISTA_COMERCIAL, Cargo.ANALISTA_COMERCIAL, Cargo.COORDENADOR_COMERCIAL,
				Cargo.COORDENADOR_TECNICO, Cargo.LIDER_TECNICO, Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("comerciaisTodos", this.consultorService.getComerciais(Cargo.CONSULTOR_COMERCIAL, Cargo.ESPECIALISTA_COMERCIAL, Cargo.SUPERVISOR_ANALISTA_COMERCIAL, Cargo.ANALISTA_COMERCIAL, Cargo.COORDENADOR_COMERCIAL,
				Cargo.COORDENADOR_TECNICO, Cargo.LIDER_TECNICO, Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("formasPagamento", FormaPagamento.values());
		mav.addObject("garantias", this.garantiaContratoService.getAll());

		return mav;
	}

	@PostMapping("/add")
	public ModelAndView create(@Valid Contrato contrato, @RequestParam("vigenciaradio") String vigenciaradio,
			@RequestParam(value = "valorfixocheck", required = false) String valorfixocheck,
			@RequestParam(value = "percentualfixocheck", required = false) String percentualfixocheck,
			@RequestParam(value = "percentualescalonadocheck", required = false) String percentualescalonadocheck,
			@RequestParam("selectNomeCombo") String selectNomeCombo, @RequestParam("nomeCombo") String nomeCombo,@RequestParam(value = "selectPeriodoCobranca", required = false) String selectPeriodoCobranca){
		System.out.println("teste contrato manual "+ contrato.isManual());

		ModelAndView mav = new ModelAndView();
		contrato.setVigencia(new VigenciaFactory().retornaVigencia(vigenciaradio));
		contrato.setHonorarios(new HonorarioFactory().retornarHonorario(valorfixocheck, percentualfixocheck,
				percentualescalonadocheck));

		for (Honorario h : contrato.getHonorarios()) {
			h.setContrato(contrato);
		}

		EtapasPagamento etapas = new EtapasPagamento();
		contrato.setEtapasPagamento(etapas);

		if ((selectNomeCombo != "") && (nomeCombo.equals("") == false)) {
			contrato.setNomeCombo(nomeCombo);
			System.out.println("os dois tem conteudo, set nomeCombo:" + nomeCombo);
		} else if ((selectNomeCombo == "") && (nomeCombo.equals("") == false)) {
			contrato.setNomeCombo(nomeCombo);
			System.out.println("So o nomeCombo tem conteudo, set nomeCombo:" + nomeCombo);
		} else if ((selectNomeCombo != "") && (nomeCombo.equals("") == true)) {
			contrato.setNomeCombo(selectNomeCombo);
			System.out.println("So o selectNomeCombo tem conteudo, set selectNomeCombo:" + selectNomeCombo);
		}
		

		System.out.println("selectPeriodoCobranca" + selectPeriodoCobranca);
		contrato.setPeriodoCobranca(selectPeriodoCobranca);

		Contrato contratoNew = contratoService.findById(this.contratoService.create(contrato));
		contratoNew.setHonorarios(contrato.getHonorarios());
		contratoService.updateHonorarios(contratoNew);

		mav.setViewName("redirect:/contratos/" + contratoNew.getId());

		if (contrato.isPagamentoEntrega()) {
			EtapasPagamento et = EtapaPagamentoService.getLastAdded();
			et.setContrato(contrato);
			EtapaPagamentoService.update(et);
		}

		return mav;
	}

	@PostMapping("/atualizaEtapas")
	@ResponseBody
	public EtapasPagamento atualizaEtapas(@RequestBody EtapasPagamento etapas) {

		EtapasPagamento etapa = EtapaPagamentoService.findByContratoId(etapas.getContrato().getId());
		etapa.setContrato(etapas.getContrato());
		Contrato contrato = contratoService.findById(etapas.getContrato().getId());

		if (contrato.getProduto().getId() != 23 && contrato.getProduto().getId() != 32) {
			try {
				if (!etapas.getHonorario1().isEmpty() || etapas.getHonorario1() != "") {
					etapa.setHonorario1(etapas.getHonorario1());
				} else {
					etapa.setHonorario1(null);
				}
				if (!etapas.getHonorario2().isEmpty() || etapas.getHonorario2() != "") {
					etapa.setHonorario2(etapas.getHonorario2());
				} else {
					etapa.setHonorario2(null);
				}
				if (!etapas.getHonorario3().isEmpty() || etapas.getHonorario3() != "") {
					etapa.setHonorario3(etapas.getHonorario3());
				} else {
					etapa.setHonorario3(null);
				}
				if (!etapas.getHonorario4().isEmpty() || etapas.getHonorario4() != "") {
					etapa.setHonorario4(etapas.getHonorario4());
				} else {
					etapa.setHonorario4(null);
				}
			} catch (Exception e) {
				System.out.println("Sem honor�rio");
			}
		}

		etapa.setValor1(etapas.getValor1());
		etapa.setValor2(etapas.getValor2());
		etapa.setValor3(etapas.getValor3());
		etapa.setValor4(etapas.getValor4());

		if (etapas.getEtapa1().getId() != null && etapas.getEtapa1().getId() != 0l) {
			etapa.setEtapa1(etapas.getEtapa1());
		} else {
			etapa.setEtapa1(null);
		}
		if (etapas.getEtapa2().getId() != null && etapas.getEtapa2().getId() != 0l) {
			etapa.setEtapa2(etapas.getEtapa2());
		} else {
			etapa.setEtapa2(null);
		}
		if (etapas.getEtapa3().getId() != null && etapas.getEtapa3().getId() != 0l) {
			etapa.setEtapa3(etapas.getEtapa3());
		} else {
			etapa.setEtapa3(null);
		}
		if (etapas.getEtapa4().getId() != null && etapas.getEtapa4().getId() != 0l) {
			etapa.setEtapa4(etapas.getEtapa4());
		} else {
			etapa.setEtapa4(null);
		}

		if (etapas.getPorcentagem1() == 0.0f) {
			etapa.setPorcentagem1(0.0f);
		} else {
			etapa.setPorcentagem1(etapas.getPorcentagem1());
		}
		if (etapas.getPorcentagem2() == 0.0f) {
			etapa.setPorcentagem2(0.0f);
		} else {
			etapa.setPorcentagem2(etapas.getPorcentagem2());
		}
		if (etapas.getPorcentagem3() == 0.0f) {
			etapa.setPorcentagem3(0.0f);
		} else {
			etapa.setPorcentagem3(etapas.getPorcentagem3());
		}
		if (etapas.getPorcentagem4() == 0.0f) {
			etapa.setPorcentagem4(0.0f);
		} else {
			etapa.setPorcentagem4(etapas.getPorcentagem4());
		}

		etapa.setParcela1(etapas.getParcela1());
		etapa.setParcela2(etapas.getParcela2());
		etapa.setParcela3(etapas.getParcela3());
		etapa.setParcela3(etapas.getParcela4());

		if (etapas.getDataFaturamento() != null) {
			etapa.setDataFaturamento(etapas.getDataFaturamento());
		}

		EtapaPagamentoService.update(etapa);
		return etapas;
	}

	@GetMapping("/cliente/{id}")
	public ModelAndView findClienteId(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/contrato/cliente");

		List<Contrato> contratos = this.contratoService.getContratoByCliente(id);
		mav.addObject("contratos", contratos);

		return mav;
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/contrato/update");

		List<GarantiaContrato> garantias = garantiaContratoService.getAll();
		Contrato contrato = contratoService.findById(id);
		List<EtapaTrabalho> etapasTrabalho = contrato.getProduto().getEtapasTrabalho();

		List<Honorario> honorarios = contrato.getHonorarios();
		List<Contrato> contratosPrincipais = contratoService.getProdutosPrincipais(contrato.getCliente().getId());

		for (Honorario honorario : honorarios) {
			System.out.println("Honor�rios do Contrato acessado: ");
			System.out.println("Nome: " + honorario.getNome());
		}

		boolean possuiFat = false;
		EtapasPagamento ep = contrato.getEtapasPagamento();
		boolean semTiming = false;

		if (ep != null) {
			if (ep.getEtapa1() == null && ep.getEtapa2() == null && ep.getEtapa3() == null && ep.getEtapa4() == null) {
				semTiming = true;
			}
		}

		List<Faturamento> fats = faturamentoService.getFaturamentoByContrato(contrato.getId());
		if (fats != null) {
			if (fats.size() > 0) {
				possuiFat = true;
			}
		}
		mav.addObject("produtosPrincipais", contratosPrincipais);
		mav.addObject("semTiming", semTiming);
		mav.addObject("contrato", contrato);
		mav.addObject("possuiFat", possuiFat);
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("comerciaisAtivos", this.consultorService.getComerciais(Cargo.CONSULTOR_COMERCIAL, Cargo.ESPECIALISTA_COMERCIAL, Cargo.SUPERVISOR_ANALISTA_COMERCIAL, Cargo.ANALISTA_COMERCIAL, Cargo.COORDENADOR_COMERCIAL,
				Cargo.COORDENADOR_TECNICO, Cargo.LIDER_TECNICO, Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("comerciaisTodos", this.consultorService.getAllComerciais(Cargo.CONSULTOR_COMERCIAL, Cargo.ESPECIALISTA_COMERCIAL, Cargo.SUPERVISOR_ANALISTA_COMERCIAL, Cargo.ANALISTA_COMERCIAL, Cargo.COORDENADOR_COMERCIAL,
				Cargo.COORDENADOR_TECNICO, Cargo.LIDER_TECNICO, Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("formasPagamento", FormaPagamento.values());
		mav.addObject("etapasTrabalho", etapasTrabalho);
		mav.addObject("etapasPagamento", contrato.getEtapasPagamento());
		mav.addObject("garantias", garantias);
		mav.addObject("honorarios", honorarios);
		mav.addObject("periodo", contrato.getPeriodoCobranca());

		mav.addObject("garantiasNo", garantias.stream().filter(garan -> !contrato.getGarantias().contains(garan))
				.collect(Collectors.toList()));

		return mav;
	}

	@GetMapping("/chile/{id}")
	public ModelAndView findByIdChile(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/contrato/chile/update");

		List<GarantiaContrato> garantias = garantiaContratoService.getAll();
		Contrato contrato = contratoService.findById(id);
		List<EtapaTrabalho> etapasTrabalho = contrato.getProduto().getEtapasTrabalho();

		List<Honorario> honorarios = contrato.getHonorarios();

		for (Honorario honorario : honorarios) {
			System.out.println("Honor�rios do Contrato acessado: ");
			System.out.println("Nome: " + honorario.getNome());
			System.out.println("Contrato: " + honorario.getContrato());
		}

		mav.addObject("contrato", contrato);
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("comerciaisAtivos", this.consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("comerciaisTodos", this.consultorService.getConsultorByCargoAll(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("formasPagamento", FormaPagamento.values());
		mav.addObject("etapasTrabalho", etapasTrabalho);
		mav.addObject("etapasPagamento", contrato.getEtapasPagamento());
		mav.addObject("garantias", garantias);
		mav.addObject("honorarios", honorarios);

		mav.addObject("garantiasNo", garantias.stream().filter(garan -> !contrato.getGarantias().contains(garan))
				.collect(Collectors.toList()));

		return mav;
	}

	@GetMapping("/all")
	public ModelAndView getAll() {

		ModelAndView modelAndView = new ModelAndView();

		List<Cliente> clientes = ClienteService.getAll();

		modelAndView.setViewName("/contrato/list");
		modelAndView.addObject("clientes", clientes);
		return modelAndView;
	}

	@GetMapping("/allCL")
	public ModelAndView getAllCL() {

		ModelAndView modelAndView = new ModelAndView();

		List<Cliente> clientes = ClienteService.getAll();

		modelAndView.setViewName("/contrato/chile/list");
		modelAndView.addObject("clientes", clientes);
		return modelAndView;
	}

	@GetMapping("/listAll")
	public ModelAndView getAllContracts() {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/contrato/listAll");
		modelAndView.addObject("contratos", contratoService.getAll());
		return modelAndView;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor garantiasContratoCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					GarantiaContrato garantiaContrato = new GarantiaContrato();
					garantiaContrato.setId(Long.parseLong(element.toString()));

					return garantiaContrato;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};

		binder.registerCustomEditor(List.class, "garantias", garantiasContratoCollector);

	}

	@Transactional
	@PostMapping("/update")
	public void add(@RequestBody @Valid Contrato contrato, @RequestBody PercentualFixo pf,
			@RequestBody PercentualEscalonado pe, @RequestBody ValorFixo vf, HttpServletResponse response) {

		List<Honorario> honorarios = new ArrayList<>();

		if (pf != null) {
			honorarios.add(pf);
		}

		if (pe != null) {
			honorarios.add(pe);
		}

		if (vf != null) {
			honorarios.add(vf);
		}

		contrato.setHonorarios(honorarios);

//		if (this.contratoService.update(contrato) != null) {
//			response.setStatus(200);
//		}
//		response.setStatus(400);
	}

	@GetMapping("/all.json")
	public @ResponseBody List<Contrato> contratoJson() {

		return contratoService.getAll();

	}

	@GetMapping("/{id}.json")
	public @ResponseBody List<Contrato> contratosByIDJson(@PathVariable("id") Long id) {

		return contratoService.getContratoByCliente(id);

	}

	@GetMapping("/unique/{id}.json")
	public @ResponseBody Contrato contratoByIdJson(@PathVariable("id") Long id) {

		return contratoService.findById(id);

	}

	@GetMapping("/honorario.json")
	public @ResponseBody List<Honorario> honorarioJson() {

		return HonorarioService.getAll();

	}

	@PostMapping("/atualizar")
	@ResponseBody
	public void atualizarContrato(@RequestBody Contrato contrato) {


		Contrato contratoAtt = contratoService.findById(contrato.getId());

		
		contratoAtt.setComercialResponsavelEntrada(
				consultorService.findById(contrato.getComercialResponsavelEntrada().getId()));
		contratoAtt.setComercialResponsavelEfetivo(
				consultorService.findById(contrato.getComercialResponsavelEfetivo().getId()));
		contratoAtt.setDescricaoContratual(contrato.getDescricaoContratual());
		contratoAtt.setObsContrato(contrato.getObsContrato());
		contratoAtt.setContratoFisico(contrato.isContratoFisico());
		contratoAtt.setCnpjFaturar(contrato.getCnpjFaturar());
		contratoAtt.setRazaoSocialFaturar(contrato.getRazaoSocialFaturar());
		contratoAtt.setEstimativaComercial(contrato.getEstimativaComercial());
		contratoAtt.setDataEntrada(contrato.getDataEntrada());
		contratoAtt.setPedidoCompra(contrato.isPedidoCompra());
		contratoAtt.setFormaPagamento(contrato.getFormaPagamento());
		contratoAtt.setPrazoPagamento(contrato.getPrazoPagamento());
		contratoAtt.setReembolsoDespesas(contrato.isReembolsoDespesas());
		contratoAtt.setReajuste(contrato.isReajuste());
		contratoAtt.setDataReajuste(contrato.getDataReajuste());
		contratoAtt.setObsContrato(contrato.getObsContrato());
		contratoAtt.setDataReajuste(contrato.getDataReajuste());
		contratoAtt.setGarantias(contrato.getGarantias());
		contratoAtt.setManual(contrato.isManual());
		contratoAtt.setContratoInativo(contrato.isContratoInativo());
		
		List<GarantiaContrato> garantias = contrato.getGarantias();

		contratoService.update(contratoAtt);

	}

	@PostMapping("/atualizarHonorarios")
	@ResponseBody
	public void atualizarHonorario(@RequestBody Contrato contrato) {

		Contrato contratoAtt = contratoService.findById(contrato.getId());

		// Honor�rios atuais
		boolean isCurrentPercentFixo = false;
		boolean isCurrentPercentEsca = false;
		boolean isCurrentValorFixo = false;

		// Possiveis novos honor�rios
		boolean isNextPercentFixo = false;
		boolean isNextPercentEsca = false;
		boolean isNextValorFixo = false;

		// Triggers de cria��o
		String percentualescalonadocheck = null;
		String percentualfixocheck = null;
		String valorfixocheck = null;

		for (Honorario honorario : contratoAtt.getHonorarios()) {
			if (honorario.getNome().equals("Percentual Escalonado")) {
				isCurrentPercentEsca = true;
			}
			if (honorario.getNome().equals("Percentual Fixo")) {
				isCurrentPercentFixo = true;
			}
			if (honorario.getNome().equals("Valor Fixo")) {
				isCurrentValorFixo = true;
			}
		}

		for (Honorario honorario : contrato.getHonorarios()) {
			System.out.println("!!!!!!!!!!!! " + honorario.getNome());
			if (honorario.getNome().equals("Percentual Escalonado")) {
				isNextPercentEsca = true;
				percentualescalonadocheck = "true";
			}
			if (honorario.getNome().equals("Percentual Fixo")) {
				isNextPercentFixo = true;
				percentualfixocheck = "true";
			}
			if (honorario.getNome().equals("Valor Fixo")) {
				isNextValorFixo = true;
				valorfixocheck = "true";
			}
		}

		System.out.println("PF Atual x Futuro: " + isCurrentPercentFixo + " x " + isNextPercentFixo);
		System.out.println("PE Atual x Futuro: " + isCurrentPercentEsca + " x " + isNextPercentEsca);
		System.out.println("VF Atual x Futuro: " + isCurrentValorFixo + " x " + isNextValorFixo);

		if (isCurrentPercentEsca == true && isNextPercentEsca == false) {
			System.out.println("Apagou PE");
			Honorario PE = HonorarioService.getHonorarioByNomeAndContrato(contratoAtt.getId(), "Percentual Escalonado");
			PE.setContrato(null);
			HonorarioService.delete(PE);
		} else if (isCurrentPercentEsca == true && isNextPercentEsca == true) {
			percentualescalonadocheck = null;
		}

		if (isCurrentPercentFixo == true && isNextPercentFixo == false) {
			System.out.println("Apagou PF");
			Honorario PF = HonorarioService.getHonorarioByNomeAndContrato(contratoAtt.getId(), "Percentual Fixo");
			PF.setContrato(null);
			HonorarioService.delete(PF);
		} else if (isCurrentPercentFixo == true && isCurrentPercentFixo == true) {
			percentualfixocheck = null;
		}

		if (isCurrentValorFixo == true && isNextValorFixo == false) {
			System.out.println("Apagou VF");
			Honorario VF = HonorarioService.getHonorarioByNomeAndContrato(contratoAtt.getId(), "Valor Fixo");
			VF.setContrato(null);
			HonorarioService.delete(VF);
		} else if (isCurrentValorFixo == true && isCurrentValorFixo == true) {
			valorfixocheck = null;
		}

		System.out.println("Vai criar PF? " + percentualfixocheck);
		System.out.println("Vai criar PE? " + percentualescalonadocheck);
		System.out.println("Vai criar VF? " + valorfixocheck);

		contratoAtt.setHonorarios(new HonorarioFactory().retornarHonorario(valorfixocheck, percentualfixocheck,
				percentualescalonadocheck));

		for (Honorario h : contratoAtt.getHonorarios()) {
			h.setContrato(contratoAtt);
		}

		contratoService.updateHonorarios(contratoAtt);

	}

	@PostMapping("/atualizarVigencia")
	@ResponseBody
	public void atualizarVigencia(@RequestBody Contrato contrato) {

		System.out.println(contrato.getVigencia().getNome());
		Contrato contratoAtt = contratoService.findById(contrato.getId());

		if (!contratoAtt.getVigencia().getNome().equalsIgnoreCase(contrato.getVigencia().getNome())) {

			String inicioVigencia = contratoAtt.getVigencia().getInicioVigencia();

			vigenciaService.delete(contratoAtt.getVigencia());
			Vigencia vigencia = new VigenciaFactory().retornaVigenciaConstruida(contrato.getVigencia().getNome(),
					inicioVigencia, null, null);
			vigenciaService.update(vigencia);

			vigenciaService.delete(contratoAtt.getVigencia());

			contratoAtt.setVigencia(vigencia);

			contratoService.update(contratoAtt);

		}

	}

	@PostMapping("/atualizarLBPreju")
	@ResponseBody
	public void atualizarLBPreju(@RequestBody Contrato contrato) {

		System.out.println("Entrou no metodo: ContratoController.atualizarLBPreju()");

		Contrato contratoAtt = contratoService.findById(contrato.getId());

		contratoAtt.setPrejuizoFixo(contrato.isPrejuizoFixo());
		contratoAtt.setPrejuizoPercentual(contrato.isPrejuizoPercentual());
		contratoAtt.setPrejuizoValorFixo(contrato.getPrejuizoValorFixo());
		contratoAtt.setPrejuizoPercentualFixo(contrato.getPrejuizoPercentualFixo());

		contratoService.update(contratoAtt);

	}

	@PostMapping("/updatePE/{id}")
	@ResponseBody
	public void atualizarLBPreju(@RequestBody PercentualEscalonado pe, @PathVariable(name = "id") Long id) {

		System.out.println(pe.getNome());

		EtapasPagamento timing = EtapaPagamentoService.findById(id);
		PercentualEscalonado percent = pe;

		PercentualEscalonado newPE = percentualEscalonadoService.update(pe);

		switch (newPE.getNome()) {
		case "Escalonamento - Etapa 1":
			timing.setEscalonado1(newPE);
			EtapaPagamentoService.update(timing);
			break;
		case "Escalonamento - Etapa 2":
			timing.setEscalonado2(newPE);
			EtapaPagamentoService.update(timing);
			break;
		case "Escalonamento - Etapa 3":
			timing.setEscalonado3(newPE);
			EtapaPagamentoService.update(timing);
			break;
		case "Escalonamento - Etapa 4":
			timing.setEscalonado4(newPE);
			EtapaPagamentoService.update(timing);
			break;

		default:
			break;
		}

	}

	@GetMapping("/{id}/timings.json")
	public @ResponseBody EtapasPagamento timings(@PathVariable(name = "id") Long id) {

		return EtapaPagamentoService.findById(id);

	}

	// @GetMapping("/{id}/.json")
	@RequestMapping(value = "/{id}/{combo}/{produtoPrincipal}/.json", method = RequestMethod.PUT)

	public @ResponseBody Contrato updateIsComboById(@PathVariable("id") Long id, @PathVariable("combo") boolean combo,
			@PathVariable("produtoPrincipal") boolean produtoPrincipal) {
		System.out.println("entrou no metodo");
		System.out.println("ok" + combo);
		System.out.println("ok" + produtoPrincipal);
		Contrato contrato = contratoService.findById(id);
		contrato.setCombo(combo);
		contrato.setProdutoAlvo(produtoPrincipal);
		contratoService.update(contrato);

		return contratoService.findById(id);
	}

	@PostMapping(value = "/{idContratoFilho}/salvaContratosPai")
	@ResponseBody

	public void updateProdutosAlvoPai(@RequestBody List<Contrato> contratosPai,
			@PathVariable("idContratoFilho") Long idContratoFilho) {

		for (Contrato contrato : contratosPai) {
			System.out.println(contrato.getId());
		}

		Contrato contratoFilho = contratoService.findById(idContratoFilho);
		contratoFilho.setProdutosAlvo(contratosPai);

		contratoService.update(contratoFilho);

	}

	@RequestMapping(value = "/{idContrato}/PegarListaContrato.json", method = RequestMethod.GET)

	public @ResponseBody List<Contrato> getListaProdutosAlvo(@PathVariable("idContrato") Long idContrato) {
		System.out.println("entrou no metodo getListaProdutosAlvo");
		System.out.println("ok idContrato:" + idContrato);

		List<Contrato> contratoChecked = new ArrayList<Contrato>();

		System.out.println();
		for (Contrato contratos : contratoChecked) {
			System.out.println(contratos.getId());
		}
		Contrato idContratoAlvoChecked = contratoService.findById(idContrato);

		for (Contrato contrato : idContratoAlvoChecked.getProdutosAlvo()) {
			contratoChecked.add(contratoService.findById(contrato.getId()));
		}

		return contratoChecked;
	}

	@RequestMapping(value = "/{idCliente}/PegarListaContratoForms.json", method = RequestMethod.GET)
	public @ResponseBody List<Contrato> getListaProdutosAlvoByCliente(@PathVariable("idCliente") Long idCliente) {
		System.out.println("entrou no metodo getListaProdutosAlvoByCliente");
		System.out.println("ok idCliente:" + idCliente);

		List<Contrato> contratosAlvo = new ArrayList<Contrato>();
		contratosAlvo = contratoService.getListaProdutosAlvoByCliente(idCliente);

		System.out.println(contratosAlvo.size() + " TAMANHO");

		for (Contrato contrato : contratosAlvo) {
			System.out.print("contratosAlvo " + contrato.getId());
		}

		return contratosAlvo;
	}

	@RequestMapping(value = "/{contratoId}/VerificaPrincipal.json", method = RequestMethod.GET)
	public @ResponseBody boolean getVerificaPrincipal(@PathVariable("contratoId") Long contratoId) {
		Contrato contrato = contratoService.findById(contratoId);
		if (contrato.isProdutoAlvo() == true) {
			return true;
		}
		return false;

	}

	@Transactional
	@PostMapping(value = "/{idContrato}/{nomeCombo}/PegarNomeCombo.json")
	public @ResponseBody Contrato updatePegaNomeCombo(@PathVariable("idContrato") Long idContrato,
			@PathVariable("nomeCombo") String nomeCombo) {

		Contrato contrato = contratoService.findById(idContrato);
		contrato.setNomeCombo(nomeCombo);
		contratoService.update(contrato);

		System.out.println("o nome do combo �:" + nomeCombo);
		return contrato;
	}

	@RequestMapping(value = "/{idCliente}/PegarListaNomeCombo.json", method = RequestMethod.GET)
	public @ResponseBody List<String> getListaNomeCombo(@PathVariable("idCliente") Long idCliente) {
		System.out.println("entrou no metodo getListaNomeCombo" + idCliente);

		List<String> listaNomeCombo = new ArrayList<String>();
		listaNomeCombo = contratoService.getListaNomeCombo(idCliente);

		System.out.println(listaNomeCombo.size() + " quantidade de nomesCombo");

		/*
		 * for (Contrato contrato : listaNomeCombo) {
		 * System.out.println("listaNomeCombo " + contrato.getId() +
		 * contrato.getNomeCombo()); }
		 */

		return listaNomeCombo;
	}

	@PostMapping(value = "/{idContrato}/{comboNome}/NomeCombo.json")
	public @ResponseBody Contrato updateSetNomeComboESelect(@PathVariable("idContrato") Long idContrato,
			@PathVariable("comboNome") String comboNome) {

		System.out.println("entrou no metodo updateSetNomeComboESelect");
		System.out.println("idContrato" + idContrato);
		System.out.println("o nome do combo �:" + comboNome);

		Contrato contrato = contratoService.findById(idContrato);
		contrato.setNomeCombo(comboNome);
		contratoService.update(contrato);

		return contrato;

	}
	
	@GetMapping("/{id}/honorarios.json")
	public @ResponseBody List<Honorario> getHonorariosContrato(@PathVariable(name = "id") Long id) {

		return contratoService.findById(id).getHonorarios();

	}
	
	@PostMapping(value = "/{idContrato}/{periodo}/alteraPeriodoCobranca.json")
	@ResponseBody
	public void   updatePeriodoCobranca(@PathVariable("idContrato") Long idContrato,
			@PathVariable("periodo") String periodo) {

		System.out.println("idContrato"+idContrato);
		Contrato contrato = contratoService.findById(idContrato);
		contrato.setPeriodoCobranca(periodo);
		contratoService.update(contrato);
	}
	
	

}
