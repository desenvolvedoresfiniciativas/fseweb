package br.com.finiciativas.fseweb.controllers;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Divisao;
import br.com.finiciativas.fseweb.enums.Especialidade;
import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.enums.MotivoCancelamento;
import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.enums.TipoDeApuracao;
import br.com.finiciativas.fseweb.models.Eficiencia;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorNegocioChile;
import br.com.finiciativas.fseweb.pojo.SeguimentoPojo;
import br.com.finiciativas.fseweb.services.ItemTarefaService;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EficienciaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FilialServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.NotaFiscalServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.RelatorioTecnicoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.UltimaAtualizacaoVNService;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioCLServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Controller
@RequestMapping(path = "/relatorios")
public class RelatoriosController {

	@Autowired
	private EtapasConclusaoServiceImpl etapasConclusaoService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private NotaFiscalServiceImpl notaFiscalService;

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private EquipeServiceImpl equipeService;

	@Autowired
	private RelatorioTecnicoServiceImpl relatorioService;

	@Autowired
	private FilialServiceImpl filialService;

	@Autowired
	private ItemValoresServiceImpl itemValoresService;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ValorNegocioServiceImpl valorNegocioService;

	@Autowired
	private EficienciaServiceImpl eficienciaService;

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Autowired
	private UltimaAtualizacaoVNService ultimaAtualizacaoService;

	@Autowired
	private ContratoServiceImpl contratoService;
	
	@Autowired
	private BalanceteCalculoServiceImpl balanceteCalculoService;
	
	@Autowired
	private ValorNegocioCLServiceImpl clServiceImpl;
	
	@Autowired
	private ItemTarefaService itemTarefaService;

	@GetMapping("/valorNegocio")
	public ModelAndView vn() {
		ModelAndView mav = new ModelAndView();
		try {
			Date dataAtualizacao = ultimaAtualizacaoService.findById(1l).getUltimaAtualizacao();
			String anoAtualizado = ultimaAtualizacaoService.findById(1l).getAno();
			if (dataAtualizacao != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy '�s' H:mm:ss");
				String dataString = dateFormat.format(dataAtualizacao);

				mav.addObject("ultimaAtualizacaoVN", dataString);
			}
			if (anoAtualizado != null) {
				mav.addObject("anoAtualizado", anoAtualizado);
			}
		} catch (Exception e) {

		}
		mav.setViewName("/producao/relatorios/valorNegocio");

		return mav;
	}
	
	@GetMapping("/valorNegocioCL")
	public ModelAndView vnchile() {
		ModelAndView mav = new ModelAndView();
		List<ValorNegocioChile> vn = clServiceImpl.getAllVN();
		mav.addObject("valorNegocioChile",vn);
		mav.setViewName("/producao/relatorios/valorNegocioCL");
		return mav;
	}
	@GetMapping("/vNegocioCL")
	public ModelAndView geraVNChile() {
		ModelAndView mav = new ModelAndView();
		clServiceImpl.geraVN();
		List<ValorNegocioChile> vn = clServiceImpl.getAllVN();
		mav.addObject("valorNegocioChile",vn);
		mav.setViewName("/producao/relatorios/valorNegocioCL");
		return mav;
	}
	@PostMapping("/gerarVN")
	@ResponseBody
	public ModelAndView gerarVN(@RequestBody String ano) {

		//VN LEI DO BEM
		valorNegocioService.geraValorDeNegocioByAno(ano);
		
	
		ModelAndView mav = new ModelAndView();
//	mav.addObject("valorNegocios", valorNegocioService.getVNbyAno(ano));
//	mav.setViewName("/producao/relatorios/valorNegocioAno/"+ ano);
		// ModelAndView mav = vnAno(ano);

		mav.setViewName("redirect:/relatorios/vNegocio?ano=" + ano);
		return mav;
	}

	

	@GetMapping("/vNegocio")
	public ModelAndView vnAno(@RequestParam("ano") String ano) {
		ModelAndView mav = new ModelAndView();
		try {
			Date dataAtualizacao = ultimaAtualizacaoService.findById(1l).getUltimaAtualizacao();
			String anoAtualizado = ultimaAtualizacaoService.findById(1l).getAno();

			if (dataAtualizacao != null) {
				DateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy '�s' H:mm:ss");
				String dataString = dateFormat.format(dataAtualizacao);

				mav.addObject("ultimaAtualizacaoVN", dataString);
			}
			if (anoAtualizado != null) {
				mav.addObject("anoAtualizado", anoAtualizado);
			}
		} catch (Exception e) {

		}
		mav.addObject("valorNegocios", valorNegocioService.getVNbyAno(ano));
		mav.setViewName("/producao/relatorios/valorNegocioAno");
		return mav;
	}
	@GetMapping("/gerenciarVN")
	public ModelAndView vnUpdate() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/producao/relatorios/valorNegocioUpdate");
		return mav;
	}

	@GetMapping("/formValorNegocio")
	public ModelAndView formvn() {
		ModelAndView mav = new ModelAndView();

		mav.setViewName("/producao/relatorios/valorNegocioManual");
		return mav;
	}

	@GetMapping("/preRelatoriosAnaliticos")
	public ModelAndView preRelatoriosAnaliticos() {

		ModelAndView mav = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));

		if (equipe != null) {
			mav.addObject("consultores", equipeService.getConsultores(equipe.getId()));
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		} else {
			mav.addObject("consultores", consultorService.getConsultores());
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		}

		mav.setViewName("/producao/relatorios/preRelatoriosAnaliticos");
//		mav.addObject("consultores", consultorService.getConsultores());
//		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("especialidade", Especialidade.values());
		mav.addObject("estados", Estado.values());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("projetos", relatorioService.getAll());
		mav.addObject("feitoPor", consultorService.getAll());
		mav.addObject("corrigidoPor", consultorService.getAll());

		return mav;

	}

	@PostMapping("/relatoriosAnaliticos")
	public ModelAndView relatoriosAnaliticos(@RequestParam(value = "ano", required = false) String ano,
			@RequestParam(value = "setor", required = false) SetorAtividade setor,
			@RequestParam(value = "especialidade", required = false) Especialidade especialidade,
			@RequestParam(value = "equipe", required = false) Long idEquipe,
			@RequestParam(value = "consultor", required = false) Long idConsultor,
			@RequestParam(value = "feitoPor", required = false) Long idFeitoPor,
			@RequestParam(value = "corrigidoPor", required = false) Long idCorrigidoPor,
			@RequestParam(value = "estado", required = false) Estado estado) {
		ModelAndView mav = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));

		List<RelatorioTecnico> analiticos = producaoService.getRelatorioAnalitico(ano, setor, especialidade, idEquipe,
				idConsultor, idFeitoPor, idCorrigidoPor, estado);

		List<EtapasConclusao> etapas = new ArrayList<EtapasConclusao>();

		for (RelatorioTecnico rel : analiticos) {
			etapas.add(etapasConclusaoService.getEtapaRelatorioByProducao(rel.getProducao().getId()));
		}

		if (equipe != null) {
			mav.addObject("consultores", equipeService.getConsultores(equipe.getId()));
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		} else {
			mav.addObject("consultores", consultorService.getConsultores());
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		}

		mav.addObject("etapas", etapas);
//		mav.addObject("consultores", consultorService.getConsultores());
//		mav.addObject("lideres", consultorService.getConsultores());
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("especialidade", Especialidade.values());
		mav.addObject("estados", Estado.values());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("projetos", relatorioService.getAll());
		mav.addObject("analitico", analiticos);
		mav.addObject("feitoPor", consultorService.getAll());
		mav.addObject("corrigidoPor", consultorService.getAll());

		mav.setViewName("/producao/relatorios/relatoriosAnaliticos");

		return mav;

	}

	@GetMapping("/PreSintetico")
	public ModelAndView PreRelatoriosSinteticos() {
		ModelAndView mav = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));

		if (equipe != null) {
			mav.addObject("consultores", equipeService.getConsultores(equipe.getId()));
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		} else {
			mav.addObject("consultores", consultorService.getConsultores());
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		}

//		if(equipe.getEquipe() != null) {
//			mav.addObject("consultores", equipeService.findEquipeByConsultor(consultor.getId()));
//			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
//		} else {
//			mav.addObject("consultores", consultorService.getConsultores());
//			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
//		}

//		mav.addObject("consultores", consultorService.getAll());
//		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("filial", filialService.getAll());
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("estados", Estado.values());
		mav.setViewName("/producao/relatorios/relatoriosPreSinteticos");
		return mav;
	}

	@PostMapping("/sintetico")
	public ModelAndView relatoriosSinteticos(@RequestParam(value = "ano", required = false) String ano,
			@RequestParam(value = "setorAtividade", required = false) SetorAtividade setorAtividade,
			@RequestParam(value = "consultor", required = false) Long idConsultor,
			@RequestParam(value = "equipe", required = false) Long idEquipe) {

		ModelAndView mav = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));

//		List<Producao> producoes = producaoService.getRelatorioSintetico(ano, setorAtividade, idConsultor, idEquipe);

		if (equipe != null) {
			mav.addObject("consultores", equipeService.getConsultores(equipe.getId()));
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		} else {
			mav.addObject("consultores", consultorService.getConsultores());
			mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		}

		List<Producao> producoes = producaoService.getRelatorioSintetico(ano, setorAtividade, idConsultor, idEquipe);

		mav.addObject("producoes", producaoService.getRelatorioSintetico(ano, setorAtividade, idConsultor, idEquipe));
//		mav.addObject("consultores", consultorService.getAll());
		mav.addObject("equipe", equipeService.getAll());
		mav.addObject("filial", filialService.getAll());
//		mav.addObject("lideres", consultorService.getConsultorByCargo(Cargo.CONSULTOR_LIDER_TECNICO));
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("estados", Estado.values());
		mav.addObject("producoes", producoes);

//		mav.addObject("anoTexto", ano);

		if (setorAtividade != null) {
			mav.addObject("setorAtividadeTexto", setorAtividade);
		}

		if (idConsultor != null) {
			mav.addObject("consultorTexto", idConsultor);
		}

		if (idEquipe != null) {
			mav.addObject("equipeTexto", idEquipe);
		}

		mav.setViewName("/producao/relatorios/relatoriosSinteticos");
		return mav;
	}

	@GetMapping("/{id}")
	public ModelAndView pontoSituacao(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();

		Produto produto = produtoService.findById(id);

		List<EtapaTrabalho> etapasTrabalho = produto.getEtapasTrabalho();
		// List<EtapasConclusao> etapasConclusao =
		// etapasConclusaoService.getAllEtapasByProduto(id);
		List<Cliente> clientesUntreated = new ArrayList<Cliente>();
		List<Cliente> clientesTreated = new ArrayList<Cliente>();

//		for (EtapasConclusao etapa : etapasConclusao) {
//			clientesUntreated.add(etapa.getProducao().getContrato().getCliente());
//		}

		for (Cliente cliente : clientesUntreated) {
			if (!clientesTreated.contains(cliente)) {
				clientesTreated.add(cliente);
			}
		}

		mav.addObject("produto", produto);
		mav.addObject("etapasTrabalho", etapasTrabalho);
//		mav.addObject("etapasConclusao", etapasConclusao);
//		mav.addObject("clientes", clientesTreated);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("equipes", equipeService.getAll());
		mav.setViewName("/pontoSituacao/relatorio");

		return mav;

	}

	@GetMapping("/PreSeguimento")
	public ModelAndView preSeguimento() {

		ModelAndView mav = new ModelAndView();

		mav.setViewName("/producao/relatorios/preSeguimento");
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("filial", filialService.getAll());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("situacao", Situacao.values());
		mav.addObject("setorAtividade", SetorAtividade.values());

		return mav;

	}

	@PostMapping("/seguimento")
	public ModelAndView seguimento(@RequestParam(value = "razaoSocial", required = false) String razaoSocial,
			@RequestParam(value = "ano", required = false) String ano,
			@RequestParam(value = "filial", required = false) Long idFilial,
			@RequestParam(value = "equipe", required = false) Long idEquipe,
			@RequestParam(value = "consultor", required = false) Long idConsultor,
			@RequestParam(value = "situacao", required = false) Situacao situacao,
			@RequestParam(value = "setor", required = false) SetorAtividade setor) {
		ModelAndView mav = new ModelAndView();

		if (razaoSocial == "") {
			razaoSocial = null;
		}

		List<SeguimentoPojo> prods = producaoService.getProducaoByCriteria(razaoSocial, ano, idFilial, idEquipe, idConsultor,
				situacao, setor);
		System.out.println(prods.size());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("filial", filialService.getAll());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("situacao", Situacao.values());
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("producao", prods);
		mav.setViewName("/producao/relatorios/seguimento");

		return mav;
	}

	@GetMapping("/PreSeguimentoOP")
	public ModelAndView preSeguimentoOP() {

		ModelAndView mav = new ModelAndView();

		mav.setViewName("/producao/relatorios/preSeguimentoOP");
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("filial", filialService.getAll());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("divisao", Divisao.values());
		mav.addObject("situacao", Situacao.values());
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("produtos", produtoService.getAll());
		return mav;

	}

	@PostMapping("/seguimentoOP")
	public ModelAndView seguimentoOP(@RequestParam(value = "razaoSocial", required = false) String razaoSocial,
			@RequestParam(value = "ano", required = false) String ano,
			@RequestParam(value = "filial", required = false) Long idFilial,
			@RequestParam(value = "equipe", required = false) Long idEquipe,
			@RequestParam(value = "consultor", required = false) Long idConsultor,
			@RequestParam(value = "situacao", required = false) Situacao situacao,
			@RequestParam(value = "setor", required = false) SetorAtividade setor,
			@RequestParam(value = "produto", required = false) Long idProduto) {
		ModelAndView mav = new ModelAndView();

		if (razaoSocial == "") {
			razaoSocial = null;
		}

		List<Producao> prods = producaoService.getProducaoByCriteriaOP(razaoSocial, ano, idFilial, idEquipe,
				idConsultor, situacao, setor, idProduto);

		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("filial", filialService.getAll());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("situacao", Situacao.values());
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("producao", prods);
		mav.addObject("produtos", produtoService.getAll());
		mav.setViewName("/producao/relatorios/seguimentoOP");

		return mav;
	}

	@GetMapping("/produtosSeguimento")
	public ModelAndView selecaoSeguimento() {

		ModelAndView mav = new ModelAndView();

		mav.setViewName("/producao/relatorios/produtos");

		return mav;
	}

	@GetMapping("/PreEficiencia")
	public ModelAndView preEficiencia() {
		
		ModelAndView mav = new ModelAndView();

		mav.setViewName("/producao/relatorios/preEficiencia");
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("filial", filialService.getAll());
		mav.addObject("situacao", Situacao.values());
		return mav;
	}

	@PostMapping("/eficiencia")
	public ModelAndView eficiencia(@RequestParam(value = "ano", required = false) String ano,
			@RequestParam(value = "setor", required = false) SetorAtividade setor,
			@RequestParam(value = "filial", required = false) Long idFilial,
			@RequestParam(value = "equipe", required = false) Long idEquipe,
			@RequestParam(value = "consultor", required = false) Long idConsultor,
			@RequestParam(value = "situacao", required = false) Situacao situacao,
			@RequestParam(value = "razaoSocial", required = false) String razaoSocial){

		ModelAndView mav = new ModelAndView();
		System.out.println("razaoSocial"+razaoSocial);
		
		if (razaoSocial == "") {
			razaoSocial = null;
		}
		System.out.println("situa��o"+situacao);
		List<Eficiencia> eficiencias = eficienciaService.getEficienciaByCriteria(ano, setor, idFilial, idEquipe,
				idConsultor,razaoSocial,situacao);

		mav.addObject("eficiencias", eficiencias);

		mav.setViewName("/producao/relatorios/eficiencia");
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("filial", filialService.getAll());
		mav.addObject("etapaTrabalho", etapaTrabalhoService.getAll());
		mav.addObject("situacao", Situacao.values());

		return mav;
	}

	@GetMapping("/{ano}/{setorAtividade}/{idConsultor}/{idEquipe}/rels.json")
	public @ResponseBody List<RelatorioTecnico> getAllRelatorios(@PathVariable("ano") String ano,
			@PathVariable("setorAtividade") String setorAtividade, @PathVariable("idConsultor") String idConsultor,
			@PathVariable("idEquipe") String idEquipe) {

		Long idConultorL = null;
		Long idEquipeL = null;

		SetorAtividade setor = null;

		if (ano.equalsIgnoreCase("NULO")) {
			ano = null;
		}

		if (!setorAtividade.equalsIgnoreCase("NULO") && !setorAtividade.equalsIgnoreCase("undefined")) {
			setor = SetorAtividade.valueOf(setorAtividade);
		}

		if (!idConsultor.equalsIgnoreCase("NULO")) {
			idConultorL = Long.parseLong(idConsultor);
		} else {
			idConultorL = null;
		}

		if (!idEquipe.equalsIgnoreCase("NULO")) {
			idEquipeL = Long.parseLong(idEquipe);
		} else {
			idEquipeL = null;
		}

		List<RelatorioTecnico> rels = relatorioService.getRelatorioTecnicoByCriteria(ano, setor, idConultorL,
				idEquipeL);

		return rels;
	}

	@GetMapping("/{ano}/{setorAtividade}/{idConsultor}/{idEquipe}/prev.json")
	public @ResponseBody List<ItemValores> getAllPrevRels(@PathVariable("ano") String ano,
			@PathVariable("setorAtividade") String setorAtividade, @PathVariable("idConsultor") String idConsultor,
			@PathVariable("idEquipe") String idEquipe) {
		Long idConultorL = null;
		Long idEquipeL = null;

		SetorAtividade setor = null;

		if (ano.equalsIgnoreCase("NULO")) {
			ano = null;
		}

		if (!setorAtividade.equalsIgnoreCase("NULO")) {
			setor = SetorAtividade.valueOf(setorAtividade);
		}

		if (!idConsultor.equalsIgnoreCase("NULO")) {
			idConultorL = Long.parseLong(idConsultor);
		} else {
			idConultorL = null;
		}

		if (!idEquipe.equalsIgnoreCase("NULO")) {
			idEquipeL = Long.parseLong(idEquipe);
		} else {
			idEquipeL = null;
		}
		List<ItemValores> prev = itemValoresService.getPrevisaoDeRelatoriosByCriteria(ano, setor, idConultorL,
				idEquipeL);

		return prev;
	}

	@GetMapping("/vn/{ano}.json")
	public @ResponseBody List<ValorNegocio> getVNbyAno(@PathVariable("ano") String ano) {
		return valorNegocioService.getVNbyAno(ano);

	}

	@PostMapping("/updateVN")
	@ResponseBody
	public void updateVN(@RequestBody ValorNegocio vn) {

		ValorNegocio vnAtt = valorNegocioService.findById(vn.getId());
		vnAtt.setValorBase(vn.getValorBase());
		vnAtt.setModoValor("Manual");

		valorNegocioService.update(vnAtt);

	}

	@GetMapping("/BuscaVN/{id}.json")
	public @ResponseBody List<ValorNegocio> getVNbyEmpresa(@PathVariable("id") Long id) {

		return valorNegocioService.getVNbyEmpresa(id);

	}

	@GetMapping("/BuscaVNAtt/{id}.json")
	public @ResponseBody ValorNegocio getVNbyID(@PathVariable("id") Long id) {

		return valorNegocioService.findById(id);

	}

	@GetMapping("/relatorioFaturamento")
	public ModelAndView relatorioFaturamento(@RequestParam(value = "campanha", required = false) String ano,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger,
			@RequestParam(value = "idProduto", required = false) Long idProduto,
			@RequestParam(value = "filial", required = false) Long idFilial,
			@RequestParam(value = "empresa", required = false) String empresa,
			@RequestParam(value = "tipoNegocio", required = false) String tipoNegocio,
			@RequestParam(value = "etapa", required = false) Long etapa) {

		ModelAndView mav = new ModelAndView();

//		List<Faturamento> faturamentos = faturamentoService.getAllFaturamentosByCriteria(campanha);
		List<Faturamento> faturamentos = new ArrayList<Faturamento>();

		if (queryTrigger != null) {

			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}
			if (ano.equalsIgnoreCase("")) {
				ano = null;
			}
//			if(etapa.equalsIgnoreCase("")) {
//				etapa = null;
//			}
			if (tipoNegocio.equalsIgnoreCase("")) {
				tipoNegocio = null;
			}

//			if(tpNegocio.equalsIgnoreCase("")){
//				tpNegocio = null;
//			}

			if (queryTrigger.equalsIgnoreCase("true")) {
				faturamentos = faturamentoService.getAllFaturamentosByCriteria(ano, idProduto, idFilial, empresa,
						tipoNegocio, etapa);

			}
		}

		List<Produto> produtos = produtoService.getAll();

		mav.addObject("produtos", produtos);
		mav.addObject("empresa", empresa);
		mav.addObject("faturamentos", faturamentos);
		mav.addObject("filiais", filialService.getAll());
		mav.setViewName("/faturamento/relatorios/relatorioFaturamento");

		return mav;
	}
	
	@GetMapping("/relatorioAcompanhamentoFaturamento")
	public ModelAndView relatorioAcompanhamentoFaturamento(@RequestParam(value = "campanha", required = false) String ano,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger,
			@RequestParam(value = "idProduto", required = false) Long idProduto,
			@RequestParam(value = "filial", required = false) Long idFilial,
			@RequestParam(value = "empresa", required = false) String empresa,
			@RequestParam(value = "tipoNegocio", required = false) String tipoNegocio,
			@RequestParam(value = "etapa", required = false) Long etapa) {

		ModelAndView mav = new ModelAndView();

//		List<Faturamento> faturamentos = faturamentoService.getAllFaturamentosByCriteria(campanha);
		List<Faturamento> faturamentos = new ArrayList<Faturamento>();

		if (queryTrigger != null) {

			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}
			if (ano.equalsIgnoreCase("")) {
				ano = null;
			}
//			if(etapa.equalsIgnoreCase("")) {
//				etapa = null;
//			}
			if (tipoNegocio.equalsIgnoreCase("")) {
				tipoNegocio = null;
			}

//			if(tpNegocio.equalsIgnoreCase("")){
//				tpNegocio = null;
//			}

			if (queryTrigger.equalsIgnoreCase("true")) {
				faturamentos = faturamentoService.getAllFaturamentosByCriteria(ano, idProduto, idFilial, empresa,
						tipoNegocio, etapa);

			}
		}

		List<Produto> produtos = produtoService.getAll();

		mav.addObject("produtos", produtos);
		mav.addObject("empresa", empresa);
		mav.addObject("faturamentos", faturamentos);
		mav.addObject("filiais", filialService.getAll());
		mav.setViewName("/faturamento/relatorios/RelatorioAcompanhamentoFaturamento");

		return mav;
	}

	@GetMapping("/relatorioNotaFiscal")
	public ModelAndView relatorioNotaFiscal(@RequestParam(value = "campanha", required = false) String ano,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger,
			@RequestParam(value = "idProduto", required = false) Long idProduto,
			@RequestParam(value = "filial", required = false) Long idFilial,
			@RequestParam(value = "empresa", required = false) String empresa,
			@RequestParam(value = "tipoNegocio", required = false) String tipoNegocio,
			@RequestParam(value = "etapa", required = false) Long etapa,
			@RequestParam(value = "cancelada", required = false) String canceladaString,
			@RequestParam(value = "emAtraso", required = false) String emAtrasoString,
			@RequestParam(value = "motivoCancelamento", required = false) String motivoCancelamento,
			@RequestParam(value = "numeroNF", required = false) String numeroNF,
			@RequestParam(value = "dataInicioEmissao", required = false) String dataInicioEmissao,
			@RequestParam(value = "dataFinalEmissao", required = false) String dataFinalEmissao,
			@RequestParam(value = "dataInicioCobrar", required = false) String dataInicioCobrar,
			@RequestParam(value = "dataFinalCobrar", required = false) String dataFinalCobrar,
			@RequestParam(value = "dataInicioCobranca", required = false) String dataInicioCobranca,
			@RequestParam(value = "dataFinalCobranca", required = false) String dataFinalCobranca) {

		ModelAndView mav = new ModelAndView();

		List<NotaFiscal> notas = new ArrayList<NotaFiscal>();

		if (queryTrigger != null) {

			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}
			if (ano.equalsIgnoreCase("")) {
				ano = null;
			}
//			if(etapa.equalsIgnoreCase("")) {
//				etapa = null;
//			}
			if (tipoNegocio.equalsIgnoreCase("")) {
				tipoNegocio = null;
			}

			if (motivoCancelamento.equals("") || motivoCancelamento.equals("NONE")) {
				motivoCancelamento = null;
			}
			if (numeroNF.equalsIgnoreCase("")) {
				numeroNF = null;
			}
			if (dataInicioEmissao.equals("") || dataFinalEmissao.equals("")) {
				dataInicioEmissao = null;
				dataFinalEmissao = null;
			} else {

				// In�cio
				String diaEmissao = dataInicioEmissao.substring(0, 2);
				String mesEmissao = dataInicioEmissao.substring(3, 5);
				String anoEmissao = dataInicioEmissao.substring(6);
				dataInicioEmissao = anoEmissao + "-" + mesEmissao + "-" + diaEmissao;

				// Fim
				String diaFimEmissao = dataFinalEmissao.substring(0, 2);
				String mesFimEmissao = dataFinalEmissao.substring(3, 5);
				String anoFimEmissao = dataFinalEmissao.substring(6);
				dataFinalEmissao = anoFimEmissao + "-" + mesFimEmissao + "-" + diaFimEmissao;

			}

			if (dataInicioCobrar.equals("") || dataFinalCobrar.equals("")) {
				dataInicioCobrar = null;
				dataFinalCobrar = null;
			} else {

				// In�cio
				String diaInicioCobrar = dataInicioCobrar.substring(0, 2);
				String mesInicioCobrar = dataInicioCobrar.substring(3, 5);
				String anoInicioCobrar = dataInicioCobrar.substring(6);
				dataInicioCobrar = anoInicioCobrar + "-" + mesInicioCobrar + "-" + diaInicioCobrar;

				// Fim
				String diaFimCobrar = dataFinalCobrar.substring(0, 2);
				String mesFimCobrar = dataFinalCobrar.substring(3, 5);
				String anoFimCobrar = dataFinalCobrar.substring(6);
				dataFinalCobrar = anoFimCobrar + "-" + mesFimCobrar + "-" + diaFimCobrar;

			}

			if (dataInicioCobranca.equals("") || dataFinalCobranca.equals("")) {
				dataInicioCobranca = null;
				dataFinalCobranca = null;
			} else {

				// In�cio
				String diaInicioCobranca = dataInicioCobranca.substring(0, 2);
				String mesInicioCobranca = dataInicioCobranca.substring(3, 5);
				String anoInicioCobranca = dataInicioCobranca.substring(6);
				dataInicioCobranca = anoInicioCobranca + "-" + mesInicioCobranca + "-" + diaInicioCobranca;

				// Fim
				String diaFimCobranca = dataFinalCobranca.substring(0, 2);
				String mesFimCobranca = dataFinalCobranca.substring(3, 5);
				String anoFimCobranca = dataFinalCobranca.substring(6);
				dataFinalCobranca = anoFimCobranca + "-" + mesFimCobranca + "-" + diaFimCobranca;

			}

			Boolean cancelada;
			Boolean emAtraso;

			if (canceladaString == "") {
				cancelada = null;
			} else if (canceladaString.equals("true")) {
				cancelada = true;
			} else {
				cancelada = false;
			}

			if (emAtrasoString == "") {
				emAtraso = null;
			} else if (emAtrasoString.equals("true")) {
				emAtraso = true;
			} else {
				emAtraso = false;
			}

			if (queryTrigger.equalsIgnoreCase("true")) {
				notas = notaFiscalService.getAllNotasByCriteria(ano, idProduto, idFilial, empresa, tipoNegocio, etapa,
						cancelada, emAtraso, motivoCancelamento, numeroNF, dataInicioEmissao, dataFinalEmissao,
						dataInicioCobrar, dataFinalCobrar, dataInicioCobranca, dataFinalCobranca);
			}

		}

		List<Produto> produtos = produtoService.getAll();

		int parcela = 1;
		double totalReceber = 0;
		double totalPago = 0;
		double soma = 0;
		String receber = "";
		String pago = "";
		String total = "";

		for (NotaFiscal notaFiscalCard : notas) {
			System.out.println("notaFiscalCard:      " + notaFiscalCard);
			// if nota atual tem parcelas, a� fa�o o c�lculo de uma parcela
			String valor = notaFiscalCard.getCobrar();

			parcela = notaFiscalCard.getParcela();

			if (notaFiscalCard.isCobranca() == false && valor.isEmpty() == false) {

				valor = valor.replaceAll("\\.", "");
				valor = valor.replaceAll("\\,", "");
				valor = valor.replaceAll("\\ ", "");
				valor = valor.replaceAll("\\R$", "");

				double valorReceber = Double.parseDouble(valor);
				double valorReceberFormatado = valorReceber / 100;
				totalReceber = totalReceber + valorReceberFormatado;
				Locale meuLocal = new Locale("pt", "BR");
				NumberFormat nfVal = NumberFormat.getCurrencyInstance(meuLocal);
				receber = nfVal.format(totalReceber);
			}

			if (notaFiscalCard.isCobranca() == true && valor.isEmpty() == false) {

				valor = valor.replaceAll("\\.", "");
				valor = valor.replaceAll("\\,", "");
				valor = valor.replaceAll("R", "");
				valor = valor.replaceAll("\\R", "");
				valor = valor.replaceAll("\\$", "");

				Double valorPago = Double.parseDouble(valor);
//				double valorPagoFormatado = valorPago / 100;
//				totalPago = totalPago + valorPagoFormatado;
				Locale meuLocal = new Locale("pt", "BR");
				NumberFormat nfVal = NumberFormat.getCurrencyInstance(meuLocal);
				pago = nfVal.format(totalPago);
			}
			soma = totalPago + totalReceber;
			total = NumberFormat.getCurrencyInstance().format(soma);
		}

//		mav.addObject("total", total);
//		mav.addObject("totalPago", pago);
//		mav.addObject("totalReceber", receber);
		mav.addObject("parcela", parcela);
		mav.addObject("produtos", produtos);
		mav.addObject("empresa", empresa);
		mav.addObject("notaFiscal", notas);
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("motivoCancelamento", MotivoCancelamento.values());
		mav.setViewName("/faturamento/relatorios/relatorioNotaFiscal");

		return mav;
	}

	@GetMapping("/relatoriosCombo")
	public ModelAndView relatoriosCombo(@RequestParam(value = "idProduto", required = false) Long idProduto,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger,
			@RequestParam(value = "empresa", required = false) String empresa,
			@RequestParam(value = "ComercialRespons�velEntrada", required = false) Long comercialRespEntrada,
			@RequestParam(value = "ComercialRespons�velAtual", required = false) Long comercialRespAtual) {

		ModelAndView mav = new ModelAndView();

		if (queryTrigger != null) {
			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}

			if (queryTrigger.equalsIgnoreCase("true")) {

				List<Contrato> combo = contratoService.getRelatorioCombo(idProduto, comercialRespEntrada,
						comercialRespAtual, empresa);
				mav.addObject("relatorioCombo", combo);
			}
		}

		mav.addObject("comerciaisTodos", this.consultorService.getConsultorByCargoAll(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("comerciaisAtivos", this.consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("empresa", empresa);
		mav.addObject("produtos", produtoService.getAll());
		mav.setViewName("/producao/relatorios/relatoriosCombo");
		return mav;
	}
	
	
	
	@GetMapping("/gerenciarValorNegocio")
	public ModelAndView gerenciarValorNegocio(@RequestParam(value = "cnpj", required = false) String cnpj,
	        @RequestParam(value = "queryTrigger", required = false) String queryTrigger,
	        @RequestParam(value = "nomeEmpresa", required = false) String nomeEmpresa) {
		System.out.println("entrou aqui :      "+cnpj);
		System.out.println("razaoSocial :      "+nomeEmpresa);
		System.out.println("entrou aqui :      ");
	    ModelAndView mav = new ModelAndView();

	    if (queryTrigger != null) {
	    	
	    	if (cnpj.equalsIgnoreCase("")) {
	    		cnpj = null;
	        }
	    	
	        if (nomeEmpresa.equalsIgnoreCase("")) {
	        	nomeEmpresa = null;
	        }

	        if (queryTrigger.equalsIgnoreCase("true")) {

	            List<ValorNegocio> gerenciarVN = valorNegocioService.getGerenciarValorNegocio(cnpj, nomeEmpresa);
	            
	            mav.addObject("gerenciarVN", gerenciarVN);
	        }
	    }
	    mav.setViewName("/producao/relatorios/valorNegocioUpdate");
	    return mav;
	}

	@PostMapping("/carrega.jsonObject")
	public @ResponseBody List<JSONObject> carregaPontoSituacao(@RequestBody JSONObject parametros) {
		List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
		System.out.println(parametros);
		String campanha = null;
		String razaoSocial = null;
		String tipoNegocio = null;
		Long idProduto = null;
		Long idFilial = null;
		Long idEtapa = null;
		Boolean quantificacao = null;
		Boolean submissao = null;
		String gerouFaturamento = null;
		
		if (Objects.nonNull(parametros.get("quantificacao").toString())
				&& parametros.get("quantificacao").toString() != "") {
			quantificacao = Boolean.parseBoolean(parametros.get("quantificacao").toString());
			System.out.println("o parametro de quantifica��o �: " + quantificacao);
		}
		if (Objects.nonNull(parametros.get("submissao").toString()) && parametros.get("submissao").toString() != "") {
			submissao = Boolean.parseBoolean(parametros.get("submissao").toString());
			System.out.println("o parametro de submissao �: " + submissao);
		}		
		if (Objects.nonNull(parametros.get("ano").toString()) && parametros.get("ano").toString() != "") {
			campanha = parametros.get("ano").toString();
			System.out.println("campanha" + campanha);
		}
		
		if (Objects.nonNull(parametros.get("razaoSocial").toString()) && parametros.get("razaoSocial").toString() != "") {
			razaoSocial = parametros.get("razaoSocial").toString();
			System.out.println("razaoSocial" + razaoSocial);
		}
		
		if (Objects.nonNull(parametros.get("tipoNegocio").toString()) && parametros.get("tipoNegocio").toString() != "") {
			tipoNegocio = parametros.get("tipoNegocio").toString();
			System.out.println("tipoNegocio" + tipoNegocio);
		}
		
		if (Objects.nonNull(parametros.get("produto").toString()) && parametros.get("produto").toString() != "") {
			idProduto = Long.parseLong(parametros.get("produto").toString());
			System.out.println("idProduto" + idProduto);
		}
		
		if (Objects.nonNull(parametros.get("filial").toString()) && parametros.get("filial").toString() != "") {
			idFilial = Long.parseLong(parametros.get("filial").toString());
			System.out.println("idFilial" + idFilial);
		}
		
		List<EtapasConclusao> etapas = etapasConclusaoService.getAllEtapasByCriteriaAcompanhamento(campanha,razaoSocial,tipoNegocio,idProduto,idFilial,idEtapa);
		try {
			List<EtapasConclusao> filtros = new ArrayList();
			
			for (EtapasConclusao etapa : etapas) {
				
				if (submissao != null && etapa.getEtapa().getId() == 68l && etapa.isConcluido() != submissao) {
					List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
							.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					if (Objects.nonNull(etapasFiltrar)) {
						filtros.addAll(etapasFiltrar);
					}
				}else if (quantificacao != null && etapa.getEtapa().getId() == 66l) {
					// List <EtapasConclusao> etapasFiltrar =
					// etapasConclusaoService.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					System.out.println("entrou no filtro de quantifica��o");
					List<BalanceteCalculo> balancetes = balanceteCalculoService
							.getBalancetesByProducao(etapa.getProducao().getId());
					int countBalancetes = 0;
					for (BalanceteCalculo balancete : balancetes) {
						if (balancete.isValidado() && balancetes.size() > 0) {
							countBalancetes++;
						}
					}
					if (quantificacao == true) {
						System.out.println("entrou no filtro de quantifica��o true" + balancetes.size());
						if (balancetes.size() != countBalancetes) {
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						} else if (balancetes.size() == 0) {
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						}
					} else {
						if (balancetes.size() == countBalancetes && balancetes.size() != 0) {
							System.out.println("entrou no filtro de quantifica��o false");
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						}
					}
				}	
			}
			
			if (Objects.nonNull(filtros)) {
				etapas.removeAll(filtros);
			}
		}catch(Exception e) {
			
		}
		
		
		int contador = 0;
		Long producaoId = null;
		JSONObject linhaAcompanhamento = new JSONObject();
		
		for (EtapasConclusao etapa : etapas) {
			if (contador == 0) {
				producaoId = etapa.getProducao().getId();

			}
			
			//condi��o para pular linha ao finalizar as etapas de uma produ��o 
			if (producaoId != etapa.getProducao().getId()) {
				System.out.println("salvou na troca de produ��o");
				System.out.println(linhaAcompanhamento);
				jsonObjects.add(linhaAcompanhamento);

				producaoId = etapa.getProducao().getId();
				linhaAcompanhamento = new JSONObject();
			}
			
			List<Faturamento> faturamentosByProducao = faturamentoService.getAllByProducao(producaoId);
			if(faturamentosByProducao.size() <= 0) {
				gerouFaturamento = "N�O";
			} else {
				gerouFaturamento = "SIM";
			}
			
			linhaAcompanhamento.put("razaoSocial", etapa.getProducao().getCliente().getRazaoSocial());
			linhaAcompanhamento.put("campanha", etapa.getProducao().getAno());
			linhaAcompanhamento.put("equipe", etapa.getProducao().getEquipe().getNome());
			linhaAcompanhamento.put("apuracao", etapa.getProducao().getTipoDeApuracao());
			linhaAcompanhamento.put("idProducao", etapa.getProducao().getId());
			linhaAcompanhamento.put("gerouFaturamento", gerouFaturamento);
			
			ValorNegocio vn = valorNegocioService.getVNbyProd(producaoId);
			try {
				linhaAcompanhamento.put("vn", vn.getValorBase());
				linhaAcompanhamento.put("modo", vn.getModoValor());
			}catch(Exception e) {
				
			}
			
			//incluir perceltual de contrato 
			linhaAcompanhamento.put("quantificacaoPercent", 0);
			EtapasPagamento etapasPagamento = etapa.getProducao().getContrato().getEtapasPagamento();
			
			if(Objects.nonNull(etapasPagamento)) {
				if(Objects.nonNull(etapasPagamento.getEtapa1())) {
					System.out.println("o contrato � " + etapa.getProducao().getContrato().getId());
					if(etapasPagamento.getEtapa1().getId() == 66l) {
						linhaAcompanhamento.put("quantificacaoPercent", etapasPagamento.getPorcentagem1());
					}
				} if(Objects.nonNull(etapasPagamento.getEtapa2())){
					if(etapasPagamento.getEtapa2().getId() == 66l) {
						linhaAcompanhamento.put("quantificacaoPercent", etapasPagamento.getPorcentagem2());
					}
				} if(Objects.nonNull(etapasPagamento.getEtapa3())) {
					if(etapasPagamento.getEtapa3().getId() == 66l) {
						linhaAcompanhamento.put("quantificacaoPercent", etapasPagamento.getPorcentagem3());
					}
				} if(Objects.nonNull(etapasPagamento.getEtapa4())) {
					if(etapasPagamento.getEtapa4().getId() == 66l) {
						linhaAcompanhamento.put("quantificacaoPercent", etapasPagamento.getPorcentagem4());
					}
				}
			}
			
			
			//percentual submiss�o
			linhaAcompanhamento.put("submissaoPercent", 0);
			if(Objects.nonNull(etapasPagamento)) {
				if(Objects.nonNull(etapasPagamento.getEtapa1())) {
			 		if(etapasPagamento.getEtapa1().getId() == 68l) {
			 			linhaAcompanhamento.put("submissaoPercent", etapasPagamento.getPorcentagem1());
			 		}
				} 
			 	if(Objects.nonNull(etapasPagamento.getEtapa2())){
					if(etapasPagamento.getEtapa2().getId() == 68l) {
						linhaAcompanhamento.put("submissaoPercent", etapasPagamento.getPorcentagem2());
					}
				} 
			 	if(Objects.nonNull(etapasPagamento.getEtapa3())) {
			 		if(etapasPagamento.getEtapa3().getId() == 68l) {
			 			linhaAcompanhamento.put("submissaoPercent", etapasPagamento.getPorcentagem3());
			 		}
				} 
			 	if(Objects.nonNull(etapasPagamento.getEtapa4())) {
			 		if(etapasPagamento.getEtapa4().getId() == 68l) {
			 			linhaAcompanhamento.put("submissaoPercent", etapasPagamento.getPorcentagem4());
			 		}
				}
			}
		 	
			
			
			if (etapa.getEtapa().getId() == 66l) {
				System.out.println("entrou quantifica��o ");
				List<BalanceteCalculo> balancetes = balanceteCalculoService
						.getBalancetesByProducao(etapa.getProducao().getId());
				System.out.println(balancetes.size());
				int countBalancetes = 0;
				for (BalanceteCalculo balancete : balancetes) {
					System.out.println(
							"a producao que esta sendo verificadas os balancetes � " + etapa.getProducao().getId());
					System.out.println("o id do balancete �: " + balancete.getId());
					if (balancete.isValidado() || balancete.isPrejuizo()) {
						countBalancetes++;
					}
				}
				int quantidadeBalancetes = 0;
				if (etapa.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
					quantidadeBalancetes = 1;
					if (countBalancetes >= 1) {
						linhaAcompanhamento.put("quantificacao", 1 + "/" + quantidadeBalancetes);
					} else {
						linhaAcompanhamento.put("quantificacao", 0 + "/" + quantidadeBalancetes);
					}

				}
				if (etapa.getProducao().getTipoDeApuracao() == TipoDeApuracao.TRIMESTRAL) {
					quantidadeBalancetes = 4;
					linhaAcompanhamento.put("quantificacao", countBalancetes + "/" + quantidadeBalancetes);
				}
				if (etapa.getProducao().getTipoDeApuracao() == TipoDeApuracao.MENSAL) {
					quantidadeBalancetes = 12;
					linhaAcompanhamento.put("quantificacao", countBalancetes + "/" + quantidadeBalancetes);
				}
				
				try {
					List<Faturamento> faturamentos = faturamentoService.getFaturamentosNaoConsolidadosByProducaoAndEtapa(etapa.getProducao().getId(), 66l);

					linhaAcompanhamento.put("qtdQuantificacao", faturamentos.size());
				}catch(Exception e) {
					
				}

			}
			
			if (etapa.getEtapa().getId() == 68l) {
				
				linhaAcompanhamento.put("submissao", (etapa.isConcluido()) ? "Sim" : "N�o");
				
				try {
					List<Faturamento> faturamentos = faturamentoService.getFaturamentosNaoConsolidadosByProducaoAndEtapa(etapa.getProducao().getId(), 68l);
					linhaAcompanhamento.put("qtdSubmissao", faturamentos.size());
				}catch(Exception e) {
					
				}
			}
			
			try {
				
			} catch (Exception e) {
				
			}
			
			if (contador == etapas.size() - 1) {
				System.out.println("salvou no fim do loop");
				jsonObjects.add(linhaAcompanhamento);
			}

			contador++;
		}
		return jsonObjects;
	}
	
	@GetMapping("/financiamento")
	public ModelAndView financiamento() {
		System.out.println("carregou a get");

		ModelAndView mav = new ModelAndView();

		//Produto Financiamento Reembols�vel
		Produto produto = produtoService.findById(29l);
		List<Equipe> equipes = equipeService.getAll();
		List<Consultor> consultores = consultorService.getConsultores();

		mav.addObject("produto", produto);
		mav.addObject("setorAtividade", SetorAtividade.values());
		mav.addObject("equipes", equipes);
		mav.setViewName("/producao/relatorios/relatorioFinanciamento");

		return mav;

	}
	
	@SuppressWarnings("null")
	@PostMapping("/carregaFinanciamento")
	public @ResponseBody List<JSONObject> carregaFinanciamento(@RequestBody JSONObject parametros) {
		List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
		
		String campanha = null;
		String razaoSocial = null;
		String seguimento = null;
		Long idEquipe = null;
		
		System.out.println(parametros);

		if (Objects.nonNull(parametros.get("campanha").toString()) 
				&& 
				parametros.get("campanha").toString() != "") {
			campanha = parametros.get("campanha").toString();
			System.out.println("campanha: " + campanha);
		}
		
		if (Objects.nonNull(parametros.get("razaoSocial").toString()) && parametros.get("razaoSocial").toString() != "") {
			razaoSocial = parametros.get("razaoSocial").toString();
			System.out.println("razaoSocial: " + razaoSocial);
		}
		
		if (Objects.nonNull(parametros.get("seguimento").toString()) && parametros.get("seguimento").toString() != "") {
			seguimento = parametros.get("seguimento").toString();
			System.out.println("seguimento: " + seguimento);
		}
		
		if (Objects.nonNull(parametros.get("equipe").toString()) && parametros.get("equipe").toString() != "") {
			idEquipe = Long.parseLong(parametros.get("equipe").toString());
			System.out.println("idEquipe: " + idEquipe);
		}
		
		//busca prods by criteria e mando os par�metros.
		List<Producao> producoes = producaoService.getProducoesByFinanciamento(campanha, razaoSocial, seguimento, idEquipe);
		System.out.println("tamanho prods " + producoes.size());
		
		for(Producao producaoAtual : producoes) {
			
			System.out.println(producaoAtual.getId());
			List<ItemTarefa> itensTarefaByProducao = itemTarefaService.getItensByProducao(producaoAtual);
			ItemValores itemValoresGarantia = itemValoresService.getOrganismoByProducaoAndTarefa(698l,255l,producaoAtual.getId());
			ItemValores itemValoresOrganismo = itemValoresService.getOrganismoByProducao(700l,producaoAtual.getId());
			ItemValores itemValoresLinha = itemValoresService.getOrganismoByProducao(701l,producaoAtual.getId());
			ItemValores itemValoresLinhaAprovacao = itemValoresService.getOrganismoByProducao(709l,producaoAtual.getId());
			ItemValores itemValoresValorFinanciado = itemValoresService.getOrganismoByProducaoAndTarefa(705l,254l,producaoAtual.getId());
			ItemValores itemValoresValorContrapartida = itemValoresService.getOrganismoByProducaoAndTarefa(705l,252l,producaoAtual.getId());
			ItemValores itemValoresValorTotalAprovado = itemValoresService.getOrganismoByProducao(699l,producaoAtual.getId());
			System.out.println(itemValoresLinha.getId());
			
			JSONObject linhaRelatorio = new JSONObject();
			linhaRelatorio.put("razaoSocial", producaoAtual.getCliente().getRazaoSocial());
			linhaRelatorio.put("organismo", itemValoresOrganismo.getValor());
			linhaRelatorio.put("linha", itemValoresLinha.getValor());
			linhaRelatorio.put("valorFinanciado", itemValoresValorFinanciado.getValor());
			linhaRelatorio.put("valorContrapartida", itemValoresValorContrapartida.getValor());
			linhaRelatorio.put("valorTotalAprovado", itemValoresValorTotalAprovado.getValor());
			linhaRelatorio.put("linhaAprovacao", itemValoresLinhaAprovacao.getValor());
			linhaRelatorio.put("garantia", itemValoresGarantia.getValor());
			linhaRelatorio.put("equipe", producaoAtual.getEquipe().getNome());
			linhaRelatorio.put("seguimento", producaoAtual.getCliente().getSetorAtividade());
			//localiza��o geogr�fica
			
			
			
			jsonObjects.add(linhaRelatorio);
		}
		System.out.println(jsonObjects);
		return jsonObjects;
	}
}
