package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.presenters.ConsultorPresenter;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FilialServiceImpl;

@Controller
@RequestMapping(path = "/filiais")
public class FilialController {

	@Autowired
	private FilialServiceImpl filialService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@GetMapping("/form")
	public ModelAndView form() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("consultores", this.consultorService.getAll());
		mav.setViewName("/filial/form");

		return mav;
	}


	@PostMapping("/add")
	public ModelAndView create(Filial filial) {
		ModelAndView mav = new ModelAndView("redirect:/filiais/all");

		this.filialService.create(filial);

		return mav;
	}

	@GetMapping("/all")
	public ModelAndView getAll() {
		ModelAndView mav = new ModelAndView("/filial/list");

		mav.addObject("filiais", this.filialService.getAll());

		return mav;
	}

	
	@PostMapping("/update")
	public ModelAndView update(@Valid Filial filial) {

		ModelAndView modelAndView = new ModelAndView("redirect:/filiais/" + filial.getId());

		filialService.update(filial);
		
		return modelAndView;
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView("/filial/update");
		
		List<Consultor> consultorList = consultorService.getAll();
		Filial filial =  filialService.findById(id);
		
		modelAndView.addObject("filial", filial);
		modelAndView.addObject("consultoresNoFilial",
				consultorList.stream().filter(consul -> !filial.getConsultores().contains(consul)).collect(Collectors.toList()));

		return modelAndView;
	}
	

	@GetMapping("/{idFilial}/consultores.json")
	public @ResponseBody List<ConsultorPresenter> getConsultoresByFilial(@PathVariable("idFilial")  Long idFilial) {
		
		List<Consultor> consultores =  this.filialService.getConsultores(idFilial);
		
		ArrayList<ConsultorPresenter> consultoresPresenter = new ArrayList<>();
		
		if (consultores != null && !consultores.isEmpty()) {
			consultoresPresenter.addAll(consultores.stream().map(c -> new ConsultorPresenter(c)).collect(Collectors.toList()));
		}
		
		return consultoresPresenter;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {

		CustomCollectionEditor consultorCollector = new CustomCollectionEditor(List.class) {

			@Override
			protected Object convertElement(Object element) {
				if (element instanceof String) {

					Consultor consultor = new Consultor();
					consultor.setId(Long.parseLong(element.toString()));

					return consultor;
				}

				throw new RuntimeException("Spring diz: N�o sei o que fazer com este elemento: " + element);
			}
		};

		binder.registerCustomEditor(List.class, "consultores", consultorCollector);

	}
	
	@Transactional
	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		filialService.remove(id);

		modelAndView.setViewName("redirect:/filiais/all");

		return modelAndView;

	}
	
}
