package br.com.finiciativas.fseweb.controllers;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.EstadoEtapa;
import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.SubEstadoFaturamento;
import br.com.finiciativas.fseweb.enums.TipoDeApuracao;
import br.com.finiciativas.fseweb.models.Parcela;
import br.com.finiciativas.fseweb.models.ReceptoresEmail;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ComissaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EnderecoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ReceptoresEmailServiceImpl;

@Controller
@RequestMapping(path = "/faturamentos")
public class FaturamentoController {

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private EnderecoClienteServiceImpl enderecoService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private ContatoClienteServiceImpl contatoService;

	@Autowired
	private HonorarioServiceImpl honorarioService;

	@Autowired
	private BalanceteCalculoServiceImpl balanceteCalculoService;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private ClienteServiceImpl clienteService;

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Autowired
	private ReceptoresEmailServiceImpl receptoresService;
	
	@Autowired
	private ProducaoServiceImpl producaoService;
	
	@Autowired
	private ComissaoServiceImpl comissaoService;
	
	public ValorFixo fixo;
	
	@Autowired
	private EtapasConclusaoServiceImpl etapasConclusaoServiceImpl;

	@GetMapping("/all")
	public ModelAndView getAll(@RequestParam(value = "razaoSocial", required = false) String razaoSocial,
			@RequestParam(value = "campanha", required = false) String anoCampanha,
			@RequestParam(value = "idProduto", required = false) Long idProduto,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger,
			@RequestParam(value = "estado", required = false) EstadoEtapa estado,
			@RequestParam(value = "tipoNegocio", required = false) String tpNegocio,
			@RequestParam(value = "etapa", required = false) Long etapaId,
			@RequestParam(value = "anoFiscal", required = false) String anoFiscal) {

		ModelAndView modelAndView = new ModelAndView();

		List<Consultor> consultores = consultorService.getConsultorByCargo(Cargo.ADMINISTRATIVO);
		ReceptoresEmail receptores = receptoresService.findById(1l);

		Set<Consultor> consultoresRestantes = new HashSet<Consultor>(consultores);

		List<Faturamento> faturamentos = new ArrayList<Faturamento>();

		if (queryTrigger != null) {

			if (razaoSocial.equalsIgnoreCase("")) {
				razaoSocial = null;
			}
			if (anoCampanha.equalsIgnoreCase("")) {
				anoCampanha = null;
			}
			if (tpNegocio.equalsIgnoreCase("")) {
				tpNegocio = null;
			}

			if (anoFiscal.equalsIgnoreCase("")) {
				anoFiscal = null;
			}

			if (queryTrigger.equalsIgnoreCase("true")) {
				faturamentos = faturamentoService.getFatNaoLiberadoByCriteria(razaoSocial, anoCampanha, idProduto,
						estado, tpNegocio, etapaId, anoFiscal);
			}

		}

		modelAndView.addObject("faturamentos", faturamentos);
		modelAndView.addObject("estado", EstadoEtapa.values());
		modelAndView.addObject("consultoresRestantes", consultoresRestantes);
		modelAndView.addObject("etapaTrabalho", etapaTrabalhoService.getAll());
		modelAndView.addObject("produtos", produtoService.getAll());

		modelAndView.setViewName("/faturamento/list");

		return modelAndView;
	}

	@PostMapping("/addReceptores")
	@ResponseBody
	public ReceptoresEmail update(@RequestBody List<Long> id) {

		List<Consultor> receptoresConsult = new ArrayList<Consultor>();

		for (Long idConsult : id) {
			System.out.println(idConsult);
			receptoresConsult.add(consultorService.findById(idConsult));
		}

		ReceptoresEmail receptor = receptoresService.findById(1l);
		receptor.setReceptores(receptoresConsult);
		return receptoresService.update(receptor);

	}

	@GetMapping("/envioFaturamento/{id}")
	public ModelAndView liberaFatura(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();

		Faturamento fat = faturamentoService.findById(id);

		if (fat.isManual()) {

			fat.setLiberado(true);

			faturamentoService.update(fat);

		} else {

			fat.setLiberado(true);

			faturamentoService.update(fat);

		}

		faturamentoService.sendEmail(fat);

		modelAndView.setViewName("redirect:/faturamentos/all");

		return modelAndView;
	}

	@GetMapping("/liberaTodos")
	public ModelAndView liberaTodos() {
		ModelAndView modelAndView = new ModelAndView();

		List<Faturamento> fat = faturamentoService.getFatNaoLiberado();
		for (Faturamento faturamento : fat) {
			faturamento.setLiberado(true);
			faturamentoService.update(faturamento);
		}

		faturamentoService.geraTxtFaturamento(fat);

		modelAndView.setViewName("redirect:/faturamentos/all");

		return modelAndView;
	}

	@GetMapping(path = "/gestao")
	public ModelAndView gestao() {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("faturamentos", faturamentoService.getGestao());

		modelAndView.setViewName("faturamento/gestao");

		return modelAndView;

	}

	@GetMapping("/details/{id}")
	public ModelAndView getDetail(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		Faturamento faturamento = faturamentoService.findById(id);
		Cliente cliente = faturamento.getCliente();

		List<ContatoCliente> contatos = new ArrayList<>();
		contatos = contatoService.getAllById(cliente.getId());

		List<ContatoCliente> referencias = new ArrayList<>();
		for (ContatoCliente contatoCliente : contatos) {
			System.out.println(contatoCliente.getNome());
			if (contatoCliente.isPessoaNfBoleto()) {
				referencias.add(contatoCliente);
			}
		}

		List<EnderecoCliente> enderecos = enderecoService.getAllById(faturamento.getCliente().getId());
		EnderecoCliente endereco = new EnderecoCliente();
		for (EnderecoCliente enderecoCliente : enderecos) {
			if (enderecoCliente.isEnderecoNotaFiscalBoleto()) {
				endereco = enderecoCliente;
			}
		}

		EtapasConclusao etapasC = faturamento.getEtapas();

		try {
			if (etapasC.isConcluido()) {
				modelAndView.addObject("conclusao", "Etapa n�o conclu�da");
			} else {
				modelAndView.addObject("conclusao", "Etapa conclu�da");
			}
		} catch (Exception e) {
			modelAndView.addObject("conclusao", "Etapa n�o conclu�da");
		}
		String percentualFaturado = "";
		try {

			if (Objects.nonNull(faturamento.getEtapa())) {
				EtapasPagamento etapas = faturamento.getContrato().getEtapasPagamento();
				if (etapas.getEtapa1() == faturamento.getEtapa()) {
					percentualFaturado = Float.toString(etapas.getPorcentagem1());
				}
				if (etapas.getEtapa2() == faturamento.getEtapa()) {
					percentualFaturado = Float.toString(etapas.getPorcentagem2());
				}
				if (etapas.getEtapa3() == faturamento.getEtapa()) {
					percentualFaturado = Float.toString(etapas.getPorcentagem3());
				}
			} else {
				percentualFaturado = "Sem percentual atribu�do";
			}
		} catch (Exception e) {
			percentualFaturado = "Sem percentual atribu�do";
		}

		Contrato contrato = new Contrato();
		List<Honorario> hon = new ArrayList<Honorario>();
		PercentualEscalonado percentual = new PercentualEscalonado();

		try {

			contrato = faturamento.getContrato();
			hon = contrato.getHonorarios();
			percentual = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());

		} catch (Exception e) {
			System.out.println("Sem contrato no carregamento do Fat.");
		}

		BigDecimal totalFaturar = new BigDecimal(0);

		try {
			if(faturamento.getValorTotal() ==  null) {
				System.out.println("totalFaturar � null");
				List<Faturamento> fats = faturamentoService.getAllByProducao(faturamento.getProducao().getId());

				for (Faturamento faturamento2 : fats) {
					totalFaturar = totalFaturar.add(faturamento2.getValorEtapa());
				}
			}else {
				totalFaturar = faturamento.getValorTotal();
				System.out.println("tem total a faturar");
			}

			

			BalanceteCalculo balancete = balanceteCalculoService
					.getLastBalanceteCalculoByProducao(faturamento.getProducao().getId());

			modelAndView.addObject("balancete", balancete);

			String totalFaturar1 = "";
			totalFaturar1 = totalFaturar1.replaceAll(",", ".");

		} catch (Exception e) {
			System.out.println("Sem total (Manual)");
			totalFaturar = faturamento.getValorTotal();
		}

		if (Objects.nonNull(faturamento.getProducao())) {

			List<BalanceteCalculo> balancetes = balanceteCalculoService
					.getBalanceteCalculoByProducao(faturamento.getProducao().getId());

			Float beneficioTotal = 0.0f;
			
			BalanceteCalculo balanceteLast = new BalanceteCalculo();
			System.out.println("balancetes.size()"+ balancetes.size());
			if (balancetes.size() > 0) {
				balanceteLast.setVersao(0);
				for (BalanceteCalculo balanceteCalculo : balancetes) {
					if (balanceteCalculo.getVersao() > balanceteLast.getVersao()) {
						balanceteLast = balanceteCalculo;
					}
					if (!balanceteCalculo.getReducaoImposto().isEmpty()) {
						beneficioTotal += Float
								.parseFloat(balanceteCalculo.getReducaoImposto().replace(".", "").replace(",", "."));
					}
				}
			} else {
				System.out.println("Sem Balancete, deixando Last = NULL");
				balanceteLast = null;
			}

			try {
				if (faturamento.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
					modelAndView.addObject("beneficio", balanceteLast.getReducaoImposto());
				} else {
					modelAndView.addObject("beneficio", beneficioTotal);
				}
			}catch(Exception e){
				System.out.println(e);
			}
			
		}

		modelAndView.addObject("total", totalFaturar);
		modelAndView.addObject("faturamento", faturamento);
		modelAndView.addObject("percentual", percentualFaturado);
		modelAndView.addObject("contatos", referencias);
		modelAndView.addObject("endereco", endereco);
		modelAndView.addObject("honors", hon);
		modelAndView.addObject("estado", EstadoEtapa.values());
		modelAndView.addObject("percentual", percentual);
		modelAndView.addObject("contrato", contrato);
		modelAndView.setViewName("/faturamento/details");

		return modelAndView;
	}

	//Este m�todo tem blocos comentados caso haja a necessidade de voltar �s configura��es padr�o, favor mant�-los.
	@PostMapping("/update")
	public ModelAndView update(@RequestParam(value = "id") Long id, @RequestParam(value = "estado") String estado,
			@RequestParam(value = "cnpjFaturar") String cnpjFaturar,
			@RequestParam(value = "razaoSocialFaturar") String razaoSocialFaturar,
			@RequestParam(value = "nf", required = false) String nf,
			@RequestParam(value = "valorEtapa", required = false) String valorE,
			@RequestParam(value = "valorTotal", required = false) String valorT,
			@RequestParam(value = "reducaoImposto", required = false) String reducaoImposto,
			@RequestParam(value = "seguimento", required = false) SetorAtividade seguimento,
			@RequestParam(value = "obsContrato", required = false) String obscontrato,
			@RequestParam(value = "obsFaturamento", required = false) String obsFaturamento,
			@RequestParam(value = "etapa", required = false) Long etapa,
			@RequestParam(value = "anoFiscal", required = false) String anoFiscal,
			@RequestParam(value = "campanha", required = false) String campanha,
			@RequestParam(value = "subEstadoFaturamento", required = false) String subEstadoFaturamento) {

		ModelAndView mav = new ModelAndView();

		mav.addObject("seguimento", SetorAtividade.values());
		// BigDecimal valor = BigDecimal.valueOf(Double.valueOf(valorFinal));
		// BigDecimal currency = valor.setScale(8, BigDecimal.ROUND_HALF_UP);

		/* BigDecimal valor = new BigDecimal(valorFaturar.replaceAll(",", ".")); */

		valorT = valorT.replaceAll("\\.", "");
		valorT = valorT.replace(",", ".");

		valorE = valorE.replaceAll("\\.", "");
		valorE = valorE.replace(",", ".");

		BigDecimal valorTotal = new BigDecimal(0);
		BigDecimal valorEtapa = new BigDecimal(valorE);

		System.out.println("a etapa de trabalho � " + etapa);
		System.out.println("este � o valor da etapa" + valorT);
		System.out.println("este � o valor total" + valorE);
		System.out.println(id);
		System.out.println("TESTE TIPO DE DADO: " + obscontrato);
		System.out.println("TESTE TIPO DE DADO: " + obsFaturamento);

		Faturamento faturamentoUpp = faturamentoService.findById(id);

		Contrato contratoUpp = faturamentoUpp.getContrato();
		Cliente cliente = faturamentoUpp.getCliente();
		BalanceteCalculo balancete = faturamentoUpp.getBalancete();

		faturamentoUpp.setObsContrato(obscontrato);
		faturamentoUpp.setObservacao(obsFaturamento);
		/*
		 * balancete.setReducaoImposto(reducaoImposto);
		 */ cliente.setSetorAtividade(seguimento);
		faturamentoUpp.setNumeroNF(nf);
		
		//verifica valorTotal antes de alterar valorEtapa
		Producao producao = faturamentoUpp.getProducao();
		Long idProducao = producao.getId();
		List<Faturamento> faturamento = faturamentoService.getAllByProducao(idProducao);
		BigDecimal totalFaturarAntes = new BigDecimal(0);
		//acessa os fats da producao e calcula o totalFaturar(soma valorEtapa)
		for (Faturamento fats : faturamento ){
			totalFaturarAntes = totalFaturarAntes.add(fats.getValorEtapa());
		}
		
		faturamentoUpp.setValorEtapa(valorEtapa);
		System.out.println("entrou na condi��o de valor da etapa alterado");
		/* List<Faturamento> AllFaturamentosByCliente = new ArrayList<>(); */
		List<Faturamento> AllFaturamentosByCliente = new ArrayList<Faturamento>();

		AllFaturamentosByCliente = faturamentoService.getFaturamentoByProdAndCampaign(faturamentoUpp.getId(),
				faturamentoUpp.getCampanha());

		BigDecimal valortTotal;
		/*
		 * for(int i = 0; i < AllFaturamentosByCliente.size(); i++) {
		 * System.out.println(AllFaturamentosByCliente.get(i).getValorTotal());
		 * 
		 * valorTotal.add(new
		 * BigDecimal(AllFaturamentosByCliente.get(i).getValorTotal())); += New
		 * BigDecimal AllFaturamentosByCliente.get(i).getValorTotal(); valorTotal =
		 * AllFaturamentosByCliente.getValorTotal().stream().filter(Objects::nonNull).
		 * reduce(BigDecimal.ZERO, BigDecimal::add); }
		 */
		
		BigDecimal totalFaturar = new BigDecimal(0);
		//acessa os fats da producao e calcula o totalFaturar atualizado(soma valorEtapa)
		for (Faturamento fats : faturamento ){
			totalFaturar = totalFaturar.add(fats.getValorEtapa());
		}
		
		//acessa cada faturamento da prod e seta o total a faturar atualizado
		for (Faturamento fats : faturamento ){
			fats.setValorTotal(totalFaturar);
		} 
		
		//se valorTotal foi alterado manualmente,todos fats da prod s�o atualizados com novo totalFaturar
		BigDecimal valorTotalUpdate = new BigDecimal(valorT);
		if(valorTotalUpdate.compareTo(totalFaturarAntes) == 1) {
			System.out.println("valorTotal alterado");
			for (Faturamento fats : faturamento ){
				fats.setValorTotal(valorTotalUpdate);
				fats.setTotalFaturarAlterado(true);
				faturamentoService.update(fats);
			}
		}
		
		/*
		 * for (Faturamento faturamento : AllFaturamentosByCliente) {
		 * valorTotal.add(faturamento.getValorEtapa());
		 * 
		 * }
		 */
		
		//faturamentoUpp.setValorTotal(totalFaturar);
		//faturamentoUpp.setValorTotal(valorTotal);
		faturamentoUpp.setAnoFiscal(anoFiscal);
		faturamentoUpp.setCampanha(campanha);
		faturamentoUpp.setEstado(estado);
		faturamentoUpp.setCnpjFaturar(cnpjFaturar);
		faturamentoUpp.setRazaoSocialFaturar(razaoSocialFaturar);
		faturamentoUpp.setEtapa(etapaTrabalhoService.findById(etapa));
		faturamentoUpp.setRazaoSocialFaturar(razaoSocialFaturar);
		faturamentoUpp.setSubEstadoFaturamento(subEstadoFaturamento);
		contratoUpp.setCnpjFaturar(cnpjFaturar);
		contratoUpp.setCnpjFaturar(cnpjFaturar);
		faturamentoService.update(faturamentoUpp);

		mav.setViewName("redirect:/faturamentos/details/" + faturamentoUpp.getId());

		return mav;

	}

	/*
	 * @GetMapping("/details/{id}") public ModelAndView
	 * getDetail(@PathVariable("id") Long id) {
	 */

	@GetMapping("/faturamentoUpdate/{id}")
	public ModelAndView updateFaturamento(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		Faturamento faturamentoUpp = faturamentoService.findById(id);

		Cliente cliente = faturamentoUpp.getCliente();
		System.out.println("etapa antes do update" + faturamentoUpp.getEtapa());

		faturamentoService.update(faturamentoUpp);

		System.out.println("etapa depois do update" + faturamentoUpp.getEtapa());

		System.out.println(id);

		BigDecimal totalFaturar = new BigDecimal(0);

		try {
			List<Faturamento> fats = faturamentoService.getAllByProducao(faturamentoUpp.getProducao().getId());

			if(faturamentoUpp.getValorTotal() ==  null) {
				for (Faturamento faturamento2 : fats) {
				totalFaturar = totalFaturar.add(faturamento2.getValorEtapa());
				System.out.println("totalFaturar get"+totalFaturar);
				}
			}else {
				totalFaturar = faturamentoUpp.getValorTotal();
				System.out.println("tem total a faturar");
			}

			BalanceteCalculo balancete = balanceteCalculoService
					.getLastBalanceteCalculoByProducao(faturamentoUpp.getProducao().getId());

			mav.addObject("balancete", balancete);

			String totalFaturar1 = "";
			totalFaturar1 = totalFaturar1.replaceAll(",", ".");

		} catch (Exception e) {
			System.out.println("Sem total (Manual)");
			totalFaturar = faturamentoUpp.getValorTotal();
		}
		
		List<ContatoCliente> contatos = new ArrayList<>();
		contatos = contatoService.getAllById(cliente.getId());

		List<ContatoCliente> referencias = new ArrayList<>();
		for (ContatoCliente contatoCliente : contatos) {
			System.out.println(contatoCliente.getNome());
			if (contatoCliente.isPessoaNfBoleto()) {
				referencias.add(contatoCliente);
			}
		}

		mav.addObject("contatos", referencias);
		mav.addObject("total", totalFaturar);
		mav.addObject("faturamento", faturamentoUpp);
		mav.addObject("estado", EstadoEtapa.values());
		mav.addObject("seguimento", SetorAtividade.values());
		mav.addObject("subEstadoFaturamento", SubEstadoFaturamento.values());
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("etapaTrabalho", faturamentoUpp.getProduto().getEtapasTrabalho());

		mav.setViewName("faturamento/faturamentoUpdate");
		return mav;
	}

	@GetMapping("/form")
	public ModelAndView formFaturamento() {

		ModelAndView mav = new ModelAndView();

		String str = " 12,12";
		str = str.replaceAll("(\\d+)\\,(\\d+)", "$1.$2");
		System.out.println("str:" + str);

		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("estado", EstadoEtapa.values());
		mav.addObject("clientes", clienteService.getAll());
		mav.addObject("etapaTrabalho", etapaTrabalhoService.getAll());
		
		mav.setViewName("/faturamento/form");

		return mav;

	}

	@Transactional
	@PostMapping(path = "/add")
	public ModelAndView create(Faturamento faturamento, @RequestParam("valorT") String valorT,
			@RequestParam("valorE") String valorE, @RequestParam(value="idProducao", required=false) String idProducao,
			@RequestParam(value="idBalancete", required=false) String idBalancete) {
		
//		System.out.println(faturamento.getEtapa().getId() + " welcome ETAPA id");
		System.out.println(faturamento.getEtapaTrabalho() + " welcome ETAPATRABALHO");
		System.out.println(faturamento.getEtapas() + " welcome ETAPAS CONCL");

		ModelAndView modelAndView = new ModelAndView();

		valorT = valorT.replaceAll("\\.", "");
		valorT = valorT.replace(",", ".");

		valorE = valorE.replaceAll("\\.", "");
		valorE = valorE.replace(",", ".");

		BigDecimal valorEtapa = new BigDecimal(valorE);
		BigDecimal valorTotal = new BigDecimal(valorT);
		
		System.out.println(faturamento.getEtapaTrabalho() + " @@@@@ etapa trabalho texto");
		System.out.println(faturamento.getEtapa() + "@@@@@ etapa trabalho objeto");

		faturamento.setValorEtapa(valorEtapa);
		faturamento.setValorTotal(valorTotal);
		faturamento.setEtapa(faturamento.getEtapa());
		faturamento.setEtapaTrabalho(faturamento.getEtapa().getNome());
		
		Date hoje = java.util.Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dataString = dateFormat.format(hoje);
		
		
		if(idProducao!=null) {
		Producao producao = producaoService.getProducaoById(Long.parseLong(idProducao));
		faturamento.setProducao(producao);
		faturamento.setContrato(producao.getContrato());	
		faturamento.setRazaoSocialFaturar(producao.getContrato().getRazaoSocialFaturar());
		faturamento.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
		faturamento.setCampanha(producao.getAno());
		faturamento.setDataCriacao(dataString);
		faturamento.setObsContrato(producao.getContrato().getObsContrato());
		}
		
		if(idBalancete!=null) {
		BalanceteCalculo balancete = balanceteCalculoService.findById(Long.parseLong(idBalancete));
		faturamento.setBalancete(balancete);
		}
		//Producao producao = producaoService.getProducaoById(Long.parseLong(idProducao));
		
		faturamento.setManual(true);

		faturamentoService.create(faturamento);

		modelAndView.setViewName("redirect:/faturamentos/details/" + faturamento.getId());

		return modelAndView;

	}

	@GetMapping("/envio/{id}")
	public ModelAndView envioCobranca(@PathVariable Long id) {

		ModelAndView mav = new ModelAndView();

		Faturamento fat = faturamentoService.findById(id);

		faturamentoService.envioDeCobranca(fat);
		mav.setViewName("redirect:/faturamentos/all");

		return mav;

	}

	@GetMapping("/gerarValorFixo")
	public ModelAndView gerarValorFixo() {

		ModelAndView mav = new ModelAndView();

		mav.setViewName("/faturamento/formValorFixo");
		return mav;

	}

	@PostMapping("/generateFixed/{ano}/{dataInserida}")
	@ResponseBody
	public void gerador(@RequestBody Long id, @PathVariable("ano") String ano, @PathVariable("dataInserida") String dataInserida) {

		LocalDate localDate = LocalDate.now();// For reference
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String criacao = localDate.format(formatter);

		Contrato contrato = contratoService.findById(id);
		List<Honorario> hons = contrato.getHonorarios();

		for (Honorario honorario : hons) {
			if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {

				fixo = honorarioService.getValorFixoByContrato(contrato.getId());
				System.out.println("-----------------" + fixo);

				if (fixo.isValorAnual()) {
					dataInserida = new StringBuilder(dataInserida).insert(dataInserida.length()-4, "/").toString();
					dataInserida = new StringBuilder(dataInserida).insert(dataInserida.length()-7, "/").toString();				
					fixo.setDataCobrancaAnual(dataInserida);

					Faturamento fat = new Faturamento();
					fat.setCliente(contrato.getCliente());
					fat.setCampanha(ano);
					fat.setCnpjFaturar(contrato.getCnpjFaturar());
					fat.setContrato(contrato);
					fat.setEstado(EstadoEtapa.AGUARDANDO_DATA.toString());
					fat.setLiberado(false);
					fat.setEmitido(false);
					fat.setManual(false);
					fat.setModo("Valor Fixo");
					fat.setObsContrato(contrato.getObsContrato());
					fat.setProduto(contrato.getProduto());
					fat.setRazaoSocialFaturar(contrato.getRazaoSocialFaturar());
					fat.setValorF(fixo);
					fat.setValorEtapa(fixo.getValor());
					fat.setDataCriacao(criacao);
					fat.setDataParaFaturar(dataInserida);
					faturamentoService.create(fat);
				}
				if (fixo.isValorMensal()) {
					
					for (int i = 0; i < 12; i++) {

						String mes = "";

						switch (i) {
						case 0:
							mes = "01";
							break;
						case 1:
							mes = "02";
							break;
						case 2:
							mes = "03";
							break;
						case 3:
							mes = "04";
							break;
						case 4:
							mes = "05";
							break;
						case 5:
							mes = "06";
							break;
						case 6:
							mes = "07";
							break;
						case 7:
							mes = "08";
							break;
						case 8:
							mes = "09";
							break;
						case 9:
							mes = "10";
							break;
						case 10:
							mes = "11";
							break;
						case 11:
							mes = "12";
							break;
						}
						
						System.out.println(dataInserida + " only shadows are meant to fade i'll hold on to you 1");
						dataInserida = dataInserida.replaceAll("[a-zA-Z]", "");
						System.out.println(dataInserida + " only shadows are meant to fade i'll hold on to you 2");
						fixo.setDataCobrancaMensal(Integer.parseInt(dataInserida));

						Faturamento fat = new Faturamento();
						fat.setCliente(contrato.getCliente());
						fat.setCnpjFaturar(contrato.getCnpjFaturar());
						fat.setCampanha(ano);
						fat.setContrato(contrato);
						fat.setEstado(EstadoEtapa.AGUARDANDO_DATA.toString());
						fat.setLiberado(false);
						fat.setEmitido(false);
						fat.setData(i + mes);
						fat.setManual(false);
						fat.setModo("Valor Fixo");
						fat.setObsContrato(contrato.getObsContrato());
						fat.setProduto(contrato.getProduto());
						fat.setRazaoSocialFaturar(contrato.getRazaoSocialFaturar());
						fat.setValorF(fixo);
						fat.setValorEtapa(fixo.getValor());
						fat.setDataCriacao(criacao);
						fat.setDataParaFaturar(dataInserida + "/" + mes);
						faturamentoService.create(fat);
					}
				}
				if (fixo.isValorTrimestral()) {

					for (int i = 0; i < 4; i++) {

						String dataCobranca = "";

						switch (i) {
						case 0:
							dataCobranca = fixo.getDataCobrancaTrimestral1();
							break;
						case 1:
							dataCobranca = fixo.getDataCobrancaTrimestral2();
							break;
						case 2:
							dataCobranca = fixo.getDataCobrancaTrimestral3();
							break;
						case 3:
							dataCobranca = fixo.getDataCobrancaTrimestral4();
							break;
						}
						System.out.println(dataInserida + " data inserida antes");
						
						String dataTrimestral1 = new StringBuilder(dataInserida).insert(2, "/").substring(0, 5).toString();
						String dataTrimestral2 = new StringBuilder(dataInserida).insert(6, "/").substring(4, 9).toString();
						String dataTrimestral3 = new StringBuilder(dataInserida).insert(10, "/").substring(8, 13).toString();
						String dataTrimestral4 = new StringBuilder(dataInserida).insert(14, "/").substring(12, 17).toString();
						
						System.out.println(dataTrimestral1 + "dataTrimestral1");
						System.out.println(dataTrimestral2 + "dataTrimestral2");
						System.out.println(dataTrimestral3 + "dataTrimestral3");
						System.out.println(dataTrimestral4 + "dataTrimestral4"); 
						
						fixo.setDataCobrancaTrimestral1(dataTrimestral1);
						fixo.setDataCobrancaTrimestral2(dataTrimestral2);
						fixo.setDataCobrancaTrimestral3(dataTrimestral3);
						fixo.setDataCobrancaTrimestral4(dataTrimestral4);

						Faturamento fat = new Faturamento();
						fat.setCliente(contrato.getCliente());
						fat.setCnpjFaturar(contrato.getCnpjFaturar());
						fat.setCampanha(ano);
						fat.setContrato(contrato);
						fat.setEstado(EstadoEtapa.AGUARDANDO_DATA.toString());
						fat.setLiberado(false);
						fat.setEmitido(false);
						fat.setData(dataCobranca);
						fat.setManual(false);
						fat.setModo("Valor Fixo");
						fat.setObsContrato(contrato.getObsContrato());
						fat.setProduto(contrato.getProduto());
						fat.setRazaoSocialFaturar(contrato.getRazaoSocialFaturar());
						fat.setValorF(fixo);
						fat.setValorEtapa(fixo.getValor());
						fat.setDataCriacao(criacao);
						fat.setDataParaFaturar(dataCobranca);
						faturamentoService.create(fat);
					}
				}
			}
		}
	}

	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id, @RequestParam("currentParam") String parameters) {
		System.out.println("parameters"+parameters);
		
		ModelAndView modelAndView = new ModelAndView();
		
		//acessa o faturamento e o balancete do fat
		Faturamento fat = faturamentoService.findById(id);
		Long idBalancete = fat.getBalancete().getId();
		BalanceteCalculo balancete = balanceteCalculoService.findById(idBalancete);
		Long idProducao = fat.getProducao().getId();
		
		faturamentoService.remove(id);
		
		//lista de faturamentos do balancete, 
		List<Faturamento> faturamentos = faturamentoService.getFaturamentoByBalancete(idBalancete);
		//se o fat = null e GerouFat = true altera GerouFat = false
		if(faturamentos.size() == 0 && balancete.isGerouFaturamento() == true){
			balancete.setGerouFaturamento(false);
			balanceteCalculoService.update(balancete);
		}else {
			System.out.println("o balancete possui faturamento");
		}
		
		//lista dos fats da produ��o
		List<Faturamento> faturamento = faturamentoService.getAllByProducao(idProducao);
		
		//calculando totalFaturar atualizado(soma valorEtapa dos fats da prod)
		BigDecimal totalFaturar = new BigDecimal(0);
		for (Faturamento fats : faturamento ){
			totalFaturar = totalFaturar.add(fats.getValorEtapa());
		}
		
		//acessando todos fats da prod e atualizando o totalFaturar
		for (Faturamento fats : faturamento ){
			fats.setValorTotal(totalFaturar);
			faturamentoService.update(fats);
		}
		
		modelAndView.setViewName("redirect:/faturamentos/all" + parameters);
		return modelAndView;

	}

	@GetMapping("/{id}/fat.json")
	public @ResponseBody Faturamento getFatById(@PathVariable("id") Long id) {

		return faturamentoService.findById(id);

	}

	@GetMapping("/{idCliente}/{campanha}/fats.json")
	public @ResponseBody List<Faturamento> getFatsByIdAndCampanha(@PathVariable("idCliente") Long idCliente,
			@PathVariable("campanha") String ano) {

		return faturamentoService.getAllFaturamentoByClienteAndCampanha(idCliente, ano);

	}

	@PostMapping("/libera")
	@ResponseBody
	public void liberaFat(@RequestBody Faturamento f) {
		
		System.out.println("esse � o m�todo");
		
		//Date dateLiberacao = 
		
		//String dataLiberacao = java.util.Calendar.getInstance().getTime().toString();

		//	String diaEmissao = dataLiberacao.substring(0, 2);
		//	String mesEmissao = dataLiberacao.substring(3, 5);
		//	String anoEmissao = dataLiberacao.substring(6);
		//	dataLiberacao = anoEmissao + "-" + mesEmissao + "-" + diaEmissao;
		//DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		//String dataString = dateFormat.format(dataLiberacao);
		
		Date date = new Date();
	      SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd");
	       String dataLiberacao = formatter.format(date);
		
		
		Faturamento fatAtt = faturamentoService.findById(f.getId());
		fatAtt.setLiberado(true);
		fatAtt.setEmitido(false);
		fatAtt.setDataLiberacao(dataLiberacao);
		fatAtt.setEstado(EstadoEtapa.EMISSAO.displayName());

		Faturamento fat = faturamentoService.update(fatAtt);

		// faturamentoService.envioFaturamentoLiberado(fat);

	}

	@PostMapping("/retorna")
	@ResponseBody
	public void retornaFat(@RequestBody Faturamento f) {

		Faturamento fatAtt = faturamentoService.findById(f.getId());
		fatAtt.setLiberado(false);
		fatAtt.setEstado(EstadoEtapa.ETAPA_FINALIZADA.displayName());

		faturamentoService.update(fatAtt);

	}

	@PostMapping("/updateEmissao2")
	@ResponseBody
	public void atualizarEmissaoFaturament2(@RequestBody Faturamento faturamento) {
		Faturamento fatAtt = faturamentoService.findById(faturamento.getId());
		fatAtt.setEmitido(true);

		faturamentoService.update(fatAtt);
	}

	@PostMapping("/updateEmissaoFat")
	@ResponseBody
	public void atualizarEmissaoFa(@RequestBody Faturamento faturamento) {
		System.out.println("entrou na /updateEmissaoFat e o faturamento �: " + faturamento.getId());
		Faturamento fatAtt = faturamentoService.findById(faturamento.getId());
		fatAtt.setEmitido(true);

		faturamentoService.update(fatAtt);
	}

	@GetMapping("/details")
	@ResponseBody
	public void getPercentualEscalonado() {

	}

	@GetMapping("/{id}/pe.json")
	public @ResponseBody PercentualEscalonado getPeByContratoId(@PathVariable("id") Long id) {

		PercentualEscalonado pe = honorarioService.getPercentualEscalonadoByContrato(id);

		return pe;
	}

	@GetMapping("/{id}/fatsCliente.json")
	public @ResponseBody List<Faturamento> getFaturamentosByCliente(@PathVariable("id") Long id) {
		return faturamentoService.getFaturamentoByCliente(id);
	}

	@GetMapping("/{id}/fatsProd.json")
	public @ResponseBody List<Faturamento> getFaturamentosByProducao(@PathVariable("id") Long id) {

		List<Faturamento> fats = faturamentoService.getAllByProducaoNotConsolidado(id);

		return fats;
	}

	@PostMapping("/simulaFatConsolidado")
	@ResponseBody
	public JSONObject returnSimulacao(@RequestBody List<JSONObject> param) {

		List<Long> fatIds = new ArrayList<Long>();
		JSONObject simulacao = new JSONObject();
		Float percent = 0.0f;

		System.out.println("tamanho"+param.size());
		for (int i = 0; i < param.size(); i++) {
			String idString = param.get(i).toString();
			Long id = Long.parseLong(idString.replaceAll("[^0-9]", ""));
			fatIds.add(id);
		}

		// FAZER M�TOROD CONSOLIDA FATURAMENTOS ok
		// SE SOMENTE MESMO BALANCETE, MENSAGEM N PODE ok
		// REFAZER C�LCULO DE HONOR�RIOS ok
		// ENVIAR OBJETO JSON CORRETAMENTE
		// ERRO DE CONSOLIDA��O DE ETAPAS DIFERENTES
		// MOSTRAR VISUALMENTE (FATURAMENTOS COMPOSTOS, BENEF�CIO ATUALIZADO, NOVA
		// MEM�RIA DE C�LCULO).

		List<Faturamento> fats = new ArrayList<Faturamento>();
		Producao producao = new Producao();

		for (Long id : fatIds) {
			Faturamento fat = faturamentoService.findById(id);
			fats.add(fat);
		}

		simulacao.put("fatsIds", fats);

		List<Long> balanceteIds = new ArrayList<Long>();
		List<String> etapasFaturamento = new ArrayList<String>();
		boolean balError = false;

		boolean isMesmoBalacente = false;
		for (int i = 0; i < fats.size(); i++) {
			try {
				balanceteIds.add(fats.get(i).getBalancete().getId());
				etapasFaturamento.add(String.valueOf(fats.get(i).getEtapa().getId()));
			} catch (Exception e) {
				e.printStackTrace();
				balError = true;
				System.out.println("NAO TEM BALANCETE");
				simulacao.put("erro", "noBalancete");
				return simulacao;
			}
		}

		if (balError == false) {
			Set<Long> balanceteIdSet = new HashSet<Long>(balanceteIds);

			if (balanceteIdSet.size() < balanceteIds.size()) {
				simulacao.put("erro", "mesmoBalancete");
				return simulacao;
			} else {
				BalanceteCalculo balanceteConsolidado = new BalanceteCalculo();

				String stringValorBalancete;
				BigDecimal beneficioConsolidado = new BigDecimal(0);

				for (Long balId : balanceteIdSet) {

					BalanceteCalculo balanceteCalc = balanceteCalculoService.findById(balId);

					stringValorBalancete = balanceteCalc.getReducaoImposto();
					stringValorBalancete = stringValorBalancete.replace(".", "");
					stringValorBalancete = stringValorBalancete.replace(",", ".");
					stringValorBalancete = stringValorBalancete.trim();

					BigDecimal balancete = new BigDecimal(stringValorBalancete);
					beneficioConsolidado = beneficioConsolidado.add(balancete);

					producao = balanceteCalc.getProducao();

				}

				if (verifyAllEqualUsingALoop(etapasFaturamento) == false) {
					simulacao.put("erro", "mesmaEtapa");
				} else {

					Long etapaFaturar = Long.parseLong(etapasFaturamento.get(0));
					EtapasPagamento timings = producao.getContrato().getEtapasPagamento();

					if (Objects.nonNull(timings.getEtapa1())) {
						if (timings.getEtapa1().getId() == etapaFaturar) {
							percent = timings.getPorcentagem1();
						}
					}
					if (Objects.nonNull(timings.getEtapa2())) {
						if (timings.getEtapa2().getId() == etapaFaturar) {
							percent = timings.getPorcentagem2();
						}
					}
					if (Objects.nonNull(timings.getEtapa3())) {
						if (timings.getEtapa3().getId() == etapaFaturar) {
							percent = timings.getPorcentagem3();
						}
					}
					if (Objects.nonNull(timings.getEtapa4())) {
						if (timings.getEtapa4().getId() == etapaFaturar) {
							percent = timings.getPorcentagem4();
						}
					}

				simulacao.put("beneficioConsolidado", beneficioConsolidado);
				simulacao.put("erro", "noError");

				Faturamento faturamentoCriado = faturamentoService.geraFaturamentoConsolidado(producao,
						beneficioConsolidado, percent, etapaFaturar);

				String idsConcat = "";
				for (Long id : fatIds) {
					idsConcat += id.toString() + ";";
				}
				
				String valorFormata = faturamentoCriado.getValorEtapa().toString();
				if(!valorFormata.contains(".")){
					valorFormata = valorFormata+".00";
				}
				
				simulacao.put("percent", percent);
				
				faturamentoCriado.setValorEtapa(new BigDecimal(valorFormata).setScale(2, BigDecimal.ROUND_UP));

				idsConcat = idsConcat.substring(0, idsConcat.length() - 1);

				faturamentoCriado.setCliente(producao.getCliente());
				faturamentoCriado.setIdsFaturamentosConsolidados(idsConcat);

				simulacao.put("faturamentoConsolidado", faturamentoCriado);
			}

		}
	}

	return simulacao;

	}

	public boolean verifyAllEqualUsingALoop(List<String> list) {
		for (String s : list) {
			if (!s.equals(list.get(0)))
				return false;
		}
		return true;
	}

	@PostMapping("/salvaBeneficioConsolidado")
	@ResponseBody
	public void salvaBenConsolidado(@RequestBody Faturamento faturamento) {

		Producao producao = producaoService.findById(faturamento.getProducao().getId());
		Contrato contrato = contratoService.findById(producao.getContrato().getId());
		
		faturamento.setCnpjFaturar(contrato.getCnpjFaturar());
		faturamento.setRazaoSocialFaturar(contrato.getRazaoSocialFaturar());
		faturamento.setContrato(contrato);
		
		faturamento.setEtapaTrabalho(etapaTrabalhoService.findById(faturamento.getEtapa().getId()).getNome());
		
		faturamento.setObsContrato(contrato.getObsContrato());
		
		String[] tokens = faturamento.getIdsFaturamentosConsolidados().split(";", -1);
		
		for (String string : tokens) {
			Long id = Long.parseLong(string);
			Faturamento fatConsolidado = faturamentoService.findById(id);
			fatConsolidado.setEstado(EstadoEtapa.CONSOLIDADO.displayName());
			
			faturamentoService.update(fatConsolidado);
		}
		
		faturamentoService.update(faturamento);
		
	}
	
	@GetMapping("/formConsolidado")
	public ModelAndView formConsolidado() {

		ModelAndView mav = new ModelAndView();

		String str = " 12,12";
		str = str.replaceAll("(\\d+)\\,(\\d+)", "$1.$2");

		mav.addObject("preLoad", "n");
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("estado", EstadoEtapa.values());
		mav.addObject("clientes", clienteService.getAll());
		mav.addObject("etapaTrabalho", etapaTrabalhoService.getAll());

		mav.setViewName("/faturamento/formConsolidado");

		return mav;

	}
	
	@PostMapping("/formConsolidadoPreLoaded")
	public ModelAndView formConsolidadoPreLoaded(@RequestParam("idProdLoad") Long id) {

		ModelAndView mav = new ModelAndView();

		String str = " 12,12";
		str = str.replaceAll("(\\d+)\\,(\\d+)", "$1.$2");

		mav.addObject("preLoad", "s");
		mav.addObject("idProdLoaded", id);
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("estado", EstadoEtapa.values());
		mav.addObject("clientes", clienteService.getAll());
		mav.addObject("etapaTrabalho", etapaTrabalhoService.getAll());

		mav.setViewName("/faturamento/formConsolidado");

		return mav;

	}
	

	@PostMapping("/setEnviado/{idBotao}")
	@ResponseBody
	public void setEnviadoAoCliente(@RequestBody Long idBotao) {
		
		Faturamento faturamento = faturamentoService.findById(idBotao);
		faturamento.setEnviadoAoCliente(true);
		faturamentoService.update(faturamento);
	}
	
	
	@GetMapping("/detailsFaturamentoConsolidado/{id}")
	public ModelAndView getDetailsFaturamentoConsolidado(@PathVariable("id") Long id) {
		System.out.println("entrou no detailsConsolido");
		
		ModelAndView modelAndView = new ModelAndView();
		Faturamento faturamento = faturamentoService.findById(id);
		Cliente cliente = faturamento.getCliente();
		
		List<ContatoCliente> contatos = new ArrayList<>();
		contatos = contatoService.getAllById(cliente.getId());
		
		List<ContatoCliente> informacoesContato = new ArrayList<>();
		for (ContatoCliente contatoCliente : contatos) {
			System.out.println("nome do cliente"+contatoCliente.getNome());
			if (contatoCliente.isPessoaNfBoleto()) {
				informacoesContato.add(contatoCliente);
			}
			System.out.println("informacoesContato"+informacoesContato);
		}
		
	
		
		modelAndView.addObject("faturamento", faturamento);
		modelAndView.addObject("contato", informacoesContato);
		
		modelAndView.setViewName("/faturamento/detailsConsolidado");
	
		return modelAndView;
	}
	
	@PostMapping("/updateConsolidado")
	public ModelAndView updateFaturamentoConsolidado(@RequestParam(value = "id") Long id, @RequestParam(value = "estado") String estado,
			@RequestParam(value = "cnpjFaturar") String cnpjFaturar,
			@RequestParam(value = "razaoSocialFaturar") String razaoSocialFaturar,
			@RequestParam(value = "nf", required = false) String nf,
			@RequestParam(value = "valorEtapa", required = false) String valorE,
			@RequestParam(value = "valorTotal", required = false) String valorT,
			@RequestParam(value = "reducaoImposto", required = false) String reducaoImposto,
			@RequestParam(value = "seguimento", required = false) SetorAtividade seguimento,
			@RequestParam(value = "obsContrato", required = false) String obscontrato,
			@RequestParam(value = "obsFaturamento", required = false) String obsFaturamento,
			@RequestParam(value = "etapa", required = false) Long etapa,
			@RequestParam(value = "anoFiscal", required = false) String anoFiscal,
			@RequestParam(value = "campanha", required = false) String campanha) {

		ModelAndView mav = new ModelAndView();

		mav.addObject("seguimento", SetorAtividade.values());

		valorT = valorT.replaceAll("\\.", "");
		valorT = valorT.replace(",", ".");

		valorE = valorE.replaceAll("\\.", "");
		valorE = valorE.replace(",", ".");

		BigDecimal valorTotal = new BigDecimal(valorT);
		BigDecimal valorEtapa = new BigDecimal(valorE);

		System.out.println("updateConsolidado a etapa de trabalho � " + etapa);
		System.out.println("updateConsolidado este � o valor da etapa" + valorT);
		System.out.println("updateConsolidado este � o valor total" + valorE);
		System.out.println(id);
		System.out.println("TESTE TIPO DE DADO: " + obscontrato);
		System.out.println("TESTE TIPO DE DADO: " + obsFaturamento);

		Faturamento faturamentoUpp = faturamentoService.findById(id);

		Contrato contratoUpp = faturamentoUpp.getContrato();
		Cliente cliente = faturamentoUpp.getCliente();
		

		faturamentoUpp.setObsContrato(obscontrato);
		faturamentoUpp.setObservacao(obsFaturamento);
		
		cliente.setSetorAtividade(seguimento);
		faturamentoUpp.setNumeroNF(nf);

		faturamentoUpp.setValorEtapa(valorEtapa);
		System.out.println("entrou na condi��o de valor da etapa alterado");
		
		List<Faturamento> AllFaturamentosByCliente = new ArrayList<Faturamento>();

		AllFaturamentosByCliente = faturamentoService.getFaturamentoByProdAndCampaign(faturamentoUpp.getId(),
				faturamentoUpp.getCampanha());


		for (Faturamento faturamento : AllFaturamentosByCliente) {
			valorTotal.add(faturamento.getValorEtapa());

		}
		System.out.println(" teste ");
		System.out.println("faturamentoUpp.getId() teste " + faturamentoUpp.getId());
		
		try {
		faturamentoUpp.setValorTotal(valorTotal);
		faturamentoUpp.setAnoFiscal(anoFiscal);
		faturamentoUpp.setCampanha(campanha);
		faturamentoUpp.setEstado(estado);
		faturamentoUpp.setCnpjFaturar(cnpjFaturar);
		faturamentoUpp.setRazaoSocialFaturar(razaoSocialFaturar);
		faturamentoUpp.setEtapa(etapaTrabalhoService.findById(etapa));
		faturamentoUpp.setRazaoSocialFaturar(razaoSocialFaturar);
		contratoUpp.setCnpjFaturar(cnpjFaturar);
		contratoUpp.setCnpjFaturar(cnpjFaturar);
		}catch(Exception e){
			e.printStackTrace();
		}
		faturamentoService.update(faturamentoUpp);
		
		System.out.println("faturamentoUpp.getId() teste 2 " + faturamentoUpp.getId());
		mav.setViewName("redirect:/faturamentos/detailsFaturamentoConsolidado/" + faturamentoUpp.getId());

		return mav;

	}
	
	@GetMapping("/faturamentoConsolidadoUpdate/{id}")
	public ModelAndView updateFaturamentoConsolidado(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		Faturamento faturamentoUpp = faturamentoService.findById(id);
		System.out.println("faturamento faturamentoUpp" + faturamentoUpp.getValorEtapa());
		
		Cliente cliente = faturamentoUpp.getCliente();
		faturamentoService.update(faturamentoUpp);
		BigDecimal totalFaturar = new BigDecimal(0);

		try {

			if(faturamentoUpp.getConsolidadoPorFat() == true) {
				System.out.println("faturamento consolidado" + faturamentoUpp.getConsolidadoPorFat());
				totalFaturar = totalFaturar.add(faturamentoUpp.getValorEtapa());
				System.out.println("faturamento consolidado totalFaturar" + totalFaturar);
			}
			else {
				List<Faturamento> fats = faturamentoService.getAllByProducao(faturamentoUpp.getProducao().getId());
			
				for (Faturamento faturamento2 : fats) {
					totalFaturar = totalFaturar.add(faturamento2.getValorEtapa());
				}
			}
			

		} catch (Exception e) {
			System.out.println("Sem total (Manual)");
			totalFaturar = faturamentoUpp.getValorTotal();
		}
		

		List<ContatoCliente> contatos = new ArrayList<>();
		contatos = contatoService.getAllById(cliente.getId());
		
		List<ContatoCliente> informacoesContato = new ArrayList<>();
		for (ContatoCliente contatoCliente : contatos) {
			if (contatoCliente.isPessoaNfBoleto()) {
				informacoesContato.add(contatoCliente);
			}
		}

		mav.addObject("contatos", informacoesContato);
		mav.addObject("total", totalFaturar);
		mav.addObject("estado", EstadoEtapa.values());
		mav.addObject("seguimento", SetorAtividade.values());
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("etapaTrabalho", faturamentoUpp.getProduto().getEtapasTrabalho());

		mav.addObject("faturamento", faturamentoUpp);
		
		mav.setViewName("/faturamento/faturamentoConsolidadoUpdate");
		
		return mav;
	}

	@GetMapping("/{idFaturamento}/faturamentoConsolidado.json")
	public @ResponseBody Faturamento getFaturamentoConsolidado(@PathVariable("idFaturamento") Long idFaturamento) {

		System.out.println( idFaturamento + " idFaturamento");
		Faturamento fat = faturamentoService.findById(idFaturamento);
		
		return fat;
	}

	@PostMapping("/simularConsolidacaoFaturamento")
	@ResponseBody
	public JSONObject returnSimulacaoConsolidacaoFaturamento(@RequestBody List<JSONObject> param) {

		System.out.println(" Entrou no simularConsolidacaoFaturamento");
		List<Long> fatIds = new ArrayList<Long>();
		JSONObject simulacao = new JSONObject();
		Float percent = 0.0f;

		System.out.println("tamanho"+param.size());
		for (int i = 0; i < param.size(); i++) {
			String idString = param.get(i).toString();
			Long id = Long.parseLong(idString.replaceAll("[^0-9]", ""));
			fatIds.add(id);
		}

	
		List<Faturamento> fats = new ArrayList<Faturamento>();
		

		for (Long id : fatIds) {
			Faturamento fat = faturamentoService.findById(id);
			fats.add(fat);
		}
		
		String idsConcatFatConsolidado = "";
		for (Long id : fatIds) {
			idsConcatFatConsolidado += id.toString() + ";";
		}
		System.out.println("simularConsolidacaoFaturamento ANTES - idsConcatFatConsolidado "+idsConcatFatConsolidado);
		idsConcatFatConsolidado = idsConcatFatConsolidado.substring(0, idsConcatFatConsolidado.length() - 1);
		System.out.println("simularConsolidacaoFaturamento DEPOIS - idsConcatFatConsolidado "+idsConcatFatConsolidado);
		

		simulacao.put("fatsConsolidadosIds", fats);
		
		List<Long> producaoIds = new ArrayList<Long>();
		
		for (int i = 0; i < fats.size(); i++) {
			
			producaoIds.add(fats.get(i).getProducao().getId());
		
		}
		
		Producao producao =  new Producao();
		
		for (Long prodId : producaoIds) {
			producao = producaoService.findById(prodId);
		}
		System.out.println("simularConsolidacaoFaturamento - producao "+producao);
		
		simulacao.put("prod", producao);
		
		BigDecimal valorEtapaconsolidado = BigDecimal.valueOf(0);
		
		
		for (Faturamento fat : fats) {
			System.out.println("simularConsolidacaoFaturamento - fat "+fat.getValorEtapa());
			BigDecimal valor =  fat.getValorEtapa();
			valorEtapaconsolidado = valorEtapaconsolidado.add(valor);
		}
		
		System.out.println("simularConsolidacaoFaturamento - valorEtapaconsolidado "+valorEtapaconsolidado);
		
		List<String> etapasFaturamento = new ArrayList<String>();


        for (int i = 0; i < fats.size(); i++) {
            etapasFaturamento.add(String.valueOf(fats.get(i).getEtapa().getId()));
        }
    	System.out.println("simularConsolidacaoFaturamento - etapasFaturamento "+etapasFaturamento);
        
        if (verifyAllEqualUsingALoop(etapasFaturamento) == false) {
			
		} else {

			Long etapaFaturar = Long.parseLong(etapasFaturamento.get(0));
			EtapasPagamento timings = producao.getContrato().getEtapasPagamento();
			System.out.println("simularConsolidacaoFaturamento - timings "+timings);

			if (Objects.nonNull(timings.getEtapa1())) {
				if (timings.getEtapa1().getId() == etapaFaturar) {
					percent = timings.getPorcentagem1();
				}
			}
			if (Objects.nonNull(timings.getEtapa2())) {
				if (timings.getEtapa2().getId() == etapaFaturar) {
					percent = timings.getPorcentagem2();
				}
			}
			if (Objects.nonNull(timings.getEtapa3())) {
				if (timings.getEtapa3().getId() == etapaFaturar) {
					percent = timings.getPorcentagem3();
				}
			}
			if (Objects.nonNull(timings.getEtapa4())) {
				if (timings.getEtapa4().getId() == etapaFaturar) {
					percent = timings.getPorcentagem4();
				}
			}
			
			
			
		}
        Faturamento faturamentoConsolidado = faturamentoService.geraConsolidacaoByFat(producao);
		System.out.println("simularConsolidacaoFaturamento - faturamentoConsolidado "+faturamentoConsolidado);
        faturamentoConsolidado.setCliente(producao.getCliente());
		faturamentoConsolidado.setValorEtapa(valorEtapaconsolidado);
		faturamentoConsolidado.setIdsFaturamentosConsolidados(idsConcatFatConsolidado);

		simulacao.put("faturamentoConsolidadoByFat", faturamentoConsolidado);
	
	return simulacao;
	}
	
	
	
	@PostMapping("/salvaFaturamentoConsolidado")
	@ResponseBody
	public void salvafatConsolidado(@RequestBody Faturamento faturamento) {

		Producao producao = producaoService.findById(faturamento.getProducao().getId());
		Contrato contrato = contratoService.findById(producao.getContrato().getId());
		
		faturamento.setCnpjFaturar(contrato.getCnpjFaturar());
		faturamento.setRazaoSocialFaturar(contrato.getRazaoSocialFaturar());
		faturamento.setContrato(contrato);
		faturamento.setConsolidado(false);
		faturamento.setConsolidadoPorFat(true);
		faturamento.setObsContrato(contrato.getObsContrato());
		System.out.println("consolidado - getConsolidadoPorFat "+faturamento.getConsolidadoPorFat());
		String[] tokens = faturamento.getIdsFaturamentosConsolidados().split(";", -1);
		
		for (String stringIdConsolidado : tokens) {
			Long id = Long.parseLong(stringIdConsolidado);
			System.out.println("salvaFaturamentoConsolidado - id "+id);
			
			Faturamento faturamentoConsolidado = faturamentoService.findById(id);
			faturamentoConsolidado.setEstado(EstadoEtapa.CONSOLIDADO.displayName());
			
			faturamentoService.update(faturamentoConsolidado);
		}
		
		faturamentoService.update(faturamento);
		
	}
	
	@PostMapping("/desconsolidaFaturamento")
	public ModelAndView desconsolidaFaturamento(@RequestParam("idFaturamento") Long idFaturamento) {
		 
		ModelAndView modelAndView = new ModelAndView();
		Faturamento faturamento = faturamentoService.findById(idFaturamento);
	
		//criando um array com os ids da consolidacao
		String[] idsFats ;
		String idsFaturamento = faturamento.getIdsFaturamentosConsolidados();
		idsFats = idsFaturamento.split(";");
		
		//percorrendo os ids do array, transformando em n�mero  e  mudando o estado dos ids da consolidacao
		for(String ids : idsFats ) {
			Long id = Long.parseLong(ids);
			Faturamento faturamentoDesconsolidar = faturamentoService.findById(id);
			faturamentoDesconsolidar.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
			faturamentoService.update(faturamentoDesconsolidar);
		}
		
		//deletando faturamento consolidado
		faturamentoService.remove(idFaturamento);
		
		modelAndView.setViewName("redirect:/faturamentos/all");
		return modelAndView; 
	}
	
	@GetMapping("/{id}/buscaDataACobrar.json")
	public @ResponseBody String getDataACobrar(@PathVariable("id") Long id) {

		Faturamento fatData = faturamentoService.findById(id);
		String retorno = "";
		if(fatData.getValorF().getId() != null) {
			if(fatData.getValorF().isValorAnual()) {
				retorno = fatData.getValorF().getDataCobrancaAnual();
			}
			if(fatData.getValorF().isValorTrimestral()) {
				retorno = String.valueOf(fatData.getValorF().getDataCobrancaMensal());
			}
			if(fatData.getValorF().isValorMensal()) {
				retorno = String.valueOf(fatData.getValorF().getDataCobrancaMensal());
			}
		} else {
			retorno = "";
		}
		/*
		 * else if (fatData.getPercentualF().getId() != null) { retorno = ""; } else
		 * if(fatData.getValorF().getId() != null) {
		 * if(fatData.getValorF().isValorAnual()) { retorno =
		 * fatData.getValorF().getDataCobrancaAnual(); }
		 * if(fatData.getValorF().isValorTrimestral()) { retorno =
		 * String.valueOf(fatData.getValorF().getDataCobrancaMensal()); }
		 * if(fatData.getValorF().isValorMensal()) { retorno =
		 * String.valueOf(fatData.getValorF().getDataCobrancaMensal()); } }
		 */
		return retorno;
	}

	@GetMapping("/{idContrato}/buscaFaturamentosPorContrato.json")
	public @ResponseBody List<Faturamento> buscaFaturamentosAAlterar(@PathVariable("idContrato") Long idContrato) {
		List<Faturamento> faturamentos = faturamentoService.getAllByContrato(idContrato);
		return faturamentos;
	}
	
	@GetMapping("/{idProducao}/buscaFaturamentosPorProducao.json")
	public @ResponseBody List<Faturamento> buscaFaturamentosAlterar(@PathVariable("idProducao") Long idProducao) {
		List<Faturamento> faturamentos = faturamentoService.getAllByProducao(idProducao);
		return faturamentos;
	}
	
	@PostMapping("/atualizaFaturamentosPorContrato/{idProducao}/{idContratoInicial}/{idContrato}")
	public ModelAndView atualizaFaturamentosDeContratoAlterado(@PathVariable("idProducao") Long idProducao, 
			@PathVariable("idContratoInicial") Long idContratoInicial, @PathVariable("idContrato") Long idContrato) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		Contrato contratoInicial = contratoService.findById(idContratoInicial);
		Contrato contratoNovo = contratoService.findById(idContrato);
		Producao producao = producaoService.findById(idProducao);
		List<Faturamento> faturamentos = faturamentoService.getAllByContrato(idContratoInicial);
		
		System.out.println("CONTRATO antigo        rc              " + contratoInicial);
		System.out.println("CONTRATO novo selecionado rc              " + contratoNovo);
		System.out.println("PRODU��O   rc            " + producao);
		System.out.println("TAMANHO FATS   rc            " + faturamentos.size());
		
		for(int i = 0;i < faturamentos.size(); i++) {
			System.out.println(faturamentos.get(i).getId() + " id fat atual");
			
			Faturamento faturamentoAtual = faturamentoService.findById(faturamentos.get(i).getId());
//			EtapasConclusao etapasConclusaoAtual = etapasConclusaoServiceImpl.findById(faturamentoAtual.getEtapas().getId());
//			EtapaTrabalho etapaTrabalhoAtual = etapaTrabalhoService.findById(faturamentoAtual.getEtapa().getId());
//			BalanceteCalculo balanceteAtual = balanceteCalculoService.findById(faturamentoAtual.getBalancete().getId());
//			if(contrato)
			
			faturamentoAtual.setContrato(contratoNovo);
			faturamentoAtual.setObsContrato(contratoNovo.getObsContrato());
			
			
			if(faturamentoAtual.getValorF() != null) {
				
			}
			//testar os 3 tipos de mem�ria aqui

			/*
			 * if (contratoNovo.isPagamentoEntrega()) { if
			 * (contratoNovo.getEtapasPagamento().getEtapa1() != null) {
			 * System.out.println("Entrou etapa 1 meu m�todo");
			 * faturamentoService.geraFaturamento(balanceteAtual,
			 * contratoNovo.getEtapasPagamento().getPorcentagem1(), etapaTrabalhoAtual,
			 * etapasConclusaoAtual); } if (contratoNovo.getEtapasPagamento().getEtapa2() !=
			 * null) { System.out.println("Entrou etapa 2");
			 * faturamentoService.geraFaturamento(balanceteAtual,
			 * contratoNovo.getEtapasPagamento().getPorcentagem2(), etapaTrabalhoAtual,
			 * etapasConclusaoAtual); } if (contratoNovo.getEtapasPagamento().getEtapa3() !=
			 * null) { System.out.println("Entrou etapa 3");
			 * faturamentoService.geraFaturamento(balanceteAtual,
			 * contratoNovo.getEtapasPagamento().getPorcentagem3(), etapaTrabalhoAtual,
			 * etapasConclusaoAtual); } }
			 */
			
//			deleteFaturamento(faturamentoAtual.getId());
		}
		
		modelAndView.setViewName("redirect:/contratos/all");

		return modelAndView;

	}
	
	public void deleteFaturamento(Long idFaturamento) {
		
		System.out.println("m�todo que apaga os faturamentos : O FATURAMENTO QUE VOU APAGAR �: " + idFaturamento);
		
		comissaoService.deleteByFaturamento(idFaturamento);
		faturamentoService.remove(idFaturamento);
	}
	
	/*
	 * @PostMapping(value = "/{id}/{totalfat}/salvaTotalFaturar.json")
	 * public @ResponseBody void salvaTotalFaturar(@PathVariable("id") Long id,
	 * 
	 * @PathVariable("totalfat") String totalfat) {
	 * 
	 * System.out.println("id"+id); System.out.println("totalfat"+totalfat);
	 * Faturamento faturamento = faturamentoService.findById(id); //verifica
	 * valorTotal antes de alterar valorEtapa Producao producao =
	 * faturamento.getProducao(); Long idProducao = producao.getId();
	 * List<Faturamento> fat = faturamentoService.getAllByProducao(idProducao);
	 * 
	 * BigDecimal totalFaturar = new BigDecimal(totalfat);
	 * System.out.println("totalFaturar"+totalFaturar);
	 * 
	 * //acessa cada faturamento da prod e seta o total a faturar atualizado for
	 * (Faturamento fats : fat ){ fats.setValorTotal(totalFaturar); }
	 * faturamentoService.update(faturamento); return; }
	 */
	
	@GetMapping("/formConsolidadoManual")
	public ModelAndView formConsolidadoManual() {

		ModelAndView mav = new ModelAndView();

		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("estado", EstadoEtapa.values());
		mav.addObject("clientes", clienteService.getAll());
		mav.addObject("etapaTrabalho", etapaTrabalhoService.getAll());

		mav.setViewName("/faturamento/formConsolidadoManual");

		return mav;

	}
	
	@PostMapping("/addManuaisConsolidados")
	public ModelAndView addManuaisConsolidados(@RequestBody JSONObject faturamentoManualAtual) {
		ModelAndView modelAndView = new ModelAndView();

		System.out.println("MIRANHAAAAAAAAAA ");
		System.out.println(faturamentoManualAtual);
		Faturamento faturamentoAtual = new Faturamento();

		String valorT = faturamentoManualAtual.get("valorT").toString();
		valorT = valorT.replace(",", ".");
		String valorE = faturamentoManualAtual.get("valorE").toString();
		valorE = valorE.replace(",", ".");

		BigDecimal valorEtapa = new BigDecimal(valorE);
		BigDecimal valorTotal = new BigDecimal(valorT);
		EtapaTrabalho etapa = etapaTrabalhoService.findById(Long.parseLong(faturamentoManualAtual.get("etapa").toString()));
		String razaoSocialFaturar = faturamentoManualAtual.get("razaoSocialFaturar").toString();
		String cnpjFaturar = faturamentoManualAtual.get("cnpjFaturar").toString();
		
		Date hoje = java.util.Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dataString = dateFormat.format(hoje);

		if(faturamentoManualAtual.get("idProducao") != null) {
			Producao producao = producaoService.getProducaoById(Long.parseLong(faturamentoManualAtual.get("idProducao").toString()));
			faturamentoAtual.setProducao(producao);
			faturamentoAtual.setContrato(producao.getContrato());
			faturamentoAtual.setRazaoSocialFaturar(producao.getContrato().getRazaoSocialFaturar());
			faturamentoAtual.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
			faturamentoAtual.setCampanha(producao.getAno());
			faturamentoAtual.setDataCriacao(dataString);
			faturamentoAtual.setObsContrato(producao.getContrato().getObsContrato());
		}
		
		if(faturamentoManualAtual.get("idBalancete") != null) {
			BalanceteCalculo balancete = balanceteCalculoService.findById(Long.parseLong(faturamentoManualAtual.get("idBalancete").toString()));
			faturamentoAtual.setBalancete(balancete);
		}

		faturamentoAtual.setValorEtapa(valorEtapa);
		faturamentoAtual.setValorTotal(valorTotal);
		faturamentoAtual.setEtapa(etapa);
		faturamentoAtual.setEtapaTrabalho(etapa.getNome());
		faturamentoAtual.setRazaoSocialFaturar(razaoSocialFaturar);
		faturamentoAtual.setCnpjFaturar(cnpjFaturar);
		faturamentoAtual.setManual(true);
		faturamentoAtual.setEstado("ETAPA OPERACIONAL FINALIZADA");
		faturamentoService.create(faturamentoAtual);

		System.out.println(faturamentoAtual.getId() + " ceasaaaaaar");

		modelAndView.setViewName("redirect:/faturamentos/details/" + faturamentoAtual.getId());
		return modelAndView;
	}
}
