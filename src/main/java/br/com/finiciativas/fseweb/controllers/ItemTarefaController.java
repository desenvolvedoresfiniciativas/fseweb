package br.com.finiciativas.fseweb.controllers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.TipoCampo;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.services.impl.ItemTarefaServiceImpl;

@Controller
@RequestMapping("/itensTarefas")
public class ItemTarefaController {
	
	@Autowired
	private ItemTarefaServiceImpl itemTarefaService;
	
	@GetMapping("/form")
	public ModelAndView form(){
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("tiposCampo", TipoCampo.values());
		mav.addObject("itens", itemTarefaService.getLista());
		
		mav.setViewName("/itemtarefa/form");
		
		return mav;
	}
	
	@PostMapping("/add")
	public String create(ItemTarefa itemTarefa){
		
		itemTarefaService.create(itemTarefa);
		
		return "redirect:/itensTarefas/form";
	}
	
	@GetMapping("/all.json")
	public @ResponseBody List<ItemTarefa> itemJson() {

		return itemTarefaService.getAll();

	}
	
	@GetMapping("/itensT.json")
	public @ResponseBody List<ItemTarefa> getAllItensTarefa(){
		
		List<ItemTarefa> itens = itemTarefaService.getAll();

		return itens;
		
	}
	
}
