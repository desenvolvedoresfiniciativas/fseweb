package br.com.finiciativas.fseweb.controllers;

import java.util.UUID;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.conf.GenericResponse;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;

@Controller
public class LoginController {

	@Autowired
	private ConsultorServiceImpl consultorService;

//	@GetMapping("/login")
	@RequestMapping(value = "/login")
	public String formLogin() {
		return "login";
	}

	@Transactional
	@GetMapping("/preLogout")
	public ModelAndView preLogout() {

		ModelAndView mav = new ModelAndView();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());

		System.out.println("O Usu�rio �: "+consultor.getNome()+" e o id �: "+consultor.getId() );
		System.out.println("O usu�rio � primeiro login? " + consultor.isPrimeiroLogin());

		if (consultor.isPrimeiroLogin() == true) {

			System.out.println("Entrou na condicional");

			consultor.setPrimeiroLogin(false);
			consultorService.update(consultor);
		}

		mav.setViewName("redirect:/logout");
		return mav;
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session) {

		session.invalidate();

		return "redirect:login";
	}

	@GetMapping("/forgetPassword")
	public ModelAndView forgotPassword() {
		
		System.out.println("forgotPassword");
		
		try {

			HtmlEmail email = new HtmlEmail();
			email.setHostName("SMTP.office365.com");
			email.setSmtpPort(587);
			email.setSSLOnConnect(false);
			email.setStartTLSEnabled(true);
			email.setStartTLSRequired(true);
			email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@@fi*1303"));
			email.setFrom("fseweb@f-iniciativas.com.br");
			email.setSubject("TOKEN SENHA ");
			email.addTo("adolfo.carvalho@fi-group.com");
			email.addTo("felipe.farias@fi-group.com");
			email.addTo("beatriz.zuchetto@fi-group.com");

			StringBuffer msg = new StringBuffer();
			msg.append("<html><body>");
			msg.append("<b>Aten��o! Envio autom�tico pelo FSEWeb, n�o responder.</b><br/>");
			msg.append("<br/>");
			msg.append("Prezado(a),<br/>");
			msg.append("<br/>");
			msg.append("Reset token: <b>"  + "</b><br/>");
			msg.append("e-mail: <b>" + "</b><br/>");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("<br/>");

			msg.append("");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("Obrigado.");

			email.setHtmlMsg(msg.toString());

			// set the alternative message
			email.setTextMsg("Your email client does not support HTML messages");
			// send the email
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}

		ModelAndView mav = new ModelAndView();
		resetPassword(null);

		mav.setViewName("/forgetPassword");
		return mav;
	}

	@PostMapping("/resetPassword")
	public GenericResponse resetPassword(@RequestParam("email") String userEmail) {
	    Consultor consultor = consultorService.findUserByEmail(userEmail);
	    System.out.println(userEmail + " userEmail");
	    if (consultor == null) {
	        System.out.println("Usu�rio n�o encontrado");
	    }

	    String token = UUID.randomUUID().toString();
	    //consultorService.createPasswordResetTokenForUser(consultor, token);
	    //System.out.println("------------------- O TOKEN �: " + token + " E O E-MAIL �: " + userEmail);

	    constructResetTokenEmail(token, consultor);
	    return new GenericResponse("","");
	}

	private void constructResetTokenEmail(String token, Consultor consultor) {
		try {

			HtmlEmail email = new HtmlEmail();
			email.setHostName("SMTP.office365.com");
			email.setSmtpPort(587);
			email.setSSLOnConnect(false);
			email.setStartTLSEnabled(true);
			email.setStartTLSRequired(true);
			email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@@fi*1303"));
			email.setFrom("fseweb@f-iniciativas.com.br");
			email.setSubject("TOKEN SENHA " + token);
			email.addTo("adolfo.carvalho@fi-group.com");
//			email.addTo("felipe.farias@fi-group.com");
//			email.addTo("beatriz.zuchetto@fi-group.com");

			StringBuffer msg = new StringBuffer();
			msg.append("<html><body>");
			msg.append("<b>Aten��o! Envio autom�tico pelo FSEWeb, n�o responder.</b><br/>");
			msg.append("<br/>");
			msg.append("Prezado(a),<br/>");
			msg.append("<br/>");
			msg.append("Reset token: <b>" + token + "</b><br/>");
			msg.append("e-mail: <b>" + "</b><br/>");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("<br/>");

			msg.append("");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("Obrigado.");

			email.setHtmlMsg(msg.toString());

			// set the alternative message
			email.setTextMsg("Your email client does not support HTML messages");
			// send the email
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
