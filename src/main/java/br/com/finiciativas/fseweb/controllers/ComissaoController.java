package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Premio;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.repositories.impl.ComissaoRepositoryImpl;
import br.com.finiciativas.fseweb.repositories.impl.EtapaPagamentoRepositoryImpl;
import br.com.finiciativas.fseweb.services.impl.ComissaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ImpostosServiceImpl;
import br.com.finiciativas.fseweb.services.impl.NotaFiscalServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PremioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Controller
@RequestMapping(path = "/comissoes")
public class ComissaoController {
	
	Locale ptBR = new Locale("pt", "BR");
	
	@Autowired
	private ComissaoServiceImpl comissaoService;
	
	@Autowired
	private ComissaoRepositoryImpl comissaoRepository;
	
	@Autowired
	private PremioServiceImpl premioService;
	
	@Autowired
	private ConsultorServiceImpl consultorService;
	
	@Autowired
	private ImpostosServiceImpl impostosService;
	
	@Autowired
	private ValorNegocioServiceImpl valorNegocioService;
	
	@Autowired
	private EtapaPagamentoRepositoryImpl etapaPagamentoService;
	
	@Autowired
	private ProducaoServiceImpl producaoService;
	
	@Autowired
	private NotaFiscalServiceImpl notaFiscalService;
	
	@Autowired
	private ContratoServiceImpl contratoService;
	
	@Autowired
	private ProdutoServiceImpl produtoService;
	
	@Autowired
	private HonorarioServiceImpl honorarioService;
	
	@GetMapping(path = "/form")
	public ModelAndView form() 
	{
		ModelAndView mav = new ModelAndView("/comissao/form");
//		Impostos impostos = impostosService.getImpostosByCampanha2020();
		
		mav.addObject("impostos", impostosService.getImpostosByCampanha2020());
		mav.addObject("consultores",  this.consultorService.getAll());
					
		return mav;
	}

	@Transactional
	@PostMapping(path = "/add")
	public ModelAndView add(@Valid Comissao comissao){
		ModelAndView mav = new ModelAndView();
		
		NotaFiscal notaFiscal = notaFiscalService.findById(comissao.getNotaFiscal().getId());
		System.out.println("o id da nota fiscal �: " + notaFiscal.getId());
		System.out.println("o id da produ��o �: " + notaFiscal.getFaturamento().getProducao().getId());
		System.out.println("o id do faturamento �: " + notaFiscal.getFaturamento().getId());
		System.out.println("o id do contrato �: " + notaFiscal.getContrato().getId());
		
		comissao.setContrato(notaFiscal.getContrato());
		comissao.setFaturamento(notaFiscal.getFaturamento());
		comissao.setProducao(notaFiscal.getFaturamento().getProducao());
		comissao.setAtiva(true);
		comissao.setDataCriacao(java.util.Calendar.getInstance().getTime());
		
		comissaoService.create(comissao);
			/*
			 * Consultor consultor = consultorService.findById(idConsultor); Producao
			 * producao = producaoService.findById(idProducao); Contrato contrato =
			 * producao.getContrato();
			 * 
			 * System.out.println("id do consultor: " + idConsultor);
			 * System.out.println("id da producao: " + idProducao);
			 * System.out.println("id do contrato: " + idContrato); System.out.println();
			 * 
			 * ModelAndView mav = new ModelAndView();
			 * 
			 * Comissao comissao = new Comissao();
			 * 
			 * comissao.setConsultor(consultor); comissao.setContrato(contrato);
			 * comissao.setProducao(producao);
			 * 
			 * comissao.setCampanha(campanha);
			 * comissao.setComissaoPercentual(percentualComissao);
			 * comissao.setComissaoValor(valorComissao);
			 * comissao.setObservacoes(observacoes);
			 * 
			 * comissao.setPagamento(false);
			 * 
			 * comissao.setValorNegocio(valorNegocioService.getVNbyProd(producao.getId()));
			 * 
			 * //Atualizar notas fiscais das comissoes, apos notas fiscais do FSEWEB
			 * estiverem prontas comissao.setNotaFiscal(null);
			 * 
			 * 
			 * // comissao = comissaoService.create(comissao); //
			 * comissaoService.create(comissao);
			 */		
		mav.setViewName("redirect:/comissoes/all");
		
		return mav;
	}
	
	@PostMapping(path = "/delete/{id}")
	public ModelAndView desativaComissao(@PathVariable("id") Long id) {
		
		Comissao comissao = comissaoService.findById(id);
		comissao.setAtiva(false);
		comissaoService.update(comissao);
		
		Long consultor = comissao.getConsultor().getId();
		
		ModelAndView mav = new ModelAndView();
		
		mav.setViewName("redirect:/comissoes/details/" + consultor);
		
		return mav;
	}
	
	@GetMapping(path = "/details/{id}")
	public ModelAndView findById(@PathVariable("id") Long id, @RequestParam(value = "campanha", required = false) String ano,
			@RequestParam(value = "queryTrigger", required = false) String queryTrigger,
			@RequestParam(value = "pagamento", required = false) String pagamentoString,
			@RequestParam(value = "simulada", required = false) String simuladaString,
			@RequestParam(value = "trimestrePagamento", required = false) String trimestrePagamento,
			@RequestParam(value = "anoPagamento", required = false) String anoPagamento,
			@RequestParam(value = "idProduto", required = false) Long idProduto,
			@RequestParam(value = "empresa", required = false) String empresa,
			@RequestParam(value = "etapa", required = false) Long etapa) {
		ModelAndView mav = new ModelAndView();
		
		Consultor consultor = consultorService.findById(id);
		
		
		List<Premio> premios = premioService.getAllByConsultor(id);
		
		System.out.println(consultor.getNome());
		System.out.println(consultor.getEmail());
		System.out.println(consultor.getCargo());
		
		if(queryTrigger != null) {
			System.out.println("filtrar comiss�es");
			
			Boolean paga;
			Boolean simulada;
			
			if (pagamentoString == "") {
				paga = null;
			} else if (pagamentoString.equals("true")) {
				paga = true;
			} else {
				paga = false;
			}

			if (simuladaString == "") {
				simulada = null;
			} else if (simuladaString.equals("true")) {
				simulada = true;
			} else {
				simulada = false;
			}
			List<Comissao> comissoes = comissaoService.getComissoesByCriteria(ano, paga, simulada, trimestrePagamento, anoPagamento, id, idProduto, empresa, etapa);
			mav.addObject("comissoes", comissoes);
		}else {
			System.out.println("n�o filtrar comiss�es");
			List<Comissao> comissoes = comissaoService.getComissaoAtivaByConsultor(id);
			mav.addObject("comissoes", comissoes);
		}
		
		mav.addObject("consultor", consultor);
		mav.addObject("produtos", produtoService.getAll());
		
		mav.addObject("premios", premios);
	
		mav.setViewName("comissao/details");
		
		return mav;
	}
	
	@GetMapping(path = "/info/{id}")
	public ModelAndView findByIdComissao(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();
		
		Comissao comissao = comissaoService.findById(id);
		Contrato contrato = contratoService.findById(comissao.getContrato().getId());

		if(comissao.getNotaFiscal() != null) {
			NotaFiscal notaFiscal = notaFiscalService.findById(comissao.getNotaFiscal().getId());
			List<Honorario> hon = new ArrayList<Honorario>();
			PercentualEscalonado percentual = new PercentualEscalonado();
			
			try {
				hon = contrato.getHonorarios();
				percentual = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());
			} catch (Exception e) {
				System.out.println(e);
			}
			
			mav.addObject("notaFiscal", notaFiscal);
			mav.addObject("honors", hon);
		}

		System.out.println(comissao.getConsultor());
		System.out.println(comissao.getCampanha());
		System.out.println(comissao.getProducao());
		System.out.println(comissao.getValorNegocio());
//		System.out.println(comissao.getNotasFiscais());
		System.out.println(comissao.getContrato());
		
		mav.addObject("comissao", comissao);
	
		mav.setViewName("comissao/info");
		
		return mav;
	}
	
	@Transactional
	@PostMapping(path = "/validar/{id}")
	public ModelAndView validarComissao(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();
		
		Comissao comissao = comissaoService.findById(id);
		
		comissao.setValidado(true);
		comissaoService.update(comissao);
		
	
		mav.setViewName("redirect:/comissoes/details/" + comissao.getConsultor().getId());
		
		return mav;
	}
	
	@GetMapping(path = "/all")
	public ModelAndView getAll() {
		
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("consultores", consultorService.getAtivosAndInativos());
		mav.addObject("cargos", Cargo.values());
		mav.setViewName("/comissao/list");
		
		//Criteria Query para listar por cargo
		
		
		return mav;
	}
	
//	@GetMapping("/listAll")
	public ModelAndView getAllContracts() {

		ModelAndView mav = new ModelAndView();
		
		return mav;
	}
	
	@GetMapping(path = "/details")
	public ModelAndView details() {
		ModelAndView mav = new ModelAndView();

		
		mav.setViewName("/comissao/details");
		
		return mav;
	}
	
	@Transactional
	@GetMapping("/update/{id}")
	public ModelAndView updateView(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView("/comissao/update");
		
		Comissao comissao = comissaoService.findById(id);
		System.out.println("entrou no m�todo");
		//System.out.println("o id do consultor �: " + comissao.getConsultor().getNome());
		
		mav.addObject("consultores", this.consultorService.getAll());
		mav.addObject("comissao",comissao);
		
		mav.setViewName("/comissao/update");
		
		return mav;
	}
	
	@PostMapping("/update")
	public ModelAndView update(@Valid Comissao comissao) {
		ModelAndView mav = new ModelAndView();
		System.out.println("teste consultor " + comissao.getConsultor().getNome());
		comissaoRepository.update(comissao);
		
		mav.setViewName("redirect:/comissoes/update/" + comissao.getId());
		
		return mav;
	}
	
	@GetMapping("/producao/{id}/valorNegocio.json")
	public @ResponseBody ValorNegocio valorNegocioByIDJson(@PathVariable("id")Long id) {
		
		System.out.println("Producao ID: " + id);
		System.out.println("achei o m�todo");
		
		if(valorNegocioService.getVNbyProd(id) != null) {
			ValorNegocio valorNegocio = valorNegocioService.getVNbyProd(id);
			
			if(valorNegocio.getEstComercial() != 0) {
				System.out.println("Estimativa comercial do VN: " + valorNegocio.getEstComercial());
			}else {
				System.out.println("Estimativa comercial do VN zerada");
			}
			System.out.println("Valor negocio ID: " + valorNegocio.getId());
			System.out.println("Modo do valor: " + valorNegocio.getModoValor());
			System.out.println("Nome empresa: " + valorNegocio.getNome_empresa());
			System.out.println("Valor base: " + valorNegocio.getValorBase());
			
			return valorNegocio;
		}else {
			System.out.println("Producao com id: " + id + " nao possui valor de negocio");
			return null;
		}
		
	}
	@GetMapping("/producao/{id}/etapaPagamento.json")
	public @ResponseBody EtapasPagamento etapaTrabalhoByIDJson(@PathVariable("id")Long id) {
		
		System.out.println("Producao ID: " + id);
		
		Producao producao = producaoService.findById(id);
		
		EtapasPagamento etapaPagamento = etapaPagamentoService.findByContratoId(producao.getContrato().getId());
		
		System.out.println("Nome da etapa 1: " + etapaPagamento.getEtapa1().getNome());
		System.out.println("Parcela 1: " + etapaPagamento.getParcela1());
		System.out.println("Porcentagem 1 : " + etapaPagamento.getPorcentagem1());
		
		System.out.println("Nome da etapa 2: " + etapaPagamento.getEtapa2());
		System.out.println("Parcela 2: " + etapaPagamento.getParcela2());
		System.out.println("Porcentagem 2 : " + etapaPagamento.getPorcentagem2());
		
		System.out.println("Nome da etapa 3: " + etapaPagamento.getEtapa3());
		System.out.println("Parcela 3: " + etapaPagamento.getParcela3());
		System.out.println("Porcentagem 3 : " + etapaPagamento.getPorcentagem3());
		
		return etapaPagamento;
		
	}
	
	@GetMapping(value = "/{id}/comissoesAtivas.json")
	public @ResponseBody List<Comissao> getComissoesAtivasByConsultor(@PathVariable(name = "id") Long id) {
		System.out.println("entrou no m�todo getComissoesAtivas");
		try {
			List<Comissao> comissoes = comissaoService.getComissaoAtivaByConsultor(id);

			return comissoes;
		}catch(Exception e) {
			e.printStackTrace();
			List<Comissao> comissoesVazias = new ArrayList<Comissao>();
			 return comissoesVazias;
		}
		
	}
	
	@PostMapping(value = "/calculaComissaoByAno/{ano}")
	@ResponseBody
	public void gerarComissoes2020(@PathVariable("ano") String ano) {
		
		comissaoService.gerarComissaoByAno(ano);
	
	}
	
	@PostMapping(value = "/calculaComissaoByConsultor/{idConsultor}")
	@ResponseBody
	public void calculaComissaoByConsultor(@PathVariable("idConsultor") Long idConsultor) {
		System.out.println("encontrou o m�todo por ajax");
		
		comissaoService.gerarComissaoByConsultor(idConsultor);
	
	}
}