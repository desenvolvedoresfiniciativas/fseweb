package br.com.finiciativas.fseweb.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.lista.ValoresLista;
import br.com.finiciativas.fseweb.services.impl.ItemTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValoresListaServiceImpl;

@Controller
@RequestMapping("/listas")
public class ValoresListaController {

	@Autowired
	private ItemTarefaServiceImpl itemTarefaService;
	
	@Autowired
	private ValoresListaServiceImpl ValoresListaService;
	
	@GetMapping("/update/{id}")
	public ModelAndView form(@PathVariable("id") Long id){
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("item", itemTarefaService.findById(id));
		mav.addObject("valores", ValoresListaService.findByItemId(id));
		
		mav.setViewName("/itemtarefa/valoreslista/form");
		
		return mav;
	}
	
	@PostMapping("/add/{id}")
	public ModelAndView add(@PathVariable("id") Long id,
			@RequestParam("valor") String valor,
			@RequestParam("idItem") Long idItem){
		ModelAndView mav = new ModelAndView();
	
		ValoresLista item = new ValoresLista();
		item.setValor(valor);
		item.setItem(itemTarefaService.findById(idItem));
		
		ValoresListaService.create(item);
		mav.setViewName("redirect:/listas/update/"+id);
		return mav;
	}
	

	
}
