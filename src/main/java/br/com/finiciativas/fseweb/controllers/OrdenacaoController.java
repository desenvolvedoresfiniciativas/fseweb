package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.models.Eficiencia;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.produto.ordenacao.OrdemItensTarefa;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.OrdemItensTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.TarefaServiceImpl;

@Controller
@RequestMapping("/ordenacoes")
public class OrdenacaoController {

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private TarefaServiceImpl tarefaService;

	@Autowired
	private OrdemItensTarefaServiceImpl ordemService;

	@Autowired
	private ItemValoresServiceImpl ItemValoresServiceImpl;

	@GetMapping("/all")
	public ModelAndView getAll() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("produtos", produtoService.getAll());

		mav.setViewName("/ordenacao/list");

		return mav;
	}

	@GetMapping("/produto/{id}")
	public ModelAndView ordenarProduto(@PathVariable("id") Long id) {
		ModelAndView mav = new ModelAndView();

		Produto produto = produtoService.findById(id);
		List<EtapaTrabalho> etapas = produto.getEtapasTrabalho();
		List<Tarefa> tarefas = new ArrayList<Tarefa>();

		for (EtapaTrabalho etapaTrabalho : etapas) {
			tarefas.addAll(etapaTrabalho.getTarefas());
		}

		mav.addObject("produto", produto);
		mav.addObject("tarefas", tarefas);

		mav.setViewName("/ordenacao/produto");

		return mav;
	}

	@GetMapping("/tarefa/{id}")
	public ModelAndView ordenarItensTarefa(@PathVariable("id") Long id, @RequestParam("idProd") Long idProd) {
		ModelAndView mav = new ModelAndView();

		// Fazer algoritmo que pega o produto atual e cria as linhas necess�rias para a
		// Ordena��o
		Tarefa tarefa = tarefaService.findById(id);

		List<OrdemItensTarefa> ordem = ordemService.getOrdenacaoByTarefa(id);

		if (ordem.isEmpty()) {
			if (!tarefa.getItensTarefa().isEmpty()) {
				System.out.println("N�o trouxe ordena��o: T� EMPTY");
				ordemService.createOrdenacaoOfProduto(idProd);
				ordem = ordemService.getOrdenacaoByTarefa(id);
			}
		}

		mav.addObject("tarefa", tarefa);
		mav.addObject("itens", ordem);

		mav.setViewName("/ordenacao/tarefa");

		return mav;
	}

	@PostMapping("/salvaOrdem")
	@ResponseBody
	public void salvaOrdem(@RequestBody List<OrdemItensTarefa> itensOrdenados) {

		for (OrdemItensTarefa ordemItensTarefa : itensOrdenados) {

			OrdemItensTarefa ordemItem = ordemService.findById(ordemItensTarefa.getId());

			List<ItemValores> itens = ItemValoresServiceImpl.findByItemAndTarefaId(ordemItem.getItem().getId(),
					ordemItem.getTarefa().getId());
			if (!itens.isEmpty()) {
				for (ItemValores item : itens) {
					item.setNumeroOrdem(ordemItensTarefa.getNumero());

					ItemValoresServiceImpl.update(item);
				}
			}

			ordemItem.setNumero(ordemItensTarefa.getNumero());
			ordemService.update(ordemItem);

		}

	}
}
