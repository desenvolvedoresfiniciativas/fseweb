package br.com.finiciativas.fseweb.controllers;


import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Departamento;
import br.com.finiciativas.fseweb.models.PeriodoPagamento;
import br.com.finiciativas.fseweb.services.impl.PeriodoPagamentoServiceImpl;



@Controller
@RequestMapping(path = "/periodoPagamento")
public class PeriodoPagamentoController {
	
	@Autowired
	private PeriodoPagamentoServiceImpl periodoPagamentoService;
	
	@GetMapping("/list")
	public ModelAndView getList() {

		ModelAndView mav = new ModelAndView();
		
		
		
		mav.addObject("periodos",periodoPagamentoService.getAll());
		mav.setViewName("/periodoPagamento/list");

		return mav;
	}
	
	@GetMapping("/form")
	public ModelAndView formPeriodoPagamento() {

		ModelAndView mav = new ModelAndView();

		mav.addObject("departamentos", Departamento.values());
		mav.setViewName("/periodoPagamento/form");

		return mav;

	}
	
	@Transactional
	@PostMapping(path = "/add")
	public ModelAndView create(@Valid PeriodoPagamento periodo) {
		System.out.println("entrou no m�todo");
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("a data de inicio �: " + periodo.getDataInicio());
		System.out.println("a data final �: " + periodo.getDataFim());
		System.out.println("a data de pagamento e: " + periodo.getDataPagamento());
		
		periodoPagamentoService.create(periodo);

		modelAndView.setViewName("redirect:/periodoPagamento/list");

		return modelAndView;

	}
}
