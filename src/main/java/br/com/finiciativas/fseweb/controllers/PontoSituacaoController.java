package br.com.finiciativas.fseweb.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.enums.TipoDeApuracao;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Role;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.RelatorioTecnicoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;

@Controller
@RequestMapping(path = "/pontoSituacao")
public class PontoSituacaoController {

	@Autowired
	private EtapasConclusaoServiceImpl etapasConclusaoService;

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private EquipeServiceImpl equipeService;

	@Autowired
	private ProducaoServiceImpl ProducaoService;

	@Autowired
	private SubProducaoServiceImpl subProducaoService;

	@Autowired
	private RelatorioTecnicoServiceImpl relatorioTecnicoService;

	@Autowired
	private BalanceteCalculoServiceImpl balanceteCalculoService;

	@Autowired
	private ItemValoresServiceImpl itemValoresService;
	
	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@GetMapping("/produtos")
	public ModelAndView produtos() {

		ModelAndView mav = new ModelAndView();

		mav.addObject("produtos", produtoService.getAll());
		mav.setViewName("pontoSituacao/produtos");

		return mav;

	}

	@GetMapping("/{id}")
	public ModelAndView pontoSituacao(@PathVariable("id") Long id) {
		System.out.println("ponto situa��o controller");
		ModelAndView mav = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Produto produto = produtoService.findById(id);

		List<EtapaTrabalho> etapasTrabalho = produto.getEtapasTrabalho();
		// List<EtapasConclusao> etapasConclusao =
		// etapasConclusaoService.getAllEtapasByProduto(id);
		// List<Cliente> clientesUntreated = new ArrayList<Cliente>();
		// List<Cliente> clientesTreated = new ArrayList<Cliente>();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = equipeService.getConsultorByEquipe(consultor.getId());

//		for (EtapasConclusao etapa : etapasConclusao) {
//			clientesUntreated.add(etapa.getProducao().getCliente());
//		}
//		
//		for (Cliente cliente : clientesUntreated) {
//			 if (!clientesTreated.contains(cliente)) {
//				 clientesTreated.add(cliente);
//			  }
//		}

		mav.addObject("cargo", Cargo.values());
		mav.addObject("consultor", consultor);
		mav.addObject("produto", produto);
		mav.addObject("etapasTrabalho", etapasTrabalho);
		// mav.addObject("etapasConclusao", etapasConclusao);
		// mav.addObject("clientes", clientesTreated);
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("equipes", equipeService.getAll());
		mav.addObject("equipe", equipe);
		mav.setViewName("/pontoSituacao/relatorio");

		return mav;

	}

	@GetMapping("/lb")
	public ModelAndView pontoSituacaoLB() {

		ModelAndView mav = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Consultor consultor = ((Consultor) auth.getPrincipal());
		Cargo cargo = consultor.getCargo();
		Equipe equipe = equipeService.findEquipeByConsultor(consultor.getId());
		List<Role> roles = consultor.getRoles();

		if (cargo == Cargo.CONSULTOR_LIDER || cargo == Cargo.CONSULTOR_LIDER_TECNICO || cargo == Cargo.LIDER_TECNICO
				|| cargo == Cargo.LIDER_DE_TRANSFORMACAO_DIGITAL || cargo == Cargo.LIDER_REGIONAL) {
			// mav.addObject("consultores",
			// consultorService.getEquipeUtilizandoConsultorLogado());
			equipe = equipeService.findByLider(consultor.getId());
			System.out.println("a equipe �: " + equipe.getNome() + "o id da equipe �: " + equipe.getId());
			mav.addObject("consultores", equipeService.getConsultores(equipe.getId()));
			mav.addObject("equipes", equipeService.findByLider(consultor.getId()));
		} else if (cargo == Cargo.CONSULTOR_TECNICO || cargo == Cargo.CONSULTOR_COMERCIAL
				|| cargo == Cargo.CONSULTOR_COMERCIAL_JUNIOR_3 || cargo == Cargo.CONSULTOR_TECNICO_TRAINEE
				|| cargo == Cargo.CONSULTOR_TRAINEE || cargo == Cargo.CONSULTOR_ESPECIALISTA) {
			mav.addObject("consultores", consultor);
			mav.addObject("equipes", equipeService.findEquipeByConsultor(consultor.getId()));
		} else if (cargo == Cargo.COORDENADOR_TECNICO) {
			List<Equipe> equipesCoordenador = equipeService.getAllEquipesByCoordenador(consultor.getId());
			List<Consultor> consultores = new ArrayList();
			for (Equipe equipes : equipesCoordenador) {
				consultores.addAll(equipeService.getConsultores(equipes.getId()));
			}

			mav.addObject("consultores", consultores);
			mav.addObject("equipes", equipesCoordenador);
		} else if (cargo == Cargo.GERENTE_GERAL || cargo == Cargo.GERENTE_CONSULTORIA_TECNICA
				|| cargo == Cargo.GERENTE_GERAL || cargo == Cargo.GERENTE_REGIONAL || cargo == Cargo.GERENTE_KAM || cargo == Cargo.GERENTE_DE_NEGOCIOS) {
			List<Equipe> equipesGerente = equipeService.getAllEquipesByGerenteGeral(consultor.getId());
			List<Consultor> consultores = new ArrayList();
			for (Equipe equipes : equipesGerente) {
				consultores.addAll(equipeService.getConsultores(equipes.getId()));
			}

			mav.addObject("consultores", consultores);
			mav.addObject("equipes", equipesGerente);
		} else if (cargo == Cargo.DIRETOR_DE_OPERACOES || cargo == Cargo.DIRETOR_EXECUTIVO || cargo == Cargo.DIRETORIA
				|| cargo == Cargo.DIRETOR_DE_NEGOCIO) {
			mav.addObject("consultores", consultorService.getAll());
			mav.addObject("equipes", equipeService.getAll());
		} else if (cargo == Cargo.ESTAGIARIO_TECNICO) {
			
			
			mav.addObject("consultores", equipeService.getConsultores(equipe.getId()));
			mav.addObject("equipes", equipe);
		}
		for (Role role : roles) {
			if (role.getNome().equals("ROLE_ADMIN")) {
				mav.addObject("consultores", consultorService.getAll());
				mav.addObject("equipes", equipeService.getAll());
			}
		}

		// mav.addObject("consultores", consultorService.getConsultores());
		mav.setViewName("/pontoSituacao/relatorioLB");

		return mav;

	}

	@SuppressWarnings("null")
	@PostMapping("/carrega")
	public @ResponseBody List<JSONObject> carregaPontoSituacao(@RequestBody JSONObject parametros) {

		List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
		Long equipeId = null;
		Long consultorId = null;
		Boolean abertura = null;
		Boolean identificacao = null;
		Boolean quantificacao = null;
		Boolean defesa = null;
		Boolean submissao = null;
		Boolean encerramento = null;
		Boolean mcti = null;
		Boolean formCliente = null;
		Boolean er = null;
		Boolean gerouFaturamento = null;

		System.out.println("objeto" + parametros);
		String ano = parametros.get("ano").toString();
		String siglaConsultor = "";
		String razaoSocial = parametros.get("razaoSocial").toString();
		if (Objects.nonNull(parametros.get("equipe").toString()) && parametros.get("equipe").toString() != "") {
			equipeId = Long.parseLong(parametros.get("equipe").toString());
			System.out.println("equipe" + equipeId);
		}
		if (Objects.nonNull(parametros.get("sigla").toString()) && parametros.get("sigla").toString() != "") {
			siglaConsultor = parametros.get("sigla").toString();
			System.out.println("consultor" + consultorId);
		}
		if (Objects.nonNull(parametros.get("abertura").toString()) && parametros.get("abertura").toString() != "") {
			abertura = Boolean.parseBoolean(parametros.get("abertura").toString());
		}
		if (Objects.nonNull(parametros.get("identificacao").toString())
				&& parametros.get("identificacao").toString() != "") {
			identificacao = Boolean.parseBoolean(parametros.get("identificacao").toString());
		}
		if (Objects.nonNull(parametros.get("quantificacao").toString())
				&& parametros.get("quantificacao").toString() != "") {
			quantificacao = Boolean.parseBoolean(parametros.get("quantificacao").toString());
		}
		if (Objects.nonNull(parametros.get("defesa").toString()) && parametros.get("defesa").toString() != "") {
			defesa = Boolean.parseBoolean(parametros.get("defesa").toString());
		}
		if (Objects.nonNull(parametros.get("submissao").toString()) && parametros.get("submissao").toString() != "") {
			submissao = Boolean.parseBoolean(parametros.get("submissao").toString());
		}
		if (Objects.nonNull(parametros.get("encerramento").toString())
				&& parametros.get("encerramento").toString() != "") {
			encerramento = Boolean.parseBoolean(parametros.get("encerramento").toString());
		}
		if (Objects.nonNull(parametros.get("formCliente").toString())
				&& parametros.get("formCliente").toString() != "") {
			formCliente = Boolean.parseBoolean(parametros.get("formCliente").toString());
			System.out.println("form cliente: " + formCliente);
		}
		if (Objects.nonNull(parametros.get("er").toString())
				&& parametros.get("er").toString() != "") {
			er = Boolean.parseBoolean(parametros.get("er").toString());
			System.out.println("er: " + er);
		}
		

		// System.out.println("sigla" + siglaConsultor);
		System.out.println("ano" + ano);
		System.out.println("razaoSocial" + razaoSocial);

		System.out.println("abertura" + abertura);

		List<EtapasConclusao> etapas = ProducaoService.getProducaoAndEtapasByCriteria(razaoSocial, ano, siglaConsultor,
				equipeId);

		//for (EtapasConclusao etapasConclusao : etapas) {
		//	System.out.println(etapasConclusao.getEtapa().getId());
		//	System.out.println("a produ��o � :" + etapasConclusao.getProducao().getId());
		//}

		try {
			List<EtapasConclusao> filtros = new ArrayList();
			for (EtapasConclusao etapa : etapas) {
				System.out.println("o id da etapa �:" + etapa.getEtapa().getId());
				if (abertura != null && etapa.getEtapa().getId() == 63l && etapa.isConcluido() != abertura) {
					System.out.println("entrou na abertura de miss�o n�o conclu�da");
					List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
							.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					if (Objects.nonNull(etapasFiltrar)) {
						filtros.addAll(etapasFiltrar);
					}
				} else if (identificacao != null && etapa.getEtapa().getId() == 65l
						&& etapa.isConcluido() != identificacao) {
					List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
							.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					if (Objects.nonNull(etapasFiltrar)) {
						filtros.addAll(etapasFiltrar);
					}
				} else if (quantificacao != null && etapa.getEtapa().getId() == 66l) {
					// List <EtapasConclusao> etapasFiltrar =
					// etapasConclusaoService.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					System.out.println("entrou no filtro de quantifica��o");
					List<BalanceteCalculo> balancetes = balanceteCalculoService
							.getBalancetesByProducao(etapa.getProducao().getId());
					int countBalancetes = 0;
					for (BalanceteCalculo balancete : balancetes) {
						if (balancete.isValidado() && balancetes.size() > 0) {
							countBalancetes++;
						}
					}
					if (quantificacao == true) {
						System.out.println("entrou no filtro de quantifica��o true" + balancetes.size());
						if (balancetes.size() != countBalancetes) {
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						} else if (balancetes.size() == 0) {
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						}
					} else {
						if (balancetes.size() == countBalancetes && balancetes.size() != 0) {
							System.out.println("entrou no filtro de quantifica��o false");
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						}
					}
				} else if (defesa != null && etapa.getEtapa().getId() == 67l) {
					List<RelatorioTecnico> relatorios = relatorioTecnicoService
							.getRelatorioTecnicoByProducao(etapa.getProducao().getId());
					int countFinalizados = 0;
					for (RelatorioTecnico relatorio : relatorios) {
						if (relatorio.getEstado() != Estado.PENDENTE && relatorio.getEstado() != Estado.EM_REDA��O
								&& relatorio.getEstado() != null) {
							countFinalizados++;
						}
					}

					if (defesa == true) {
						if (relatorios.size() != countFinalizados) {
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						} else if (relatorios.size() == 0) {
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						}
					} else {
						if (relatorios.size() == countFinalizados && relatorios.size() != 0) {
							List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
									.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
							if (Objects.nonNull(etapasFiltrar)) {
								filtros.addAll(etapasFiltrar);
							}
						}
					}

				} else if (submissao != null && etapa.getEtapa().getId() == 68l && etapa.isConcluido() != submissao) {
					List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
							.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					if (Objects.nonNull(etapasFiltrar)) {
						filtros.addAll(etapasFiltrar);
					}
				} else if (formCliente != null && etapa.getEtapa().getId() == 68l) {
					List<Tarefa> tarefas = etapa.getEtapa().getTarefas();
					System.out.println("criou o array de tarefas no filtro");
					for (Tarefa tarefa : tarefas) {
						if (tarefa.getId() == 155) {
							System.out.println("----------------ACHOU A TAREFA NO FILTRO--------------------------");
							List<ItemTarefa> itensTarefa = tarefa.getItensTarefa();
							if (formCliente == true) {
								for (ItemTarefa itemTarefa : itensTarefa) {
									if (itemTarefa.getId() == 331l) {
										System.out.println(
												"----------------ACHOU O ITEM TAREFA NO FILTRO--------------------------");
										ItemValores item = itemValoresService.findByTarefaItemTarefaAndProducao(
												tarefa.getId(), etapa.getProducao().getId(), itemTarefa.getId());
										if (Objects.nonNull(item.getValor()) && item.getValor().isEmpty()) {
											System.out.println("entoru no filtro itemTarefa sim");
											List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
													.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
											if (Objects.nonNull(etapasFiltrar)) {
												filtros.addAll(etapasFiltrar);
											}
										}
									}

								}
							} else {
								for (ItemTarefa itemTarefa : itensTarefa) {
									if (itemTarefa.getId() == 331l) {
										System.out.println(
												"----------------ACHOU O ITEM TAREFA NO FILTRO--------------------------");
										ItemValores item = itemValoresService.findByTarefaItemTarefaAndProducao(
												tarefa.getId(), etapa.getProducao().getId(), itemTarefa.getId());
										if (Objects.nonNull(item.getValor()) && !item.getValor().isEmpty()) {
											System.out.println("entoru no filtro itemTarefa n�o");
											List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
													.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
											if (Objects.nonNull(etapasFiltrar)) {
												filtros.addAll(etapasFiltrar);
											}
										}
									}

								}
							}

						}

					}

				}else if (er != null && etapa.getEtapa().getId() == 69l) {
					List<Tarefa> tarefas = etapa.getEtapa().getTarefas();
					System.out.println("criou o array de tarefas no filtro");
					for (Tarefa tarefa : tarefas) {
						if (tarefa.getId() == 157) {
							System.out.println("----------------ACHOU A TAREFA NO FILTRO--------------------------");
							List<ItemTarefa> itensTarefa = tarefa.getItensTarefa();
							if (er == true) {
								for (ItemTarefa itemTarefa : itensTarefa) {
									if (itemTarefa.getId() == 347l) {
										System.out.println(
												"----------------ACHOU O ITEM TAREFA NO FILTRO--------------------------");
										System.out.println(itemTarefa.getNome());
										ItemValores item = itemValoresService.findByTarefaItemTarefaAndProducao(
												tarefa.getId(), etapa.getProducao().getId(), itemTarefa.getId());
										if (item.getValor().isEmpty() || item.getValor().equalsIgnoreCase("false")) {
											System.out.println("entoru no filtro itemTarefa sim");
											List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
													.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
											if (Objects.nonNull(etapasFiltrar)) {
												filtros.addAll(etapasFiltrar);
											}
										}
									}

								}
							} else {
								for (ItemTarefa itemTarefa : itensTarefa) {
									if (itemTarefa.getId() == 347l) {
										System.out.println(
												"----------------ACHOU O ITEM TAREFA NO FILTRO--------------------------");
										System.out.println(itemTarefa.getNome());
										ItemValores item = itemValoresService.findByTarefaItemTarefaAndProducao(
												tarefa.getId(), etapa.getProducao().getId(), itemTarefa.getId());
										try {
											if (item.getValor().equalsIgnoreCase("true")) {
												System.out.println("entoru no filtro itemTarefa n�o");
												List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
														.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
												if (Objects.nonNull(etapasFiltrar)) {
													filtros.addAll(etapasFiltrar);
												}
											}
										}catch(Exception e) {
											
										}
										
									}

								}
							}

						}

					}
					
				
				}else if (encerramento != null && etapa.getEtapa().getId() == 69l
						&& etapa.isConcluido() != encerramento) {
					List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
							.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					if (Objects.nonNull(etapasFiltrar)) {
						filtros.addAll(etapasFiltrar);
					}
				} else if (mcti != null && etapa.getEtapa().getId() == 70l && etapa.isConcluido() != mcti) {
					List<EtapasConclusao> etapasFiltrar = etapasConclusaoService
							.getAllEtapasConclusaoByProducao(etapa.getProducao().getId());
					if (Objects.nonNull(etapasFiltrar)) {
						filtros.addAll(etapasFiltrar);
					}
				} 

			}

			if (Objects.nonNull(filtros)) {
				etapas.removeAll(filtros);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		int contador = 0;
		Long producaoId = null;
		JSONObject linhaPontoSituacao = new JSONObject();
		// try {

		for (EtapasConclusao etapa : etapas) {
			System.out.println("entrou no loop");

			if (contador == 0) {
				producaoId = etapa.getProducao().getId();

			}

			if (producaoId != etapa.getProducao().getId()) {
				System.out.println("salvou na troca de produ��o");
				System.out.println(linhaPontoSituacao);
				jsonObjects.add(linhaPontoSituacao);

				producaoId = etapa.getProducao().getId();
				linhaPontoSituacao = new JSONObject();
			}

			System.out.println("o id da produ��o �: " + etapa.getProducao().getId());
			///////aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
//			try {
				List<Faturamento> faturamentos = faturamentoService.getAllByProducao(etapa.getProducao().getId());
//				System.out.println(faturamento.getId() + " ------------------- fat id");
				if(faturamentos.size() > 0) {
					linhaPontoSituacao.put("gerouFaturamento", "Sim");
					} else {
					linhaPontoSituacao.put("gerouFaturamento", "N�o");
					}
//			} catch(Exception e) {
//				gerouFaturamento = false;
//			}

			linhaPontoSituacao.put("razaoSocial", etapa.getProducao().getCliente().getRazaoSocial());
			linhaPontoSituacao.put("idProducao", etapa.getProducao().getId());
			linhaPontoSituacao.put("ano", etapa.getProducao().getAno());
			linhaPontoSituacao.put("equipe", etapa.getProducao().getEquipe().getNome());
			linhaPontoSituacao.put("consultor", etapa.getProducao().getConsultor().getSigla());
			if (etapa.getEtapa().getId() == 63l) {
				System.out.println("entrou abertura de miss�o");
				linhaPontoSituacao.put("abertura", (etapa.isConcluido()) ? "Sim" : "N�o");
			} else if (etapa.getEtapa().getId() == 65l) {
				System.out.println("entrou identifica��o");
				linhaPontoSituacao.put("identifica��o", (etapa.isConcluido()) ? "Sim" : "N�o");
			} else if (etapa.getEtapa().getId() == 66l) {
				System.out.println("entrou quantifica��o ");
				List<BalanceteCalculo> balancetes = balanceteCalculoService
						.getBalancetesByProducao(etapa.getProducao().getId());
				System.out.println(balancetes.size());
				int countBalancetes = 0;
				for (BalanceteCalculo balancete : balancetes) {
					System.out.println(
							"a producao que esta sendo verificadas os balancetes � " + etapa.getProducao().getId());
					System.out.println("o id do balancete �: " + balancete.getId());
					if (balancete.isValidado() || balancete.isPrejuizo()) {
						countBalancetes++;
					}
				}
				int quantidadeBalancetes = 0;
				if (etapa.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
					quantidadeBalancetes = 1;
					if (countBalancetes >= 1) {
						linhaPontoSituacao.put("quantifica��o", 1 + "/" + quantidadeBalancetes);
					} else {
						linhaPontoSituacao.put("quantifica��o", 0 + "/" + quantidadeBalancetes);
					}

				}
				if (etapa.getProducao().getTipoDeApuracao() == TipoDeApuracao.TRIMESTRAL) {
					quantidadeBalancetes = 4;
					linhaPontoSituacao.put("quantifica��o", countBalancetes + "/" + quantidadeBalancetes);
				}
				if (etapa.getProducao().getTipoDeApuracao() == TipoDeApuracao.MENSAL) {
					quantidadeBalancetes = 12;
					linhaPontoSituacao.put("quantifica��o", countBalancetes + "/" + quantidadeBalancetes);
				}

			} else

			if (etapa.getEtapa().getId() == 67l) {
				System.out.println("entrou defesa");
				List<RelatorioTecnico> relatorios = relatorioTecnicoService
						.getRelatorioTecnicoByProducao(etapa.getProducao().getId());
				int countFinalizados = 0;
				for (RelatorioTecnico relatorio : relatorios) {
					if (relatorio.getEstado() != Estado.PENDENTE && relatorio.getEstado() != Estado.EM_REDA��O
							&& relatorio.getEstado() != null) {
						countFinalizados++;
					}
				}

				linhaPontoSituacao.put("defesa", countFinalizados + "/" + relatorios.size());

			} else if (etapa.getEtapa().getId() == 68l) {
				List<Tarefa> tarefas = etapa.getEtapa().getTarefas();
				//etapa submiss�o MCTI
				for (Tarefa tarefa : tarefas) {
					if (tarefa.getId() == 156) {
						System.out.println("----------------ACHOU A TAREFA--------------------------");
						List<ItemTarefa> itensTarefa = tarefa.getItensTarefa();
						for (ItemTarefa itemTarefa : itensTarefa) {
							if (itemTarefa.getId() == 369l) {
								System.out.println("----------------ACHOU O ITEM TAREFA DO ER--------------------------");
								System.out.println("o item � "+ itemTarefa.getNome());
								ItemValores item = itemValoresService.findByTarefaItemTarefaAndProducao(tarefa.getId(),
										producaoId, itemTarefa.getId());
								System.out.println("O CONTE�DO DO STATUS FORMUL�RIO �: " + item.getValor());
								if(item.getValor().equalsIgnoreCase("recebido cliente")) {
									System.out.println("ACHOU FORM SIM");
									linhaPontoSituacao.put("FormCliente", "Sim");
								}else {
									linhaPontoSituacao.put("FormCliente", "N�o");
								}
								//linhaPontoSituacao.put("FormCliente",
								//		(Objects.nonNull(item.getValor()) && !item.getValor().isEmpty() ? "Sim"
								//				: "N�o"));
							}

						}
					}

				}

				// linhaPontoSituacao.put("submissao", );
				linhaPontoSituacao.put("submissao", (etapa.isConcluido()) ? "Sim" : "N�o");
			} else if (etapa.getEtapa().getId() == 69l) {
				System.out.println("entrou encerramento");
				List<Tarefa> tarefas = etapa.getEtapa().getTarefas();

				for (Tarefa tarefa : tarefas) {
					if (tarefa.getId() == 157) {
						System.out.println("----------------ACHOU A TAREFA--------------------------");
						List<ItemTarefa> itensTarefa = tarefa.getItensTarefa();
						for (ItemTarefa itemTarefa : itensTarefa) {
							if (itemTarefa.getId() == 347l) {
								System.out.println("----------------ACHOU O ITEM TAREFA DO DOSSI� final--------------------------");
								System.out.println("o item � "+ itemTarefa.getNome());
								ItemValores item = itemValoresService.findByTarefaItemTarefaAndProducao(tarefa.getId(),
										producaoId, itemTarefa.getId());
								if(item.getValor().isEmpty()){
									linhaPontoSituacao.put("ER",( "N�o"));
								}else {
									linhaPontoSituacao.put("ER", (item.getValor().equalsIgnoreCase("true") ? "Sim" : "N�o"));
								}
								
								
										
							}

						}
					}

				}
				
				linhaPontoSituacao.put("encerramento", (etapa.isConcluido()) ? "Sim" : "N�o");
				
				
			} else if (etapa.getEtapa().getId() == 70l) {
				System.out.println("entrou mcti");
				linhaPontoSituacao.put("mcti", (etapa.isConcluido()) ? "Sim" : "N�o");
			} else if (etapa.getEtapa().getId() == 71l) {
				System.out.println("entrou rfb");
				linhaPontoSituacao.put("rfb", (etapa.isConcluido()) ? "Sim" : "N�o");
			}

			// jsonObjects.add(linhaPontoSituacao);
			if (contador == etapas.size() - 1) {
				System.out.println("salvou no fim do loop");
				jsonObjects.add(linhaPontoSituacao);
			}

			contador++;
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		return jsonObjects;
	}

	@GetMapping("/{id}/{equipe}/etapas.json")
	public @ResponseBody List<EtapasConclusao> getAllByProdutoAndEquipe(@PathVariable("id") Long id,
			@PathVariable("equipe") Long equipe) {

		System.out.println("entrou na controller");

		// List<EtapasConclusao> etapas =
		// etapasConclusaoService.getAllEtapasByProduto(id);
		List<EtapasConclusao> etapas = etapasConclusaoService.getAllEtapasByProdutoAndEquipe(id, equipe, "");

		return etapas;

	}

	@GetMapping("/{id}/etapas.json")
	public @ResponseBody List<EtapasConclusao> getAllByProduto(@PathVariable("id") Long id) {

		List<EtapasConclusao> etapas = etapasConclusaoService.getAllEtapasByProduto(id, "");

		return etapas;

	}

	@GetMapping("/{id}/etapasCL.json")
	public @ResponseBody List<EtapasConclusao> getAllByCL(@PathVariable("id") Long id) {

		List<EtapasConclusao> etapas = etapasConclusaoService.getAllEtapasByProduto(id, "");

		return etapas;

	}

	@GetMapping("/{idEtapa}/{idProducao}/subProds.json")
	public @ResponseBody JSONObject getConclusaoSubProds(@PathVariable("idEtapa") Long idEtapa,
			@PathVariable("idProducao") Long idProducao) {

		JSONObject resultObj = new JSONObject();

		System.out.println("Recebi prod " + idProducao + " e etapa " + idEtapa);

		resultObj.put("idProducao", idProducao);
		resultObj.put("idEtapa", idEtapa);

		List<SubProducao> countSubProds = subProducaoService.getSubProducaobyProducaoAndEtapa(idProducao, idEtapa);
		int trueCounter = 0;
		for (SubProducao subProducao : countSubProds) {
			if (subProducao.isFinalizado()) {
				trueCounter++;
			}
		}

		String done = "";
		if (trueCounter == countSubProds.size()) {
			done = "true";
		} else {
			done = "false";
		}

		if (trueCounter == 0 && countSubProds.size() == 0) {
			done = "false";
		}

		resultObj.put("scts", +trueCounter + "/" + countSubProds.size());
		resultObj.put("done", done);

		return resultObj;

	}
	
	@GetMapping("/buscaFaturamentosGeradosByProducao/{idProducao}")
	public @ResponseBody String buscaFaturamentos(@PathVariable("idProducao") Long idProducao) {
		System.out.println("produ��o a buscar " + idProducao);

		List<Faturamento> faturamentos = faturamentoService.getAllByProducao(idProducao);
		System.out.println(faturamentos.size() + " tamnho fats dessa prod");
		String retorno = "";
		if(faturamentos.size() > 0) {
			retorno = "Sim";
		} else {
			System.out.println("N�o possui fats");
			retorno = "N�o";
		}
		System.out.println(retorno + " retorno");

		return retorno;
	}
}
