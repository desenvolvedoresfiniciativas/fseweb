package br.com.finiciativas.fseweb.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.Aprovacao;
import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.FilialExterna;
import br.com.finiciativas.fseweb.enums.SituacaoReco;
import br.com.finiciativas.fseweb.models.IeM.Evento;
import br.com.finiciativas.fseweb.models.IeM.Institucional;
import br.com.finiciativas.fseweb.models.IeM.Marketing;
import br.com.finiciativas.fseweb.models.IeM.RecoInterna;
import br.com.finiciativas.fseweb.models.IeM.RecoInternacional;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EventoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.InstitucionalServiceImpl;
import br.com.finiciativas.fseweb.services.impl.MarketingServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.RecoInternaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.RecoInternacionalServiceImpl;

// IeM = Inova��o e Marketing
@Controller
@RequestMapping(path = "/IeM")
public class IeMController {
	
	@Autowired
	private RecoInternaServiceImpl recoInternaService;
	
	@Autowired
	private RecoInternacionalServiceImpl recoInternacionalService;
	
	@Autowired
	private InstitucionalServiceImpl institucionalService;
	
	@Autowired
	private MarketingServiceImpl marketingService;
	
	@Autowired
	private EventoServiceImpl eventoServiceImpl;
	
	@Autowired
	private ConsultorServiceImpl consultorService;
	
	@Autowired
	private ProdutoServiceImpl produtoService;

	@GetMapping("/all")
	public ModelAndView getAll() {

		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("recosInternas", recoInternaService.getAll());
		modelAndView.addObject("recosInternacionais", recoInternacionalService.getAll());
		modelAndView.addObject("institucionais", institucionalService.getAll());
		modelAndView.addObject("marketings", marketingService.getAll());
		modelAndView.addObject("eventos", eventoServiceImpl.getAll());

		modelAndView.setViewName("/IeM/list");

		return modelAndView;
	}
	
	@GetMapping("/form")
	public ModelAndView formRecoInterna() {

		ModelAndView mav = new ModelAndView();
		
		mav.addObject("aprovacao", Aprovacao.values());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("comerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("filiais", FilialExterna.values());
		mav.addObject("situacoes", SituacaoReco.values());
			
		mav.setViewName("/IeM/form");

		return mav;
	}
	
	@GetMapping("/recoInterna/{id}")
	public ModelAndView formRecoInterna(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();
		
		RecoInterna recoInterna = recoInternaService.findById(id);
		
		mav.addObject("reco", recoInterna);
		mav.addObject("aprovacao", Aprovacao.values());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("comerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("filiais", FilialExterna.values());
		mav.addObject("situacoes", SituacaoReco.values());
			
		mav.setViewName("/IeM/recoInternaUpdate");

		return mav;
	}
	
	@GetMapping("/recoInternacional/{id}")
	public ModelAndView formRecoInternacional(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();
		
		RecoInternacional recoInternacional = recoInternacionalService.findById(id);
		
		mav.addObject("reco", recoInternacional);
		mav.addObject("aprovacao", Aprovacao.values());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("comerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("filiais", FilialExterna.values());
		mav.addObject("situacoes", SituacaoReco.values());
			
		mav.setViewName("/IeM/recoInternacionalUpdate");

		return mav;
	}
	
	@GetMapping("/institucional/{id}")
	public ModelAndView formInstitucional(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();
		
		Institucional institucional = institucionalService.findById(id);
		
		mav.addObject("institucional", institucional);
		mav.addObject("aprovacao", Aprovacao.values());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("comerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("filiais", FilialExterna.values());
		mav.addObject("situacoes", SituacaoReco.values());
			
		mav.setViewName("/IeM/recoInstitucionalUpdate");

		return mav;
	}
	
	@GetMapping("/evento/{id}")
	public ModelAndView formEvento(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();
		
		Evento evento = eventoServiceImpl.findById(id);
		
		mav.addObject("evento", evento);
		mav.addObject("aprovacao", Aprovacao.values());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("comerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("filiais", FilialExterna.values());
		mav.addObject("situacoes", SituacaoReco.values());
			
		mav.setViewName("/IeM/recoEventoUpdate");

		return mav;
	}
	
	@GetMapping("/marketing/{id}")
	public ModelAndView formMarketing(@PathVariable("id") Long id) {

		ModelAndView mav = new ModelAndView();
		
		Marketing marketing = marketingService.findById(id);
		
		mav.addObject("marketing", marketing);
		mav.addObject("aprovacao", Aprovacao.values());
		mav.addObject("consultores", consultorService.getConsultores());
		mav.addObject("comerciais", consultorService.getConsultorByCargo(Cargo.CONSULTOR_COMERCIAL));
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("filiais", FilialExterna.values());
		mav.addObject("situacoes", SituacaoReco.values());
			
		mav.setViewName("/IeM/recoMarketingUpdate");

		return mav;
	}
	
	@Transactional
	@PostMapping("/addRecoInterna")
	public ModelAndView addRecoInterna(@Valid RecoInterna recoInterna) {

		ModelAndView mav = new ModelAndView();
		
		recoInternaService.create(recoInterna);
			
		mav.setViewName("redirect:/IeM/all");

		return mav;
	}
	
	@Transactional
	@PostMapping("/addRecoInternacional")
	public ModelAndView addRecoInternacional(@Valid RecoInternacional recoInternacional) {

		ModelAndView mav = new ModelAndView();
		
		recoInternacionalService.create(recoInternacional);
			
		mav.setViewName("redirect:/IeM/all");

		return mav;
	}
	
	@Transactional
	@PostMapping("/addInstitucional")
	public ModelAndView addInstitucional(@Valid Institucional institucional) {
		
		ModelAndView mav = new ModelAndView();
		
		institucionalService.create(institucional);
		
		mav.setViewName("redirect:/IeM/all");
		
		return mav;
	}
	
	@Transactional
	@PostMapping("/addEvento")
	public ModelAndView addEvento(@Valid Evento evento) {
		
		ModelAndView mav = new ModelAndView();
		
		eventoServiceImpl.create(evento);
		
		mav.setViewName("redirect:/IeM/all");
		
		return mav;
	}
	
	@Transactional
	@PostMapping("/addMarketing")
	public ModelAndView addMarketing(@Valid Marketing marketing) {
		
		ModelAndView mav = new ModelAndView();
		
		marketingService.create(marketing);
		
		mav.setViewName("redirect:/IeM/all");
		
		return mav;
	}
	
	@PostMapping("/recoInterna/update/")
	public ModelAndView updateRecoInterna(@Valid RecoInterna recoInterna) {
		
		ModelAndView mav = new ModelAndView();
		
		recoInternaService.update(recoInterna);
		
		mav.setViewName("redirect:/IeM/update/" + recoInterna.getId());
		
		return mav;
		
	}
	
	@PostMapping("/recoInternacional/update")
	public ModelAndView updateRecoInternacional(@Valid RecoInternacional recoInternacional) {
		
		ModelAndView mav = new ModelAndView();
		
		recoInternacionalService.update(recoInternacional);
		
		mav.setViewName("redirect:/IeM/update/" + recoInternacional.getId());
		
		return mav;
		
	}
	
	@PostMapping("/institucional/update")
	public ModelAndView updateInstitucional(@Valid Institucional institucional) {
		
		ModelAndView mav = new ModelAndView();
		
		institucional = institucionalService.update(institucional);
		
		mav.setViewName("redirect:/IeM/update" + institucional.getId());
		
		return mav;
		
	}
	
	@PostMapping("/evento/update")
	public ModelAndView updateEvento(@Valid Evento evento) {
		
		ModelAndView mav = new ModelAndView();
		
		eventoServiceImpl.update(evento);
		
		mav.setViewName("redirect:/IeM/update/" + evento.getId());
		
		return mav;
		
	}
	
	@PostMapping("/marketing/update")
	public ModelAndView updateMarketing(@Valid Marketing marketing) {
		
		ModelAndView mav = new ModelAndView();
		
		marketingService.update(marketing);
		
		mav.setViewName("redirect:/IeM/update/" + marketing.getId());
		
		return mav;
		
	}
	
	@Transactional
	@PostMapping("/deleteRecoInterna/{id}")
	public ModelAndView deleteRecoInterna(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();
		
		RecoInterna recoInterna = recoInternaService.findById(id);
		
		recoInternaService.delete(recoInterna);

		modelAndView.setViewName("redirect:/IeM/all");

		return modelAndView;

	}
	
	@Transactional
	@PostMapping("/deleteRecoInternacional/{id}")
	public ModelAndView deleteRecoInternacional(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();
		
		RecoInternacional recoInternacional = recoInternacionalService.findById(id);
		
		recoInternacionalService.delete(recoInternacional);

		modelAndView.setViewName("redirect:/IeM/all");

		return modelAndView;

	}
	
	@Transactional
	@PostMapping("/deleteInstitucional/{id}")
	public ModelAndView deleteInstitucional(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();
		
		Institucional institucional = institucionalService.findById(id);
		
		institucionalService.delete(institucional);

		modelAndView.setViewName("redirect:/IeM/all");

		return modelAndView;

	}
	
	@Transactional
	@PostMapping("/deleteEvento/{id}")
	public ModelAndView deleteEvento(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();
		
		Evento evento = eventoServiceImpl.findById(id);
		
		eventoServiceImpl.delete(evento);

		modelAndView.setViewName("redirect:/IeM/all");

		return modelAndView;

	}
	
	@Transactional
	@PostMapping("/deleteMarketing/{id}")
	public ModelAndView deleteMarketing(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();
		
		Marketing marketing = marketingService.findById(id);
		
		marketingService.delete(marketing);

		modelAndView.setViewName("redirect:/IeM/all");

		return modelAndView;

	}
	
}
