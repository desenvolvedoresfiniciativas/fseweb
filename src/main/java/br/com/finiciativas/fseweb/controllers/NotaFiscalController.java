package br.com.finiciativas.fseweb.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.finiciativas.fseweb.enums.EstadoEtapa;
import br.com.finiciativas.fseweb.enums.Incidencia;
import br.com.finiciativas.fseweb.enums.MotivoCancelamento;
import br.com.finiciativas.fseweb.enums.TipoNotaFiscal;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.Parcela;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.services.ComissaoService;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EnderecoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FilialServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.NotaFiscalServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ParcelaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;

@Controller
@RequestMapping("/notasFiscais")
public class NotaFiscalController {

	@Autowired
	private NotaFiscalServiceImpl notaFiscalServiceImpl;

	@Autowired
	private FilialServiceImpl filialService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private NotaFiscalServiceImpl notaFiscalService;

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private ParcelaServiceImpl parcelaService;

	@Autowired
	private ComissaoService comissaoService;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private EnderecoClienteServiceImpl enderecoService;

	@Autowired
	private ContatoClienteServiceImpl contatoClienteService;

	@Autowired
	private HonorarioServiceImpl honorarioService;

	@Autowired
	private BalanceteCalculoServiceImpl balanceteCalculoService;

	@GetMapping("/all")
	public ModelAndView getAll(@RequestParam(value = "campanha", required = false) String ano,
			@RequestParam(value = "queryTriggerNotas", required = false) String queryTriggerNotas,
			@RequestParam(value = "queryTriggerFaturamento", required = false) String queryTriggerFaturamento,
			@RequestParam(value = "idProduto", required = false) Long idProduto,
			@RequestParam(value = "etapa", required = false) Long etapa,
			@RequestParam(value = "filial", required = false) Long idFilial,
			@RequestParam(value = "razaoSocial", required = false) String empresa,
			@RequestParam(value = "numeroNF", required = false) String numeroNF,
			@RequestParam(value = "tipoNegocio", required = false) String tipoNegocio,
			@RequestParam(value = "emAtraso", required = false) String emAtraso,
			@RequestParam(value = "paga", required = false) String statusPagamento,
			@RequestParam(value = "mesBusca", required = false) String mesSelecionado) {
		
		ModelAndView mav = new ModelAndView();
		
		List<NotaFiscal> notas = new ArrayList<NotaFiscal>();

		if (queryTriggerNotas == null) {
			System.out.println("reconheceu o querytrigger notas fiscais false");
			mav.addObject("notasFiscais", this.notaFiscalServiceImpl.getAll());
		}

		Boolean atraso;
		Boolean pago;

		if (queryTriggerNotas != null) {
			System.out.println("reconheceu o querytrigger de notas fiscais true");
			if (emAtraso.equalsIgnoreCase("")) {
				atraso = null;
			} else if (emAtraso.equals("true")) {
				atraso = true;
			} else {
				atraso = false;
			}
			System.out.println("o status do atraso �" + atraso);

			if (statusPagamento.equalsIgnoreCase("")) {
				pago = null;
			} else if (statusPagamento.equalsIgnoreCase("true")) {
				pago = true;
			} else {
				pago = false;
			}

			if (ano.equalsIgnoreCase("")) {
				ano = null;
			}

			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}

			if (tipoNegocio.equalsIgnoreCase("")) {
				tipoNegocio = null;
			}
			
			if (numeroNF.equalsIgnoreCase("")) {
				numeroNF = null;
			}

			if (queryTriggerNotas.equalsIgnoreCase("true")) {

				notas = notaFiscalService.getNotasByCriteria(ano, idProduto, etapa, idFilial, empresa, tipoNegocio, numeroNF,
						atraso, pago);

			}
			mav.addObject("notasFiscais", notas);
		}

		// List<Faturamento> faturamentosNaoEmitidos =
		// faturamentoService.getFaturamentosNaoEmitidos();
		List<Faturamento> faturamentosNaoEmitidos = new ArrayList<>();
		// List<Faturamento> faturamentos = new ArrayList<Faturamento>();

		if (queryTriggerFaturamento == null) {
			System.out.println("reconheceu query trigger faturamento false");
			mav.addObject("faturamentosNaoEmitidos", faturamentoService.getFaturamentosNaoEmitidos());
		}

		if (queryTriggerFaturamento != null) {
			System.out.println("entrou no criteria de faturamento true");
			System.out.println("reconheceu query trigger faturamento");
			if (ano.equalsIgnoreCase("")) {
				ano = null;
			}
			if (empresa.equalsIgnoreCase("")) {
				empresa = null;
			}

			if (tipoNegocio.equalsIgnoreCase("")) {
				tipoNegocio = null;
			}
			
//			if (dataInicio.equalsIgnoreCase("")) {
//				dataInicio = null;
//			}
//			
//			if (dataFinal.equalsIgnoreCase("")) {
//				dataFinal = null;
//			}
			
			if (mesSelecionado.equalsIgnoreCase("")) {
				mesSelecionado = null;
			}

			if (queryTriggerFaturamento.equalsIgnoreCase("true")) {
				faturamentosNaoEmitidos = faturamentoService.getFaturamentosByCriteria(ano, idProduto, etapa, idFilial,
						empresa, tipoNegocio, mesSelecionado);

				// mav.setViewName("/notaFiscal/list");
			}
			mav.addObject("faturamentosNaoEmitidos", faturamentosNaoEmitidos);
		}

		// mav.addObject("notasFiscais", this.notaFiscalServiceImpl.getAll());

		// mav.addObject("faturamentos", faturamentoService.getFatLiberado());
		mav.addObject("incidencia", Incidencia.values());
		mav.addObject("motivoCancelamento", MotivoCancelamento.values());
		mav.addObject("tipoNotaFiscal", TipoNotaFiscal.values());
		mav.addObject("filiais", filialService.getAll());

		// mav.addObject("faturamentosEmitidos",
		// faturamentoService.getFaturamentosEmitidos());
		// mav.addObject("faturamentosNaoEmitidos",
		// faturamentoService.getFaturamentosNaoEmitidos());
		System.out.println("achei a pesquisa");
		System.out.println(ano);

		List<Produto> produtos = produtoService.getAll();

		mav.addObject("estado", filialService.getAll());
		mav.addObject("produtos", produtos);

//		mav.addObject("faturamentosEmitidos", faturamentoService.getFaturamentosEmitidos());
		// mav.addObject("faturamentosNaoEmitidos",
		// faturamentoService.getFaturamentosNaoEmitidos());

		mav.setViewName("/notaFiscal/list");

		return mav;
	}

	@GetMapping("/form")
	public ModelAndView form() {
		ModelAndView mav = new ModelAndView("/notaFiscal/form");

		mav.addObject("filiais", filialService.getAll());
		mav.addObject("incidencia", Incidencia.values());
		mav.addObject("produtos", produtoService.getAll());
		mav.addObject("tipoNotaFiscal", TipoNotaFiscal.values());

		return mav;
	}

	@GetMapping("/{id}")
	public ModelAndView findById(@PathVariable("id") Long id) throws Exception {

		ModelAndView mav = new ModelAndView();

		NotaFiscal nf = notaFiscalServiceImpl.findById(id);
		
		if (Objects.nonNull(nf.getFaturamento())) {
			Long idFaturamento = nf.getFaturamento().getId();

			Faturamento faturamentoUpp = faturamentoService.findById(idFaturamento);

			List<Faturamento> faturamentos = new ArrayList<Faturamento>();

			if (faturamentoUpp.getProducao() != null) {
				faturamentos = faturamentoService.getAllByProducao(faturamentoUpp.getProducao().getId());
			} else if(faturamentoUpp.getContrato()!=null){
				faturamentos = faturamentoService.getAllByContrato(faturamentoUpp.getContrato().getId());
			}

			BigDecimal valorTotal = new BigDecimal("0.0");

			for (Faturamento faturamento : faturamentos) {
				valorTotal = valorTotal.add(faturamento.getValorEtapa());
				System.out.println(faturamento.getValorEtapa());
				System.out.println(valorTotal);

			}

			mav.addObject("total", valorTotal);

		} else {
			mav.addObject("total", 0);
		}

		mav.addObject("notaFiscal", notaFiscalServiceImpl.findById(id));
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("incidencia", Incidencia.values());
		mav.addObject("motCancelamento", MotivoCancelamento.values());
		mav.addObject("tipoNotaFiscal", TipoNotaFiscal.values());
		
		mav.setViewName("/notaFiscal/update");

		return mav;
	}

	@PostMapping("/add")
	public ModelAndView create(@RequestBody @Valid NotaFiscal notaFiscal) {

		ModelAndView modelAndView = new ModelAndView();

		Faturamento fatHolder = faturamentoService.findById(notaFiscal.getFaturamento().getId());

		notaFiscal.setContrato(fatHolder.getContrato());
		notaFiscal.setTiming(fatHolder.getEtapa().getNome());
		fatHolder.setEstado("EMITIDO");
		fatHolder.setNumeroNF(notaFiscal.getNumeroNF());

		String dataEmissao = notaFiscal.getDataEmissao();

		if (!dataEmissao.isEmpty() && dataEmissao != null) {
			String diaEmissao = dataEmissao.substring(0, 2);
			String mesEmissao = dataEmissao.substring(3, 5);
			String anoEmissao = dataEmissao.substring(6);
			dataEmissao = anoEmissao + "-" + mesEmissao + "-" + diaEmissao;
		} else {
			dataEmissao = null;
		}

		String dataVencimento = notaFiscal.getDataVencimento();

		if (!dataVencimento.isEmpty() && dataVencimento != null) {
			String diaVencimento = dataVencimento.substring(0, 2);
			String mesVencimento = dataVencimento.substring(3, 5);
			String anoVencimento = dataVencimento.substring(6);
			dataVencimento = anoVencimento + "-" + mesVencimento + "-" + diaVencimento;
		} else {
			dataVencimento = null;
		}

		String dataCobranca = notaFiscal.getDataCobranca();

		if (!dataCobranca.isEmpty() && dataCobranca != null) {
			String diaCobranca = dataCobranca.substring(0, 2);
			String mesCobranca = dataCobranca.substring(3, 5);
			String anoCobranca = dataCobranca.substring(6);
			dataCobranca = anoCobranca + "-" + mesCobranca + "-" + diaCobranca;
		} else {
			dataCobranca = null;
		}

		notaFiscal.setDataEmissao(dataEmissao);
		notaFiscal.setDataVencimento(dataVencimento);
		notaFiscal.setDataCobranca(dataCobranca);

		NotaFiscal notaAdd = notaFiscalServiceImpl.update(notaFiscal);

		List<Parcela> parcelas = notaAdd.getParcelas();
		for (Parcela parcela : parcelas) {
			parcela.setNotaFiscal(notaAdd);
			parcelaService.update(parcela);
		}

		if (notaAdd.isCancelada() == false) {
			// comissaoService.createComissaoByNota(notaAdd);
		}

		modelAndView.setViewName("redirect:/notasFiscais/" + notaAdd.getId());

		return modelAndView;

	}

	@Transactional
	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		ModelAndView modelAndView = new ModelAndView();

		NotaFiscal notaFiscal = notaFiscalServiceImpl.findById(id);
		modelAndView.addObject("incidencia", Incidencia.values());
		modelAndView.addObject("motivoCancelamento", MotivoCancelamento.values());
		modelAndView.addObject("tipoNotaFiscal", TipoNotaFiscal.values());

		notaFiscalServiceImpl.delete(notaFiscal);

		modelAndView.setViewName("redirect:/notasFiscais/all");

		return modelAndView;
	}

	@Transactional
	@PostMapping("/update")
	public ModelAndView update(@RequestBody @Valid NotaFiscal notaFiscal) {
		
		ModelAndView modelAndView = new ModelAndView();

		NotaFiscal notaFiscalAtt = notaFiscalServiceImpl.findById(notaFiscal.getId());
		Faturamento faturamentoAtt = faturamentoService.findById(notaFiscal.getFaturamento().getId());
		
//		nao t� salvando a data, mas pega o id certo reencaminhar a tela de exclusao de subprod atributo campanha em nf

		List<Parcela> parcelas = notaFiscal.getParcelas();
		// carregaParcelas(notaFiscal);

		for (Parcela parcela : parcelas) {
			parcela.setNotaFiscal(notaFiscalAtt);
			parcelaService.update(parcela);
		}

		String dataCobranca = notaFiscal.getDataCobranca();
		String dataVencimento = notaFiscal.getDataVencimento();

		parcelas = parcelaService.getParcelasByNF(notaFiscal.getId());

		List<Parcela> parcela = new ArrayList<Parcela>();
		for (Parcela par : parcelas) {
			parcela.add(par);
		}

		for (int i = 0 ; i<parcela.size(); i ++) {
			int tamanhoVetor = parcela.size();

			if (parcela.get(i).isPago() == true) {
				if (i + 1 == tamanhoVetor) {
					dataCobranca = parcela.get(i).getDataCobranca();
					dataVencimento = parcela.get(i).getDataVencimento();
				} else {
					if (parcela.get(i).isPago() == true && parcela.get(i + 1).isPago() == false) {
						dataCobranca = parcela.get(i).getDataCobranca();
						dataVencimento = parcela.get(i + 1).getDataVencimento();
					}
				}
			}
		}

		if (!dataVencimento.isEmpty() && dataVencimento != null) {
			String diaVencimento = dataVencimento.substring(0, 2);
			String mesVencimento = dataVencimento.substring(3, 5);
			String anoVencimento = dataVencimento.substring(6);
			dataVencimento = anoVencimento + "-" + mesVencimento + "-" + diaVencimento;
		} else {
			dataVencimento = null;
		}

		if (dataCobranca != null && !dataCobranca.isEmpty()) {
			String diaCobranca = dataCobranca.substring(0, 2);
			String mesCobranca = dataCobranca.substring(3, 5);
			String anoCobranca = dataCobranca.substring(6);
			dataCobranca = anoCobranca + "-" + mesCobranca + "-" + diaCobranca;
		} else {
			dataCobranca = null;
		}

		String dataEmissao = notaFiscal.getDataEmissao();

		if (!dataEmissao.isEmpty() && dataEmissao != null) {
			String diaEmissao = dataEmissao.substring(0, 2);
			String mesEmissao = dataEmissao.substring(3, 5);
			String anoEmissao = dataEmissao.substring(6);
			dataEmissao = anoEmissao + "-" + mesEmissao + "-" + diaEmissao;
		} else {
			dataEmissao = null;
		}
		
		if(notaFiscal.getCampanha() == faturamentoAtt.getCampanha()) {
			notaFiscalAtt.setCampanha(faturamentoAtt.getCampanha());
			faturamentoAtt.setCampanha(notaFiscal.getCampanha());
		} else {
			notaFiscalAtt.setCampanha(notaFiscal.getCampanha());
			faturamentoAtt.setCampanha(notaFiscal.getCampanha());
		}

		notaFiscalAtt.setRazaoSocialFaturar(notaFiscal.getRazaoSocialFaturar());
		notaFiscalAtt.setCnpjFaturar(notaFiscal.getCnpjFaturar());
		notaFiscalAtt.setFilial(notaFiscal.getFilial());
		notaFiscalAtt.setNumeroNF(notaFiscal.getNumeroNF());
		notaFiscalAtt.setCancelada(notaFiscal.isCancelada());
		notaFiscalAtt.setCobranca(notaFiscal.isCobranca());
		notaFiscalAtt.setAtraso(notaFiscal.isAtraso());
		notaFiscalAtt.setDataEmissao(dataEmissao);
		notaFiscalAtt.setDataVencimento(dataVencimento);
		notaFiscalAtt.setDataCobranca(dataCobranca);
		notaFiscalAtt.setMotivoCancelamento(notaFiscal.getMotivoCancelamento());
		notaFiscalAtt.setIncidencia(notaFiscal.getIncidencia());
		notaFiscalAtt.setFaturar(notaFiscal.getFaturar());
		notaFiscalAtt.setCobrar(notaFiscal.getCobrar());
		notaFiscalAtt.setValorCobranca(notaFiscal.getValorCobranca());
		notaFiscalAtt.setTipoNotaFiscal(notaFiscal.getTipoNotaFiscal());
		notaFiscalAtt.setObsNota(notaFiscal.getObsNota());
		notaFiscalAtt.setObsCobranca(notaFiscal.getObsCobranca());
		notaFiscalAtt.setParcela(parcelas.size());
		notaFiscalAtt.setValorParcela(notaFiscal.getValorParcela());
		notaFiscalAtt.setDataCobrancaParcela(notaFiscal.getDataCobrancaParcela());
		notaFiscalAtt.setDataVencimentoParcela(notaFiscal.getDataVencimentoParcela());
		notaFiscalAtt.setNumeroNotaSubstituida(notaFiscal.getNumeroNotaSubstituida());
		
		Faturamento fat = null;
		if (Objects.nonNull(notaFiscal.getFaturamento())) {
			fat = faturamentoService.findById(notaFiscal.getFaturamento().getId());
		}

		// ACRESCENTA CR�DITO DE SUBMISS�O � PRODU��O DE EX-TARIFARIO
		if (notaFiscalAtt.isCobranca() && notaFiscalAtt.getFaturamento().getProduto().getId() == 28l) {
			if (Objects.nonNull(fat)) {
				if(Objects.nonNull(fat.getEtapa())) {
					if (fat.getEtapa().getId() == 79l) {

						Producao prod = fat.getProducao();

						BigDecimal credito = prod.getCredito();
						System.out.println("o valor da etapa �: " + fat.getValorEtapa());
						credito = credito.add(fat.getValorEtapa());

						prod.setCredito(credito);

						producaoService.update(prod);
					}
				}
			}
		}

		notaFiscalAtt.setParcelas(parcelas);

		notaFiscalServiceImpl.update(notaFiscalAtt);
		
		/* teste */
		System.out.println("notaFiscal.getFaturamento().getId(): " + notaFiscal.getFaturamento().getId());
		System.out.println("notaFiscal.getNumeroNF(): " + notaFiscal.getNumeroNF());
		
		Faturamento faturamento = faturamentoService.findById(notaFiscal.getFaturamento().getId());
		faturamento.setNumeroNF(notaFiscal.getNumeroNF());
		faturamentoService.update(fat);

		/* fimTeste */

		modelAndView.setViewName("redirect:/notasFiscais/" + notaFiscalAtt.getId());

		return modelAndView;
	}

	@PostMapping("/geraNF")
	@ResponseBody
	public void gerarNF(@RequestBody NotaFiscal nf) {
		System.out.println("nf" + nf.getFilial().getNome());
		System.out.println("o valor a cobrar �: " + nf.getCobrar());

		Faturamento fat = faturamentoService.findById(nf.getFaturamento().getId());

		List<Parcela> parcelas = nf.getParcelas();
		if (parcelas != null && parcelas.size() > 0) {
			for (Parcela parcela : parcelas) {
				System.out.println("LOOP DAS PARCELAS " + parcela.getId());
				parcela.setNotaFiscal(nf);
				parcelaService.update(parcela);
			}
		}

		NotaFiscal notaFiscal = new NotaFiscal();
		String dataEmissao = nf.getDataEmissao();

		if (!dataEmissao.isEmpty() && dataEmissao != null) {
			String diaEmissao = dataEmissao.substring(0, 2);
			String mesEmissao = dataEmissao.substring(3, 5);
			String anoEmissao = dataEmissao.substring(6);
			dataEmissao = anoEmissao + "-" + mesEmissao + "-" + diaEmissao;
		} else {
			dataEmissao = null;
		}

		String dataVencimento = nf.getDataVencimento();

		if (!dataVencimento.isEmpty() && dataVencimento != null) {
			String diaVencimento = dataVencimento.substring(0, 2);
			String mesVencimento = dataVencimento.substring(3, 5);
			String anoVencimento = dataVencimento.substring(6);
			dataVencimento = anoVencimento + "-" + mesVencimento + "-" + diaVencimento;
		} else {
			dataVencimento = null;
		}

		notaFiscal.setCategoriaNF(nf.getCategoriaNF());
		notaFiscal.setCliente(fat.getCliente());
		try {
			notaFiscal.setCnpjFaturar(fat.getContrato().getCnpjFaturar());
		} catch (Exception e) {
			e.printStackTrace();
		}

		notaFiscal.setCobrar(nf.getCobrar());
		notaFiscal.setContrato(fat.getContrato());
		// notaFiscal.setDataCobranca(nf.getDataCobranca());
		notaFiscal.setDataEmissao(dataEmissao);
		notaFiscal.setDataVencimento(dataVencimento);
		notaFiscal.setDuplaTributacao(nf.isDuplaTributacao());
		notaFiscal.setFaturamento(fat);
		notaFiscal.setFaturar(nf.getFaturar());
		try {
			//notaFiscal.setFilial(fat.getProducao().getFilial()); 			
			notaFiscal.setFilial(nf.getFilial());
		} catch (Exception e) {
			e.printStackTrace();
		}
		notaFiscal.setISSvariavel(nf.getISSvariavel());
		notaFiscal.setNumeroNF(nf.getNumeroNF());
		notaFiscal.setObsNota(nf.getObsNota());
		notaFiscal.setParcela(nf.getParcela());
		notaFiscal.setPercentVariavel(nf.getPercentVariavel());
		try {
			notaFiscal.setRazaoSocialFaturar(fat.getContrato().getRazaoSocialFaturar());
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			notaFiscal.setTiming(fat.getEtapa().getNome());
		}catch(Exception e) {
			System.out.println("nota sem timing");
		}
		notaFiscal.setTipoNotaFiscal(nf.getTipoNotaFiscal());
		notaFiscal.setValorCobranca(nf.getValorCobranca());
		notaFiscal.setMotivoCancelamento(nf.getMotivoCancelamento());
		notaFiscal.setValorParcela(nf.getValorParcela());
		notaFiscal.setDataCobrancaParcela(nf.getDataCobrancaParcela());
		notaFiscal.setDataVencimentoParcela(nf.getDataVencimentoParcela());
		notaFiscal.setNumeroNotaSubstituida(nf.getNumeroNotaSubstituida());

		notaFiscal.setParcelas(parcelas);

		notaFiscalServiceImpl.update(notaFiscal);
		
		fat.setNumeroNF(nf.getNumeroNF());
		fat.setEstado("EMITIDO");
		faturamentoService.update(fat);
		
	}

	@GetMapping("/{id}/parcelas.json")
	public @ResponseBody List<Parcela> carregaParcelas(@PathVariable(name = "id") Long id) {

		List<Parcela> parcela = parcelaService.getParcelasByNF(id);
		System.out.println(parcela);

		return parcela;
	}

	@GetMapping("/{id}/notasCliente.json")
	public @ResponseBody List<NotaFiscal> getNotasByCliente(@PathVariable("id") Long id) {
		System.out.println("entrou na controller de notas fiscais: " + id);
		return notaFiscalService.getNotasByCliente(id);
	}

	@GetMapping("/{id}/getNota.json")
	public @ResponseBody NotaFiscal getNotaById(@PathVariable("id") Long id) {
		System.out.println("entrou na controller de notas fiscais: " + id);
		return notaFiscalService.findById(id);
	}

	@GetMapping("/details/{id}")
	public ModelAndView detailsNotaFiscal(@PathVariable("id") Long id) throws Exception {

		ModelAndView mav = new ModelAndView();

		NotaFiscal nf = notaFiscalServiceImpl.findById(id);
		List<ContatoCliente> contatos = new ArrayList<ContatoCliente>();
		contatos = contatoClienteService.getContatosCliente(nf.getCliente().getId());

		List<EnderecoCliente> enderecos = new ArrayList<EnderecoCliente>();
		enderecos = enderecoService.getEnderecoClienteNF(nf.getCliente().getId());

		Faturamento fat = faturamentoService.findById(nf.getFaturamento().getId());
		List<Honorario> hon = new ArrayList<Honorario>();
		Contrato contrato = new Contrato();
		PercentualEscalonado percentual = new PercentualEscalonado();

		try {
			contrato = fat.getContrato();
			hon = contrato.getHonorarios();
			percentual = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());

		} catch (Exception e) {
			System.out.println("Sem contrato no carregamento do Fat.");
		}

		mav.addObject("contatos", contatos);
		mav.addObject("enderecos", enderecos);
		mav.addObject("honors", hon);
		mav.addObject("faturamento", fat);

		if (Objects.nonNull(nf.getFaturamento())) {
			Long idFaturamento = nf.getFaturamento().getId();

			Faturamento faturamentoUpp = faturamentoService.findById(idFaturamento);

			List<Faturamento> faturamentos = new ArrayList<Faturamento>();

			if(faturamentoUpp.getProducao()!=null) {
				faturamentos = faturamentoService.getAllByProducao(faturamentoUpp.getProducao().getId());
			}else if(faturamentoUpp.getContrato()!=null) {
				faturamentos = faturamentoService.getAllByContrato(faturamentoUpp.getContrato().getId());
			}
			BigDecimal valorTotal = new BigDecimal("0.0");

			for (Faturamento faturamento : faturamentos) {
				valorTotal = valorTotal.add(faturamento.getValorEtapa());
				System.out.println(faturamento.getValorEtapa());
				System.out.println(valorTotal);

			}

			if (fat.getConsolidado() == true) {
//				List<BalanceteCalculo> balancetes = balanceteCalculoService.getBalanceteCalculoByProducao(faturamento.getProducao().getId());
//				BalanceteCalculo balancete = balanceteCalculoService.();
//				mav.addObject("balancetes", balancetes);
			} else if(fat.getBalancete() != null) {
				BalanceteCalculo balancete = balanceteCalculoService.findById(fat.getBalancete().getId());
				mav.addObject("balancetes", balancete);
			} else if(fat.getBalancete() == null) {
				BalanceteCalculo balancete = null;
				mav.addObject("balancetes", balancete);
			}
			
			mav.addObject("total", valorTotal);

		} else {
			mav.addObject("total", 0);
		}
		
		if(nf.getParcela() > 0) {
			List<Parcela> parcelas = nf.getParcelas();
			mav.addObject("parcelas", parcelas);
		}else {
			List<Parcela> parcelas = new ArrayList();
			mav.addObject("parcelas", parcelas);
		}

		mav.addObject("notaFiscal", notaFiscalServiceImpl.findById(id));
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("incidencia", Incidencia.values());
		mav.addObject("motCancelamento", MotivoCancelamento.values());
		mav.addObject("tipoNotaFiscal", TipoNotaFiscal.values());
		
		mav.setViewName("/notaFiscal/details");

		return mav;
	}

	@GetMapping("/detailsNotaConsolidada/{id}")
	public ModelAndView detailsNotaFiscalConsolidada(@PathVariable("id") Long id) throws Exception {

		ModelAndView mav = new ModelAndView();

		NotaFiscal nf = notaFiscalServiceImpl.findById(id);
		List<ContatoCliente> contatos = new ArrayList<ContatoCliente>();
		contatos = contatoClienteService.getContatosCliente(nf.getCliente().getId());

		List<EnderecoCliente> enderecos = new ArrayList<EnderecoCliente>();
		enderecos = enderecoService.getEnderecoClienteNF(nf.getCliente().getId());

		Faturamento fat = faturamentoService.findById(nf.getFaturamento().getId());
		List<Honorario> hon = new ArrayList<Honorario>();
		Contrato contrato = new Contrato();
		PercentualEscalonado percentual = new PercentualEscalonado();

		try {
			contrato = fat.getContrato();
			hon = contrato.getHonorarios();
			percentual = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());

		} catch (Exception e) {
			System.out.println("Sem contrato no carregamento do Fat.");
		}

		mav.addObject("contatos", contatos);
		mav.addObject("enderecos", enderecos);
		mav.addObject("honors", hon);
		mav.addObject("faturamento", fat);

		if (Objects.nonNull(nf.getFaturamento())) {
			Long idFaturamento = nf.getFaturamento().getId();

			Faturamento faturamentoUpp = faturamentoService.findById(idFaturamento);

			List<Faturamento> faturamentos = new ArrayList<Faturamento>();

			faturamentos = faturamentoService.getAllByProducao(faturamentoUpp.getProducao().getId());

			BigDecimal valorTotal = new BigDecimal("0.0");

			for (Faturamento faturamento : faturamentos) {
				valorTotal = valorTotal.add(faturamento.getValorEtapa());
				System.out.println(faturamento.getValorEtapa());
				System.out.println(valorTotal);

			}

			mav.addObject("total", valorTotal);

		} else {
			mav.addObject("total", 0);
		}

		mav.addObject("notaFiscal", notaFiscalServiceImpl.findById(id));
		mav.addObject("filiais", filialService.getAll());
		mav.addObject("incidencia", Incidencia.values());
		mav.addObject("motCancelamento", MotivoCancelamento.values());
		mav.addObject("tipoNotaFiscal", TipoNotaFiscal.values());
		
		mav.setViewName("/notaFiscal/detailsNotaConsolidada");

		return mav;
	}
	
	@GetMapping("/{id}/contabilizaValorRecebido.json")
	public @ResponseBody String getValorRecebido(@PathVariable("id") Long id) {
		NotaFiscal nfAtual = notaFiscalService.findById(id);
		BigDecimal somatoriaValoresDasParcelas = BigDecimal.ZERO;
		
		if(nfAtual.getParcelas().size()>0) {
			List<Parcela> parcelas = parcelaService.getParcelasPagasByNF(nfAtual.getId());
			
			for (Parcela parcela : parcelas) {
				somatoriaValoresDasParcelas = somatoriaValoresDasParcelas.add(parcela.getValorParcela());
			}
		} else {
			String valorAtual = (nfAtual.getValorCobranca());
			BigDecimal valorSomado = BigDecimal.valueOf(0);
			try {
				valorSomado = valorSomado.add(BigDecimal.valueOf(Float.parseFloat(valorAtual.replace(".", "").replace(",", "."))));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			somatoriaValoresDasParcelas = somatoriaValoresDasParcelas.add(valorSomado);
		}

		return somatoriaValoresDasParcelas.toString();
	}
	
	
	  @Transactional	
	  @PostMapping("/retornarNotaFiscal") 
	  public ModelAndView retornarNotaFiscal(@RequestParam("idNotaFiscal") Long idNotaFiscal) {
	  
	  ModelAndView modelAndView = new ModelAndView(); 
	  NotaFiscal nf = notaFiscalServiceImpl.findById(idNotaFiscal);
	  
	  //pegar o id do faturamento da nota fiscal 
	  long idFaturamento; 
	  idFaturamento= nf.getFaturamento().getId();
	  
	  //acessar o faturamento e colocar o atributo emitido = 0 
	  Faturamento faturamento = faturamentoService.findById(idFaturamento);
	  faturamento.setEmitido(false);
	  
	  //deletando notaFiscal 
	  notaFiscalServiceImpl.delete(nf);
	  
	  modelAndView.setViewName("redirect:/notasFiscais/all");
	  return modelAndView; 
	  }
	 
	  
	  @GetMapping("/{idNotaSubstituida}/notaSubstituida.json")
		public @ResponseBody NotaFiscal getNotaSubstituida(@PathVariable("idNotaSubstituida") String idNotaSubstituida) throws Exception {
		  
		  System.out.println("idNotaSubstituida"+idNotaSubstituida);
		  NotaFiscal notaFiscal = new NotaFiscal();
		  
		  List<NotaFiscal> listaNF = new ArrayList<NotaFiscal>();
		  listaNF = notaFiscalService.getNotaSubstituida(idNotaSubstituida);
		
		 for (NotaFiscal nota : listaNF) {
			 	if(nota.isCancelada() == false) {
			 		long id = nota.getId();
			 		notaFiscal = notaFiscalServiceImpl.findById(id);
			 		System.out.println("notaFiscal"+notaFiscal.getId());
			 	}
		}
		
		return notaFiscal; 
		
	  }
	  
	  @PostMapping("/geraNFSubstituta")
		@ResponseBody
		public void geraNFSubstituta(@RequestBody NotaFiscal nf) {
			System.out.println("nf" + nf);
			System.out.println("o valor a cobrar �: " + nf.getCobrar());

			Faturamento fat = faturamentoService.findById(nf.getFaturamento().getId());

			List<Parcela> parcelas = nf.getParcelas();
			if (parcelas != null && parcelas.size() > 0) {
				for (Parcela parcela : parcelas) {
					System.out.println("LOOP DAS PARCELAS " + parcela.getId());
					parcela.setNotaFiscal(nf);
					parcelaService.update(parcela);
				}
			}

			NotaFiscal notaFiscal = new NotaFiscal();
			String dataEmissao = nf.getDataEmissao();

			if (!dataEmissao.isEmpty() && dataEmissao != null) {
				String diaEmissao = dataEmissao.substring(0, 2);
				String mesEmissao = dataEmissao.substring(3, 5);
				String anoEmissao = dataEmissao.substring(6);
				dataEmissao = anoEmissao + "-" + mesEmissao + "-" + diaEmissao;
			} else {
				dataEmissao = null;
			}

			String dataVencimento = nf.getDataVencimento();

			if (!dataVencimento.isEmpty() && dataVencimento != null) {
				String diaVencimento = dataVencimento.substring(0, 2);
				String mesVencimento = dataVencimento.substring(3, 5);
				String anoVencimento = dataVencimento.substring(6);
				dataVencimento = anoVencimento + "-" + mesVencimento + "-" + diaVencimento;
			} else {
				dataVencimento = null;
			}

			notaFiscal.setCategoriaNF(nf.getCategoriaNF());
			notaFiscal.setCliente(fat.getCliente());
			try {
				notaFiscal.setCnpjFaturar(fat.getContrato().getCnpjFaturar());
			} catch (Exception e) {
				e.printStackTrace();
			}

			notaFiscal.setCobrar(nf.getCobrar());
			notaFiscal.setContrato(fat.getContrato());
			// notaFiscal.setDataCobranca(nf.getDataCobranca());
			notaFiscal.setDataEmissao(dataEmissao);
			notaFiscal.setDataVencimento(dataVencimento);
			notaFiscal.setDuplaTributacao(nf.isDuplaTributacao());
			notaFiscal.setFaturamento(fat);
			notaFiscal.setFaturar(nf.getFaturar());
			try {

				notaFiscal.setFilial(nf.getFilial());
			} catch (Exception e) {
				e.printStackTrace();
			}
			notaFiscal.setISSvariavel(nf.getISSvariavel());
			notaFiscal.setNumeroNF(nf.getNumeroNF());
			notaFiscal.setObsNota(nf.getObsNota());
			notaFiscal.setParcela(nf.getParcela());
			notaFiscal.setPercentVariavel(nf.getPercentVariavel());
			try {
				notaFiscal.setRazaoSocialFaturar(fat.getContrato().getRazaoSocialFaturar());
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {

				notaFiscal.setTiming(fat.getEtapa().getNome());
			}catch(Exception e) {
				System.out.println("nota sem timing");
			}
			notaFiscal.setTipoNotaFiscal(nf.getTipoNotaFiscal());
			notaFiscal.setValorCobranca(nf.getValorCobranca());
			notaFiscal.setMotivoCancelamento(nf.getMotivoCancelamento());
			notaFiscal.setValorParcela(nf.getValorParcela());
			notaFiscal.setDataCobrancaParcela(nf.getDataCobrancaParcela());
			notaFiscal.setDataVencimentoParcela(nf.getDataVencimentoParcela());
			notaFiscal.setNumeroNotaSubstituida(nf.getNumeroNotaSubstituida());

			notaFiscal.setParcelas(parcelas);

			notaFiscalServiceImpl.update(notaFiscal);
		}
}