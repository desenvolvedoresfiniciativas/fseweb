package br.com.finiciativas.fseweb.pojo;

import br.com.finiciativas.fseweb.models.producao.Producao;

public class SeguimentoPojo extends Producao{

	private String regimeTributacao;
	
	private String lucroFiscal;
	
	private String previsaoEntrega;
	
	private String prevValorDispendio;
	
	private String prevValorExclusao;

	public String getRegimeTributacao() {
		return regimeTributacao;
	}

	public void setRegimeTributacao(String regimeTributacao) {
		this.regimeTributacao = regimeTributacao;
	}

	public String getLucroFiscal() {
		return lucroFiscal;
	}

	public void setLucroFiscal(String lucroFiscal) {
		this.lucroFiscal = lucroFiscal;
	}

	public String getPrevisaoEntrega() {
		return previsaoEntrega;
	}

	public void setPrevisaoEntrega(String previsaoEntrega) {
		this.previsaoEntrega = previsaoEntrega;
	}

	public String getPrevValorDispendio() {
		return prevValorDispendio;
	}

	public void setPrevValorDispendio(String prevValorDispendio) {
		this.prevValorDispendio = prevValorDispendio;
	}

	public String getPrevValorExclusao() {
		return prevValorExclusao;
	}

	public void setPrevValorExclusao(String prevValorExclusao) {
		this.prevValorExclusao = prevValorExclusao;
	}
	
	
	
}
