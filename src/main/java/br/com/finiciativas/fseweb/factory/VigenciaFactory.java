package br.com.finiciativas.fseweb.factory;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.finiciativas.fseweb.models.vigencia.Vigencia;
import br.com.finiciativas.fseweb.models.vigencia.VigenciaDeterminada;
import br.com.finiciativas.fseweb.models.vigencia.VigenciaIndeterminada;
import br.com.finiciativas.fseweb.services.impl.VigenciaServiceImpl;

public class VigenciaFactory {
	
	@Autowired
	private VigenciaServiceImpl vigenciaService;

	public Vigencia retornaVigencia(String vigencia) {

		if (vigencia != null) {

			if (vigencia.equals("vigenciaDeterminada")) {
				return new VigenciaDeterminada();
			}

			if (vigencia.equals("vigenciaIndeterminada")) {
				return new VigenciaIndeterminada();
			}

		}

		return null;

	}

//	public Vigencia retornaVigencia(Long id, String nome, String inicioVigencia, 
//			String fimVigencia, String dataEncerramentoContrato){
//		
//		if (nome.equals("Vig�ncia Determinada")) {
//			VigenciaDeterminada vd = new VigenciaDeterminada();
//			vd.setId(id);
//			vd.setInicioVigencia(inicioVigencia);
//			vd.setFimVigencia(fimVigencia);
//			return vd;
//		}else{
//			VigenciaIndeterminada vi = new VigenciaIndeterminada();
//			vi.setId(id);
//			vi.setInicioVigencia(inicioVigencia);
//			vi.setDataEncerramentoContrato(dataEncerramentoContrato);
//			return vi;
//		}
//		
//	}

	public Vigencia retornaVigenciaConstruida(String nome, String inicioVigencia, String fimVigencia,
			String dataEncerramentoContrato) {

		if (nome.equals("vigenciaDeterminada")) {
			VigenciaDeterminada vd = new VigenciaDeterminada();
			vd.setInicioVigencia(inicioVigencia);
			vd.setFimVigencia(fimVigencia);
			return vd;
		} else {
			VigenciaIndeterminada vi = new VigenciaIndeterminada();
			vi.setInicioVigencia(inicioVigencia);
			vi.setDataEncerramentoContrato(dataEncerramentoContrato);
			return vi;
		}

	}

}
