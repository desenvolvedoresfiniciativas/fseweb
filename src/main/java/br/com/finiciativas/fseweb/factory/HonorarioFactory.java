package br.com.finiciativas.fseweb.factory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.finiciativas.fseweb.models.honorario.FaixasEscalonado;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;

public class HonorarioFactory {
	
	public List<Honorario> retornarHonorario(String valorFixo, String percentualFixo, String percentualEscalonado){
		
		List<Honorario> honorarios = new ArrayList<>();
		
		if (valorFixo != null){
			System.out.println("Entrou VF");
			ValorFixo vf = new ValorFixo("Valor Fixo");
			vf.setDataCobrancaAnual("");
			vf.setDataCobrancaMensal(0);
			honorarios.add(vf);
		}
		
		if (percentualFixo != null) {
			System.out.println("Entrou PF");
			honorarios.add(new PercentualFixo("Percentual Fixo"));
		}
		
		if (percentualEscalonado != null) {
			System.out.println("Entrou PE");
			FaixasEscalonado faixasEscalonado = new FaixasEscalonado();
			faixasEscalonado.setValorPercentual(null);
			faixasEscalonado.setValorInicial(null);
			faixasEscalonado.setValorFinal(null);
			List<FaixasEscalonado> list = new ArrayList<>();
			list.add(faixasEscalonado);
			PercentualEscalonado percentualEscalonadoAdd = new PercentualEscalonado("Percentual Escalonado");
			percentualEscalonadoAdd.setFaixasEscalonamento(list);
			percentualEscalonadoAdd.setLimitacao(BigDecimal.valueOf(0.0));
			honorarios.add(percentualEscalonadoAdd);
		}
		
		return honorarios;
		
	}
	
}
