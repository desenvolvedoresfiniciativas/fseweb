package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeFiname {
	SEM_INTERESSE("N�o tem Interesse"),
	IRREGULARIDADE_FISCAL("Irregularidade fiscal"),
	NAO_EMPRESA_INDUSTRIAL_ENG("N�o ser uma empresa do setor Industrial ou Engenharia"),
	ESTABELECIMENTO_FORA_BRASIL("Estabelecimento fora do Brasil"),
	SEM_COMPROVACAO_FABRI_LOCAL("N�o comprova��o de fabrica��o local"),
	SEM_RETORNO("Sem retorno");
	
	
	private String displayName;

	private MotivoInatividadeFiname(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
