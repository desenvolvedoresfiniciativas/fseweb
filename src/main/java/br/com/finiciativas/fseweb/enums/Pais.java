package br.com.finiciativas.fseweb.enums;

public enum Pais {

	BRASIL("Brasil"),
	CHILE("Chile"),
	COLOMBIA("Colombia");
	
	private String pais;
	
	private Pais(String pais) {
		this.pais = pais;
	}
	
	public String displayName(){
		return this.pais.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.pais.toUpperCase();
	}
	
}
