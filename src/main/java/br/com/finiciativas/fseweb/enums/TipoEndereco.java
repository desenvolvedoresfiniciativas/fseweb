package br.com.finiciativas.fseweb.enums;

public enum TipoEndereco {
	
	MATRIZ("Matriz"),
	ESCRITORIO("Escritorio"),
	FABRICA("F�brica");
	;
	
	private String tipoEndereco;
	
	private TipoEndereco(String tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}
	
	public String displayName(){
		return this.tipoEndereco.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.tipoEndereco.toUpperCase();
	}

}
