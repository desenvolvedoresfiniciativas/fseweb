package br.com.finiciativas.fseweb.enums;

public enum TipoCampo {
	TEXTO("Texto"),
	TEXTOLONGO("Texto Longo"),
	DINHEIRO("Dinheiro $"),
	DATA("Data"),
	CHECK("Check"),
	LISTA("Lista de Valores"),
	LISTA_MULTI("Lista de sele��o multipla"),
	LISTA_CONSULTOR("Lista de Consultor"),
	PORCENTAGEM("Porcentagem");
	
	private String tipoCampo;

	private TipoCampo(String tipoCampo) {
		this.tipoCampo = tipoCampo;
	}

	public String displayName(){
		return this.tipoCampo.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.tipoCampo.toUpperCase();
	}
}
