package br.com.finiciativas.fseweb.enums;

public enum MotivoCancelamento {
    
	NONE(""),
    NOTA_RECUSADA_POR_ERRO("NOTA RECUSADA POR ERRO"),
    EMISSAO_FORA_DO_PERIODO_DE_FATURAMENTO("EMISS�O FORA DO PER�ODO DE FATURAMENTO"),
    ERRO_DE_CALCULO("ERRO DE C�LCULO"),
    DADOS_INCORRETOS("DADOS INCORRETOS"),
    DATA_DE_FECHAMENTO_EXPIRADA("DATA DE FECHAMENTO EXPIRADA"),
    CNPJ_INCORRETO("CNPJ INCORRETO"),
    OUTRO("OUTRO");
    
    private String motivoCancelamento;
    
    private MotivoCancelamento(String motivoCancelamento) {
        this.motivoCancelamento = motivoCancelamento;
    }
    
    public String displayName(){
        return this.motivoCancelamento.toUpperCase();
    }
    
    @Override
    public String toString() {
        return this.motivoCancelamento.toUpperCase();
    }
    
}