package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividade {
	//LEI DO BEM || LB RETROATIVA
	NONE(""),
	PREJUIZO_FISCAL("Preju�zo Fiscal"),
	LUCRO_PRESUMIDO("Lucro Presumido"),
	SEM_PED("N�o tem P&D"),
	SEM_CND("N�o tem CND"),
	SEM_INTERESSE("N�o tem Interesse"),
	REC_JUDICIAL("Recupera��o Judicial"),
	SEM_CONTATO("Sem Contato"),
	SEM_POTENCIAL_IMPORTACAO("Sem potencial para importa��o"),
	PROD_IMPOTADO_N�O_BIT_BK("Produtos importados que n�o se classificam como BIT/BK"),
	COMPROVACAO_SIM_NASC("Comprova��o de similar nacional"),
	SEM_ATENDIMENTO_CPC("N�o atendimento a CPC-04"),
	SEM_OBRIGACAO_AUD_ESPECIFICA("Sem obriga��o de realizar auditoria espec�fica(teto 10 milh�es)"),
	IRREGULARIDADE_FISCAL("Irregularidade fiscal"),
	NAO_EMPRESA_INDUSTRIAL_ENG("N�o ser uma empresa do setor Industrial ou Engenharia"),
	ESTABELECIMENTO_FORA_BRASIL("Estabelecimento fora do Brasil"),
	SEM_COMPROVACAO_FABRI_LOCAL("N�o comprova��o de fabrica��o local"),
	SEM_RETORNO("Sem retorno"),
	SEM_PROJ_LINHA_FINANCIAMENTO("Sem projetos enquadrav�is nas linhas  de Financiamento"),
	SEM_GARANTIA_DISP("N�o possui garantias dispon�veis"),
	INDICADORES_FIN_NEGATIVOS("Indicadores financeiros negativos"),
	REVISAO_COMERCIAL("Em revis�o comercial"),
	SEM_OPORTUNIDADES_CRITERIOS("Sem oportunidade para atingir os crit�rios"),
	CADASTRO_NAO_APROVADO("Cadastro n�o aprovado pelo CNPq"),
	SEM_COMP_NASC("N�o comprova��o de similar nacional"),
	SEM_ATENDIMENTO_FISCAIS("N�o atendimento aos pr�-requisitos fiscais"),
	SEM_PERFIL("N�o possui perfil"),
	SEM_PROJ_TIC("Sem projetos eleg�veis de TIC"),
	SEM_CUMPRIMENTO_PPB("N�o cumprimento do PPB"),
	SEM_POTENCIAL_HAB_PROD("Sem potencial para Habilita��o do produto"),
	SEM_PROJ_PED_AUTOMOTIVO("N�o realiza projetos de P&D no setor automotivo"),
	SEM_HABILITACAO_PROG("N�o realizou Habilita��o no programa"),
	SEM_INDUSTRIALIZACAO_EQUIP_IMPORT("N�o realiza industrializa��o dos equipamentos importados"),
	SEM_IMPORT_AUTO("N�o realiza importa��es de autope�as eleg�veis"),
	PERDA_PRAZO_ENVIO("Perda dos prazos de envio do pleito");
	
	private String displayName;

	private MotivoInatividade(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
