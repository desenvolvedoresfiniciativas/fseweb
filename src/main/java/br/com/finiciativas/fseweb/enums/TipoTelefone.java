package br.com.finiciativas.fseweb.enums;

public enum TipoTelefone {
	
	CELULAR, 
	FIXO
}
