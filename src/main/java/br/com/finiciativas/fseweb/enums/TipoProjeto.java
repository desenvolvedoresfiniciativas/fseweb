package br.com.finiciativas.fseweb.enums;

public enum TipoProjeto {
	INTERNO("Interno"),
	ICT_PUBLICO_NE("ICT P�blico NE"),
	ICT_PRIVADO_NE("ICT Privado NE"),
	OUTROS_ICTS("Outros ICTs");
	
	private String tipoProjeto;

	private TipoProjeto(String tipoProjeto) {
		this.tipoProjeto = tipoProjeto;
	}

	public String displayName(){
		return this.tipoProjeto.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.tipoProjeto.toUpperCase();
	}
	
}
