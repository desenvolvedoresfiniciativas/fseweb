package br.com.finiciativas.fseweb.enums;

public enum SubMotivo {
	
	FINALIZACAO_DA_ETAPA_EM_ABERTO("Finaliza��o da etapa em aberto"),
	EM_NEGOCIACAO("Em negocia��o"),
	EM_AVALIACAO_DE_PERDA("Em avalia��o de perda"),
	AGUARDANDO_CLIENTE("Aguardando Cliente"),
	AGUARDANDO_MCTI("Aguardando MCTI"),
	AGUARDANDO_INT("Aguardando INT"),
	AGUARDANDO_ENVIO_DO_PLEITO_OU_BALANCETE_FINAL("Aguardando envio do pleito/balancete final"),
	EM_ANALISE("Em an�lise");
	
	private String subMotivo;
	
	private SubMotivo(String subMotivo) {
		this.subMotivo = subMotivo;
	}
	
	public String displayName(){
		return this.subMotivo.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.subMotivo.toUpperCase();
	}
	
}
