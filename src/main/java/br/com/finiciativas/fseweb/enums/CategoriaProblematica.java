package br.com.finiciativas.fseweb.enums;

public enum CategoriaProblematica {

	BAIXA("Baixa"),
	MEDIA("M�dia"),
	ALTA("Alta");
	
	public String string;
	
	private CategoriaProblematica(String situacao) {
		this.string = situacao;
	}
	
	public String displayName(){
		return this.string.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.string.toUpperCase();
	}
	
}
