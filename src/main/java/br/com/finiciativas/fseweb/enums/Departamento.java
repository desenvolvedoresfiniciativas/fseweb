package br.com.finiciativas.fseweb.enums;

public enum Departamento {
	

	CONSULTORIA_TECNICA("Consultoria T�cnica"),
	CONSULTORIA_COMERCIAL("Consultoria Comercial"),
	JURIDICO("Jur�dico"),
	INOVA�AO_E_MARKETING("Inova��o e Marketing"),
	QUALIDADE_E_MAUTENCAO("Qualidade e Manuten��o"),
	RH("RH"),
	CORPORATIVO("Corporativo"),
	TECNOLOGIA("fi tech"),
	FINANCEIRO("Financeiro"),
	CONTROLADORIA("Controladoria"),
	OPERACOES("Opera��es"),
	DIRETORIA("Diretoria"),
	NEGOCIOS("neg�cios"),
	CONTROLE_E_PLANEJAMENTO("Controle e planejamento"),
	PROGRAMA_SPEED("Programa Speed");
	
	
	private String departamento;
	
	private Departamento (String departamento) {
		this.departamento = departamento;
	}
	
	public String displayName(){
		return this.departamento.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.departamento.toUpperCase();
	}
}
