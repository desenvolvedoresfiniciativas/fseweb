package br.com.finiciativas.fseweb.enums;

public enum Atividade {
	
	FEITO_POR("Feito por"),
	CORRIGIDO_POR("Corrigido por"),
	VALIDADO_POR("Validado por");
	
	private String atividade;
	
	private Atividade(String atividade) {
		this.atividade = atividade;
	}
	
	public String displayName(){
		return this.atividade.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.atividade.toUpperCase();
	}

}
