package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeImportInov {
	SEM_INTERESSE("N�o tem Interesse"),
	IRREGULARIDADE_IMPORT_UTI("Irregularidade na importa��o ou utiliza��o dos bens importados"),
	CADASTRO_NAO_APROVADO("Cadastro n�o aprovado pelo CNPq"),
	SEM_PED("N�o tem P&D"),
	SEM_COMP_NASC("N�o comprova��o de similar nacional"),
	SEM_ATENDIMENTO_FISCAIS("N�o atendimento aos pr�-requisitos fiscais"),
	SEM_RETORNO("Sem retorno");
	
	private String displayName;

	private MotivoInatividadeImportInov(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
