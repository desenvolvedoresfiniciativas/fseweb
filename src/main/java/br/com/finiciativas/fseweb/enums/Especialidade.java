package br.com.finiciativas.fseweb.enums;

public enum Especialidade {

	TI("T.I"), 
	IND("IND"),
	QUIM("QUIM");
	
	private String displayName;

	private Especialidade(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
