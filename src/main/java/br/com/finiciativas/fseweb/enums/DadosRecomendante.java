package br.com.finiciativas.fseweb.enums;

public enum DadosRecomendante {
	
	TELEFONE("Telefone"),
	E_MAIL("E-mail"),
	SITE("Site"),
	MANUAL_DE_FRASCATI("Manual de Frascati"),
	OUTROS("Outros");
	
private String dadosRecomendante;
	
	private DadosRecomendante(String dadosRecomendante) {
		this.dadosRecomendante = dadosRecomendante;
	}
	
	public String displayName(){
		return this.dadosRecomendante.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.dadosRecomendante.toUpperCase();
	}

}
