package br.com.finiciativas.fseweb.enums;

public enum SituacaoCO {

	ATIVA("Ativa"), 
	INATIVA("Inativa"), 
	PREVISAO_ATIVA("Previs�o Ativa"), 
	PREVISAO_INATIVA("Previs�o Inativa");
	
	public String situacaoCO;

	private SituacaoCO(String situacaoCO) {
		this.situacaoCO = situacaoCO;
	}

	public String displayName() {
		return this.situacaoCO.toUpperCase();
	}

	@Override
	public String toString() {
		return this.situacaoCO.toUpperCase();
	}

}
