package br.com.finiciativas.fseweb.enums;

public enum FilialExterna {
	
	ESPANHA("Espanha"),
	FRAN�A("Fran�a"),
	REINO_UNIDO("Reino Unido"),
	PORTUGAL("Portugal"),
	BELGICA("B�lgica"),
	BRASIL("Brasil"),
	CHILE("Chile"),
	CANADA("Canad�"),
	ITALIA("It�lia"),
	PERU("Peru"),
	HOLANDA("Holanda"),
	COLOMBIA("Col�mbia"),
	MEXICO("M�xico"),
	ARGENTINA("Argentina"),
	OUTROS("Outros");
	
	private String filialExterna;
	
	private FilialExterna(String filialExterna) {
		this.filialExterna = filialExterna;
	}
	
	public String displayName(){
		return this.filialExterna.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.filialExterna.toUpperCase();
	}
	
}
