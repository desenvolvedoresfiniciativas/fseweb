package br.com.finiciativas.fseweb.enums;

public enum FormaPagamento {
	
	BOLETO("Boleto Banc�rio"), 
	DEPOSITO("Dep�sito Banc�rio");

	private String displayName;

	private FormaPagamento(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
