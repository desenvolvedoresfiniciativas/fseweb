package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeRDA {
	//ASSESSORIA LEI DA INFORM�TICA || ESCRITA RDA || DEFESA RDA
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_RETORNO("Sem retorno");
	
	private String displayName;

	private MotivoInatividadeRDA(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
