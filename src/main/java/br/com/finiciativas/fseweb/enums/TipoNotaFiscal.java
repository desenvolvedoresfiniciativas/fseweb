package br.com.finiciativas.fseweb.enums;

public enum TipoNotaFiscal {

	CLIENTE("CLIENTE"),
	FORNECEDOR("FORNECEDOR"),
	SUBSTITUIDA("SUBSTITUIDA"),
	SUBSTITUIDA_E_CANCELADA("SUBSTITUIDA E CANCELADA");
	
	private String tipoNotaFiscal;
	
	private TipoNotaFiscal(String tipoNotaFiscal) {
		this.tipoNotaFiscal = tipoNotaFiscal;
	}
	
	public String displayName(){
		return this.tipoNotaFiscal.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.tipoNotaFiscal.toUpperCase();
	}
	
}
