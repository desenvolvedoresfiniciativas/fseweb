package br.com.finiciativas.fseweb.enums;

public enum Aprovacao {

	NOVA("0 - Nova"),
	APROVADO_BCS("1 - Aprovado BCS"),
	REPROVADO_BCS("2 - Reprovado BCS"),
	APROVADO_COMERCIAL("3 - Aprovado Comercial"),
	REPROVADO_COMERCIAL("4 - Reprovado Comercial");
	
	private String aprovacao;
	
	private Aprovacao(String aprovacao) {
		this.aprovacao = aprovacao;
	}
	
	public String displayName(){
		return this.aprovacao.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.aprovacao.toUpperCase();
	}
	
}
