package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeFinanciamento {

	SEM_INTERESSE("N�o tem Interesse"),
	SEM_RETORNO("Sem retorno"),
	SEM_PROJ_LINHA_FINANCIAMENTO("Sem projetos enquadrav�is nas linhas  de Financiamento"),
	SEM_GARANTIA_DISP("N�o possui garantias dispon�veis"),
	INDICADORES_FIN_NEGATIVOS("Indicadores financeiros negativos"),
	REVISAO_COMERCIAL("Em revis�o comercial"),
	IRREGULARIDADE_FISCAL("Irregularidade fiscal");
	
	private String displayName;

	private MotivoInatividadeFinanciamento(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
