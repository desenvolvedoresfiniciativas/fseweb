package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeLB {
	//LB PREJUIZO || LB DEFESA || AUDITORIA LB
	SEM_INTERESSE("N�o tem Interesse"),
	LUCRO_PRESUMIDO("Lucro Presumido"),
	SEM_PED("N�o tem P&D"),
	REC_JUDICIAL("Recupera��o Judicial"),
	SEM_RETORNO("Sem retorno");
	
	private String displayName;

	private MotivoInatividadeLB(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
