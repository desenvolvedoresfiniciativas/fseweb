package br.com.finiciativas.fseweb.enums;

public enum Estado {

	PENDENTE("Pendente"), 
	EM_REDA��O("Em reda��o"),
	EM_CORRECAO("Em corre��o"),
	COMPLETO("Completo"),
	EM_REVISAO("Em revis�o"),
	CLIENTE("Cliente"),
	APROVADO("Aprovado");
	
	private String displayName;

	private Estado(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
