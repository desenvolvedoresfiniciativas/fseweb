package br.com.finiciativas.fseweb.enums;

public enum TipoInstituicao {
	
	APORTADOR("Aportador"),
	ICT("ICT"),
	OUTROS("Outros");
	
	private String displayName;

	private TipoInstituicao(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
