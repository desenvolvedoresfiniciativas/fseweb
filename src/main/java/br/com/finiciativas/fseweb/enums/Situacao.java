package br.com.finiciativas.fseweb.enums;

public enum Situacao {

	ATIVA("Ativa"), 
	INATIVA("Inativa"), 
	PREVISAO_ATIVA("Previs�o Ativa"), 
	PREVISAO_INATIVA("Previs�o Inativa"),
	NEGOCIACAO("Negocia��o"),
	LB_PREJUIZO("LB Preju�zo");
	
	public String situacao;

	private Situacao(String situacao) {
		this.situacao = situacao;
	}

	public String displayName() {
		return this.situacao.toUpperCase();
	}

	@Override
	public String toString() {
		return this.situacao.toUpperCase();
	}

}
