package br.com.finiciativas.fseweb.enums;

public enum TipoDeApuracao {
	
	MENSAL("Mensal"),
	TRIMESTRAL("Trimestral"),
	ANUAL("Anual");
	
public String tipoDeApuracao;
	
	private TipoDeApuracao(String tipoDeApuracao) {
		this.tipoDeApuracao = tipoDeApuracao;
	}
	
	public String displayName(){
		return this.tipoDeApuracao;
	}
	
	@Override
	public String toString() {
		return this.tipoDeApuracao;
	}
	
	

}
