package br.com.finiciativas.fseweb.enums;

public enum Resultado {

	APROVADO("Aprovado"),
	GLOSA("Glosa"),
	APROVACAO_PARCIAL("Aprova��o Parcial");
	
	public String string;
	
	private Resultado(String situacao) {
		this.string = situacao;
	}
	
	public String displayName(){
		return this.string.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.string.toUpperCase();
	}
	
}
