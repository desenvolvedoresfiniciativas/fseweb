package br.com.finiciativas.fseweb.enums;

public enum Divisao {
	
	TI("T.I"),
	QUIMICA("Qu�mica"),
	INDUSTRIAL("Industrial");
	
private String setorAtividade;
	
	private Divisao(String setorAtividade) {
		this.setorAtividade = setorAtividade;
	}

	public String displayName(){
		return this.setorAtividade.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.setorAtividade.toUpperCase();
	}

}
