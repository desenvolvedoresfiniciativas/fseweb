package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeCVM {
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_RETORNO("Sem retorno"),
	SEM_OBRIGACAO_AUD_ESPECIFICA("Sem obriga��o de realizar auditoria espec�fica(teto 10 milh�es)");
	
	private String displayName;

	private MotivoInatividadeCVM(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
