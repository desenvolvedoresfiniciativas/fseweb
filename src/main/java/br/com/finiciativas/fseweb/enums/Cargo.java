package br.com.finiciativas.fseweb.enums;

import br.com.finiciativas.fseweb.models.CalcularPremio;

public enum Cargo implements CalcularPremio{
	
	ADMINISTRATIVO("Analista Administrativo"),
	ANALISTA_COMERCIAL("Analista Comercial"),
	ANALISTA_CRM("Analista de CRM"),
	ANALISTA_DE_COMUNICACAO("Analista De Comunica��o"),
	ANALISTA_DE_CONTROLADORIA("Analista De Controladoria"),
	ANALISTA_DE_CRM("Analista De CRM"),
	ANALISTA_DE_IMPORTA��O("Analista De Importa��o"),
    ANALISTA_DE_INFRAESTRUTURA("Analista De Infraestrutura"),
    ANALISTA_DE_INTELIGENCIA_TRIBUTARIA("Analista De Intelig�ncia Tribut�ria"),
    ANALISTA_DE_MARKETING("Analista De Marketing"),
    ANALISTA_DE_MARKETING_DIGITAL("Analista De Marketing Digital"),
    ANALISTA_DE_NOVOS_NEGOCIOS("Analista De Novos Neg�cios"),
    ANALISTA_DE_PLANEJAMENTO_DO_NEGOCIO("Analista de planejamento do neg�cio"),
    ANALISTA_DE_PROCESSOS_DE_RH("Analista De Processos De RH"),
    ANALISTA_DE_PRODUTOS("Analista De Produtos"),
    ANALISTA_DE_QUALIDADE("Analista De Qualidade"),
    ANALISTA_DE_RH("Analista De RH"),
    ANALISTA_DE_SEGURANCA_DA_INFORMACAO("Analista De Seguran�a Da Informa��o"),
    ANALISTA_DE_SERVI�OS("Analista De Servi�os"),
    ANALISTA_DE_SISTEMA("Analista De Sistema"),
    ANALISTA_FINANCEIRO("Analista Financeiro"),
    ANALISTA_JURIDICO("Analista Juridico"),
	ANALISTA_MARKETING("Analista de Marketing"),
	ANALISTA_RH("Analista de Recursos Humanos"),
	ANALISTA_TI("Analista de T.I"),
    ASSESSOR_COMERCIAL("Assessor Comercial"),
    ASSESSOR_DE_NOVOS_NEGOCIOS("Assessor De Novos Neg�cios"),
    ASSISTENTE_DE_CONSULTORIA("Assistente De Consultoria"),
    ASSISTENTE_DE_INOVACAO("Assistente De Inova��o"),
    ASSISTENTE_DE_RH("Assistente De RH"),
    ASSISTENTE_DE_TI("Assistente De TI"),
    ASSISTENTE_JURIDICO("Assistente Jur�dico"),
    AUXILIAR_DE_SERVICOS_GERAIS("Auxiliar De Servi�os Gerais"),
	CONSULTOR_COMERCIAL("Consultor Comercial"),
	CONSULTOR_COMERCIAL_JUNIOR_3("Consultor Comercial Junior 3"),
    CONSULTOR_DE_RELACIONAMENTO_CORPORATIVO("Consultor De Relacionamento Corporativo"),
    CONSULTOR_DE_RH("Consultor De RH"),
    CONSULTOR_ESPECIALISTA("Consultor Especialista"),
    CONSULTOR_LIDER("Consultor L�der"),
	CONSULTOR_LIDER_TECNICO("Consultor L�der T�cnico"),
    CONSULTOR_TECNICO("Consultor T�cnico")
	{@Override public float calcularPremio2021(float salario, float percentualCalculado) {
		System.out.println("ENTROU NO M�TODO CONSULTOR T�CNICO PELO ENUM");
		return 0;
	}public float calculaPremio2022() {
		return 0;
		}
	},
    CONSULTOR_TECNICO_TRAINEE("Consultor T�cnico Trainee"),
	CONSULTOR_TRAINEE("Consultor Trainee")
	{public float calculaPremio2021(float salario, float percentualCalculado) {
		System.out.println("ENTROU NO M�TODO CONSULTOR TREINEE PELO ENUM");
		return 0;
	} public float calculaPremio2022() {
			return 0;
		}	
	},
    COORDENADOR_ADM_E_FINANCEIRO("Coordenador Adm E Financeiro"),
    COORDENADOR_COMERCIAL("Coordenador Comercial"),
    COORDENADOR_DE_CEP("Coordenador de C&P"),
    COORDENADOR_DE_INOVACAO("Coordenador De Inova��o"),
    COORDENADOR_DE_OPERACOES("Coordenador De Opera��es"),
    COORDENADOR_DE_PROJETOS("Coordenador De Projetos"),
    COORDENADOR_DE_RH("Coordenador De RH"),
    COORDENADOR_DE_SERVICOS("Coordenador De Servi�os"),
    COORDENADOR_FINANCEIRO("Coordenador Financeiro"),
    COORDENADOR_JURIDICO("Coordenador Jur�dico"),
	COORDENADOR_MARKETING("Coordenador de Marketing"),
	COORDENADOR_TECNICO("Coordenador(a) T�cnico(a)"),
    DIRETOR_DE_CONTAS_INTERNACIONAIS_E_CORPORATIVAS("Diretor De Contas Internacionais E Corporativas"),
    DIRETOR_DE_INOVACAO_E_MARKETING("Diretor De Inova��o E Marketing"),
    DIRETOR_DE_NEGOCIO("Diretor De Neg�cio"),
    DIRETOR_DE_OPERACOES("Diretor De Opera��es"),
    DIRETOR_EXECUTIVO("Diretor Executivo"),
	DIRETORIA("Diretoria"),
	ESPECIALISTA_COMERCIAL("Especialista Comercial"),
    ESPECIALISTA_DE_CONTROLADORIA("Especialista De Controladoria"),
	ESPECIALISTA_DE_PRODUTOS("Especialista de Produtos"),
    ESPECIALISTA_TECNICO("Especialista T�cnico"),
	ESTAGIARIO_ADMINISTRATIVO("Estagi�rio Administrativo"),
	ESTAGIARIO_DECEP("Estagi�rio de C&P"),
	ESTAGIARIO_RH("Estagi�rio Recursos Humanos"),
	ESTAGIARIO_SPEED("Estagi�rio Speed"),
	ESTAGIARIO_TECNICO("Estagi�rio T�cnico"),
	ESTAGIARIO_TI("Estagi�rio T.I"),
    GERENTE_ADM_FINANCEIRO("Gerente Adm Financeiro"),
	GERENTE_CONSULTORIA_TECNICA("Gerente de Consultoria T�cnica"),
    GERENTE_CORPORATIVO("Gerente Corporativo"),
	GERENTE_CORPORATIVO_NEGOCIOS("Gerente Corporativo de Neg�cios"),
    GERENTE_DE_CONTAS_ESTRATEGICAS("Gerente De Contas Estrat�gicas"),
    GERENTE_DE_FINANCEIRO_E_CONTROLADORIA("Gerente De Financeiro E Controladoria"),
    GERENTE_DE_INOVACAO("Gerente De Inova��o"),
    GERENTE_DE_MARKETING("Gerente De Marketing"),
    GERENTE_DE_NEGOCIOS("Gerente De Neg�cios"),
    GERENTE_DE_RH("Gerente De RH"),
    GERENTE_GERAL("Gerente Geral"),
	GERENTE_KAM("Gerente de Contas Estrat�gicas (KAM)"),
	GERENTE_REGIONAL("Gerente Regional"),
    LIDER_DE_INFRAESTRUTURA_DE_TI("L�der De Infraestrutura De TI"),
    LIDER_DE_SISTEMA_DE_INFORMACAO("L�der De Sistema De Informa��o"),
    LIDER_DE_TRANSFORMACAO_DIGITAL("L�der De Transforma��o Digital"),
    LIDER_REGIONAL("L�der Regional"),
	LIDER_TECNICO("L�der T�cnico"),
	SUPERVISOR_ANALISTA_COMERCIAL("Supervisor Analista Comercial"),
    SUPERVISOR_DE_ASSESSORIA_COMERCIAL("Supervisor De Assessoria Comercial");
	
	private String cargo;
	
	private Cargo(String cargo) {
		this.cargo = cargo;
	}
	


	public String displayName(){
		return this.cargo.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.cargo.toUpperCase();
	}



	@Override
	public float calcularPremio2021(float salario, float percentualCalculado) {
		// TODO Auto-generated method stub
		return 0;
	}




	
}
