package br.com.finiciativas.fseweb.enums;

public enum SetorAtividade {
	
	AGROINDUSTRIA("Agroindustria"),
	ALIMENTOS("Alimentos"),
	BENS_DE_CONSUMO("Bens de Consumo"),
	CONSTRUCAO_CIVIL("Constru��o Civil"),
	ELETROELETRONICA("Eletroeletr�nica"),
	FARMACEUTICA("Farmac�utica"),
	MECANICA("Mec�nica"),
	TRANSPORTE("Transporte"),
	METALURGICA("Metal�rgica"),
	MECANICA_E_TRANSPORTES("Mec�nica e Transportes"),
	MOVELEIRA("Moveleira"),
	PAPEL_CELULOSE("Papel e Celulose"),
	PETROQUIMICA("Petroqu�mica"),
	QUIMICA("Qu�mica"),
	SOFTWARE("Software"),
	TELECOMUNICACAO("Telecomunica��o"),
	TEXTIL("T�xtil"),
	OUTRAS_INDUSTRIAS("Outras Industrias");
	
	private String setorAtividade;
	
	private SetorAtividade(String setorAtividade) {
		this.setorAtividade = setorAtividade;
	}

	public String displayName(){
		return this.setorAtividade.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.setorAtividade.toUpperCase();
	}
	
}
