package br.com.finiciativas.fseweb.enums;

public enum Cobrado {
	SIM("Sim"), 
	NAO("N�o"),
	CONFERIR("Conferir"),
	EM_VALIDACAO("Em valida��o");
	
	private String cobrado;
	

	private Cobrado(String cobrado) {
		this.cobrado = cobrado;
	}
	
	public String displayName(){
		return this.cobrado.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.cobrado.toUpperCase();
	}
	
}
