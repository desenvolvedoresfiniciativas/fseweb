package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeRotaII {
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_PROJ_PED_AUTOMOTIVO("N�o realiza projetos de P&D no setor automotivo"),
	SEM_PERFIL("N�o possui perfil"),
	IRREGULARIDADE_FISCAL("Irregularidade fiscal"),
	SEM_HABILITACAO_PROG("N�o realizou Habilita��o no programa");
	
	
	private String displayName;

	private MotivoInatividadeRotaII(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	

}


