package br.com.finiciativas.fseweb.enums;

public enum CategoriaConsultor {
	
	JUNIOR("Junior"),
	PLENO("Pleno"),
	SENIOR("Senior");
	
	private String categoriaConsultor;
	
	private CategoriaConsultor(String categoriaConsultor) {
		this.categoriaConsultor = categoriaConsultor;
	}
	
	public String displayName(){
		return this.categoriaConsultor.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.categoriaConsultor.toUpperCase();
	}

}
