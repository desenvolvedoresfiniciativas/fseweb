package br.com.finiciativas.fseweb.enums;

public enum EstadoEtapa {
	
	AGUARDANDO_ETAPA("Aguardando Finalização da Etapa"),
	AGUARDANDO_DATA("Aguardando Data de Liberação"),
	ETAPA_FINALIZADA("Etapa Operacional Finalizada"),
	COMUNICACAO_NF("Comunicacao NF ao Cliente"),
	EMISSAO("Em Processo de Emissão"),
	EMITIDO("Emitido"),
	SUBSTITUIDA("Substituída"),
	CONSOLIDADO("Consolidado"),
	EM_NEGOCIACAO("Em Negociação"),
	CANCELADO("Cancelado"),
	INTERROMPIDO_CONSULTOR("Interrompido pelo Consultor"),
	INTERROMPIDO_ADM("Interrompido pelo ADM"),
	PERDIDO("Perdido");
	
	private String estadoEtapa;
	
	private EstadoEtapa(String estadoEtapa) {
		this.estadoEtapa = estadoEtapa;
	}
	
	public String displayName(){
		return this.estadoEtapa.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.estadoEtapa.toUpperCase();
	}
	
}
