package br.com.finiciativas.fseweb.enums;

public enum FormaPagamentoCL {
	
	TRANSFERENCIA("Transferencia Bancaria"),
	CHEQUE("Cheque"),
	VALE_VISTA("Vale Vista");

	private String displayName;

	private FormaPagamentoCL(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
