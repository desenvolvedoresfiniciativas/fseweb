package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeLI {
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_RETORNO("Sem retorno"),
	SEM_PERFIL("N�o possui perfil"),
	IRREGULARIDADE_FISCAL("Irregularidade fiscal"),
	SEM_PROJ_TIC("Sem projetos eleg�veis de TIC"),
	SEM_CUMPRIMENTO_PPB("N�o cumprimento do PPB"),
	SEM_POTENCIAL_HAB_PROD("Sem potencial para Habilita��o do produto");
	
	
	private String displayName;

	private MotivoInatividadeLI(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
