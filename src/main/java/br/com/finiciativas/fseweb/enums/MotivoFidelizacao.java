package br.com.finiciativas.fseweb.enums;

public enum MotivoFidelizacao {
	
	NONE(""),
	ACT("ACT"),
	REFERENCIA("REFERÊNCIA"),
	ELOGIOS("ELOGIOS"),
	RECO("RECO");
	
	private String motivoFidelizacao;
	
	private MotivoFidelizacao(String motivoFidelizacao) {
		this.motivoFidelizacao = motivoFidelizacao;
	}
	
	public String displayName(){
		return this.motivoFidelizacao.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.motivoFidelizacao.toUpperCase();
	}
}
