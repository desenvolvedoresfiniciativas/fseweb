package br.com.finiciativas.fseweb.enums;

public enum SituacaoReco {

	PARADA("Parada"),
	CURSO("Em curso"),
	CONSEGUIDA("Conseguida"),
	PERDIDA("Perdida");
	
	private String situacaoReco;
	
	private SituacaoReco(String situacaoReco) {
		this.situacaoReco = situacaoReco;
	}
	
	public String displayName(){
		return this.situacaoReco.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.situacaoReco.toUpperCase();
	}
	
	
}
