package br.com.finiciativas.fseweb.enums;

public enum ScoreCL {
	
	APlus("A+"),
	A("A"),
	B("B"),
	C("C"),
	D("D"),
	E("E");
	
	public String scoreCL;

	private ScoreCL(String scoreCL) {
		this.scoreCL = scoreCL;
	}
	
	public String displayName() {
		return this.scoreCL.toUpperCase();
	}

	public String toString() {
		return this.scoreCL.toUpperCase();
	}
	
}
