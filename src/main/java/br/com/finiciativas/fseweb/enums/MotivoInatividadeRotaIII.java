package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeRotaIII {
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_RETORNO("Sem retorno"),
	SEM_INDUSTRIALIZACAO_EQUIP_IMPORT("N�o realiza industrializa��o dos equipamentos importados"),
	SEM_IMPORT_AUTO("N�o realiza importa��es de autope�as eleg�veis"),
	PERDA_PRAZO_ENVIO("Perda dos prazos de envio do pleito");
	
	
	private String displayName;

	private MotivoInatividadeRotaIII(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
