package br.com.finiciativas.fseweb.enums;

public enum SubEstadoFaturamento {
	AGUARDANDO_INFORMACAO_DO_CLIENTE("Aguardando informa��o do cliente"),
	AGUARDANDO_RESPOSTA_DO_INT("Aguardando resposta do int"),
	EM_ANALISE("em analise"),
	EM_NEGOCIACAO("Em negocia��o"),
	EMISSAO_AGENDADA("Emiss�o agendada"),
	ERRO_PERDA_OU_CANCELAMENTO("Erro, perda ou cancelamento"),
	FINALIZACAO_DA_ETAPA_EM_ABERTO("Finaliza��o da etapa em aberto");
	
	private String SubEstadoFaturamento;
	
	private SubEstadoFaturamento(String subEstadoFaturamento) {
		this.SubEstadoFaturamento = subEstadoFaturamento;
	}
	
	public String displayName(){
		return this.SubEstadoFaturamento.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.SubEstadoFaturamento.toUpperCase();
	}
}
