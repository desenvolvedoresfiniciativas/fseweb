package br.com.finiciativas.fseweb.enums;

public enum RelatorioCalculo {

	ANUAL("Anual"), 
	T1("T1"),
	T2("T2"),
	T3("T3"),
	T4("T4");

	private String displayName;

	private RelatorioCalculo(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
