package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeLeiDoBem {
	PREJUIZO_FISCAL("Preju�zo Fiscal"),
	LUCRO_PRESUMIDO("Lucro Presumido"),
	SEM_PED("N�o tem P&D"),
	//SEM_CND("N�o tem CND"),
	SEM_INTERESSE("N�o tem Interesse"),
	REC_JUDICIAL("Recupera��o Judicial"),
	SEM_RETORNO("Sem retorno");
	
	private String displayName;

	private MotivoInatividadeLeiDoBem(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
