package br.com.finiciativas.fseweb.enums;

public enum SituacaoCL {

	ATIVA("Ativa"), 
	INATIVA("Inativa"), 
	PREVISAO_ATIVA("Previs�o Ativa"), 
	PREVISAO_INATIVA("Previs�o Inativa"),
	EVALUACI�N("Evaluaci�n");
	
	public String situacaoCL;

	private SituacaoCL(String situacaoCL) {
		this.situacaoCL = situacaoCL;
	}

	public String displayName() {
		return this.situacaoCL.toUpperCase();
	}

	@Override
	public String toString() {
		return this.situacaoCL.toUpperCase();
	}

}
