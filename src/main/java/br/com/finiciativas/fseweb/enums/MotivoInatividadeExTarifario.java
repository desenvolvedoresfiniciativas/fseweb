package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeExTarifario {
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_POTENCIAL_IMPORTACAO("Sem potencial para importa��o"),
	PROD_IMPOTADO_N�O_BIT_BK("Produtos importados que n�o se classificam como BIT/BK"),
	SEM_RETORNO("Sem retorno"),
	COMPROVACAO_SIM_NASC("Comprova��o de similar nacional");
	
	private String displayName;

	private MotivoInatividadeExTarifario(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	

}
