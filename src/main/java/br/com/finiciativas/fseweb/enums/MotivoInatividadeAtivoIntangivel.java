package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeAtivoIntangivel {
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_RETORNO("Sem retorno"),
	SEM_ATENDIMENTO_CPC("N�o atendimento a CPC-04");
	
	private String displayName;

	private MotivoInatividadeAtivoIntangivel(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}
