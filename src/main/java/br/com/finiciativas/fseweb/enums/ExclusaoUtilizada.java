package br.com.finiciativas.fseweb.enums;

public enum ExclusaoUtilizada {

	SESSENTA("60"), 
	SETENTA("70"),
	OITENTA("80");
	
	private String displayName;

	private ExclusaoUtilizada(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
	
}
