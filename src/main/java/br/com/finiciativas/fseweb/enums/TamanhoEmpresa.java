package br.com.finiciativas.fseweb.enums;

public enum TamanhoEmpresa {

	MICRO_1("Micro 1"),
	MICRO_2("Micro 2"),
	MICRO_3("Micro 3"),
	PEQUENA_1("Peque�a 1"),
	PEQUENA_2("Peque�a 2"),
	PEQUENA_3("Peque�a 3"),
	MEDIANA_1("Mediana 1"),
	MEDIANA_2("Mediana 2"),
	GRANDE_1("Grande 1"),
	GRANDE_2("Grande 2"),
	GRANDE_3("Grande 3"),
	GRANDE_4("Grande 4");
	
	private String tamanhoEmpresa;
	
	private TamanhoEmpresa(String tamanhoEmpresa) {
		this.tamanhoEmpresa = tamanhoEmpresa;
	}
	
	public String displayName(){
		return this.tamanhoEmpresa.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.tamanhoEmpresa.toUpperCase();
	}
	
}
