package br.com.finiciativas.fseweb.enums;

public enum Categoria {
	

	Aplus("A+"),
	A("A"),
	B("B"),
	C("C");
	;
	
	private String categoria;
	
	private Categoria(String cargo) {
		this.categoria = cargo;
	}
	
	public String displayName(){
		return this.categoria.toUpperCase();
	}
	
	@Override
	public String toString() {
		return this.categoria.toUpperCase();
	}
	
}
