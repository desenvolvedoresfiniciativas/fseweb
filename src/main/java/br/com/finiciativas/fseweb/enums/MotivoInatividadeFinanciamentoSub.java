package br.com.finiciativas.fseweb.enums;

public enum MotivoInatividadeFinanciamentoSub {
	SEM_INTERESSE("N�o tem Interesse"),
	SEM_RETORNO("Sem retorno"),
	SEM_OPORTUNIDADES_CRITERIOS("Sem oportunidade para atingir os crit�rios");
	
	private String displayName;

	private MotivoInatividadeFinanciamentoSub(String displayName) {
		this.displayName = displayName;
	}

	public String displayName() {
		return this.displayName.toUpperCase();
	}

	@Override
	public String toString() {
		return this.displayName.toUpperCase();
	}
}

