package br.com.finiciativas.fseweb.models.old;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//tabela tb_valor_negocio_temp FSE OLD 

@Entity
public class ValorNegocioOld {

	@Id
	@GeneratedValue
	private Long id;

	private int idCampanha;
	private int idEmpresa;

	private String nomeEmpresa;
	private String grupoEconomico;
	private String cnpj;
	private String cidade;
	private String estado;
	private String equipe;
	private String sigla;
	private String divisao;
	private String setor;
	private String situacao;
	private String tipoNegocio;

	private BigDecimal estComercial;
	private BigDecimal prevDespesa;
	private BigDecimal VCAtual;

	private String VCFim;
	private String VCValCLi;

	private BigDecimal prevBeneficio;
	private BigDecimal ctrVlfixo;

	private int ctrTipoFee;

	private BigDecimal ctrValorFee;
	private BigDecimal prevValorFee;

	private String ctrObs;

	private BigDecimal ctrTeto;
	private BigDecimal antDispendio;

	private String comercial;
	private String categoria;

	private BigDecimal fatReal;
	private BigDecimal fatPrev;
	private BigDecimal valorBase;

	private String modoValor;
	private BigDecimal antBeneficio;
	private String preEntregaCalc;

	private int ctrPcDe;
	private int ctrPcSm;
	private int ctrPcRm;

	private BigDecimal antFat;
	private BigDecimal VCDisp;

	private String dtEntCli;

	private int rrPrimAno;

	private String exclPrev;
	private String exclreal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(int idCampanha) {
		this.idCampanha = idCampanha;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getGrupoEconomico() {
		return grupoEconomico;
	}

	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEquipe() {
		return equipe;
	}

	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDivisao() {
		return divisao;
	}

	public void setDivisao(String divisao) {
		this.divisao = divisao;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(String tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public BigDecimal getEstComercial() {
		return estComercial;
	}

	public void setEstComercial(BigDecimal estComercial) {
		this.estComercial = estComercial;
	}

	public BigDecimal getPrevDespesa() {
		return prevDespesa;
	}

	public void setPrevDespesa(BigDecimal prevDespesa) {
		this.prevDespesa = prevDespesa;
	}

	public BigDecimal getVCAtual() {
		return VCAtual;
	}

	public void setVCAtual(BigDecimal vCAtual) {
		VCAtual = vCAtual;
	}

	public String getVCFim() {
		return VCFim;
	}

	public void setVCFim(String vCFim) {
		VCFim = vCFim;
	}

	public String getVCValCLi() {
		return VCValCLi;
	}

	public void setVCValCLi(String vCValCLi) {
		VCValCLi = vCValCLi;
	}

	public BigDecimal getPrevBeneficio() {
		return prevBeneficio;
	}

	public void setPrevBeneficio(BigDecimal prevBeneficio) {
		this.prevBeneficio = prevBeneficio;
	}

	public BigDecimal getCtrVlfixo() {
		return ctrVlfixo;
	}

	public void setCtrVlfixo(BigDecimal ctrVlfixo) {
		this.ctrVlfixo = ctrVlfixo;
	}

	public int getCtrTipoFee() {
		return ctrTipoFee;
	}

	public void setCtrTipoFee(int ctrTipoFee) {
		this.ctrTipoFee = ctrTipoFee;
	}

	public BigDecimal getCtrValorFee() {
		return ctrValorFee;
	}

	public void setCtrValorFee(BigDecimal ctrValorFee) {
		this.ctrValorFee = ctrValorFee;
	}

	public BigDecimal getPrevValorFee() {
		return prevValorFee;
	}

	public void setPrevValorFee(BigDecimal prevValorFee) {
		this.prevValorFee = prevValorFee;
	}

	public String getCtrObs() {
		return ctrObs;
	}

	public void setCtrObs(String ctrObs) {
		this.ctrObs = ctrObs;
	}

	public BigDecimal getCtrTeto() {
		return ctrTeto;
	}

	public void setCtrTeto(BigDecimal ctrTeto) {
		this.ctrTeto = ctrTeto;
	}

	public BigDecimal getAntDispendio() {
		return antDispendio;
	}

	public void setAntDispendio(BigDecimal antDispendio) {
		this.antDispendio = antDispendio;
	}

	public String getComercial() {
		return comercial;
	}

	public void setComercial(String comercial) {
		this.comercial = comercial;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public BigDecimal getFatReal() {
		return fatReal;
	}

	public void setFatReal(BigDecimal fatReal) {
		this.fatReal = fatReal;
	}

	public BigDecimal getFatPrev() {
		return fatPrev;
	}

	public void setFatPrev(BigDecimal fatPrev) {
		this.fatPrev = fatPrev;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public String getModoValor() {
		return modoValor;
	}

	public void setModoValor(String modoValor) {
		this.modoValor = modoValor;
	}

	public BigDecimal getAntBeneficio() {
		return antBeneficio;
	}

	public void setAntBeneficio(BigDecimal antBeneficio) {
		this.antBeneficio = antBeneficio;
	}

	public String getPreEntregaCalc() {
		return preEntregaCalc;
	}

	public void setPreEntregaCalc(String preEntregaCalc) {
		this.preEntregaCalc = preEntregaCalc;
	}

	public int getCtrPcDe() {
		return ctrPcDe;
	}

	public void setCtrPcDe(int ctrPcDe) {
		this.ctrPcDe = ctrPcDe;
	}

	public int getCtrPcSm() {
		return ctrPcSm;
	}

	public void setCtrPcSm(int ctrPcSm) {
		this.ctrPcSm = ctrPcSm;
	}

	public int getCtrPcRm() {
		return ctrPcRm;
	}

	public void setCtrPcRm(int ctrPcRm) {
		this.ctrPcRm = ctrPcRm;
	}

	public BigDecimal getAntFat() {
		return antFat;
	}

	public void setAntFat(BigDecimal antFat) {
		this.antFat = antFat;
	}

	public BigDecimal getVCDisp() {
		return VCDisp;
	}

	public void setVCDisp(BigDecimal vCDisp) {
		VCDisp = vCDisp;
	}

	public String getDtEntCli() {
		return dtEntCli;
	}

	public void setDtEntCli(String dtEntCli) {
		this.dtEntCli = dtEntCli;
	}

	public int getRrPrimAno() {
		return rrPrimAno;
	}

	public void setRrPrimAno(int rrPrimAno) {
		this.rrPrimAno = rrPrimAno;
	}

	public String getExclPrev() {
		return exclPrev;
	}

	public void setExclPrev(String exclPrev) {
		this.exclPrev = exclPrev;
	}

	public String getExclreal() {
		return exclreal;
	}

	public void setExclreal(String exclreal) {
		this.exclreal = exclreal;
	}

}
