package br.com.finiciativas.fseweb.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class LogError {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String modulo;
	
	private String erro;
	
	private Long idTrigger;
	
	private String moduloTrigger;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public Long getIdTrigger() {
		return idTrigger;
	}

	public void setIdTrigger(Long idTrigger) {
		this.idTrigger = idTrigger;
	}

	public String getModuloTrigger() {
		return moduloTrigger;
	}

	public void setModuloTrigger(String moduloTrigger) {
		this.moduloTrigger = moduloTrigger;
	}

}
