package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class Acompanhamento {

	@Id
	@GeneratedValue
	private Long id;
	
	private String nome;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Documentacao documentacao;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private EnvioRelatorio envioRelatorio;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private InformacaoCalculo informacaoCalculo;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private InformacaoTecnica informacaoTecnica;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Questionamentos questionamentos;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Recurso recurso;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private RelatorioTecnicoFinanceiro relatorioTecnico;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private ResultadoFinal resultadoFinal;

	@ManyToOne
	private Producao producao;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Documentacao getDocumentacao() {
		return documentacao;
	}

	public void setDocumentacao(Documentacao documentacao) {
		this.documentacao = documentacao;
	}

	public EnvioRelatorio getEnvioRelatorio() {
		return envioRelatorio;
	}

	public void setEnvioRelatorio(EnvioRelatorio envioRelatorio) {
		this.envioRelatorio = envioRelatorio;
	}

	public InformacaoCalculo getInformacaoCalculo() {
		return informacaoCalculo;
	}

	public void setInformacaoCalculo(InformacaoCalculo informacaoCalculo) {
		this.informacaoCalculo = informacaoCalculo;
	}

	public InformacaoTecnica getInformacaoTecnica() {
		return informacaoTecnica;
	}

	public void setInformacaoTecnica(InformacaoTecnica informacaoTecnica) {
		this.informacaoTecnica = informacaoTecnica;
	}

	public Questionamentos getQuestionamentos() {
		return questionamentos;
	}

	public void setQuestionamentos(Questionamentos questionamentos) {
		this.questionamentos = questionamentos;
	}

	public Recurso getRecurso() {
		return recurso;
	}

	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}

	public RelatorioTecnicoFinanceiro getRelatorioTecnico() {
		return relatorioTecnico;
	}

	public void setRelatorioTecnico(RelatorioTecnicoFinanceiro relatorioTecnico) {
		this.relatorioTecnico = relatorioTecnico;
	}

	public ResultadoFinal getResultadoFinal() {
		return resultadoFinal;
	}

	public void setResultadoFinal(ResultadoFinal resultadoFinal) {
		this.resultadoFinal = resultadoFinal;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

}

