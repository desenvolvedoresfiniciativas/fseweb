package br.com.finiciativas.fseweb.models.produto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.models.consultor.Comissao;

@Entity
public class EtapaTrabalho {

	@Id
	@GeneratedValue
	private Long id;

	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;

	@ColumnTransformer(read = "UPPER(DESCRICAO)")
	private String descricao;

	@ColumnTransformer(read = "UPPER(NOME_STR)")
	private String nomeStr;
	
	private boolean subProducao;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@OrderBy("nome ASC")
	private List<Tarefa> tarefas = new ArrayList<Tarefa>();
	
	@OneToOne
	private Comissao comissao;

	public EtapaTrabalho() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Tarefa> getTarefas() {
		return tarefas;
	}

	public void setTarefas(List<Tarefa> tarefas) {
		this.tarefas = tarefas;
	}

	public String getNomeStr() {
		return nomeStr;
	}

	public void setNomeStr(String nomeStr) {
		this.nomeStr = nomeStr;
	}

	public boolean isSubProducao() {
		return subProducao;
	}

	public void setSubProducao(boolean subProducao) {
		this.subProducao = subProducao;
	}

}
