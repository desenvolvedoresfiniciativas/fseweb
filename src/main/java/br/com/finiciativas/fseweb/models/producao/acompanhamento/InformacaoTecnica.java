package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class InformacaoTecnica {

	@Id
	@GeneratedValue
	private Long id;
	
	private String previsaoDocumentacao;
	
	private boolean informacaoRecebida;
	
	private int reunioes;
	
	@OneToOne
	private Acompanhamento acompanhamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrevisaoDocumentacao() {
		return previsaoDocumentacao;
	}

	public void setPrevisaoDocumentacao(String previsaoDocumentacao) {
		this.previsaoDocumentacao = previsaoDocumentacao;
	}

	public boolean isInformacaoRecebida() {
		return informacaoRecebida;
	}

	public void setInformacaoRecebida(boolean informacaoRecebida) {
		this.informacaoRecebida = informacaoRecebida;
	}

	public int getReunioes() {
		return reunioes;
	}

	public void setReunioes(int reunioes) {
		this.reunioes = reunioes;
	}

	public Acompanhamento getAcompanhamento() {
		return acompanhamento;
	}

	public void setAcompanhamento(Acompanhamento acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	
}
