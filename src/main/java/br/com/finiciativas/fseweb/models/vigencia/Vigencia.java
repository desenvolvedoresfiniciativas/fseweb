package br.com.finiciativas.fseweb.models.vigencia;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.finiciativas.fseweb.models.contrato.Contrato;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Vigencia implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "vigencia")
	private List<Contrato> contrato;
	
	private String inicioVigencia;
	
	private String campanhaInicial;
	
	private String campanhaFinal;
	
	Vigencia() {
		
	}
	
	public String getCampanhaInicial() {
		return campanhaInicial;
	}

	public void setCampanhaInicial(String campanhaInicial) {
		this.campanhaInicial = campanhaInicial;
	}

	public String getCampanhaFinal() {
		return campanhaFinal;
	}

	public void setCampanhaFinal(String campanhaFinal) {
		this.campanhaFinal = campanhaFinal;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Contrato> getContrato() {
		return contrato;
	}

	public void setContrato(List<Contrato> contrato) {
		this.contrato = contrato;
	}

	@Override
	public String toString() {
		return "Nome : " + this.nome + " Inicio : " + this.inicioVigencia;
	}

	
}
