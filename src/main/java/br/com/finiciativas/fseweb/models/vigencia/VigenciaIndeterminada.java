package br.com.finiciativas.fseweb.models.vigencia;

import javax.persistence.Entity;

@Entity
public class VigenciaIndeterminada extends Vigencia {

	private static final long serialVersionUID = 1L;
	
	private String dataEncerramentoContrato;
	
	public VigenciaIndeterminada() {
		super.setNome("Vig�ncia Indeterminada");
	}
	
	public VigenciaIndeterminada(String inicioVigencia, String encerramentoContrato) {
		super.setInicioVigencia(inicioVigencia);
		this.dataEncerramentoContrato = encerramentoContrato;
	}	

	public String getDataEncerramentoContrato() {
		return dataEncerramentoContrato;
	}

	public void setDataEncerramentoContrato(String dataEncerramentoContrato) {
		this.dataEncerramentoContrato = dataEncerramentoContrato;
	}
	
	@Override
	public void setInicioVigencia(String inicioVigencia) {
		super.setInicioVigencia(inicioVigencia);
	}
	
	@Override
	public String getInicioVigencia() {
		return super.getInicioVigencia();
	}
}
