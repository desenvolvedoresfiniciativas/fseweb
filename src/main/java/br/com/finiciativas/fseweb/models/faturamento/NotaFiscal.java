package br.com.finiciativas.fseweb.models.faturamento;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.enums.Incidencia;
import br.com.finiciativas.fseweb.enums.MotivoCancelamento;
import br.com.finiciativas.fseweb.enums.TipoNotaFiscal;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.Parcela;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.contrato.Contrato;

@Entity
public class NotaFiscal {

	@Id
	@GeneratedValue
	private long id;

	@Enumerated(EnumType.STRING)
	private Incidencia incidencia;

	@Enumerated(EnumType.STRING)
	private MotivoCancelamento motivoCancelamento;

	@Enumerated(EnumType.STRING)
	private TipoNotaFiscal tipoNotaFiscal;

	@ManyToOne
	private Contrato contrato;

	@OneToOne
	private Cliente cliente;

	@ManyToOne
	private Filial filial;

	@OneToOne
	private Faturamento faturamento;
	
	@OneToOne
	private Comissao comissao;

	private String categoriaNF;

	private String timing;

	private boolean cancelada;

	private boolean cobranca;

	private boolean atraso;

	private String dataEmissao;

	private String dataVencimento;

	private String dataCobranca;

	private String numeroNF;

	private String faturar;

	private String cobrar;

	private String cnpjFaturar;

	private String valorCobranca;

	private String obsNota;
	
	private String obsCobranca;

	private int parcela;

	private boolean duplaTributacao;

	private float percentVariavel;

	private String ISSvariavel;

	private String statusNF;

	private String valorParcela;

	private String dataCobrancaParcela;

	private String dataVencimentoParcela;

	private String numeroNotaSubstituida;

	private String campanha;

	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<Parcela> parcelas;

	public List<Parcela> getParcelas() {
		System.out.println("ENTROU NO GET PARCELAS");
		return parcelas;
	}

	public void setParcelas(List<Parcela> parcelas) {
		this.parcelas = parcelas;
	}

	@ColumnTransformer(read = "UPPER(RAZAO_SOCIAL_FATURAR)")
	private String razaoSocialFaturar;
	
	
	public String getObsCobranca() {
		return obsCobranca;
	}

	public void setObsCobranca(String obsCobranca) {
		this.obsCobranca = obsCobranca;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Incidencia getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(Incidencia incidencia) {
		this.incidencia = incidencia;
	}

	public MotivoCancelamento getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(MotivoCancelamento motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public TipoNotaFiscal getTipoNotaFiscal() {
		return tipoNotaFiscal;
	}

	public void setTipoNotaFiscal(TipoNotaFiscal tipoNotaFiscal) {
		this.tipoNotaFiscal = tipoNotaFiscal;
	}

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public String getTiming() {
		return timing;
	}

	public void setTiming(String timing) {
		this.timing = timing;
	}

	public boolean isCancelada() {
		return cancelada;
	}

	public void setCancelada(boolean cancelada) {
		this.cancelada = cancelada;
	}

	public boolean isCobranca() {
		return cobranca;
	}

	public void setCobranca(boolean cobranca) {
		this.cobranca = cobranca;
	}

	public boolean isAtraso() {
		return atraso;
	}

	public void setAtraso(boolean atraso) {
		this.atraso = atraso;
	}
	
	public Comissao getComissao() {
		return comissao;
	}

	public void setComissao(Comissao comissao) {
		this.comissao = comissao;
	}

	public String getDataEmissao() {
		try {
			if (dataEmissao.matches(".*/.*/.*")) {
				return dataEmissao;
			} else {
				String ano = dataEmissao.substring(0, 4);
				String mes = dataEmissao.substring(5, 7);
				String dia = dataEmissao.substring(8, 10);
				String dataFormatada = dia +"/"+ mes +"/"+ ano;

				return dataFormatada;
			}
		} catch (Exception e) {
			return "";
		}
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getDataVencimento() {
		try {
			if (dataVencimento.matches(".*/.*/.*")) {
				return dataVencimento;
			} else {
				String ano = dataVencimento.substring(0, 4);
				String mes = dataVencimento.substring(5, 7);
				String dia = dataVencimento.substring(8, 10);
				String dataFormatada = dia + "/" + mes + "/" + ano;

				return dataFormatada;
			}
		} catch (Exception e) {
			return "";
		}
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getDataCobranca() {
		try {
			if (dataCobranca.matches(".*/.*/.*")) {
				return dataCobranca;
			} else {
				String ano = dataCobranca.substring(0, 4);
				String mes = dataCobranca.substring(5, 7);
				String dia = dataCobranca.substring(8, 10);
				String dataFormatada = dia + "/" + mes + "/" + ano;
				return dataFormatada;
			}
		} catch (Exception e) {
			return "";
		}
	}

	public void setDataCobranca(String dataCobranca) {
		this.dataCobranca = dataCobranca;
	}

	public String getNumeroNF() {
		return numeroNF;
	}

	public void setNumeroNF(String numeroNF) {
		this.numeroNF = numeroNF;
	}

	public String getFaturar() {
		return faturar;
	}

	public void setFaturar(String faturar) {
		this.faturar = faturar;
	}

	public String getCobrar() {
		return cobrar;
	}

	public void setCobrar(String cobrar) {
		this.cobrar = cobrar;
	}

	public String getCnpjFaturar() {
		return cnpjFaturar;
	}

	public void setCnpjFaturar(String cnpjFaturar) {
		this.cnpjFaturar = cnpjFaturar;
	}

	public String getRazaoSocialFaturar() {
		return razaoSocialFaturar;
	}

	public void setRazaoSocialFaturar(String razaoSocialFaturar) {
		this.razaoSocialFaturar = razaoSocialFaturar;
	}

	public String getValorCobranca() {
		return valorCobranca;
	}

	public void setValorCobranca(String valorCobranca) {
		this.valorCobranca = valorCobranca;
	}

	public String getCategoriaNF() {
		return categoriaNF;
	}

	public void setCategoriaNF(String categoriaNF) {
		this.categoriaNF = categoriaNF;
	}

	public void setFaturamento(Faturamento faturamento) {
		this.faturamento = faturamento;
	}

	public String getObsNota() {
		return obsNota;
	}

	public void setObsNota(String obsNota) {
		this.obsNota = obsNota;
	}

	public Faturamento getFaturamento() {
		return faturamento;
	}

	public int getParcela() {
		return parcela;
	}

	public void setParcela(int parcela) {
		this.parcela = parcela;
	}

	public boolean isDuplaTributacao() {
		return duplaTributacao;
	}

	public void setDuplaTributacao(boolean duplaTributacao) {
		this.duplaTributacao = duplaTributacao;
	}

	public float getPercentVariavel() {
		return percentVariavel;
	}

	public void setPercentVariavel(float percentVariavel) {
		this.percentVariavel = percentVariavel;
	}

	public String getISSvariavel() {
		return ISSvariavel;
	}

	public void setISSvariavel(String iSSvariavel) {
		ISSvariavel = iSSvariavel;
	}

	public String getStatusNF() {
		return statusNF;
	}

	public void setStatusNF(String statusNF) {
		this.statusNF = statusNF;
	}

	public String getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(String valorParcela) {
		this.valorParcela = valorParcela;
	}

	public String getDataCobrancaParcela() {
		return dataCobrancaParcela;
	}

	public void setDataCobrancaParcela(String dataCobrancaParcela) {
		this.dataCobrancaParcela = dataCobrancaParcela;
	}

	public String getDataVencimentoParcela() {
		return dataVencimentoParcela;
	}

	public void setDataVencimentoParcela(String dataVencimentoParcela) {
		this.dataVencimentoParcela = dataVencimentoParcela;
	}

	public String getNumeroNotaSubstituida() {
		return numeroNotaSubstituida;
	}

	public void setNumeroNotaSubstituida(String numeroNotaSubstituida) {
		this.numeroNotaSubstituida = numeroNotaSubstituida;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	@Override
	public String toString() {
		return "NotaFiscal [id=" + id + ", incidencia=" + incidencia + ", motivoCancelamento=" + motivoCancelamento
				+ ", tipoNotaFiscal=" + tipoNotaFiscal + ", contrato=" + contrato + ", cliente=" + cliente + ", filial="
				+ filial + ", faturamento=" + faturamento + ", categoriaNF=" + categoriaNF + ", timing=" + timing
				+ ", cancelada=" + cancelada + ", cobranca=" + cobranca + ", atraso=" + atraso + ", dataEmissao="
				+ dataEmissao + ", dataVencimento=" + dataVencimento + ", dataCobranca=" + dataCobranca + ", numeroNF="
				+ numeroNF + ", faturar=" + faturar + ", cobrar=" + cobrar + ", cnpjFaturar=" + cnpjFaturar
				+ ", valorCobranca=" + valorCobranca + ", obsNota=" + obsNota + ", parcela=" + parcela
				+ ", duplaTributacao=" + duplaTributacao + ", percentVariavel=" + percentVariavel + ", ISSvariavel="
				+ ISSvariavel + ", statusNF=" + statusNF + ", valorParcela=" + valorParcela + ", dataCobrancaParcela="
				+ dataCobrancaParcela + ", dataVencimentoParcela=" + dataVencimentoParcela + ", numeroNotaSubstituida="
				+ numeroNotaSubstituida + ", parcelas=" + parcelas + ", razaoSocialFaturar=" + razaoSocialFaturar + "]";
	}
}
