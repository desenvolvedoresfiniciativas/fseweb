package br.com.finiciativas.fseweb.models.old;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//tabela tb_contratos FSE OLD 

@Entity
public class ContratoOld {

	@Id
	@GeneratedValue
	private Long id;

	private int idEmpresa;

	private String cnpjFaturar;
	private String razaoSocialFaturar;
	private String tipoContrato;
	private String dataEntrada;
	private String inicioVigencia;
	private String fimVigencia;

	private char comercial;

	private String analista;
	private String recoTecnico;

	private char comercialEmpresa;
	private double estimativaComercial;

	private int renovacaoPrejuizo;
	private int renovacaoAutomatica;

	private String obsRenovacao;
	private int fixo;
	private String tipo;
	private int parcela;
	private String inicioFaturacao;
	private double valorMensalTrimestral;
	private int escalonamento;
	private double valorEscalo;

	private String pagamentoImpostos;
	private String emissaoNfe;
	private String limiteNfe;
	private String obsNfe;

	private char contratoPadrao;

	private double escalonamento1;
	private double f1;
	private double escalonamento2;
	private double f2;
	private double escalonamento3;
	private double f3;
	private double escalonamento4;
	private double f4;
	private double escalonamento5;
	private double f5;
	private double escalonamento6;
	private double f6;

	private int dossieEconomico;
	private int dossieTecnicoMcti;
	private int validacaoMcti;

	private String reembolso;
	private String contratoFisico;
	private String valorReajustado;
	private String obsReferencia;
	private String formaPgto;

	private double tetoFaturacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getCnpjFaturar() {
		return cnpjFaturar;
	}

	public void setCnpjFaturar(String cnpjFaturar) {
		this.cnpjFaturar = cnpjFaturar;
	}

	public String getRazaoSocialFaturar() {
		return razaoSocialFaturar;
	}

	public void setRazaoSocialFaturar(String razaoSocialFaturar) {
		this.razaoSocialFaturar = razaoSocialFaturar;
	}

	public String getTipoContrato() {
		return tipoContrato;
	}

	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	public String getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(String dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public String getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public String getFimVigencia() {
		return fimVigencia;
	}

	public void setFimVigencia(String fimVigencia) {
		this.fimVigencia = fimVigencia;
	}

	public char getComercial() {
		return comercial;
	}

	public void setComercial(char comercial) {
		this.comercial = comercial;
	}

	public String getAnalista() {
		return analista;
	}

	public void setAnalista(String analista) {
		this.analista = analista;
	}

	public String getRecoTecnico() {
		return recoTecnico;
	}

	public void setRecoTecnico(String recoTecnico) {
		this.recoTecnico = recoTecnico;
	}

	public char getComercialEmpresa() {
		return comercialEmpresa;
	}

	public void setComercialEmpresa(char comercialEmpresa) {
		this.comercialEmpresa = comercialEmpresa;
	}

	public double getEstimativaComercial() {
		return estimativaComercial;
	}

	public void setEstimativaComercial(double estimativaComercial) {
		this.estimativaComercial = estimativaComercial;
	}

	public int getRenovacaoPrejuizo() {
		return renovacaoPrejuizo;
	}

	public void setRenovacaoPrejuizo(int renovacaoPrejuizo) {
		this.renovacaoPrejuizo = renovacaoPrejuizo;
	}

	public int getRenovacaoAutomatica() {
		return renovacaoAutomatica;
	}

	public void setRenovacaoAutomatica(int renovacaoAutomatica) {
		this.renovacaoAutomatica = renovacaoAutomatica;
	}

	public String getObsRenovacao() {
		return obsRenovacao;
	}

	public void setObsRenovacao(String obsRenovacao) {
		this.obsRenovacao = obsRenovacao;
	}

	public int getFixo() {
		return fixo;
	}

	public void setFixo(int fixo) {
		this.fixo = fixo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getParcela() {
		return parcela;
	}

	public void setParcela(int parcela) {
		this.parcela = parcela;
	}

	public String getInicioFaturacao() {
		return inicioFaturacao;
	}

	public void setInicioFaturacao(String inicioFaturacao) {
		this.inicioFaturacao = inicioFaturacao;
	}

	public double getValorMensalTrimestral() {
		return valorMensalTrimestral;
	}

	public void setValorMensalTrimestral(double valorMensalTrimestral) {
		this.valorMensalTrimestral = valorMensalTrimestral;
	}

	public int getEscalonamento() {
		return escalonamento;
	}

	public void setEscalonamento(int escalonamento) {
		this.escalonamento = escalonamento;
	}

	public double getValorEscalo() {
		return valorEscalo;
	}

	public void setValorEscalo(double valorEscalo) {
		this.valorEscalo = valorEscalo;
	}

	public String getPagamentoImpostos() {
		return pagamentoImpostos;
	}

	public void setPagamentoImpostos(String pagamentoImpostos) {
		this.pagamentoImpostos = pagamentoImpostos;
	}

	public String getEmissaoNfe() {
		return emissaoNfe;
	}

	public void setEmissaoNfe(String emissaoNfe) {
		this.emissaoNfe = emissaoNfe;
	}

	public String getLimiteNfe() {
		return limiteNfe;
	}

	public void setLimiteNfe(String limiteNfe) {
		this.limiteNfe = limiteNfe;
	}

	public String getObsNfe() {
		return obsNfe;
	}

	public void setObsNfe(String obsNfe) {
		this.obsNfe = obsNfe;
	}

	public char getContratoPadrao() {
		return contratoPadrao;
	}

	public void setContratoPadrao(char contratoPadrao) {
		this.contratoPadrao = contratoPadrao;
	}

	public double getEscalonamento1() {
		return escalonamento1;
	}

	public void setEscalonamento1(double escalonamento1) {
		this.escalonamento1 = escalonamento1;
	}

	public double getF1() {
		return f1;
	}

	public void setF1(double f1) {
		this.f1 = f1;
	}

	public double getEscalonamento2() {
		return escalonamento2;
	}

	public void setEscalonamento2(double escalonamento2) {
		this.escalonamento2 = escalonamento2;
	}

	public double getF2() {
		return f2;
	}

	public void setF2(double f2) {
		this.f2 = f2;
	}

	public double getEscalonamento3() {
		return escalonamento3;
	}

	public void setEscalonamento3(double escalonamento3) {
		this.escalonamento3 = escalonamento3;
	}

	public double getF3() {
		return f3;
	}

	public void setF3(double f3) {
		this.f3 = f3;
	}

	public double getEscalonamento4() {
		return escalonamento4;
	}

	public void setEscalonamento4(double escalonamento4) {
		this.escalonamento4 = escalonamento4;
	}

	public double getF4() {
		return f4;
	}

	public void setF4(double f4) {
		this.f4 = f4;
	}

	public double getEscalonamento5() {
		return escalonamento5;
	}

	public void setEscalonamento5(double escalonamento5) {
		this.escalonamento5 = escalonamento5;
	}

	public double getF5() {
		return f5;
	}

	public void setF5(double f5) {
		this.f5 = f5;
	}

	public double getEscalonamento6() {
		return escalonamento6;
	}

	public void setEscalonamento6(double escalonamento6) {
		this.escalonamento6 = escalonamento6;
	}

	public double getF6() {
		return f6;
	}

	public void setF6(double f6) {
		this.f6 = f6;
	}

	public int getDossieEconomico() {
		return dossieEconomico;
	}

	public void setDossieEconomico(int dossieEconomico) {
		this.dossieEconomico = dossieEconomico;
	}

	public int getDossieTecnicoMcti() {
		return dossieTecnicoMcti;
	}

	public void setDossieTecnicoMcti(int dossieTecnicoMcti) {
		this.dossieTecnicoMcti = dossieTecnicoMcti;
	}

	public int getValidacaoMcti() {
		return validacaoMcti;
	}

	public void setValidacaoMcti(int validacaoMcti) {
		this.validacaoMcti = validacaoMcti;
	}

	public String getReembolso() {
		return reembolso;
	}

	public void setReembolso(String reembolso) {
		this.reembolso = reembolso;
	}

	public String getContratoFisico() {
		return contratoFisico;
	}

	public void setContratoFisico(String contratoFisico) {
		this.contratoFisico = contratoFisico;
	}

	public String getValorReajustado() {
		return valorReajustado;
	}

	public void setValorReajustado(String valorReajustado) {
		this.valorReajustado = valorReajustado;
	}

	public String getObsReferencia() {
		return obsReferencia;
	}

	public void setObsReferencia(String obsReferencia) {
		this.obsReferencia = obsReferencia;
	}

	public String getFormaPgto() {
		return formaPgto;
	}

	public void setFormaPgto(String formaPgto) {
		this.formaPgto = formaPgto;
	}

	public double getTetoFaturacao() {
		return tetoFaturacao;
	}

	public void setTetoFaturacao(double tetoFaturacao) {
		this.tetoFaturacao = tetoFaturacao;
	}

}
