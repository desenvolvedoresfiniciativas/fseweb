package br.com.finiciativas.fseweb.models.contrato;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//tabela tb_equipe FSE OLD 

@Entity
public class EquipeOld {

	@Id
	@GeneratedValue
	private Long id;
	
	private String equipe;
	
}
