package br.com.finiciativas.fseweb.models.faturamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;

@Entity
public class Parametros {

	@Id
	@GeneratedValue
	private Long id;
	
	@ColumnTransformer(read = "UPPER(PRODUTO)")
	private String produto;
	
	@ColumnTransformer(read = "UPPER(CODIGO)")
	private String codigo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
