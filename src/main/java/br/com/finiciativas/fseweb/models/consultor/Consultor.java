package br.com.finiciativas.fseweb.models.consultor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.CategoriaConsultor;
import br.com.finiciativas.fseweb.enums.Departamento;
import br.com.finiciativas.fseweb.enums.Divisao;
import br.com.finiciativas.fseweb.enums.Pais;
import br.com.finiciativas.fseweb.models.Equipe;

@Entity
@DynamicUpdate(true)
public class Consultor implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	@ColumnTransformer(read = "UPPER(USERNAME)")
	private String username;

	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;
	
	@ColumnTransformer(read = "UPPER(SIGLA)")
	private String sigla;

	@Column(unique = true)
	@ColumnTransformer(read = "UPPER(EMAIL)")
	private String email;

	@JsonIgnore
	private String senha;

	private boolean ativo;
	
	private boolean primeiroLogin = true;
	
	@Enumerated(EnumType.STRING)
	@ColumnTransformer(read = "UPPER(CARGO)")
	private Cargo cargo;
	
	@Enumerated(EnumType.STRING)
	@ColumnTransformer(read = "UPPER(DEPARTAMENTO)")
	private Departamento departamento;
	
	@Enumerated(EnumType.STRING)
	@ColumnTransformer(read = "UPPER(SETOR_ATIVIDADE)")
	private Divisao setorAtividade;

	@ManyToMany(fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Role> roles = new ArrayList<Role>();
	
	@JsonIgnore
	@ManyToOne
	private Equipe equipe;
	
	@Enumerated(EnumType.STRING)
	@ColumnTransformer(read = "UPPER(CATEGORIA_CONSULTOR)")
	private CategoriaConsultor categoriaConsultor;
	
	@Enumerated(EnumType.STRING)
	@ColumnTransformer(read = "UPPER(PAIS)")
	private Pais pais;

	
	public Consultor() {
	}

	public Consultor(Consultor consultor) {

		if (consultor != null) {
			this.id = consultor.getId();
			this.username = consultor.getUsername();
			this.nome = consultor.getNome();
			this.sigla = consultor.getSigla();
			this.email = consultor.getEmail();
			this.ativo = consultor.isAtivo();
			this.cargo = consultor.getCargo();
			this.departamento = consultor.getDepartamento();
			this.setorAtividade = consultor.getSetorAtividade();
			
			if (consultor.getRoles() != null) {
				this.roles = consultor.getRoles();
			}
		}
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

    public String getEmail() {
		return email;
	}

    @JsonIgnore
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.roles;
	}

	@JsonIgnore
	public String getPassword() {
		return this.senha;
	}

	@JsonIgnore
	public String getUsername() {
		return this.username;
	}

	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	public boolean isEnabled() {
		return this.ativo;
	}

	public void criptarSenha() {
		this.senha = new BCryptPasswordEncoder().encode(this.senha);
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public Divisao getSetorAtividade() {
		return setorAtividade;
	}

	public void setSetorAtividade(Divisao setorAtividade) {
		this.setorAtividade = setorAtividade;
	}
	
	
	

	public String rolesString() {
		StringBuilder sb = new StringBuilder();
		String prefix = "";
		for (Role role : this.roles) {
			sb.append(prefix);
			prefix = " , ";
			sb.append(role.getDescricao());
		}
		return sb.toString();
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	public CategoriaConsultor getCategoriaConsultor() {
		return categoriaConsultor;
	}

	public void setCategoriaConsultor(CategoriaConsultor categoriaConsultor) {
		this.categoriaConsultor = categoriaConsultor;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public boolean isPrimeiroLogin() {
		return primeiroLogin;
	}

	public void setPrimeiroLogin(boolean primeiroLogin) {
		this.primeiroLogin = primeiroLogin;
	}

}
