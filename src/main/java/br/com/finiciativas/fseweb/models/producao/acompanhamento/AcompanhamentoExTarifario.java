package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.SubProducao;

@Entity
public class AcompanhamentoExTarifario {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private SubProducao subProducao;

	private boolean contatoRealizado;

	private boolean envioComprovante;

	private String dataInicio;

	private String dataFim;

	private int itensRecebidos;

	private String valorItensRecebidos;

	private String moedaTransacao;

	private String percentualReducao;

	private String beneficioCalculado;

	@ManyToOne
	private Consultor feitoPor;

	@ManyToOne
	private Consultor validadoPor;

	private String dataEnviado;

	private String dataAprovado;
	
	private boolean aprovado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isContatoRealizado() {
		return contatoRealizado;
	}

	public void setContatoRealizado(boolean contatoRealizado) {
		this.contatoRealizado = contatoRealizado;
	}

	public boolean isEnvioComprovante() {
		return envioComprovante;
	}

	public void setEnvioComprovante(boolean envioComprovante) {
		this.envioComprovante = envioComprovante;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public int getItensRecebidos() {
		return itensRecebidos;
	}

	public void setItensRecebidos(int itensRecebidos) {
		this.itensRecebidos = itensRecebidos;
	}

	public String getValorItensRecebidos() {
		return valorItensRecebidos;
	}

	public void setValorItensRecebidos(String valorItensRecebidos) {
		this.valorItensRecebidos = valorItensRecebidos;
	}

	public String getMoedaTransacao() {
		return moedaTransacao;
	}

	public void setMoedaTransacao(String moedaTransacao) {
		this.moedaTransacao = moedaTransacao;
	}

	public String getPercentualReducao() {
		return percentualReducao;
	}

	public void setPercentualReducao(String percentualReducao) {
		this.percentualReducao = percentualReducao;
	}

	public String getBeneficioCalculado() {
		return beneficioCalculado;
	}

	public void setBeneficioCalculado(String beneficioCalculado) {
		this.beneficioCalculado = beneficioCalculado;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public String getDataEnviado() {
		return dataEnviado;
	}

	public void setDataEnviado(String dataEnviado) {
		this.dataEnviado = dataEnviado;
	}

	public String getDataAprovado() {
		return dataAprovado;
	}

	public void setDataAprovado(String dataAprovado) {
		this.dataAprovado = dataAprovado;
	}

	public SubProducao getSubProducao() {
		return subProducao;
	}

	public void setSubProducao(SubProducao subProducao) {
		this.subProducao = subProducao;
	}

	public boolean isAprovado() {
		return aprovado;
	}

	public void setAprovado(boolean aprovado) {
		this.aprovado = aprovado;
	}

}
