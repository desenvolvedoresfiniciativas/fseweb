package br.com.finiciativas.fseweb.models.produto;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.finiciativas.fseweb.enums.TipoCampo;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ItemTarefa {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToMany
	@JsonIgnore
	private List<Tarefa> tarefa;
	
	@Enumerated(EnumType.STRING)
	private TipoCampo tipo;
	
	private boolean possuiDataPrevisao;
	
	private boolean possuiDataRealizado;
	
	private boolean defineRealizacao;

	private boolean readOnly;
	
	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoCampo getTipo() {
		return tipo;
	}

	public void setTipo(TipoCampo tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isPossuiDataPrevisao() {
		return possuiDataPrevisao;
	}

	public void setPossuiDataPrevisao(boolean possuiDataPrevisao) {
		this.possuiDataPrevisao = possuiDataPrevisao;
	}

	public boolean isPossuiDataRealizado() {
		return possuiDataRealizado;
	}

	public void setPossuiDataRealizado(boolean possuiDataRealizado) {
		this.possuiDataRealizado = possuiDataRealizado;
	}

	public List<Tarefa> getTarefa() {
		return tarefa;
	}

	public void setTarefa(List<Tarefa> tarefa) {
		this.tarefa = tarefa;
	}

	public boolean isDefineRealizacao() {
		return defineRealizacao;
	}

	public void setDefineRealizacao(boolean defineRealizacao) {
		this.defineRealizacao = defineRealizacao;
	}
	
	@Override
    public boolean equals(Object anObject) {
        if (!(anObject instanceof ItemTarefa)) {
            return false;
        }
        ItemTarefa otherMember = (ItemTarefa)anObject;
        return otherMember.getId().equals(getId());
    }

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
	

}
