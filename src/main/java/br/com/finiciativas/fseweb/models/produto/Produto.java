package br.com.finiciativas.fseweb.models.produto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.converters.LocalDateAttributeConverter;

@Entity
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;
	
	@ColumnTransformer(read = "UPPER(DESCRICAO)")
	private String descricao; 

	@JsonIgnore
	@ManyToMany
	private List<EtapaTrabalho> etapasTrabalho = new ArrayList<EtapaTrabalho>();

	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataCriacao;
	
	public Produto() {

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<EtapaTrabalho> getEtapasTrabalho() {
		return etapasTrabalho;
	}

	public void setEtapasTrabalho(List<EtapaTrabalho> etapasTrabalho) {
		this.etapasTrabalho = etapasTrabalho;
	}

	public LocalDate getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDate dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
