package br.com.finiciativas.fseweb.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
@DynamicUpdate(true)
public class Equipe {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;
	
	@ManyToOne
	private Consultor lider;
	
	@OneToOne
	private Consultor coordenador;
	
	@OneToOne
	private Consultor gerente;
	
	@OneToOne
	private Filial filial;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY) 
	@Where(clause = "ativo = 1")
	private List<Consultor> consultores;
	
	private boolean ativa;

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public boolean isAtiva() {
		return ativa;
	}

	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Consultor getLider() {
		return lider;
	}

	public void setLider(Consultor lider) {
		this.lider = lider;
	}
	
	public Consultor getCoordenador() {
		return coordenador;
	}
	
	public void setCoordenador(Consultor coordenador) {
		this.coordenador = coordenador;
	}
	
	public Consultor getGerente() {
	return gerente;
	}

	public void setGerente(Consultor gerente) {
		this.gerente = gerente;
	}
	
	public List<Consultor> getConsultores() {
		return consultores;
	}

	public void setConsultores(List<Consultor> consultores) {
		this.consultores = consultores;
	}

}
