package br.com.finiciativas.fseweb.models.IeM;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;

import br.com.finiciativas.fseweb.enums.Aprovacao;
import br.com.finiciativas.fseweb.enums.SituacaoReco;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.produto.Produto;

@Entity
public class RecoInterna {

	@Id
	@GeneratedValue
	private Long id;
	
	@ColumnTransformer(read = "UPPER(OPORTUNIDADE)")
	private String oportunidade;
	
	@Enumerated(EnumType.STRING)
	private Aprovacao aprovacao;
	
	private boolean recoAtiva;
	
	@ColumnTransformer(read = "UPPER(TIPO_RECO)")
	private String tipoReco;
	
	@ColumnTransformer(read = "UPPER(NOME_RECOMENDANTE)")
	private String nomeRecomendante;
	
	@ColumnTransformer(read = "UPPER(TIPO_RECOMENDANTE_DIRETO)")
	private String tipoRecomendanteDireto;
	
	private String contato;
	
	@OneToOne
	private Consultor consultor;
	
	private String data;
	
	private String campanha;
	
	private String nomeRecomendada;
	
	@OneToOne
	private Consultor comercial;
	
	@OneToOne
	private Produto produto;
	
	@Enumerated(EnumType.STRING)
	private SituacaoReco situacaoReco;
	
	private String dataSituacao;
	
	@ColumnTransformer(read = "UPPER(OBSERVACAO)")
	private String observacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Aprovacao getAprovacao() {
		return aprovacao;
	}

	public void setAprovacao(Aprovacao aprovacao) {
		this.aprovacao = aprovacao;
	}

	public boolean isRecoAtiva() {
		return recoAtiva;
	}

	public void setRecoAtiva(boolean recoAtiva) {
		this.recoAtiva = recoAtiva;
	}

	public String getTipoReco() {
		return tipoReco;
	}

	public void setTipoReco(String tipoReco) {
		this.tipoReco = tipoReco;
	}

	public String getNomeRecomendante() {
		return nomeRecomendante;
	}

	public void setNomeRecomendante(String nomeRecomendante) {
		this.nomeRecomendante = nomeRecomendante;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getNomeRecomendada() {
		return nomeRecomendada;
	}

	public void setNomeRecomendada(String nomeRecomendada) {
		this.nomeRecomendada = nomeRecomendada;
	}

	public Consultor getComercial() {
		return comercial;
	}

	public void setComercial(Consultor comercial) {
		this.comercial = comercial;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public SituacaoReco getSituacaoReco() {
		return situacaoReco;
	}

	public void setSituacaoReco(SituacaoReco situacaoReco) {
		this.situacaoReco = situacaoReco;
	}

	public String getDataSituacao() {
		return dataSituacao;
	}

	public void setDataSituacao(String dataSituacao) {
		this.dataSituacao = dataSituacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getTipoRecomendanteDireto() {
		return tipoRecomendanteDireto;
	}

	public void setTipoRecomendanteDireto(String tipoRecomendanteDireto) {
		this.tipoRecomendanteDireto = tipoRecomendanteDireto;
	}

	public String getOportunidade() {
		return oportunidade;
	}

	public void setOportunidade(String oportunidade) {
		this.oportunidade = oportunidade;
	}
	
}
