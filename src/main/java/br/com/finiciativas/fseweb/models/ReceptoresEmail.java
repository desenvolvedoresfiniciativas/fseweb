package br.com.finiciativas.fseweb.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
public class ReceptoresEmail {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToMany
	private List<Consultor> receptores;
	
	private String tipoRegistro;
	
	//private List<String> holder;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Consultor> getReceptores() {
		return receptores;
	}

	public void setReceptores(List<Consultor> receptores) {
		this.receptores = receptores;
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	
}
