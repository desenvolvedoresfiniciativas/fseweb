package br.com.finiciativas.fseweb.models.cliente;

import java.io.Serializable;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicUpdate;

import br.com.finiciativas.fseweb.abstractclass.Contato;

@Entity
@DynamicUpdate(true)
public class ContatoCliente extends Contato implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private boolean pesquisaSatisfacao;

	private boolean referencia;
 
	private boolean pessoaNfBoleto;
 
	public ContatoCliente() {

	}
	
	public ContatoCliente(ContatoCliente contatoCliente){
		if (contatoCliente != null) {
			this.setId(contatoCliente.getId());
			this.setNome(contatoCliente.getNome());
			this.setEmail(contatoCliente.getEmail());
			this.setCargo(contatoCliente.getCargo());
			this.setTelefone1(contatoCliente.getTelefone1());
			this.setTelefone2(contatoCliente.getTelefone2());
			this.pesquisaSatisfacao = contatoCliente.isPesquisaSatisfacao();
			this.referencia = contatoCliente.isReferencia();
			this.pessoaNfBoleto = contatoCliente.isPessoaNfBoleto();
			this.setAtivo(contatoCliente.isAtivo());
		}
	}

	public boolean isPesquisaSatisfacao() {
		return pesquisaSatisfacao;
	}

	public void setPesquisaSatisfacao(boolean pesquisaSatisfacao) {
		this.pesquisaSatisfacao = pesquisaSatisfacao;
	}

	public boolean isReferencia() {
		return referencia;
	}

	public void setReferencia(boolean referencia) {
		this.referencia = referencia;
	}

	public boolean isPessoaNfBoleto() {
		return pessoaNfBoleto;
	}

	public void setPessoaNfBoleto(boolean pessoaNfBoleto) {
		this.pessoaNfBoleto = pessoaNfBoleto; 
	}
	
}
