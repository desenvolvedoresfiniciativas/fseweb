package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;

@Entity
public class EtapasConclusao {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Producao producao;
	
	@ManyToOne
	private EtapaTrabalho etapa;
	
	private boolean concluido;
	
	private boolean inclusaoHoras;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public EtapaTrabalho getEtapa() {
		return etapa;
	}

	public void setEtapa(EtapaTrabalho etapa) {
		this.etapa = etapa;
	}

	public boolean isConcluido() {
		return concluido;
	}

	public void setConcluido(boolean concluido) {
		this.concluido = concluido;
	}

	public boolean isInclusaoHoras() {
		return inclusaoHoras;
	}

	public void setInclusaoHoras(boolean inclusaoHoras) {
		this.inclusaoHoras = inclusaoHoras;
	}	
	
}
