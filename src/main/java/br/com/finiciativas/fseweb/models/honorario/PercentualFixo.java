package br.com.finiciativas.fseweb.models.honorario;

import java.math.BigDecimal;

import javax.persistence.Entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class PercentualFixo extends Honorario {
	
	private static final long serialVersionUID = 1L;
	
	private Float valorPercentual;
	
	private Long contratoId;
	
	private BigDecimal limitacao = BigDecimal.valueOf(0);
	
	public PercentualFixo() {
		
	}
	
	public PercentualFixo(String nome) {
		super(nome);
	}

	public Float getValorPercentual() {
		return valorPercentual;
	}

	public void setValorPercentual(Float valorPercentual) {
		this.valorPercentual = valorPercentual;
	}

	public Long getContratoId() {
		return contratoId;
	}

	public void setContratoId(Long contratoId) {
		this.contratoId = contratoId;
	}

	public BigDecimal getLimitacao() {
		return limitacao;
	}

	public void setLimitacao(BigDecimal limitacao) {
		this.limitacao = limitacao;
	}
	
}
