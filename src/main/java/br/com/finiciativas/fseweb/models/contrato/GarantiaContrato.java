package br.com.finiciativas.fseweb.models.contrato;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;

@Entity
public class GarantiaContrato {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ColumnTransformer(read = "UPPER(DESCRICAO)")
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
