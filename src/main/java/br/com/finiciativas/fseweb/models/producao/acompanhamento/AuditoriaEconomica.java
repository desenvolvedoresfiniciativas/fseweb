package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class AuditoriaEconomica {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nomeProjeto;
	
	private String tipoDispendio;
	
	private String valorAprovado;
	
	private String valorGlosado;
	
	private String valorTotal;
	
	private String observacoes;
	
	@ManyToOne
	private Consultor feitoPor;
	
	private String porcentagemRealizado;
	
	@ManyToOne
	private Consultor validadoPor;
	
	@ManyToOne
	private Producao producao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeProjeto() {
		return nomeProjeto;
	}

	public void setNomeProjeto(String nomeProjeto) {
		this.nomeProjeto = nomeProjeto;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public String getTipoDispendio() {
		return tipoDispendio;
	}

	public void setTipoDispendio(String tipoDispendio) {
		this.tipoDispendio = tipoDispendio;
	}

	public String getValorAprovado() {
		return valorAprovado;
	}

	public void setValorAprovado(String valorAprovado) {
		this.valorAprovado = valorAprovado;
	}

	public String getValorGlosado() {
		return valorGlosado;
	}

	public void setValorGlosado(String valorGlosado) {
		this.valorGlosado = valorGlosado;
	}

	public String getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public String getPorcentagemRealizado() {
		return porcentagemRealizado;
	}

	public void setPorcentagemRealizado(String porcentagemRealizado) {
		this.porcentagemRealizado = porcentagemRealizado;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}
	
}
