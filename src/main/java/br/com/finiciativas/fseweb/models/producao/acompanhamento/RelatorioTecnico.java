package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.enums.Especialidade;
import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class RelatorioTecnico {

	@Id
	@GeneratedValue
	private Long id;
	
	private int numero;
	
	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;
	
	@Enumerated(EnumType.STRING)
	private Especialidade especialidade;
	
	private boolean documentacaoCompleta;
	
	@OneToOne
	private Consultor feitoPor;
	
	private int porcentagemFeita;
	
	@OneToOne
	private Consultor validadoPor;
	
	private String enviadoCliente;
	
	@Enumerated(EnumType.STRING)
	private Estado estado;
	
	@ManyToOne
	private Producao producao;	
	
	private String evidencia;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Especialidade getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}

	public boolean isDocumentacaoCompleta() {
		return documentacaoCompleta;
	}

	public void setDocumentacaoCompleta(boolean documentacaoCompleta) {
		this.documentacaoCompleta = documentacaoCompleta;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public int getPorcentagemFeita() {
		return porcentagemFeita;
	}

	public void setPorcentagemFeita(int porcentagemFeita) {
		this.porcentagemFeita = porcentagemFeita;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public String getEnviadoCliente() {
		return enviadoCliente;
	}

	public void setEnviadoCliente(String enviadoCliente) {
		this.enviadoCliente = enviadoCliente;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public String getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}
	
}
