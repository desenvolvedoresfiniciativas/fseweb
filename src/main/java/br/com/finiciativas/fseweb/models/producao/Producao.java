package br.com.finiciativas.fseweb.models.producao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.finiciativas.fseweb.enums.MotivoInatividade;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.enums.TipoDeApuracao;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;

@Entity
public class Producao {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String ano;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Contrato contrato;
	
	@ManyToOne
	private Cliente cliente;
	
	@ManyToOne
	private Produto produto;
	
	@Enumerated(EnumType.STRING)
	private Situacao situacao;	
	
	@OneToOne
	private Filial filial;
	
	@OneToOne
	private Equipe equipe;
	
	@OneToOne
	private Consultor consultor;
	
	@Enumerated(EnumType.STRING)
	private TipoDeApuracao tipoDeApuracao;
	
	@Enumerated(EnumType.STRING)
	private MotivoInatividade motivoInatividade;
	
	@ColumnTransformer(read = "UPPER(RAZAO_INATIVIDADE)")
	private String razaoInatividade;
	
	@ColumnTransformer(read = "UPPER(RAZAO_PREVISAO_INATIVIDADE)")
	private String razaoPrevisaoInatividade;
	
	@ColumnTransformer(read = "UPPER(TIPO_DE_NEGOCIO)")
	private String tipoDeNegocio;
	
	private boolean contratoTemporario;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "producao")
	private List<ItemValores> valores; 
	
	private String ultimoContato;
	
	private String observacoes;
	
	private String nomeProjeto;
	
	private String nomeSIA;
	
	@ManyToOne
	private PreProducao preProducao;
	
	private BigDecimal credito;
	
	private boolean oppIdentificadas;

	public String getTipoDeNegocio() {
		return tipoDeNegocio;
	}

	public void setTipoDeNegocio(String tipoDeNegocio) {
		this.tipoDeNegocio = tipoDeNegocio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}
	
	@JsonIgnore
	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Situacao getSituacao() {
		return situacao;
	}

	public void setSituacao(Situacao situacao) {
		this.situacao = situacao;
	}

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}
	
	public TipoDeApuracao getTipoDeApuracao() {
		return tipoDeApuracao;
	}

	public void setTipoDeApuracao(TipoDeApuracao tipoDeApuracao) {
		this.tipoDeApuracao = tipoDeApuracao;
	}

	public String getRazaoInatividade() {
		return razaoInatividade;
	}

	public void setRazaoInatividade(String razaoInatividade) {
		this.razaoInatividade = razaoInatividade;
	}

	public boolean isContratoTemporario() {
		return contratoTemporario;
	}

	public void setContratoTemporario(boolean contratoTemporario) {
		this.contratoTemporario = contratoTemporario;
	}

	public String getRazaoPrevisaoInatividade() {
		return razaoPrevisaoInatividade;
	}

	public void setRazaoPrevisaoInatividade(String razaoPrevisaoInatividade) {
		this.razaoPrevisaoInatividade = razaoPrevisaoInatividade;
	}

	public MotivoInatividade getMotivoInatividade() {
		return motivoInatividade;
	}

	public void setMotivoInatividade(MotivoInatividade motivoInatividade) {
		this.motivoInatividade = motivoInatividade;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemValores> getValores() {
		return valores;
	}

	public void setValores(List<ItemValores> valores) {
		this.valores = valores;
	}

	public String getUltimoContato() {
		return ultimoContato;
	}

	public void setUltimoContato(String ultimoContato) {
		this.ultimoContato = ultimoContato;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getNomeProjeto() {
		return nomeProjeto;
	}

	public void setNomeProjeto(String nomeProjeto) {
		this.nomeProjeto = nomeProjeto;
	}

	public String getNomeSIA() {
		return nomeSIA;
	}

	public void setNomeSIA(String nomeSIA) {
		this.nomeSIA = nomeSIA;
	}

	public PreProducao getPreProducao() {
		return preProducao;
	}

	public void setPreProducao(PreProducao preProducao) {
		this.preProducao = preProducao;
	}

	public BigDecimal getCredito() {
		return credito;
	}

	public void setCredito(BigDecimal credito) {
		this.credito = credito;
	}

	public boolean isOppIdentificadas() {
		return oppIdentificadas;
	}

	public void setOppIdentificadas(boolean oppIdentificadas) {
		this.oppIdentificadas = oppIdentificadas;
	}
	
}
