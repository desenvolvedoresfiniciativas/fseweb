package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class RelatorioSintetico {

	@Id
	@GeneratedValue
	private Long id;
	
	private String razaoSocial;
	
	private String setorAtividade;
	
	private int relCadastrados;
	
	private int aRedigir;
	
	private int redacao;
	
	private int correcao;
	
	private int revisao;
	
	private int compleo;
	
	private int cliente;
	
	private int aprovado;
	
	@ManyToOne
	private Producao producao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getSetorAtividade() {
		return setorAtividade;
	}

	public void setSetorAtividade(String setorAtividade) {
		this.setorAtividade = setorAtividade;
	}

	public int getRelCadastrados() {
		return relCadastrados;
	}

	public void setRelCadastrados(int relCadastrados) {
		this.relCadastrados = relCadastrados;
	}

	public int getaRedigir() {
		return aRedigir;
	}

	public void setaRedigir(int aRedigir) {
		this.aRedigir = aRedigir;
	}

	public int getRedacao() {
		return redacao;
	}

	public void setRedacao(int redacao) {
		this.redacao = redacao;
	}

	public int getCorrecao() {
		return correcao;
	}

	public void setCorrecao(int correcao) {
		this.correcao = correcao;
	}

	public int getRevisao() {
		return revisao;
	}

	public void setRevisao(int revisao) {
		this.revisao = revisao;
	}

	public int getCompleo() {
		return compleo;
	}

	public void setCompleo(int compleo) {
		this.compleo = compleo;
	}

	public int getCliente() {
		return cliente;
	}

	public void setCliente(int cliente) {
		this.cliente = cliente;
	}

	public int getAprovado() {
		return aprovado;
	}

	public void setAprovado(int aprovado) {
		this.aprovado = aprovado;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}
	
}
