package br.com.finiciativas.fseweb.models.producao;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemValores;

@Entity
public class SubProducao {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String ano;

	@ManyToOne
	@JsonIgnore
	private Producao producao;
	
	@ManyToOne
	private EtapaTrabalho etapaTrabalho;
	
	@ColumnTransformer(read = "UPPER(DESCRICAO)")
	private String descricao;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "producao")
	private List<ItemValores> valores; 
	
	private boolean gerouFaturamento;
	
	private boolean finalizado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public EtapaTrabalho getEtapaTrabalho() {
		return etapaTrabalho;
	}

	public void setEtapaTrabalho(EtapaTrabalho etapaTrabalho) {
		this.etapaTrabalho = etapaTrabalho;
	}

	public List<ItemValores> getValores() {
		return valores;
	}

	public void setValores(List<ItemValores> valores) {
		this.valores = valores;
	}

	public boolean isGerouFaturamento() {
		return gerouFaturamento;
	}

	public void setGerouFaturamento(boolean gerouFaturamento) {
		this.gerouFaturamento = gerouFaturamento;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}
	
}
