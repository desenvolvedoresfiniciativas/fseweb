package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;

import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.enums.TipoProjeto;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class Projetos {

	@Id
	@GeneratedValue
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private TipoProjeto tipo;
	
	@ColumnTransformer(read = "UPPER(DESCRICAO)")
	private String descricao;
	
	private String previsaoReceberDadosEconomicos;
	
	private String previsaoReceberDadosTecnicos;
	
	private boolean dadosEconomicosRecebidos;
	
	private boolean dadosTecnicosRecebidos;
	
	private boolean apresentadoAnoAnterior;
	
	private String valorAdiantadoAnoAnterior;	
	
	private String valorAdiantadoAnoSeguinte;
	
	private String valorRepassadoEsseAno;
	
	@Enumerated(EnumType.STRING)
	private Estado estadoTecnico;
	
	@Enumerated(EnumType.STRING)
	private Estado estadoEconomico;
	
	@OneToOne
	private Consultor relatorioEconomicoFeitoPor;
	
	@OneToOne
	private Consultor relatorioEconomicoValidadoPor;
	
	@OneToOne
	private Consultor relatorioTecnicoFeitoPor;
	
	@OneToOne
	private Consultor relatorioTecnicoValidadoPor;
	
	private String relatoriosEnviadosEm;
	
	private String relatoriosValidadosEm;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Producao producao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProjeto getTipo() {
		return tipo;
	}

	public void setTipo(TipoProjeto tipo) {
		this.tipo = tipo;
	}

	public String getPrevisaoReceberDadosEconomicos() {
		return previsaoReceberDadosEconomicos;
	}

	public void setPrevisaoReceberDadosEconomicos(String previsaoReceberDadosEconomicos) {
		this.previsaoReceberDadosEconomicos = previsaoReceberDadosEconomicos;
	}

	public String getPrevisaoReceberDadosTecnicos() {
		return previsaoReceberDadosTecnicos;
	}

	public void setPrevisaoReceberDadosTecnicos(String previsaoReceberDadosTecnicos) {
		this.previsaoReceberDadosTecnicos = previsaoReceberDadosTecnicos;
	}

	public boolean isDadosEconomicosRecebidos() {
		return dadosEconomicosRecebidos;
	}

	public void setDadosEconomicosRecebidos(boolean dadosEconomicosRecebidos) {
		this.dadosEconomicosRecebidos = dadosEconomicosRecebidos;
	}

	public boolean isDadosTecnicosRecebidos() {
		return dadosTecnicosRecebidos;
	}

	public void setDadosTecnicosRecebidos(boolean dadosTecnicosRecebidos) {
		this.dadosTecnicosRecebidos = dadosTecnicosRecebidos;
	}

	public boolean isApresentadoAnoAnterior() {
		return apresentadoAnoAnterior;
	}

	public void setApresentadoAnoAnterior(boolean apresentadoAnoAnterior) {
		this.apresentadoAnoAnterior = apresentadoAnoAnterior;
	}

	public String getValorAdiantadoAnoAnterior() {
		return valorAdiantadoAnoAnterior;
	}

	public void setValorAdiantadoAnoAnterior(String valorAdiantadoAnoAnterior) {
		this.valorAdiantadoAnoAnterior = valorAdiantadoAnoAnterior;
	}

	public String getValorAdiantadoAnoSeguinte() {
		return valorAdiantadoAnoSeguinte;
	}

	public void setValorAdiantadoAnoSeguinte(String valorAdiantadoAnoSeguinte) {
		this.valorAdiantadoAnoSeguinte = valorAdiantadoAnoSeguinte;
	}

	public String getValorRepassadoEsseAno() {
		return valorRepassadoEsseAno;
	}

	public void setValorRepassadoEsseAno(String valorRepassadoEsseAno) {
		this.valorRepassadoEsseAno = valorRepassadoEsseAno;
	}

	public Consultor getRelatorioEconomicoFeitoPor() {
		return relatorioEconomicoFeitoPor;
	}

	public void setRelatorioEconomicoFeitoPor(Consultor relatorioEconomicoFeitoPor) {
		this.relatorioEconomicoFeitoPor = relatorioEconomicoFeitoPor;
	}

	public Consultor getRelatorioEconomicoValidadoPor() {
		return relatorioEconomicoValidadoPor;
	}

	public void setRelatorioEconomicoValidadoPor(Consultor relatorioEconomicoValidadoPor) {
		this.relatorioEconomicoValidadoPor = relatorioEconomicoValidadoPor;
	}

	public Consultor getRelatorioTecnicoFeitoPor() {
		return relatorioTecnicoFeitoPor;
	}

	public void setRelatorioTecnicoFeitoPor(Consultor relatorioTecnicoFeitoPor) {
		this.relatorioTecnicoFeitoPor = relatorioTecnicoFeitoPor;
	}

	public Consultor getRelatorioTecnicoValidadoPor() {
		return relatorioTecnicoValidadoPor;
	}

	public void setRelatorioTecnicoValidadoPor(Consultor relatorioTecnicoValidadoPor) {
		this.relatorioTecnicoValidadoPor = relatorioTecnicoValidadoPor;
	}

	public String getRelatoriosEnviadosEm() {
		return relatoriosEnviadosEm;
	}

	public void setRelatoriosEnviadosEm(String relatoriosEnviadosEm) {
		this.relatoriosEnviadosEm = relatoriosEnviadosEm;
	}

	public String getRelatoriosValidadosEm() {
		return relatoriosValidadosEm;
	}

	public void setRelatoriosValidadosEm(String relatoriosValidadosEm) {
		this.relatoriosValidadosEm = relatoriosValidadosEm;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public Estado getEstadoTecnico() {
		return estadoTecnico;
	}

	public void setEstadoTecnico(Estado estadoTecnico) {
		this.estadoTecnico = estadoTecnico;
	}

	public Estado getEstadoEconomico() {
		return estadoEconomico;
	}

	public void setEstadoEconomico(Estado estadoEconomico) {
		this.estadoEconomico = estadoEconomico;
	}
	
}
