package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.enums.Resultado;

@Entity
public class ResultadoFinal {

	@Id
	@GeneratedValue
	private Long id;
	
	private String dataRecebimento;
	
	@Enumerated(EnumType.STRING)
	private Resultado resultado;
	
	private String valorQuestionado;
	
	@OneToOne
	private Acompanhamento acompanhamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(String dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public Resultado getResultado() {
		return resultado;
	}

	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}

	public String getValorQuestionado() {
		return valorQuestionado;
	}

	public void setValorQuestionado(String valorQuestionado) {
		this.valorQuestionado = valorQuestionado;
	}

	public Acompanhamento getAcompanhamento() {
		return acompanhamento;
	}

	public void setAcompanhamento(Acompanhamento acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	
}

