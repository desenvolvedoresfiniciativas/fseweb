package br.com.finiciativas.fseweb.models.CRM;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;

@Entity
public class Oportunidade {

	@Id
	@GeneratedValue
	private Long id;

	private Long oportunidade;

	@ColumnTransformer(read = "UPPER(ACCOUNT_NAME)")
	private String account_name;

	@ColumnTransformer(read = "UPPER(SERVICE_STATUS)")
	private String service_status;

	private BigDecimal fee;

	@ColumnTransformer(read = "UPPER(COUNTRY)")
	private String contry;

	@ColumnTransformer(read = "UPPER(LEAD)")
	private String lead;

	@ColumnTransformer(read = "UPPER(CLOSE_DATE)")
	private String close_date;

	private int numero_fila;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOportunidade() {
		return oportunidade;
	}

	public void setOportunidade(Long oportunidade) {
		this.oportunidade = oportunidade;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getService_status() {
		return service_status;
	}

	public void setService_status(String service_status) {
		this.service_status = service_status;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getContry() {
		return contry;
	}

	public void setContry(String contry) {
		this.contry = contry;
	}

	public String getLead() {
		return lead;
	}

	public void setLead(String lead) {
		this.lead = lead;
	}

	public String getClose_date() {
		return close_date;
	}

	public void setClose_date(String close_date) {
		this.close_date = close_date;
	}

	public int getNumero_fila() {
		return numero_fila;
	}

	public void setNumero_fila(int numero_fila) {
		this.numero_fila = numero_fila;
	}

}
