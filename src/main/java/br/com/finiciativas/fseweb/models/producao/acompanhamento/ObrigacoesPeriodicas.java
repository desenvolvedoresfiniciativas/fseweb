package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class ObrigacoesPeriodicas {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "producao_id")
	private Producao producao;
	
	private String dataRecebimento;
	
	private boolean informacaoRecebida;
	
	@OneToOne
	private Consultor feitoPor;
	
	@OneToOne
	private Consultor validadePor;
	
	private String enviadoCliente;
	
	private String aprovadoCliente;

	public ObrigacoesPeriodicas(){}
	
	public ObrigacoesPeriodicas(String dataRecebimento, boolean informacaoRecebida, Consultor feitoPor,
			Consultor validadePor, String enviadoCliente, String aprovadoCliente, Producao producao) {
		super();
		this.dataRecebimento = dataRecebimento;
		this.informacaoRecebida = informacaoRecebida;
		this.feitoPor = feitoPor;
		this.validadePor = validadePor;
		this.enviadoCliente = enviadoCliente;
		this.aprovadoCliente = aprovadoCliente;
		this.producao = producao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(String dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public boolean isInformacaoRecebida() {
		return informacaoRecebida;
	}

	public void setInformacaoRecebida(boolean informacaoRecebida) {
		this.informacaoRecebida = informacaoRecebida;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public Consultor getValidadePor() {
		return validadePor;
	}

	public void setValidadePor(Consultor validadePor) {
		this.validadePor = validadePor;
	}

	public String getEnviadoCliente() {
		return enviadoCliente;
	}

	public void setEnviadoCliente(String enviadoCliente) {
		this.enviadoCliente = enviadoCliente;
	}

	public String getAprovadoCliente() {
		return aprovadoCliente;
	}

	public void setAprovadoCliente(String aprovadoCliente) {
		this.aprovadoCliente = aprovadoCliente;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}
	
}
