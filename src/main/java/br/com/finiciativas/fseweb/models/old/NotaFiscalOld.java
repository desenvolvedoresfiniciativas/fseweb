package br.com.finiciativas.fseweb.models.old;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//tabela tb_dados_nfe FSE OLD

@Entity
public class NotaFiscalOld {

	@Id
	@GeneratedValue
	private Long id;

	private int idFaturamento;

	private String tipo;
	private String categoria;
	private String tipoServico;

	private int idEmpresa;
	private int idCampanha;

	private String anoCampanha;

	private double valorFaturacao;

	private String timing;
	private String dataNfe;

	private int numeroNfe;

	private double valorCobrar;

	private String formaPagamento;

	private double impostosRetidosIrrf;
	private double impostosRetidosCssl;
	private double impostosRetidosPis;
	private double impostosRetidosCofins;
	private double impostosDevidosPis;
	private double impostosDevidosCofins;
	private double impostosDevidosIss;

	private int duplaTributacao;

	private double porcentagemIssVariavel;
	private double impostosDevidosIssVariavel;

	private int cancelado;

	private String dataCancelamento;
	private String motivoCancelamento;
	private String comentarioCancelado;
	private String comentarioNFE;
	private String incidencia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getIdFaturamento() {
		return idFaturamento;
	}

	public void setIdFaturamento(int idFaturamento) {
		this.idFaturamento = idFaturamento;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(int idCampanha) {
		this.idCampanha = idCampanha;
	}

	public String getAnoCampanha() {
		return anoCampanha;
	}

	public void setAnoCampanha(String anoCampanha) {
		this.anoCampanha = anoCampanha;
	}

	public double getValorFaturacao() {
		return valorFaturacao;
	}

	public void setValorFaturacao(double valorFaturacao) {
		this.valorFaturacao = valorFaturacao;
	}

	public String getTiming() {
		return timing;
	}

	public void setTiming(String timing) {
		this.timing = timing;
	}

	public String getDataNfe() {
		return dataNfe;
	}

	public void setDataNfe(String dataNfe) {
		this.dataNfe = dataNfe;
	}

	public int getNumeroNfe() {
		return numeroNfe;
	}

	public void setNumeroNfe(int numeroNfe) {
		this.numeroNfe = numeroNfe;
	}

	public double getValorCobrar() {
		return valorCobrar;
	}

	public void setValorCobrar(double valorCobrar) {
		this.valorCobrar = valorCobrar;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public double getImpostosRetidosIrrf() {
		return impostosRetidosIrrf;
	}

	public void setImpostosRetidosIrrf(double impostosRetidosIrrf) {
		this.impostosRetidosIrrf = impostosRetidosIrrf;
	}

	public double getImpostosRetidosCssl() {
		return impostosRetidosCssl;
	}

	public void setImpostosRetidosCssl(double impostosRetidosCssl) {
		this.impostosRetidosCssl = impostosRetidosCssl;
	}

	public double getImpostosRetidosPis() {
		return impostosRetidosPis;
	}

	public void setImpostosRetidosPis(double impostosRetidosPis) {
		this.impostosRetidosPis = impostosRetidosPis;
	}

	public double getImpostosRetidosCofins() {
		return impostosRetidosCofins;
	}

	public void setImpostosRetidosCofins(double impostosRetidosCofins) {
		this.impostosRetidosCofins = impostosRetidosCofins;
	}

	public double getImpostosDevidosPis() {
		return impostosDevidosPis;
	}

	public void setImpostosDevidosPis(double impostosDevidosPis) {
		this.impostosDevidosPis = impostosDevidosPis;
	}

	public double getImpostosDevidosCofins() {
		return impostosDevidosCofins;
	}

	public void setImpostosDevidosCofins(double impostosDevidosCofins) {
		this.impostosDevidosCofins = impostosDevidosCofins;
	}

	public double getImpostosDevidosIss() {
		return impostosDevidosIss;
	}

	public void setImpostosDevidosIss(double impostosDevidosIss) {
		this.impostosDevidosIss = impostosDevidosIss;
	}

	public int getDuplaTributacao() {
		return duplaTributacao;
	}

	public void setDuplaTributacao(int duplaTributacao) {
		this.duplaTributacao = duplaTributacao;
	}

	public double getPorcentagemIssVariavel() {
		return porcentagemIssVariavel;
	}

	public void setPorcentagemIssVariavel(double porcentagemIssVariavel) {
		this.porcentagemIssVariavel = porcentagemIssVariavel;
	}

	public double getImpostosDevidosIssVariavel() {
		return impostosDevidosIssVariavel;
	}

	public void setImpostosDevidosIssVariavel(double impostosDevidosIssVariavel) {
		this.impostosDevidosIssVariavel = impostosDevidosIssVariavel;
	}

	public int getCancelado() {
		return cancelado;
	}

	public void setCancelado(int cancelado) {
		this.cancelado = cancelado;
	}

	public String getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(String dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public String getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public String getComentarioCancelado() {
		return comentarioCancelado;
	}

	public void setComentarioCancelado(String comentarioCancelado) {
		this.comentarioCancelado = comentarioCancelado;
	}

	public String getComentarioNFE() {
		return comentarioNFE;
	}

	public void setComentarioNFE(String comentarioNFE) {
		this.comentarioNFE = comentarioNFE;
	}

	public String getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(String incidencia) {
		this.incidencia = incidencia;
	}

}
