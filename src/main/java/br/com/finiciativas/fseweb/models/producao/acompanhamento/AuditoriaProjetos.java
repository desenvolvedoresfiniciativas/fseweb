package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class AuditoriaProjetos {

	@Id
	@GeneratedValue
	private Long id;
	
	private String nomeProjeto;
	
	private String porcentagemRealizado;
	
	@ManyToOne
	private Consultor feitoPor;
	
	@ManyToOne
	private Consultor validadoPor;
	
	@ManyToOne
	private Producao producao;
	
	private boolean atividadesRelacionadas;
	
	private boolean infoPodemSerValidadas;

	private boolean atividadesDentroPeriodo;
	
	private String categoria;
	
	private String resultado;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeProjeto() {
		return nomeProjeto;
	}

	public void setNomeProjeto(String nomeProjeto) {
		this.nomeProjeto = nomeProjeto;
	}

	public String getPorcentagemRealizado() {
		return porcentagemRealizado;
	}

	public void setPorcentagemRealizado(String porcentagemRealizado) {
		this.porcentagemRealizado = porcentagemRealizado;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public boolean isAtividadesRelacionadas() {
		return atividadesRelacionadas;
	}

	public void setAtividadesRelacionadas(boolean atividadesRelacionadas) {
		this.atividadesRelacionadas = atividadesRelacionadas;
	}

	public boolean isInfoPodemSerValidadas() {
		return infoPodemSerValidadas;
	}

	public void setInfoPodemSerValidadas(boolean infoPodemSerValidadas) {
		this.infoPodemSerValidadas = infoPodemSerValidadas;
	}

	public boolean isAtividadesDentroPeriodo() {
		return atividadesDentroPeriodo;
	}

	public void setAtividadesDentroPeriodo(boolean atividadesDentroPeriodo) {
		this.atividadesDentroPeriodo = atividadesDentroPeriodo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
}
