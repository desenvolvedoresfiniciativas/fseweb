package br.com.finiciativas.fseweb.models.valorNegocio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class ValorNegocioCL {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@JoinColumn(name="producao_id", unique=true)
	private Producao producao;
	
	@ManyToOne
	private Cliente cliente;
	
	private String tipoContrato;
	
	private String ano;
	
	private String dataReferencia;
	
	private String campanha;
	
	private String modoValor;
	
	private String etapaValor;
	
	private float valor;
	
	private float valorReducao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public String getModoValor() {
		return modoValor;
	}

	public void setModoValor(String modoValor) {
		this.modoValor = modoValor;
	}

	public String getEtapaValor() {
		return etapaValor;
	}

	public void setEtapaValor(String etapaValor) {
		this.etapaValor = etapaValor;
	}

	public String getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	public String getTipoContrato() {
		return tipoContrato;
	}

	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	public float getValorReducao() {
		return valorReducao;
	}

	public void setValorReducao(float valorReducao) {
		this.valorReducao = valorReducao;
	}
	
}
