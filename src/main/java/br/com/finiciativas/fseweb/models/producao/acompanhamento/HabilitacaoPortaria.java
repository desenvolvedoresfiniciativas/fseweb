package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class HabilitacaoPortaria {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Producao producao;
	
	private String senhaSigplaniProduto;
	
	private String senhaSigplaniModelo;
	
	private String tipoInclusao;
	
	private String descricaoItem;
	
	private boolean informacaoRecebida;
	
	private String situacao;
	
	private String dataSubmissao;
	
	private boolean habilitacaoProvisoria;
	
	private String numeroPortariaProvisoria;
	
	private boolean habilitacaoDefinitiva;
	
	private String numeroPortariaDefinitiva;
	
	private String modeloshabilitados;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}


	public String getSenhaSigplaniProduto() {
		return senhaSigplaniProduto;
	}

	public void setSenhaSigplaniProduto(String senhaSigplaniProduto) {
		this.senhaSigplaniProduto = senhaSigplaniProduto;
	}

	public String getSenhaSigplaniModelo() {
		return senhaSigplaniModelo;
	}

	public void setSenhaSigplaniModelo(String senhaSigplaniModelo) {
		this.senhaSigplaniModelo = senhaSigplaniModelo;
	}

	public String getTipoInclusao() {
		return tipoInclusao;
	}

	public void setTipoInclusao(String tipoInclusao) {
		this.tipoInclusao = tipoInclusao;
	}

	public String getDescricaoItem() {
		return descricaoItem;
	}

	public void setDescricaoItem(String descricaoItem) {
		this.descricaoItem = descricaoItem;
	}

	public boolean isInformacaoRecebida() {
		return informacaoRecebida;
	}

	public void setInformacaoRecebida(boolean informacaoRecebida) {
		this.informacaoRecebida = informacaoRecebida;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getDataSubmissao() {
		return dataSubmissao;
	}

	public void setDataSubmissao(String dataSubmissao) {
		this.dataSubmissao = dataSubmissao;
	}

	public boolean isHabilitacaoProvisoria() {
		return habilitacaoProvisoria;
	}

	public void setHabilitacaoProvisoria(boolean habilitacaoProvisoria) {
		this.habilitacaoProvisoria = habilitacaoProvisoria;
	}

	public String getNumeroPortariaProvisoria() {
		return numeroPortariaProvisoria;
	}

	public void setNumeroPortariaProvisoria(String numeroPortariaProvisoria) {
		this.numeroPortariaProvisoria = numeroPortariaProvisoria;
	}

	public boolean isHabilitacaoDefinitiva() {
		return habilitacaoDefinitiva;
	}

	public void setHabilitacaoDefinitiva(boolean habilitacaoDefinitiva) {
		this.habilitacaoDefinitiva = habilitacaoDefinitiva;
	}

	public String getNumeroPortariaDefinitiva() {
		return numeroPortariaDefinitiva;
	}

	public void setNumeroPortariaDefinitiva(String numeroPortariaDefinitiva) {
		this.numeroPortariaDefinitiva = numeroPortariaDefinitiva;
	}

	public String getModeloshabilitados() {
		return modeloshabilitados;
	}

	public void setModeloshabilitados(String modeloshabilitados) {
		this.modeloshabilitados = modeloshabilitados;
	}
	
}
