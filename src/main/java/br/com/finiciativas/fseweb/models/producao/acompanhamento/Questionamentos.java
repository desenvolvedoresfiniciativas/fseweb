package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Questionamentos {

	@Id
	@GeneratedValue
	private Long id;
	
	private String dataRecebimento;
	
	private String dataLimite;
	
	private String questionamento;
	
	private String valorQuestionado;
	
	@OneToOne
	private Acompanhamento acompanhamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(String dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public String getDataLimite() {
		return dataLimite;
	}

	public void setDataLimite(String dataLimite) {
		this.dataLimite = dataLimite;
	}

	public String getQuestionamento() {
		return questionamento;
	}

	public void setQuestionamento(String questionamento) {
		this.questionamento = questionamento;
	}

	public String getValorQuestionado() {
		return valorQuestionado;
	}

	public void setValorQuestionado(String valorQuestionado) {
		this.valorQuestionado = valorQuestionado;
	}

	public Acompanhamento getAcompanhamento() {
		return acompanhamento;
	}

	public void setAcompanhamento(Acompanhamento acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	
}
