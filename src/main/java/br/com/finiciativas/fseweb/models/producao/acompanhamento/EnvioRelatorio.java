package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
public class EnvioRelatorio {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private Consultor feitoPor;
	
	@OneToOne
	private Consultor validadoPor;
	
	private String enviadoEm;
	
	private String validadoEm;
	
	private String dataProtocolo;

	@OneToOne
	private Acompanhamento acompanhamento;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public String getEnviadoEm() {
		return enviadoEm;
	}

	public void setEnviadoEm(String enviadoEm) {
		this.enviadoEm = enviadoEm;
	}

	public String getValidadoEm() {
		return validadoEm;
	}

	public void setValidadoEm(String validadoEm) {
		this.validadoEm = validadoEm;
	}

	public String getDataProtocolo() {
		return dataProtocolo;
	}

	public void setDataProtocolo(String dataProtocolo) {
		this.dataProtocolo = dataProtocolo;
	}

	public Acompanhamento getAcompanhamento() {
		return acompanhamento;
	}

	public void setAcompanhamento(Acompanhamento acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	
}
