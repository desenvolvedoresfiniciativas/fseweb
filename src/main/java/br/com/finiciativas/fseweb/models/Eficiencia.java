package br.com.finiciativas.fseweb.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;

@Entity
public class Eficiencia {

	@Id
	@GeneratedValue
	private Long id;
	
	private String atividade;
	
	@OneToOne
	private Consultor consultor;
	
	@ManyToOne
	private Producao producao;
	
	@ManyToOne
	private EtapaTrabalho etapa;
	
	private float horas;
	
	private String dataApontamento;
	
	private String Descritivo;
	
	@ManyToOne
	private ValorNegocio vn;
	
	@ManyToOne
	private Tarefa tarefa;
	
	@ManyToOne
	private ItemTarefa itemTarefa;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAtividade() {
		return atividade;
	}

	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public EtapaTrabalho getEtapa() {
		return etapa;
	}

	public void setEtapa(EtapaTrabalho etapa) {
		this.etapa = etapa;
	}

	public float getHoras() {
		return horas;
	}

	public void setHoras(float horas) {
		this.horas = horas;
	}


	public String getDescritivo() {
		return Descritivo;
	}

	public void setDescritivo(String descritivo) {
		Descritivo = descritivo;
	}

	public ValorNegocio getVn() {
		return vn;
	}

	public void setVn(ValorNegocio vn) {
		this.vn = vn;
	}
	
	public Tarefa getTarefa() {
		return tarefa;
	}

	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}

	public ItemTarefa getItemTarefa() {
		return itemTarefa;
	}

	public void setItemTarefa(ItemTarefa itemTarefa) {
		this.itemTarefa = itemTarefa;
	}
	
	public String getDataApontamento() {
		return dataApontamento;
	}

	public void setDataApontamento(String dataApontamento) {
		this.dataApontamento = dataApontamento;
	}
	
}
