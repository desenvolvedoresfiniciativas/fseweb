package br.com.finiciativas.fseweb.models.producao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.finiciativas.fseweb.enums.SituacaoCL;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
public class PreProducao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	private Cliente cliente;
	
	private String campanha;
	
	@ManyToOne
	private Filial filial;
	
	@ManyToOne
	private Equipe equipe;
	
	@ManyToOne
	private Consultor consultor;
	
	private String tipoNegocio;
	
	private boolean primeiraCampanha;
	
	private SituacaoCL situacao;
	
	private String reactivacionLevantamiento;
	
	private String responsableReactivacion;
	
	private String ultimoContatoMail;
	
	private String ultimoContatoTelefone;
	
	private String estadoCliente;
	
	private String motivoSituacao;
	
	private String motivoReferencia;
	
	private String explicacaoMotivo;
	
	private String numeroIniciativas;
	
	private String estrategiaProjetos;
	
	private String projetosFormulacao;
	
	private String projetosInformacao;
	
	private String fechaActivacion;
	
	private String reuniaoLancamento;
	
	private String levantamentoInicial;
	
	private String diagnosticoEstrategia;
	
	private String diagnosticoValidado;
	
	private String enviadaProgramacao;

	private String envioEstrategia;
	
	private String proximaReuniao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}

	public String getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(String tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public boolean isPrimeiraCampanha() {
		return primeiraCampanha;
	}

	public void setPrimeiraCampanha(boolean primeiraCampanha) {
		this.primeiraCampanha = primeiraCampanha;
	}

	public SituacaoCL getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoCL situacao) {
		this.situacao = situacao;
	}

	public String getReactivacionLevantamiento() {
		return reactivacionLevantamiento;
	}

	public void setReactivacionLevantamiento(String reactivacionLevantamiento) {
		this.reactivacionLevantamiento = reactivacionLevantamiento;
	}

	public String getResponsableReactivacion() {
		return responsableReactivacion;
	}

	public void setResponsableReactivacion(String responsableReactivacion) {
		this.responsableReactivacion = responsableReactivacion;
	}

	public String getUltimoContatoMail() {
		return ultimoContatoMail;
	}

	public void setUltimoContatoMail(String ultimoContatoMail) {
		this.ultimoContatoMail = ultimoContatoMail;
	}

	public String getUltimoContatoTelefone() {
		return ultimoContatoTelefone;
	}

	public void setUltimoContatoTelefone(String ultimoContatoTelefone) {
		this.ultimoContatoTelefone = ultimoContatoTelefone;
	}

	public String getEstadoCliente() {
		return estadoCliente;
	}

	public void setEstadoCliente(String estadoCliente) {
		this.estadoCliente = estadoCliente;
	}

	public String getMotivoSituacao() {
		return motivoSituacao;
	}

	public void setMotivoSituacao(String motivoSituacao) {
		this.motivoSituacao = motivoSituacao;
	}

	public String getMotivoReferencia() {
		return motivoReferencia;
	}

	public void setMotivoReferencia(String motivoReferencia) {
		this.motivoReferencia = motivoReferencia;
	}

	public String getExplicacaoMotivo() {
		return explicacaoMotivo;
	}

	public void setExplicacaoMotivo(String explicacaoMotivo) {
		this.explicacaoMotivo = explicacaoMotivo;
	}

	public String getNumeroIniciativas() {
		return numeroIniciativas;
	}

	public void setNumeroIniciativas(String numeroIniciativas) {
		this.numeroIniciativas = numeroIniciativas;
	}

	public String getEstrategiaProjetos() {
		return estrategiaProjetos;
	}

	public void setEstrategiaProjetos(String estrategiaProjetos) {
		this.estrategiaProjetos = estrategiaProjetos;
	}

	public String getProjetosFormulacao() {
		return projetosFormulacao;
	}

	public void setProjetosFormulacao(String projetosFormulacao) {
		this.projetosFormulacao = projetosFormulacao;
	}

	public String getProjetosInformacao() {
		return projetosInformacao;
	}

	public void setProjetosInformacao(String projetosInformacao) {
		this.projetosInformacao = projetosInformacao;
	}

	public String getFechaActivacion() {
		return fechaActivacion;
	}

	public void setFechaActivacion(String fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	public String getReuniaoLancamento() {
		return reuniaoLancamento;
	}

	public void setReuniaoLancamento(String reuniaoLancamento) {
		this.reuniaoLancamento = reuniaoLancamento;
	}

	public String getLevantamentoInicial() {
		return levantamentoInicial;
	}

	public void setLevantamentoInicial(String levantamentoInicial) {
		this.levantamentoInicial = levantamentoInicial;
	}

	public String getDiagnosticoEstrategia() {
		return diagnosticoEstrategia;
	}

	public void setDiagnosticoEstrategia(String diagnosticoEstrategia) {
		this.diagnosticoEstrategia = diagnosticoEstrategia;
	}

	public String getDiagnosticoValidado() {
		return diagnosticoValidado;
	}

	public void setDiagnosticoValidado(String diagnosticoValidado) {
		this.diagnosticoValidado = diagnosticoValidado;
	}

	public String getEnviadaProgramacao() {
		return enviadaProgramacao;
	}

	public void setEnviadaProgramacao(String enviadaProgramacao) {
		this.enviadaProgramacao = enviadaProgramacao;
	}

	public String getEnvioEstrategia() {
		return envioEstrategia;
	}

	public void setEnvioEstrategia(String envioEstrategia) {
		this.envioEstrategia = envioEstrategia;
	}

	public String getProximaReuniao() {
		return proximaReuniao;
	}

	public void setProximaReuniao(String proximaReuniao) {
		this.proximaReuniao = proximaReuniao;
	}
	
}
