package br.com.finiciativas.fseweb.models.IeM;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;

import br.com.finiciativas.fseweb.enums.SituacaoReco;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.produto.Produto;

@Entity
public class RecoInternacional {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ColumnTransformer(read = "UPPER(OPORTUNIDADE)")
	private String oportunidade;
	
	private boolean recoAtiva;
	
	@ColumnTransformer(read = "UPPER(STATUS_RECO)")
	private String statusReco;
	
	@ColumnTransformer(read = "UPPER(RECOMENDANTE_NOME)")
	private String recomendanteNome;
	
	@ColumnTransformer(read = "UPPER(CONTATO)")
	private String contato;
	
	private String campanha;
	
	private String data;
	
	@OneToOne
	private Filial filialRecomendante;
	
	@OneToOne
	private Filial filialRecomendada;
	
	private String nomeRecomendada;
	
	@OneToOne
	private Consultor consultorRecomendante;
	
	@OneToOne
	private Consultor comercial;
	
	@OneToOne
	private Produto produto;
	
	@Enumerated(EnumType.STRING)
	private SituacaoReco situacaoReco;
	
	private String dataSituacao;
	
	private String observacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isRecoAtiva() {
		return recoAtiva;
	}

	public void setRecoAtiva(boolean recoAtiva) {
		this.recoAtiva = recoAtiva;
	}

	public String getStatusReco() {
		return statusReco;
	}

	public void setStatusReco(String statusReco) {
		this.statusReco = statusReco;
	}

	public String getRecomendanteNome() {
		return recomendanteNome;
	}

	public void setRecomendanteNome(String recomendanteNome) {
		this.recomendanteNome = recomendanteNome;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Filial getFilialRecomendante() {
		return filialRecomendante;
	}

	public void setFilialRecomendante(Filial filialRecomendante) {
		this.filialRecomendante = filialRecomendante;
	}

	public Filial getFilialRecomendada() {
		return filialRecomendada;
	}

	public void setFilialRecomendada(Filial filialRecomendada) {
		this.filialRecomendada = filialRecomendada;
	}

	public String getNomeRecomendada() {
		return nomeRecomendada;
	}

	public void setNomeRecomendada(String nomeRecomendada) {
		this.nomeRecomendada = nomeRecomendada;
	}

	public Consultor getComercial() {
		return comercial;
	}

	public void setComercial(Consultor comercial) {
		this.comercial = comercial;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public SituacaoReco getSituacaoReco() {
		return situacaoReco;
	}

	public void setSituacaoReco(SituacaoReco situacaoReco) {
		this.situacaoReco = situacaoReco;
	}

	public String getDataSituacao() {
		return dataSituacao;
	}

	public void setDataSituacao(String dataSituacao) {
		this.dataSituacao = dataSituacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Consultor getConsultorRecomendante() {
		return consultorRecomendante;
	}

	public void setConsultorRecomendante(Consultor consultorRecomendante) {
		this.consultorRecomendante = consultorRecomendante;
	}

	public String getOportunidade() {
		return oportunidade;
	}

	public void setOportunidade(String oportunidade) {
		this.oportunidade = oportunidade;
	}
	
}
