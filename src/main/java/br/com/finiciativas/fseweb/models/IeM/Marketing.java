package br.com.finiciativas.fseweb.models.IeM;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;

import br.com.finiciativas.fseweb.enums.DadosRecomendante;
import br.com.finiciativas.fseweb.enums.SituacaoReco;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.produto.Produto;

@Entity
public class Marketing {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ColumnTransformer(read = "UPPER(OPORTUNIDADE)")
	private String oportunidade;
	
	private boolean recoAtiva;
	
	@Enumerated(EnumType.STRING)
	private DadosRecomendante dadosRecomendante;
	
	private String site;
	
	private String data;
	
	private String campanha;
	
	@ColumnTransformer(read = "UPPER(OBSERVACAO)")
	private String observacao;
	
	@ColumnTransformer(read = "UPPER(NOME_RECOMENDADA)")
	private String nomeRecomendada;
	
	@OneToOne
	private Consultor comercial;
	
	@OneToOne
	private Produto produto;
	
	@Enumerated(EnumType.STRING)
	private SituacaoReco situacaoReco;
	
	private String dataSituacao;
	
	@ColumnTransformer(read = "UPPER(OBSERVACAO_RECOMENDADA)")
	private String observacaoRecomendada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isRecoAtiva() {
		return recoAtiva;
	}

	public void setRecoAtiva(boolean recoAtiva) {
		this.recoAtiva = recoAtiva;
	}

	public DadosRecomendante getDadosRecomendante() {
		return dadosRecomendante;
	}

	public void setDadosRecomendante(DadosRecomendante dadosRecomendante) {
		this.dadosRecomendante = dadosRecomendante;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getNomeRecomendada() {
		return nomeRecomendada;
	}

	public void setNomeRecomendada(String nomeRecomendada) {
		this.nomeRecomendada = nomeRecomendada;
	}

	public Consultor getComercial() {
		return comercial;
	}

	public void setComercial(Consultor comercial) {
		this.comercial = comercial;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public SituacaoReco getSituacaoReco() {
		return situacaoReco;
	}

	public void setSituacaoReco(SituacaoReco situacaoReco) {
		this.situacaoReco = situacaoReco;
	}

	public String getDataSituacao() {
		return dataSituacao;
	}

	public void setDataSituacao(String dataSituacao) {
		this.dataSituacao = dataSituacao;
	}

	public String getObservacaoRecomendada() {
		return observacaoRecomendada;
	}

	public void setObservacaoRecomendada(String observacaoRecomendada) {
		this.observacaoRecomendada = observacaoRecomendada;
	}

	public String getOportunidade() {
		return oportunidade;
	}

	public void setOportunidade(String oportunidade) {
		this.oportunidade = oportunidade;
	}

}
