package br.com.finiciativas.fseweb.models.instituicao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import br.com.finiciativas.fseweb.abstractclass.Empresa;
import br.com.finiciativas.fseweb.enums.TipoInstituicao;

@Entity
@DynamicUpdate(true)
@Where(clause = "ativo = 1")
public class Instituicao extends Empresa {

	@ColumnTransformer(read = "UPPER(OBSERVACOES)")
	private String observacoes;
	
	@Enumerated(EnumType.STRING)
	private TipoInstituicao tipo;
	
	private boolean contratoVigente;
	
	@OneToMany
	private List<ContatoInstituicao> contatos = new ArrayList<>();
	
	@OneToOne(cascade = CascadeType.ALL)
	private EnderecoInstituicao endereco;

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public TipoInstituicao getTipo() {
		return tipo;
	}

	public void setTipo(TipoInstituicao tipo) {
		this.tipo = tipo;
	}

	public boolean isContratoVigente() {
		return contratoVigente;
	}

	public void setContratoVigente(boolean contratoVigente) {
		this.contratoVigente = contratoVigente;
	}

	public List<ContatoInstituicao> getContatos() {
		return contatos;
	}

	public void setContatos(List<ContatoInstituicao> contatos) {
		this.contatos = contatos;
	}

	public EnderecoInstituicao getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoInstituicao endereco) {
		this.endereco = endereco;
	}
	
}
