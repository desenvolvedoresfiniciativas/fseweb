package br.com.finiciativas.fseweb.models.valorNegocioCL;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;

@Entity
public class ValorFiscalizado {

	@Id
	@GeneratedValue
	private Long id;
	
	private String valor;
	
	private String campanha;
	
	private String anoGasto;
	
	private String fase;
	
	private String rut;
	
	@OneToOne
	@JoinColumn(name="subProducao_id", unique=false)
	private SubProducao subProducao;
	
	@OneToOne
	@JoinColumn(name="producao_id", unique=false)
	private Producao producao;
	
	@OneToOne
	@JoinColumn(name="contrato_id", unique=false)
	private Contrato contrato;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public SubProducao getSubProducao() {
		return subProducao;
	}

	public void setSubProducao(SubProducao subProducao) {
		this.subProducao = subProducao;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public String getAnoGasto() {
		return anoGasto;
	}

	public void setAnoGasto(String anoGasto) {
		this.anoGasto = anoGasto;
	}

	public String getFase() {
		return fase;
	}

	public void setFase(String fase) {
		this.fase = fase;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}
	
	
}
