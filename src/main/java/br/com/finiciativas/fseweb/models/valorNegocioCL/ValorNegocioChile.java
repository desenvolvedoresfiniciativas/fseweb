package br.com.finiciativas.fseweb.models.valorNegocioCL;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.contrato.Contrato;

@Entity
public class ValorNegocioChile {

	@Id
	@GeneratedValue
	private Long id;
	
	private String campanha;
	
	private String ano_fiscal;
	
	private String nome_empresa;
	
	private String etapa;
	
	private String fee;
	
	private String valor;
	
	private String tipo;
	
	private String tipo_negocio;
	
	private String situacao;
	
	@OneToOne
	@JoinColumn(name="contrato_id", unique = false)
	private Contrato contrato;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getAno_fiscal() {
		return ano_fiscal;
	}

	public void setAno_fiscal(String ano_fiscal) {
		this.ano_fiscal = ano_fiscal;
	}

	public String getNome_empresa() {
		return nome_empresa;
	}

	public void setNome_empresa(String nome_empresa) {
		this.nome_empresa = nome_empresa;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipo_negocio() {
		return tipo_negocio;
	}

	public void setTipo_negocio(String tipo_negocio) {
		this.tipo_negocio = tipo_negocio;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	
	
}
