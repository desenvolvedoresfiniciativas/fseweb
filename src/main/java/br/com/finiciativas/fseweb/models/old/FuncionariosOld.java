package br.com.finiciativas.fseweb.models.old;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//tabela tb_funcionarios FSE OLD 

@Entity
public class FuncionariosOld {

	@Id
	@GeneratedValue
	private Long id;

	private String nome;
	private String sobrenome;

	private char sigla;

	private String email;
	private String cargo;
	private String grupo;
	private String senha;

	private int situacao;
	private int adm;
	private int comercial;
	private int diretoria;
	private int responsavelFilial;
	private int juridico;
	private int responsavelTecnico;
	private int liderTecnico;
	private int tecnico;
	private int filtros;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public char getSigla() {
		return sigla;
	}

	public void setSigla(char sigla) {
		this.sigla = sigla;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getSituacao() {
		return situacao;
	}

	public void setSituacao(int situacao) {
		this.situacao = situacao;
	}

	public int getAdm() {
		return adm;
	}

	public void setAdm(int adm) {
		this.adm = adm;
	}

	public int getComercial() {
		return comercial;
	}

	public void setComercial(int comercial) {
		this.comercial = comercial;
	}

	public int getDiretoria() {
		return diretoria;
	}

	public void setDiretoria(int diretoria) {
		this.diretoria = diretoria;
	}

	public int getResponsavelFilial() {
		return responsavelFilial;
	}

	public void setResponsavelFilial(int responsavelFilial) {
		this.responsavelFilial = responsavelFilial;
	}

	public int getJuridico() {
		return juridico;
	}

	public void setJuridico(int juridico) {
		this.juridico = juridico;
	}

	public int getResponsavelTecnico() {
		return responsavelTecnico;
	}

	public void setResponsavelTecnico(int responsavelTecnico) {
		this.responsavelTecnico = responsavelTecnico;
	}

	public int getLiderTecnico() {
		return liderTecnico;
	}

	public void setLiderTecnico(int liderTecnico) {
		this.liderTecnico = liderTecnico;
	}

	public int getTecnico() {
		return tecnico;
	}

	public void setTecnico(int tecnico) {
		this.tecnico = tecnico;
	}

	public int getFiltros() {
		return filtros;
	}

	public void setFiltros(int filtros) {
		this.filtros = filtros;
	}

}
