package br.com.finiciativas.fseweb.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;

@Entity
public class Parcela {
	
	@Id
	@GeneratedValue
    private Long id;
	
	private int nParcela;
	
	private BigDecimal valorParcela;
	private String dataCobranca;
	private String dataVencimento;
	private boolean pago;
	
	@ManyToOne
	@JsonIgnore
	private NotaFiscal notaFiscal;
	
	
	public int getnParcela() {
		return nParcela;
	}
	public void setnParcela(int nParcela) {
		this.nParcela = nParcela;
	}
	public BigDecimal getValorParcela() {
		return valorParcela;
	}
	public void setValorParcela(BigDecimal valorParcela) {
		this.valorParcela = valorParcela;
	}
	public String getDataCobranca() {
		return dataCobranca;
	}
	public void setDataCobranca(String dataCobranca) {
		this.dataCobranca = dataCobranca;
	}
	public String getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public boolean isPago() {
		return pago;
	}
	public void setPago(boolean pago) {
		this.pago = pago;
	}
	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}
	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
