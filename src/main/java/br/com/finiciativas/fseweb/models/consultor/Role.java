package br.com.finiciativas.fseweb.models.consultor;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;
import org.springframework.security.core.GrantedAuthority;

@Entity
public class Role implements GrantedAuthority {

	private static final long serialVersionUID = 1L;

	@Id
	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;

	@ColumnTransformer(read = "UPPER(DESCRICAO)")
	private String descricao;
	
	public Role() {
		
	}
	
	public Role(String nome, String descricao) {
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public String getAuthority() {
		return this.nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
