package br.com.finiciativas.fseweb.models.cliente;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import br.com.finiciativas.fseweb.abstractclass.Empresa;
import br.com.finiciativas.fseweb.enums.CategoriaProblematica;
import br.com.finiciativas.fseweb.enums.Divisao;
import br.com.finiciativas.fseweb.enums.MotivoFidelizacao;
import br.com.finiciativas.fseweb.enums.SetorAtividade;

@Entity
@DynamicUpdate(true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Cliente extends Empresa implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String rut;
	private String nit;

	@Enumerated(EnumType.STRING)
	private SetorAtividade setorAtividade;
	
	@Enumerated(EnumType.STRING)
	private CategoriaProblematica categoriaProblematica;
	
	@Enumerated(EnumType.STRING)
	private Divisao divisao;
	
	@Enumerated(EnumType.STRING)
	private MotivoFidelizacao motivoFidelizacao;
	
	//@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL)
	private List<EnderecoCliente> endereco;
	
	@Where(clause = "ativo = 1")
	@OneToMany
	//@JsonIgnore
	private List<ContatoCliente> contatos = new ArrayList<>();
	
	private boolean possuiContrato;
	
	@ColumnTransformer(read = "UPPER(GRUPO_ECONOMICO)")
	private String grupoEconomico;
	
	private String categoria;
	
	private String caminhoPasta;
	
	private String caminhoACT;
	
	private boolean clean;
	
	private String dataFidelizacao;
	
	private String fidelizacaoElogio;
	
	private String score;
	
	private String dataVencimentoFidelizacao;
	
	private String caminhoRECO;
	
	public String getCaminhoRECO() {
		return caminhoRECO;
	}

	public void setCaminhoRECO(String caminhoRECO) {
		this.caminhoRECO = caminhoRECO;
	}

	
	public String getCaminhoACT() {
		return caminhoACT;
	}

	public void setCaminhoACT(String caminhoACT) {
		this.caminhoACT = caminhoACT;
	}

	private boolean fidelizado;
	
	public MotivoFidelizacao getMotivoFidelizacao() {
		return motivoFidelizacao;
	}

	public String getDataVencimentoFidelizacao() {
		return dataVencimentoFidelizacao;
	}

	public void setDataVencimentoFidelizacao(String dataVencimentoFidelizacao) {
		this.dataVencimentoFidelizacao = dataVencimentoFidelizacao;
	}

	public void setMotivoFidelizacao(MotivoFidelizacao motivoFidelizacao) {
		this.motivoFidelizacao = motivoFidelizacao;
	}

	public boolean isFidelizado() {
		return fidelizado;
	}

	public void setFidelizado(boolean fidelizado) {
		this.fidelizado = fidelizado;
	}

	public Cliente() {

	}
	
	public Cliente(Cliente empresa){
		if (empresa != null) {
			this.setId(empresa.getId());
			this.setCnpj(empresa.getCnpj());
			this.setRut(empresa.getRut());
			this.setRazaoSocial(empresa.getRazaoSocial());
			this.setSite(empresa.getSite());
			this.setEndereco(empresa.getEndereco());
			this.setAtivo(empresa.isAtivo());
			this.setorAtividade = empresa.getSetorAtividade();
			this.categoriaProblematica = empresa.getCategoriaProblematica();
			this.setDivisao(empresa.getDivisao());
		}
	}

	public SetorAtividade getSetorAtividade() {
		return setorAtividade;
	}

	public void setSetorAtividade(SetorAtividade setorAtividade) {
		this.setorAtividade = setorAtividade;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	public List<EnderecoCliente> getEndereco() {
		return endereco;
	}

	public void setEndereco(List<EnderecoCliente> endereco) {
		this.endereco = endereco;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	public List<ContatoCliente> getContatos() {
		return contatos;
	}

	public void setContatos(List<ContatoCliente> contatos) {
		this.contatos = contatos;
	}

	public String getGrupoEconomico() {
		return grupoEconomico;
	}

	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public boolean isPossuiContrato() {
		return possuiContrato;
	}

	public void setPossuiContrato(boolean possuiContrato) {
		this.possuiContrato = possuiContrato;
	}
	
	public Divisao getDivisao() {
		return divisao;
	}

	public void setDivisao(Divisao divisao) {
		this.divisao = divisao;
	}

	@Override
    public boolean equals(Object anObject) {
        if (!(anObject instanceof Cliente)) {
            return false;
        }
        Cliente otherMember = (Cliente)anObject;
        return otherMember.getId().equals(getId());
    }

	public String getCaminhoPasta() {
		return caminhoPasta;
	}
	
	public CategoriaProblematica getCategoriaProblematica() {
		return categoriaProblematica;
	}

	public void setCategoriaProblematica(CategoriaProblematica categoriaProblematica) {
		this.categoriaProblematica = categoriaProblematica;
	}

	public void setCaminhoPasta(String caminhoPasta) {
		this.caminhoPasta = caminhoPasta;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public boolean isClean() {
		return clean;
	}

	public void setClean(boolean clean) {
		this.clean = clean;
	}

	public String getDataFidelizacao() {
		return dataFidelizacao;
	}

	public void setDataFidelizacao(String dataFidelizacao) {
		this.dataFidelizacao = dataFidelizacao;
	}

	public String getFidelizacaoElogio() {
		return fidelizacaoElogio;
	}

	public void setFidelizacaoElogio(String fidelizacaoElogio) {
		this.fidelizacaoElogio = fidelizacaoElogio;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}
	
}
