package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class BalanceteCalculo {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private Consultor feitoPor;
	
	private int porcentagemRealizado;
	
	@OneToOne
	private Consultor validadoPor;
	
	private String valorRH;
	
	private String valorMateriais;
	
	private String valorServicosTerceiros;
	
	private String valorOutrasDespesas;
	
	private String valorTotal;
	
	private String exclusaoUtilizada;
	
	private String reducaoImposto;
	
	private String enviadoEm;
	
	private String validadoEm;
	
	private boolean versaoFinal;
	
	private String relatorioCalculo;
	
	private int reunioesNecessarias;
	
	private int versao;
	
	private boolean gerouFaturamento;
	
	@ManyToOne
	private Producao producao;
	
//	private boolean consolidado;
	
	private boolean validado;

	private String observacoes;
	
	private boolean prejuizo;
	
	private String evidencia;

	public boolean isPrejuizo() {
		return prejuizo;
	}

	public void setPrejuizo(boolean prejuizo) {
		this.prejuizo = prejuizo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public int getPorcentagemRealizado() {
		return porcentagemRealizado;
	}

	public void setPorcentagemRealizado(int porcentagemRealizado) {
		this.porcentagemRealizado = porcentagemRealizado;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public String getExclusaoUtilizada() {
		return exclusaoUtilizada;
	}

	public void setExclusaoUtilizada(String exclusaoUtilizada) {
		this.exclusaoUtilizada = exclusaoUtilizada;
	}

	public String getEnviadoEm() {
		return enviadoEm;
	}

	public void setEnviadoEm(String enviadoEm) {
		this.enviadoEm = enviadoEm;
	}

	public String getValidadoEm() {
		return validadoEm;
	}

	public void setValidadoEm(String validadoEm) {
		this.validadoEm = validadoEm;
	}

	public boolean isVersaoFinal() {
		return versaoFinal;
	}

	public void setVersaoFinal(boolean versaoFinal) {
		this.versaoFinal = versaoFinal;
	}

	public String getRelatorioCalculo() {
		return relatorioCalculo;
	}

	public void setRelatorioCalculo(String relatorioCalculo) {
		this.relatorioCalculo = relatorioCalculo;
	}

	public int getReunioesNecessarias() {
		return reunioesNecessarias;
	}

	public void setReunioesNecessarias(int reunioesNecessarias) {
		this.reunioesNecessarias = reunioesNecessarias;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public String getValorRH() {
		return valorRH;
	}

	public void setValorRH(String valorRH) {
		this.valorRH = valorRH;
	}

	public String getValorMateriais() {
		return valorMateriais;
	}

	public void setValorMateriais(String valorMateriais) {
		this.valorMateriais = valorMateriais;
	}

	public String getValorServicosTerceiros() {
		return valorServicosTerceiros;
	}

	public void setValorServicosTerceiros(String valorServicosTerceiros) {
		this.valorServicosTerceiros = valorServicosTerceiros;
	}

	public String getValorOutrasDespesas() {
		return valorOutrasDespesas;
	}

	public void setValorOutrasDespesas(String valorOutrasDespesas) {
		this.valorOutrasDespesas = valorOutrasDespesas;
	}

	public String getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getReducaoImposto() {
		return reducaoImposto;
	}

	public void setReducaoImposto(String reducaoImposto) {
		this.reducaoImposto = reducaoImposto;
	}

	public int getVersao() {
		return versao;
	}

	public void setVersao(int versao) {
		this.versao = versao;
	}

	public boolean isGerouFaturamento() {
		return gerouFaturamento;
	}

	public void setGerouFaturamento(boolean gerouFaturamento) {
		this.gerouFaturamento = gerouFaturamento;
	}

//	public boolean isConsolidado() {
//		return consolidado;
//	}

//	public void setConsolidado(boolean consolidado) {
//		this.consolidado = consolidado;
//	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}
	
	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getEvidencia() {
		return evidencia;
	}

	public void setEvidencia(String evidencia) {
		this.evidencia = evidencia;
	}
	
}
