package br.com.finiciativas.fseweb.models.contrato;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;

@Entity
public class EtapasPagamento {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private Contrato contrato;

	private String dataFaturamento;

	@OneToOne
	private EtapaTrabalho etapa1;
	private String honorario1;
	private BigDecimal valor1;
	private float porcentagem1;
	@OneToOne
	private PercentualEscalonado escalonado1;
	
	@OneToOne
	private EtapaTrabalho etapa2;
	private String honorario2;
	private BigDecimal valor2;
	private float porcentagem2;
	@OneToOne
	private PercentualEscalonado escalonado2;

	@OneToOne
	private EtapaTrabalho etapa3;
	private String honorario3;
	private BigDecimal valor3;
	private float porcentagem3;
	@OneToOne
	private PercentualEscalonado escalonado3;

	@OneToOne
	private EtapaTrabalho etapa4;
	private String honorario4;
	private BigDecimal valor4;
	private float porcentagem4;
	@OneToOne
	private PercentualEscalonado escalonado4;

	private boolean etapa1ok;
	private boolean etapa2ok;
	private boolean etapa3ok;
	private boolean etapa4ok;
	

	private int parcela1 = 1;
	private int parcela2 = 1;
	private int parcela3 = 1;
	private int parcela4 = 1;
	
	@OneToOne
	private Comissao comissao;

	public Comissao getComissao() {
		return comissao;
	}

	public void setComissao(Comissao comissao) {
		this.comissao = comissao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EtapaTrabalho getEtapa1() {
		return etapa1;
	}

	public void setEtapa1(EtapaTrabalho etapa1) {
		this.etapa1 = etapa1;
	}

	public float getPorcentagem1() {
		return porcentagem1;
	}

	public void setPorcentagem1(float porcentagem1) {
		System.out.println("------------------P1----------------" + porcentagem1);
		this.porcentagem1 = porcentagem1;
	}

	public EtapaTrabalho getEtapa2() {
		return etapa2;
	}

	public void setEtapa2(EtapaTrabalho etapa2) {
		this.etapa2 = etapa2;
	}

	public float getPorcentagem2() {
		return porcentagem2;
	}

	public void setPorcentagem2(float porcentagem2) {
		this.porcentagem2 = porcentagem2;
	}

	public EtapaTrabalho getEtapa3() {
		return etapa3;
	}

	public void setEtapa3(EtapaTrabalho etapa3) {
		this.etapa3 = etapa3;
	}

	public float getPorcentagem3() {
		return porcentagem3;
	}

	public void setPorcentagem3(float porcentagem3) {
		this.porcentagem3 = porcentagem3;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public boolean isEtapa1ok() {
		return etapa1ok;
	}

	public void setEtapa1ok(boolean etapa1ok) {
		this.etapa1ok = etapa1ok;
	}

	public boolean isEtapa2ok() {
		return etapa2ok;
	}

	public void setEtapa2ok(boolean etapa2ok) {
		this.etapa2ok = etapa2ok;
	}

	public boolean isEtapa3ok() {
		return etapa3ok;
	}

	public void setEtapa3ok(boolean etapa3ok) {
		this.etapa3ok = etapa3ok;
	}

	public String getDataFaturamento() {
		return dataFaturamento;
	}

	public void setDataFaturamento(String dataFaturamento) {
		this.dataFaturamento = dataFaturamento;
	}

	public int getParcela1() {
		return parcela1;
	}

	public void setParcela1(int parcela1) {
		this.parcela1 = parcela1;
	}

	public int getParcela2() {
		return parcela2;
	}

	public void setParcela2(int parcela2) {
		this.parcela2 = parcela2;
	}

	public int getParcela3() {
		return parcela3;
	}

	public void setParcela3(int parcela3) {
		this.parcela3 = parcela3;
	}

	public EtapaTrabalho getEtapa4() {
		return etapa4;
	}

	public void setEtapa4(EtapaTrabalho etapa4) {
		this.etapa4 = etapa4;
	}

	public float getPorcentagem4() {
		return porcentagem4;
	}

	public void setPorcentagem4(float porcentagem4) {
		this.porcentagem4 = porcentagem4;
	}

	public boolean isEtapa4ok() {
		return etapa4ok;
	}

	public void setEtapa4ok(boolean etapa4ok) {
		this.etapa4ok = etapa4ok;
	}

	public int getParcela4() {
		return parcela4;
	}

	public void setParcela4(int parcela4) {
		this.parcela4 = parcela4;
	}

	public String getHonorario1() {
		return honorario1;
	}

	public void setHonorario1(String honorario1) {
		this.honorario1 = honorario1;
	}


	public String getHonorario2() {
		return honorario2;
	}

	public void setHonorario2(String honorario2) {
		this.honorario2 = honorario2;
	}

	public String getHonorario3() {
		return honorario3;
	}

	public void setHonorario3(String honorario3) {
		this.honorario3 = honorario3;
	}

	public String getHonorario4() {
		return honorario4;
	}

	public void setHonorario4(String honorario4) {
		this.honorario4 = honorario4;
	}

	public BigDecimal getValor1() {
		return valor1;
	}

	public void setValor1(BigDecimal valor1) {
		this.valor1 = valor1;
	}

	public BigDecimal getValor2() {
		return valor2;
	}

	public void setValor2(BigDecimal valor2) {
		this.valor2 = valor2;
	}

	public BigDecimal getValor3() {
		return valor3;
	}

	public void setValor3(BigDecimal valor3) {
		this.valor3 = valor3;
	}

	public BigDecimal getValor4() {
		return valor4;
	}

	public void setValor4(BigDecimal valor4) {
		this.valor4 = valor4;
	}

	public PercentualEscalonado getEscalonado1() {
		return escalonado1;
	}

	public void setEscalonado1(PercentualEscalonado escalonado1) {
		this.escalonado1 = escalonado1;
	}

	public PercentualEscalonado getEscalonado2() {
		return escalonado2;
	}

	public void setEscalonado2(PercentualEscalonado escalonado2) {
		this.escalonado2 = escalonado2;
	}

	public PercentualEscalonado getEscalonado3() {
		return escalonado3;
	}

	public void setEscalonado3(PercentualEscalonado escalonado3) {
		this.escalonado3 = escalonado3;
	}

	public PercentualEscalonado getEscalonado4() {
		return escalonado4;
	}

	public void setEscalonado4(PercentualEscalonado escalonado4) {
		this.escalonado4 = escalonado4;
	}

}
