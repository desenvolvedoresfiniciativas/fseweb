package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
public class RelatorioTecnicoFinanceiro {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private Consultor feitoPor;
	
	private int porcentagemRealizado;
	
	@OneToOne
	private Consultor validadoPor;
	
	private String enviadoEm;
	
	@Enumerated(EnumType.STRING)
	private Estado estado;

	@OneToOne
	private Acompanhamento acompanhamento;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public int getPorcentagemRealizado() {
		return porcentagemRealizado;
	}

	public void setPorcentagemRealizado(int porcentagemRealizado) {
		this.porcentagemRealizado = porcentagemRealizado;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public String getEnviadoEm() {
		return enviadoEm;
	}

	public void setEnviadoEm(String enviadoEm) {
		this.enviadoEm = enviadoEm;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Acompanhamento getAcompanhamento() {
		return acompanhamento;
	}

	public void setAcompanhamento(Acompanhamento acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	
}
