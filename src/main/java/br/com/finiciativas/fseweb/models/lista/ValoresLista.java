package br.com.finiciativas.fseweb.models.lista;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnTransformer;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;

@Entity
public class ValoresLista {

	@Id
	@GeneratedValue
	Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private ItemTarefa item;
	
	@ColumnTransformer(read = "UPPER(VALOR)")
	private String valor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public ItemTarefa getItem() {
		return item;
	}

	public void setItem(ItemTarefa item) {
		this.item = item;
	}
	
}
