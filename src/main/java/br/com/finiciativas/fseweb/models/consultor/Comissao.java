package br.com.finiciativas.fseweb.models.consultor;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.enums.Cobrado;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;

@Entity
public class Comissao {

	@Id
	@GeneratedValue
	private Long id;

	private String campanha;

	@OneToOne(fetch = FetchType.LAZY)
	private Contrato contrato;

	@OneToOne
	@JsonIgnore
	private Producao producao;

	@ManyToOne
	private Consultor consultor;

	private Boolean bonusEconomico;

	@ManyToOne
	@JsonIgnore
	private ValorNegocio valorNegocio;

	private String dataDeRecebimentoCliente;

	private String dataRecebimentoFuncionario;

	@Enumerated(EnumType.STRING)
	private Cobrado cobrado;

	@OneToOne
	private Faturamento faturamento;
	
	@OneToOne
	private EtapasPagamento etapaPagamento;
	
	@OneToOne
	private EtapaTrabalho etapaTrabalho;
	
	@OneToOne
	@JsonIgnore
	private NotaFiscal notaFiscal;

	private Float comissaoPercentual;

	private String comissaoValor;

	private boolean autorizacao;

	private boolean validado;

	private boolean pagamento;

	private boolean comissaoSimulada;
	
	private boolean ativa;
	
	private boolean manual;

	private Date dataCriacao;

	private String data_autorizacao;

	private String data_pagamento;

	private String observacoes;
	
	private String trimestrePagamento;
	
	private String anoPagamento;

	private boolean finalizado;

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public String getTrimestrePagamento() {
		return trimestrePagamento;
	}

	public void setTrimestrePagamento(String trimestrePagamento) {
		this.trimestrePagamento = trimestrePagamento;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}

	public Boolean getBonusEconomico() {
		return bonusEconomico;
	}

	public void setBonusEconomico(Boolean bonusEconomico) {
		this.bonusEconomico = bonusEconomico;
	}

	public ValorNegocio getValorNegocio() {
		return valorNegocio;
	}

	public void setValorNegocio(ValorNegocio valorNegocio) {
		this.valorNegocio = valorNegocio;
	}

	public String getDataDeRecebimentoCliente() {
		return dataDeRecebimentoCliente;
	}

	public void setDataDeRecebimentoCliente(String dataDeRecebimentoCliente) {
		this.dataDeRecebimentoCliente = dataDeRecebimentoCliente;
	}

	public Cobrado getCobrado() {
		return cobrado;
	}

	public void setCobrado(Cobrado cobrado) {
		this.cobrado = cobrado;
	}

	public String getDataRecebimentoFuncionario() {
		return dataRecebimentoFuncionario;
	}

	public void setDataRecebimentoFuncionario(String dataRecebimentoFuncionario) {
		this.dataRecebimentoFuncionario = dataRecebimentoFuncionario;
	}

	public Faturamento getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Faturamento faturamento) {
		this.faturamento = faturamento;
	}

	
	public EtapasPagamento getEtapaPagamento() {
		return etapaPagamento;
	}

	public void setEtapaPagamento(EtapasPagamento etapaPagamento) {
		this.etapaPagamento = etapaPagamento;
	}
	
	public EtapaTrabalho getEtapaTrabalho() {
		return etapaTrabalho;
	}

	public void setEtapaTrabalho(EtapaTrabalho etapaTrabalho) {
		this.etapaTrabalho = etapaTrabalho;
	}

	public Float getComissaoPercentual() {
		return comissaoPercentual;
	}

	public void setComissaoPercentual(Float comissaoPercentual) {
		this.comissaoPercentual = comissaoPercentual;
	}
	
	public String getComissaoValor() {
		return comissaoValor;
	}

	public void setComissaoValor(String comissaoValor) {
		this.comissaoValor = comissaoValor;
	}

	public boolean isAutorizacao() {
		return autorizacao;
	}

	public void setAutorizacao(boolean autorizacao) {
		this.autorizacao = autorizacao;
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}

	public boolean isPagamento() {
		return pagamento;
	}

	public void setPagamento(boolean pagamento) {
		this.pagamento = pagamento;
	}

	public boolean isComissaoSimulada() {
		return comissaoSimulada;
	}

	public void setComissaoSimulada(boolean comissaoSimulada) {
		this.comissaoSimulada = comissaoSimulada;
	}

	public String getData_autorizacao() {
		return data_autorizacao;
	}

	public void setData_autorizacao(String data_autorizacao) {
		this.data_autorizacao = data_autorizacao;
	}

	public String getData_pagamento() {
		return data_pagamento;
	}

	public void setData_pagamento(String data_pagamento) {
		this.data_pagamento = data_pagamento;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	
	public boolean isAtiva() {
		return ativa;
	}

	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	
	public boolean isManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}
	
	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}
	
	public String getAnoPagamento() {
		return anoPagamento;
	}

	public void setAnoPagamento(String anoPagamento) {
		this.anoPagamento = anoPagamento;
	}

}
