package br.com.finiciativas.fseweb.models.valorNegocioCL;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.contrato.Contrato;

@Entity
public class ValorMixto {

	@Id
	@GeneratedValue
	private Long id;
	
	private String campanha;
	
	private String anoFiscal;
	
	private String valor;
	
	private String etapa;
	
	private String Fee;
	
	private Boolean erro;
	
	@OneToOne
	@JoinColumn(name="contrato_id", unique=false)
	private Contrato contrato;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getAnoFiscal() {
		return anoFiscal;
	}

	public void setAnoFiscal(String anoFiscal) {
		this.anoFiscal = anoFiscal;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getFee() {
		return Fee;
	}

	public void setFee(String fee) {
		Fee = fee;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Boolean getErro() {
		return erro;
	}

	public void setErro(Boolean erro) {
		this.erro = erro;
	}
	
}
