package br.com.finiciativas.fseweb.models.cliente;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.finiciativas.fseweb.abstractclass.Endereco;

@Entity
@DynamicUpdate(true)
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class EnderecoCliente extends Endereco {

	@ManyToOne
	private Cliente cliente;
	
	private boolean enderecoNotaFiscalBoleto;

	@ColumnTransformer(read = "UPPER(TIPO)")
	private String tipo;

	public EnderecoCliente() {

	}

	public EnderecoCliente(EnderecoCliente endereco) {
		this.setId(endereco.getId());
		this.setLogradouro(endereco.getLogradouro());
		this.setCidade(endereco.getCidade());
		this.setComplemento(endereco.getComplemento());
		this.setCep(endereco.getCep());
		this.setEnderecoNotaFiscalBoleto(endereco.isEnderecoNotaFiscalBoleto());
		this.setBairro(endereco.getBairro());
		this.tipo = endereco.getTipo();
		
	}

	public boolean isEnderecoNotaFiscalBoleto() {
		return enderecoNotaFiscalBoleto;
	}

	public void setEnderecoNotaFiscalBoleto(boolean enderecoNotaFiscalBoleto) {
		this.enderecoNotaFiscalBoleto = enderecoNotaFiscalBoleto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipoEndereco) {
		this.tipo = tipoEndereco;
	}

}

