package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
public class Recurso {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private Consultor feitoPor;
	
	@OneToOne
	private Consultor validadoPor;
	
	private String enviadoEm;
	
	private boolean validado;
	
	private String dataProtocolizacao;
	
	@OneToOne
	private Acompanhamento acompanhamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public String getEnviadoEm() {
		return enviadoEm;
	}

	public void setEnviadoEm(String enviadoEm) {
		this.enviadoEm = enviadoEm;
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}

	public String getDataProtocolizacao() {
		return dataProtocolizacao;
	}

	public void setDataProtocolizacao(String dataProtocolizacao) {
		this.dataProtocolizacao = dataProtocolizacao;
	}

	public Acompanhamento getAcompanhamento() {
		return acompanhamento;
	}

	public void setAcompanhamento(Acompanhamento acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	
}
