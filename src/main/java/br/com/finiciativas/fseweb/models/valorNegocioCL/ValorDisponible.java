package br.com.finiciativas.fseweb.models.valorNegocioCL;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;

@Entity
public class ValorDisponible {

	@Id
	@GeneratedValue
	private Long id;
	
	private String campanha;
	
	private String fechaReferencia;
	
	private String anoFiscal;
	
	private String valor;
	
	private String corfo;
	
	@OneToOne
	@JoinColumn(name="producao_id", unique=false)
	private Producao producao;
	
	@OneToOne
	@JoinColumn(name="subProducao_id", unique=false, nullable = true)
	private SubProducao subProducao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getFechaReferencia() {
		return fechaReferencia;
	}

	public void setFechaReferencia(String fechaReferencia) {
		this.fechaReferencia = fechaReferencia;
	}

	public String getAnoFiscal() {
		return anoFiscal;
	}

	public void setAnoFiscal(String anoFiscal) {
		this.anoFiscal = anoFiscal;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public SubProducao getSubProducao() {
		return subProducao;
	}

	public void setSubProducao(SubProducao subProducao) {
		this.subProducao = subProducao;
	}

	public String getCorfo() {
		return corfo;
	}

	public void setCorfo(String corfo) {
		this.corfo = corfo;
	}
	
	
}
