package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class FNDCT {

	@Id
	@GeneratedValue
	private Long id;
	
	private String primeroEnviadoEm;
	
	private String primeroRecebidoEm;
	
	private String segundoEnviadoEm;
	
	private String segundoRecebidoEm;
	
	private String terceiroEnviadoEm;
	
	private String terceiroRecebidoEm;
	
	private String quartoEnviadoEm;
	
	private String quartoRecebidoEm;
	
	private String quintoEnviadoEm;
	
	private String quintoRecebidoEm;
	
	@ManyToOne
	@NotFound(action=NotFoundAction.IGNORE)
	private Producao producao;

	public FNDCT(){}
	
	public FNDCT(Long id, String primeroEnviadoEm, String primeroRecebidoEm, String segundoEnviadoEm,
			String segundoRecebidoEm, String terceiroEnviadoEm, String terceiroRecebidoEm, String quartoEnviadoEm,
			String quartoRecebidoEm, String quintoEnviadoEm, String quintoRecebidoEm, Producao producao) {
		super();
		this.id = id;
		this.primeroEnviadoEm = primeroEnviadoEm;
		this.primeroRecebidoEm = primeroRecebidoEm;
		this.segundoEnviadoEm = segundoEnviadoEm;
		this.segundoRecebidoEm = segundoRecebidoEm;
		this.terceiroEnviadoEm = terceiroEnviadoEm;
		this.terceiroRecebidoEm = terceiroRecebidoEm;
		this.quartoEnviadoEm = quartoEnviadoEm;
		this.quartoRecebidoEm = quartoRecebidoEm;
		this.quintoEnviadoEm = quintoEnviadoEm;
		this.quintoRecebidoEm = quintoRecebidoEm;
		this.producao = producao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrimeroEnviadoEm() {
		return primeroEnviadoEm;
	}

	public void setPrimeroEnviadoEm(String primeroEnviadoEm) {
		this.primeroEnviadoEm = primeroEnviadoEm;
	}

	public String getPrimeroRecebidoEm() {
		return primeroRecebidoEm;
	}

	public void setPrimeroRecebidoEm(String primeroRecebidoEm) {
		this.primeroRecebidoEm = primeroRecebidoEm;
	}

	public String getSegundoEnviadoEm() {
		return segundoEnviadoEm;
	}

	public void setSegundoEnviadoEm(String segundoEnviadoEm) {
		this.segundoEnviadoEm = segundoEnviadoEm;
	}

	public String getSegundoRecebidoEm() {
		return segundoRecebidoEm;
	}

	public void setSegundoRecebidoEm(String segundoRecebidoEm) {
		this.segundoRecebidoEm = segundoRecebidoEm;
	}

	public String getTerceiroEnviadoEm() {
		return terceiroEnviadoEm;
	}

	public void setTerceiroEnviadoEm(String terceiroEnviadoEm) {
		this.terceiroEnviadoEm = terceiroEnviadoEm;
	}

	public String getTerceiroRecebidoEm() {
		return terceiroRecebidoEm;
	}

	public void setTerceiroRecebidoEm(String terceiroRecebidoEm) {
		this.terceiroRecebidoEm = terceiroRecebidoEm;
	}

	public String getQuartoEnviadoEm() {
		return quartoEnviadoEm;
	}

	public void setQuartoEnviadoEm(String quartoEnviadoEm) {
		this.quartoEnviadoEm = quartoEnviadoEm;
	}

	public String getQuartoRecebidoEm() {
		return quartoRecebidoEm;
	}

	public void setQuartoRecebidoEm(String quartoRecebidoEm) {
		this.quartoRecebidoEm = quartoRecebidoEm;
	}

	public String getQuintoEnviadoEm() {
		return quintoEnviadoEm;
	}

	public void setQuintoEnviadoEm(String quintoEnviadoEm) {
		this.quintoEnviadoEm = quintoEnviadoEm;
	}

	public String getQuintoRecebidoEm() {
		return quintoRecebidoEm;
	}

	public void setQuintoRecebidoEm(String quintoRecebidoEm) {
		this.quintoRecebidoEm = quintoRecebidoEm;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	}
