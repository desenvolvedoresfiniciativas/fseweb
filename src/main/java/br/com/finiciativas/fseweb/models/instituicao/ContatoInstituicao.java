package br.com.finiciativas.fseweb.models.instituicao;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicUpdate;

import br.com.finiciativas.fseweb.abstractclass.Contato;

@Entity
@DynamicUpdate(true)
public class ContatoInstituicao extends Contato{

	public ContatoInstituicao() {

	}
	
	public ContatoInstituicao(ContatoInstituicao contatoInstituicao){
		if (contatoInstituicao != null) {
			this.setId(contatoInstituicao.getId());
			this.setNome(contatoInstituicao.getNome());
			this.setEmail(contatoInstituicao.getEmail());
			this.setCargo(contatoInstituicao.getCargo());
			this.setTelefone1(contatoInstituicao.getTelefone1());
			this.setTelefone2(contatoInstituicao.getTelefone2());
			this.setAtivo(contatoInstituicao.isAtivo());
		}
	}
	
}
