package br.com.finiciativas.fseweb.models.instituicao;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicUpdate;

import br.com.finiciativas.fseweb.abstractclass.Endereco;

@Entity
@DynamicUpdate(true)
public class EnderecoInstituicao extends Endereco {
	
}
