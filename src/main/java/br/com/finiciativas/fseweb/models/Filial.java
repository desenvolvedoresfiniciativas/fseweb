package br.com.finiciativas.fseweb.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
@DynamicUpdate(true)
@Where(clause = "ativo = 1")
public class Filial {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@Where(clause = "ativo = 1")
	private List<Consultor> consultores = new ArrayList<>();
	
	private boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Consultor> getConsultores() {
		return consultores;
	}

	public void setConsultores(List<Consultor> consultores) {
		this.consultores = consultores;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
