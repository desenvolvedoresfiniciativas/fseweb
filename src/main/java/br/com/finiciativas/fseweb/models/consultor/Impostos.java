package br.com.finiciativas.fseweb.models.consultor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Impostos {

	@Id
	@GeneratedValue
	private Long id;

	private String campanha;
	private Double ISS;
	private Double COFINS;
	private Double PIS;
	private Double CSLL;
	private Double IRPJ;
	private Double totalPorcentagemImposto;

	// Para campanha de 2020 os impostos s�o:
	// ISS = 5%;
	// COFINS = 3%;
	// PIS = 0,65%;
	// CSLL = 1%;
	// IRPJ = 1,5%;
	
	// Total = 11,15%;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public Double getISS() {
		return ISS;
	}

	public void setISS(Double iSS) {
		ISS = iSS;
	}

	public Double getCOFINS() {
		return COFINS;
	}

	public void setCOFINS(Double cOFINS) {
		COFINS = cOFINS;
	}

	public Double getPIS() {
		return PIS;
	}

	public void setPIS(Double pIS) {
		PIS = pIS;
	}

	public Double getCSLL() {
		return CSLL;
	}

	public void setCSLL(Double cSLL) {
		CSLL = cSLL;
	}

	public Double getIRPJ() {
		return IRPJ;
	}

	public void setIRPJ(Double iRPJ) {
		IRPJ = iRPJ;
	}

	public Double getTotalPorcentagemImposto() {
		return totalPorcentagemImposto;
	}

	public void setTotalPorcentagemImposto(Double totalPorcentagemImposto) {
		this.totalPorcentagemImposto = totalPorcentagemImposto;
	}

	// Valores que s�o retirados do pagamento l�quido
	// Total de 11,15% em impostos pagos pela FI

	// valorLiquido = (valorLiquido * iss) + (valorLiquido * cofins) + (valorLiquido
	// * pis) +
	// (valorLiquido * csll) + (valorLiquido * irpj);

	// Foi informado pelo cliente que os impostos possuem valores constantes,
	// Caso mude algum dia, ser� sinalizado

}
