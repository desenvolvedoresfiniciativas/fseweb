package br.com.finiciativas.fseweb.models.honorario;

import java.math.BigDecimal;

import javax.persistence.Entity;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@OnDelete(action = OnDeleteAction.CASCADE)
public class ValorFixo extends Honorario {
	
	private static final long serialVersionUID = 1L;
	
	private BigDecimal valor;
	
	private Long contratoId;
	
	private boolean valorMensal;
	
	private boolean valorAnual;
	
	private boolean valorTrimestral;
	
	private String dataCobrancaAnual; 
	
	private Integer dataCobrancaMensal; 
	
	private boolean reajuste;
	
	@ColumnTransformer(read = "UPPER(OBESRVACAO)")
	private String obesrvacao;
	
	private String dataCobrancaTrimestral1;
	
	private String dataCobrancaTrimestral2;
	
	private String dataCobrancaTrimestral3;
	
	private String dataCobrancaTrimestral4;
	
	public ValorFixo() {
		
	}
	
	public ValorFixo(String nome) {
		super(nome);
	}
	
	public ValorFixo(ValorFixo valorFixo) {
		this.valor = valorFixo.getValor();
		this.valorMensal = valorFixo.isValorMensal();
		this.valorAnual = valorFixo.isValorAnual();
		this.dataCobrancaAnual = valorFixo.getDataCobrancaAnual();
		this.reajuste = valorFixo.isReajuste();
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public boolean isValorMensal() {
		return valorMensal;
	}

	public void setValorMensal(boolean valorMensal) {
		this.valorMensal = valorMensal;
	}

	public boolean isValorAnual() {
		return valorAnual;
	}

	public void setValorAnual(boolean valorAnual) {
		this.valorAnual = valorAnual;
	}

	public String getDataCobrancaAnual() {
		return dataCobrancaAnual;
	}

	public void setDataCobrancaAnual(String dataCobrancaAnual) {
		this.dataCobrancaAnual = dataCobrancaAnual;
	}

	public boolean isReajuste() {
		return reajuste;
	}

	public void setReajuste(boolean reajuste) {
		this.reajuste = reajuste;
	}

	public Long getContratoId() {
		return contratoId;
	}

	public void setContratoId(Long contratoId) {
		this.contratoId = contratoId;
	}

	public int getDataCobrancaMensal() {
		return dataCobrancaMensal;
	}

	public void setDataCobrancaMensal(int dataCobrancaMensal) {
		this.dataCobrancaMensal = dataCobrancaMensal;
	}

	public boolean isValorTrimestral() {
		return valorTrimestral;
	}

	public void setValorTrimestral(boolean valorTrimestral) {
		this.valorTrimestral = valorTrimestral;
	}

	public void setDataCobrancaMensal(Integer dataCobrancaMensal) {
		this.dataCobrancaMensal = dataCobrancaMensal;
	}

	public String getDataCobrancaTrimestral1() {
		return dataCobrancaTrimestral1;
	}

	public void setDataCobrancaTrimestral1(String dataCobrancaTrimestral1) {
		this.dataCobrancaTrimestral1 = dataCobrancaTrimestral1;
	}

	public String getDataCobrancaTrimestral2() {
		return dataCobrancaTrimestral2;
	}

	public void setDataCobrancaTrimestral2(String dataCobrancaTrimestral2) {
		this.dataCobrancaTrimestral2 = dataCobrancaTrimestral2;
	}

	public String getDataCobrancaTrimestral3() {
		return dataCobrancaTrimestral3;
	}

	public void setDataCobrancaTrimestral3(String dataCobrancaTrimestral3) {
		this.dataCobrancaTrimestral3 = dataCobrancaTrimestral3;
	}

	public String getObesrvacao() {
		return obesrvacao;
	}

	public void setObesrvacao(String obesrvacao) {
		this.obesrvacao = obesrvacao;
	}

	public String getDataCobrancaTrimestral4() {
		return dataCobrancaTrimestral4;
	}

	public void setDataCobrancaTrimestral4(String dataCobrancaTrimestral4) {
		this.dataCobrancaTrimestral4 = dataCobrancaTrimestral4;
	}

}
