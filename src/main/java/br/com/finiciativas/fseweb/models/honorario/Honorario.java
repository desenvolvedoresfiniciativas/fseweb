package br.com.finiciativas.fseweb.models.honorario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.finiciativas.fseweb.models.contrato.Contrato;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Honorario implements Serializable {
	 
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(updatable = false)
	private String nome;
	
	@ManyToOne
	@JoinColumn(updatable = false)
	private Contrato contrato;
	
	@OneToMany
	private List<PercentualEscalonado> percentualEscalonado;
	
	public Honorario() {
		
	}
	
	public Honorario(String nome) {
		this.setNome(nome);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
}
