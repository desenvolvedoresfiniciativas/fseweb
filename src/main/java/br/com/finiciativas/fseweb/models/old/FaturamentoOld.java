package br.com.finiciativas.fseweb.models.old;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//tabela tb_faturamento FSE OLD 

@Entity
public class FaturamentoOld {

	@Id
	@GeneratedValue
	private Long id;

	private int amId;
	private int idContrato;
	private int idEmpresa;
	private int anoFaturamento;

	private double totalFaturar;

	private String tpEtapa;

	private double valorEtapa;

	private char versaoFaturada;

	private double diferencaVersao;

	private int statusEtapa;

	private String comentarios;
	private String dataAlteracaoNFCliente;

	private int emailAutomaticoEnviado;
	private int envioAutomaticoAtivo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAmId() {
		return amId;
	}

	public void setAmId(int amId) {
		this.amId = amId;
	}

	public int getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(int idContrato) {
		this.idContrato = idContrato;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getAnoFaturamento() {
		return anoFaturamento;
	}

	public void setAnoFaturamento(int anoFaturamento) {
		this.anoFaturamento = anoFaturamento;
	}

	public double getTotalFaturar() {
		return totalFaturar;
	}

	public void setTotalFaturar(double totalFaturar) {
		this.totalFaturar = totalFaturar;
	}

	public String getTpEtapa() {
		return tpEtapa;
	}

	public void setTpEtapa(String tpEtapa) {
		this.tpEtapa = tpEtapa;
	}

	public double getValorEtapa() {
		return valorEtapa;
	}

	public void setValorEtapa(double valorEtapa) {
		this.valorEtapa = valorEtapa;
	}

	public char getVersaoFaturada() {
		return versaoFaturada;
	}

	public void setVersaoFaturada(char versaoFaturada) {
		this.versaoFaturada = versaoFaturada;
	}

	public double getDiferencaVersao() {
		return diferencaVersao;
	}

	public void setDiferencaVersao(double diferencaVersao) {
		this.diferencaVersao = diferencaVersao;
	}

	public int getStatusEtapa() {
		return statusEtapa;
	}

	public void setStatusEtapa(int statusEtapa) {
		this.statusEtapa = statusEtapa;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getDataAlteracaoNFCliente() {
		return dataAlteracaoNFCliente;
	}

	public void setDataAlteracaoNFCliente(String dataAlteracaoNFCliente) {
		this.dataAlteracaoNFCliente = dataAlteracaoNFCliente;
	}

	public int getEmailAutomaticoEnviado() {
		return emailAutomaticoEnviado;
	}

	public void setEmailAutomaticoEnviado(int emailAutomaticoEnviado) {
		this.emailAutomaticoEnviado = emailAutomaticoEnviado;
	}

	public int getEnvioAutomaticoAtivo() {
		return envioAutomaticoAtivo;
	}

	public void setEnvioAutomaticoAtivo(int envioAutomaticoAtivo) {
		this.envioAutomaticoAtivo = envioAutomaticoAtivo;
	}

}
