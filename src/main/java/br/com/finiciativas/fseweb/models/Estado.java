package br.com.finiciativas.fseweb.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;

@Entity
public class Estado {

	@Id
	@GeneratedValue
	private Long id;

	@ColumnTransformer(read = "UPPER(UF)")
	private String uf;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

}
