package br.com.finiciativas.fseweb.models.produto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;

@Entity
public class ItemValores {

	@Id
	@GeneratedValue
	private Long id;

	@ColumnTransformer(read = "UPPER(VALOR)")
	private String valor;

	private String dataPrevisao;

	private String dataRealizado;

	@ManyToOne
	@JoinColumn(name = "item_id")
	private ItemTarefa item;

	@ManyToOne
	@JoinColumn(name = "tarefa_id")
	private Tarefa tarefa;

	@ManyToOne
	@JoinColumn(name = "producao_id")
	private Producao producao;

	private boolean SubProducao;

	@ManyToOne
	@JoinColumn(name = "sub_producao_index_id")
	@JsonIgnore
	private SubProducao subProducaoIndex;

	private int numeroOrdem;

	public ItemValores() {

	}

	public ItemValores(Long id, String valor, String dataPrevisao, String dataRealizado, ItemTarefa item, Tarefa tarefa,
			Producao producao) {
		super();
		this.id = id;
		this.valor = valor;
		this.dataPrevisao = dataPrevisao;
		this.dataRealizado = dataRealizado;
		this.item = item;
		this.tarefa = tarefa;
		this.producao = producao;
	}

	public String getDataPrevisao() {
		return dataPrevisao;
	}

	public void setDataPrevisao(String dataPrevisao) {
		this.dataPrevisao = dataPrevisao;
	}

	public String getDataRealizado() {
		return dataRealizado;
	}

	public void setDataRealizado(String dataRealizado) {
		this.dataRealizado = dataRealizado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public ItemTarefa getItem() {
		return item;
	}

	public void setItem(ItemTarefa item) {
		this.item = item;
	}

	public Tarefa getTarefa() {
		return tarefa;
	}

	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public SubProducao getSubProducaoIndex() {
		return subProducaoIndex;
	}

	public void setSubProducaoIndex(SubProducao subProducaoIndex) {
		this.subProducaoIndex = subProducaoIndex;
	}

	public int getNumeroOrdem() {
		return numeroOrdem;
	}

	public void setNumeroOrdem(int numeroOrdem) {
		this.numeroOrdem = numeroOrdem;
	}

	public boolean isSubProducao() {
		return SubProducao;
	}

	public void setSubProducao(boolean subProducao) {
		SubProducao = subProducao;
	}
	
}
