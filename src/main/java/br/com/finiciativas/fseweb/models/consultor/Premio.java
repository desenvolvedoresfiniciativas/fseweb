package br.com.finiciativas.fseweb.models.consultor;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.Evolucao;
import br.com.finiciativas.fseweb.models.PeriodoPagamento;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;

@Entity
public class Premio {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	private Contrato contrato;

	@OneToOne(fetch = FetchType.LAZY)
	private Producao producao;

	@ManyToOne
	private Consultor consultor;

	@OneToOne
	private ValorNegocio valorNegocio;

	private String campanha;
	
	private String dataDeRecebimentoCliente;

	private String dataRecebimentoFuncionario;

	@OneToOne
	private Faturamento faturamento;
	
	private BigDecimal evolucaoTotal;
	
	private LinkedList<Evolucao> evolucao;

	@OneToMany(fetch = FetchType.LAZY)
	private List<NotaFiscal> notasFiscais;
	
	private float valorNegocioPretendido;
	private float valorNegocioRealizado;

	private float eficienciaPropostasPretendido;
	private float eficienciaPropostasRealizado;
	private float premioKPIEficiencia;

	private float eficienciaContratosPretendido;
	private float eficienciaContratosRealizado;
	private float premioKPIServicos;
	
	private float ticketMedioPretendido;
	private float ticketMedioRealizado;
	private float premioKPITicketMedio;

	private float faturamentoPretendido;
	private float faturamentoRealizado;
	private float premioKPIFluxoDeCaixa;

	private float miniCampanhaPretendida;
	private float miniCampanhaRealizada;

	private float premioAcumuladoEquipe;
	private float referenciaPremioAcumuladoEquipe;
	
	private float porcentagemPremio;
	
	private BigDecimal valorPremio;
	
	private String observacoes;
	
	private Boolean premioTotal;
	
	private Boolean ativo;
	
	private boolean pago;

	public boolean isPago() {
		return pago;
	}

	public void setPago(boolean pago) {
		this.pago = pago;
	}

	public Boolean getPremioTotal() {
		return premioTotal;
	}

	public void setPremioTotal(Boolean premioTotal) {
		this.premioTotal = premioTotal;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}

	public ValorNegocio getValorNegocio() {
		return valorNegocio;
	}

	public void setValorNegocio(ValorNegocio valorNegocio) {
		this.valorNegocio = valorNegocio;
	}
	
	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getDataDeRecebimentoCliente() {
		return dataDeRecebimentoCliente;
	}

	public void setDataDeRecebimentoCliente(String dataDeRecebimentoCliente) {
		this.dataDeRecebimentoCliente = dataDeRecebimentoCliente;
	}

	public String getDataRecebimentoFuncionario() {
		return dataRecebimentoFuncionario;
	}

	public void setDataRecebimentoFuncionario(String dataRecebimentoFuncionario) {
		this.dataRecebimentoFuncionario = dataRecebimentoFuncionario;
	}

	public Faturamento getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Faturamento faturamento) {
		this.faturamento = faturamento;
	}

	public List<NotaFiscal> getNotasFiscais() {
		return notasFiscais;
	}

	public void setNotasFiscais(List<NotaFiscal> notasFiscais) {
		this.notasFiscais = notasFiscais;
	}
	
	public float getValorNegocioPretendido() {
		return valorNegocioPretendido;
	}

	public void setValorNegocioPretendido(float valorNegocioPretendido) {
		this.valorNegocioPretendido = valorNegocioPretendido;
	}

	public float getValorNegocioRealizado() {
		return valorNegocioRealizado;
	}

	public void setValorNegocioRealizado(float valorNegocioRealizado) {
		this.valorNegocioRealizado = valorNegocioRealizado;
	}

	public float getEficienciaPropostasPretendido() {
		return eficienciaPropostasPretendido;
	}

	public void setEficienciaPropostasPretendido(float eficienciaPropostasPretendido) {
		this.eficienciaPropostasPretendido = eficienciaPropostasPretendido;
	}

	public float getEficienciaPropostasRealizado() {
		return eficienciaPropostasRealizado;
	}

	public void setEficienciaPropostasRealizado(float eficienciaPropostasRealizado) {
		this.eficienciaPropostasRealizado = eficienciaPropostasRealizado;
	}

	public float getEficienciaContratosPretendido() {
		return eficienciaContratosPretendido;
	}

	public void setEficienciaContratosPretendido(float eficienciaContratosPretendido) {
		this.eficienciaContratosPretendido = eficienciaContratosPretendido;
	}

	public float getEficienciaContratosRealizado() {
		return eficienciaContratosRealizado;
	}

	public void setEficienciaContratosRealizado(float eficienciaContratosRealizado) {
		this.eficienciaContratosRealizado = eficienciaContratosRealizado;
	}

	public float getTicketMedioPretendido() {
		return ticketMedioPretendido;
	}

	public void setTicketMedioPretendido(float ticketMedioPretendido) {
		this.ticketMedioPretendido = ticketMedioPretendido;
	}

	public float getTicketMedioRealizado() {
		return ticketMedioRealizado;
	}

	public void setTicketMedioRealizado(float ticketMedioRealizado) {
		this.ticketMedioRealizado = ticketMedioRealizado;
	}

	public float getFaturamentoPretendido() {
		return faturamentoPretendido;
	}

	public void setFaturamentoPretendido(float faturamentoPretendido) {
		this.faturamentoPretendido = faturamentoPretendido;
	}

	public float getFaturamentoRealizado() {
		return faturamentoRealizado;
	}

	public void setFaturamentoRealizado(float faturamentoRealizado) {
		this.faturamentoRealizado = faturamentoRealizado;
	}

	public float getMiniCampanhaPretendida() {
		return miniCampanhaPretendida;
	}

	public void setMiniCampanhaPretendida(float miniCampanhaPretendida) {
		this.miniCampanhaPretendida = miniCampanhaPretendida;
	}

	public float getMiniCampanhaRealizada() {
		return miniCampanhaRealizada;
	}

	public void setMiniCampanhaRealizada(float miniCampanhaRealizada) {
		this.miniCampanhaRealizada = miniCampanhaRealizada;
	}

	public float getPremioAcumuladoEquipe() {
		return premioAcumuladoEquipe;
	}

	public void setPremioAcumuladoEquipe(float premioAcumuladoEquipe) {
		this.premioAcumuladoEquipe = premioAcumuladoEquipe;
	}

	public float getReferenciaPremioAcumuladoEquipe() {
		return referenciaPremioAcumuladoEquipe;
	}

	public void setReferenciaPremioAcumuladoEquipe(float referenciaPremioAcumuladoEquipe) {
		this.referenciaPremioAcumuladoEquipe = referenciaPremioAcumuladoEquipe;
	}
	
	public float getPorcentagemPremio() {
		return porcentagemPremio;
	}

	public void setPorcentagemPremio(float porcentagemPremio) {
		this.porcentagemPremio = porcentagemPremio;
	}

	public BigDecimal getValorPremio() {
		return valorPremio;
	}

	public void setValorPremio(BigDecimal valorPremio) {
		this.valorPremio = valorPremio;
	}
	
	public float getPremioKPIEficiencia() {
		return premioKPIEficiencia;
	}

	public void setPremioKPIEficiencia(float premioKPIEficiencia) {
		this.premioKPIEficiencia = premioKPIEficiencia;
	}

	public float getPremioKPIServicos() {
		return premioKPIServicos;
	}

	public void setPremioKPIServicos(float premioKPIServicos) {
		this.premioKPIServicos = premioKPIServicos;
	}

	public float getPremioKPITicketMedio() {
		return premioKPITicketMedio;
	}

	public void setPremioKPITicketMedio(float premioKPITicketMedio) {
		this.premioKPITicketMedio = premioKPITicketMedio;
	}

	public float getPremioKPIFluxoDeCaixa() {
		return premioKPIFluxoDeCaixa;
	}

	public void setPremioKPIFluxoDeCaixa(float premioKPIFluxoDeCaixa) {
		this.premioKPIFluxoDeCaixa = premioKPIFluxoDeCaixa;
	}

	public BigDecimal getEvolucaoTotal() {
		return evolucaoTotal;
	}

	public void setEvolucaoTotal(BigDecimal evolucaoTotal) {
		this.evolucaoTotal = evolucaoTotal;
	}

	public LinkedList<Evolucao> getEvolucao() {
		return evolucao;
	}

	public void setEvolucao(LinkedList<Evolucao> evolucao) {
		this.evolucao = evolucao;
	}
	
	
	

}
