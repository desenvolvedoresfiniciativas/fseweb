package br.com.finiciativas.fseweb.models;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Entity;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Premio;

@Entity
public class Evolucao {
	
	@Id
	@GeneratedValue
	private long id;
	
	private float percentualAtingido;
	
	private float percentualAnterior;
	
	private String campanha;
		
	//private Consultor consultor;
	
	@ManyToOne
	private Premio premio;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getPercentualAtingido() {
		return percentualAtingido;
	}

	public void setPercentualAtingido(float percentualAtingido) {
		this.percentualAtingido = percentualAtingido;
	}

	public float getPercentualAnterior() {
		return percentualAnterior;
	}

	public void setPercentualAnterior(float percentualAnterior) {
		this.percentualAnterior = percentualAnterior;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public Premio getPremio() {
		return premio;
	}

	public void setPremio(Premio premio) {
		this.premio = premio;
	}

	

	/*
	 * public Consultor getConsultor() { return consultor; }
	 * 
	 * public void setConsultor(Consultor consultor) { this.consultor = consultor; }
	 */
	
	
}
