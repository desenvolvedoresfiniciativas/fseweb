package br.com.finiciativas.fseweb.models.honorario;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@OnDelete(action = OnDeleteAction.CASCADE)
public class PercentualEscalonado extends Honorario {
	
	private static final long serialVersionUID = 1L;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<FaixasEscalonado> faixasEscalonamento;
	
	private BigDecimal acimaDe;
	
	private Float percentAcimaDe;
	
	private BigDecimal limitacao = BigDecimal.valueOf(0);
	
	private Long contratoId;
	
	public PercentualEscalonado() {
		
	}
	
	public PercentualEscalonado(String nome) {
		super(nome);
	}

	public List<FaixasEscalonado> getFaixasEscalonamento() {
		return faixasEscalonamento;
	}

	public void setFaixasEscalonamento(List<FaixasEscalonado> faixasEscalonamento) {
		this.faixasEscalonamento = faixasEscalonamento;
	}

	public BigDecimal getLimitacao() {
		return limitacao;
	}

	public void setLimitacao(BigDecimal limitacao) {
		this.limitacao = limitacao;
	}

	public Long getContratoId() {
		return contratoId;
	}

	public void setContratoId(Long contratoId) {
		this.contratoId = contratoId;
	}

	public BigDecimal getAcimaDe() {
		return acimaDe;
	}

	public void setAcimaDe(BigDecimal acimaDe) {
		this.acimaDe = acimaDe;
	}

	public Float getPercentAcimaDe() {
		return percentAcimaDe;
	}

	public void setPercentAcimaDe(Float percentAcimaDe) {
		this.percentAcimaDe = percentAcimaDe;
	}
	
}
