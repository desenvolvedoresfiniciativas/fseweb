package br.com.finiciativas.fseweb.models.produto.ordenacao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;

@Entity
public class OrdemItensTarefa {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Produto produto;
	
	@ManyToOne
	private Tarefa tarefa;
	
	@OneToOne
	private ItemTarefa item;
	
	private int numero;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ItemTarefa getItem() {
		return item;
	}

	public void setItem(ItemTarefa item) {
		this.item = item;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Tarefa getTarefa() {
		return tarefa;
	}

	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	
}
