package br.com.finiciativas.fseweb.models.vigencia;

import javax.persistence.Entity;

@Entity
public class VigenciaDeterminada extends Vigencia {
	
	private static final long serialVersionUID = 1L;
	
	private String fimVigencia;
	
	public VigenciaDeterminada() {
		super.setNome("Vig�ncia Determinada");
	}
	
	public VigenciaDeterminada(String inicioVigencia, String fimVigencia) {
		super.setInicioVigencia(inicioVigencia);
		this.fimVigencia = fimVigencia;
	}

	public String getFimVigencia() {
		return fimVigencia;
	}

	public void setFimVigencia(String fimVigencia) {
		this.fimVigencia = fimVigencia;
	}
	
}
