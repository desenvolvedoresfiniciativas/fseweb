package br.com.finiciativas.fseweb.models.contrato;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.finiciativas.fseweb.enums.FormaPagamento;
import br.com.finiciativas.fseweb.enums.FormaPagamentoCL;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.vigencia.Vigencia;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Contrato {

	@Id
	@GeneratedValue
	private Long id;

	private boolean statusProducao;

	@ManyToOne(fetch = FetchType.LAZY)
	private Cliente cliente;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Vigencia vigencia;

	@NotFound(action = NotFoundAction.IGNORE)
	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "contrato")
	private List<Honorario> honorarios;

	@OneToOne
	private Produto produto;

	@Enumerated(EnumType.STRING)
	private FormaPagamento formaPagamento;
	
	@Enumerated(EnumType.STRING)
	private FormaPagamentoCL formaPagamentoCL;

	private int prazoPagamento;

	private boolean reembolsoDespesas;

	@ManyToMany
	private List<GarantiaContrato> garantias;

	@ManyToOne
	private Consultor comercialResponsavelEntrada;

	@ManyToOne
	private Consultor comercialResponsavelEfetivo;

	@Column(name = "pagamento_entrega")
	private boolean pagamentoEntrega = false;
	@Column(name = "pagamento_data")
	private boolean pagamentoData = false;

	@JsonBackReference
	@OneToMany
	private List<Producao> producao = new ArrayList<>();

//	@JsonIgnore
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "contrato", cascade = CascadeType.PERSIST)
	private EtapasPagamento etapasPagamento;

	@JsonIgnore
	@ColumnTransformer(read = "UPPER(DESCRICAONF)")
	private String descricaoNF;

	@ColumnTransformer(read = "UPPER(DESCRICAO_CONTRATUAL)")
	private String descricaoContratual;

	private String estimativaComercial;
	
	private String cnpjFaturar;
	
	private String rutFaturar;
	
	private String nitFaturar;

	@ColumnTransformer(read = "UPPER(RAZAO_SOCIAL_FATURAR)")
	private String razaoSocialFaturar;

	private boolean pedidoCompra;

	private boolean combo;
	
	private boolean produtoAlvo;
	
	private String periodoCobranca;
	
//	private List<ProdutoAlvo> nomeProdutoAlvo;

	

	@ManyToMany
	@JsonIgnore
	private List<Contrato> produtosAlvo; 
	
	private boolean reajuste;

	@JsonIgnore
	private String dataReajuste;

	@JsonIgnore
	private String obsReajuste;

	@ColumnTransformer(read = "UPPER(OBS_CONTRATO)")
	private String obsContrato;

	private boolean contratoFisico;

	private boolean temporario;
	
	private String dataEntrada;
	
	private String tipo;
	
	private boolean prejuizoFixo;
	
	private boolean prejuizoPercentual;
	
	private String prejuizoValorFixo;
	
	private float prejuizoPercentualFixo;
	
	private String nomeCombo;
	
	private boolean comissaoGerada;
	
	private boolean manual;
	
	private String variable;
	
	private String nContrato;
	
	private boolean contratoInativo;
	
	public boolean isContratoInativo() {
		return contratoInativo;
	}

	public void setContratoInativo(boolean contratoInativo) {
		this.contratoInativo = contratoInativo;
	}

	public boolean isManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}

	public boolean isComissaoGerada() {
		return comissaoGerada;
	}

	public void setComissaoGerada(boolean comissaoGerada) {
		this.comissaoGerada = comissaoGerada;
	}

	public String getPrejuizoValorFixo() {
		return prejuizoValorFixo;
	}

	public void setPrejuizoValorFixo(String prejuizoValorFixo) {
		this.prejuizoValorFixo = prejuizoValorFixo;
	}

	public float getPrejuizoPercentualFixo() {
		return prejuizoPercentualFixo;
	}

	public void setPrejuizoPercentualFixo(float prejuizoPercentualFixo) {
		this.prejuizoPercentualFixo = prejuizoPercentualFixo;
	}

	public Contrato() {

	}

	public Contrato(Contrato contrato) {
		if (contrato != null) {
			this.id = contrato.getId();
			this.prazoPagamento = contrato.getPrazoPagamento();
			this.reembolsoDespesas = contrato.isReembolsoDespesas();
			this.statusProducao = contrato.isStatusProducao();

			if (contrato.getCliente() != null)
				this.cliente = contrato.getCliente();

			if (contrato.getVigencia() != null)
				this.vigencia = contrato.getVigencia();

			if (contrato.getHonorarios() != null)
				this.honorarios = contrato.getHonorarios();

			if (contrato.getProduto() != null)
				this.produto = contrato.getProduto();

			if (contrato.getFormaPagamento() != null)
				this.formaPagamento = contrato.getFormaPagamento();

			if (contrato.getGarantias() != null)
				this.garantias = contrato.getGarantias();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public int getPrazoPagamento() {
		return prazoPagamento;
	}

	public void setPrazoPagamento(int prazoPagamento) {
		this.prazoPagamento = prazoPagamento;
	}

	public boolean isReembolsoDespesas() {
		return reembolsoDespesas;
	}

	public void setReembolsoDespesas(boolean reembolsoDespesas) {
		this.reembolsoDespesas = reembolsoDespesas;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Vigencia getVigencia() {
		return vigencia;
	}

	public void setVigencia(Vigencia vigencia) {
		this.vigencia = vigencia;
	}

	public List<Honorario> getHonorarios() {
		return honorarios;
	}

	public void setHonorarios(List<Honorario> honorarios) {
		this.honorarios = honorarios;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<GarantiaContrato> getGarantias() {
		return garantias;
	}

	public void setGarantias(List<GarantiaContrato> garantias) {
		this.garantias = garantias;
	}

	public boolean isPagamentoEntrega() {
		return pagamentoEntrega;
	}

	public void setPagamentoEntrega(boolean pagamentoEntrega) {
		this.pagamentoEntrega = pagamentoEntrega;
	}

	public boolean isStatusProducao() {
		return statusProducao;
	}

	public void setStatusProducao(boolean statusProducao) {
		this.statusProducao = statusProducao;
	}

//	public List<Producao> getProducao() {
//		return producao;
//	}
//
//	public void setProducao(List<Producao> producao) {
//		this.producao = producao;
//	}

	public EtapasPagamento getEtapasPagamento() {
		return etapasPagamento;
	}

	public void setEtapasPagamento(EtapasPagamento etapasPagamento) {
		this.etapasPagamento = etapasPagamento;
	}

	public String getDescricaoNF() {
		return descricaoNF;
	}

	public void setDescricaoNF(String descricaoNF) {
		this.descricaoNF = descricaoNF;
	}

	public String getDescricaoContratual() {
		return descricaoContratual;
	}

	public void setDescricaoContratual(String descricaoContratual) {
		this.descricaoContratual = descricaoContratual;
	}

	public String getEstimativaComercial() {
		return estimativaComercial;
	}

	public void setEstimativaComercial(String estimativaComercial) {
		this.estimativaComercial = estimativaComercial;
	}

	public String getCnpjFaturar() {
		return cnpjFaturar;
	}

	public void setCnpjFaturar(String cnpjFaturar) {
		this.cnpjFaturar = cnpjFaturar;
	}

	public String getRazaoSocialFaturar() {
		return razaoSocialFaturar;
	}

	public void setRazaoSocialFaturar(String razaoSocialFaturar) {
		this.razaoSocialFaturar = razaoSocialFaturar;
	}

	public boolean isPedidoCompra() {
		return pedidoCompra;
	}

	public void setPedidoCompra(boolean pedidoCompra) {
		this.pedidoCompra = pedidoCompra;
	}

	public boolean isReajuste() {
		return reajuste;
	}

	public void setReajuste(boolean reajuste) {
		this.reajuste = reajuste;
	}

	public String getDataReajuste() {
		return dataReajuste;
	}

	public void setDataReajuste(String dataReajuste) {
		this.dataReajuste = dataReajuste;
	}

	public String getObsReajuste() {
		return obsReajuste;
	}

	public void setObsReajuste(String obsReajuste) {
		this.obsReajuste = obsReajuste;
	}

	public boolean isPagamentoData() {
		return pagamentoData;
	}

	public void setPagamentoData(boolean pagamentoData) {
		this.pagamentoData = pagamentoData;
	}

	public String getObsContrato() {
		return obsContrato;
	}

	public void setObsContrato(String obsContrato) {
		this.obsContrato = obsContrato;
	}

	public boolean isContratoFisico() {
		return contratoFisico;
	}

	public void setContratoFisico(boolean contratoFisico) {
		this.contratoFisico = contratoFisico;
	}

	public Consultor getComercialResponsavelEntrada() {
		return comercialResponsavelEntrada;
	}

	public void setComercialResponsavelEntrada(Consultor comercialResponsavelEntrada) {
		this.comercialResponsavelEntrada = comercialResponsavelEntrada;
	}

	public Consultor getComercialResponsavelEfetivo() {
		return comercialResponsavelEfetivo;
	}

	public void setComercialResponsavelEfetivo(Consultor comercialResponsavelEfetivo) {
		this.comercialResponsavelEfetivo = comercialResponsavelEfetivo;
	}

	public boolean isTemporario() {
		return temporario;
	}

	public void setTemporario(boolean temporario) {
		this.temporario = temporario;
	}

	public String getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(String dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public List<Producao> getProducao() {
		return producao;
	}

	public void setProducao(List<Producao> producao) {
		this.producao = producao;
	}

	public String getRutFaturar() {
		return rutFaturar;
	}

	public void setRutFaturar(String rutFaturar) {
		this.rutFaturar = rutFaturar;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public FormaPagamentoCL getFormaPagamentoCL() {
		return formaPagamentoCL;
	}

	public void setFormaPagamentoCL(FormaPagamentoCL formaPagamentoCL) {
		this.formaPagamentoCL = formaPagamentoCL;
	}
	
	public boolean isPrejuizoFixo() {
		return prejuizoFixo;
	}

	public void setPrejuizoFixo(boolean prejuizoFixo) {
		this.prejuizoFixo = prejuizoFixo;
	}

	public boolean isPrejuizoPercentual() {
		return prejuizoPercentual;
	}

	public void setPrejuizoPercentual(boolean prejuizoPercentual) {
		this.prejuizoPercentual = prejuizoPercentual;
	}
	
	public boolean isCombo() {
		return combo;
	}

	public void setCombo(boolean combo) {
		this.combo = combo;
	}
	
	public boolean isProdutoAlvo() {
		return produtoAlvo;
	}

	public void setProdutoAlvo(boolean produtoPrincipal) {
		this.produtoAlvo = produtoPrincipal;
	}
	
	
	public List<Contrato> getProdutosAlvo() {
		return produtosAlvo;
	}

	public void setProdutosAlvo(List<Contrato> produtosAlvo) {
		this.produtosAlvo = produtosAlvo;
	}
	

	public String getNomeCombo() {
		return nomeCombo;
	}

	public void setNomeCombo(String nomeCombo) {
		this.nomeCombo = nomeCombo;
	}
	
	public String getNitFaturar() {
		return nitFaturar;
	}

	public void setNitFaturar(String nitFaturar) {
		this.nitFaturar = nitFaturar;
	}
	
	public String getPeriodoCobranca() {
		return periodoCobranca;
	}

	public void setPeriodoCobranca(String periodoCobranca) {
		this.periodoCobranca = periodoCobranca;
	}

	@Override
	public String toString() {
		return "Contrato [id=" + id + ", statusProducao=" + statusProducao + ", cliente=" + cliente + ", vigencia="
				+ vigencia + ", honorarios=" + honorarios + ", produto=" + produto + ", formaPagamento="
				+ formaPagamento + ", formaPagamentoCL=" + formaPagamentoCL + ", prazoPagamento=" + prazoPagamento
				+ ", reembolsoDespesas=" + reembolsoDespesas + ", garantias=" + garantias
				+ ", comercialResponsavelEntrada=" + comercialResponsavelEntrada + ", comercialResponsavelEfetivo="
				+ comercialResponsavelEfetivo + ", pagamentoEntrega=" + pagamentoEntrega + ", pagamentoData="
				+ pagamentoData + ", producao=" + producao + ", etapasPagamento=" + etapasPagamento + ", descricaoNF="
				+ descricaoNF + ", descricaoContratual=" + descricaoContratual + ", estimativaComercial="
				+ estimativaComercial + ", cnpjFaturar=" + cnpjFaturar + ", rutFaturar=" + rutFaturar
				+ ", razaoSocialFaturar=" + razaoSocialFaturar + ", pedidoCompra=" + pedidoCompra + ", reajuste="
				+ reajuste + ", dataReajuste=" + dataReajuste + ", obsReajuste=" + obsReajuste + ", obsContrato="
				+ obsContrato + ", contratoFisico=" + contratoFisico + ", temporario=" + temporario + ", dataEntrada="
				+ dataEntrada + ", tipo=" + tipo + ", prejuizoFixo=" + prejuizoFixo + ", prejuizoPercentual="
				+ prejuizoPercentual + ", prejuizoValorFixo=" + prejuizoValorFixo + ", prejuizoPercentualFixo="
				+ prejuizoPercentualFixo + "]";
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getnContrato() {
		return nContrato;
	}

	public void setnContrato(String nContrato) {
		this.nContrato = nContrato;
	}

	
	

}
