package br.com.finiciativas.fseweb.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;

@Entity
public class FITechReport {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Consultor consultor;

	private String mensagem;
	
	private String tipoMensagem;
	
	private boolean atendido = false;
	
	private String DataReport;
	
	private String DataConclusao;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getTipoMensagem() {
		return tipoMensagem;
	}

	public void setTipoMensagem(String tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	public boolean isAtendido() {
		return atendido;
	}

	public void setAtendido(boolean atendido) {
		this.atendido = atendido;
	}

	public String getDataReport() {
		return DataReport;
	}

	public void setDataReport(String dataReport) {
		DataReport = dataReport;
	}

	public String getDataConclus�o() {
		return DataConclusao;
	}

	public void setDataConclus�o(String dataConclusao) {
		DataConclusao = dataConclusao;
	}

}
