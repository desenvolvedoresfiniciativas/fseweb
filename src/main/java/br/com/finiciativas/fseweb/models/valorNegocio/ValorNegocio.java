package br.com.finiciativas.fseweb.models.valorNegocio;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class ValorNegocio {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@JoinColumn(name="producao_id", unique=true)
	private Producao producao;
	
	@ManyToOne
	private Cliente cliente;
	
	private String campanha;
	
	private String nome_empresa;
	
	private String equipe;
	
	private String sigla;
	
	private String divisao;
	
	private String setor;
	
	private String situacao;
	
	private String tipoNegocio;
	
	private float estComercial;
	
	private float prevDespesa;
	
	private float VCAtual;
	
	private String VCFim;
	
	private String VCValCli;
	
	private String prevBeneficio;
	
	private float ctrVlFixo;
	
	private String ctrTipoFee;
	
	private float prevValorFee;
	
	private String ctrObs;
	
	private float antDispendio;
	
	private String comercial;
	
	private String categoria;
	
	private float fatReal;
	
	private BigDecimal valorBase;
	
	private String modoValor;
	
	private float antBeneficio;
	
	private String preEntregaCalc;
	
	private float ctrPcDe;
	
	private float ctrPcSm;
	
	private float ctrPcRm;
	
	private float ant_fat;
	
	private float VCDisp;
	
	private String dtEntCli;
	
	private boolean rrPrimAno;
	
	private String exclPrev;
	
	private String exclReal;
	
	private Float fatPrev;
	
	private boolean fidelizado;
	
	private String prevRelatorios;
	
	private boolean formPeD;
	
	private boolean submissao;
	
	private String cnpj;
	
	private String cidade;  
	
	private String estado;
	
	private float dispendioAtual;
	
	private float exclusaoAtual;
	
	private float aliquotaAtual;
	
	private float valorCalculo;
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public float getValorCalculo() {
		return valorCalculo;
	}

	public void setValorCalculo(float valorCalculo) {
		this.valorCalculo = valorCalculo;
	}

	public float getDispendioAtual() {
		return dispendioAtual;
	}

	public void setDispendioAtual(float dispendioAtual) {
		this.dispendioAtual = dispendioAtual;
	}

	public float getExclusaoAtual() {
		return exclusaoAtual;
	}

	public void setExclusaoAtual(float exclusaoAtual) {
		this.exclusaoAtual = exclusaoAtual;
	}

	public float getAliquotaAtual() {
		return aliquotaAtual;
	}

	public void setAliquotaAtual(float aliquotaAtual) {
		this.aliquotaAtual = aliquotaAtual;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getNome_empresa() {
		return nome_empresa;
	}

	public void setNome_empresa(String nome_empresa) {
		this.nome_empresa = nome_empresa;
	}

	public String getEquipe() {
		return equipe;
	}

	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDivisao() {
		return divisao;
	}

	public void setDivisao(String divisao) {
		this.divisao = divisao;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getTipoNegocio() {
		return tipoNegocio;
	}

	public void setTipoNegocio(String tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}

	public float getEstComercial() {
		return estComercial;
	}

	public void setEstComercial(float estComercial) {
		this.estComercial = estComercial;
	}

	public float getPrevDespesa() {
		return prevDespesa;
	}

	public void setPrevDespesa(float prevDespesa) {
		this.prevDespesa = prevDespesa;
	}

	public float getVCAtual() {
		return VCAtual;
	}

	public void setVCAtual(float vCAtual) {
		VCAtual = vCAtual;
	}

	public String getVCFim() {
		return VCFim;
	}

	public void setVCFim(String vCFim) {
		VCFim = vCFim;
	}

	public String getVCValCli() {
		return VCValCli;
	}

	public void setVCValCli(String vCValCli) {
		VCValCli = vCValCli;
	}

	public String getPrevBeneficio() {
		return prevBeneficio;
	}

	public void setPrevBeneficio(String prevBeneficio) {
		this.prevBeneficio = prevBeneficio;
	}

	public float getCtrVlFixo() {
		return ctrVlFixo;
	}

	public void setCtrVlFixo(float ctrVlFixo) {
		this.ctrVlFixo = ctrVlFixo;
	}

	public String getCtrTipoFee() {
		return ctrTipoFee;
	}

	public void setCtrTipoFee(String ctrTipoFee) {
		this.ctrTipoFee = ctrTipoFee;
	}

	public float getPrevValorFee() {
		return prevValorFee;
	}

	public void setPrevValorFee(float prevValorFee) {
		this.prevValorFee = prevValorFee;
	}

	public String getCtrObs() {
		return ctrObs;
	}

	public void setCtrObs(String ctrObs) {
		this.ctrObs = ctrObs;
	}

	public float getAntDispendio() {
		return antDispendio;
	}

	public void setAntDispendio(float antDispendio) {
		this.antDispendio = antDispendio;
	}

	public String getComercial() {
		return comercial;
	}

	public void setComercial(String comercial) {
		this.comercial = comercial;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public float getFatReal() {
		return fatReal;
	}

	public void setFatReal(float fatReal) {
		this.fatReal = fatReal;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase(BigDecimal valorBase) {
		this.valorBase = valorBase;
	}

	public String getModoValor() {
		return modoValor;
	}

	public void setModoValor(String modoValor) {
		this.modoValor = modoValor;
	}

	public float getAntBeneficio() {
		return antBeneficio;
	}

	public void setAntBeneficio(float antBeneficio) {
		this.antBeneficio = antBeneficio;
	}

	public String getPreEntregaCalc() {
		return preEntregaCalc;
	}

	public void setPreEntregaCalc(String preEntregaCalc) {
		this.preEntregaCalc = preEntregaCalc;
	}

	public float getCtrPcDe() {
		return ctrPcDe;
	}

	public void setCtrPcDe(float ctrPcDe) {
		this.ctrPcDe = ctrPcDe;
	}

	public float getCtrPcSm() {
		return ctrPcSm;
	}

	public void setCtrPcSm(float ctrPcSm) {
		this.ctrPcSm = ctrPcSm;
	}

	public float getCtrPcRm() {
		return ctrPcRm;
	}

	public void setCtrPcRm(float ctrPcRm) {
		this.ctrPcRm = ctrPcRm;
	}

	public float getAnt_fat() {
		return ant_fat;
	}

	public void setAnt_fat(float ant_fat) {
		this.ant_fat = ant_fat;
	}

	public float getVCDisp() {
		return VCDisp;
	}

	public void setVCDisp(float vCDisp) {
		VCDisp = vCDisp;
	}

	public String getDtEntCli() {
		return dtEntCli;
	}

	public void setDtEntCli(String dtEntCli) {
		this.dtEntCli = dtEntCli;
	}

	public boolean isRrPrimAno() {
		return rrPrimAno;
	}

	public void setRrPrimAno(boolean rrPrimAno) {
		this.rrPrimAno = rrPrimAno;
	}

	public String getExclPrev() {
		return exclPrev;
	}

	public void setExclPrev(String exclPrev) {
		this.exclPrev = exclPrev;
	}

	public String getExclReal() {
		return exclReal;
	}

	public void setExclReal(String exclReal) {
		this.exclReal = exclReal;
	}

	public Float getFatPrev() {
		return fatPrev;
	}

	public void setFatPrev(Float fatPrev) {
		this.fatPrev = fatPrev;
	}

	public boolean isFidelizado() {
		return fidelizado;
	}

	public void setFidelizado(boolean fidelizado) {
		this.fidelizado = fidelizado;
	}

	public String getPrevRelatorios() {
		return prevRelatorios;
	}

	public void setPrevRelatorios(String prevRelatorios) {
		this.prevRelatorios = prevRelatorios;
	}

	public boolean isFormPeD() {
		return formPeD;
	}

	public void setFormPeD(boolean formPeD) {
		this.formPeD = formPeD;
	}

	public boolean isSubmissao() {
		return submissao;
	}

	public void setSubmissao(boolean submissao) {
		this.submissao = submissao;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

}
