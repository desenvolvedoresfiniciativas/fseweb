package br.com.finiciativas.fseweb.models.valorNegocioCO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
@Table(name = "valor_aprobado_co")
public class ValorAprobadoCo {
	
	@Id
	@GeneratedValue 
	private Long id;

	private String campanha;
	private String anoFiscal;
	private String valor;
	
	@OneToOne
	@JoinColumn(name="producao_id", unique=false)
	private Producao producao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public String getAnoFiscal() {
		return anoFiscal;
	}

	public void setAnoFiscal(String anoFiscal) {
		this.anoFiscal = anoFiscal;
	}
}