package br.com.finiciativas.fseweb.models.faturamento;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnTransformer;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.finiciativas.fseweb.enums.SubEstadoFaturamento;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Produto;

@Entity
public class Faturamento {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private Cliente cliente;

	@ManyToOne
	private Producao producao;

	@ManyToOne
	private SubProducao subProducao;

	@ManyToOne
	private Contrato contrato;

	@OneToOne
	private ValorFixo valorF;

	private BigDecimal valorEtapa;

	private BigDecimal valorTotal;

	@OneToOne
	private EtapaTrabalho etapa;

	private float percetEtapa;

	@OneToOne
	private PercentualEscalonado percentualE;

	@OneToOne
	private PercentualFixo percentualF;

	@ColumnTransformer(read = "UPPER(OBSERVACAO)")
	private String observacao;

	private boolean liberado;

	private String estado;

	private boolean manual = false;

	@OneToOne
	private EtapasConclusao etapas;

	@ColumnTransformer(read = "UPPER(OBS_CONTRATO)")
	private String obsContrato;

	private String cnpjFaturar;

	private String razaoSocialFaturar;

	@ManyToOne
	private Produto produto;

	private String produtoManual;

	private String etapaTrabalho;
	
	@Value("#{props.string.subEstadoFaturamento:''}")
	private String subEstadoFaturamento;

	private String campanha;
	
	private String anoFiscal;
	
	private Boolean consolidado;
	
	private Boolean consolidadoPorFat;
	
	private String idsFaturamentosConsolidados;

	private Boolean enviadoAoCliente;
	
	@OneToOne
	private Comissao comissao;

	private String NumeroNF;

	@ManyToOne
	private BalanceteCalculo balancete;

	private String data;

	private String modo;

	private String dataCriacao;

	private boolean emitido;
	
	private String dataLiberacao;
	
	public String getDataLiberacao() {
		try {
			if (dataLiberacao.matches(".*/.*/.*")) {
				return dataLiberacao;
			} else {
				String ano = dataLiberacao.substring(0, 4);
				String mes = dataLiberacao.substring(5, 7);
				String dia = dataLiberacao.substring(8, 10);
				String dataFormatada = dia +"/"+ mes +"/"+ ano;

				return dataFormatada;
			}
		} catch (Exception e) {
			return "";
		}
	}

	public void setDataLiberacao(String dataLiberacao) {
		this.dataLiberacao = dataLiberacao;
	}


	
	private boolean totalFaturarAlterado;
	
	public boolean isTotalFaturarAlterado() {
		return totalFaturarAlterado;
	}

	public void setTotalFaturarAlterado(boolean totalFaturarAlterado) {
		this.totalFaturarAlterado = totalFaturarAlterado;
	}


	private String dataParaFaturar;


	public String getSubEstadoFaturamento() {
		return subEstadoFaturamento;
	}

	public void setSubEstadoFaturamento(String subEstadoFaturamento) {
		this.subEstadoFaturamento = subEstadoFaturamento;
	}

	public Comissao getComissao() {
		return comissao;
	}

	public void setComissao(Comissao comissao) {
		this.comissao = comissao;
	}

	public void setEmitido(boolean emitido) {
		this.emitido = emitido;
	}

	public String getAnoFiscal() {
		return anoFiscal;
	}

	public void setAnoFiscal(String anoFiscal) {
		this.anoFiscal = anoFiscal;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public String getCnpjFaturar() {
		return cnpjFaturar;
	}

	public void setCnpjFaturar(String cnpjFaturar) {
		this.cnpjFaturar = cnpjFaturar;
	}

	public String getRazaoSocialFaturar() {
		return razaoSocialFaturar;
	}

	public void setRazaoSocialFaturar(String razaoSocialFaturar) {
		this.razaoSocialFaturar = razaoSocialFaturar;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public ValorFixo getValorF() {
		return valorF;
	}

	public void setValorF(ValorFixo valorF) {
		this.valorF = valorF;
	}

	public PercentualEscalonado getPercentualE() {
		return percentualE;
	}

	public void setPercentualE(PercentualEscalonado percentualE) {
		this.percentualE = percentualE;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public BigDecimal getValorEtapa() {
		return valorEtapa;
	}

	public void setValorEtapa(BigDecimal valorEtapa) {
		this.valorEtapa = valorEtapa;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public boolean isLiberado() {
		return liberado;
	}

	public void setLiberado(boolean liberado) {
		this.liberado = liberado;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public EtapaTrabalho getEtapa() {
		return etapa;
	}

	public void setEtapa(EtapaTrabalho etapa) {
		this.etapa = etapa;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public float getPercetEtapa() {
		return percetEtapa;
	}

	public void setPercetEtapa(float percetEtapa) {
		this.percetEtapa = percetEtapa;
	}

	public PercentualFixo getPercentualF() {
		return percentualF;
	}

	public void setPercentualF(PercentualFixo percentualF) {
		this.percentualF = percentualF;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public EtapasConclusao getEtapas() {
		return etapas;
	}

	public void setEtapas(EtapasConclusao etapas) {
		this.etapas = etapas;
	}

	public String getObsContrato() {
		return obsContrato;
	}

	public void setObsContrato(String obsContrato) {
		this.obsContrato = obsContrato;
	}

	public String getEtapaTrabalho() {
		return etapaTrabalho;
	}

	public void setEtapaTrabalho(String etapaTrabalho) {
		this.etapaTrabalho = etapaTrabalho;
	}

	public String getCampanha() {
		return campanha;
	}

	public void setCampanha(String campanha) {
		this.campanha = campanha;
	}

	public boolean isManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}

	public String getNumeroNF() {
		return NumeroNF;
	}

	public void setNumeroNF(String numeroNF) {
		NumeroNF = numeroNF;
	}

	public BalanceteCalculo getBalancete() {
		return balancete;
	}

	public void setBalancete(BalanceteCalculo balancete) {
		this.balancete = balancete;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public String getDataCriacao() {
		try {
			if (dataCriacao.matches(".*/.*/.*")) {
				return dataCriacao;
			} else {
				String ano = dataCriacao.substring(0, 4);
				String mes = dataCriacao.substring(5, 7);
				String dia = dataCriacao.substring(8, 10);
				String dataFormatada = dia +"/"+ mes +"/"+ ano;

				return dataFormatada;
			}
		} catch (Exception e) {
			return "";
		}
	}

	public void setDataCriacao(String dataCriacao) {
		String dataFormatada = "";
		System.out.println(dataCriacao + " ppppppppp");
		
		try {
			if (dataCriacao.matches(".*/.*/.*")) {
				String ano = dataCriacao.substring(6, 10);
				String mes = dataCriacao.substring(3, 5);
				String dia = dataCriacao.substring(0, 2);
				dataFormatada = ano + "-" + mes + "-" + dia;
				this.dataCriacao = dataFormatada;
			} else {
				dataFormatada = dataCriacao;
			}
		} catch (Exception e) {
			dataFormatada = "";
		
		this.dataCriacao = dataFormatada;
		}
	}

	public String getProdutoManual() {
		return produtoManual;
	}

	public void setProdutoManual(String produtoManual) {
		this.produtoManual = produtoManual;
	}
	
	public Boolean getEmitido() {
		return emitido;
	}

	public void setEmitido(Boolean emitido) {
		this.emitido = emitido;
	}

	public SubProducao getSubProducao() {
		return subProducao;
	}

	public void setSubProducao(SubProducao subProducao) {
		this.subProducao = subProducao;
	}

	public Boolean getConsolidado() {
		return consolidado;
	}

	public void setConsolidado(Boolean consolidado) {
		this.consolidado = consolidado;
	}

	public String getIdsFaturamentosConsolidados() {
		return idsFaturamentosConsolidados;
	}

	public void setIdsFaturamentosConsolidados(String idsFaturamentosConsolidados) {
		this.idsFaturamentosConsolidados = idsFaturamentosConsolidados;
	}
	
	public Boolean getEnviadoAoCliente() {
		return enviadoAoCliente;
	}

	public void setEnviadoAoCliente(Boolean enviadoAoCliente) {
		this.enviadoAoCliente = enviadoAoCliente;
	}
	
	public Boolean getConsolidadoPorFat() {
		return consolidadoPorFat;
	}

	public void setConsolidadoPorFat(Boolean consolidadoPorFat) {
		this.consolidadoPorFat = consolidadoPorFat;
	}
	
	public String getDataParaFaturar() {
		return dataParaFaturar;
	}

	public void setDataParaFaturar(String dataParaFaturar) {
		this.dataParaFaturar = dataParaFaturar;
	}

}
