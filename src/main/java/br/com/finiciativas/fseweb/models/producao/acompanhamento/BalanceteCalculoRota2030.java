package br.com.finiciativas.fseweb.models.producao.acompanhamento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.producao.Producao;

@Entity
public class BalanceteCalculoRota2030 {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private Consultor feitoPor;
	
	private int porcentagemRealizado;
	
	@OneToOne
	private Consultor validadoPor;
	
	private boolean dispendiosEstrategicos;
	
	private String valorDispendiosNaoEstrategicos;
	
	private String valorDispendiosEstrategicos;
	
	private String valorDispendiosTotal;
	
	private String exclusaoUtilizada;
	
	private String reducaoImposto;
	
	private String enviadoCliente;
	
	private String validadoCliente;
	
	private int versao;
	
	@ManyToOne
	private Producao producao;
	
	private boolean versaoFinal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Consultor getFeitoPor() {
		return feitoPor;
	}

	public void setFeitoPor(Consultor feitoPor) {
		this.feitoPor = feitoPor;
	}

	public int getPorcentagemRealizado() {
		return porcentagemRealizado;
	}

	public void setPorcentagemRealizado(int porcentagemRealizado) {
		this.porcentagemRealizado = porcentagemRealizado;
	}

	public Consultor getValidadoPor() {
		return validadoPor;
	}

	public void setValidadoPor(Consultor validadoPor) {
		this.validadoPor = validadoPor;
	}

	public boolean isDispendiosEstrategicos() {
		return dispendiosEstrategicos;
	}

	public void setDispendiosEstrategicos(boolean dispendiosEstrategicos) {
		this.dispendiosEstrategicos = dispendiosEstrategicos;
	}

	public String getValorDispendiosNaoEstrategicos() {
		return valorDispendiosNaoEstrategicos;
	}

	public void setValorDispendiosNaoEstrategicos(String valorDispendiosNaoEstrategicos) {
		this.valorDispendiosNaoEstrategicos = valorDispendiosNaoEstrategicos;
	}

	public String getValorDispendiosEstrategicos() {
		return valorDispendiosEstrategicos;
	}

	public void setValorDispendiosEstrategicos(String valorDispendiosEstrategicos) {
		this.valorDispendiosEstrategicos = valorDispendiosEstrategicos;
	}

	public String getValorDispendiosTotal() {
		return valorDispendiosTotal;
	}

	public void setValorDispendiosTotal(String valorDispendiosTotal) {
		this.valorDispendiosTotal = valorDispendiosTotal;
	}

	public String getExclusaoUtilizada() {
		return exclusaoUtilizada;
	}

	public void setExclusaoUtilizada(String exclusaoUtilizada) {
		this.exclusaoUtilizada = exclusaoUtilizada;
	}

	public String getReducaoImposto() {
		return reducaoImposto;
	}

	public void setReducaoImposto(String reducaoImposto) {
		this.reducaoImposto = reducaoImposto;
	}

	public String getEnviadoCliente() {
		return enviadoCliente;
	}

	public void setEnviadoCliente(String enviadoCliente) {
		this.enviadoCliente = enviadoCliente;
	}

	public String getValidadoCliente() {
		return validadoCliente;
	}

	public void setValidadoCliente(String validadoCliente) {
		this.validadoCliente = validadoCliente;
	}

	public int getVersao() {
		return versao;
	}

	public void setVersao(int versao) {
		this.versao = versao;
	}

	public Producao getProducao() {
		return producao;
	}

	public void setProducao(Producao producao) {
		this.producao = producao;
	}

	public boolean isVersaoFinal() {
		return versaoFinal;
	}

	public void setVersaoFinal(boolean versaoFinal) {
		this.versaoFinal = versaoFinal;
	}
	
}
