package br.com.finiciativas.fseweb.models.produto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Tarefa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;

	@ColumnTransformer(read = "UPPER(DESCRICAO)")
	private String descricao;
	
	@JsonIgnore
	@ManyToMany
	private List<ItemTarefa> itensTarefa = new ArrayList<ItemTarefa>();
	
	@ManyToMany(fetch = FetchType.LAZY)
	private List<EtapaTrabalho> etapas;	

	public Tarefa() {

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<ItemTarefa> getItensTarefa() {
		return itensTarefa;
	}

	public void setItensTarefa(List<ItemTarefa> itensTarefa) {
		this.itensTarefa = itensTarefa;
	}

	public List<EtapaTrabalho> getEtapas() {
		return etapas;
	}

	public void setEtapas(List<EtapaTrabalho> etapas) {
		this.etapas = etapas;
	}

}
