package br.com.finiciativas.fseweb.abstractclass;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.swing.text.MaskFormatter;

import org.hibernate.annotations.ColumnTransformer;

@MappedSuperclass
public abstract class Empresa {

	@Id
	@GeneratedValue
	private Long id;

	private String cnpj;
	
	@ColumnTransformer(read = "UPPER(RAZAO_SOCIAL)")
	private String razaoSocial;

	private String site;
	
	private String categoria;

	private boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpj() {
		this.cnpj = this.cnpj.replaceAll("(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})", "$1.$2.$3/$4-$5");
	    	return cnpj;	
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}
	
	public void removeCharacteresCnpj() {
		if(this.cnpj==null) {
			cnpj = "";
		}
		this.cnpj = this.cnpj.replace(".", "").replace("-", "").replace("/", "");
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
}
