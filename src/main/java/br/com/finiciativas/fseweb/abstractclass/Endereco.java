package br.com.finiciativas.fseweb.abstractclass;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.ColumnTransformer;

@MappedSuperclass
public abstract class Endereco {

	@Id
	@GeneratedValue
	private Long id;

	private String cep;

	private String logradouro;

	@ColumnTransformer(read = "UPPER(CIDADE)")
	private String cidade;
	
	@ColumnTransformer(read = "UPPER(ESTADO)")
	private String estado;

	@ColumnTransformer(read = "UPPER(COMPLEMENTO)")
	private String complemento;
	
	@ColumnTransformer(read = "UPPER(BAIRRO)")
	private String bairro;
	
	private String numero;

	public Endereco() {

	}

	public Endereco(Endereco endereco) {

		if (endereco != null) {
			this.id = endereco.getId();
			this.cep = endereco.getCep();
			this.logradouro = endereco.getLogradouro();
			this.cidade = endereco.getCidade();
			this.bairro = endereco.getBairro();
			this.numero = endereco.getNumero();
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	

}
