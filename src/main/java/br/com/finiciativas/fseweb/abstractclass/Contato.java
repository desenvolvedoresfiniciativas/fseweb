package br.com.finiciativas.fseweb.abstractclass;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.ColumnTransformer;

import br.com.finiciativas.fseweb.models.cliente.Cliente;

@MappedSuperclass
public abstract class Contato {

	@Id
	@GeneratedValue
	private Long id;

	@ColumnTransformer(read = "UPPER(NOME)")
	private String nome;

	@ColumnTransformer(read = "UPPER(CARGO)")
	private String cargo;

	private String telefone1;
	
	private String telefone2;

	@ColumnTransformer(read = "UPPER(EMAIL)")
	private String email;

	private boolean ativo;
	
	@ManyToOne
	private Cliente cliente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
