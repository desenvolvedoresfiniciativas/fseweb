package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.instituicao.ContatoInstituicao;

public interface ContatoInstituicaoService {

	List<ContatoInstituicao> getContatosInstituicao(Long id);
	
	void create(ContatoInstituicao contatoInstituicao, Long id);
	
	void update(ContatoInstituicao contatoInstituicao);
	
	void remove(ContatoInstituicao contatoInstituicao);
	
	ContatoInstituicao findById(Long id);
	
	Long getIdInstituicaoOfContact(Long id);
	
	
}
