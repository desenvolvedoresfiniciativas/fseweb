package br.com.finiciativas.fseweb.services.impl;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.finiciativas.fseweb.repositories.impl.ValorNegocioCORepositoryImpl;

@Service
@Transactional 
public class ValorNegocioCOServiceImpl {

	@Autowired
	private ValorNegocioCORepositoryImpl coRepositoryImpl;
	
	
	/**
	 * 
	 * Me'todo para generar Valor de Negocio Colombia:
	 * 		Este metodo toma los valores de itemValores de las producciones 
	 * 		y  llena estos datos en las tablas ValorEstimado, ValorPostulado, ValorAprovado 
	 */
	public void generarValorNegocioColombia() {
		System.out.println("Entramos a service...");
		coRepositoryImpl.generarValorNegocioColombia();
	}
	
}
