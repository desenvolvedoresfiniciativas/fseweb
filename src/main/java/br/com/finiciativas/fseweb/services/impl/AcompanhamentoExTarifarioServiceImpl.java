package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.AcompanhamentoExTarifario;
import br.com.finiciativas.fseweb.repositories.impl.AcompanhamentoExTarifarioRepositoryImpl;

@Service
public class AcompanhamentoExTarifarioServiceImpl {

	@Autowired
	private AcompanhamentoExTarifarioRepositoryImpl repository;
	
	public AcompanhamentoExTarifario create(AcompanhamentoExTarifario acompanhamento) {
		 return this.repository.create(acompanhamento);
	}

	public void delete(AcompanhamentoExTarifario acompanhamento) {
		this.repository.delete(acompanhamento);
	}

	public AcompanhamentoExTarifario update(AcompanhamentoExTarifario acompanhamento) {
		return this.repository.update(acompanhamento);
	}

	public List<AcompanhamentoExTarifario> getAll() {
		return this.repository.getAll();
	}

	public AcompanhamentoExTarifario findById(Long id) {
		return this.repository.findById(id);
	}

	public List<AcompanhamentoExTarifario> getAcompanhamentoExTarifarioBySubProducao(Long id) {
		return this.repository.getAcompanhamentoExTarifarioBySubProducao(id);
	}

}
