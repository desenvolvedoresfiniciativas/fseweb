package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;

public interface ProdutoService {
	
	List<Produto> getAll();
	
	void create(Produto produto);
	
	void delete(Produto produto);
	
	Produto update(Produto produto);
	
	Produto findById(Long id);
	
	List<EtapaTrabalho> getEtapasTrabalho(Long idProduto);
	
	List<Tarefa> getTarefas(Long idEtapa);
	
}
