package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.repositories.impl.PercentualEscalonadoRepositoryImpl;

@Service
public class PercentualEscalonadoServiceImpl{

	@Autowired
	private PercentualEscalonadoRepositoryImpl PercentualEscalonadoRepository;
	
	public PercentualEscalonado create(PercentualEscalonado PercentualEscalonado) {
		return this.PercentualEscalonadoRepository.create(PercentualEscalonado);
	}

	public PercentualEscalonado update(PercentualEscalonado PercentualEscalonado) {
		return this.PercentualEscalonadoRepository.update(PercentualEscalonado);
	}

	public PercentualEscalonado findById(Long id) {
		return this.PercentualEscalonadoRepository.findById(id);
	}
	
	public List<PercentualEscalonado> getAll() {
		return this.PercentualEscalonadoRepository.getAll();
	}
	

}
