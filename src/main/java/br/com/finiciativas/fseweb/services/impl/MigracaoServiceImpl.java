package br.com.finiciativas.fseweb.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.repositories.impl.MigracaoRepositoryImpl;

@Service
public class MigracaoServiceImpl {

	@Autowired
	private MigracaoRepositoryImpl repository;

	public void lerExcel() {
		this.repository.lerExcel();
	}
	
	public void populaProducoes(){
		this.repository.populaProducoes();
	}
	
	public void criarFiscalizacoes() {
		this.repository.criarFiscalizacoes();
	}
	
	public void populaFiscalizacao(){
		this.repository.populaFiscalizacao();
	}
	
	public void identificaPostulacoes() {
		this.repository.identificaPostulacoes();
	}
	
	public void identificaFiscalizacoes() {
		this.repository.identificaFiscalizacoes();
	}
	
	public void atualizarTipoContrato() {
		this.repository.atualizarTipoContrato();
	}
	
	public void procuraProds() {
		this.repository.procuraProds();
	}
	
	public void geraModificaiones() {
		this.repository.geraModificaiones();
	}
	
}