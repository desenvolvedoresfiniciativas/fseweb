package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnicoFinanceiro;
import br.com.finiciativas.fseweb.repositories.RelatorioTecnicoFinanceiroRepositoryImpl;

@Service
public class RelatorioTecnicoFinanceiroServiceImpl {

	@Autowired
	private RelatorioTecnicoFinanceiroRepositoryImpl repository;
	
	public void create(RelatorioTecnicoFinanceiro relatorio) {
		this.repository.create(relatorio);
	}

	public void delete(RelatorioTecnicoFinanceiro relatorio) {
		this.repository.delete(relatorio);
	}

	public RelatorioTecnicoFinanceiro update(RelatorioTecnicoFinanceiro relatorio) {
		return this.repository.update(relatorio);
	}

	public List<RelatorioTecnicoFinanceiro> getAll() {
		return this.repository.getAll();
	}

	public RelatorioTecnicoFinanceiro findById(Long id) {
		return this.repository.findById(id);
	}

	public List<RelatorioTecnicoFinanceiro> getRelatorioTecnicoFinanceiroByAcompanhamento(Long id) {
		return this.repository.getRelatorioTecnicoFinanceiroByAcompanhamento(id);
	}
	
	
}
