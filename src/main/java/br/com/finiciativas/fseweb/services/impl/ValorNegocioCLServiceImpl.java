package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocioCL;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorAprovado;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorNegocioChile;
import br.com.finiciativas.fseweb.repositories.impl.PresupuestosValorNegocioCRepositoryImpl;
import br.com.finiciativas.fseweb.repositories.impl.ValorDisponibleRepositoryImpl;
import br.com.finiciativas.fseweb.repositories.impl.ValorNegocioCLRepositoryImpl;

@Service
public class ValorNegocioCLServiceImpl {

	@Autowired
	private ValorDisponibleRepositoryImpl disponibleRepositoryImpl;
	
	@Autowired
	private ValorNegocioCLRepositoryImpl clRepositoryImpl;
	
	public void updateDisponible() {
		disponibleRepositoryImpl.geraValorDisponible();
	}
	
	public void geraVN() {
		clRepositoryImpl.geraVN();
	}
	public List<ValorNegocioChile> getAllVN() {
		return clRepositoryImpl.getAllFromVNCL();
	}
}
