package br.com.finiciativas.fseweb.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.repositories.impl.NotaFiscalRepositoryImpl;

@Service
public class NotaFiscalServiceImpl {

	@Autowired
	private NotaFiscalRepositoryImpl repositoryImpl;
	
	public NotaFiscal create(NotaFiscal notaFiscal) {
		return this.repositoryImpl.create(notaFiscal);
	}
	
	public void delete(NotaFiscal notaFiscal) {
		this.repositoryImpl.delete(notaFiscal);
	}
	
	public NotaFiscal update(NotaFiscal notaFiscal) {
		return this.repositoryImpl.update(notaFiscal);
	}
	
	public List<NotaFiscal> getAll(){
		return this.repositoryImpl.getAll();
	}
	
	public List<NotaFiscal> getNotasByAnoWhereNN(String ano){
		return this.repositoryImpl.getNotasByAnoWhereNN(ano);
	}
	
	public List<NotaFiscal> getNotasByCampanha(String ano) {
		return this.repositoryImpl.getNotasByCampanha(ano);
	}
	
	public List<NotaFiscal> getNotasByComercialEntrada(Long idConsultor) {
		return this.repositoryImpl.getNotasByComercialEntrada(idConsultor);
	}

	public NotaFiscal findById (Long id) {
		return this.repositoryImpl.findById(id);
	}
	
	public NotaFiscal findByClienteContratoFaturamento (Long idCliente, Long idContrato, Long idFaturamento) {
		return this.repositoryImpl.findByClienteContratoFaturamento(idCliente, idContrato, idFaturamento);
	}
	
	public NotaFiscal findByFaturamento(Long idFaturamento) {
		return this.repositoryImpl.findByFaturamento(idFaturamento);
	}
	
	public List<NotaFiscal> getNotasByCliente(Long id) {
		return this.repositoryImpl.getNotasByCliente(id);
	}
	
	public List<NotaFiscal> getNotasByFaturamento(Long id){
		return this.repositoryImpl.getNotasByFaturamento(id);
	}
	
	public List<NotaFiscal> getAllNotasByCriteria(String campanha, Long idProduto, Long idFilial, String empresa,

			String tipoDeNegocio, Long etapa, Boolean cancelada, Boolean emAtraso, String motivoCancelamento, 
			String numeroNF, String dataInicioEmissao, String dataFinalEmissao, String dataInicioCobrar, String dataFinalCobrar,
			String dataInicioCobranca, String dataFinalCobranca){

		
		return this.repositoryImpl.getAllNotasByCriteria(campanha, idProduto, idFilial, empresa, tipoDeNegocio, etapa, cancelada,
				emAtraso, motivoCancelamento, numeroNF, dataInicioEmissao,dataFinalEmissao,dataInicioCobrar,dataFinalCobrar,dataInicioCobranca,dataFinalCobranca);
	}
	
	public List<NotaFiscal> getNotasByCriteria(String ano, Long idProduto, Long etapa, Long idFilial, String empresa, String tipoNegocio,String numeroNF, Boolean atraso, Boolean pago){
		return this.repositoryImpl.getNotasByCriteria(ano, idProduto, etapa, idFilial, empresa, tipoNegocio, numeroNF, atraso, pago);
	}
	
	public List<NotaFiscal> getNotaSubstituida(String idNotaSubstituida) {
		 return this.repositoryImpl.getNotaSubstituida(idNotaSubstituida);
	}

	public List<NotaFiscal> getSomaPagamentoByCriteria(String dataInicial, String dataFinal, String campanha) {
		return this.repositoryImpl.getSomaPagamentoByCriteria(dataInicial, dataFinal, campanha);
	}
	
	public List<NotaFiscal> getSomaPagamentoDateByCriteria(Date dataInicial, Date dataFinal, String campanha) {
		return this.repositoryImpl.getSomaPagamentoDateByCriteria(dataInicial, dataFinal, campanha);
	}
	
}
