package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.consultor.Consultor;

public interface EquipeService {
	
	List<Equipe> getAll();
	
	void create(Equipe equipe);
	
	Equipe update(Equipe equipe);
	
	Equipe findById(Long id);
	
	void remove(Long id);
	
	List<Consultor> getConsultores(Long idEquipe);
	
	Equipe getConsultorByEquipe(Long id);
	
	Equipe findEquipeByConsultor(Long idConsultor);

	List<Consultor> getAllConsultores();

	List<Equipe> getAllEquipesByCoordenador(Long idCoordenador);

	List<Equipe> getAllEquipesByGerenteGeral(Long idGerenteGeral);
	
}