package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.consultor.Role;
import br.com.finiciativas.fseweb.repositories.impl.RoleRepositoryImpl;
import br.com.finiciativas.fseweb.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepositoryImpl repository;

	public List<Role> getAll() {
		return this.repository.getAll();
	}

}
