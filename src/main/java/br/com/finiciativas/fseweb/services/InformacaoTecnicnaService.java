package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoTecnica;

public interface InformacaoTecnicnaService {

	void create(InformacaoTecnica informacao);

	void delete(InformacaoTecnica informacao);
		
	InformacaoTecnica update(InformacaoTecnica informacao);
	
	List<InformacaoTecnica> getAll();
	
	InformacaoTecnica findById(Long id);
	
	List<InformacaoTecnica> getInformacaoTecnicaByAcompanhamento(Long id);

	
}
