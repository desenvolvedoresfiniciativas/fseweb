package br.com.finiciativas.fseweb.services;

import java.util.List;

public interface RelatorioTecnicoFinanceiro {

	void create(RelatorioTecnicoFinanceiro relatorio);

	void delete(RelatorioTecnicoFinanceiro relatorio);
		
	RelatorioTecnicoFinanceiro update(RelatorioTecnicoFinanceiro relatorio);
	
	List<RelatorioTecnicoFinanceiro> getAll();
	
	RelatorioTecnicoFinanceiro findById(Long id);
	
	List<RelatorioTecnicoFinanceiro> getRelatorioTecnicoFinanceiroByAcompanhamento(Long id);
	
}
