package br.com.finiciativas.fseweb.services;

import br.com.finiciativas.fseweb.models.vigencia.Vigencia;

public interface VigenciaService {
	
	Vigencia update(Vigencia vigencia);
	
}
