package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.HabilitacaoPortaria;
import br.com.finiciativas.fseweb.repositories.impl.HabilitacaoPortariaRepositoryImpl;

@Service
public class HabilitacaoPortariaServiceImpl {

	@Autowired
	private HabilitacaoPortariaRepositoryImpl repository;
	
	public void create(HabilitacaoPortaria HabilitacaoPortaria) {
		this.repository.create(HabilitacaoPortaria);
	}

	public void delete(HabilitacaoPortaria HabilitacaoPortaria) {
		this.repository.delete(HabilitacaoPortaria);
	}

	public HabilitacaoPortaria update(HabilitacaoPortaria HabilitacaoPortaria) {
		return this.repository.update(HabilitacaoPortaria);
	}
	
	public List<HabilitacaoPortaria> getAll() {
		return this.repository.getAll();
	}
	
	public HabilitacaoPortaria findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<HabilitacaoPortaria> getHabilitacaoPortariaByProducao(Long id) {
		return this.repository.getHabilitacaoPortariaByProducao(id);
	}
	
}
