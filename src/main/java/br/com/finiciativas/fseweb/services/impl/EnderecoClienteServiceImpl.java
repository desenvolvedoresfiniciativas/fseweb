package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.repositories.impl.EnderecoClienteRepositoryImpl;
import br.com.finiciativas.fseweb.services.EnderecoClienteService;

@Service
public class EnderecoClienteServiceImpl implements EnderecoClienteService {
	
	@Autowired
	private EnderecoClienteRepositoryImpl enderecoRepository;

	@Override
	public EnderecoCliente update(EnderecoCliente enderecoCliente) {
		return this.enderecoRepository.update(enderecoCliente);
	}
	
	public EnderecoCliente getLastItemAdded() {
		return this.enderecoRepository.getLastItemAdded();
	}
	
	public List<EnderecoCliente> getAllById(Long id){
		return this.enderecoRepository.getAllById(id);
	}

	@Override
	public void create(EnderecoCliente enderecoCliente) {
		this.enderecoRepository.create(enderecoCliente);
	}
	
	public void delete(EnderecoCliente ender) {
		this.enderecoRepository.delete(ender);
	}
	
	public EnderecoCliente findById(Long id) {
		return this.enderecoRepository.findById(id);
	}
	
	public List<EnderecoCliente> getEnderecoClienteNF(Long idCliente) {
		return this.enderecoRepository.getEnderecoClienteNF(idCliente);
	}
	
}
