package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.old.ContratoOld;
import br.com.finiciativas.fseweb.repositories.impl.ContratoOldRepositoryImpl;

@Service
public class ContratoOldServiceImpl {

	@Autowired
	private ContratoOldRepositoryImpl repository;

	public void create(ContratoOld contratoOld) {
		this.repository.create(contratoOld);
	}

	public void delete(ContratoOld contratoOld) {
		this.repository.delete(contratoOld);
	}

	public ContratoOld update(ContratoOld contratoOld) {
		return this.repository.update(contratoOld);
	}

	public List<ContratoOld> getAll() {
		return this.repository.getAll();
	}

	public ContratoOld findById(Long id) {
		return this.repository.findById(id);
	}
}
