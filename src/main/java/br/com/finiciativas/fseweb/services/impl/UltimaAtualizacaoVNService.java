package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.valorNegocio.UltimaAtualizacaoVN;
import br.com.finiciativas.fseweb.repositories.impl.UltimaAtualizacaoVNRepositoryImpl;

@Service
public class UltimaAtualizacaoVNService {
	
	@Autowired
	private UltimaAtualizacaoVNRepositoryImpl repository;
	
	public void create(UltimaAtualizacaoVN vn) {
		this.repository.create(vn);
	}
	
	public void update(UltimaAtualizacaoVN vn) {
		 this.repository.update(vn);
	}

	public UltimaAtualizacaoVN findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<UltimaAtualizacaoVN>  getAll() {
		return this.repository.getAll();
	}

}
