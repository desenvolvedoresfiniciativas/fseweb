package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.PreProducao;
import br.com.finiciativas.fseweb.repositories.impl.PreProducaoRepositoryImpl;

@Service
public class PreProducaoServiceImpl {

	@Autowired
	private PreProducaoRepositoryImpl repository;
	
	public PreProducao create(PreProducao preProducao) {
		 return this.repository.create(preProducao);
	}

	public void delete(PreProducao preProducao) {
		this.repository.delete(preProducao);
	}

	public PreProducao update(PreProducao preProducao) {
		return this.repository.update(preProducao);
	}

	public List<PreProducao> getAll() {
		return this.repository.getAll();
	}

	public PreProducao findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<PreProducao> getAllByCliente(Long id) {
		return this.repository.getAllByCliente(id);
	}
	
	public PreProducao getPreByAnoCliente(Long id, String ano) {
		return this.repository.getPreByAnoCliente(id, ano);
	}
}
