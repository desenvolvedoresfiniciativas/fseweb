package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.CRM.Oportunidade;
import br.com.finiciativas.fseweb.repositories.impl.OportunidadeRepositoryImpl;

@Service
public class OportunidadeServiceImpl {

	@Autowired
	private OportunidadeRepositoryImpl repository;
	
	public List<Oportunidade> getAll() {
		return this.repository.getAll();
	}

	public void create(Oportunidade Oportunidade) {
		this.repository.create(Oportunidade);
	}

	public Oportunidade update(Oportunidade Oportunidade) {
		return this.repository.update(Oportunidade);
	}

	public Oportunidade findById(Long id) {
		return this.repository.findById(id);
	}

	public void remove(Long id) {
		this.repository.remove(id);
	}
	
}
