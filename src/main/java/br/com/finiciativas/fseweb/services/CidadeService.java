package br.com.finiciativas.fseweb.services;

import br.com.finiciativas.fseweb.models.Cidade;

public interface CidadeService {
	
	Cidade findCidade(Cidade cidade);
	
}
