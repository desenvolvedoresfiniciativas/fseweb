package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.ReceptoresEmail;
import br.com.finiciativas.fseweb.repositories.impl.ReceptoresEmailRepositoryImpl;

@Service
public class ReceptoresEmailServiceImpl {

	@Autowired
	private ReceptoresEmailRepositoryImpl repository;
	
	public void create(ReceptoresEmail ReceptoresEmail) {
		this.repository.create(ReceptoresEmail);
	}

	public void delete(ReceptoresEmail ReceptoresEmail) {
		this.repository.delete(ReceptoresEmail);
	}

	public ReceptoresEmail update(ReceptoresEmail ReceptoresEmail) {
		return this.repository.update(ReceptoresEmail);
	}

	public List<ReceptoresEmail> getAll() {
		return this.repository.getAll();
	}

	public ReceptoresEmail findById(Long id) {
		return this.repository.findById(id);
	}

	public ReceptoresEmail getByTipo(String tipo) {
		return this.repository.getByTipo(tipo);
	}
	
}
