package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.produto.ordenacao.OrdemItensTarefa;
import br.com.finiciativas.fseweb.repositories.impl.OrdemItensTarefaRepositoryImpl;

@Service
public class OrdemItensTarefaServiceImpl {

	@Autowired
	private OrdemItensTarefaRepositoryImpl repository;
	
	public List<OrdemItensTarefa> getAll() {
		return this.repository.getAll();
	}

	public void create(OrdemItensTarefa OrdemItensTarefa) {
		this.repository.create(OrdemItensTarefa);
	}

	public OrdemItensTarefa update(OrdemItensTarefa OrdemItensTarefa) {
		return this.repository.update(OrdemItensTarefa);
	}

	public OrdemItensTarefa findById(Long id) {
		return this.repository.findById(id);
	}

	public void remove(Long id) {
		this.repository.remove(id);
	}
	
	public List<OrdemItensTarefa> getOrdenacaoByProduto(Long id){
		return this.repository.getOrdenacaoByProduto(id);
	}
	
	public List<OrdemItensTarefa> getOrdenacaoByTarefa(Long id){
		return this.repository.getOrdenacaoByTarefa(id);
	}
	
	public OrdemItensTarefa checkOrdenacaoExists(Long idItem, Long idTarefa, Long idProduto){
		return this.repository.checkOrdenacaoExists(idItem, idTarefa, idProduto);
	}
	
	public void createOrdenacaoOfProduto(Long id) {
		this.repository.createOrdenacaoOfProduto(id);
	}
	
	public OrdemItensTarefa getLastOrdemOfTarefa(Long id){
		return this.repository.getLastOrdemOfTarefa(id);
	}
	
	public void ordenaEmMassa(List<OrdemItensTarefa> ordenacao){
		this.repository.ordenaEmMassa(ordenacao);
	}
	
}
