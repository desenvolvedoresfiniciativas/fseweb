package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;

public interface ComissaoService {

	Comissao create(Comissao comissao);

	Comissao update(Comissao comissao);

	void delete(Comissao comissao);

	Comissao findById(Long id);

	Comissao getLastAdded();

	List<Comissao> getAll();

	List<Comissao> getComissaoByConsultor(Long id);

	List<Comissao> getComissaoByGestor(Long id);

	List<Comissao> getComissaoByAnalista(Long id);

	Comissao findByProducao(Long id);
	
	Comissao checkIfComissaoExistWithProducao(Long id);

	List<Comissao> getAllByProducao(Long id);

	public void gerarComissaoByAno(String ano) ;

	public void createComissaoByNota(NotaFiscal nota);
	
	List<Comissao> getComissaoByFaturamento(Long id);

	List<Comissao> getComissaoAtivaByConsultor(Long id);

	List<Comissao> getComissoesReaisByContrato(Long id);

	void gerarComissaoByConsultor(Long idConsultor);

	void deleteByContrato(Long idContrato);

	void deleteByFaturamento(Long idFaturamento);
}
