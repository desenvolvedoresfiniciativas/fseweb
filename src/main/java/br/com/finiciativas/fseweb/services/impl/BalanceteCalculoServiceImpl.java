package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.repositories.impl.BalanceteCalculoRepositoryImpl;

@Service
public class BalanceteCalculoServiceImpl {

	@Autowired
	private BalanceteCalculoRepositoryImpl repository;
	
	public BalanceteCalculo create(BalanceteCalculo balancete) {
		 return this.repository.create(balancete);
	}

	public void delete(BalanceteCalculo balancete) {
		this.repository.delete(balancete);
	}
	
	public void deleteOfProducao(Long id){
		this.repository.deleteOfProducao(id);
	}

	public BalanceteCalculo update(BalanceteCalculo balancete) {
		return this.repository.update(balancete);
	}

	public List<BalanceteCalculo> getAll() {
		return this.repository.getAll();
	}

	public BalanceteCalculo findById(Long id) {
		return this.repository.findById(id);
	}

	public List<BalanceteCalculo> getBalanceteCalculoByProducao(Long id) {
		return this.repository.getBalanceteCalculoByProducao(id);
	}
	
	public BalanceteCalculo getLastBalanceteCalculoByProducao(Long id) {
		return this.repository.getLastBalanceteCalculoByProducao(id);
	}
	
	public BalanceteCalculo getReducaoImpostoByProducao(Long id) {
		return this.repository.getLastBalanceteCalculoByProducao(id);
	}
	
	public List<BalanceteCalculo> getAllBalanceteByAno(String ano){
		return this.repository.getAllBalanceteByAno(ano);
	}

	public List<BalanceteCalculo> getBalancetesByProducao(Long idProducao){
		return this.repository.getBalancetesByProducao(idProducao);
	}

	public List<BalanceteCalculo> getBalancetesPrejuizoByProducao(Long idProducao) {
		return this.repository.getBalancetesPrejuizoByProducao(idProducao);
	}

	public List<BalanceteCalculo> getBalancetesNaoPrejuizoByProducao(Long idProducao) {
		return this.repository.getBalancetesNaoPrejuizoByProducao(idProducao);
	}

}
