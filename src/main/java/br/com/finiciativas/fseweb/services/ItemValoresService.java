package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.produto.ItemValores;

public interface ItemValoresService {

	void create(ItemValores item);

	List<ItemValores> getAll();

	ItemValores update(ItemValores itemTarefa);

	ItemValores findById(Long id);

	List<ItemValores> findByTarefaId(Long id);

	List<ItemValores> findByTarefaAndProducao(Long id, Long idProd);

	ItemValores findByItemTarefaAndProducao(Long producaoId, Long id);

}
