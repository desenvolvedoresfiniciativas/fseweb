package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.AuditoriaEconomica;
import br.com.finiciativas.fseweb.repositories.impl.AuditoriaEconomicaRepositoryImpl;

@Service
public class AuditoriaEconomicaServiceImpl {

	@Autowired
	private AuditoriaEconomicaRepositoryImpl repository;
	
	public void create(AuditoriaEconomica AuditoriaEconomica) {
		this.repository.create(AuditoriaEconomica);
	}

	public void delete(AuditoriaEconomica AuditoriaEconomica) {
		this.repository.delete(AuditoriaEconomica);
	}

	public AuditoriaEconomica update(AuditoriaEconomica AuditoriaEconomica) {
		return this.repository.update(AuditoriaEconomica);
	}
	
	public List<AuditoriaEconomica> getAll() {
		return this.repository.getAll();
	}
	
	public AuditoriaEconomica findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<AuditoriaEconomica> getAuditoriaEconomicaByProducao(Long id) {
		return this.repository.getAuditoriaEconomicaByProducao(id);
	}
	
}
