package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.repositories.impl.EtapaPagamentoRepositoryImpl;

@Service
public class EtapaPagamentoServiceImpl {
	
	@Autowired
	private EtapaPagamentoRepositoryImpl repository;

	public List<EtapasPagamento> getAll() {
		return this.repository.getAll();
	}

	public void create(EtapasPagamento EtapasPagamento) {
		this.repository.create(EtapasPagamento);
	}
	
	public EtapasPagamento findByContratoId(Long id) {
		return this.repository.findByContratoId(id);
	}

	public EtapasPagamento update(EtapasPagamento EtapasPagamento) {
		return this.repository.update(EtapasPagamento);
	}
	
	public EtapasPagamento findById(Long id) {
		return this.repository.findById(id);
	}

	public void remove(Long id) {
		this.repository.remove(id);
	}
		
	public EtapasPagamento getLastAdded(){
		return this.repository.getLastAdded();
	}
	
}
