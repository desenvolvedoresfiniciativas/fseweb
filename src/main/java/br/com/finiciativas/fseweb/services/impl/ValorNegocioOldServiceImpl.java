package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.old.ValorNegocioOld;
import br.com.finiciativas.fseweb.repositories.impl.ValorNegocioOldRepositoryImpl;

@Service
public class ValorNegocioOldServiceImpl {

	@Autowired
	private ValorNegocioOldRepositoryImpl repository;

	public void create(ValorNegocioOld ValorNegocioOld) {
		this.repository.create(ValorNegocioOld);
	}

	public void delete(ValorNegocioOld ValorNegocioOld) {
		this.repository.delete(ValorNegocioOld);
	}

	public ValorNegocioOld update(ValorNegocioOld ValorNegocioOld) {
		return this.repository.update(ValorNegocioOld);
	}

	public List<ValorNegocioOld> getAll() {
		return this.repository.getAll();
	}

	public ValorNegocioOld findById(Long id) {
		return this.repository.findById(id);
	}

}
