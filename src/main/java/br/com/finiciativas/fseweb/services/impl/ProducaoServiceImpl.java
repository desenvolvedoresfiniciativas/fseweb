package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.Especialidade;
import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.producao.PreProducao;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Projetos;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioSintetico;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.pojo.SeguimentoPojo;
import br.com.finiciativas.fseweb.repositories.impl.ProducaoRepositoryImpl;

@Service
public class ProducaoServiceImpl {

	@Autowired
	private ProducaoRepositoryImpl repository;
	
	public List<Producao> getAll(){
		return this.repository.getAll();
	}
	
	public void deleteProducao(Long id) {
		this.repository.deleteProducao(id);
	}

	public Producao create (Producao producao){
		return this.repository.create(producao);
	}
	
	public void delete (Producao producao){
		this.repository.delete(producao);
	}
	
	public Producao update (Producao producao){
		return this.repository.update(producao);
	}
	
	public Producao updateOperacional(Producao producao) {
		return this.repository.updateOperacional(producao);
	}
	
	public Producao findById(Long id){
		return this.repository.findById(id);
	}
	
	public List<Producao> getProducaoByContrato(Long id){
		return this.repository.getProducaoByContrato(id);
	}
	
	public List<Producao> getProducaoByCodigo(String codigo) {
		return this.repository.getProducaoByCodigo(codigo);
	}
	
	public List<Producao> getProducaoByConsultor(Long id){
		return this.repository.getProducaoByConsultor(id);
	}
	  
	public void createItemValores(Producao producao){
		this.repository.createItemValores(producao);
	}

	public Producao getLastItemAdded(){
		return this.repository.getLastItemAdded();
	}
	
	public void addValor(Long id, List<ItemTarefa> itens, Producao producao){
		this.repository.addValor(id, itens, producao);
	}
	
	public void addValorSubProducao(Long id, List<ItemTarefa> itens, SubProducao subProd) {
		this.repository.addValorSubProducao(id, itens, subProd);
	}
	
	public List<RelatorioTecnico> getRelatorioAnalitico(String ano, SetorAtividade setor, Especialidade especialidade, Long idEquipe, Long idConsultor, Long idFeitoPor, Long idCorrigidoPor, Estado estado){
		return this.repository.getRelatorioAnalitico(ano, setor, especialidade, idEquipe, idConsultor, idFeitoPor, idCorrigidoPor, estado);
	}
	
	public List<Producao> getRelatorioSintetico(String ano, SetorAtividade setorAtividade, Long idConsultor, Long idEquipe){
		return this.repository.getRelatorioSintetico(ano, setorAtividade, idConsultor, idEquipe);
	}
	
	public void bindProjetosToRelatorio(List<Projetos> proj) {
		this.repository.bindProjetosToRelatorio(proj);
	}
	
	public List<RelatorioSintetico> getAllRelatorio(){
		return this.repository.getAllRelatorio();
	}
	
	public void criarExcelAnalitico(List<Projetos> projetos, String caminho, String nomeArquivo) {
		this.repository.criarExcelAnalitico(projetos, caminho, nomeArquivo);
	}
	
	public void criarExcelSintetico(List<RelatorioSintetico> relatorios, String caminho, String nomeArquivo) {
		this.repository.criarExcelSintetico(relatorios, caminho, nomeArquivo);
	}
	
	public List<Producao> getProducaoByProduto(Long id) {
		return this.repository.getProducaoByProduto(id);
	}
	
	public Producao getProducaoByAnoEmpresaProduto(String ano, Long id, Long idProd) {
		return this.repository.getProducaoByAnoEmpresaProduto(ano, id,idProd);
	}
	
	public Producao getProducaoByAnoClienteProdutoOnlyNN(String ano, Long idCliente, Long idProd) {
		return this.repository.getProducaoByAnoClienteProdutoOnlyNN(ano, idCliente,idProd);
	}
	
	public Producao getProducaoByAnoAndRazaoSocial(String ano, String nome) {
		return this.repository.getProducaoByAnoAndRazaoSocial(ano, nome);
	}
	
	public List<Producao> getAllProducaoBynome(String nome){
		return this.repository.getAllProducaoBynome(nome);
	}
	
	public List<Producao> getAllProducaoByConulstorAndSituacao(Situacao situacao, Long id){
		return this.repository.getAllProducaoByConsultorAndSituacao(situacao, id);
	}
	
	public List<Producao> getAllProducaoByConsultorAndNN( Long id){
		return this.repository.getAllProducaoByConsultorAndNN( id);
	}
	
	public List<Producao> getAllProducaoByConsultorAndAno(String ano, Long id){
		return this.repository.getAllProducaoByConsultorAndAno(ano, id);
	}
	
	public List<Producao> getProducaoByEquipe(Long id) {
		return this.repository.getProducaoByEquipe(id);
	}
	
	public List<Producao> getProducaoByCoordenador(Long id) {
		return this.repository.getProducaoByCoordenador(id);
	}
	
	public List<Producao> getProducaoByGerente(Long id) {
		return this.repository.getProducaoByGerente(id);
	}
	
	public List<Producao> getProducaoDaEquipe(Long id) {
		return this.repository.getProducaoDaEquipe(id);
	}
	
	public List<Producao> getAllProducaoByEquipeAndSituacao(Situacao situacao, Long id){
		return this.repository.getAllProducaoByEquipeAndSituacao(situacao, id);
	}
	
	public List<Producao> getAllProducaoByDiretorAndSituacao(Situacao situacao){
		return this.repository.getAllProducaoByDiretorAndSituacao(situacao);
	}
	
	public List<Producao> getAllProducaoByEquipeAndAno(String ano, Long id){
		return this.repository.getAllProducaoByEquipeAndAno(ano, id);
	}
	
	public List<Producao> getAllProducaoByDiretorAndAno(String ano){
		return this.repository.getAllProducaoByDiretorAndAno(ano);
	}
	
	public Producao getProducaoChileCriteria(String ano, Long idProd, Long idCliente, String observacao, String nomeProjeto, String nombreSIA) {
		return this.repository.getProducaoChileCriteria(ano, idProd, idCliente, observacao, nomeProjeto, nombreSIA);
	}
	
	public Producao checkForProducao(Long idCliente, Long idProduto, String ano){
		return this.repository.checkForProducao(idCliente, idProduto, ano);
	}
	
	public PreProducao checkForPreProducao(Long idCliente, String ano) {
		return this.repository.checkForPreProducao(idCliente, ano);
	}
	
	public List<Producao> getProducoesByCliente(Long id) { 
		return this.repository.getProducoesByCliente(id);
	}
	
	public List<Producao> getAllByAno(String ano){
		return this.repository.getAllByAno(ano);
	}
	
	public List<Producao> getAllByAnoWhereNN(String ano){
		return this.repository.getAllByAnoWhereNN(ano);
	}
	
	public List<Producao> getAllAtivasByAnoWhereNN(String ano){
		return this.repository.getAllAtivasByAnoWhereNN(ano);
	}
	
	public List<SubProducao> getSubproducoesOfProducao(Long id){
		return this.repository.getSubproducoesOfProducao(id);
	}
	
	public List<SeguimentoPojo> getProducaoByCriteria(String razaoSocial, String ano, Long idFilial, Long idEquipe, Long idConsultor, Situacao situacao, SetorAtividade setor){
		return this.repository.getProducaoByCriteria(razaoSocial, ano, idFilial, idEquipe, idConsultor, situacao, setor);
	}
	
	public List<Producao> getProducaoByCriteriaOP(String razaoSocial, String ano, Long idFilial, Long idEquipe, Long idConsultor, Situacao situacao, SetorAtividade setor, Long idProduto) { 
		return this.repository.getProducaoByCriteriaOP(razaoSocial, ano, idFilial, idEquipe, idConsultor, situacao, setor, idProduto);
	}

	public List<Projetos> getRelatorioSinteticoPrev() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void geraProducoes(String ano) {
		this.repository.geraProducoes(ano);
	}

	public List<Producao> getListaBalancete(Long id) {
		return this.repository.getListaBalancete(id);
	}
	
	public List<Producao> getListaRelatorioTecnico(Long id) {
		return this.repository.getListaRelatorioTecnico(id);
	}
	
	public List<Producao> getListaBalanceteFaturamento(Long idProducao) {
		return this.repository.getListaBalanceteFaturamento(idProducao);
	}
	
	public List<Producao> getProducoesByClienteProdutoCampanha(Long id, Long produto, String ano) {
		return this.repository.getProducoesByClienteProdutoCampanha(id, produto, ano);
	}
	
	public List<Producao> getProducoesByCnpjEProduto(String cnpj, Long produto) {
		return this.repository.getProducoesByCnpjEProduto(cnpj, produto);
	}
	
	public Producao getProducaoById(Long id) { 
		return this.repository.getProducaoById(id);
	}

	public List<EtapasConclusao> getProducaoAndEtapasByCriteria(String razaoSocial, String ano, String siglaConsultor, Long idEquipe) {
		return this.repository.getProducaoAndEtapasByCriteria(razaoSocial, ano,siglaConsultor,idEquipe);
		
	}
	
	public List<String> getTotalAnos(Long id){
		return this.repository.getTotalAnos(id);
	}
	
	public List<Producao> getProducaoByAnoAndContrato(Long id, String ano){
		return this.repository.getProducaoByAnoAndContrato(id, ano);
	}
	
	public void linkPre(Long id, Long pre) {
		this.repository.updatePreProducao(id, pre);
	}
	
	public List<Producao> getProducoesByFinanciamento(String campanha, String razaoSocial, String seguimento, Long idEquipe) {
		return this.repository.getProducoesByFinanciamento(campanha, razaoSocial, seguimento, idEquipe);
	}
	
	public List<Producao> getAllProducaoByProdutoAndAno(Long produtoId, String ano){
		return this.repository.getAllProducaoByProdutoAndAno(produtoId, ano);
	}
	
}
