package br.com.finiciativas.fseweb.services;

import br.com.finiciativas.fseweb.models.consultor.Impostos;

public interface ImpostosService {

	Impostos getImpostosByCampanha2020();
	
	Impostos getImpostosbyId(Long id);
}
