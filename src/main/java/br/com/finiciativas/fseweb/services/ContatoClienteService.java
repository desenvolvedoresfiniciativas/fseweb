package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;

public interface ContatoClienteService {

	List<ContatoCliente> getContatosCliente(Long id);

	void create(ContatoCliente contatoCliente, Long id);

	void update(ContatoCliente contatoCliente);

	void remove(ContatoCliente contatoCliente);

	ContatoCliente findById(Long id);

	Long getIdClienteOfContact(Long id);

}
