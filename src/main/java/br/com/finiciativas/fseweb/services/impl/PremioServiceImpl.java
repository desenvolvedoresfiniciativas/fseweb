package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.models.consultor.Premio;
import br.com.finiciativas.fseweb.repositories.impl.PremioRepositoryImpl;
import br.com.finiciativas.fseweb.services.PremioService;

@Service
public class PremioServiceImpl implements PremioService{

	@Autowired
	private PremioRepositoryImpl premioRepository;

	@Override
	public Premio update(Premio premio) {
		return this.premioRepository.update(premio);
	}

	@Override
	public void delete(Premio premio) {
		this.premioRepository.delete(premio);
	}

	@Override
	public Premio findById(Long id) {
		return this.premioRepository.findById(id);
	}
	
	@Override
	public Premio findByConsultorAndCampanha(Long idConsultor, String campanha) {
		return this.premioRepository.findByConsultorAndCampanha(idConsultor, campanha);
	}
	
	@Override
	public boolean checkIfExistsByConsultorAndCampanha(Long idConsultor, String campanha) {
		return this.premioRepository.checkIfExistsByConsultorAndCampanha(idConsultor, campanha);
	}

	@Override
	public List<Premio> getAll() {
		return this.premioRepository.getAll();
	}
	
	@Override
	public List<Premio> getAllByProducao(Long id) {
		return this.premioRepository.getAllByProducao(id);
	}
	
	@Override
	public List<Premio> getAllByContrato(Long id) {
		return this.premioRepository.getAllByProducao(id);
	}

	@Override
	public List<Premio> getAllByAnalista(Long id) {
		return this.premioRepository.getAllByAnalista(id);
	}

	@Override
	public List<Premio> getAllByGestor(Long id) {
		return this.premioRepository.getAllByGestor(id);
	}

	@Override
	public List<Premio> getAllByConsultor(Long id) {
		return this.premioRepository.getAllByConsultor(id);
	}

	@Override
	public Premio getLastAdded() {
		return this.premioRepository.getLastAdded();
	}
	
	public void gerarPremioByCampanha2020() {
		this.premioRepository.gerarPremioByCampanha2020();
	}
	
	@Override
	public boolean checkIfPremioExistsInProducao(Long id) {
		return this.premioRepository.checkIfPremioExistsInProducao(id);
	}
	
	@Override
	public boolean checkIfPremioExistsInContrato(Long id) {
		return this.premioRepository.checkIfPremioExistsInContrato(id);
	}

	public List<JSONObject> readExcel() {
		return this.premioRepository.readExcelFuncionarios();
		
	}

	public void calculaPremio2021(List<JSONObject> jsonObjects) {
		this.premioRepository.calculaPremio2021(jsonObjects);
		
	}

	public Premio getPremioTotalByConsultorAndCampanha(Long id, String campanha) {
		return this.premioRepository.getPremioTotalByConsultorAndCampanha(id,campanha);
	}



}
