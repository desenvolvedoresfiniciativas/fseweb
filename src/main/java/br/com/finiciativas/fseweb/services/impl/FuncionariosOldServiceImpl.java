package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.old.FuncionariosOld;
import br.com.finiciativas.fseweb.repositories.impl.FuncionariosOldRepositoryImpl;

@Service
public class FuncionariosOldServiceImpl {
	@Autowired
	private FuncionariosOldRepositoryImpl repository;

	public void create(FuncionariosOld funcionariosOld) {
		this.repository.create(funcionariosOld);
	}

	public void delete(FuncionariosOld funcionariosOld) {
		this.repository.delete(funcionariosOld);
	}

	public FuncionariosOld update(FuncionariosOld funcionariosOld) {
		return this.repository.update(funcionariosOld);
	}

	public List<FuncionariosOld> getAll() {
		return this.repository.getAll();
	}

	public FuncionariosOld findById(Long id) {
		return this.repository.findById(id);
	}

}
