package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Tarefa;

public interface ItemTarefaService {
	
	void create(ItemTarefa itemTarefa);
	
	List<ItemTarefa> getAll();
	
	List<ItemTarefa> getLista();
	
	ItemTarefa update(ItemTarefa itemTarefa);
	
	List<ItemTarefa> getItensByProducao(Producao producao);
	
	ItemTarefa findById(Long id);
	
	List<ItemTarefa> getItensByTarefa(Tarefa tarefa);
	
	List<ItemTarefa> getItemByTarefa(Long idTarefa);
	
	
	
}
