package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.old.NotaFiscalOld;
import br.com.finiciativas.fseweb.repositories.impl.NotaFiscalOldRepositoryImpl;

@Service
public class NotaFiscalOldServiceImpl {

	@Autowired
	private NotaFiscalOldRepositoryImpl repository;

	public void create(NotaFiscalOld notaFiscalOld) {
		this.repository.create(notaFiscalOld);
	}

	public void delete(NotaFiscalOld notaFiscalOld) {
		this.repository.delete(notaFiscalOld);
	}

	public NotaFiscalOld update(NotaFiscalOld notaFiscalOld) {
		return this.repository.update(notaFiscalOld);
	}

	public List<NotaFiscalOld> getAll() {
		return this.repository.getAll();
	}

	public NotaFiscalOld findById(Long id) {
		return this.repository.findById(id);
	}

}
