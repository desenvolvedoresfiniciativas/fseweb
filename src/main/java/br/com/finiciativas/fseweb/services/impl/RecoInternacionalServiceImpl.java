package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.IeM.RecoInternacional;
import br.com.finiciativas.fseweb.repositories.impl.RecoInternacionalRepositoryImpl;

@Service
public class RecoInternacionalServiceImpl {
	
	@Autowired
	private RecoInternacionalRepositoryImpl repository;


	public void create(RecoInternacional reco) {
		this.repository.create(reco);
	}

	public void delete(RecoInternacional reco) {
		this.repository.delete(reco);
	}


	public RecoInternacional update(RecoInternacional reco) {
		return this.repository.update(reco);
	}

	public List<RecoInternacional> getAll() {
		return this.repository.getAll();
	}

	public RecoInternacional findById(Long id) {
		return this.repository.findById(id);
	}

}
