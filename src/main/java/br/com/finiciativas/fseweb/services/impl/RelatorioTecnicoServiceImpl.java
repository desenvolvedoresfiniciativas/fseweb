package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;
import br.com.finiciativas.fseweb.repositories.impl.RelatorioTecnicoRepositoryImpl;

@Service
public class RelatorioTecnicoServiceImpl {

	@Autowired
	private RelatorioTecnicoRepositoryImpl repository;
	
	public void create(RelatorioTecnico relatorio) {
		this.repository.create(relatorio);
	}

	public void delete(RelatorioTecnico relatorio) {
		this.repository.delete(relatorio);
	}
	
	public void deleteOfProducao(Long id){
		this.repository.deleteOfProducao(id);
	}

	public RelatorioTecnico update(RelatorioTecnico relatorio) {
		return this.repository.update(relatorio);
	}

	public List<RelatorioTecnico> getAll() {
		return this.repository.getAll();
	}

	public RelatorioTecnico findById(Long id) {
		return this.repository.findById(id);
	}

	public List<RelatorioTecnico> getRelatorioTecnicoByProducao(Long id) {
		return this.repository.getRelatorioTecnicoByProducao(id);
	}
	
	public List<RelatorioTecnico> getRelatorioTecnicoByCriteria(String ano, SetorAtividade setorAtividade, Long idConsultor, Long idEquipe) {
		return this.repository.getRelatorioTecnicoByCriteria(ano, setorAtividade, idConsultor, idEquipe);
	}
}
