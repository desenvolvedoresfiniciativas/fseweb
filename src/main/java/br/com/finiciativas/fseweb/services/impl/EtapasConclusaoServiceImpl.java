package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.repositories.impl.EtapasConclusaoRepositoryImpl;

@Service
public class EtapasConclusaoServiceImpl {

	@Autowired
	private EtapasConclusaoRepositoryImpl repository;
	
	public List<EtapasConclusao> getAll() {
		return this.repository.getAll();
	}

	@Transactional
	public void create(EtapasConclusao EtapasConclusao) {
		this.repository.create(EtapasConclusao);
	}
	
	public void deleteOfProducao(Long id){
		repository.deleteOfProducao(id);
	}

	public void delete(EtapasConclusao EtapasConclusao) {
		this.repository.delete(EtapasConclusao);
	}

	@Transactional
	public EtapasConclusao update(EtapasConclusao EtapasConclusao) {
		return this.repository.update(EtapasConclusao);
	}

	public EtapasConclusao findById(Long id) {
		return this.repository.findById(id);
	}

	public EtapasConclusao getEtapasConclusaoByProducaoAndEtapa(Long idProd, Long idEtapa) {
		return this.repository.getEtapasConclusaoByProducaoAndEtapa(idProd, idEtapa);
	}
	
	public void createEtapasConclusaoByProd(Producao producao) {
		this.repository.createEtapasConclusaoByProd(producao);
	}
	
	public List<EtapasConclusao> getAllEtapasConclusaoByProducao(Long id) {
		return this.repository.getAllEtapasConclusaoByProducao(id);
	}
	
	public EtapasConclusao getEtapaRelatorioByProducao(Long id) {
		return this.repository.getEtapaRelatorioByProducao(id);
	}
	
	public EtapasConclusao getLastItemAdded() {
		return this.repository.getLastItemAdded();
	}
	
	public List<EtapasConclusao> getAllEtapasOfConsultorByAno(String ano, Long id) {
		return this.repository.getAllEtapasOfConsultorByAno(ano, id);
	}
	
	public List<EtapasConclusao> getAllEtapasOfEquipeByAno(String ano, Long id) {
		return this.repository.getAllEtapasOfEquipeByAno(ano, id);
	}
	
	public List<EtapasConclusao> getAllEtapasOfDiretorByAno(String ano) {
		return this.repository.getAllEtapasOfDiretorByAno(ano);
	}
	public void addEtapaInProductions() {
		 this.repository.addEtapaInProductions();
	}
	
	public List<EtapasConclusao> getAllEtapasByProduto(Long id, String ano){
		return this.repository.getAllEtapasByProduto(id, ano);
	}
	
	public List<EtapasConclusao> getAllEtapasByProdutoAndEquipe(Long id, Long idEquipe, String ano){
		return this.repository.getAllEtapasByProdutoAndEquipe(id, idEquipe, ano);
	}
	
	public List<EtapasConclusao> getAllEtapasByProdutoAndCriteria(Long idProduto, String ano, Long idFilial, Long idEquipe, Long idConsultor){
		return this.repository.getAllEtapasByProdutoAndCriteria(idProduto, ano, idFilial, idEquipe, idConsultor);
	}
	
	public EtapasConclusao getEtapasBalanceteProducao(Long id){
		return this.repository.getEtapasBalanceteProducao(id);
	}
	
	public EtapasConclusao getEtapasRelatorioProducao(Long id){
		return this.repository.getEtapasRelatorioProducao(id);
	}
	
	public List<EtapasConclusao> getAllEtapasByAno(String ano) {
		return this.repository.getAllEtapasByAno(ano);
	}

	public List<EtapasConclusao> getAllEtapasByCriteriaAcompanhamento(String campanha, String razaoSocial,
			String tipoNegocio, Long idProduto, Long idFilial, Long idEtapa) {
		return this.repository.getAllEtapasByCriteriaAcompanhamento(campanha,razaoSocial,tipoNegocio,idProduto,idFilial,idEtapa);
	}
}
