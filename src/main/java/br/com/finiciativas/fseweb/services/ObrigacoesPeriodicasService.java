package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ObrigacoesPeriodicas;
import br.com.finiciativas.fseweb.models.produto.ItemValores;

public interface ObrigacoesPeriodicasService {

	List<ObrigacoesPeriodicas> getAll();
	
	void create (ObrigacoesPeriodicas producao);
	
	void delete (ObrigacoesPeriodicas producao);
	
	ObrigacoesPeriodicas update (ObrigacoesPeriodicas producao);
	
	ObrigacoesPeriodicas findById(Long id);
	
	ObrigacoesPeriodicas getLastItemAdded();
	
	void addObrigacoesByFrequencia(ItemValores itemvalor);
	
	List<ObrigacoesPeriodicas> getObrigacoesByProducao(Long id);
	
	ItemValores getFNDCTbyProducaoId(Long id);	
	
	ItemValores getObrigacoesPeriodicasByProducao(Long id);

	}
