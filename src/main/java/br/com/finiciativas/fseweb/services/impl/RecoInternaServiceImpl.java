package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.IeM.RecoInterna;
import br.com.finiciativas.fseweb.repositories.impl.RecoInternaRepositoryImpl;

@Service
public class RecoInternaServiceImpl {
	
	@Autowired
	private RecoInternaRepositoryImpl repository;


	public void create(RecoInterna reco) {
		this.repository.create(reco);
	}

	public void delete(RecoInterna reco) {
		this.repository.delete(reco);
	}


	public RecoInterna update(RecoInterna reco) {
		return this.repository.update(reco);
	}

	public List<RecoInterna> getAll() {
		return this.repository.getAll();
	}

	public RecoInterna findById(Long id) {
		return this.repository.findById(id);
	}
	
}
