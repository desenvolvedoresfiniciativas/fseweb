package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.instituicao.Instituicao;

public interface InstituicaoService {
	
	void create(Instituicao instituicao);
	
	void delete(Long id);
	
	List<Instituicao> getAll();
	
	Instituicao update(Instituicao instituicao);
	
	Instituicao findById(Long id);
	
	void remove(Instituicao instituicao);
	
	Instituicao loadFullEmpresa(Long id);
	
}
