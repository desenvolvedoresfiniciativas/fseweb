package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.CategoriaConsultor;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.repositories.impl.ConsultorRepositoryImpl;
import br.com.finiciativas.fseweb.services.ConsultorService;

@Service
public class ConsultorServiceImpl implements UserDetailsService, ConsultorService {

	@Autowired
	ConsultorRepositoryImpl repository;

	public void create(Consultor consultor) {
		this.repository.create(consultor);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}

	public UserDetails loadUserByUsername(String username) {
		Consultor collab = this.repository.findByUsername(username);

		if (collab == null) {
			throw new UsernameNotFoundException(username);
		}

		return collab;
	}

	public List<Consultor> getAll() {
		return this.repository.getAll();
	}
	
	public List<Consultor> getAtivo() {
		return this.repository.getAtivo();
	}
	
	public Consultor getConsultorByUser(String username) {
		return this.repository.getConsultorByUser(username);
	}

	public boolean enviarEmailBoasVindas(Consultor consultor) {
		return this.repository.enviarEmailBoasVindas(consultor);
	}

	public Consultor findByUsername(String username) {
		return this.repository.findByUsername(username);
	}

	public Consultor update(Consultor consultor) {
		return this.repository.update(consultor);
	}
	
	public Consultor updateSenha(Consultor consultor) {
		return this.repository.updateSenha(consultor);
	}

	@Override
	public Consultor findById(Long id) {
		return this.repository.findById(id);
	}

	@Override
	public List<Consultor> getConsultorByCargo(Cargo cargo) {
		return this.repository.getConsultorByCargo(cargo);
	}
	
	@Override
	public List<Consultor> getLideres() {
		return this.repository.getLideres();
	}
	
	@Override
	public List<Consultor> getCoordenadores() {
		return this.repository.getCoordenadores();
	}
	
	@Override
	public List<Consultor> getGerentes() {
		return this.repository.getGerentes();
	}

	@Override
	public List<Consultor> getConsultoresSemEquipe() {
		return this.repository.getConsultoresSemEquipe();
	}
	
	public List<Consultor> getConsultores() {
		return this.repository.getConsultores();
	}
		
	public List<Consultor> getConsultorByCargoAll(Cargo cargo) {
		return this.repository.getConsultorByCargoAll(cargo);
	}
	
	public Consultor getConsultorBySigla() {
		return this.repository.getConsultorBySigla();
	}
	
	public List<Consultor> getCategoriaConsultor(CategoriaConsultor categoriaConsultor) {
		return this.repository.getCategoriaConsultor(categoriaConsultor);
	}
	
	public Consultor findBySigla(String sigla){
		return this.repository.findBySigla(sigla);
	}

	@Override
	public List<Consultor> getAllComercialAndConsultor() {
		return this.repository.getAllComercialAndConsultor();
	}
	

	@Override
	public List<Consultor> getAtivosAndInativos(){
		return this.repository.getAtivosAndInativos();
	}
	
	@Override
	public List<Consultor> getEquipeUtilizandoConsultorLogado(){
		return this.repository.getEquipeUtilizandoConsultorLogado();
	}
		
	public Consultor findUserByEmail(String email) {
		return this.repository.getUserByEmail(email);
	}
	
	public void createPasswordResetTokenForUser(Consultor consultor, String token) {
//	    PasswordResetToken myToken = new PasswordResetToken(token, consultor);
//	    passwordTokenRepository.save(myToken);

	}

	public Object getComerciais(Cargo consultorComercial, Cargo especialistaComercial, Cargo coordenadorComercial,
			Cargo supervisorAnalistaComercial, Cargo analistaComercial, Cargo coordenadorTecnico, Cargo liderTecnico,
			Cargo consultorLiderTecnico) {
		return this.repository.getComerciais(consultorComercial, especialistaComercial, supervisorAnalistaComercial, analistaComercial,coordenadorComercial, coordenadorTecnico, liderTecnico, consultorLiderTecnico);
	}
	
	public Object getAllComerciais(Cargo consultorComercial, Cargo especialistaComercial, Cargo coordenadorComercial,
			Cargo supervisorAnalistaComercial, Cargo analistaComercial, Cargo coordenadorTecnico, Cargo liderTecnico,
			Cargo consultorLiderTecnico) {
		return this.repository.getAllComerciais(consultorComercial, especialistaComercial, supervisorAnalistaComercial, analistaComercial,coordenadorComercial, coordenadorTecnico, liderTecnico, consultorLiderTecnico);
	}



	
}
