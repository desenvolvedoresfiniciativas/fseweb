package br.com.finiciativas.fseweb.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.Cidade;
import br.com.finiciativas.fseweb.repositories.impl.CidadeRepositoryImpl;
import br.com.finiciativas.fseweb.services.CidadeService;

@Service
public class CidadeServiceImpl implements CidadeService {
	
	@Autowired
	CidadeRepositoryImpl repository;

	@Override
	public Cidade findCidade(Cidade cidade) {
		return repository.findCidade(cidade);
	}

}
