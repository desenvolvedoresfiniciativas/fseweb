package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.repositories.impl.ComissaoRepositoryImpl;
import br.com.finiciativas.fseweb.services.ComissaoService;

@Service
public class ComissaoServiceImpl implements ComissaoService {

	@Autowired
	private ComissaoRepositoryImpl comissaoRepository;

	@Override
	public Comissao create(Comissao comissao) {
		return this.comissaoRepository.create(comissao);
	}

	@Override
	public Comissao update(Comissao comissao) {
		return this.comissaoRepository.update(comissao);
	}
	
	@Override
	public void delete(Comissao comissao) {
		this.comissaoRepository.delete(comissao);
	}

	@Override
	public Comissao findById(Long id) {
		return this.comissaoRepository.findById(id);
	}
	
	@Override
	public Comissao findByProducao(Long id) {
		return this.comissaoRepository.findByProducao(id);
	}

	@Override
	public List<Comissao> getAll() {
		return comissaoRepository.getAll();
	}
	
	@Override
	public List<Comissao> getAllByProducao(Long id) {
		return comissaoRepository.getAllByProducao(id);
	}

	@Override
	public List<Comissao> getComissaoByConsultor(Long id) {
		return this.comissaoRepository.getComissaoByConsultor(id);
	}

	@Override
	public List<Comissao> getComissaoByGestor(Long id) {
		return this.comissaoRepository.getComissaoByGestor(id);
	}

	@Override
	public List<Comissao> getComissaoByAnalista(Long id) {
		return this.comissaoRepository.getComissaoByAnalista(id);
	}

	@Override
	public Comissao getLastAdded() {
		return this.comissaoRepository.getLastAdded();
	}

	@Override
	public Comissao checkIfComissaoExistWithProducao(Long id) {
		return this.comissaoRepository.checkIfComissaoExistWithProducao(id);
	}
	
	@Override
	@Transactional
	public void gerarComissaoByAno(String ano) {
		this.comissaoRepository.calculaComissaoByAno(ano);
	}
	
	@Override
	public void gerarComissaoByConsultor(Long idConsultor) {
		this.comissaoRepository.gerarComissaoByConsultor(idConsultor);
	}
	
	@Override
	public void createComissaoByNota(NotaFiscal nota) {
		 this.comissaoRepository.createComissaoByNota(nota);
	}

	@Override
	public List<Comissao> getComissaoByFaturamento(Long id) {
		return this.comissaoRepository.getComissaoByFaturamento(id);
	}
	
	@Override
	public List<Comissao> getComissaoAtivaByConsultor(Long id) {
		return this.comissaoRepository.getComissaoAtivaByConsultor(id);
	}
	
	@Override
	public List<Comissao> getComissoesReaisByContrato(Long id) {
		return this.comissaoRepository.getComissoesReaisByContrato(id);
	}

	public void unlinkComissaoOfVNByProducao(Long id) {
		this.comissaoRepository.unlinkComissaoOfVNByProducao(id);
	}
	
	public void unlinkComissaoOfProducao(Long id) {
		this.comissaoRepository.unlinkComissaoOfProducao(id);
	}

	public List<Comissao> getComissoesByCriteria(String ano,Boolean pagamento, Boolean simulada, String trimestrePagamento, String anoPagamento, Long id, Long idProduto, String empresa, Long etapa) {
		// TODO Auto-generated method stub
		return this.comissaoRepository.getComissoesByCriteria(ano, pagamento, simulada, trimestrePagamento, anoPagamento, id, idProduto, empresa, etapa); 
	}

	public List<Comissao> getComissaoByProdAndProduto(Producao prod, Long id) {
		// TODO Auto-generated method stub
		return this.comissaoRepository.getComissaoByProdAndProduto(prod, id);
	}
	
	@Override
	public void deleteByContrato(Long idContrato) {
		this.comissaoRepository.deleteByContrato(idContrato);
	}
	
	@Override
	public void deleteByFaturamento(Long idFaturamento) {
		this.comissaoRepository.deleteByFaturamento(idFaturamento);
	}
	
}