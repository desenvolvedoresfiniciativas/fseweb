package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Acompanhamento;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;

public interface AcompanhamentoService {

	void create(Acompanhamento acompanhamento);

	void delete(Acompanhamento acompanhamento);
		
	BalanceteCalculo update(Acompanhamento acompanhamento);
	
	List<Acompanhamento> getAll();
	
	Acompanhamento findById(Long id);
	
	List<Acompanhamento> getAcompanhamentoByProducao(Long id);

}
