package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Projetos;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioSintetico;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;

public interface ProducaoService {

	List<Producao> getAll();
	
	void create (Producao producao);
	
	void delete (Producao producao);
	
	Producao update (Producao producao);
	
	Producao findById(Long id);
	
	List<Producao> getProducaoByContrato(Long id);

	void createItemValores(Producao producao);
	
	Producao getLastItemAdded();
	
	void addValor(Long id, List<ItemTarefa> itens, Producao producao);
	
	List<Projetos> getRelatorioAnalitico(String ano, SetorAtividade setorAtividade, Long feitoPorEconomico, 
			Long validadoPorEconomico, Long feitoPorTecnico, Long validadoPorTecnico, 
			Estado estadoEconomico, Estado estadoTecnico);
	
	List<Projetos> getRelatorioSintetico(String ano, SetorAtividade setor);
	
	void bindProjetosToRelatorio(List<Projetos> proj);
	
	List<RelatorioSintetico> getAllRelatorio();
	
	void criarExcelAnalitico(List<Projetos> projetos, String caminho);
	
	void criarExcelSintetico(List<RelatorioSintetico> relatorios, String caminho, String nomeArquivo);
	
	List<Producao> getProducaoByProduto(Long id);
	
	List<Producao> getProducaoByEquipe(Long id);
	
	List<Producao> getAllProducaoByConsultorAndAno(String ano, Long id);
	
	List<Producao> getAllProducaoByEquipeAndSituacao(Situacao situacao, Long id);
	
	List<Producao> getAllProducaoByEquipeAndAno(String ano, Long id);
	
	 List<Producao> getProducaoByDiretor();
	 
	 List<Producao> getAllProducaoByDiretorAndAno(String ano);
	 
	 List<Producao> getAllProducaoByDiretorAndSituacao(Situacao situacao);

}
