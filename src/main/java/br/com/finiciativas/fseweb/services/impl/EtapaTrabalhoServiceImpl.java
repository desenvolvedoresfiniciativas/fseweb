package br.com.finiciativas.fseweb.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.impl.EtapaTrabalhoRepositoryImpl;
import br.com.finiciativas.fseweb.services.EtapaTrabalhoService;

@Service
public class EtapaTrabalhoServiceImpl implements EtapaTrabalhoService {
	
	@Autowired
	private EtapaTrabalhoRepositoryImpl repository;
	
	@Autowired
	private ItemTarefaServiceImpl itemTarefaService;
	
	@Autowired
	private ProducaoServiceImpl producaoService;

	@Override
	public void create(EtapaTrabalho etapaTrabalho) {
		repository.create(etapaTrabalho);
	}

	@Override
	public List<EtapaTrabalho> getAll() {
		return repository.getAll();
	}
	
	@Override
	public Tarefa getTarefas(Long idEtapaTrabalho) {
		return this.repository.getTarefas(idEtapaTrabalho);
	}
	
	@Override
	public EtapaTrabalho findById(Long id){
		return this.repository.findById(id);
	}
	
	@Override
	public EtapaTrabalho update(EtapaTrabalho etapaTrabalho) {
		return this.repository.update(etapaTrabalho);
	}
	
	@Override
	public void bindEtapaToTarefas(EtapaTrabalho etapa){
		this.repository.bindEtapaToTarefas(etapa);
	}
	
	@Override
	public EtapaTrabalho getLastAdded(){
		return this.repository.getLastAdded();
	}
	
	public void createItemValoresSubProducao(EtapaTrabalho etapaTrabalho, SubProducao subProducao) {
		
		List<Tarefa> tarefas = etapaTrabalho.getTarefas();
		for (Tarefa tarefa : tarefas) {
			List<ItemTarefa> item = new ArrayList<ItemTarefa>();
			item = itemTarefaService.getItensByTarefa(tarefa);
			producaoService.addValorSubProducao(tarefa.getId(), item, subProducao);
		}
				
	}


}
