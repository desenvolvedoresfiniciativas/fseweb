package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Projetos;
import br.com.finiciativas.fseweb.repositories.impl.ProjetosRepositoryImpl;

@Service
public class ProjetosServiceImpl {

	@Autowired
	private ProjetosRepositoryImpl repository;
	
	public void create(Projetos projetos) {
		this.repository.create(projetos);
	}

	public void delete(Projetos projetos) {
		this.repository.delete(projetos);
	}

	public Projetos update(Projetos projetos) {
		return this.repository.update(projetos);
	}
	
	public List<Projetos> getAll() {
		return this.repository.getAll();
	}
	
	public Projetos findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<Projetos> getProjetosByProducao(Long id) {
		return this.repository.getProjetosByProducao(id);
	}
	
}
