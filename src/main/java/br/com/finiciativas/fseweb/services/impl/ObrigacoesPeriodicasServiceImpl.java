package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ObrigacoesPeriodicas;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.repositories.impl.ObrigacoesPeriodicasRepositoryImpl;

@Service
public class ObrigacoesPeriodicasServiceImpl {

	@Autowired
	private ObrigacoesPeriodicasRepositoryImpl repository;
	
	public List<ObrigacoesPeriodicas> getAll(){
		return this.repository.getAll();
	}

	public void create (ObrigacoesPeriodicas producao){
		this.repository.create(producao);
	}
	
	public void delete (ObrigacoesPeriodicas producao){
		this.repository.delete(producao);
	}

	public ObrigacoesPeriodicas update(ObrigacoesPeriodicas producao){
		return this.repository.update(producao);
	}
	
	public ObrigacoesPeriodicas findById(Long id){
		return this.repository.findById(id);
	}

	public ObrigacoesPeriodicas getLastItemAdded(){
		return this.repository.getLastItemAdded();
	}
	
	public void addObrigacoesByFrequencia(ItemValores itemvalor){
		this.repository.addObrigacoesByFrequencia(itemvalor);
	}
	
	public List<ObrigacoesPeriodicas> getObrigacoesByProducao(Long id){
		return this.repository.getObrigacoesByProducao(id);
	}
	
	public ItemValores getFNDCTbyProducaoId(Long id) {
		return this.repository.getFNDCTbyProducaoId(id);
	}
	
	public ItemValores getObrigacoesPeriodicasByProducao(Long id){
		return this.repository.getObrigacoesPeriodicasByProducao(id);
	}
}
