package br.com.finiciativas.fseweb.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.Versao;
import br.com.finiciativas.fseweb.repositories.impl.VersaoRepositoryImpl;

@Service
public class VersaoServiceImpl {
	
	@Autowired
	private VersaoRepositoryImpl repository;

	public Versao getVersaoAtual(){
		return this.repository.getVersaoAtual();
	}

	
}
