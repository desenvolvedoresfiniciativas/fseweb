package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ResultadoFinal;
import br.com.finiciativas.fseweb.repositories.impl.ResultadoFinalRepositoryImpl;

@Service
public class ResultadoFinalServiceImpl {

	@Autowired
	private ResultadoFinalRepositoryImpl repository;
	
	public void create(ResultadoFinal resultado) {
		this.repository.create(resultado);
	}

	public void delete(ResultadoFinal resultado) {
		this.repository.delete(resultado);
	}

	public ResultadoFinal update(ResultadoFinal resultado) {
		return this.repository.update(resultado);
	}

	public List<ResultadoFinal> getAll() {
		return this.repository.getAll();
	}

	public ResultadoFinal findById(Long id) {
		return this.repository.findById(id);
	}

	public List<ResultadoFinal> getResultadoFinalByAcompanhamento(Long id) {
		return this.repository.getResultadoFinalByAcompanhamento(id);
	}
	
}
