package br.com.finiciativas.fseweb.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.consultor.Impostos;
import br.com.finiciativas.fseweb.repositories.impl.ImpostosRepositoryImpl;
import br.com.finiciativas.fseweb.services.ImpostosService;

@Service
public class ImpostosServiceImpl implements ImpostosService{

	@Autowired
	private ImpostosRepositoryImpl impostosRepository;
	
	@Override
	public Impostos getImpostosByCampanha2020() {
		return this.impostosRepository.getImpostosByCampanha2020();
	}

	@Override
	public Impostos getImpostosbyId(Long id) {
		return this.impostosRepository.getImpostosbyId(id);
	}
}
