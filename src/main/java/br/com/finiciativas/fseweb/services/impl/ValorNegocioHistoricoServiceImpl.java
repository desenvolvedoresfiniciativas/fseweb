package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocioHistorico;
import br.com.finiciativas.fseweb.repositories.impl.ValorNegocioHistoricoRepositoryImpl;

@Service
public class ValorNegocioHistoricoServiceImpl {

	@Autowired
	private ValorNegocioHistoricoRepositoryImpl repository;
	
	public void create(ValorNegocioHistorico ValorNegocioHistorico) {
		this.repository.create(ValorNegocioHistorico);
	}

	public void delete(ValorNegocioHistorico ValorNegocioHistorico) {
		this.repository.delete(ValorNegocioHistorico);
	}

	public ValorNegocioHistorico update(ValorNegocioHistorico ValorNegocioHistorico) {
		return this.repository.update(ValorNegocioHistorico);
	}
	
	public List<ValorNegocioHistorico> getAll() {
		return this.repository.getAll();
	}
	
	public ValorNegocioHistorico findById(Long id) {
		return this.repository.findById(id);
	}
	
	public void captureValorNegocioHistorico(){
		this.repository.captureValorNegocioHistorico();
	}
	
}
