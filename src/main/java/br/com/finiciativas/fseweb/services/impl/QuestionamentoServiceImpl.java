package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Questionamentos;
import br.com.finiciativas.fseweb.repositories.impl.QuestionamentosRepositoryImpl;

@Service
public class QuestionamentoServiceImpl {


	@Autowired
	private QuestionamentosRepositoryImpl repository;
	
	public void create(Questionamentos questionamento) {
		this.repository.create(questionamento);
	}

	public void delete(Questionamentos questionamento) {
		this.repository.delete(questionamento);
	}

	public Questionamentos update(Questionamentos questionamento) {
		return this.repository.update(questionamento);
	}

	public List<Questionamentos> getAll() {
		return this.repository.getAll();
	}

	public Questionamentos findById(Long id) {
		return this.repository.findById(id);
	}

	public List<Questionamentos> getQuestionamentosByAcompanhamento(Long id) {
		return this.repository.getQuestionamentosByAcompanhamento(id);
	}
	
}
