package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.instituicao.Instituicao;
import br.com.finiciativas.fseweb.repositories.impl.InstituicaoRepositoryImpl;
import br.com.finiciativas.fseweb.services.InstituicaoService;

@Service
public class InstituicaoServiceImpl implements InstituicaoService {
	
	@Autowired
	private InstituicaoRepositoryImpl instituicaoRepository;

	@Override
	public void create(Instituicao instituicao) {
		this.instituicaoRepository.create(instituicao);
	}
	
	@Override
	public void delete(Long id) {
		this.instituicaoRepository.delete(id);
	}

	@Override
	public List<Instituicao> getAll() {
		return this.instituicaoRepository.getAll();
	}

	@Override
	public Instituicao update(Instituicao instituicao) {
		return this.instituicaoRepository.update(instituicao);
	}

	@Override
	public Instituicao findById(Long id) {
		return this.instituicaoRepository.findById(id);
	}

	@Override
	public void remove(Instituicao instituicao) {
		this.instituicaoRepository.remove(instituicao);
	}

	@Override
	public Instituicao loadFullEmpresa(Long id) {
		return this.instituicaoRepository.loadFullEmpresa(id);
	}
	
}
