package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.repositories.ContratoRepository;
import br.com.finiciativas.fseweb.repositories.impl.ContratoRepositoryImpl;
import br.com.finiciativas.fseweb.services.ContratoService;

@Service
public class ContratoServiceImpl implements ContratoService {
	
	@Autowired
	private ContratoRepository contratoRepository;
	
	@Autowired
	private ContratoRepositoryImpl contratoRepositoryImpl;

	@Override
	public Long create(Contrato contrato) {
		return this.contratoRepository.create(contrato);
	}

	@Override
	public Contrato update(Contrato contrato) {
		return this.contratoRepository.update(contrato);
	}
	
	public void updateHonorarios(Contrato contrato) {
		this.contratoRepository.updateHonorarios(contrato);
	}


	@Override
	public boolean remove(Long id) {
		return this.contratoRepository.remove(id);
	}

	@Override
	public List<Contrato> getAll() {
		return this.contratoRepository.getAll();
	}

	@Override
	public Contrato findById(Long id) {
		return this.contratoRepository.findById(id);
	}
	
	@Override
	public List<Contrato> getContratoByCliente(Long id){
		return this.contratoRepository.getContratoByCliente(id);
	}
	
	public Contrato getLastAdded(){
		return this.contratoRepository.getLastAdded();
	}
	
	public List<Contrato> getAllByComercial(Long id){
		return this.contratoRepository.getAllByComercial(id);
	}
	
	public List<Contrato> getContratoByCampanha(String dataInicio){
		return this.contratoRepository.getContratoByCampanha(dataInicio);
	}

	public List<Contrato> getProdutosPrincipais(Long id) {
		return this.contratoRepository.getProdutosPrincipais(id);
		
	}
	
	public List<Contrato> getListaProdutosAlvoByCliente(Long id) {
		return this.contratoRepository.getListaProdutosAlvoByCliente(id);
	}
	
	public List<Contrato> getRelatorioCombo(Long idProduto, Long comercialRespEntrada, Long comercialRespAtual,String empresa) {
		return this.contratoRepository.getRelatorioCombo(idProduto, comercialRespEntrada, comercialRespAtual,empresa);

	}
	
	public List<String> getListaNomeCombo(Long clienteId){
		return this.contratoRepository.getListaNomeCombo(clienteId);
	}

	public List<Contrato> getContratosByProdutoAndCliente(Long idProduto, String ano, Long idCliente) {
		return this.contratoRepository.getContratosByProdutoAndCliente(idProduto, ano,idCliente);
	}
	public List<Contrato> findByIdList(Long id){
		return this.contratoRepositoryImpl.findByIdList(id);
	}

}
