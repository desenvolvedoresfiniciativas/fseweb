package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.repositories.impl.FilialRepositoryImpl;
import br.com.finiciativas.fseweb.services.FilialService;

@Service
public class FilialServiceImpl implements FilialService {
	
	@Autowired
	private FilialRepositoryImpl repository;
	
	@Override
	public List<Filial> getAll() {
		return this.repository.getAll();
	}

	@Override
	public void create(Filial filial) {
		this.repository.create(filial);
	}

	@Override
	public Filial update(Filial filial) {
		return this.repository.update(filial);
	}

	@Override
	public Filial findById(Long id) {
		return this.repository.findById(id);
	}

	@Override
	public void remove(Long id) {
		this.repository.remove(id);
	}

	@Override
	public List<Consultor> getConsultores(Long idFilial) {
		return this.repository.getConsultores(idFilial);
	}
	
	public Filial findByNome(String nome){
		return this.repository.findByNome(nome);
	}

}
