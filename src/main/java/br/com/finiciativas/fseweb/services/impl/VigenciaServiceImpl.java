package br.com.finiciativas.fseweb.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.vigencia.Vigencia;
import br.com.finiciativas.fseweb.repositories.impl.VigenciaRepositoryImpl;
import br.com.finiciativas.fseweb.services.VigenciaService;

@Service
public class VigenciaServiceImpl implements VigenciaService {
	
	@Autowired
	private VigenciaRepositoryImpl repository;

	@Override
	public Vigencia update(Vigencia vigencia) {
		return this.repository.update(vigencia);
	}
	
	public void delete(Vigencia vigencia) {
		this.repository.delete(vigencia);
	}

}
