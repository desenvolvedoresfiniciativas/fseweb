package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.impl.ItemTarefaRepositoryImpl;
import br.com.finiciativas.fseweb.services.ItemTarefaService;

@Service
public class ItemTarefaServiceImpl implements ItemTarefaService {
	
	@Autowired
	private ItemTarefaRepositoryImpl itemTarefaRepository;

	@Override
	public void create(ItemTarefa itemTarefa) {
		itemTarefaRepository.create(itemTarefa);
	}

	@Override
	public List<ItemTarefa> getAll() {
		return itemTarefaRepository.getAll();
	}
	
	@Override
	public List<ItemTarefa> getLista() {
		return itemTarefaRepository.getAll();
	}

	@Override
	public ItemTarefa update(ItemTarefa itemTarefa) {
		return itemTarefaRepository.update(itemTarefa);
	}
	
	@Override
	public List<ItemTarefa> getItensByProducao(Producao producao){
		return itemTarefaRepository.getItensByProducao(producao);
	} 
	
	@Override
	public ItemTarefa findById(Long id){
		return itemTarefaRepository.findById(id);
	}
	
	@Override
	public List<ItemTarefa> getItensByTarefa(Tarefa tarefa){
		return itemTarefaRepository.getItensByTarefa(tarefa);
	}
	
	public List<ItemTarefa> getItemByTarefa(Long idTarefa){
		return itemTarefaRepository.getItemByTarefa(idTarefa);
	}
	
	
}
