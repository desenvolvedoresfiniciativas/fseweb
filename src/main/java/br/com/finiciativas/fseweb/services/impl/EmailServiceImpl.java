package br.com.finiciativas.fseweb.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.repositories.impl.EmailRepositoryImpl;
import br.com.finiciativas.fseweb.services.EmailService;

@Service
public class EmailServiceImpl implements EmailService {
	
	@Autowired
	private EmailRepositoryImpl emailRepository;

	public void send(String nome, String emailDestinatario, String assunto, String msg) {
		emailRepository.send(nome, emailDestinatario, assunto, msg);
	}

}
