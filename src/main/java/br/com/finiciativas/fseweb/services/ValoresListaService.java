package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.lista.ValoresLista;

public interface ValoresListaService {

List<ValoresLista> getAll();
	
	void create (ValoresLista valores);
	
	void delete (ValoresLista valores);
	
	ValoresLista update (ValoresLista valores);
	
	List<ValoresLista> findByItemId(Long id);
	
}
