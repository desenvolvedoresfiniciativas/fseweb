package br.com.finiciativas.fseweb.services;

public interface EmailService {
	
	void send(String nome, String emailDestinatario, String assunto, String msg);
	
}
