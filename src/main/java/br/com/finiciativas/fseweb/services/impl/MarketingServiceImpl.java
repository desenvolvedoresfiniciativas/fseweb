package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.IeM.Marketing;
import br.com.finiciativas.fseweb.repositories.impl.MarketingRepositoryImpl;

@Service
public class MarketingServiceImpl {
	
	@Autowired
	private MarketingRepositoryImpl repository;


	public void create(Marketing reco) {
		this.repository.create(reco);
	}

	public void delete(Marketing reco) {
		this.repository.delete(reco);
	}


	public Marketing update(Marketing reco) {
		return this.repository.update(reco);
	}

	public List<Marketing> getAll() {
		return this.repository.getAll();
	}

	public Marketing findById(Long id) {
		return this.repository.findById(id);
	}

}
