package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.repositories.impl.EquipeRepositoryImpl;
import br.com.finiciativas.fseweb.services.EquipeService;

@Service
public class EquipeServiceImpl implements EquipeService {

	@Autowired
	private EquipeRepositoryImpl repository;

	@Override
	public List<Equipe> getAll() {
		return this.repository.getAll();
	}

	@Override
	public void create(Equipe equipe) {
		this.repository.create(equipe);
	}

	@Override
	public Equipe update(Equipe equipe) {
		return this.repository.update(equipe);
	}

	@Override
	public Equipe findById(Long id) {
		return this.repository.findById(id);
	}

	@Override
	public void remove(Long id) {
		this.repository.remove(id);
	}

	@Override
	public List<Consultor> getConsultores(Long idEquipe) {
		return this.repository.getConsultores(idEquipe);
	}
	
	@Override
	public List<Equipe> getAllEquipesByCoordenador(Long idCoordenador) {
		return this.repository.getAllEquipesByCoordenador(idCoordenador);
	}
	
	@Override
	public List<Equipe> getAllEquipesByGerenteGeral(Long idGerenteGeral) {
		return this.repository.getAllEquipesByGerenteGeral(idGerenteGeral);
	}

	@Override
	public List<Consultor> getAllConsultores() {
		return this.repository.getAllConsultores();
	}

	public Equipe getConsultorByEquipe(Long id) {
		return this.repository.getConsultorByEquipe(id);
	}
	
	public Equipe findByNome(String nome){
		return this.repository.findByNome(nome);
	}
	
	public Equipe findByLider(Long idConsultor){
		return this.repository.findEquipeByLider(idConsultor);
	}

	@Override
	public Equipe findEquipeByConsultor(Long idConsultor) {
		return this.repository.findEquipeByConsultor(idConsultor);
	}
	
	public Equipe getEquipeByLider(Long idLider){
		return this.repository.getEquipeByLider(idLider);
	}

	public List<Equipe> getAllEquipesByFilial(Long id) {
		return this.repository.getAllEquipesByFilial(id);
	}

}
