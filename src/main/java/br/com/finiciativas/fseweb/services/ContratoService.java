package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.contrato.Contrato;

public interface ContratoService {

	Long create(Contrato contrato);

	Contrato update(Contrato contrato);

	boolean remove(Long id);

	List<Contrato> getAll();

	Contrato findById(Long id);

	List<Contrato> getContratoByCliente(Long id);

	Contrato getLastAdded();
	
	List<Contrato> getAllByComercial(Long id);

}
