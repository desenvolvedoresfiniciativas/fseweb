package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.lista.ValoresLista;
import br.com.finiciativas.fseweb.repositories.impl.ValoresListaRepositoryImpl;

@Service
public class ValoresListaServiceImpl {

	@Autowired
	private ValoresListaRepositoryImpl repository;
	
	public List<ValoresLista> getAll(){
		return this.repository.getAll();
	}

	public void create (ValoresLista lista){
		this.repository.create(lista);
	}
	
	public void delete (ValoresLista lista){
		this.repository.delete(lista);
	}
	
	public ValoresLista update (ValoresLista lista){
		return this.repository.update(lista);
	}
	
	public List<ValoresLista> findByItemId(Long id){
		return this.repository.findByItemId(id);
	}
	
}
