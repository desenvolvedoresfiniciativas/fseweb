package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.impl.TarefaRepositoryImpl;
import br.com.finiciativas.fseweb.services.TarefaService;

@Service
public class TarefaServiceImpl implements TarefaService {
	
	@Autowired
	private TarefaRepositoryImpl repository;
	
	@Override
	public void create(Tarefa tarefa) {
		repository.create(tarefa);
	}

	@Override
	public List<Tarefa> getAll() {
		return repository.getAll();
	}

	@Override
	public Tarefa findById(Long id) {
		return this.repository.findById(id);
	}
	
	@Override
	public Tarefa updateTarefaItens(Tarefa tarefa, Long produtoId, Long etapaId) {
		return this.repository.updateTarefaItens(tarefa, produtoId, etapaId);
	}
	
	@Override
	public Tarefa update(Tarefa tarefa) {
		return this.repository.update(tarefa);
	}

	@Override
	public Tarefa getLastItemAdded() {
		return this.repository.getLastItemAdded();
	}
	
	@Override
	public List<Tarefa> getTarefasByEtapa(EtapaTrabalho etapa) {
		return this.repository.getTarefasByEtapa(etapa);
	}
	
}
