package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.old.EmpresaOld;
import br.com.finiciativas.fseweb.repositories.impl.EmpresaOldRepositoryImpl;

@Service
public class EmpresaOldServiceImpl {

	@Autowired
	private EmpresaOldRepositoryImpl repository;

	public void create(EmpresaOld empresaOld) {
		this.repository.create(empresaOld);
	}

	public void delete(EmpresaOld empresaOld) {
		this.repository.delete(empresaOld);
	}

	public EmpresaOld update(EmpresaOld empresaOld) {
		return this.repository.update(empresaOld);
	}

	public List<EmpresaOld> getAll() {
		return this.repository.getAll();
	}

	public EmpresaOld findById(Long id) {
		return this.repository.findById(id);
	}

}
