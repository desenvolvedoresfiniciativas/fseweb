package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.repositories.impl.ClienteRepositoryImpl;
import br.com.finiciativas.fseweb.services.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	ClienteRepositoryImpl repository;

	@Override
	public Cliente create(Cliente cliente) {
		return repository.create(cliente);
	}

	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Override
	public Cliente update(Cliente cliente) {
		return repository.update(cliente);
	}

	@Override
	public Cliente findByCnpj(String cnpj) {
		return repository.findByCnpj(cnpj);
	}
	
	public Cliente findByRUT(String rut) {
		return repository.findByRUT(rut);
	}
	
	public Cliente findByNIT(String nit) {
		return repository.findByNIT(nit);
	}

	@Override
	public List<Cliente> getAll() {
		return repository.getAll();
	}

	@Override
	public Cliente findById(Long id) {
		return this.repository.findById(id);
	}

	@Override
	public Cliente loadFullEmpresa(Long id) {
		return this.repository.loadFullEmpresa(id);
	}
	
	public Cliente getLastItemAdded() {
		return this.repository.getLastItemAdded();
	}
	
	public Cliente updateCL(Cliente cliente) {
		return this.repository.updateCL(cliente);
	}
	
	public Cliente updateCO(Cliente cliente) {
		return this.repository.updateCO(cliente);
	}
	
	public List<Cliente> getFiltroProducao(String empresa) {
		return repository.getFiltroProducao(empresa);
	}
	
	public List<Cliente> getFiltroCliente(String empresa) {
		return repository.getFiltroCliente(empresa);
	}
	

}
