package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ResultadoFinal;

public interface ResultadoFinalService {

	void create(ResultadoFinal resultado);

	void delete(ResultadoFinal resultado);
		
	ResultadoFinal update(ResultadoFinal resultado);
	
	List<ResultadoFinal> getAll();
	
	ResultadoFinal findById(Long id);
	
	List<ResultadoFinal> getResultadoFinalByAcompanhamento(Long id);

	
}
