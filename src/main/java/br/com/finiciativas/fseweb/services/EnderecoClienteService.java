package br.com.finiciativas.fseweb.services;

import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;

public interface EnderecoClienteService {
	
	EnderecoCliente update(EnderecoCliente enderecoCliente);
	
	void create(EnderecoCliente enderecoCliente);
	
}
