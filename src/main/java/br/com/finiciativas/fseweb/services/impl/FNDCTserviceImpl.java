package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.FNDCT;
import br.com.finiciativas.fseweb.repositories.impl.FNDCTrepositoryImpl;

@Service
public class FNDCTserviceImpl {

	@Autowired
	private FNDCTrepositoryImpl repository;
	
	public List<FNDCT> getAll(){
		return this.repository.getAll();
	}

	public void create (FNDCT producao){
		this.repository.create(producao);
	}
	
	public void delete (FNDCT producao){
		this.repository.delete(producao);
	}

	public FNDCT update(FNDCT producao){
		return this.repository.update(producao);
	}
	
	public FNDCT findById(Long id){
		return this.repository.findById(id);
	}
	
	public FNDCT getFNDCTByProducao(Long id){
		return this.repository.getFNDCTByProducao(id);
	}
}
