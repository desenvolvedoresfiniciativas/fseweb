package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Documentacao;
import br.com.finiciativas.fseweb.repositories.impl.DocumentacaoRepositoryImpl;

@Service
public class DocumentacaoServiceImpl {
	
	@Autowired
	private DocumentacaoRepositoryImpl repository;
	
	public void create(Documentacao documentacao) {
		this.repository.create(documentacao);
	}

	public void delete(Documentacao documentacao) {
		this.repository.delete(documentacao);
	}

	public Documentacao update(Documentacao documentacao) {
		return this.repository.update(documentacao);
	}

	public List<Documentacao> getAll() {
		return this.repository.getAll();
	}

	public Documentacao findById(Long id) {
		return this.repository.findById(id);
	}

	public List<Documentacao> getDocumentacaoByAcompanhamento(Long id) {
		return this.repository.getDocumentacaoByAcompanhamento(id);
	}

}
