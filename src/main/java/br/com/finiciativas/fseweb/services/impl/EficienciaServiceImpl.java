package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.Eficiencia;
import br.com.finiciativas.fseweb.repositories.impl.EficienciaRepositoryImpl;

@Service
public class EficienciaServiceImpl {

	@Autowired
	private EficienciaRepositoryImpl repository;
	
	public void create(Eficiencia Eficiencia) {
		this.repository.create(Eficiencia);
	}

	public void delete(Eficiencia Eficiencia) {
		this.repository.delete(Eficiencia);
	}

	public Eficiencia update(Eficiencia Eficiencia) {
		return this.repository.update(Eficiencia);
	}
	
	public List<Eficiencia> getAll() {
		return this.repository.getAll();
	}
	
	public Eficiencia findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<Eficiencia> getEficienciaByProducao(Long id) {
		return this.repository.getEficienciaByProducao(id);
	}
	
	public List<Eficiencia> getEficienciaByCriteria(String ano, SetorAtividade setor, Long idFilial, Long idEquipe,	Long idConsultor, String razaoSocial,Situacao situacao) { 
		return this.repository.getEficienciaByCriteria(ano, setor, idFilial, idEquipe, idConsultor,razaoSocial,situacao);
	}
	
	
	public void deleteOfProducao(Long id){
		this.repository.deleteOfProducao(id);
	}
	
	public List<Eficiencia> getEficienciaByProdAndEtapa(Long idProducao) { 
		return this.repository.getEficienciaByProdAndEtapa(idProducao);
	}
}
