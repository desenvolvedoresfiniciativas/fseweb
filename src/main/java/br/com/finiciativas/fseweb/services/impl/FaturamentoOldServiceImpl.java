package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.old.FaturamentoOld;
import br.com.finiciativas.fseweb.repositories.impl.FaturamentoOldRepositoryImpl;

@Service
public class FaturamentoOldServiceImpl {

	@Autowired
	private FaturamentoOldRepositoryImpl repository;

	public void create(FaturamentoOld faturamentoOld) {
		this.repository.create(faturamentoOld);
	}

	public void delete(FaturamentoOld faturamentoOld) {
		this.repository.delete(faturamentoOld);
	}

	public FaturamentoOld update(FaturamentoOld faturamentoOld) {
		return this.repository.update(faturamentoOld);
	}

	public List<FaturamentoOld> getAll() {
		return this.repository.getAll();
	}

	public FaturamentoOld findById(Long id) {
		return this.repository.findById(id);
	}

}
