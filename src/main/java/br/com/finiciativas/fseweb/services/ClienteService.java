package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.cliente.Cliente;

public interface ClienteService {
	
	Cliente create(Cliente cliente);
	
	void delete(Long id);
	
	Cliente update(Cliente cliente);
	
	Cliente findByCnpj(String cnpj);
	
	Cliente findById(Long id);
	
	Cliente loadFullEmpresa(Long id);
	
	List<Cliente> getAll();

	List<Cliente> getFiltroCliente(String empresa);
	
}
