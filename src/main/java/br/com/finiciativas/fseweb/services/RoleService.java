package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.consultor.Role;

public interface RoleService {
	
	List<Role> getAll();
	
}
