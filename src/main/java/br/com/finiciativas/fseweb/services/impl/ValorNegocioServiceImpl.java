package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.repositories.impl.ValorNegocioRepositoryImpl;

@Service
public class ValorNegocioServiceImpl {

	@Autowired
	private ValorNegocioRepositoryImpl repository;

	public List<ValorNegocio> getAll() {
		return this.repository.getAll();
	}

	public void create(ValorNegocio vn) {
		this.repository.create(vn);
	}

	public void delete(ValorNegocio vn) {
		this.repository.delete(vn);
	}
	
	public void deleteOfProducao(Long id){
		this.repository.deleteOfProducao(id);
	}

	public void update(ValorNegocio vn) {
		 this.repository.update(vn);
	}

//	public void geraValorDeNegocio() {
//		this.repository.geraValorDeNegocio();
//	}
	
	public void geraValorDeNegocioByAno(String ano) {
		this.repository.geraValorDeNegocioByAno();
	}
	
	public List<ValorNegocio> getVNbyAno(String ano){
		return this.repository.getVNbyAno(ano);
	}
	
	public List<ValorNegocio> getAllAtivasbyAno(String ano){
		return this.repository.getAllAtivasbyAno(ano);
	}
	
	public List<ValorNegocio> getVNbyEmpresa(Long id) {
		return this.repository.getVNbyEmpresa(id);
	}
	
	public ValorNegocio getVNbyProd(Long id) {
		return this.repository.getVNbyProd(id);
	}
	public ValorNegocio getVNbyEmpresaAndProducao(Long idEmpresa, Long idProducao) {
		return this.repository.getVNbyEmpresaAndProducao(idEmpresa, idProducao);
	}
	
	public ValorNegocio findById(Long id) {
		return this.repository.findById(id);
	}
	
	public ValorNegocio getVNbyProducao(Long id) {
		return this.repository.getVNbyProducao(id);
	}

	public List<ValorNegocio> getAllByConsultor(Long idConsultor) {
		return this.repository.getAllByConsultor(idConsultor);
	}
	
	public List<ValorNegocio> getVNByEquipe(String nome) {
		return this.repository.getVNByEquipe(nome);
	}

	public List<ValorNegocio> getAllByConsultorAndNN2020(Long id, String campanha) {
		return this.repository.getAllByConsultorAndNN2020(id, campanha);
	}
	public List<ValorNegocio> getAllByConsultor2020Finalizado(Long id, String campanha) {
		return this.repository.getAllByConsultor2020Finalizado(id, campanha);
	}
	
	public List<ValorNegocio> getGerenciarValorNegocio(String cnpj, String nome_empresa) {
		return this.repository.getGerenciarValorNegocio(cnpj, nome_empresa);
	}
	
	public void geraValorDeNegocioLIByAno(String ano) {
		this.repository.geraValorDeNegocioLIByAno(ano);
	}
	//public void geraValorDeNegocioROTA2030ByAno(String ano) {
		//this.repository.geraValorDeNegocioROTA2030ByAno(ano);
	//}
	

}
