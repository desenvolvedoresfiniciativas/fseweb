package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.AuditoriaProjetos;
import br.com.finiciativas.fseweb.repositories.impl.AuditoriaProjetosRepositoryImpl;

@Service
public class AuditoriaProjetosServiceImpl {

	@Autowired
	private AuditoriaProjetosRepositoryImpl repository;
	
	public void create(AuditoriaProjetos AuditoriaProjetos) {
		this.repository.create(AuditoriaProjetos);
	}

	public void delete(AuditoriaProjetos AuditoriaProjetos) {
		this.repository.delete(AuditoriaProjetos);
	}

	public AuditoriaProjetos update(AuditoriaProjetos AuditoriaProjetos) {
		return this.repository.update(AuditoriaProjetos);
	}
	
	public List<AuditoriaProjetos> getAll() {
		return this.repository.getAll();
	}
	
	public AuditoriaProjetos findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<AuditoriaProjetos> getAuditoriaProjetosByProducao(Long id) {
		return this.repository.getAuditoriaProjetosByProducao(id);
	}
	
}
