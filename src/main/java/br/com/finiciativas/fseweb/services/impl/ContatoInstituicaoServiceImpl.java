package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.instituicao.ContatoInstituicao;
import br.com.finiciativas.fseweb.repositories.impl.ContatoInstituicaoRepositoryImpl;
import br.com.finiciativas.fseweb.services.ContatoInstituicaoService;

@Service
public class ContatoInstituicaoServiceImpl implements ContatoInstituicaoService {

	@Autowired
	private ContatoInstituicaoRepositoryImpl repositoryContatoEmpresa;

	@Override
	public List<ContatoInstituicao> getContatosInstituicao(Long id) {
		return repositoryContatoEmpresa.getContatosInstituicao(id);
	}

	@Override
	public void create(ContatoInstituicao contatoInstituicao, Long id) { 
		repositoryContatoEmpresa.create(contatoInstituicao, id);
	}

	@Override
	public void update(ContatoInstituicao contatoEmpresa) {
		repositoryContatoEmpresa.update(contatoEmpresa);
	}

	@Override
	public void remove(ContatoInstituicao contatoEmpresa) {
		repositoryContatoEmpresa.remove(contatoEmpresa);
	}

	@Override
	public ContatoInstituicao findById(Long id) {
		return repositoryContatoEmpresa.findById(id);
	}

	@Override
	public Long getIdInstituicaoOfContact(Long id) {
		return repositoryContatoEmpresa.getIdInstituicaoOfContact(id);
	}

	
}
