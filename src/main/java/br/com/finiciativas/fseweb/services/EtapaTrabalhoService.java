package br.com.finiciativas.fseweb.services;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Tarefa;

@Service
public interface EtapaTrabalhoService {
	
	void create(EtapaTrabalho etapaTrabalho);
	
	List<EtapaTrabalho> getAll();

	Tarefa getTarefas(Long idEtapaTrabalho);

	EtapaTrabalho findById(Long id);

	EtapaTrabalho update(EtapaTrabalho etapaTrabalho);
	
	void bindEtapaToTarefas(EtapaTrabalho etapa);
	
	public EtapaTrabalho getLastAdded();
}
