package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.IeM.Evento;
import br.com.finiciativas.fseweb.repositories.impl.EventoRepositoryImpl;

@Service
public class EventoServiceImpl {
	
	@Autowired
	private EventoRepositoryImpl repository;


	public void create(Evento reco) {
		this.repository.create(reco);
	}

	public void delete(Evento reco) {
		this.repository.delete(reco);
	}


	public Evento update(Evento reco) {
		return this.repository.update(reco);
	}

	public List<Evento> getAll() {
		return this.repository.getAll();
	}

	public Evento findById(Long id) {
		return this.repository.findById(id);
	}

}
