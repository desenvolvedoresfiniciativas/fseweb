package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.honorario.Honorario;

public interface HonorarioService {
	
	Honorario update(Honorario honorario);
	
	List<Honorario> getAll();
	
}
