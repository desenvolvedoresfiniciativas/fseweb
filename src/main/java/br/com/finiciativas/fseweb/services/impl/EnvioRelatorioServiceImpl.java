package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.EnvioRelatorio;
import br.com.finiciativas.fseweb.repositories.impl.EnvioRelatorioRepositoryImpl;

@Service
public class EnvioRelatorioServiceImpl {

	@Autowired
	private EnvioRelatorioRepositoryImpl repository;
	
	public void create(EnvioRelatorio envio) {
		this.repository.create(envio);
	}

	public void delete(EnvioRelatorio envio) {
		this.repository.delete(envio);
	}

	public EnvioRelatorio update(EnvioRelatorio envio) {
		return this.repository.update(envio);
	}

	public List<EnvioRelatorio> getAll() {
		return this.repository.getAll();
	}

	public EnvioRelatorio findById(Long id) {
		return this.repository.findById(id);
	}

	public List<EnvioRelatorio> getEnvioRelatorioByAcompanhamento(Long id) {
		return this.repository.getEnvioRelatorioByAcompanhamento(id);
	}
	
	
}
