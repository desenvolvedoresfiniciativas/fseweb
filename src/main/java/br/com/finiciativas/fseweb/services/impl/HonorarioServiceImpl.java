package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.honorario.FaixasEscalonado;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.repositories.impl.HonorarioRepositoryImpl;
import br.com.finiciativas.fseweb.services.HonorarioService;

@Service
public class HonorarioServiceImpl implements HonorarioService {

	@Autowired
	private HonorarioRepositoryImpl repository;

	@Override
	public Honorario update(Honorario honorario) {
		return this.repository.update(honorario);
	}

	@Override
	public List<Honorario> getAll() {
		return this.repository.getAll();
	}

	public ValorFixo getValorFixoByContrato(Long id) {
		return this.repository.getValorFixoByContrato(id);
	}
	
	public PercentualFixo getPercentualFixoByContrato(Long id) {
		return this.repository.getPercentualFixoByContrato(id);
	}

	public PercentualEscalonado getPercentualEscalonadoByContrato(Long id) {
		return this.repository.getPercentualEscalonadoByContrato(id);
	}
	
	public void delete(Honorario honorario) {
		this.repository.delete(honorario);
	}
	
	public Honorario getHonorarioByNomeAndContrato(Long id, String nome) {
		return this.repository.getHonorarioByNomeAndContrato(id, nome);
	}
	
	public FaixasEscalonado getMaxFaixa() {
		return this.repository.getMaxFaixa();
	}
	public List<Honorario> getListHonorarioByContrato(Long id) {
		return this.repository.getListHonorarioByContrato(id);
	}
	public PercentualEscalonado getPercentualEscalonadoById(Long id) {
		return this.repository.getPercentualEscalonadoById(id);
	}
	public PercentualFixo getPercentualFixoById(Long id) {
		return this.repository.getPercentualFixoById(id);
	}

}
