package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoCalculo;
import br.com.finiciativas.fseweb.repositories.impl.InformacaoCalculoRepositoryImpl;

@Service
public class InformacaoCalculoServiceImpl {

	@Autowired
	private InformacaoCalculoRepositoryImpl repository;
	
	public void create(InformacaoCalculo informacao) {
		this.repository.create(informacao);
	}

	public void delete(InformacaoCalculo informacao) {
		this.repository.delete(informacao);
	}

	public InformacaoCalculo update(InformacaoCalculo informacao) {
		return this.repository.update(informacao);
	}

	public List<InformacaoCalculo> getAll() {
		return this.repository.getAll();
	}

	public InformacaoCalculo findById(Long id) {
		return this.repository.findById(id);
	}

	public List<InformacaoCalculo> getInformacaoCalculoByAcompanhamento(Long id) {
		return this.repository.getInformacaoCalculoByAcompanhamento(id);
	}
	
	
}
