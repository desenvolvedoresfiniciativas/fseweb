package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.repositories.impl.ContatoClienteRepositoryImpl;
import br.com.finiciativas.fseweb.services.ContatoClienteService;

@Service
public class ContatoClienteServiceImpl implements ContatoClienteService {

	@Autowired
	private ContatoClienteRepositoryImpl repositoryContatoEmpresa;

	@Override
	public List<ContatoCliente> getContatosCliente(Long id) {
		return repositoryContatoEmpresa.getContatosCliente(id);
	}

	@Override
	public void create(ContatoCliente contatoEmpresa, Long id) { 
		repositoryContatoEmpresa.create(contatoEmpresa, id);
	}

	@Override
	public void update(ContatoCliente contatoEmpresa) {
		repositoryContatoEmpresa.update(contatoEmpresa);
	}

	@Override
	public void remove(ContatoCliente contatoEmpresa) {
		repositoryContatoEmpresa.remove(contatoEmpresa);
	}

	@Override
	public ContatoCliente findById(Long id) {
		return repositoryContatoEmpresa.findById(id);
	}

	@Override
	public Long getIdClienteOfContact(Long id) {
		return repositoryContatoEmpresa.getIdClienteOfContact(id);
	}
	
	public List<ContatoCliente> getAllById(Long id){
		return repositoryContatoEmpresa.getAllById(id);
	}
	
	public void delete(ContatoCliente cont) {
		repositoryContatoEmpresa.delete(cont);
	}
	
	

}
