package br.com.finiciativas.fseweb.services.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.EstadoEtapa;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.AcompanhamentoExTarifario;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.repositories.impl.FaturamentoRepositoryImpl;

@Service
public class FaturamentoServiceImpl {

	@Autowired
	private FaturamentoRepositoryImpl repository;

	@Transactional
	public void create(Faturamento Faturamento) {
		repository.create(Faturamento);
	}
	
	public void unlinkFaturamentoOfProducao(Long id){
		repository.unlinkFaturamentoOfProducao(id);
	}

	public void remove(Long id) {
		repository.remove(id);
	}

	public List<Faturamento> getAll() {
		return repository.getAll();
	}

	public Faturamento findById(Long id) {
		return this.repository.findById(id);
	}

	public Faturamento update(Faturamento Faturamento) {
		return this.repository.update(Faturamento);
	}

	public void sendEmail(Faturamento faturamento) {
		this.repository.sendEmail(faturamento);
	}

	public void sendEmailSemContrato(Faturamento faturamento) {
		this.repository.sendEmailSemContrato(faturamento);
	}

	public Faturamento geraFaturamento(BalanceteCalculo balanceteObj, float percent, EtapaTrabalho etapa,
			EtapasConclusao etapas) {
		System.out.println("ENTROU NA GERACAO");
		return this.repository.geraFaturamento(balanceteObj, percent, etapa, etapas);
	}

	public List<Faturamento> getFatNaoLiberado() {
		return this.repository.getFatNaoLiberado();
	}

	public List<Faturamento> getFatLiberado() {
		return this.repository.getFatLiberado();
	}

	public List<Faturamento> getFatNaoLiberadoByCriteria(String razaoSocial, String ano, Long idProduto,
			EstadoEtapa estado, String tpNegocio, Long etapaId, String anoFiscal) {
		return this.repository.getFatNaoLiberadoByCriteria(razaoSocial, ano, idProduto, estado, tpNegocio, etapaId, anoFiscal);
	}

	public void geraTxtFaturamento(List<Faturamento> fat) {
		this.repository.geraTxtFaturamento(fat);
	}

	public List<Faturamento> getAllByProducao(Long id) {
		return this.repository.getAllByProducao(id);
	}
	
	public List<Faturamento> getAllByContrato(Long id) {
		return this.repository.getAllByContrato(id);
	}

	public List<Faturamento> getGestao() {
		return this.repository.getGestao();
	}

	public List<Faturamento> getContrato(Long idContrato) {
		return this.repository.getContrato(idContrato);
	}

	public List<Faturamento> getFaturamentoByProducaoEtapaContrato(Long producao, Long etapa, Long contrato) {
		return this.repository.getFaturamentoByProducaoEtapaContrato(producao, etapa, contrato);
	}
	
	public List<Faturamento> getFaturamentosNaoConsolidadosByProducaoAndEtapa(Long producaoId, Long etapa){
		return this.repository.getFaturamentosNaoConsolidadosByProducaoAndEtapa(producaoId, etapa);
	}

	public Faturamento getFaturamentoByProducaoEtapaContrato2(Long producao, Long etapa, Long contrato) {
		return this.repository.getFaturamentoByProducaoEtapaContrato2(producao, etapa, contrato);
	}

	public Faturamento getFaturamentoByProd(Long idProducao) {
		return this.repository.getFaturamentoByProd(idProducao);
	}

	public Faturamento getFaturamentoByProdOrContrato(Long idProducao, Long idContrato) {
		return this.repository.getFaturamentoByProdOrContrato(idProducao, idContrato);
	}

	public List<Faturamento> getFaturamentoByContrato(Long contrato) {
		return this.repository.getFaturamentoByContrato(contrato);
	}

	public void envioDeCobranca(Faturamento fat) {
		this.repository.envioDeCobranca(fat);
	}

	public void aplicaLimitacao(Long id) {
		this.repository.aplicaLimitacao(id);
	}

	public Faturamento geraFaturamentoLI(Producao producao, float percent, EtapaTrabalho etapa, EtapasConclusao etapas) {
		return this.repository.geraFaturamentoLI(producao, percent, etapa, etapas);
	}

	public List<Faturamento> getAllFaturamentosByCriteria(String campanha, Long idProduto, Long idFilial,
			String empresa, String tipoDeNegocio, Long etapa) {
		System.out
				.println("---------------------------- CAMPANHA dentro do service  " + idProduto + " ---------------");
		return this.repository.getAllFaturamentosByCriteria(campanha, idProduto, idFilial, empresa, tipoDeNegocio,
				etapa);
	}

	public List<Faturamento> getFaturamentosByCriteria(String ano, Long idProduto, Long etapa, Long idFilial,
			String empresa, String tipoNegocio, String mesSelecionado) {
		return this.repository.getFaturamentosByCriteria(ano, idProduto, etapa, idFilial, empresa, tipoNegocio, mesSelecionado);
	}

	public List<Faturamento> getFaturamentosNaoEmitidos() {
		return this.repository.getFaturamentosNaoEmitidos();
	}

	public Faturamento geraFaturamentoExTarifario(String trigger, Producao producao, SubProducao subProducao, AcompanhamentoExTarifario acompanhamento) {
		return this.repository.geraFaturamentoExTarifario(trigger, producao, subProducao, acompanhamento);
	}

	public List<Faturamento> getFaturamentosEmitidos() {
		return this.repository.getFaturamentosEmitidos();
	}

	public List<Faturamento> getFaturamentoByCliente(Long id) {
		return this.repository.getFaturamentoByCliente(id);
	}

	public List<Faturamento> getFaturamentoByProdAndCampaign(Long id, String campanha) {
		return this.repository.getFaturamentoByProdAndCampaign(id, campanha);
	}

	public Faturamento geraFaturamentoFinanciamento(String trigger, Producao producao, SubProducao subProducao) {
		return this.repository.geraFaturamentoFinanciamento(trigger, producao, subProducao);
	}
	
	public void envioFaturamentoLiberado(Faturamento fat) {
		this.repository.envioFaturamentoLiberado(fat);
	}
	
	public List<Faturamento> getAllFaturamentoByClienteAndCampanha(Long id, String ano) {
		return this.repository.getAllFaturamentoByClienteAndCampanha(id, ano);
	}
	
	public Faturamento geraFaturamentoConsolidado(Producao producao, BigDecimal beneficioConsolidado, Float percent, Long idEtapaFaturar) {
		return this.repository.geraFaturamentoConsolidado(producao, beneficioConsolidado, percent, idEtapaFaturar);
	}
	
	public List<Faturamento> getAllByProducaoNotConsolidado(Long id) {
		return this.repository.getAllByProducaoNotConsolidado(id);
	}
	
	public Faturamento geraConsolidacaoByFat(Producao producao) {
		return this.repository.geraConsolidacaoByFat(producao);
	}
	

	public List<Faturamento> getFaturamentoByBalancete(Long idBalancete) {
		return this.repository.getFaturamentoByBalancete(idBalancete);
	}
	
	public void deleteByContrato(Long idContrato){
		this.repository.deleteByContrato(idContrato);
	}
	
	public List<Faturamento> pegaDatasFormatadas() {
		return this.repository.pegaDatasFormatadas();
	}

	public void salvaDatasFormatadas(Long idFaturamento, String data) {
		this.repository.salvaDatasFormatadas(idFaturamento, data);
	}
}
