package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;

public interface RelatorioTecnicoService {

	void create(RelatorioTecnico balancete);

	void delete(RelatorioTecnico balancete);
		
	RelatorioTecnico update(RelatorioTecnico balancete);
	
	List<RelatorioTecnico> getAll();
	
	RelatorioTecnico findById(Long id);
	
	List<RelatorioTecnico> getRelatorioTecnicoByProducao(Long id);
	
}
