package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.impl.ProdutoRepositoryImpl;
import br.com.finiciativas.fseweb.services.ProdutoService;

@Service
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepositoryImpl repository;

	public List<Produto> getAll() {
		return this.repository.getAll();
	}

	public void create(Produto produto) {
		this.repository.create(produto);
	}

	public void delete(Produto produto) {
		this.repository.delete(produto);
	}

	public Produto update(Produto produto) {
		return this.repository.update(produto);
	}

	public Produto findById(Long id) {
		return this.repository.findById(id);
	}

	@Override
	public List<EtapaTrabalho> getEtapasTrabalho(Long idProduto) {
		return this.repository.getEtapasTrabalho(idProduto);
	}
	
	@Override
	public List<Tarefa> getTarefas(Long idEtapa) {
		return this.repository.getTarefas(idEtapa);
	}
	
}
