package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.repositories.impl.SubProducaoRepositoryImpl;

@Service
public class SubProducaoServiceImpl {

	@Autowired
	private SubProducaoRepositoryImpl repositoryImpl;
	
	public void create (SubProducao subProducao) {
		this.repositoryImpl.create(subProducao);
	}
	
//	public void deleteOfProducao(Long id){
//		this.repositoryImpl.deleteOfProducao(id);
//	}
	
	public void deleteSubProdOfProducao(Long id){
		repositoryImpl.deleteSubProdOfProducao(id);
	}
	
	public void delete(SubProducao subProducao) {
		this.repositoryImpl.delete(subProducao);
	}
	
	public SubProducao update(SubProducao subProducao) {
		return this.repositoryImpl.update(subProducao);
	}
	
	public List<SubProducao> getAll(){
		return this.repositoryImpl.getAll();
	}
	
	public SubProducao findById(Long id) {
		return this.repositoryImpl.findById(id);
	}
	
	public List<SubProducao> getSubProducaobyProducaoAndEtapa(Long idProd, Long idEtapa){
		return this.repositoryImpl.getSubProducaobyProducaoAndEtapa(idProd,idEtapa);
	}
	
	public SubProducao getLastAddedByProducao(Long id){
		return this.repositoryImpl.getLastAddedByProducao(id);
	}
	
	public SubProducao getFiscalizacaoByDescricaoAndAno(String descricao, String ano){
		return this.repositoryImpl.getFiscalizacaoByDescricaoAndAno(descricao, ano);
	}
	
	public SubProducao getSubProducaoByProducaoDescricaoAndEtapa(Long idProd, String descricao, Long idEtapa){
		return this.repositoryImpl.getSubProducaoByProducaoDescricaoAndEtapa(idProd, descricao, idEtapa);
	}
	public List<SubProducao> getSubProducaoByProducao(Long id){
		return this.repositoryImpl.getSubProducaoByProducao(id);
	}
	
	public void deleteSubProducao(Long id){
		this.repositoryImpl.deleteSubProducao(id);
	}
	public List<SubProducao> getAllOrderProd(){
		return this.repositoryImpl.getAllOrderProd();
	}
}
