package br.com.finiciativas.fseweb.services;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.CategoriaConsultor;
import br.com.finiciativas.fseweb.models.consultor.Consultor;


public interface ConsultorService {
	
	void create(Consultor consultor);
	
	void delete(Long id);
	
	Consultor findByUsername(String username);
	
	Consultor findById(Long id);
	
	Iterable<Consultor> getAll();
	
	List<Consultor> getAllComercialAndConsultor();
	
	UserDetails loadUserByUsername(String username);
	
	boolean enviarEmailBoasVindas(Consultor consultor);
	
	Consultor update(Consultor consultor);
	
	List<Consultor> getConsultorByCargo(Cargo cargo);
	
	List<Consultor> getConsultoresSemEquipe();
	
	List<Consultor> getConsultores();
	
	Consultor getConsultorBySigla();
	
	List<Consultor> getCategoriaConsultor(CategoriaConsultor categoriaConsultor);

	List<Consultor> getLideres();

	List<Consultor> getCoordenadores();

	List<Consultor> getGerentes();
	
	List<Consultor> getAtivosAndInativos();

	List<Consultor> getEquipeUtilizandoConsultorLogado();
	 
}
