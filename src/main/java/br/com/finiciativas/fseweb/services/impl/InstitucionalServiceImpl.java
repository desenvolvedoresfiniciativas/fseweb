package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.IeM.Institucional;
import br.com.finiciativas.fseweb.repositories.impl.InstitucionalRepositoryImpl;

@Service
public class InstitucionalServiceImpl {
	
	@Autowired
	private InstitucionalRepositoryImpl repository;


	public void create(Institucional reco) {
		this.repository.create(reco);
	}

	public void delete(Institucional reco) {
		this.repository.delete(reco);
	}


	public Institucional update(Institucional reco) {
		return this.repository.update(reco);
	}

	public List<Institucional> getAll() {
		return this.repository.getAll();
	}

	public Institucional findById(Long id) {
		return this.repository.findById(id);
	}

}
