package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.PreProducaoCO;
import br.com.finiciativas.fseweb.repositories.impl.PreProducaoCORepositoryImpl;

@Service
public class PreProducaoCOServiceImpl {

	@Autowired
	private PreProducaoCORepositoryImpl repository;
	
	public PreProducaoCO create(PreProducaoCO preProducaoCO) {
		 return this.repository.create(preProducaoCO);
	}

	public void delete(PreProducaoCO preProducaoCO) {
		this.repository.delete(preProducaoCO);
	}

	public PreProducaoCO update(PreProducaoCO preProducaoCO) {
		return this.repository.update(preProducaoCO);
	}

	public List<PreProducaoCO> getAll() {
		return this.repository.getAll();
	}

	public PreProducaoCO findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<PreProducaoCO> getAllByCliente(Long id) {
		return this.repository.getAllByCliente(id);
	}
}
