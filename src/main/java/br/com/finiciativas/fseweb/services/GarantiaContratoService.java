package br.com.finiciativas.fseweb.services;

import java.util.List;

import br.com.finiciativas.fseweb.models.contrato.GarantiaContrato;

public interface GarantiaContratoService {
	
	List<GarantiaContrato> getAll();
	
	GarantiaContrato findById(Long id);

}
