package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Acompanhamento;
import br.com.finiciativas.fseweb.repositories.impl.AcompanhamentoRepositoryImpl;

@Service
public class AcompanhamentoServiceImpl {

	@Autowired
	private AcompanhamentoRepositoryImpl repository;
	
	public void create(Acompanhamento acompanhamento) {
		this.repository.create(acompanhamento);
	}

	public void delete(Acompanhamento acompanhamento) {
		this.repository.delete(acompanhamento);
	}

	public Acompanhamento update(Acompanhamento acompanhamento) {
		return this.repository.update(acompanhamento);
	}

	public List<Acompanhamento> getAll() {
		return this.repository.getAll();
	}

	public Acompanhamento findById(Long id) {
		return this.repository.findById(id);
	}

	public List<Acompanhamento> getAcompanhamentoByProducao(Long id) {
		return this.repository.getAcompanhamentoByProducao(id);
	}
	
	
}
