package br.com.finiciativas.fseweb.services.impl;

import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.LogError;
import br.com.finiciativas.fseweb.repositories.impl.LogErrorRepositoryImpl;

@Service
public class LogErrorServiceImpl {
	
	LogErrorRepositoryImpl repository;
	
	public void delete(LogError logError) {
		this.repository.delete(logError);
	}
	
}
