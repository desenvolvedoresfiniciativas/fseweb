package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.contrato.GarantiaContrato;
import br.com.finiciativas.fseweb.repositories.impl.GarantiaContratoRepositoryImpl;
import br.com.finiciativas.fseweb.services.GarantiaContratoService;

@Service
public class GarantiaContratoServiceImpl implements GarantiaContratoService {
	
	@Autowired
	private GarantiaContratoRepositoryImpl garantiaRepository;

	@Override
	public List<GarantiaContrato> getAll() {
		return this.garantiaRepository.getAll();
	}
	
	@Override
	public GarantiaContrato findById(Long id) {
		return this.findById(id);
	}

}
