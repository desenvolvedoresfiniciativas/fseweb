package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoTecnica;
import br.com.finiciativas.fseweb.repositories.impl.InformacaoTecnicaRepositoryImpl;

@Service
public class InformacaoTecnicaServiceImpl {

private InformacaoTecnicaRepositoryImpl repository;
	
	public void create(InformacaoTecnica informacao) {
		this.repository.create(informacao);
	}

	public void delete(InformacaoTecnica informacao) {
		this.repository.delete(informacao);
	}

	public InformacaoTecnica update(InformacaoTecnica informacao) {
		return this.repository.update(informacao);
	}

	public List<InformacaoTecnica> getAll() {
		return this.repository.getAll();
	}

	public InformacaoTecnica findById(Long id) {
		return this.repository.findById(id);
	}

	public List<InformacaoTecnica> getInformacaoTecnicaByAcompanhamento(Long id) {
		return this.repository.getInformacaoTecnicaByAcompanhamento(id);
	}
	
}
