package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Recurso;
import br.com.finiciativas.fseweb.repositories.impl.RecursoRepositoryImpl;

@Service
public class RecursoServiceImpl {

	@Autowired
	private RecursoRepositoryImpl repository;
	
	public void create(Recurso recurso) {
		this.repository.create(recurso);
	}

	public void delete(Recurso recurso) {
		this.repository.delete(recurso);
	}

	public Recurso update(Recurso recurso) {
		return this.repository.update(recurso);
	}

	public List<Recurso> getAll() {
		return this.repository.getAll();
	}

	public Recurso findById(Long id) {
		return this.repository.findById(id);
	}

	public List<Recurso> getRecursoByAcompanhamento(Long id) {
		return this.repository.getRecursoByAcompanhamento(id);
	}
	
}
