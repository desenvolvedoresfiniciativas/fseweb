package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.FITechReport;
import br.com.finiciativas.fseweb.repositories.impl.FITechReportRepositoryImpl;

@Service
public class FITechReportServiceImpl {

	@Autowired
	private FITechReportRepositoryImpl repository;
	
	public void create(FITechReport FITechReport) {
		this.repository.create(FITechReport);
	}

	public void delete(FITechReport FITechReport) {
		this.repository.delete(FITechReport);
	}

	public FITechReport update(FITechReport FITechReport) {
		return this.repository.update(FITechReport);
	}
	
	public List<FITechReport> getAll() {
		return this.repository.getAll();
	}
	
	public FITechReport findById(Long id) {
		return this.repository.findById(id);
	}
	
}
