package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.Departamento;
import br.com.finiciativas.fseweb.models.PeriodoPagamento;
import br.com.finiciativas.fseweb.repositories.impl.PeriodoPagamentoRepositoryImpl;

@Service
public class PeriodoPagamentoServiceImpl {
	
	@Autowired
	private PeriodoPagamentoRepositoryImpl repository;
	
	@Transactional
	public void create(PeriodoPagamento periodoPagamento) {
		repository.create(periodoPagamento);
	}

	public List<PeriodoPagamento> getAll() {
		return this.repository.getAll();
	}

	public List<PeriodoPagamento> getAllNaoPagos() {
		return this.repository.getAllNaoPagos();
	}

	public List<PeriodoPagamento> getAllPeriodosNaoPagosByDepartamento(String consultoriaTecnica) {
		return this.repository.getAllPeriodosNaoPagosByDepartamento(consultoriaTecnica);
	}
}
