package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.impl.ItemValoresRepositoryImpl;

@Service
public class ItemValoresServiceImpl {

	@Autowired
	private ItemValoresRepositoryImpl itemValoresRepository;

	public void create(ItemValores itemValores) {
		itemValoresRepository.create(itemValores);
	}
	
	public void save(ItemValores itemValor) {
		itemValoresRepository.save(itemValor);
	}
	
	public void deleteOfProducao(Long id){
		itemValoresRepository.deleteOfProducao(id);
	}
	
	public void deleteOfSubProducao(Long id){
		itemValoresRepository.deleteOfSubProducao(id);
	}
	

	public List<ItemValores> getAll() {
		return itemValoresRepository.getAll();
	}

	public ItemValores update(ItemValores itemTarefa) {
		return itemValoresRepository.update(itemTarefa);
	}
	
	public void updatePuntoSituacion(String valor, Long producao_id, Long item_id, Long tarefa_id_PuntoSituacion) {
		itemValoresRepository.updatePuntoSituacion(valor, producao_id, item_id, tarefa_id_PuntoSituacion);
	}
	
	
	public ItemValores findById(Long id){
		return itemValoresRepository.findById(id);
	}
	
	public List<ItemValores> findByTarefaId(Long id){
		return itemValoresRepository.findByTarefaId(id);
	}
	//retorna los valores estimados
	public List<ItemValores> findByItemIdAndProducaoValoresEstimados(Long idProd){
		return itemValoresRepository.findByItemIdAndProducaoValoresEstimados(idProd);
	}
	//retorna los valores postulados
	public List<ItemValores> findByItemIdAndProducaoValoresPostulados(Long idProd){
		return itemValoresRepository.findByItemIdAndProducaoValoresPostulados(idProd);
	}
	//retorna los valores aprobados
		public List<ItemValores> findByItemIdAndProducaoValoresAprobados(Long idProd){
			return itemValoresRepository.findByItemIdAndProducaoValoresAprobados(idProd);
	}
	
	public List<ItemValores> findByTarefaAndProducao(Long id,Long idProd){
		return itemValoresRepository.findByTarefaAndProducao(id, idProd);
	}
	
	public List<ItemValores> getAllbyProducao(Long id) {
		return itemValoresRepository.getAllbyProducao(id);
	}
	
	public List<ItemValores> getAllbySubProducao(Long id) {
		return itemValoresRepository.getAllbySubProducao(id);
	}
	
	public ItemValores getAliquotaByProducao(Long id) {
		return itemValoresRepository.getAliquotaByProducao(id);
	}
	
	public List<ItemValores> getPrevisaoDeRelatorios(){
		return itemValoresRepository.getPrevisaoDeRelatorios();
	}
	
	public ItemValores getPrevValorDispendio(Long id) {
		return itemValoresRepository.getPrevValorDispendio(id);
	}
	
	public ItemValores getValorExclusao(Long id) {
		return itemValoresRepository.getValorExclusao(id);
	}
	
	public ItemValores getPrevisaoDeRelatoriosByProducao(Long id) {
		return itemValoresRepository.getPrevisaoDeRelatoriosByProducao(id);
	}
	
	public ItemValores getStatusFormByProducao(Long id) {
		return itemValoresRepository.getStatusFormByProducao(id);
	}
	
	public ItemValores getDataSubmissaoByProducao(Long id) {
		return itemValoresRepository.getDataSubmissaoByProducao(id);
	}
	
	public ItemValores getInvestimentoPeDByProducao(Long id) {
		return itemValoresRepository.getInvestimentoPeDByProducao(id);
	}
	
	public void atualizaDataFinalizacao(Long idProd, Long idEtapa, String data, String ano) {
		itemValoresRepository.atualizaDataFinalizacao(idProd, idEtapa, data, ano);
	}
	
	public List<ItemValores> findByItemAndTarefaId(Long idItem, Long idTarefa) {
		return itemValoresRepository.findByItemAndTarefaId(idItem, idTarefa);
	}
	
	public List<ItemValores> findByItemTarefaAndProducao(Long idItem, Long idTarefa, Long idProducao) {
		return itemValoresRepository.findByItemTarefaAndProducao(idItem, idTarefa, idProducao);
	}
	
	public List<ItemValores> getItemValoresOfSubproducao(Long id){
		return itemValoresRepository.getItemValoresOfSubproducao(id);
	}
	
	public ItemValores getPrevisaoBalanceteByProducao(Long id) {
		return itemValoresRepository.getPrevisaoBalanceteByProducao(id);
	}
	
	public List<ItemValores> getPrevisaoDeRelatoriosByCriteria(String ano, SetorAtividade setorAtividade, Long idConsultor, Long idEquipe) {
		return itemValoresRepository.getPrevisaoDeRelatoriosByCriteria(ano, setorAtividade, idConsultor, idEquipe);
	}
	
	public ItemValores getFechaInicioByProducao(Long id) {
		return itemValoresRepository.getFechaInicioByProducao(id);
	}
	
	public ItemValores getBeneficioCalculadoBySubProducao(Long id) {
		return itemValoresRepository.getBeneficioCalculadoBySubProducao(id);
	}
	
	public List<ItemValores> getCodigoProyectoByCliente(Long id){
		return itemValoresRepository.getCodigoProyectoByCliente(id);
	}
	
	public ItemValores getCodigoProyectoByProducao(Long id){
		return itemValoresRepository.getCodigoProyectoByProducao(id);
	}
	
	public ItemValores getFechaPrevistaBySubProducao(Long id){
		return itemValoresRepository.getFechaPrevistaBySubProducao(id);
	}
	
	public List<ItemValores> getEstadoProyectoByCliente(Long id){
		return itemValoresRepository.getEstadoProyectoByCliente(id);
	}

	public ItemValores findByTarefaItemTarefaAndProducao(Long idTarefa,Long producaoId, Long id) {
		return itemValoresRepository.findByTarefaItemTarefaAndProducao(idTarefa,producaoId, id);
	}
	
	public void createModificaciones(List<ItemTarefa> itens, Producao prod, Tarefa tarefa){
		itemValoresRepository.createModificaciones(itens, prod, tarefa);
	}
	public ItemValores getFechaGastoByProducao(Long id) {
		return itemValoresRepository.getFechaGastoByProducao(id);
	}
	public ItemValores getPresupuestoPreliminarByProducao(Long id) {
		return itemValoresRepository.getPresupuestoPreliminarByProducao(id);
	}
	public ItemValores getDuracionPreliminarByProducao(Long id) {
		return itemValoresRepository.getDuracionPreliminarByProducao(id);
	}
	public List<ItemValores> getCheckPostuladoByProducao(Long id){
		return itemValoresRepository.getCheckPostuladoByProducao(id);
	}
	public ItemValores getPresupuestoPostuladoByProducao(Long id,Long pid) {
		return itemValoresRepository.getPresupuestoPostuladoByProducao(id,pid);
	}
	public List<ItemValores> getCheckAprovadoByProducao(Long id){
		return itemValoresRepository.getCheckAprovadoByProducao(id);
	}
	public ItemValores getPresupuestoAprovadoByProducao(Long id,Long pid) {
		return itemValoresRepository.getPresupuestoAprovadoByProducao(id,pid);
	}
	public ItemValores getCertificadoModificacion(Long id){
		return itemValoresRepository.getCertificadoModificacion(id);
	}
	public ItemValores getValorCorfo(Long id) {
		return itemValoresRepository.getValorCorfo(id);
	}
	public ItemValores getFiscalizadoPresentar(Long id){
		return itemValoresRepository.getFiscalizadoPresentar(id);
	}
	public ItemValores getFiscalizacionCorfo(Long id){
		return itemValoresRepository.getFiscalizacionCorfo(id);
	}
	public ItemValores getCorrecionIPC(Long id) {
		return itemValoresRepository.getCorrecionIPC(id);
	}
	public ItemValores getEjecucionGasto(Long id){
		return itemValoresRepository.getEjecucionGasto(id);
	}
	public ItemValores getEtapaAtualByProd(Long id){
		return itemValoresRepository.getEtapaAtualByProd(id);
	}
	public void updateDisponible(String valor, Long id) {
		itemValoresRepository.updateDisponible(valor, id);
	}
	public List<ItemValores> getFechaInicioByCliente(Long id){
		return itemValoresRepository.getFechaInicioByCliente(id);
	}
	public List<ItemValores> getFechaTerminoByCliente(Long id){
		return itemValoresRepository.getFechaTerminoByCliente(id);
	}
	public List<ItemValores> getNombreProyectoByCliente(Long id){
		return itemValoresRepository.getNombreProyectoByCliente(id);
	}
	
	public ItemValores getOrganismoByProducao(Long idItem,Long idProducao) {
		return itemValoresRepository.getOrganismoByProducao(idItem, idProducao);
	}
	
	public ItemValores getOrganismoByProducaoAndTarefa(Long idItem, Long idTarefa, Long idProducao) {
		return itemValoresRepository.getOrganismoByProducaoAndTarefa(idItem, idTarefa, idProducao);
	}
}
