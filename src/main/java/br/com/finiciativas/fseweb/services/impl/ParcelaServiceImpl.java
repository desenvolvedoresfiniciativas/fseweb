package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.Parcela;
import br.com.finiciativas.fseweb.repositories.impl.ParcelasRepositoryImpl;

@Service
public class ParcelaServiceImpl {

	
	@Autowired
	private ParcelasRepositoryImpl repositoryImpl;
	
	public Parcela create(Parcela parcela) {
		return this.repositoryImpl.create(parcela);
	}
	
	public void delete(Parcela parcela) {
		this.repositoryImpl.delete(parcela);
	}
	
	public Parcela update(Parcela parcela) {
		return this.repositoryImpl.update(parcela);
	}
	
	public List<Parcela> getAll(){
		return this.repositoryImpl.getAll();
	}
	
	public Parcela findById (Long id) {
		return this.repositoryImpl.findById(id);
	}
	
	public List<Parcela> getParcelasByNF(Long idNf){
		return this.repositoryImpl.getParcelasByNF(idNf);
	}
	
	public List<Parcela> getParcelasPagasByNF(Long idNf){
		return this.repositoryImpl.getParcelasPagasByNF(idNf);
	}
}
