package br.com.finiciativas.fseweb.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculoRota2030;
import br.com.finiciativas.fseweb.repositories.impl.BalanceteCalculoROTARepositoryImpl;

@Service
public class BalanceteCalculoROTAServiceImpl {

	@Autowired
	private BalanceteCalculoROTARepositoryImpl repository;
	
	public BalanceteCalculoRota2030 create(BalanceteCalculoRota2030 balancete) {
		 return this.repository.create(balancete);
	}

	public void delete(BalanceteCalculoRota2030 balancete) {
		this.repository.delete(balancete);
	}

	public BalanceteCalculoRota2030 update(BalanceteCalculoRota2030 balancete) {
		return this.repository.update(balancete);
	}

	public List<BalanceteCalculoRota2030> getAll() {
		return this.repository.getAll();
	}

	public BalanceteCalculoRota2030 findById(Long id) {
		return this.repository.findById(id);
	}

	public List<BalanceteCalculoRota2030> getBalanceteCalculoRota2030ByProducao(Long id) {
		return this.repository.getBalanceteCalculoRota2030ByProducao(id);
	}
	
	public BalanceteCalculoRota2030 getLastBalanceteCalculoRota2030ByProducao(Long id) {
		return this.repository.getLastBalanceteCalculoRota2030ByProducao(id);
	}
	
	public BalanceteCalculoRota2030 getReducaoImpostoByProducao(Long id) {
		return this.repository.getLastBalanceteCalculoRota2030ByProducao(id);
	}
	
	public List<BalanceteCalculoRota2030> getAllBalanceteByAno(String ano){
		return this.repository.getAllBalanceteByAno(ano);
	}
}
