package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.consultor.Impostos;

public interface ImpostosRepository {

	Impostos getImpostosByCampanha2020();
	
	Impostos getImpostosbyId(Long id);
	
}
