package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoCalculo;

@Repository
public class InformacaoCalculoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(InformacaoCalculo informacao) {
		this.em.persist(informacao);
	}

	public void delete(InformacaoCalculo informacao) {
		this.em.remove(informacao);
	}

	@Transactional
	public InformacaoCalculo update(InformacaoCalculo informacao) {
		return this.em.merge(informacao);
	}
	
	public List<InformacaoCalculo> getAll() {
		return this.em.createQuery("SELECT distinct o FROM InformacaoCalculo o ", InformacaoCalculo.class)
				.getResultList();
	}

	public InformacaoCalculo findById(Long id) {
		return this.em.find(InformacaoCalculo.class, id);
	}

	public List<InformacaoCalculo> getInformacaoCalculoByAcompanhamento(Long id) {
		return this.em.createQuery("SELECT distinct o FROM InformacaoCalculo o WHERE o.acompanhamento.id = :pId ", InformacaoCalculo.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
