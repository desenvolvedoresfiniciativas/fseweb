	package br.com.finiciativas.fseweb.repositories.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorDisponible;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorFiscalizado;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioCLServiceImpl;

@Repository
public class ValorDisponibleRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ItemValoresServiceImpl ItemValoresService;

	@Autowired
	private SubProducaoServiceImpl subProducaoService;
	
	@Autowired
	private PresupuestosValorNegocioCRepositoryImpl presupuestosImpl;

	@Transactional
	public void geraValorDisponible() {
		List<Producao> prodAll= producaoService.getAll();
		DeleteAllFromDisponible();
		for(Producao prod : prodAll) {
			System.out.println(prod.getId());
			String fechaInicio;
			try {
				fechaInicio = ItemValoresService.getFechaInicioByProducao(prod.getId()).getValor();
			} catch (Exception e) {
				fechaInicio = "0";
			}
			Integer fechaInicioAno = 0;

			try {
				String parts[] = fechaInicio.split("/");
				fechaInicioAno = Integer.parseInt(parts[2]);
			} catch (Exception e) {
				fechaInicioAno = 0;
			}
			String fechaGasto;
			try {
				fechaGasto = ItemValoresService.getFechaGastoByProducao(prod.getId()).getValor();
			} catch (Exception e) {
				// TODO: handle exception
				fechaGasto = "";
			}
			Integer fechaGastoAno = 0;
			try {
				String part[] = fechaGasto.split("/");
				fechaGastoAno = Integer.parseInt(part[2]);
			} catch (Exception e) {
				fechaGastoAno = 0;
			}
			String fechaReferencia = null;
			if (fechaInicioAno <= 0 || fechaInicioAno == null) {
				fechaReferencia = prod.getAno();
			} else if (fechaInicioAno > fechaGastoAno) {
				fechaReferencia = String.valueOf(fechaInicioAno);
			} else {
				fechaReferencia = String.valueOf(fechaGastoAno);
			}
			System.out.println(fechaReferencia);
			try {
				BigDecimal certificadoModifica = BigDecimal.ZERO;
				try {
					if (ItemValoresService.getCertificadoModificacion(prod.getId()).getValor().contains(",")) {
						certificadoModifica = new BigDecimal(ItemValoresService.getCertificadoModificacion(prod.getId()).getValor().replace(".", "").replace(",", ".")).setScale(2,RoundingMode.HALF_DOWN);	
					}else {
						certificadoModifica = new BigDecimal(ItemValoresService.getCertificadoModificacion(prod.getId()).getValor()).setScale(2,RoundingMode.HALF_DOWN);
					}
				} catch (Exception e) {
					certificadoModifica = BigDecimal.ZERO;
				}
				System.out.println("at� aqui ok");
				BigDecimal disponible = BigDecimal.ZERO;
				if (certificadoModifica.compareTo(BigDecimal.ZERO)==1) {
					System.out.println("aqui");
					disponible = certificadoModifica;
				}else {
					System.out.println("ali");
					disponible = new BigDecimal(presupuestosImpl.getAprovadoByProducao(prod.getId())).setScale(2,RoundingMode.HALF_DOWN);
				}
				List<SubProducao> subs = subProducaoService.getSubProducaoByProducao(prod.getId());
				System.out.println(subs);
				Integer trabajadas = subs.size();
				Integer i = 0;
				boolean value = true;
				BigDecimal valor = BigDecimal.ZERO;
				System.out.println(subs.size());
				for(SubProducao sub : subs) {
					BigDecimal corfo = BigDecimal.ZERO;
					try {
						if (ItemValoresService.getValorCorfo(sub.getId()).getValor().contains(",")) {
							corfo = new BigDecimal(ItemValoresService.getValorCorfo(sub.getId()).getValor().replace(".", "").replace(",", "."));
						}else {
							corfo = new BigDecimal(ItemValoresService.getValorCorfo(sub.getId()).getValor());
						}
					} catch (Exception e) {
						System.out.println("sem corfo");
					}
					if (value == true && corfo.compareTo(BigDecimal.ZERO)==1) {
						ValorDisponible primeiroDisponible = new ValorDisponible();
						primeiroDisponible.setFechaReferencia(fechaReferencia);
						primeiroDisponible.setAnoFiscal(fechaReferencia );
						primeiroDisponible.setCampanha(prod.getAno());
						primeiroDisponible.setValor(disponible.toString());
						primeiroDisponible.setSubProducao(sub);
						primeiroDisponible.setProducao(prod);
						primeiroDisponible.setCorfo(corfo.toString());
						System.out.println("primeiro");
						try {
							ItemValoresService.updateDisponible(disponible.toString(), sub.getId());
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
							System.out.println("not work");
						}
						Create(primeiroDisponible);
						BigDecimal disponibleFinal = disponible.subtract(corfo);
						disponible = disponibleFinal;
						trabajadas = trabajadas-1;
						value=false;
						i++;
						
					}else {
						if (corfo.compareTo(BigDecimal.ZERO)==1) {
							try {
								ValorDisponible valorDisponible = new ValorDisponible();
								valorDisponible.setAnoFiscal(String.valueOf(Integer.parseInt(fechaReferencia)+i));
								valorDisponible.setCampanha(prod.getAno());
								valorDisponible.setFechaReferencia(fechaReferencia);
								valorDisponible.setProducao(prod);
								valorDisponible.setSubProducao(sub);
								valorDisponible.setCorfo(corfo.toString());
								valorDisponible.setValor(disponible.toString());
								System.out.println("segundo");
								try {
									ItemValoresService.updateDisponible(disponible.toString(), sub.getId());
								} catch (Exception e) {
									// TODO: handle exception
									System.out.println("not work");
								}
								Create(valorDisponible);
								System.out.println(disponible.subtract(corfo));
								BigDecimal disponibleFinal = disponible.subtract(corfo);
								disponible = disponibleFinal;
								trabajadas = trabajadas-1;
								i++;
								
							} catch (Exception e) {
								// TODO: handle exception
								System.out.println("erro "+e);
							}
							
						}else {
							valor  = disponible.divide(new BigDecimal(trabajadas),2,RoundingMode.HALF_DOWN);
							ValorDisponible valorDisponible = new ValorDisponible();
							valorDisponible.setAnoFiscal(String.valueOf(Integer.parseInt(fechaReferencia)+i));
							valorDisponible.setCampanha(prod.getAno());
							valorDisponible.setFechaReferencia(fechaReferencia);
							valorDisponible.setProducao(prod);
							valorDisponible.setSubProducao(sub);
							valorDisponible.setCorfo(null);
							valorDisponible.setValor(valor.toString());
							System.out.println("terceiro");
							try {
								ItemValoresService.updateDisponible(disponible.toString(), sub.getId());
							} catch (Exception e) {
								// TODO: handle exception
								System.out.println("not work");
							}
							Create(valorDisponible);
							i++;
							
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
			
			
			
		}
	}

	@Transactional
	public void Create(ValorDisponible valorDisponible) {
		this.em.persist(valorDisponible);
	}

	@Transactional
	public void DeleteAllFromDisponible() {
		this.em.createNativeQuery("DELETE FROM valor_disponible").executeUpdate();
	}
	public ValorDisponible getValorDisponibleBySubProd(Long id) {
		try {
			return em.createQuery("SELECT vd from ValorDisponible vd WHERE vd.subProducao.id = :pId", ValorDisponible.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	@Transactional
	public void geraValorFiscalizado() {
		List<SubProducao> subproducoes = subProducaoService.getAllOrderProd();
		DeleteAllFromFiscalizado();
		try {
			String anosub = "";
			Long producaoPass = 0L;
			for (SubProducao sub : subproducoes) {
				System.out.println(sub.getProducao().getId());
				try {
					
					String EjecucionGasto = ItemValoresService.getEjecucionGasto(sub.getId()).getValor();
					try {
						String parts[] = EjecucionGasto.split("/");
						EjecucionGasto = parts[2];
					} catch (Exception e) {
						
					}
					ValorFiscalizado fiscalizado = new ValorFiscalizado();
					fiscalizado.setCampanha(sub.getProducao().getAno());
					fiscalizado.setProducao(sub.getProducao());
					fiscalizado.setSubProducao(sub);
					fiscalizado.setAnoGasto(EjecucionGasto);
					fiscalizado.setContrato(sub.getProducao().getContrato());
					BigDecimal correcionIpc = new BigDecimal("0.01");
					BigDecimal fiscalizacionCorfo = new BigDecimal("0.00");
					BigDecimal fiscalizadoPresentar = new BigDecimal("0.00");
					BigDecimal valorDisponible = new BigDecimal("0.00");
					try {
						if (ItemValoresService.getCorrecionIPC(sub.getId()).getValor().contains(",")) {
							correcionIpc = new BigDecimal(ItemValoresService.getCorrecionIPC(sub.getId()).getValor().replace(".", "").replace(",", "."));
						}else {
							correcionIpc = new BigDecimal(ItemValoresService.getCorrecionIPC(sub.getId()).getValor());							
						}
						
					} catch (Exception e) {
						// TODO: handle exception
					}
					try {
						if (ItemValoresService.getFiscalizacionCorfo(sub.getId()).getValor().contains(",")) {
							fiscalizacionCorfo = new BigDecimal(ItemValoresService.getFiscalizacionCorfo(sub.getId()).getValor().replace(".", "").replace(",", "."));
						}else {
							fiscalizacionCorfo = new BigDecimal(ItemValoresService.getFiscalizacionCorfo(sub.getId()).getValor());
						}
						
					} catch (Exception e) {
						// TODO: handle exception
					}
					try {
						if (ItemValoresService.getFiscalizadoPresentar(sub.getId()).getValor().contains(",")) {
							fiscalizadoPresentar = new BigDecimal(ItemValoresService.getFiscalizadoPresentar(sub.getId()).getValor().replace(".", "").replace(",", "."));
						}else {
							fiscalizadoPresentar = new BigDecimal(ItemValoresService.getFiscalizadoPresentar(sub.getId()).getValor());
						}
						
					} catch (Exception e) {
						// TODO: handle exception
					}
					try {
						valorDisponible = new BigDecimal(getValorDisponibleBySubProd(sub.getId()).getValor());
					} catch (Exception e) { 
						// TODO: handle exception
						System.out.println(e);
					}
					if (correcionIpc.compareTo(new BigDecimal("0.01"))!=0) {
						fiscalizado.setRut(sub.getProducao().getContrato().getCliente().getRut());
						fiscalizado.setValor(correcionIpc.toString());
						fiscalizado.setFase("Correcion IPC");
					}else if(correcionIpc.compareTo(BigDecimal.ZERO)==0){
						fiscalizado.setRut(sub.getProducao().getContrato().getCliente().getRut());
						fiscalizado.setValor(correcionIpc.toString());
						fiscalizado.setFase("Correcion IPC zero");
					} else if (fiscalizacionCorfo.compareTo(new BigDecimal("0.00"))!=0) {
						fiscalizado.setValor(fiscalizacionCorfo.toString());
						fiscalizado.setRut(sub.getProducao().getContrato().getCliente().getRut());
						fiscalizado.setFase("corfo");
					}else if (valorDisponible.compareTo(new BigDecimal("0.00"))!=0) {
						fiscalizado.setFase("disponible");
						fiscalizado.setRut(sub.getProducao().getContrato().getCliente().getRut());
						fiscalizado.setValor(valorDisponible.toString());
					} else {
						continue;
					}
					
					Create(fiscalizado);
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
				
			}
		} catch (Exception e) {
			System.out.println(e);

		} finally {
			System.out.println("End");
		}
	}

	public ValorFiscalizado getValueBySubId(Long id) {
		return this.em.createQuery("SELECT vf FROM ValorFiscalizado vf WHERE vf.subProducao.id = :subId",
				ValorFiscalizado.class).setParameter("subId", id).getSingleResult();
	}

	public ValorFiscalizado getValueByProdId(Long id) {
		return this.em.createQuery("SELECT vf FROM ValorFiscalizado vf WHERE vf.producao.id = :prodId",
				ValorFiscalizado.class).setParameter("prodId", id).getSingleResult();
	}

	@Transactional
	public void Create(ValorFiscalizado valorFiscalizado) {
		this.em.persist(valorFiscalizado);
	}

	@Transactional
	public void DeleteAllFromFiscalizado() {
		this.em.createNativeQuery("DELETE FROM valor_fiscalizado").executeUpdate();
	}

	public List<ValorFiscalizado> getValoresByProd(Long id,String ano) {
		try {
			return em.createQuery("SELECT vf FROM ValorFiscalizado vf WHERE vf.producao.id = :prodId and vf.anoGasto = :ano ORDER BY vf.anoGasto", ValorFiscalizado.class)
					.setParameter("prodId", id).setParameter("ano", ano).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	public List<String> getAnoGastoByProducao(Long id) {
		try {
			return em.createNativeQuery("SELECT DISTINCT(ano_gasto) FROM valor_fiscalizado WHERE producao_id = "+id+" ORDER BY ano_gasto").getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public List<ValorFiscalizado> getValoresByContrato(Long id) {
		try {
			return em.createQuery("SELECT vf FROM ValorFiscalizado vf WHERE vf.contrato.id = :cId",
					ValorFiscalizado.class).setParameter("cId", id).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public List<ValorFiscalizado> getFiscalizadoByProdAndAnoAndCampanha(Long id,String ano, String camp) {
		try {
			return em.createQuery("SELECT vp FROM ValorFiscalizado vp WHERE vp.producao.id = :prodId and vp.anoGasto = :ano and vp.campanha = :camp ORDER BY vp.anoFiscal", ValorFiscalizado.class)
					.setParameter("prodId", id).setParameter("ano", ano).setParameter("camp", camp).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}
