package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;

@Repository
public class ItemValoresRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ProducaoServiceImpl producaoService;
	
	@Autowired
	private SubProducaoServiceImpl subProducaoService;

	public List<ItemValores> getAll() {
		return em.createQuery("SELECT distinct iv FROM ItemValores iv ", ItemValores.class).getResultList();
	}
	
	@Transactional
	public void deleteOfProducao(Long id){
		em.createQuery("DELETE FROM ItemValores v WHERE v.producao.id = :pId")
				.setParameter("pId", id)
				.executeUpdate();
	}
	
	@Transactional
	public void deleteOfSubProducao(Long id){
		em.createQuery("DELETE FROM ItemValores v WHERE v.subProducaoIndex.id = :pId")
				.setParameter("pId", id)
				.executeUpdate();
	}

	public List<ItemValores> getAllbyProducao(Long id) {
		return em.createQuery(
				"SELECT distinct iv FROM ItemValores iv WHERE iv.producao.id = :pId ORDER BY iv.numeroOrdem",
				ItemValores.class).setParameter("pId", id).getResultList();
	}

	public List<ItemValores> getAllbySubProducao(Long id) {
		return em.createQuery("SELECT distinct iv FROM ItemValores iv WHERE iv.subProducaoIndex.id = :pId ORDER BY iv.numeroOrdem",
				ItemValores.class).setParameter("pId", id).getResultList();
	}

	public List<ItemValores> findByItemAndTarefaId(Long idItem, Long idTarefa) {
		try{
			return em.createQuery("SELECT distinct i FROM ItemValores i WHERE i.item.id = :pIdItem AND i.tarefa.id = :pIdTarefa", ItemValores.class)
				.setParameter("pIdItem", idItem)
				.setParameter("pIdTarefa", idTarefa)
				.getResultList();
			} catch (Exception e) {
				System.out.println(e);
				List<ItemValores> itens = new ArrayList<ItemValores>();
				return itens;
			}
	}

	public List<ItemValores> findByItemTarefaAndProducao(Long idItem, Long idTarefa, Long idProducao) {
		try{
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pIdItem AND i.tarefa.id = :pIdTarefa AND i.producao.id = :pIdProducao", ItemValores.class)
				.setParameter("pIdItem", idItem)
				.setParameter("pIdTarefa", idTarefa)
				.setParameter("pIdProducao", idProducao)
				.getResultList();
			} catch (Exception e) {
				System.out.println(e);
				List<ItemValores> itens = new ArrayList<ItemValores>();
				return itens;
			}
	}
	//Retorna los valores estimados
	public List<ItemValores> findByItemIdAndProducaoValoresEstimados(Long idProducao) {
		try{
			return em.createQuery("SELECT distinct i FROM ItemValores i WHERE i.item.id in(29, 30, 31, 32, 33) AND i.producao.id = :pIdProducao", ItemValores.class)
				.setParameter("pIdProducao", idProducao)
				.getResultList();
			} catch (Exception e) {
				System.out.println(e);
				List<ItemValores> itens = new ArrayList<ItemValores>();
				return itens;
			}
	}
	
	//Retorna los valores postulados
	public List<ItemValores> findByItemIdAndProducaoValoresPostulados(Long idProducao) {
		try{
			return em.createQuery("SELECT distinct i FROM ItemValores i WHERE i.item.id in(61, 62, 63, 64, 65) AND i.producao.id = :pIdProducao", ItemValores.class)
				.setParameter("pIdProducao", idProducao)
				.getResultList();
			} catch (Exception e) {
				System.out.println(e);
				List<ItemValores> itens = new ArrayList<ItemValores>();
				return itens;
			}
	}

	//Retorna los valores aprobados
		public List<ItemValores> findByItemIdAndProducaoValoresAprobados(Long idProducao) {
			try{
				return em.createQuery("SELECT distinct i FROM ItemValores i WHERE i.item.id in(86, 87, 88, 89, 90) AND i.producao.id = :pIdProducao", ItemValores.class)
					.setParameter("pIdProducao", idProducao)
					.getResultList();
				} catch (Exception e) {
					System.out.println(e);
					List<ItemValores> itens = new ArrayList<ItemValores>();
					return itens;
				}
		}
	
	public List<ItemValores> findByTarefaId(Long id) {
		return em.createQuery(
				"SELECT distinct i " + "FROM ItemValores i " + "JOIN i.tarefa " + "WHERE i.tarefa.id = :pId",
				ItemValores.class).setParameter("pId", id).getResultList();
	}

	public List<ItemValores> findByTarefaAndProducao(Long id, Long idProd) {
		return em
				.createQuery("SELECT distinct i " + "FROM ItemValores i " + "JOIN i.tarefa " + "JOIN i.producao "
						+ "WHERE i.tarefa.id = :pId " + "AND i.producao.id = :pIdProd", ItemValores.class)
				.setParameter("pId", id).setParameter("pIdProd", idProd).getResultList();
	}

	@Transactional
	public void create(ItemValores itemValores) {
		itemValores.setValor("");
		this.em.persist(itemValores);
	}
	
	@Transactional
	public void save(ItemValores itemValores) {
		this.em.persist(itemValores);
	}
	
	@Transactional
	public ItemValores update(ItemValores itemValores) {
		return this.em.merge(itemValores);
	}

	public ItemValores findById(Long id) {
		return this.em.find(ItemValores.class, id);
	}

	public ItemValores getAliquotaByProducao(Long id) {

		System.out.println("ID DE BUSCA " + id);

		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.producao.id = :pId AND iv.item.id = :pIdItem",
					ItemValores.class).setParameter("pId", id).setParameter("pIdItem", 368l).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public List<ItemValores> getPrevisaoDeRelatorios() {

		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId", ItemValores.class)
					.setParameter("pId", 366l).getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	public ItemValores getPrevValorDispendio(Long id) {

		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 323l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public ItemValores getValorExclusao(Long id) {

		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 324l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public ItemValores getPrevisaoDeRelatoriosByProducao(Long id) {

		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 366l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public ItemValores getStatusFormByProducao(Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 370l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public ItemValores getDataSubmissaoByProducao(Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 370l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public ItemValores getInvestimentoPeDByProducao(Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 595l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public ItemValores getPrevisaoBalanceteByProducao(Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 325l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public void atualizaDataFinalizacao(Long idProd, Long idEtapa, String data, String ano) {

		Query query = em.createQuery(
				"UPDATE ItemValores i SET i.dataPrevisao = :data JOIN i.tarefa.etapas e WHERE e.id = :idEtapa AND i.producao.produto = :idProd AND i.producao.ano = :ano");
		query.setParameter("data", data);
		query.setParameter("idEtapa", idEtapa);
		query.setParameter("idProd", idProd);
		query.setParameter("ano", ano);
		query.executeUpdate();

	}
	
	@Transactional
	public void  updatePuntoSituacion(String valor, Long producao_id, Long item_id, Long tarefa_id_PuntoSituacion) {
		Query query = em.createQuery(
				"UPDATE ItemValores i SET i.valor =:valor "
				+ "WHERE i.producao.id =:producao_id AND i.item.id =:item_id AND i.tarefa.id =:tarefa_id_PuntoSituacion");
		query.setParameter("valor", valor);
		query.setParameter("producao_id", producao_id);
		if ( 100L == item_id ) {
			item_id = 21L;
			query.setParameter("item_id", item_id);
		} else query.setParameter("item_id", item_id);
		query.setParameter("tarefa_id_PuntoSituacion", tarefa_id_PuntoSituacion);
		query.executeUpdate();
	}
	
	public List<ItemValores> getItemValoresOfSubproducao(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.subProducaoIndex.id = :pId",
					ItemValores.class)
					.setParameter("pId", id)
					.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<ItemValores> getPrevisaoDeRelatoriosByCriteria(String ano, SetorAtividade setor, Long idConsultor, Long idEquipe) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<ItemValores> root = cq.from(ItemValores.class);

		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (ano != null) {
			predicates.add(qb.equal(root.get("producao").get("ano"), ano));
		}
		
		if (idEquipe != null) {
			predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), idEquipe));
		}
		
		if (idConsultor != null) {
			predicates.add(qb.equal(root.get("producao").get("consultor").get("id"), idConsultor));
		}
		
		if (setor != null) {
			predicates.add(qb.equal(root.get("producao").get("cliente").get("setorAtividade"), setor));
		}
		
		predicates.add(qb.equal(root.get("item").get("id"), 366l));

		// query itself
		cq.orderBy(qb.desc(root.get("producao").get("cliente").get("razaoSocial")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}
	
	public ItemValores getFechaInicioByProducao(Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 50l).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public ItemValores getBeneficioCalculadoBySubProducao(Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.subProducaoIndex.id = :pIdSubProd",
					ItemValores.class).setParameter("pId", 690l).setParameter("pIdSubProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<ItemValores> getCodigoProyectoByCliente(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.cliente.id = :pIdCliente",
					ItemValores.class).setParameter("pId", 20l).setParameter("pIdCliente", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<ItemValores> getEstadoProyectoByCliente(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.cliente.id = :pIdCliente",
					ItemValores.class).setParameter("pId", 188l).setParameter("pIdCliente", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	public ItemValores getCodigoProyectoByProducao(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProducao",
					ItemValores.class).setParameter("pId", 20l).setParameter("pIdProducao", id).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public ItemValores getFechaPrevistaBySubProducao(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.subProducaoIndex.id = :pIdSubProducao",
					ItemValores.class).setParameter("pId", 172l).setParameter("pIdSubProducao", id).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ItemValores findByTarefaItemTarefaAndProducao(Long idTarefa,Long producaoId, Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProducao AND iv.tarefa.id = :pIdTarefa",
					ItemValores.class).setParameter("pIdTarefa", idTarefa).setParameter("pId", id).setParameter("pIdProducao", producaoId).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public void createModificaciones(List<ItemTarefa> itens, Producao prod, Tarefa tarefa){
		
		for (ItemTarefa itemTarefa : itens) {
			
			ItemValores iv = new ItemValores();
			iv.setProducao(prod);
			iv.setItem(itemTarefa);
			iv.setTarefa(tarefa);
			iv.setNumeroOrdem(0);
			
			create(iv);
			
		}
		
	}
	public ItemValores getDuracionPreliminarByProducao(Long id) {
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 22L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	public ItemValores getPresupuestoPreliminarByProducao(Long id) {
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 23L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	public ItemValores getFechaGastoByProducao(Long id) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", 14L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	public List<ItemValores> getCheckPostuladoByProducao(Long id){
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id >=63 AND i.item.id <=87 AND i.producao.id = :pId and i.valor <> ''",
					ItemValores.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	public ItemValores getPresupuestoPostuladoByProducao(Long id, Long pid) {
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", pid).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	public List<ItemValores> getCheckAprovadoByProducao(Long id){
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id >=125 AND i.item.id <=149 AND i.producao.id = :pId and i.valor <> ''",
					ItemValores.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	public ItemValores getPresupuestoAprovadoByProducao(Long id, Long pid) {
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.producao.id = :pIdProd",
					ItemValores.class).setParameter("pId", pid).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	public ItemValores getCertificadoModificacion(Long id) {
		try {
			return this.em
					.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.producao.id = :pIdProd",
							ItemValores.class)
					.setParameter("pId", 124L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public ItemValores getValorCorfo(Long id) {
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.subProducaoIndex.id = :pIdProd",
					ItemValores.class).setParameter("pId", 165L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	public ItemValores getEjecucionGasto(Long id) {
		try {
			return em.createQuery(
					"SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.subProducaoIndex.id = :pIdProd",
					ItemValores.class).setParameter("pId", 154L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	public ItemValores getFiscalizadoPresentar(Long id) {
		try {
			return em.createQuery(
					"SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.subProducaoIndex.id = :pIdProd",
					ItemValores.class).setParameter("pId", 159L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	public ItemValores getFiscalizacionCorfo(Long id) {
		try {
			return em.createQuery(
					"SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.subProducaoIndex.id = :pIdProd",
					ItemValores.class).setParameter("pId", 165L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	public ItemValores getCorrecionIPC(Long id) {
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.subProducaoIndex.id = :pIdProd", ItemValores.class)
					.setParameter("pId", 168L).setParameter("pIdProd", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public ItemValores getEtapaAtualByProd(Long id) {
		try {
			return em.createQuery("SELECT i FROM ItemValores i WHERE i.item.id = :pId AND i.producao.id = :pIdProd AND i.valor <> :val",ItemValores.class)
					.setParameter("pId", 188L).setParameter("pIdProd", id).setParameter("val", "").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return null;
		}
	}
	
	public void updateDisponible(String valor, Long sub) {
		em.createQuery("UPDATE ItemValores i set i.valor = :iValor WHERE i.subProducaoIndex.id = :pIdProd and i.item.id = :pId")
				.setParameter("iValor", valor).setParameter("pIdProd", sub).setParameter("pId", 158L).executeUpdate();
	}
	public List<ItemValores> getFechaInicioByCliente(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.cliente.id = :pIdCliente",
					ItemValores.class).setParameter("pId", 50l).setParameter("pIdCliente", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	public List<ItemValores> getFechaTerminoByCliente(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.cliente.id = :pIdCliente",
					ItemValores.class).setParameter("pId", 51l).setParameter("pIdCliente", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	public List<ItemValores> getNombreProyectoByCliente(Long id){
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.cliente.id = :pIdCliente",
					ItemValores.class).setParameter("pId", 18l).setParameter("pIdCliente", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	public ItemValores getOrganismoByProducao(Long idItem, Long idProducao) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.producao.id = :pIdSubProd",
					ItemValores.class).setParameter("pId", idItem).setParameter("pIdSubProd", idProducao).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public ItemValores getOrganismoByProducaoAndTarefa(Long idItem, Long idTarefa, Long idProducao) {
		try {
			return em.createQuery("SELECT iv FROM ItemValores iv WHERE iv.item.id = :pId AND iv.tarefa.id = :pTarefaId AND iv.producao.id = :pIdSubProd",
					ItemValores.class).setParameter("pId", idItem).setParameter("pTarefaId", idTarefa).setParameter("pIdSubProd", idProducao).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}
