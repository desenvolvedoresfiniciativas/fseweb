package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.contrato.GarantiaContrato;
import br.com.finiciativas.fseweb.repositories.GarantiaContratoRepository;

@Repository
public class GarantiaContratoRepositoryImpl implements GarantiaContratoRepository {
	
	@Autowired
	private EntityManager em;

	@Override
	public List<GarantiaContrato> getAll() {
		try {
			return this.em.createQuery("FROM GarantiaContrato", GarantiaContrato.class).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	public GarantiaContrato findById(Long id) {
		return this.em.find(GarantiaContrato.class, id);
	}

}
