package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.lista.ValoresLista;

public interface ValoresListaRepository {

	List<ValoresLista> getAll();
	
	void create (ValoresLista lista);
	
	void delete (ValoresLista lista);
	
	ValoresLista update (ValoresLista lista);
	
	List<ValoresLista> findByItemId(Long id);
	
}
