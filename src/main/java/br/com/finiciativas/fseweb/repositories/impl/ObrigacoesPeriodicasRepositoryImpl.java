package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ObrigacoesPeriodicas;
import br.com.finiciativas.fseweb.models.produto.ItemValores;

@Repository
public class ObrigacoesPeriodicasRepositoryImpl {

	@Autowired
	private EntityManager em;

	public List<ObrigacoesPeriodicas> getAll() {
		return this.em.createQuery("SELECT distinct o FROM ObrigacoesPeriodicas o ", ObrigacoesPeriodicas.class)
				.getResultList();
	}

	@Transactional
	public void create(ObrigacoesPeriodicas obrigacoes) {
		this.em.merge(obrigacoes);
	}

	public void delete(ObrigacoesPeriodicas obrigacoes) {
		this.em.remove(obrigacoes);
	}

	@Transactional
	public ObrigacoesPeriodicas update(ObrigacoesPeriodicas obrigacoes) {
		
		return this.em.merge(obrigacoes);
	}

	public ObrigacoesPeriodicas findById(Long id) {
		return this.em.find(ObrigacoesPeriodicas.class, id);
	}

	public ObrigacoesPeriodicas getLastItemAdded() {

		String sQuery = "SELECT p " + "FROM ObrigacoesPeriodicas p "
				+ "WHERE p.id=(SELECT max(id) FROM ObrigacoesPeriodicas)";

		return em.createQuery(sQuery, ObrigacoesPeriodicas.class).getSingleResult();
	}

	public void addObrigacoesByFrequencia(ItemValores itemvalor) {
		int qntAcomp = 0;
		System.out.println("");
		if (itemvalor.getValor() != null) {
			switch (itemvalor.getValor()) {
			case ("Mensal"):
				qntAcomp = 12;
			case ("Trimestral"):
				qntAcomp = 4;
			case ("Anual"):
				qntAcomp = 1;
			}
			for (int i = 0; i <= qntAcomp; i++) {
				ObrigacoesPeriodicas obrigacoes = new ObrigacoesPeriodicas();
				obrigacoes.setProducao(itemvalor.getProducao());
				this.em.merge(obrigacoes);;
			}
		}
	}
	
	public List<ObrigacoesPeriodicas> getObrigacoesByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM ObrigacoesPeriodicas o WHERE o.producao.id = :pId ", ObrigacoesPeriodicas.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
	public ItemValores getFNDCTbyProducaoId(Long id){
		System.out.println("-.-.->> ESTOU PROCURANDO O ID: "+id+" NO BANCO DE DADOS <-.-.-");
		return this.em.createQuery("SELECT i FROM ItemValores i WHERE i.item.nome = :pNome AND i.producao.id = :pId", ItemValores.class)
				.setParameter("pId", id)
				.setParameter("pNome", "FNDCT")
				.getSingleResult();
	}

	public ItemValores getObrigacoesPeriodicasByProducao(Long id){
		return this.em.createQuery("SELECT distinct i FROM ItemValores i WHERE i.item.nome = :pNome AND i.producao.id = :pId ", ItemValores.class)
				.setParameter("pId", id)
				.setParameter("pNome", "Frenquência de acompanhamento")
				.getSingleResult();
	}
	
}
