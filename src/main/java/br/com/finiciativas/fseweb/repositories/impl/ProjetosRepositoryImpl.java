package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Projetos;

@Repository
public class ProjetosRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Projetos projetos) {
		this.em.persist(projetos);
	}

	public void delete(Projetos projetos) {
		this.em.remove(projetos);
	}

	@Transactional
	public Projetos update(Projetos projetos) {
		return this.em.merge(projetos);
	}
	
	public List<Projetos> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Projetos o ", Projetos.class)
				.getResultList();
	}
	
	public Projetos findById(Long id) {
		return this.em.find(Projetos.class, id);
	}
	
	public List<Projetos> getProjetosByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM Projetos o WHERE o.producao.id = :pId ", Projetos.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
