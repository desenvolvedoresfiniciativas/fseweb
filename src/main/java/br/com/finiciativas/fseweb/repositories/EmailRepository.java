package br.com.finiciativas.fseweb.repositories;

public interface EmailRepository {
	
	void send(String nome, String emailDestinatario, String assunto, String msg);
	
}
