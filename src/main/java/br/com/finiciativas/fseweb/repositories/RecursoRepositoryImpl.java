package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Recurso;

public interface RecursoRepositoryImpl {

	void create(Recurso recurso);

	void delete(Recurso recurso);
		
	Recurso update(Recurso recurso);
	
	List<Recurso> getAll();
	
	Recurso findById(Long id);
	
	List<Recurso> getRecursoByAcompanhamento(Long id);
	
}
