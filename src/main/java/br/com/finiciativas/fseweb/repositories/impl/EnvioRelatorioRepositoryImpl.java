package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.EnvioRelatorio;

@Repository
public class EnvioRelatorioRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(EnvioRelatorio envio) {
		this.em.persist(envio);
	}

	public void delete(EnvioRelatorio envio) {
		this.em.remove(envio);
	}

	@Transactional
	public EnvioRelatorio update(EnvioRelatorio envio) {
		return this.em.merge(envio);
	}
	
	public List<EnvioRelatorio> getAll() {
		return this.em.createQuery("SELECT distinct o FROM EnvioRelatorio o ", EnvioRelatorio.class)
				.getResultList();
	}

	public EnvioRelatorio findById(Long id) {
		return this.em.find(EnvioRelatorio.class, id);
	}

	public List<EnvioRelatorio> getEnvioRelatorioByAcompanhamento(Long id) {
		return this.em.createQuery("SELECT distinct o FROM EnvioRelatorio o WHERE o.acompanhamento.id = :pId ", EnvioRelatorio.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
