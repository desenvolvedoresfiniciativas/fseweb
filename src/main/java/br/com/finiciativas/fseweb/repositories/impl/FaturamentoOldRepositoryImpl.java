package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.old.FaturamentoOld;

@Repository
public class FaturamentoOldRepositoryImpl {

		@Autowired
		private EntityManager em;
		
		@Transactional
		public void create(FaturamentoOld faturamentoOld) {
			
			
			this.em.persist(faturamentoOld);
		}

		public void delete(FaturamentoOld faturamentoOld) {
			this.em.remove(faturamentoOld);
		}

		@Transactional
		public FaturamentoOld update(FaturamentoOld faturamentoOld) {
			return this.em.merge(faturamentoOld);
		}

		public List<FaturamentoOld> getAll() {
			return this.em.createQuery("SELECT distinct FO FROM FaturamentoOld FO ", FaturamentoOld.class)
					.getResultList();
		}

		public FaturamentoOld findById(Long id) {
			return this.em.find(FaturamentoOld.class, id);
		}
	
}
