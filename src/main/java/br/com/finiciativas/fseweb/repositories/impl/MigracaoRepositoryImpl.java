package br.com.finiciativas.fseweb.repositories.impl;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.produto.ordenacao.OrdemItensTarefa;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FilialServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.OrdemItensTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;

@Repository
public class MigracaoRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ClienteServiceImpl clienteService;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private EtapasConclusaoServiceImpl etapasConclusaoService;

	@Autowired
	private OrdemItensTarefaServiceImpl ordemService;

	@Autowired
	private ItemValoresServiceImpl itemValoresService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private EquipeServiceImpl equipeService;

	@Autowired
	private FilialServiceImpl filialService;

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Autowired
	private SubProducaoServiceImpl subProducaoService;

	public void lerExcel() {
		try {
			File file = new File("\\\\10.33.1.210\\users\\GGS\\Desktop\\Datasource\\Chile\\migracao.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 2017;
			int index = 1;
			for (int i = 4; i < rows; i++) {

				String servico = sheet.getRow(i).getCell(28).getStringCellValue();

				if (servico.equalsIgnoreCase("POSTULACI�N")) {

					XSSFRow row = sheet.getRow(i);
					String rut = sheet.getRow(i).getCell(4).getStringCellValue();

					if (rut != null || rut != "") {

						rut = rut.replaceAll("[^a-zA-Z0-9]+", "");

						Cliente cliente = clienteService.findByRUT(rut);
						List<Contrato> contratos = new ArrayList<Contrato>();

						if (Objects.nonNull(cliente)) {
							contratos = contratoService.getContratoByCliente(cliente.getId());
						}

						if (contratos.size() == 1) {

							System.out.println("Criando a produ��o " + index + " de 1355");
							System.out.println("Empresa: " + cliente.getRazaoSocial());

							Contrato contrato = contratos.get(0);
							Producao producao = new Producao();

							int ano = (int) sheet.getRow(i).getCell(1).getNumericCellValue();
							String sigla = sheet.getRow(i).getCell(12).getStringCellValue();
							String equipe = sheet.getRow(i).getCell(11).getStringCellValue();
							String filial = sheet.getRow(i).getCell(10).getStringCellValue();
							String situacao = sheet.getRow(i).getCell(15).getStringCellValue();
							String razaoInatividade = sheet.getRow(i).getCell(20).getStringCellValue();
							String obs = sheet.getRow(i).getCell(21).getStringCellValue();
							String primeiraCampanha = sheet.getRow(i).getCell(13).getStringCellValue();
							String ultimoC = "";
							String nombreProyecto = sheet.getRow(i).getCell(31).getStringCellValue();
							String nombreSIA = sheet.getRow(i).getCell(41).getStringCellValue();

							try {
								if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(19))) {
									if (sheet.getRow(i).getCell(19).getRawValue() == null
											|| sheet.getRow(i).getCell(19).getCellTypeEnum() == CellType.BLANK) {
										System.out.println("C�LULA EM BRANCO");
									} else {
										DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
										Date date = sheet.getRow(i).getCell(19).getDateCellValue();
										ultimoC = df.format(date);
									}
								}
							} catch (Exception e) {
								System.out.println("Erro ao processar campo, criando valor vazio");
							}

							System.out.println("RUT: " + rut);
							System.out.println("Ano: " + ano);
							System.out.println("Sigla: " + sigla);
							System.out.println("Equipe: " + equipe);
							System.out.println("Filial: " + filial);
							System.out.println("Situa��o: " + situacao);
							System.out.println("Raz�o Sit: " + razaoInatividade);
							System.out.println("Obs: " + obs);
							System.out.println("Primeira? " + primeiraCampanha);
							System.out.println("UltimoC: " + ultimoC);
							System.out.println("nombreProyecto: " + nombreProyecto);

							Consultor consultor = new Consultor();

							if (sigla.equalsIgnoreCase("POR DEFINIR")) {
								consultor = consultorService.findById(4l);
							} else {
								consultor = consultorService.findBySigla(sigla);
							}

							Equipe team = equipeService.findByNome(equipe);
							Filial filia = filialService.findByNome(filial);

							producao.setAno(String.valueOf(ano));
							producao.setConsultor(consultor);
							producao.setEquipe(team);
							producao.setFilial(filia);
							producao.setRazaoInatividade(razaoInatividade);
							producao.setObservacoes(obs);
							producao.setNomeProjeto(nombreProyecto);
							producao.setNomeSIA(nombreSIA);

							if (primeiraCampanha.equalsIgnoreCase("SI")) {
								producao.setTipoDeNegocio("NN");
							} else {
								producao.setTipoDeNegocio("RR");
							}

							producao.setUltimoContato(ultimoC);

							switch (situacao) {
							case "ACTIVO":
								producao.setSituacao(Situacao.ATIVA);
								break;
							case "INACTIVO":
								producao.setSituacao(Situacao.INATIVA);
								break;
							}

							producao.setContrato(contrato);
							producao.setProduto(contrato.getProduto());
							producao.setCliente(contrato.getCliente());

							Producao prod = producaoService.create(producao);

							etapasConclusaoService.createEtapasConclusaoByProd(prod);

							producaoService.createItemValores(prod);

							List<OrdemItensTarefa> ordenacao = ordemService
									.getOrdenacaoByProduto(prod.getProduto().getId());

							ordemService.ordenaEmMassa(ordenacao);

						}
					}
				}
				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void geraModificaiones() {
		try {
			File file = new File("C:\\Users\\GGS\\OneDrive - FI Group\\Escritorio\\migra\\chile.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 2017;

			for (int i = 4; i <= rows; i++) {
				System.out.println("processando linha " + i);

				String servico = sheet.getRow(i).getCell(28).getStringCellValue();
				if (servico.equalsIgnoreCase("POSTULACI�N")) {

					System.out.println("Estou em uma linha de postula��o");

					XSSFRow row = sheet.getRow(i);
					String rut = sheet.getRow(i).getCell(4).getStringCellValue();

					if (rut != null || rut != "") {

						rut = rut.replaceAll("[^a-zA-Z0-9]+", "");

						Cliente cliente = clienteService.findByRUT(rut);

						int ano = (int) sheet.getRow(i).getCell(1).getNumericCellValue();

						if (Objects.nonNull(cliente)) {
							System.out.println(
									"Procurando produ��o do cliente " + cliente.getRazaoSocial() + " do ano " + ano);

							String nombreProyecto = sheet.getRow(i).getCell(31).getStringCellValue();
							String obs = sheet.getRow(i).getCell(23).getStringCellValue();
							String nombreSIA = sheet.getRow(i).getCell(41).getStringCellValue();

							Producao producao = producaoService.getProducaoChileCriteria(String.valueOf(ano), 1l,
									cliente.getId(), obs, nombreProyecto, nombreSIA);

							if (Objects.nonNull(producao)) {
								System.out.println("Produ��o do cliente" + cliente.getRazaoSocial() + " do ano " + ano
										+ " achada");

								List<EtapaTrabalho> etapas = producao.getProduto().getEtapasTrabalho();
								for (EtapaTrabalho etapaTrabalho : etapas) {
									List<Tarefa> tarefas = new ArrayList<Tarefa>();

									if (etapaTrabalho.getId() == 10) {
										tarefas = etapaTrabalho.getTarefas();
										
										for (Tarefa tarefa : tarefas) {
											List<ItemTarefa> itens = tarefa.getItensTarefa();
											
											itemValoresService.createModificaciones(itens, producao, tarefa);
											
										}
										
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Transactional
	public void populaProducoes() {

		try {
			File file = new File("C:\\Users\\GGS\\OneDrive - FI Group\\Escritorio\\migra\\chile.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 2017;

			for (int i = 4; i <= rows; i++) {
				System.out.println("processando linha " + i);

				String servico = sheet.getRow(i).getCell(28).getStringCellValue();
				if (servico.equalsIgnoreCase("POSTULACI�N")) {

					System.out.println("Estou em uma linha de postula��o");

					XSSFRow row = sheet.getRow(i);
					String rut = sheet.getRow(i).getCell(4).getStringCellValue();

					if (rut != null || rut != "") {

						rut = rut.replaceAll("[^a-zA-Z0-9]+", "");

						Cliente cliente = clienteService.findByRUT(rut);

						int ano = (int) sheet.getRow(i).getCell(1).getNumericCellValue();

						if (Objects.nonNull(cliente)) {
							System.out.println(
									"Procurando produ��o do cliente " + cliente.getRazaoSocial() + " do ano " + ano);

							String nombreProyecto = sheet.getRow(i).getCell(31).getStringCellValue();
							String obs = sheet.getRow(i).getCell(23).getStringCellValue();
							String nombreSIA = sheet.getRow(i).getCell(41).getStringCellValue();

							Producao producao = producaoService.getProducaoChileCriteria(String.valueOf(ano), 1l,
									cliente.getId(), obs, nombreProyecto, nombreSIA);

							if (Objects.nonNull(producao)) {
								List<ItemValores> itens = producao.getValores();

								// Atualiza gerais
								String sigla = sheet.getRow(i).getCell(12).getStringCellValue();
								String equipe = sheet.getRow(i).getCell(11).getStringCellValue();
								String filial = sheet.getRow(i).getCell(10).getStringCellValue();

								Consultor consultor = new Consultor();

								if (sigla.equalsIgnoreCase("POR DEFINIR")) {
									consultor = consultorService.findById(4l);
								} else {
									consultor = consultorService.findBySigla(sigla);
								}

								Equipe team = equipeService.findByNome(equipe);
								Filial filia = filialService.findByNome(filial);

								producao.setConsultor(consultor);
								producao.setEquipe(team);
								producao.setFilial(filia);

								producaoService.update(producao);

								for (ItemValores itemValores : itens) {

									switch (itemValores.getItem().getId().intValue()) {
									case 2:
										String numeroSIA = "";
										System.out.println("-- !! -- !! -- Numero estimado SIA:");
										if (sheet.getRow(i).getCell(36).getCellTypeEnum() == CellType.NUMERIC) {
											numeroSIA = String
													.valueOf(sheet.getRow(i).getCell(36).getNumericCellValue());
										}
										itemValores.setValor(numeroSIA);
										itemValoresService.update(itemValores);
										break;
									case 3:
										System.out.println("-- !! -- !! -- Requisitos SIA:");
										String requisitoSIA;
										if (sheet.getRow(i).getCell(37).getStringCellValue().equalsIgnoreCase("SI")) {
											requisitoSIA = "true";
										} else {
											requisitoSIA = "false";
										}
										itemValores.setValor(requisitoSIA);
										itemValoresService.update(itemValores);
										break;
									case 4:
										System.out.println("-- !! -- !! -- SIA Presentada? OK");
										String SIApresentada = sheet.getRow(i).getCell(38).getStringCellValue();
										itemValores.setValor(SIApresentada);
										itemValoresService.update(itemValores);
										break;
									case 5:
										System.out.println("-- !! -- !! -- Estado SIA OK");
										String estadoSIA = sheet.getRow(i).getCell(39).getStringCellValue();
										itemValores.setValor(estadoSIA);
										itemValoresService.update(itemValores);
										break;
									case 6:
										System.out.println("-- !! -- !! -- FECHA de presentaci�n SIA ok");
										String fechaPresentacionSIA = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(40))) {
											if (sheet.getRow(i).getCell(40) == null || sheet.getRow(i).getCell(40)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(40).getDateCellValue();
												fechaPresentacionSIA = df.format(date);
											}
										}
										itemValores.setValor(fechaPresentacionSIA);
										itemValoresService.update(itemValores);
										break;
									case 7:
										System.out.println("-- !! -- !! -- Nombre SIA ok");
										String nomeSIA = sheet.getRow(i).getCell(41).getStringCellValue();
										itemValores.setValor(String.valueOf(nomeSIA));
										itemValoresService.update(itemValores);
										break;
									case 9:
										System.out.println("-- !! -- !! -- C�digo SIA? ok");
										String codigoSIA = sheet.getRow(i).getCell(42).getStringCellValue();
										itemValores.setValor(String.valueOf(codigoSIA));
										itemValoresService.update(itemValores);
										break;
									case 10:
										System.out.println("-- !! -- !! -- Aclaraci�n de documentos SIA?");
										String aclaracionDocumentos;
										if (sheet.getRow(i).getCell(43).getStringCellValue().equalsIgnoreCase("SI")) {
											aclaracionDocumentos = "true";
										} else {
											aclaracionDocumentos = "false";
										}
										itemValores.setValor(aclaracionDocumentos);
										itemValoresService.update(itemValores);
										break;
									case 11:
										System.out.println("-- !! -- !! -- Presentaci�n de documentos?");
										String presentacionDoc = sheet.getRow(i).getCell(44).getStringCellValue();
										itemValores.setValor(String.valueOf(presentacionDoc));
										itemValoresService.update(itemValores);
										break;
									case 12:
										System.out.println("-- !! -- !! -- Fecha de Acogida ok");
										String fechaAcogida = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(45))) {
											if (sheet.getRow(i).getCell(45) == null || sheet.getRow(i).getCell(45)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(45).getDateCellValue();
												fechaAcogida = df.format(date);
											}
										}
										itemValores.setValor(fechaAcogida);
										itemValoresService.update(itemValores);
										break;
									case 13:
										System.out.println("-- !! -- !! -- Fecha vigencia SIA");
										String fechaVigenciaSIA = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(46))) {
											if (sheet.getRow(i).getCell(46) == null || sheet.getRow(i).getCell(46)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(46).getDateCellValue();
												fechaVigenciaSIA = df.format(date);
											}
										}
										itemValores.setValor(fechaVigenciaSIA);
										itemValoresService.update(itemValores);
										break;
									case 14:
										System.out.println("-- !! -- !! -- Fecha reconocimiento gasto");
										String fechaRecoGasto = "";
										try {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(47))) {
												if (sheet.getRow(i).getCell(47) == null || sheet.getRow(i).getCell(47)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(47).getDateCellValue();
													fechaRecoGasto = df.format(date);
												}
											}
										} catch (Exception e) {
											System.out.println("Erro ao processar campo, criando valor vazio");
										}
										itemValores.setValor(fechaRecoGasto);
										itemValoresService.update(itemValores);
										break;
									case 15:
										System.out.println("-- !! -- !! -- Fecha Referencia");
										String fechaReferencia = "";
										try {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(48))) {
												if (sheet.getRow(i).getCell(48) == null || sheet.getRow(i).getCell(48)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(48).getDateCellValue();
													fechaRecoGasto = df.format(date);
												}
											}
										} catch (Exception e) {
											System.out.println("Erro ao processar campo, criando valor vazio");
										}
										itemValores.setValor(fechaReferencia);
										itemValoresService.update(itemValores);
										break;
									case 16:
										System.out.println("-- !! -- !! -- Cruce de datos");
										String cruceDatos = sheet.getRow(i).getCell(49).getStringCellValue();
										itemValores.setValor(cruceDatos);
										itemValoresService.update(itemValores);
										break;
									case 17:
										System.out.println("-- !! -- !! -- Fecha del env�o del cruce de datos");
										String fechaEnvioCruce = "";
										try {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(50))) {
												if (sheet.getRow(i).getCell(50) == null || sheet.getRow(i).getCell(50)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(50).getDateCellValue();
													fechaRecoGasto = df.format(date);
												}
											}
										} catch (Exception e) {
											System.out.println("Erro ao processar campo, criando valor vazio");
										}
										itemValores.setValor(fechaEnvioCruce);
										itemValoresService.update(itemValores);
										break;
									case 20:
										System.out.println("-- !! -- !! -- C�digo del proyecto Certificaci�n");
										String codigoProyectoCert = sheet.getRow(i).getCell(57).getStringCellValue();
										itemValores.setValor(codigoProyectoCert);
										itemValoresService.update(itemValores);
										break;
									case 21:
										System.out.println("-- !! -- !! -- Fecha presentaci�n Certificaci�n");
										String fechaPresentacionCert = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(58))) {
											if (sheet.getRow(i).getCell(58) == null || sheet.getRow(i).getCell(58)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(58).getDateCellValue();
												fechaPresentacionCert = df.format(date);
											}
										}
										itemValores.setValor(fechaPresentacionCert);
										itemValoresService.update(itemValores);
										break;
									case 22:
										System.out.println("-- !! -- !! -- Duraci�n preliminar (Estimada en a�os)");
										String duracionPreliminar = String
												.valueOf(sheet.getRow(i).getCell(59).getNumericCellValue());
										itemValores.setValor(String.valueOf(duracionPreliminar));
										itemValoresService.update(itemValores);
										break;
									case 23:
										System.out.println("-- !! -- !! --  Presupuesto preliminar");
										String presuPreliminar = sheet.getRow(i).getCell(60).getRawValue();
										if (presuPreliminar != null) {
											if (!presuPreliminar.contains(".")) {
												presuPreliminar = presuPreliminar + ".00";
											}
										}
										itemValores.setValor(presuPreliminar);
										itemValoresService.update(itemValores);
										break;
									case 24:
										System.out.println("-- !! -- !! --  Valor estimado a�o 1");
										String valorEstAno1 = sheet.getRow(i).getCell(171).getRawValue();
										if (valorEstAno1 != null) {
											if (!valorEstAno1.contains(".")) {
												valorEstAno1 = valorEstAno1 + ".00";
											}
										}
										itemValores.setValor(valorEstAno1);
										itemValoresService.update(itemValores);
										break;
									case 25:
										System.out.println("-- !! -- !! --  Valor estimado a�o 2");
										String valorEstAno2 = sheet.getRow(i).getCell(172).getRawValue();
										if (valorEstAno2 != null) {
											if (!valorEstAno2.contains(".")) {
												valorEstAno2 = valorEstAno2 + ".00";
											}
										}
										itemValores.setValor(valorEstAno2);
										itemValoresService.update(itemValores);
										break;
									case 26:
										System.out.println("-- !! -- !! --  Valor estimado a�o 3");
										String valorEstAno3 = sheet.getRow(i).getCell(173).getRawValue();
										if (valorEstAno3 != null) {
											if (!valorEstAno3.contains(".")) {
												valorEstAno3 = valorEstAno3 + ".00";
											}
										}
										itemValores.setValor(valorEstAno3);
										itemValoresService.update(itemValores);
										break;
									case 27:
										System.out.println("-- !! -- !! --  Valor estimado a�o 4");
										String valorEstAno4 = sheet.getRow(i).getCell(174).getRawValue();
										if (valorEstAno4 != null) {
											if (!valorEstAno4.contains(".")) {
												valorEstAno4 = valorEstAno4 + ".00";
											}
										}
										itemValores.setValor(valorEstAno4);
										itemValoresService.update(itemValores);
										break;
									case 28:
										System.out.println("-- !! -- !! --  Valor estimado a�o 5");
										String valorEstAno5 = sheet.getRow(i).getCell(175).getRawValue();
										if (valorEstAno5 != null) {
											if (!valorEstAno5.contains(".")) {
												valorEstAno5 = valorEstAno5 + ".00";
											}
										}
										itemValores.setValor(valorEstAno5);
										itemValoresService.update(itemValores);
										break;
									case 29:
										System.out.println("-- !! -- !! --  Valor estimado a�o 6");
										String valorEstAno6 = sheet.getRow(i).getCell(176).getRawValue();
										if (valorEstAno6 != null) {
											if (!valorEstAno6.contains(".")) {
												valorEstAno6 = valorEstAno6 + ".00";
											}
										}
										itemValores.setValor(valorEstAno6);
										itemValoresService.update(itemValores);
										break;
									case 30:
										System.out.println("-- !! -- !! --  Valor estimado a�o 7");
										String valorEstAno7 = sheet.getRow(i).getCell(177).getRawValue();
										if (valorEstAno7 != null) {
											if (!valorEstAno7.contains(".")) {
												valorEstAno7 = valorEstAno7 + ".00";
											}
										}
										itemValores.setValor(valorEstAno7);
										itemValoresService.update(itemValores);
										break;
									case 31:
										System.out.println("-- !! -- !! --  Valor estimado a�o 8");
										String valorEstAno8 = sheet.getRow(i).getCell(178).getRawValue();
										if (valorEstAno8 != null) {
											if (!valorEstAno8.contains(".")) {
												valorEstAno8 = valorEstAno8 + ".00";
											}
										}
										itemValores.setValor(valorEstAno8);
										itemValoresService.update(itemValores);
										break;
									case 32:
										System.out.println("-- !! -- !! --  Valor estimado a�o 9");
										String valorEstAno9 = sheet.getRow(i).getCell(179).getRawValue();
										if (valorEstAno9 != null) {
											if (!valorEstAno9.contains(".")) {
												valorEstAno9 = valorEstAno9 + ".00";
											}
										}
										itemValores.setValor(valorEstAno9);
										itemValoresService.update(itemValores);
										break;
									case 33:
										System.out.println("-- !! -- !! --  Valor estimado a�o 10");
										String valorEstAno10 = sheet.getRow(i).getCell(180).getRawValue();
										if (valorEstAno10 != null) {
											if (!valorEstAno10.contains(".")) {
												valorEstAno10 = valorEstAno10 + ".00";
											}
										}
										itemValores.setValor(valorEstAno10);
										itemValoresService.update(itemValores);
										break;
									case 34:
										System.out.println("-- !! -- !! --  Valor estimado a�o 11");
										String valorEstAno11 = sheet.getRow(i).getCell(181).getRawValue();
										if (valorEstAno11 != null) {
											if (!valorEstAno11.contains(".")) {
												valorEstAno11 = valorEstAno11 + ".00";
											}
										}
										itemValores.setValor(valorEstAno11);
										itemValoresService.update(itemValores);
										break;
									case 35:
										System.out.println("-- !! -- !! --  Valor estimado a�o 12");
										String valorEstAno12 = sheet.getRow(i).getCell(182).getRawValue();
										if (valorEstAno12 != null) {
											if (!valorEstAno12.contains(".")) {
												valorEstAno12 = valorEstAno12 + ".00";
											}
										}
										itemValores.setValor(valorEstAno12);
										itemValoresService.update(itemValores);
										break;
									case 36:
										System.out.println("-- !! -- !! --  Valor estimado a�o 13");
										String valorEstAno13 = sheet.getRow(i).getCell(183).getRawValue();
										if (valorEstAno13 != null) {
											if (!valorEstAno13.contains(".")) {
												valorEstAno13 = valorEstAno13 + ".00";
											}
										}
										itemValores.setValor(valorEstAno13);
										itemValoresService.update(itemValores);
										break;
									case 37:
										System.out.println("-- !! -- !! --  Valor estimado a�o 14");
										String valorEstAno14 = sheet.getRow(i).getCell(184).getRawValue();
										if (valorEstAno14 != null) {
											if (!valorEstAno14.contains(".")) {
												valorEstAno14 = valorEstAno14 + ".00";
											}
										}
										itemValores.setValor(valorEstAno14);
										itemValoresService.update(itemValores);
										break;
									case 38:
										System.out.println("-- !! -- !! --  Valor estimado a�o 15");
										String valorEstAno15 = sheet.getRow(i).getCell(185).getRawValue();
										if (valorEstAno15 != null) {
											if (!valorEstAno15.contains(".")) {
												valorEstAno15 = valorEstAno15 + ".00";
											}
										}
										itemValores.setValor(valorEstAno15);
										itemValoresService.update(itemValores);
										break;
									case 39:
										System.out.println("-- !! -- !! --  Valor estimado a�o 16");
										String valorEstAno16 = sheet.getRow(i).getCell(186).getRawValue();
										if (valorEstAno16 != null) {
											if (!valorEstAno16.contains(".")) {
												valorEstAno16 = valorEstAno16 + ".00";
											}
										}
										itemValores.setValor(valorEstAno16);
										itemValoresService.update(itemValores);
										break;
									case 40:
										System.out.println("-- !! -- !! --  Valor estimado a�o 17");
										String valorEstAno17 = sheet.getRow(i).getCell(187).getRawValue();
										if (valorEstAno17 != null) {
											if (!valorEstAno17.contains(".")) {
												valorEstAno17 = valorEstAno17 + ".00";
											}
										}
										itemValores.setValor(valorEstAno17);
										itemValoresService.update(itemValores);
										break;
									case 41:
										System.out.println("-- !! -- !! --  Valor estimado a�o 18");
										String valorEstAno18 = sheet.getRow(i).getCell(188).getRawValue();
										if (valorEstAno18 != null) {
											if (!valorEstAno18.contains(".")) {
												valorEstAno18 = valorEstAno18 + ".00";
											}
										}
										itemValores.setValor(valorEstAno18);
										itemValoresService.update(itemValores);
										break;
									case 42:
										System.out.println("-- !! -- !! --  Valor estimado a�o 19");
										String valorEstAno19 = sheet.getRow(i).getCell(189).getRawValue();
										if (valorEstAno19 != null) {
											if (!valorEstAno19.contains(".")) {
												valorEstAno19 = valorEstAno19 + ".00";
											}
										}
										itemValores.setValor(valorEstAno19);
										itemValoresService.update(itemValores);
										break;
									case 43:
										System.out.println("-- !! -- !! --  Valor estimado a�o 20");
										String valorEstAno20 = sheet.getRow(i).getCell(190).getRawValue();
										if (valorEstAno20 != null) {
											if (!valorEstAno20.contains(".")) {
												valorEstAno20 = valorEstAno20 + ".00";
											}
										}
										itemValores.setValor(valorEstAno20);
										itemValoresService.update(itemValores);
										break;
									case 44:
										System.out.println("-- !! -- !! --  Valor estimado a�o 21");
										String valorEstAno21 = sheet.getRow(i).getCell(191).getRawValue();
										if (valorEstAno21 != null) {
											if (!valorEstAno21.contains(".")) {
												valorEstAno21 = valorEstAno21 + ".00";
											}
										}
										itemValores.setValor(valorEstAno21);
										break;
									case 45:
										System.out.println("-- !! -- !! --  Valor estimado a�o 22");
										String valorEstAno22 = sheet.getRow(i).getCell(192).getRawValue();
										if (valorEstAno22 != null) {
											if (!valorEstAno22.contains(".")) {
												valorEstAno22 = valorEstAno22 + ".00";
											}
										}
										itemValores.setValor(valorEstAno22);
										break;
									case 46:
										System.out.println("-- !! -- !! --  Valor estimado a�o 23");
										String valorEstAno23 = sheet.getRow(i).getCell(193).getRawValue();
										if (valorEstAno23 != null) {
											if (!valorEstAno23.contains(".")) {
												valorEstAno22 = valorEstAno23 + ".00";
											}
										}
										itemValores.setValor(valorEstAno23);
										break;
									case 47:
										System.out.println("-- !! -- !! --  Valor estimado a�o 24");
										String valorEstAno24 = sheet.getRow(i).getCell(194).getRawValue();
										if (valorEstAno24 != null) {
											if (!valorEstAno24.contains(".")) {
												valorEstAno24 = valorEstAno24 + ".00";
											}
										}
										itemValores.setValor(valorEstAno24);
										break;
									case 48:
										System.out.println("-- !! -- !! --  Valor estimado a�o 25");
										String valorEstAno25 = sheet.getRow(i).getCell(195).getRawValue();
										if (valorEstAno25 != null) {
											if (!valorEstAno25.contains(".")) {
												valorEstAno25 = valorEstAno25 + ".00";
											}
										}
										itemValores.setValor(valorEstAno25);
										break;
									case 49:
										System.out.println("-- !! -- !! -- Carta Gantt de proyecto");
										String cartaGanttProyecto;
										if (sheet.getRow(i).getCell(64).getStringCellValue().equalsIgnoreCase("si")) {
											cartaGanttProyecto = "true";
										} else {
											cartaGanttProyecto = "false";
										}
										itemValores.setValor(cartaGanttProyecto);
										itemValoresService.update(itemValores);
										break;
									case 50:
										System.out.println("-- !! -- !! -- Fecha de Inicio");
										String fechaInicio = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(65))) {
											if (sheet.getRow(i).getCell(65) == null || sheet.getRow(i).getCell(65)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(65).getDateCellValue();
												fechaInicio = df.format(date);
											}
										}
										itemValores.setValor(fechaInicio);
										itemValoresService.update(itemValores);
										break;
									case 51:
										System.out.println("-- !! -- !! -- Fecha de termino");
										String fechaTermino = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(67))) {
											if (sheet.getRow(i).getCell(67) == null || sheet.getRow(i).getCell(67)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(67).getDateCellValue();
												fechaTermino = df.format(date);
											}
										}
										itemValores.setValor(fechaTermino);
										itemValoresService.update(itemValores);
										break;
									case 52:
										System.out.println("-- !! -- !! --  Duraci�n del proyecto en meses");
										String durancionMeses = String
												.valueOf(sheet.getRow(i).getCell(68).getNumericCellValue());
										itemValores.setValor(durancionMeses);
										itemValoresService.update(itemValores);
										break;
									case 53:
										System.out.println("-- !! -- !! -- Total de campa�as trabajadas");
										String totalCampanasTrab = "";
										try {
											totalCampanasTrab = String
													.valueOf(sheet.getRow(i).getCell(69).getNumericCellValue());
										} catch (Exception e) {
											System.out.println("Campo com erro, criando valor vazio");
										}
										itemValores.setValor(String.valueOf(totalCampanasTrab));
										itemValoresService.update(itemValores);
										break;
									case 54:
										System.out.println("-- !! -- !! -- Levantamiento del presupuesto del proyecto");
										String levantamientoPresupuesto = "";
										try {
											if (sheet.getRow(i).getCell(70).getStringCellValue()
													.equalsIgnoreCase("SI")) {
												levantamientoPresupuesto = "true";
											} else {
												levantamientoPresupuesto = "false";
											}
										} catch (Exception e) {
											System.out.println("Campo com erro, criando valor vazio");
										}
										itemValores.setValor(levantamientoPresupuesto);
										itemValoresService.update(itemValores);
										break;
									case 55:
										System.out.println("-- !! -- !! -- Carta Gantt de RRHH");
										String cartaGanttRH = "";
										try {
											if (sheet.getRow(i).getCell(71).getStringCellValue()
													.equalsIgnoreCase("SI")) {
												cartaGanttRH = "true";
											} else {
												cartaGanttRH = "false";
											}
										} catch (Exception e) {
											System.out.println("Campo com erro, criando valor vazio");
										}
										itemValores.setValor(cartaGanttRH);
										itemValoresService.update(itemValores);
										break;
									case 56:
										System.out.println("-- !! -- !! -- Presupuesto de RRHH");
										String presupuestoRH = sheet.getRow(i).getCell(72).getRawValue();
										if (presupuestoRH != null) {
											if (!presupuestoRH.contains(".")) {
												presupuestoRH = presupuestoRH + ".00";
											}
										}
										itemValores.setValor(presupuestoRH);
										itemValoresService.update(itemValores);
										break;
									case 57:
										System.out.println("-- !! -- !! -- Presupuesto Gastos Directos");
										String presupuestoGastoDirectos = sheet.getRow(i).getCell(73).getRawValue();
										if (presupuestoGastoDirectos != null) {
											if (!presupuestoGastoDirectos.contains(".")) {
												presupuestoGastoDirectos = presupuestoGastoDirectos + ".00";
											}
										}
										itemValores.setValor(presupuestoGastoDirectos);
										itemValoresService.update(itemValores);
										break;
									case 58:
										System.out.println("-- !! -- !! -- Presupuesto de Subcontratos");
										String presupuestoSubcontratos = sheet.getRow(i).getCell(74).getRawValue();
										if (presupuestoSubcontratos != null) {
											if (!presupuestoSubcontratos.contains(".")) {
												presupuestoSubcontratos = presupuestoSubcontratos + ".00";
											}
										}
										itemValores.setValor(presupuestoSubcontratos);
										itemValoresService.update(itemValores);
										break;
									case 59:
										System.out.println("-- !! -- !! -- Presupuestos de Inversi�n");
										String presupuestoInversion = sheet.getRow(i).getCell(75).getRawValue();
										if (presupuestoInversion != null) {
											if (!presupuestoInversion.contains(".")) {
												presupuestoInversion = presupuestoInversion + ".00";
											}
										}
										itemValores.setValor(presupuestoInversion);
										itemValoresService.update(itemValores);
										break;
									case 60:
										System.out.println("-- !! -- !! -- Presupuesto Total a postular");
										String presupuestoTotalPostular = sheet.getRow(i).getCell(76).getRawValue();
										if (presupuestoTotalPostular != null) {
											if (!presupuestoTotalPostular.contains(".")) {
												presupuestoTotalPostular = presupuestoTotalPostular + ".00";
											}
										}
										itemValores.setValor(presupuestoTotalPostular);
										itemValoresService.update(itemValores);
										break;
									case 61:
//										System.out.println("-- !! -- !! -- �Presupuesto enviado a cliente?");
//										String enviadoCliente = "";
//										if (sheet.getRow(i).getCell(63).getCellTypeEnum() == CellType.STRING) {
//											System.out.println(sheet.getRow(i).getCell(63).getStringCellValue());
//										} else {
//											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(63))) {
//												if (sheet.getRow(i).getCell(63) == null || sheet.getRow(i).getCell(63)
//														.getCellTypeEnum() == CellType.BLANK) {
//													System.out.println("C�LULA EM BRANCO");
//												} else {
//													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//													Date date = sheet.getRow(i).getCell(63).getDateCellValue();
//													enviadoCliente = df.format(date);
//												}
//											}
//										}
//										itemValores.setValor(enviadoCliente);
//										itemValoresService.update(itemValores);
//										break;
									case 62:
										System.out.println("-- !! -- !! -- �Presupuesto validado por cliente?");
										String validadoCliente = "";
										try {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(78))) {
												if (sheet.getRow(i).getCell(78) == null || sheet.getRow(i).getCell(78)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(78).getDateCellValue();
													validadoCliente = df.format(date);
												}
											}
										} catch (Exception e) {
											System.out.println("Campo com erro, criando valor vazio");
										}
										itemValores.setValor(validadoCliente);
										itemValoresService.update(itemValores);
										break;
									case 63:
										System.out.println("-- !! -- !! --  Presupuesto a�o 1");
										String presupuestoAno1;
										if (sheet.getRow(i).getCell(79).getRawValue() != null
												&& sheet.getRow(i).getCell(79).getRawValue() != ""
												&& sheet.getRow(i).getCell(79).getNumericCellValue() != 0.0) {
											presupuestoAno1 = sheet.getRow(i).getCell(79).getRawValue();
											if (presupuestoAno1.contains(".")) {
												String subStr = presupuestoAno1.substring(presupuestoAno1.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno1 = presupuestoAno1.replace(
															presupuestoAno1.substring(presupuestoAno1.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno1 = presupuestoAno1 + ".00";
											}
										} else {
											presupuestoAno1 = "";
										}
										itemValores.setValor(presupuestoAno1);
										itemValoresService.update(itemValores);
										break;
									case 64:
										System.out.println("-- !! -- !! --  Presupuesto a�o 2");
										String presupuestoAno2;
										if (sheet.getRow(i).getCell(80).getRawValue() != null
												&& sheet.getRow(i).getCell(80).getRawValue() != ""
												&& sheet.getRow(i).getCell(80).getNumericCellValue() != 0.0) {
											presupuestoAno2 = sheet.getRow(i).getCell(80).getRawValue();
											if (presupuestoAno2.contains(".")) {
												String subStr = presupuestoAno2.substring(presupuestoAno2.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno2 = presupuestoAno2.replace(
															presupuestoAno2.substring(presupuestoAno2.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno2 = presupuestoAno2 + ".00";
											}
										} else {
											presupuestoAno2 = "";
										}
										itemValores.setValor(presupuestoAno2);
										itemValoresService.update(itemValores);
										break;
									case 65:
										System.out.println("-- !! -- !! --  Presupuesto a�o 3");
										String presupuestoAno3;
										if (sheet.getRow(i).getCell(81).getRawValue() != null
												&& sheet.getRow(i).getCell(81).getRawValue() != ""
												&& sheet.getRow(i).getCell(81).getNumericCellValue() != 0.0) {
											presupuestoAno3 = sheet.getRow(i).getCell(81).getRawValue();
											if (presupuestoAno3.contains(".")) {
												String subStr = presupuestoAno3.substring(presupuestoAno3.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno3 = presupuestoAno3.replace(
															presupuestoAno3.substring(presupuestoAno3.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno3 = presupuestoAno3 + ".00";
											}
										} else {
											presupuestoAno3 = "";
										}
										itemValores.setValor(presupuestoAno3);
										itemValoresService.update(itemValores);
										break;
									case 66:
										System.out.println("-- !! -- !! --  Presupuesto a�o 4");
										String presupuestoAno4;
										if (sheet.getRow(i).getCell(82).getRawValue() != null
												&& sheet.getRow(i).getCell(82).getRawValue() != ""
												&& sheet.getRow(i).getCell(82).getNumericCellValue() != 0.0) {
											presupuestoAno4 = sheet.getRow(i).getCell(82).getRawValue();
											if (presupuestoAno4.contains(".")) {
												String subStr = presupuestoAno4.substring(presupuestoAno4.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno4 = presupuestoAno4.replace(
															presupuestoAno4.substring(presupuestoAno4.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno4 = presupuestoAno4 + ".00";
											}
										} else {
											presupuestoAno4 = "";
										}
										itemValores.setValor(presupuestoAno4);
										itemValoresService.update(itemValores);
									case 67:
										System.out.println("-- !! -- !! --  Presupuesto a�o 5");
										String presupuestoAno5;
										if (sheet.getRow(i).getCell(83).getRawValue() != null
												&& sheet.getRow(i).getCell(83).getRawValue() != ""
												&& sheet.getRow(i).getCell(83).getNumericCellValue() != 0.0) {
											presupuestoAno5 = sheet.getRow(i).getCell(83).getRawValue();
											if (presupuestoAno5.contains(".")) {
												String subStr = presupuestoAno5.substring(presupuestoAno5.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno5 = presupuestoAno5.replace(
															presupuestoAno5.substring(presupuestoAno5.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno5 = presupuestoAno5 + ".00";
											}
										} else {
											presupuestoAno5 = "";
										}
										itemValores.setValor(presupuestoAno5);
										itemValoresService.update(itemValores);
										break;
									case 68:
										System.out.println("-- !! -- !! --  Presupuesto a�o 6");
										String presupuestoAno6;
										if (sheet.getRow(i).getCell(84).getRawValue() != null
												&& sheet.getRow(i).getCell(84).getRawValue() != ""
												&& sheet.getRow(i).getCell(84).getNumericCellValue() != 0.0) {
											presupuestoAno6 = sheet.getRow(i).getCell(84).getRawValue();
											if (presupuestoAno6.contains(".")) {
												String subStr = presupuestoAno6.substring(presupuestoAno6.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno6 = presupuestoAno6.replace(
															presupuestoAno6.substring(presupuestoAno6.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno6 = presupuestoAno6 + ".00";
											}
										} else {
											presupuestoAno6 = "";
										}
										itemValores.setValor(presupuestoAno6);
										itemValoresService.update(itemValores);
										break;
									case 69:
										System.out.println("-- !! -- !! --  Presupuesto a�o 7");
										String presupuestoAno7;
										if (sheet.getRow(i).getCell(85).getRawValue() != null
												&& sheet.getRow(i).getCell(85).getRawValue() != ""
												&& sheet.getRow(i).getCell(85).getNumericCellValue() != 0.0) {
											presupuestoAno7 = sheet.getRow(i).getCell(85).getRawValue();
											if (presupuestoAno7.contains(".")) {
												String subStr = presupuestoAno7.substring(presupuestoAno7.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno7 = presupuestoAno7.replace(
															presupuestoAno7.substring(presupuestoAno7.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno7 = presupuestoAno7 + ".00";
											}
										} else {
											presupuestoAno7 = "";
										}
										itemValores.setValor(presupuestoAno7);
										itemValoresService.update(itemValores);
										break;
									case 70:
										System.out.println("-- !! -- !! --  Presupuesto a�o 8");
										String presupuestoAno8;
										if (sheet.getRow(i).getCell(86).getRawValue() != null
												&& sheet.getRow(i).getCell(86).getRawValue() != ""
												&& sheet.getRow(i).getCell(86).getNumericCellValue() != 0.0) {
											presupuestoAno8 = sheet.getRow(i).getCell(86).getRawValue();
											if (presupuestoAno8.contains(".")) {
												String subStr = presupuestoAno8.substring(presupuestoAno8.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno8 = presupuestoAno8.replace(
															presupuestoAno8.substring(presupuestoAno8.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno8 = presupuestoAno8 + ".00";
											}
										} else {
											presupuestoAno8 = "";
										}
										itemValores.setValor(presupuestoAno8);
										itemValoresService.update(itemValores);
										break;
									case 71:
										System.out.println("-- !! -- !! --  Presupuesto a�o 9");
										String presupuestoAno9;
										if (sheet.getRow(i).getCell(87).getRawValue() != null
												&& sheet.getRow(i).getCell(87).getRawValue() != ""
												&& sheet.getRow(i).getCell(87).getNumericCellValue() != 0.0) {
											presupuestoAno9 = sheet.getRow(i).getCell(87).getRawValue();
											if (presupuestoAno9.contains(".")) {
												String subStr = presupuestoAno9.substring(presupuestoAno9.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno9 = presupuestoAno9.replace(
															presupuestoAno9.substring(presupuestoAno9.indexOf(".") + 3),
															"");
												}
											} else {
												presupuestoAno9 = presupuestoAno9 + ".00";
											}
										} else {
											presupuestoAno9 = "";
										}
										itemValores.setValor(presupuestoAno9);
										itemValoresService.update(itemValores);
										break;
									case 72:
										System.out.println("-- !! -- !! --  Presupuesto a�o 10");
										String presupuestoAno10;
										if (sheet.getRow(i).getCell(88).getRawValue() != null
												&& sheet.getRow(i).getCell(88).getRawValue() != ""
												&& sheet.getRow(i).getCell(88).getNumericCellValue() != 0.0) {
											presupuestoAno10 = sheet.getRow(i).getCell(88).getRawValue();
											if (presupuestoAno10.contains(".")) {
												String subStr = presupuestoAno10
														.substring(presupuestoAno10.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno10 = presupuestoAno10.replace(presupuestoAno10
															.substring(presupuestoAno10.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno10 = presupuestoAno10 + ".00";
											}
										} else {
											presupuestoAno10 = "";
										}
										itemValores.setValor(presupuestoAno10);
										itemValoresService.update(itemValores);
										break;
									case 73:
										System.out.println("-- !! -- !! --  Presupuesto a�o 11");
										String presupuestoAno11;
										if (sheet.getRow(i).getCell(89).getRawValue() != null
												&& sheet.getRow(i).getCell(89).getRawValue() != ""
												&& sheet.getRow(i).getCell(89).getNumericCellValue() != 0.0) {
											presupuestoAno11 = sheet.getRow(i).getCell(89).getRawValue();
											if (presupuestoAno11.contains(".")) {
												String subStr = presupuestoAno11
														.substring(presupuestoAno11.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno11 = presupuestoAno11.replace(presupuestoAno11
															.substring(presupuestoAno11.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno11 = presupuestoAno11 + ".00";
											}
										} else {
											presupuestoAno11 = "";
										}
										itemValores.setValor(presupuestoAno11);
										itemValoresService.update(itemValores);
										break;
									case 74:
										System.out.println("-- !! -- !! --  Presupuesto a�o 12");
										String presupuestoAno12;
										if (sheet.getRow(i).getCell(90).getRawValue() != null
												&& sheet.getRow(i).getCell(90).getRawValue() != ""
												&& sheet.getRow(i).getCell(90).getNumericCellValue() != 0.0) {
											presupuestoAno12 = sheet.getRow(i).getCell(90).getRawValue();
											if (presupuestoAno12.contains(".")) {
												String subStr = presupuestoAno12
														.substring(presupuestoAno12.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno12 = presupuestoAno12.replace(presupuestoAno12
															.substring(presupuestoAno12.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno12 = presupuestoAno12 + ".00";
											}
										} else {
											presupuestoAno12 = "";
										}
										itemValores.setValor(presupuestoAno12);
										itemValoresService.update(itemValores);
										break;
									case 75:
										System.out.println("-- !! -- !! --  Presupuesto a�o 13");
										String presupuestoAno13;
										if (sheet.getRow(i).getCell(91).getRawValue() != null
												&& sheet.getRow(i).getCell(91).getRawValue() != ""
												&& sheet.getRow(i).getCell(91).getNumericCellValue() != 0.0) {
											presupuestoAno13 = sheet.getRow(i).getCell(91).getRawValue();
											if (presupuestoAno13.contains(".")) {
												String subStr = presupuestoAno13
														.substring(presupuestoAno13.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno13 = presupuestoAno13.replace(presupuestoAno13
															.substring(presupuestoAno13.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno13 = presupuestoAno13 + ".00";
											}
										} else {
											presupuestoAno13 = "";
										}
										itemValores.setValor(presupuestoAno13);
										itemValoresService.update(itemValores);
										break;
									case 76:
										System.out.println("-- !! -- !! --  Presupuesto a�o 14");
										String presupuestoAno14;
										if (sheet.getRow(i).getCell(92).getRawValue() != null
												&& sheet.getRow(i).getCell(92).getRawValue() != ""
												&& sheet.getRow(i).getCell(92).getNumericCellValue() != 0.0) {
											presupuestoAno14 = sheet.getRow(i).getCell(92).getRawValue();
											if (presupuestoAno14.contains(".")) {
												String subStr = presupuestoAno14
														.substring(presupuestoAno14.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno14 = presupuestoAno14.replace(presupuestoAno14
															.substring(presupuestoAno14.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno14 = presupuestoAno14 + ".00";
											}
										} else {
											presupuestoAno14 = "";
										}
										itemValores.setValor(presupuestoAno14);
										itemValoresService.update(itemValores);
										break;
									case 77:
										System.out.println("-- !! -- !! --  Presupuesto a�o 15");
										String presupuestoAno15;
										if (sheet.getRow(i).getCell(93).getRawValue() != null
												&& sheet.getRow(i).getCell(93).getRawValue() != ""
												&& sheet.getRow(i).getCell(93).getNumericCellValue() != 0.0) {
											presupuestoAno15 = sheet.getRow(i).getCell(93).getRawValue();
											if (presupuestoAno15.contains(".")) {
												String subStr = presupuestoAno15
														.substring(presupuestoAno15.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno15 = presupuestoAno15.replace(presupuestoAno15
															.substring(presupuestoAno15.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno15 = presupuestoAno15 + ".00";
											}
										} else {
											presupuestoAno15 = "";
										}
										itemValores.setValor(presupuestoAno15);
										itemValoresService.update(itemValores);
										break;
									case 78:
										System.out.println("-- !! -- !! --  Presupuesto a�o 16");
										String presupuestoAno16;
										if (sheet.getRow(i).getCell(94).getRawValue() != null
												&& sheet.getRow(i).getCell(94).getRawValue() != ""
												&& sheet.getRow(i).getCell(94).getNumericCellValue() != 0.0) {
											presupuestoAno16 = sheet.getRow(i).getCell(94).getRawValue();
											if (presupuestoAno16.contains(".")) {
												String subStr = presupuestoAno16
														.substring(presupuestoAno16.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno16 = presupuestoAno16.replace(presupuestoAno16
															.substring(presupuestoAno16.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno16 = presupuestoAno16 + ".00";
											}
										} else {
											presupuestoAno16 = "";
										}
										itemValores.setValor(presupuestoAno16);
										itemValoresService.update(itemValores);
										break;
									case 79:
										System.out.println("-- !! -- !! --  Presupuesto a�o 17");
										String presupuestoAno17;
										if (sheet.getRow(i).getCell(95).getRawValue() != null
												&& sheet.getRow(i).getCell(95).getRawValue() != ""
												&& sheet.getRow(i).getCell(95).getNumericCellValue() != 0.0) {
											presupuestoAno17 = sheet.getRow(i).getCell(95).getRawValue();
											if (presupuestoAno17.contains(".")) {
												String subStr = presupuestoAno17
														.substring(presupuestoAno17.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno17 = presupuestoAno17.replace(presupuestoAno17
															.substring(presupuestoAno17.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno17 = presupuestoAno17 + ".00";
											}
										} else {
											presupuestoAno17 = "";
										}
										itemValores.setValor(presupuestoAno17);
										itemValoresService.update(itemValores);
										break;
									case 80:
										System.out.println("-- !! -- !! --  Presupuesto a�o 18");
										String presupuestoAno18;
										if (sheet.getRow(i).getCell(96).getRawValue() != null
												&& sheet.getRow(i).getCell(96).getRawValue() != ""
												&& sheet.getRow(i).getCell(96).getNumericCellValue() != 0.0) {
											presupuestoAno18 = sheet.getRow(i).getCell(96).getRawValue();
											if (presupuestoAno18.contains(".")) {
												String subStr = presupuestoAno18
														.substring(presupuestoAno18.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno18 = presupuestoAno18.replace(presupuestoAno18
															.substring(presupuestoAno18.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno18 = presupuestoAno18 + ".00";
											}
										} else {
											presupuestoAno18 = "";
										}
										itemValores.setValor(presupuestoAno18);
										itemValoresService.update(itemValores);
										break;
									case 81:
										System.out.println("-- !! -- !! --  Presupuesto a�o 19");
										String presupuestoAno19;
										if (sheet.getRow(i).getCell(97).getRawValue() != null
												&& sheet.getRow(i).getCell(97).getRawValue() != ""
												&& sheet.getRow(i).getCell(97).getNumericCellValue() != 0.0) {
											presupuestoAno19 = sheet.getRow(i).getCell(97).getRawValue();
											if (presupuestoAno19.contains(".")) {
												String subStr = presupuestoAno19
														.substring(presupuestoAno19.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno19 = presupuestoAno19.replace(presupuestoAno19
															.substring(presupuestoAno19.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno19 = presupuestoAno19 + ".00";
											}
										} else {
											presupuestoAno19 = "";
										}
										itemValores.setValor(presupuestoAno19);
										itemValoresService.update(itemValores);
										break;
									case 82:
										System.out.println("-- !! -- !! --  Presupuesto a�o 20");
										String presupuestoAno20;
										if (sheet.getRow(i).getCell(98).getRawValue() != null
												&& sheet.getRow(i).getCell(98).getRawValue() != ""
												&& sheet.getRow(i).getCell(98).getNumericCellValue() != 0.0) {
											presupuestoAno20 = sheet.getRow(i).getCell(98).getRawValue();
											if (presupuestoAno20.contains(".")) {
												String subStr = presupuestoAno20
														.substring(presupuestoAno20.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno20 = presupuestoAno20.replace(presupuestoAno20
															.substring(presupuestoAno20.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno20 = presupuestoAno20 + ".00";
											}
										} else {
											presupuestoAno20 = "";
										}
										itemValores.setValor(presupuestoAno20);
										itemValoresService.update(itemValores);
										break;
									case 83:
										System.out.println("-- !! -- !! --  Presupuesto a�o 21");
										String presupuestoAno21;
										if (sheet.getRow(i).getCell(99).getRawValue() != null
												&& sheet.getRow(i).getCell(99).getRawValue() != ""
												&& sheet.getRow(i).getCell(99).getNumericCellValue() != 0.0) {
											presupuestoAno21 = sheet.getRow(i).getCell(99).getRawValue();
											if (presupuestoAno21.contains(".")) {
												String subStr = presupuestoAno21
														.substring(presupuestoAno21.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno21 = presupuestoAno21.replace(presupuestoAno21
															.substring(presupuestoAno21.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno21 = presupuestoAno21 + ".00";
											}
										} else {
											presupuestoAno21 = "";
										}
										itemValores.setValor(presupuestoAno21);
										itemValoresService.update(itemValores);
										break;
									case 84:
										System.out.println("-- !! -- !! --  Presupuesto a�o 22");
										String presupuestoAno22;
										if (sheet.getRow(i).getCell(100).getRawValue() != null
												&& sheet.getRow(i).getCell(100).getRawValue() != ""
												&& sheet.getRow(i).getCell(100).getNumericCellValue() != 0.0) {
											presupuestoAno22 = sheet.getRow(i).getCell(100).getRawValue();
											if (presupuestoAno22.contains(".")) {
												String subStr = presupuestoAno22
														.substring(presupuestoAno22.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno22 = presupuestoAno22.replace(presupuestoAno22
															.substring(presupuestoAno22.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno22 = presupuestoAno22 + ".00";
											}
										} else {
											presupuestoAno22 = "";
										}
										itemValores.setValor(presupuestoAno22);
										itemValoresService.update(itemValores);
										break;
									case 85:
										System.out.println("-- !! -- !! --  Presupuesto a�o 23");
										String presupuestoAno23;
										if (sheet.getRow(i).getCell(101).getRawValue() != null
												&& sheet.getRow(i).getCell(101).getRawValue() != ""
												&& sheet.getRow(i).getCell(101).getNumericCellValue() != 0.0) {
											presupuestoAno23 = sheet.getRow(i).getCell(101).getRawValue();
											if (presupuestoAno23.contains(".")) {
												String subStr = presupuestoAno23
														.substring(presupuestoAno23.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno22 = presupuestoAno23.replace(presupuestoAno23
															.substring(presupuestoAno23.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno23 = presupuestoAno23 + ".00";
											}
										} else {
											presupuestoAno23 = "";
										}
										itemValores.setValor(presupuestoAno23);
										itemValoresService.update(itemValores);
										break;
									case 86:
										System.out.println("-- !! -- !! --  Presupuesto a�o 24");
										String presupuestoAno24;
										if (sheet.getRow(i).getCell(102).getRawValue() != null
												&& sheet.getRow(i).getCell(102).getRawValue() != ""
												&& sheet.getRow(i).getCell(102).getNumericCellValue() != 0.0) {
											presupuestoAno24 = sheet.getRow(i).getCell(102).getRawValue();
											if (presupuestoAno24.contains(".")) {
												String subStr = presupuestoAno24
														.substring(presupuestoAno24.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno24 = presupuestoAno24.replace(presupuestoAno24
															.substring(presupuestoAno24.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno24 = presupuestoAno24 + ".00";
											}
										} else {
											presupuestoAno24 = "";
										}
										itemValores.setValor(presupuestoAno24);
										itemValoresService.update(itemValores);
										break;
									case 87:
										System.out.println("-- !! -- !! --  Presupuesto a�o 25");
										String presupuestoAno25;
										if (sheet.getRow(i).getCell(103).getRawValue() != null
												&& sheet.getRow(i).getCell(103).getRawValue() != ""
												&& sheet.getRow(i).getCell(103).getNumericCellValue() != 0.0) {
											presupuestoAno25 = sheet.getRow(i).getCell(103).getRawValue();
											if (presupuestoAno25.contains(".")) {
												String subStr = presupuestoAno25
														.substring(presupuestoAno25.indexOf("."));
												if (subStr.length() > 2) {
													presupuestoAno25 = presupuestoAno25.replace(presupuestoAno25
															.substring(presupuestoAno25.indexOf(".") + 3), "");
												}
											} else {
												presupuestoAno25 = presupuestoAno25 + ".00";
											}
										} else {
											presupuestoAno25 = "";
										}
										itemValores.setValor(presupuestoAno25);
										itemValoresService.update(itemValores);
										break;
									case 88:
										System.out.println("-- !! -- !! -- Personer�a Jur�dica");
										String personJuridica = sheet.getRow(i).getCell(104).getStringCellValue();
										itemValores.setValor(personJuridica);
										itemValoresService.update(itemValores);
										break;
									case 89:
										System.out.println("-- !! -- !! -- Sistema Financiero Contable");
										String sistemaFinanceiroContable = sheet.getRow(i).getCell(105)
												.getStringCellValue();
										itemValores.setValor(sistemaFinanceiroContable);
										itemValoresService.update(itemValores);
										break;
									case 90:
										System.out.println("-- !! -- !! --  Declaraci�n Jurada");
										String declaracionJurada = sheet.getRow(i).getCell(106).getStringCellValue();
										itemValores.setValor(declaracionJurada);
										itemValoresService.update(itemValores);
										break;
									case 91:
										System.out.println(
												"-- !! -- !! --  Contrato de persona que acredita capacidades");
										String contratoPersonCapacidades = sheet.getRow(i).getCell(107)
												.getStringCellValue();
										itemValores.setValor(contratoPersonCapacidades);
										itemValoresService.update(itemValores);
										break;
									case 92:
										System.out
												.println("-- !! -- !! --  T�tulo de persona que acredita capacidades");
										String tituloPersonaCapacidades = sheet.getRow(i).getCell(108)
												.getStringCellValue();
										itemValores.setValor(tituloPersonaCapacidades);
										itemValoresService.update(itemValores);
										break;
									case 93:
										System.out.println("-- !! -- !! --  Pago Arancel de postulaci�n");
										String pagoArancel = sheet.getRow(i).getCell(109).getStringCellValue();
										itemValores.setValor(pagoArancel);
										itemValoresService.update(itemValores);
										break;
									case 94:
										System.out.println("-- !! -- !! --  Formulario T�cnico");
										String formularioTecnico = sheet.getRow(i).getCell(111).getStringCellValue();
										itemValores.setValor(formularioTecnico);
										itemValoresService.update(itemValores);
										break;
									case 95:
										System.out.println("-- !! -- !! --  Diagrama de soluci�n");
										String diagramaSolucion = sheet.getRow(i).getCell(112).getStringCellValue();
										itemValores.setValor(diagramaSolucion);
										itemValoresService.update(itemValores);
										break;
									case 96:
										System.out.println("-- !! -- !! --  Desafios, presupuesto y plan de trabajo");
										String desafiosPlanoTrabajo = sheet.getRow(i).getCell(114).getStringCellValue();
										itemValores.setValor(desafiosPlanoTrabajo);
										itemValoresService.update(itemValores);
										break;
									case 97:
										System.out.println("-- !! -- !! --  Capacidades materiales");
										String capacidadesMateriales = sheet.getRow(i).getCell(115)
												.getStringCellValue();
										itemValores.setValor(capacidadesMateriales);
										itemValoresService.update(itemValores);
										break;
									case 98:
										System.out.println("-- !! -- !! --  Planilla de profesionales");
										String planillaProfesionales = sheet.getRow(i).getCell(116)
												.getStringCellValue();
										itemValores.setValor(planillaProfesionales);
										itemValoresService.update(itemValores);
										break;
									case 99:
										System.out.println("-- !! -- !! --  L�nea base (encuesta)");
										String lineaBase = sheet.getRow(i).getCell(117).getStringCellValue();
										itemValores.setValor(lineaBase);
										itemValoresService.update(itemValores);
										break;
									case 101:
										System.out.println("-- !! -- !! --  �Proyecto validado por cliente?");
										String proyectoValidadoCliente = "";
										if (sheet.getRow(i).getCell(118).getCellTypeEnum() == CellType.STRING) {
											System.out.println(sheet.getRow(i).getCell(118).getStringCellValue());
										} else {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(118))) {
												if (sheet.getRow(i).getCell(118) == null || sheet.getRow(i).getCell(118)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(118).getDateCellValue();
													proyectoValidadoCliente = df.format(date);
												}
											}
										}
										itemValores.setValor(proyectoValidadoCliente);
										itemValoresService.update(itemValores);
										break;
									case 100:
										System.out.println("-- !! -- !! --  Fecha de postulaci�n");
										String fechaPostulacion = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(119))) {
											if (sheet.getRow(i).getCell(119) == null || sheet.getRow(i).getCell(119)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(119).getDateCellValue();
												fechaPostulacion = df.format(date);
											}
										}
										itemValores.setValor(fechaPostulacion);
										itemValoresService.update(itemValores);
										break;
									case 102:
										System.out.println("-- !! -- !! --  Informe de verificaci�n recibido? ");
										String informeVerificacion = "";
										try {
											informeVerificacion = sheet.getRow(i).getCell(120).getStringCellValue();
										} catch (Exception e) {
											System.out.println("Valor com erro, criando valor em branco");
										}
										itemValores.setValor(informeVerificacion);
										itemValoresService.update(itemValores);
										break;
									case 178:
										System.out.println("-- !! -- !! --  Ejecutivo de Evaluaci�n");
										String ejecutivoEvaluacion = sheet.getRow(i).getCell(121).getStringCellValue();
										itemValores.setValor(ejecutivoEvaluacion);
										itemValoresService.update(itemValores);
										break;
									case 103:
										System.out.println(
												"-- !! -- !! --  Fecha de recepci�n del Informe de Verificaci�n");
										String fechaRecepInformeVer = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(122))) {
											if (sheet.getRow(i).getCell(122) == null || sheet.getRow(i).getCell(122)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(122).getDateCellValue();
												fechaRecepInformeVer = df.format(date);
											}
										}
										itemValores.setValor(fechaRecepInformeVer);
										itemValoresService.update(itemValores);
										break;
									case 104:
										System.out.println("-- !! -- !! --  Fecha l�mite para env�o de aclaraciones");
										String fechaLimiteEnvio = "";
										try {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(123))) {
												if (sheet.getRow(i).getCell(123) == null || sheet.getRow(i).getCell(123)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(123).getDateCellValue();
													fechaLimiteEnvio = df.format(date);
												}
											}
										} catch (Exception e) {
											System.out.println("Criando valor vazio");
										}
										itemValores.setValor(fechaLimiteEnvio);
										itemValoresService.update(itemValores);
										break;
									case 105:
										System.out.println(
												"-- !! -- !! -- Fecha de respuesta al Informe de Verificaci�n");
										String fechaRespuestaVerificacion = "";
										if (sheet.getRow(i).getCell(124).getCellTypeEnum() == CellType.STRING) {
											System.out.println(sheet.getRow(i).getCell(124).getStringCellValue());
										} else {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(124))) {
												if (sheet.getRow(i).getCell(124) == null || sheet.getRow(i).getCell(124)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(124).getDateCellValue();
													fechaRespuestaVerificacion = df.format(date);
													System.out.println(fechaRespuestaVerificacion);
												}
											}
										}
										itemValores.setValor(fechaRespuestaVerificacion);
										itemValoresService.update(itemValores);
										break;
									case 176:
										System.out.println(
												"-- !! -- !! --  Ajuste de Presupuesto en Informe de Verificaci�n");
										String ajustePresupuesto = "";
										if (sheet.getRow(i).getCell(125).getStringCellValue().equalsIgnoreCase("SI")) {
											ajustePresupuesto = "true";
										} else {
											ajustePresupuesto = "false";
										}
										itemValores.setValor(ajustePresupuesto);
										itemValoresService.update(itemValores);
										break;
									case 177:
										System.out.println(
												"-- !! -- !! --  Presupuesto despu�s de Informe de Verificaci�n");
										String pressInforme = sheet.getRow(i).getCell(126).getRawValue();
										if (pressInforme != null) {
											if (!pressInforme.contains(".")) {
												pressInforme = pressInforme + ".00";
											}
										}
										itemValores.setValor(pressInforme);
										itemValoresService.update(itemValores);
										break;
									case 106:
										System.out.println("-- !! -- !! --  N�mero de resoluci�n");
										System.out.println(sheet.getRow(i).getCell(127).getNumericCellValue());
										int nProjetosSIA = (int) sheet.getRow(i).getCell(127).getNumericCellValue();
										itemValores.setValor(String.valueOf(nProjetosSIA));
										itemValoresService.update(itemValores);
										break;
									case 108:
										System.out.println("-- !! -- !! -- Fecha Resoluci�n");
										String fechaResolucion = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(128))) {
											if (sheet.getRow(i).getCell(128) == null || sheet.getRow(i).getCell(128)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(128).getDateCellValue();
												fechaResolucion = df.format(date);
												System.out.println(fechaResolucion);
											}
										}
										itemValores.setValor(fechaResolucion);
										itemValoresService.update(itemValores);
										break;
									case 107:
										System.out.println("-- !! -- !! --  Proyecto Certificado?");
										String proyectoCertificado;
										if (sheet.getRow(i).getCell(129).getStringCellValue().equalsIgnoreCase("SI")
												|| sheet.getRow(i).getCell(129).getStringCellValue()
														.equalsIgnoreCase("S�")) {
											proyectoCertificado = "true";
										} else {
											proyectoCertificado = "false";
										}
										itemValores.setValor(proyectoCertificado);
										itemValoresService.update(itemValores);
										break;
									case 116:
										System.out.println("-- !! -- !! --  Valor certificado");
										String valorCertC = sheet.getRow(i).getCell(130).getRawValue();
										if (valorCertC != null) {
											if (!valorCertC.contains(".")) {
												valorCertC = valorCertC + ".00";
											}
										}
										itemValores.setValor(valorCertC);
										itemValoresService.update(itemValores);
										break;
									case 117:
										System.out.println("-- !! -- !! --  Valor certificado de RH");
										String valorCertRH = sheet.getRow(i).getCell(140).getRawValue();
										if (valorCertRH != null) {
											if (!valorCertRH.contains(".")) {
												valorCertRH = valorCertRH + ".00";
											}
										}
										itemValores.setValor(valorCertRH);
										itemValoresService.update(itemValores);
										break;
									case 118:
										System.out.println("-- !! -- !! --  Valor certificado de Gastos Directos");
										String valorCertGastosDiretos = sheet.getRow(i).getCell(141).getRawValue();
										if (valorCertGastosDiretos != null) {
											if (!valorCertGastosDiretos.contains(".")) {
												valorCertGastosDiretos = valorCertGastosDiretos + ".00";
											}
										}
										itemValores.setValor(valorCertGastosDiretos);
										itemValoresService.update(itemValores);
										break;
									case 119:
										System.out.println("-- !! -- !! --  Valor certificado de Subcontratos");
										String valorCertSubcontratos = sheet.getRow(i).getCell(142).getRawValue();
										if (valorCertSubcontratos != null) {
											if (!valorCertSubcontratos.contains(".")) {
												valorCertSubcontratos = valorCertSubcontratos + ".00";
											}
										}
										itemValores.setValor(valorCertSubcontratos);
										itemValoresService.update(itemValores);
										break;
									case 120:
										System.out.println("-- !! -- !! --  Valor certificado de inversi�n");
										String valorCertIversion = sheet.getRow(i).getCell(143).getRawValue();
										if (valorCertIversion != null) {
											if (!valorCertIversion.contains(".")) {
												valorCertIversion = valorCertIversion + ".00";
											}
										}
										itemValores.setValor(valorCertIversion);
										itemValoresService.update(itemValores);
										break;
									case 109:
										System.out.println("-- !! -- !! --  Recurso Reposici�n");
										String recursoReposicion;
										if (sheet.getRow(i).getCell(131).getStringCellValue().equalsIgnoreCase("SI")) {
											recursoReposicion = "true";
										} else {
											recursoReposicion = "false";
										}
										itemValores.setValor(recursoReposicion);
										itemValoresService.update(itemValores);
										break;
									case 110:
										System.out.println("-- !! -- !! -- Fecha m�xima para env�o de recurso");
										String fechaMaxEnvio = "";
										try {
											if (sheet.getRow(i).getCell(132).getCellTypeEnum() == CellType.STRING) {
												System.out.println(sheet.getRow(i).getCell(132).getStringCellValue());
											} else {
												if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(132))) {
													if (sheet.getRow(i).getCell(132) == null || sheet.getRow(i)
															.getCell(132).getCellTypeEnum() == CellType.BLANK) {
														System.out.println("C�LULA EM BRANCO");
													} else {
														DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
														Date date = sheet.getRow(i).getCell(132).getDateCellValue();
														fechaMaxEnvio = df.format(date);
													}
												}
											}
										} catch (Exception e) {
											System.out.println("Valor com erro, criando vazio.");
										}
										itemValores.setValor(fechaMaxEnvio);
										itemValoresService.update(itemValores);
										break;
									case 111:
										System.out.println("-- !! -- !! -- Fecha de env�o recurso reposici�n");
										String fechaEnvioReposicion = "";
										if (sheet.getRow(i).getCell(133).getCellTypeEnum() == CellType.STRING) {
											System.out.println(sheet.getRow(i).getCell(133).getStringCellValue());
										} else {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(133))) {
												if (sheet.getRow(i).getCell(133) == null || sheet.getRow(i).getCell(133)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(133).getDateCellValue();
													fechaEnvioReposicion = df.format(date);
												}
											}
										}
										itemValores.setValor(fechaEnvioReposicion);
										itemValoresService.update(itemValores);
										break;
									case 112:
										System.out
												.println("-- !! -- !! -- Fecha m�xima de respuesta (30 d�as habiles)");
										String fechaMaxRespuesta = "";
										try {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(134))) {
												if (sheet.getRow(i).getCell(134) == null || sheet.getRow(i).getCell(134)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(134).getDateCellValue();
													fechaMaxRespuesta = df.format(date);
													System.out.println(fechaMaxRespuesta);
												}
											}
										} catch (Exception e) {
											System.out.println("Criando valor vazio");
										}
										itemValores.setValor(fechaMaxRespuesta);
										itemValoresService.update(itemValores);
										break;
									case 113:
										System.out.println("-- !! -- !! -- Fecha de respuesta");
										String fechaRespuesta = "";
										if (sheet.getRow(i).getCell(135).getCellTypeEnum() == CellType.STRING) {
											System.out.println(sheet.getRow(i).getCell(135).getStringCellValue());
										} else {
											if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(135))) {
												if (sheet.getRow(i).getCell(135) == null || sheet.getRow(i).getCell(135)
														.getCellTypeEnum() == CellType.BLANK) {
													System.out.println("C�LULA EM BRANCO");
												} else {
													DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													Date date = sheet.getRow(i).getCell(135).getDateCellValue();
													fechaRespuesta = df.format(date);
												}
											}
										}
										itemValores.setValor(fechaRespuesta);
										itemValoresService.update(itemValores);
										break;
									case 114:
										System.out.println("-- !! -- !! -- Nueva Fecha Resoluci�n");
										String nuevaFechaResolucion = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(136))) {
											if (sheet.getRow(i).getCell(136) == null || sheet.getRow(i).getCell(136)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(136).getDateCellValue();
												nuevaFechaResolucion = df.format(date);
											}
										}
										itemValores.setValor(nuevaFechaResolucion);
										itemValoresService.update(itemValores);
										break;
									case 115:
										System.out.println("-- !! -- !! --  Nuevo N� Resoluci�n");
										String nuevoNResolucion = String
												.valueOf(sheet.getRow(i).getCell(137).getNumericCellValue());
										itemValores.setValor(nuevoNResolucion);
										itemValoresService.update(itemValores);
										break;
									case 181:
										System.out.println(
												"-- !! -- !! -- Proyecto certificado tras recurso de reposici�n?");
										String proyectoTrasRecurso = "";
										if (sheet.getRow(i).getCell(138).getStringCellValue().equalsIgnoreCase("SI")) {
											proyectoTrasRecurso = "true";
										} else {
											proyectoTrasRecurso = "false";
										}
										itemValores.setValor(proyectoTrasRecurso);
										itemValoresService.update(itemValores);
										break;
									case 121:
										System.out.println("-- !! -- !! -- Postulaci�n modificada?");
										String postulacionModificada = "";
										if (sheet.getRow(i).getCell(30).getStringCellValue().equalsIgnoreCase("SI")) {
											postulacionModificada = "true";
										} else {
											postulacionModificada = "false";
										}
										itemValores.setValor(postulacionModificada);
										itemValoresService.update(itemValores);
										break;
									case 122:
										System.out.println("-- !! -- !! -- Tipo de modificaci�n?");
										String tipoModificacion = sheet.getRow(i).getCell(29).getStringCellValue();
										itemValores.setValor(tipoModificacion);
										itemValoresService.update(itemValores);
										break;
									case 123:
										System.out.println(
												"-- !! -- !! -- A�O �LTIMA MODIFICACI�N CON CAMBIO PRESUPUESTO");
										String anoUltimaModificacion = String
												.valueOf(sheet.getRow(i).getCell(144).getNumericCellValue());
										itemValores.setValor(anoUltimaModificacion);
										itemValoresService.update(itemValores);
										break;
									case 124:
										System.out.println(
												"-- !! -- !! -- PRESUPUESTO CERTIFICADO �LTIMA MODIFICACI�N PROYECTO");
										String presupuestoCertMod = sheet.getRow(i).getCell(145).getRawValue();
										if (presupuestoCertMod != null) {
											if (!presupuestoCertMod.contains(".")) {
												presupuestoCertMod = presupuestoCertMod + ".00";
											}
										}
										itemValores.setValor(presupuestoCertMod);
										itemValoresService.update(itemValores);
										break;
									case 125:
										System.out.println("-- !! -- !! --  Valor certificado a�o 1");
										String valorCert1 = "";
										if (sheet.getRow(i).getCell(146).getRawValue() != null
												&& sheet.getRow(i).getCell(146).getRawValue() != ""
												&& sheet.getRow(i).getCell(146).getNumericCellValue() != 0.0) {
											valorCert1 = sheet.getRow(i).getCell(146).getRawValue();
											if (valorCert1.contains(".")) {
												String subStr = valorCert1.substring(valorCert1.indexOf("."));
												if (subStr.length() > 2) {
													valorCert1 = valorCert1.replace(
															valorCert1.substring(valorCert1.indexOf(".") + 3), "");
												}
											} else {
												valorCert1 = valorCert1 + ".00";
											}
										}
										itemValores.setValor(valorCert1);
										itemValoresService.update(itemValores);
										break;
									case 126:
										System.out.println("-- !! -- !! --  Valor certificado a�o 2");
										String valorCert2 = "";
										if (sheet.getRow(i).getCell(147).getRawValue() != null
												&& sheet.getRow(i).getCell(147).getRawValue() != ""
												&& sheet.getRow(i).getCell(147).getNumericCellValue() != 0.0) {
											valorCert2 = sheet.getRow(i).getCell(147).getRawValue();
											if (valorCert2.contains(".")) {
												String subStr = valorCert2.substring(valorCert2.indexOf("."));
												if (subStr.length() > 2) {
													valorCert2 = valorCert2.replace(
															valorCert2.substring(valorCert2.indexOf(".") + 3), "");
												}
											} else {
												valorCert2 = valorCert2 + ".00";
											}
										}
										itemValores.setValor(valorCert2);
										itemValoresService.update(itemValores);
										break;
									case 127:
										System.out.println("-- !! -- !! --  Valor certificado a�o 3");
										String valorCert3 = "";
										if (sheet.getRow(i).getCell(148).getRawValue() != null
												&& sheet.getRow(i).getCell(148).getRawValue() != ""
												&& sheet.getRow(i).getCell(148).getNumericCellValue() != 0.0) {
											valorCert3 = sheet.getRow(i).getCell(148).getRawValue();
											if (valorCert3.contains(".")) {
												String subStr = valorCert3.substring(valorCert3.indexOf("."));
												if (subStr.length() > 2) {
													valorCert3 = valorCert3.replace(
															valorCert3.substring(valorCert3.indexOf(".") + 3), "");
												}
											} else {
												valorCert3 = valorCert3 + ".00";
											}
										}
										itemValores.setValor(valorCert3);
										itemValoresService.update(itemValores);
										break;
									case 128:
										System.out.println("-- !! -- !! --  Valor certificado a�o 4");
										String valorCert4 = "";
										if (sheet.getRow(i).getCell(149).getRawValue() != null
												&& sheet.getRow(i).getCell(149).getRawValue() != ""
												&& sheet.getRow(i).getCell(149).getNumericCellValue() != 0.0) {
											valorCert4 = sheet.getRow(i).getCell(149).getRawValue();
											if (valorCert4.contains(".")) {
												String subStr = valorCert4.substring(valorCert4.indexOf("."));
												if (subStr.length() > 2) {
													valorCert4 = valorCert4.replace(
															valorCert4.substring(valorCert4.indexOf(".") + 3), "");
												}
											} else {
												valorCert4 = valorCert4 + ".00";
											}
										}
										itemValores.setValor(valorCert4);
										itemValoresService.update(itemValores);
										break;
									case 129:
										System.out.println("-- !! -- !! --  Valor certificado a�o 5");
										String valorCert5 = "";
										if (sheet.getRow(i).getCell(150).getRawValue() != null
												&& sheet.getRow(i).getCell(150).getRawValue() != ""
												&& sheet.getRow(i).getCell(150).getNumericCellValue() != 0.0) {
											valorCert5 = sheet.getRow(i).getCell(150).getRawValue();
											if (valorCert5.contains(".")) {
												String subStr = valorCert5.substring(valorCert5.indexOf("."));
												if (subStr.length() > 2) {
													valorCert5 = valorCert5.replace(
															valorCert5.substring(valorCert5.indexOf(".") + 3), "");
												}
											} else {
												valorCert5 = valorCert5 + ".00";
											}
										}
										itemValores.setValor(valorCert5);
										itemValoresService.update(itemValores);
										break;
									case 130:
										System.out.println("-- !! -- !! --  Valor certificado a�o 6");
										String valorCert6 = "";
										if (sheet.getRow(i).getCell(151).getRawValue() != null
												&& sheet.getRow(i).getCell(151).getRawValue() != ""
												&& sheet.getRow(i).getCell(151).getNumericCellValue() != 0.0) {
											valorCert6 = sheet.getRow(i).getCell(151).getRawValue();
											if (valorCert6.contains(".")) {
												String subStr = valorCert6.substring(valorCert6.indexOf("."));
												if (subStr.length() > 2) {
													valorCert6 = valorCert6.replace(
															valorCert6.substring(valorCert6.indexOf(".") + 3), "");
												}
											} else {
												valorCert6 = valorCert6 + ".00";
											}
										}
										itemValores.setValor(valorCert6);
										itemValoresService.update(itemValores);
										break;
									case 131:
										System.out.println("-- !! -- !! --  Valor certificado a�o 7");
										String valorCert7 = "";
										if (sheet.getRow(i).getCell(152).getRawValue() != null
												&& sheet.getRow(i).getCell(152).getRawValue() != ""
												&& sheet.getRow(i).getCell(152).getNumericCellValue() != 0.0) {
											valorCert7 = sheet.getRow(i).getCell(152).getRawValue();
											if (valorCert7.contains(".")) {
												String subStr = valorCert7.substring(valorCert7.indexOf("."));
												if (subStr.length() > 2) {
													valorCert7 = valorCert7.replace(
															valorCert7.substring(valorCert7.indexOf(".") + 3), "");
												}
											} else {
												valorCert7 = valorCert7 + ".00";
											}
										}
										itemValores.setValor(valorCert7);
										itemValoresService.update(itemValores);
										break;
									case 132:
										System.out.println("-- !! -- !! --  Valor certificado a�o 8");
										String valorCert8 = "";
										if (sheet.getRow(i).getCell(153).getRawValue() != null
												&& sheet.getRow(i).getCell(153).getRawValue() != ""
												&& sheet.getRow(i).getCell(153).getNumericCellValue() != 0.0) {
											valorCert8 = sheet.getRow(i).getCell(153).getRawValue();
											if (valorCert8.contains(".")) {
												String subStr = valorCert8.substring(valorCert8.indexOf("."));
												if (subStr.length() > 2) {
													valorCert8 = valorCert8.replace(
															valorCert8.substring(valorCert8.indexOf(".") + 3), "");
												}
											} else {
												valorCert8 = valorCert8 + ".00";
											}
										}
										itemValores.setValor(valorCert8);
										itemValoresService.update(itemValores);
										break;
									case 133:
										System.out.println("-- !! -- !! --  Valor certificado a�o 9");
										String valorCert9 = "";
										if (sheet.getRow(i).getCell(154).getRawValue() != null
												&& sheet.getRow(i).getCell(154).getRawValue() != ""
												&& sheet.getRow(i).getCell(154).getNumericCellValue() != 0.0) {
											valorCert9 = sheet.getRow(i).getCell(154).getRawValue();
											if (valorCert9.contains(".")) {
												String subStr = valorCert9.substring(valorCert9.indexOf("."));
												if (subStr.length() > 2) {
													valorCert9 = valorCert9.replace(
															valorCert9.substring(valorCert9.indexOf(".") + 3), "");
												}
											} else {
												valorCert9 = valorCert9 + ".00";
											}
										}
										itemValores.setValor(valorCert9);
										itemValoresService.update(itemValores);
										break;
									case 134:
										System.out.println("-- !! -- !! --  Valor certificado a�o 10");
										String valorCert10 = "";
										if (sheet.getRow(i).getCell(155).getRawValue() != null
												&& sheet.getRow(i).getCell(155).getRawValue() != ""
												&& sheet.getRow(i).getCell(155).getNumericCellValue() != 0.0) {
											valorCert10 = sheet.getRow(i).getCell(155).getRawValue();
											if (valorCert10.contains(".")) {
												String subStr = valorCert10.substring(valorCert10.indexOf("."));
												if (subStr.length() > 2) {
													valorCert10 = valorCert10.replace(
															valorCert10.substring(valorCert10.indexOf(".") + 3), "");
												}
											} else {
												valorCert10 = valorCert10 + ".00";
											}
										}
										itemValores.setValor(valorCert10);
										itemValoresService.update(itemValores);
										break;
									case 135:
										System.out.println("-- !! -- !! --  Valor certificado a�o 11");
										String valorCert11 = "";
										if (sheet.getRow(i).getCell(156).getRawValue() != null
												&& sheet.getRow(i).getCell(156).getRawValue() != ""
												&& sheet.getRow(i).getCell(156).getNumericCellValue() != 0.0) {
											valorCert11 = sheet.getRow(i).getCell(156).getRawValue();
											if (valorCert11.contains(".")) {
												String subStr = valorCert11.substring(valorCert11.indexOf("."));
												if (subStr.length() > 2) {
													valorCert11 = valorCert11.replace(
															valorCert11.substring(valorCert11.indexOf(".") + 3), "");
												}
											} else {
												valorCert11 = valorCert11 + ".00";
											}
										}
										itemValores.setValor(valorCert11);
										itemValoresService.update(itemValores);
										break;
									case 136:
										System.out.println("-- !! -- !! --  Valor certificado a�o 12");
										String valorCert12 = "";
										if (sheet.getRow(i).getCell(157).getRawValue() != null
												&& sheet.getRow(i).getCell(157).getRawValue() != ""
												&& sheet.getRow(i).getCell(157).getNumericCellValue() != 0.0) {
											valorCert12 = sheet.getRow(i).getCell(157).getRawValue();
											if (valorCert12.contains(".")) {
												String subStr = valorCert12.substring(valorCert12.indexOf("."));
												if (subStr.length() > 2) {
													valorCert12 = valorCert12.replace(
															valorCert12.substring(valorCert12.indexOf(".") + 3), "");
												}
											} else {
												valorCert12 = valorCert12 + ".00";
											}
										}
										itemValores.setValor(valorCert12);
										itemValoresService.update(itemValores);
										break;
									case 137:
										System.out.println("-- !! -- !! --  Valor certificado a�o 13");
										String valorCert13 = "";
										if (sheet.getRow(i).getCell(158).getRawValue() != null
												&& sheet.getRow(i).getCell(158).getRawValue() != ""
												&& sheet.getRow(i).getCell(158).getNumericCellValue() != 0.0) {
											valorCert13 = sheet.getRow(i).getCell(158).getRawValue();
											if (valorCert13.contains(".")) {
												String subStr = valorCert13.substring(valorCert13.indexOf("."));
												if (subStr.length() > 2) {
													valorCert13 = valorCert13.replace(
															valorCert13.substring(valorCert13.indexOf(".") + 3), "");
												}
											} else {
												valorCert13 = valorCert13 + ".00";
											}
										}
										itemValores.setValor(valorCert13);
										itemValoresService.update(itemValores);
										break;
									case 138:
										System.out.println("-- !! -- !! --  Valor certificado a�o 14");
										String valorCert14 = "";
										if (sheet.getRow(i).getCell(159).getRawValue() != null
												&& sheet.getRow(i).getCell(159).getRawValue() != ""
												&& sheet.getRow(i).getCell(159).getNumericCellValue() != 0.0) {
											valorCert14 = sheet.getRow(i).getCell(159).getRawValue();
											if (valorCert14.contains(".")) {
												String subStr = valorCert14.substring(valorCert14.indexOf("."));
												if (subStr.length() > 2) {
													valorCert14 = valorCert14.replace(
															valorCert14.substring(valorCert14.indexOf(".") + 3), "");
												}
											} else {
												valorCert14 = valorCert14 + ".00";
											}
										}
										itemValores.setValor(valorCert14);
										itemValoresService.update(itemValores);
										break;
									case 139:
										System.out.println("-- !! -- !! --  Valor certificado a�o 15");
										String valorCert15 = "";
										if (sheet.getRow(i).getCell(160).getRawValue() != null
												&& sheet.getRow(i).getCell(160).getRawValue() != ""
												&& sheet.getRow(i).getCell(160).getNumericCellValue() != 0.0) {
											valorCert15 = sheet.getRow(i).getCell(160).getRawValue();
											if (valorCert15.contains(".")) {
												String subStr = valorCert15.substring(valorCert15.indexOf("."));
												if (subStr.length() > 2) {
													valorCert15 = valorCert15.replace(
															valorCert15.substring(valorCert15.indexOf(".") + 3), "");
												}
											} else {
												valorCert15 = valorCert15 + ".00";
											}
										}
										itemValores.setValor(valorCert15);
										itemValoresService.update(itemValores);
										break;
									case 140:
										System.out.println("-- !! -- !! --  Valor certificado a�o 16");
										String valorCert16 = "";
										if (sheet.getRow(i).getCell(161).getRawValue() != null
												&& sheet.getRow(i).getCell(161).getRawValue() != ""
												&& sheet.getRow(i).getCell(161).getNumericCellValue() != 0.0) {
											valorCert16 = sheet.getRow(i).getCell(161).getRawValue();
											if (valorCert16.contains(".")) {
												String subStr = valorCert16.substring(valorCert16.indexOf("."));
												if (subStr.length() > 2) {
													valorCert16 = valorCert16.replace(
															valorCert16.substring(valorCert16.indexOf(".") + 3), "");
												}
											} else {
												valorCert16 = valorCert16 + ".00";
											}
										}
										itemValores.setValor(valorCert16);
										itemValoresService.update(itemValores);
										break;
									case 141:
										System.out.println("-- !! -- !! --  Valor certificado a�o 17");
										String valorCert17 = "";
										if (sheet.getRow(i).getCell(162).getRawValue() != null
												&& sheet.getRow(i).getCell(162).getRawValue() != ""
												&& sheet.getRow(i).getCell(162).getNumericCellValue() != 0.0) {
											valorCert17 = sheet.getRow(i).getCell(162).getRawValue();
											if (valorCert17.contains(".")) {
												String subStr = valorCert17.substring(valorCert17.indexOf("."));
												if (subStr.length() > 2) {
													valorCert17 = valorCert17.replace(
															valorCert17.substring(valorCert17.indexOf(".") + 3), "");
												}
											} else {
												valorCert17 = valorCert17 + ".00";
											}
										}
										itemValores.setValor(valorCert17);
										itemValoresService.update(itemValores);
										break;
									case 142:
										System.out.println("-- !! -- !! --  Valor certificado a�o 18");
										String valorCert18 = "";
										if (sheet.getRow(i).getCell(163).getRawValue() != null
												&& sheet.getRow(i).getCell(163).getRawValue() != ""
												&& sheet.getRow(i).getCell(163).getNumericCellValue() != 0.0) {
											valorCert18 = sheet.getRow(i).getCell(163).getRawValue();
											if (valorCert18.contains(".")) {
												String subStr = valorCert18.substring(valorCert18.indexOf("."));
												if (subStr.length() > 2) {
													valorCert18 = valorCert18.replace(
															valorCert18.substring(valorCert18.indexOf(".") + 3), "");
												}
											} else {
												valorCert18 = valorCert18 + ".00";
											}
										}
										itemValores.setValor(valorCert18);
										itemValoresService.update(itemValores);
										break;
									case 143:
										System.out.println("-- !! -- !! --  Valor certificado a�o 19");
										String valorCert19 = "";
										if (sheet.getRow(i).getCell(164).getRawValue() != null
												&& sheet.getRow(i).getCell(164).getRawValue() != ""
												&& sheet.getRow(i).getCell(164).getNumericCellValue() != 0.0) {
											valorCert19 = sheet.getRow(i).getCell(164).getRawValue();
											if (valorCert19.contains(".")) {
												String subStr = valorCert19.substring(valorCert19.indexOf("."));
												if (subStr.length() > 2) {
													valorCert19 = valorCert19.replace(
															valorCert19.substring(valorCert19.indexOf(".") + 3), "");
												}
											} else {
												valorCert19 = valorCert19 + ".00";
											}
										}
										itemValores.setValor(valorCert19);
										itemValoresService.update(itemValores);
										break;
									case 144:
										System.out.println("-- !! -- !! --  Valor certificado a�o 20");
										String valorCert20 = "";
										if (sheet.getRow(i).getCell(165).getRawValue() != null
												&& sheet.getRow(i).getCell(165).getRawValue() != ""
												&& sheet.getRow(i).getCell(165).getNumericCellValue() != 0.0) {
											valorCert20 = sheet.getRow(i).getCell(165).getRawValue();
											if (valorCert20.contains(".")) {
												String subStr = valorCert20.substring(valorCert20.indexOf("."));
												if (subStr.length() > 2) {
													valorCert20 = valorCert20.replace(
															valorCert20.substring(valorCert20.indexOf(".") + 3), "");
												}
											} else {
												valorCert20 = valorCert20 + ".00";
											}
										}
										itemValores.setValor(valorCert20);
										itemValoresService.update(itemValores);
										break;
									case 145:
										System.out.println("-- !! -- !! --  Valor certificado a�o 21");
										String valorCert21 = "";
										if (sheet.getRow(i).getCell(166).getRawValue() != null
												&& sheet.getRow(i).getCell(166).getRawValue() != ""
												&& sheet.getRow(i).getCell(166).getNumericCellValue() != 0.0) {
											valorCert21 = sheet.getRow(i).getCell(166).getRawValue();
											if (valorCert21.contains(".")) {
												String subStr = valorCert21.substring(valorCert21.indexOf("."));
												if (subStr.length() > 2) {
													valorCert21 = valorCert21.replace(
															valorCert21.substring(valorCert21.indexOf(".") + 3), "");
												}
											} else {
												valorCert21 = valorCert21 + ".00";
											}
										}
										itemValores.setValor(valorCert21);
										itemValoresService.update(itemValores);
										break;
									case 146:
										System.out.println("-- !! -- !! --  Valor certificado a�o 22");
										String valorCert22 = "";
										if (sheet.getRow(i).getCell(167).getRawValue() != null
												&& sheet.getRow(i).getCell(167).getRawValue() != ""
												&& sheet.getRow(i).getCell(167).getNumericCellValue() != 0.0) {
											valorCert22 = sheet.getRow(i).getCell(167).getRawValue();
											if (valorCert22.contains(".")) {
												String subStr = valorCert22.substring(valorCert22.indexOf("."));
												if (subStr.length() > 2) {
													valorCert22 = valorCert22.replace(
															valorCert22.substring(valorCert22.indexOf(".") + 3), "");
												}
											} else {
												valorCert22 = valorCert22 + ".00";
											}
										}
										itemValores.setValor(valorCert22);
										itemValoresService.update(itemValores);
										break;
									case 147:
										System.out.println("-- !! -- !! --  Valor certificado a�o 23");
										String valorCert23 = "";
										if (sheet.getRow(i).getCell(168).getRawValue() != null
												&& sheet.getRow(i).getCell(168).getRawValue() != ""
												&& sheet.getRow(i).getCell(168).getNumericCellValue() != 0.0) {
											valorCert23 = sheet.getRow(i).getCell(168).getRawValue();
											if (valorCert23.contains(".")) {
												String subStr = valorCert23.substring(valorCert23.indexOf("."));
												if (subStr.length() > 2) {
													valorCert23 = valorCert23.replace(
															valorCert23.substring(valorCert23.indexOf(".") + 3), "");
												}
											} else {
												valorCert23 = valorCert23 + ".00";
											}
										}
										itemValores.setValor(valorCert23);
										itemValoresService.update(itemValores);
										break;
									case 148:
										System.out.println("-- !! -- !! --  Valor certificado a�o 24");
										String valorCert24 = "";
										if (sheet.getRow(i).getCell(169).getRawValue() != null
												&& sheet.getRow(i).getCell(169).getRawValue() != ""
												&& sheet.getRow(i).getCell(169).getNumericCellValue() != 0.0) {
											valorCert24 = sheet.getRow(i).getCell(169).getRawValue();
											if (valorCert24.contains(".")) {
												String subStr = valorCert24.substring(valorCert24.indexOf("."));
												if (subStr.length() > 2) {
													valorCert24 = valorCert24.replace(
															valorCert24.substring(valorCert24.indexOf(".") + 3), "");
												}
											} else {
												valorCert24 = valorCert24 + ".00";
											}
										}
										itemValores.setValor(valorCert24);
										itemValoresService.update(itemValores);
										break;
									case 149:
										System.out.println("-- !! -- !! --  Valor certificado a�o 25");
										String valorCert25 = "";
										if (sheet.getRow(i).getCell(170).getRawValue() != null
												&& sheet.getRow(i).getCell(170).getRawValue() != ""
												&& sheet.getRow(i).getCell(170).getNumericCellValue() != 0.0) {
											valorCert25 = sheet.getRow(i).getCell(170).getRawValue();
											if (valorCert25.contains(".")) {
												String subStr = valorCert25.substring(valorCert25.indexOf("."));
												if (subStr.length() > 2) {
													valorCert25 = valorCert25.replace(
															valorCert25.substring(valorCert25.indexOf(".") + 3), "");
												}
											} else {
												valorCert25 = valorCert25 + ".00";
											}
										}
										itemValores.setValor(valorCert25);
										itemValoresService.update(itemValores);
										break;
									case 172:
										System.out.println("-- !! -- !! -- Previsi�n de fecha de fiscalizaci�n");
										String previsionFechaFiscalizacion = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(238))) {
											if (sheet.getRow(i).getCell(238) == null || sheet.getRow(i).getCell(238)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(238).getDateCellValue();
												previsionFechaFiscalizacion = df.format(date);
											}
										}
										itemValores.setValor(previsionFechaFiscalizacion);
										itemValoresService.update(itemValores);
										break;
									case 173:
										System.out.println("-- !! -- !! --  Solicitud de informaci�n");
										String solicitudInformacion = sheet.getRow(i).getCell(239).getStringCellValue();
										itemValores.setValor(solicitudInformacion);
										itemValoresService.update(itemValores);
										break;
									case 174:
										System.out.println("-- !! -- !! -- Estado de redacci�n de documento");
										String estadoRedacion = sheet.getRow(i).getCell(240).getStringCellValue();
										itemValores.setValor(estadoRedacion);
										itemValoresService.update(itemValores);
										break;
									case 175:
										System.out.println("-- !! -- !! -- Fiscalizaci�n t�cnica enviada");
										String fiscalizacionTecnicaEnviada = "";
										if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(241))) {
											if (sheet.getRow(i).getCell(241) == null || sheet.getRow(i).getCell(241)
													.getCellTypeEnum() == CellType.BLANK) {
												System.out.println("C�LULA EM BRANCO");
											} else {
												DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
												Date date = sheet.getRow(i).getCell(241).getDateCellValue();
												fiscalizacionTecnicaEnviada = df.format(date);
											}
										}
										itemValores.setValor(fiscalizacionTecnicaEnviada);
										itemValoresService.update(itemValores);
										break;
									case 171:
										System.out.println("-- !! -- !! -- Auditoria em terreno");
										String auditoriaTerreno = sheet.getRow(i).getCell(242).getStringCellValue();
										itemValores.setValor(auditoriaTerreno);
										itemValoresService.update(itemValores);
										break;
									case 18:
										System.out.println("-- !! -- !! -- Nombre Proyecto");
										String nombreProyectoCamp = sheet.getRow(i).getCell(31).getStringCellValue();
										itemValores.setValor(nombreProyectoCamp);
										itemValoresService.update(itemValores);
//									case 189:
//										System.out.println("-- !! -- !! -- GRADO DE AVANCE DEL PRESUPUESTO");
//										77
//									case 190:
//										System.out.println("-- !! -- !! -- Metodolog�a");
//										113
									}
								}
							}
						}
					}
				}
			}

		} catch (

		Exception e) {
			e.printStackTrace();
			System.out.println(e.getStackTrace()[0].getLineNumber());
		}
	}

	@Transactional
	public void criarFiscalizacoes() {

//		 C�digo certifica��o Fiscaliza��o = 181
//		 C�digo certifica��o Postula��o = 46

		String codigoCert = "";
		Producao producao = new Producao();
		EtapaTrabalho etapa = etapaTrabalhoService.findById(7l);

		try {
			File file = new File("\\\\10.33.1.210\\users\\GGS\\Desktop\\Datasource\\Chile\\migracao.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 2017;
//			 int rows = 78;

			for (int i = 4; i <= rows; i++) {

				String servico = sheet.getRow(i).getCell(28).getStringCellValue();

				XSSFRow row = sheet.getRow(i);
				String rut = sheet.getRow(i).getCell(4).getStringCellValue();

				if (rut != null || rut != "") {

					rut = rut.replaceAll("[^a-zA-Z0-9]+", "");

					Cliente cliente = clienteService.findByRUT(rut);

					if (Objects.nonNull(cliente)) {

						String service = sheet.getRow(i).getCell(28).getStringCellValue();

						if (servico.equalsIgnoreCase("POSTULACI�N")) {

							int ano = (int) sheet.getRow(i).getCell(1).getNumericCellValue();
							String nombreProyecto = sheet.getRow(i).getCell(31).getStringCellValue();
							String obs = sheet.getRow(i).getCell(23).getStringCellValue();
							String nombreSIA = sheet.getRow(i).getCell(41).getStringCellValue();

							producao = producaoService.getProducaoChileCriteria(String.valueOf(ano), 1l,
									cliente.getId(), obs, nombreProyecto, nombreSIA);

							if (Objects.nonNull(producao)) {

								codigoCert = sheet.getRow(i).getCell(57).getStringCellValue();

							}

						} else if (servico.equalsIgnoreCase("FISCALIZACI�N")) {

							System.out.println(cliente.getRazaoSocial() + " Fiscalizacao");

							String nomeProjeto = sheet.getRow(i).getCell(198).getStringCellValue();
							int anoFisc = (int) sheet.getRow(i).getCell(1).getNumericCellValue();

							SubProducao fiscalizacao = new SubProducao();
							fiscalizacao.setProducao(producao);
							fiscalizacao.setEtapaTrabalho(etapa);
							fiscalizacao.setDescricao("FISCALIZACI�N ECONOMICA " + nomeProjeto.toUpperCase());
							fiscalizacao.setAno(String.valueOf(anoFisc));

							subProducaoService.create(fiscalizacao);
							etapaTrabalhoService.createItemValoresSubProducao(fiscalizacao.getEtapaTrabalho(),
									fiscalizacao);

						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void populaFiscalizacao() {

		try {
			File file = new File("C:\\Users\\GGS\\OneDrive - FI Group\\Escritorio\\migra\\chile.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 1691;

			for (int i = 4; i <= rows; i++) {

				String servico = sheet.getRow(i).getCell(28).getStringCellValue();
				if (servico.equalsIgnoreCase("FISCALIZACI�N")) {

					String nomeProjeto = sheet.getRow(i).getCell(31).getStringCellValue();
					int anoFisc = (int) sheet.getRow(i).getCell(1).getNumericCellValue();

					String descricao = "FISCALIZACI�N ECONOMICA " + nomeProjeto.toUpperCase();

					System.out.println("--- --- --- --- --- PROCESSANDO: " + descricao + " " + anoFisc);

					SubProducao fiscalizacao = subProducaoService.getFiscalizacaoByDescricaoAndAno(descricao,
							String.valueOf(anoFisc));

					if (Objects.nonNull(fiscalizacao)) {
						List<ItemValores> valores = itemValoresService
								.getItemValoresOfSubproducao(fiscalizacao.getId());
						for (ItemValores itemValores : valores) {
							switch (itemValores.getItem().getId().intValue()) {
							case 150:
								System.out.println("-- !! -- !! --  N�mero de proyectos a fiscalizar");
								int numeroProyectosCert = (int) sheet.getRow(i).getCell(196).getNumericCellValue();
								System.out.println(numeroProyectosCert);
								itemValores.setValor(String.valueOf(numeroProyectosCert));
								itemValoresService.update(itemValores);
								break;
							case 151:
								System.out.println("-- !! -- !! --  C�digo de certificaci�n");
								String codigoCert = sheet.getRow(i).getCell(197).getStringCellValue();
								System.out.println(codigoCert);
								itemValores.setValor(codigoCert);
								itemValoresService.update(itemValores);
								break;
							case 152:
								System.out.println("-- !! -- !! --  Nombre del proyecto certificado");
								String nombreProyectoCert = sheet.getRow(i).getCell(198).getStringCellValue();
								System.out.println(nombreProyectoCert);
								itemValores.setValor(nombreProyectoCert);
								itemValoresService.update(itemValores);
								break;
							case 178:
								System.out.println("-- !! -- !! --  Ejecutivo de Evaluaci�n");
								String ejecutivoEvaluacion = sheet.getRow(i).getCell(199).getStringCellValue();
								System.out.println(ejecutivoEvaluacion);
								itemValores.setValor(ejecutivoEvaluacion);
								itemValoresService.update(itemValores);
								break;
							case 153:
								System.out.println("-- !! -- !! --  Campa�a de postulaci�n");
								int campanaPostulacion = (int) sheet.getRow(i).getCell(200).getNumericCellValue();
								System.out.println(campanaPostulacion);
								itemValores.setValor(String.valueOf(campanaPostulacion));
								itemValoresService.update(itemValores);
								break;
							case 154:
								System.out.println("-- !! -- !! --  A�o de ejecuci�n del gasto");
								int anoExecucaoGasto = (int) sheet.getRow(i).getCell(201).getNumericCellValue();
								System.out.println(anoExecucaoGasto);
								itemValores.setValor(String.valueOf(anoExecucaoGasto));
								itemValoresService.update(itemValores);
								break;
							case 155:
								System.out.println("-- !! -- !! --  Informaci�n economica");
								String infomacionEconomica = sheet.getRow(i).getCell(203).getStringCellValue();
								System.out.println(infomacionEconomica);
								itemValores.setValor(infomacionEconomica);
								itemValoresService.update(itemValores);
								break;
							case 156:
								System.out.println("-- !! -- !! --  Creaci�n/Actualizaci�n planilla fiscalizaci�n");
								String planillaFisc = sheet.getRow(i).getCell(211).getStringCellValue();
								if (planillaFisc.equalsIgnoreCase("REALIZADO")) {
									planillaFisc = "true";
								} else {
									planillaFisc = "false";
								}
								itemValores.setValor(planillaFisc);
								itemValoresService.update(itemValores);
								break;
							case 157:
								System.out.println(
										"-- !! -- !! --  Valor Anualizado - Valor a ser fiscalizado, conforme certificaci�n");
								String valorAnualizado;
								if (sheet.getRow(i).getCell(214).getRawValue() != null
										&& sheet.getRow(i).getCell(214).getRawValue() != ""
										&& sheet.getRow(i).getCell(214).getNumericCellValue() != 0.0) {
									valorAnualizado = sheet.getRow(i).getCell(214).getRawValue();
									if (valorAnualizado.contains(".")) {
										String subStr = valorAnualizado.substring(valorAnualizado.indexOf("."));
										if (subStr.length() > 2) {
											valorAnualizado = valorAnualizado.replace(
													valorAnualizado.substring(valorAnualizado.indexOf(".") + 3), "");
										}
									} else {
										valorAnualizado = valorAnualizado + ".00";
									}
								} else {
									valorAnualizado = "";
								}
								System.out.println(valorAnualizado);
								itemValores.setValor(valorAnualizado);
								itemValoresService.update(itemValores);
								break;
							case 158:
								System.out.println(
										"-- !! -- !! --  Valor Disponible - Valor a ser fiscalizado considerando a�os anteriores");
								String valorDisponible = "";
								try {
									if (sheet.getRow(i).getCell(215).getRawValue() != null
											&& sheet.getRow(i).getCell(215).getRawValue() != ""
											&& sheet.getRow(i).getCell(215).getNumericCellValue() != 0.0) {
										valorDisponible = sheet.getRow(i).getCell(215).getRawValue();
										if (valorDisponible.contains(".")) {
											String subStr = valorDisponible.substring(valorDisponible.indexOf("."));
											if (subStr.length() > 2) {
												valorDisponible = valorDisponible.replace(
														valorDisponible.substring(valorDisponible.indexOf(".") + 3),
														"");
											}
										} else {
											valorDisponible = valorDisponible + ".00";
										}
									} else {
										valorDisponible = "";
									}
								} catch (Exception e) {
									System.out.println("Valor com erro");
								}
								System.out.println(valorDisponible);
								itemValores.setValor(valorDisponible);
								itemValoresService.update(itemValores);
								break;
							case 159:
								System.out.println("-- !! -- !! --  Valor fiscalizado a presentar");
								String valorFiscalizado;
								if (sheet.getRow(i).getCell(225).getRawValue() != null
										&& sheet.getRow(i).getCell(225).getRawValue() != ""
										&& sheet.getRow(i).getCell(225).getNumericCellValue() != 0.0) {
									valorFiscalizado = sheet.getRow(i).getCell(225).getRawValue();
									if (valorFiscalizado.contains(".")) {
										String subStr = valorFiscalizado.substring(valorFiscalizado.indexOf("."));
										if (subStr.length() > 2) {
											valorFiscalizado = valorFiscalizado.replace(
													valorFiscalizado.substring(valorFiscalizado.indexOf(".") + 3), "");
										}
									} else {
										valorFiscalizado = valorFiscalizado + ".00";
									}
								} else {
									valorFiscalizado = "";
								}
								System.out.println(valorFiscalizado);
								itemValores.setValor(valorFiscalizado);
								itemValoresService.update(itemValores);
								break;
							case 160:
								System.out.println("-- !! -- !! --  Validaci�n de cliente");
								String validacionCliente = "";
								try {
									validacionCliente = sheet.getRow(i).getCell(226).getStringCellValue();
									if (validacionCliente.equalsIgnoreCase("RECIBIDA")) {
										validacionCliente = "true";
									} else {
										validacionCliente = "false";
									}
								} catch (Exception e) {
									System.out.println("Valor com erro");
								}
								System.out.println(validacionCliente);
								itemValores.setValor(validacionCliente);
								itemValoresService.update(itemValores);
								break;
							case 161:
								System.out.println("-- !! -- !! -- Carga de la rendici�n economica");
								String cargaRendicion = sheet.getRow(i).getCell(227).getStringCellValue();
								if (cargaRendicion.equalsIgnoreCase("REALIZADO")) {
									cargaRendicion = "true";
								} else {
									cargaRendicion = "false";
								}
								itemValores.setValor(cargaRendicion);
								itemValoresService.update(itemValores);
								break;
							case 162:
								System.out.println("-- !! -- !! -- Declaraci�n jurada firmada");
								String declaracionJurada = sheet.getRow(i).getCell(228).getStringCellValue();
								System.out.println(declaracionJurada);
								itemValores.setValor(declaracionJurada);
								itemValoresService.update(itemValores);
								break;
							case 163:
								System.out.println("-- !! -- !! -- Fiscalizaci�n enviada");
								String fiscEnviada = "";
								try {
									fiscEnviada = sheet.getRow(i).getCell(229).getStringCellValue();
								} catch (Exception e) {
									System.out.println("Erro ao carregar, atribuindo valor nulo");
								}
								System.out.println(fiscEnviada);
								itemValores.setValor(fiscEnviada);
								itemValoresService.update(itemValores);
								break;
							case 164:
								System.out.println("-- !! -- !! -- Comprobante de fiscalizaci�n");
								String comprobanteFisc = "";
								try {
									comprobanteFisc = sheet.getRow(i).getCell(230).getStringCellValue();
									if (comprobanteFisc.equalsIgnoreCase("REALIZADO")) {
										comprobanteFisc = "true";
									} else {
										comprobanteFisc = "false";
									}
								} catch (Exception e) {
									System.out.println("Valor com erro");
								}
								System.out.println(comprobanteFisc);
								itemValores.setValor(comprobanteFisc);
								itemValoresService.update(itemValores);
								break;
							case 165:
								System.out.println("-- !! -- !! -- Valor aprobado de fiscalizaci�n CORFO");
								String valorCORFO;
								if (sheet.getRow(i).getCell(231).getRawValue() != null
										&& sheet.getRow(i).getCell(231).getRawValue() != ""
										&& sheet.getRow(i).getCell(231).getNumericCellValue() != 0.0) {
									valorCORFO = sheet.getRow(i).getCell(231).getRawValue();
									if (valorCORFO.contains(".")) {
										String subStr = valorCORFO.substring(valorCORFO.indexOf("."));
										if (subStr.length() > 2) {
											valorCORFO = valorCORFO
													.replace(valorCORFO.substring(valorCORFO.indexOf(".") + 3), "");
										}
									} else {
										valorCORFO = valorCORFO + ".00";
									}
								} else {
									valorCORFO = "";
								}
								System.out.println(valorCORFO);
								itemValores.setValor(valorCORFO);
								itemValoresService.update(itemValores);
								break;
							case 166:
								System.out.println("-- !! -- !! -- Realizaci�n instructivo");
								String realizacionInstructivo = sheet.getRow(i).getCell(232).getStringCellValue();
								if (realizacionInstructivo.equalsIgnoreCase("REALIZADO")) {
									realizacionInstructivo = "true";
								} else {
									realizacionInstructivo = "false";
								}
								itemValores.setValor(realizacionInstructivo);
								itemValoresService.update(itemValores);
								break;
							case 167:
								System.out.println("-- !! -- !! -- Instructivo de aplicaci�n del cr�dito enviado");
								String instructivoAplicacion = sheet.getRow(i).getCell(233).getStringCellValue();
								System.out.println(instructivoAplicacion);
								itemValores.setValor(instructivoAplicacion);
								itemValoresService.update(itemValores);
								break;
							case 168:
								System.out.println("-- !! -- !! -- Valor fiscalizado com correci�n IPC");
								String valorFiscalizadoCorrecion;
								if (sheet.getRow(i).getCell(234).getRawValue() != null
										&& sheet.getRow(i).getCell(234).getRawValue() != ""
										&& sheet.getRow(i).getCell(234).getNumericCellValue() != 0.0) {
									valorFiscalizadoCorrecion = sheet.getRow(i).getCell(234).getRawValue();
									if (valorFiscalizadoCorrecion.contains(".")) {
										String subStr = valorFiscalizadoCorrecion
												.substring(valorFiscalizadoCorrecion.indexOf("."));
										if (subStr.length() > 2) {
											valorFiscalizadoCorrecion = valorFiscalizadoCorrecion
													.replace(valorFiscalizadoCorrecion
															.substring(valorFiscalizadoCorrecion.indexOf(".") + 3), "");
										}
									} else {
										valorFiscalizadoCorrecion = valorFiscalizadoCorrecion + ".00";
									}
								} else {
									valorFiscalizadoCorrecion = "";
								}
								System.out.println(valorFiscalizadoCorrecion);
								itemValores.setValor(valorFiscalizadoCorrecion);
								itemValoresService.update(itemValores);
								break;
							case 169:
								System.out.println("-- !! -- !! -- Valor Referencia Fiscalizaci�n");
								String valorReferenciaFisc = "";
								try {
									if (sheet.getRow(i).getCell(235).getRawValue() != null
											&& sheet.getRow(i).getCell(235).getRawValue() != ""
											&& sheet.getRow(i).getCell(235).getNumericCellValue() != 0.0) {
										valorReferenciaFisc = sheet.getRow(i).getCell(235).getRawValue();
										if (valorReferenciaFisc.contains(".")) {
											String subStr = valorReferenciaFisc
													.substring(valorReferenciaFisc.indexOf("."));
											if (subStr.length() > 2) {
												valorReferenciaFisc = valorReferenciaFisc.replace(valorReferenciaFisc
														.substring(valorReferenciaFisc.indexOf(".") + 3), "");
											}
										} else {
											valorReferenciaFisc = valorReferenciaFisc + ".00";
										}
									} else {
										valorReferenciaFisc = "";
									}
								} catch (Exception e) {
									System.out.println("Erro de valor");
								}
								System.out.println(valorReferenciaFisc);
								itemValores.setValor(valorReferenciaFisc);
								itemValoresService.update(itemValores);
								break;
							case 170:
								System.out.println("-- !! -- !! -- Estado de preparaci�n de carpeta");
								String estadoCarpeta = sheet.getRow(i).getCell(236).getStringCellValue();
								System.out.println(estadoCarpeta);
								itemValores.setValor(estadoCarpeta);
								itemValoresService.update(itemValores);
								break;
							case 171:
								System.out.println("-- !! -- !! -- Auditoria em terreno");
								String auditoriaTerreno = sheet.getRow(i).getCell(237).getStringCellValue();
								System.out.println(auditoriaTerreno);
								itemValores.setValor(auditoriaTerreno);
								itemValoresService.update(itemValores);
								break;
							}

						}
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void identificaPostulacoes() {

		try {
			File file = new File("C:\\Users\\ggs\\Desktop\\Migra\\CHILE\\ferramentaMIGRA.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 1188;
//			int rows = 78;

			for (int i = 4; i <= rows; i++) {

				String servico = sheet.getRow(i).getCell(19).getStringCellValue();

				XSSFRow row = sheet.getRow(i);
				String rut = sheet.getRow(i).getCell(4).getStringCellValue();

				if (rut != null || rut != "") {

					rut = rut.replaceAll("[^a-zA-Z0-9]+", "");

					Cliente cliente = clienteService.findByRUT(rut);

					if (Objects.nonNull(cliente)) {

						String service = sheet.getRow(i).getCell(19).getStringCellValue();

						if (servico.equalsIgnoreCase("POSTULACI�N")) {

							int ano = (int) sheet.getRow(i).getCell(1).getNumericCellValue();
							String nombreProyecto = sheet.getRow(i).getCell(22).getStringCellValue();
							String obs = sheet.getRow(i).getCell(18).getStringCellValue();
							String nombreSIA = sheet.getRow(i).getCell(30).getStringCellValue();

							Producao producao = producaoService.getProducaoChileCriteria(String.valueOf(ano), 1l,
									cliente.getId(), obs, nombreProyecto, nombreSIA);

							if (Objects.nonNull(producao)) {

							} else {
								System.out.println("Cliente: " + cliente.getRazaoSocial() + " a�o " + ano
										+ ", proyecto " + nombreProyecto);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void identificaFiscalizacoes() {

		try {
			File file = new File("C:\\Users\\ggs\\Desktop\\Migra\\CHILE\\ferramentaMIGRA.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 1188;
//			int rows = 78;

			for (int i = 4; i <= rows; i++) {

				String servico = sheet.getRow(i).getCell(19).getStringCellValue();

				XSSFRow row = sheet.getRow(i);
				String rut = sheet.getRow(i).getCell(4).getStringCellValue();

				if (rut != null || rut != "") {

					rut = rut.replaceAll("[^a-zA-Z0-9]+", "");

					Cliente cliente = clienteService.findByRUT(rut);

					if (Objects.nonNull(cliente)) {

						String service = sheet.getRow(i).getCell(19).getStringCellValue();

						if (servico.equalsIgnoreCase("FISCALIZACI�N")) {

							String nomeProjeto = sheet.getRow(i).getCell(182).getStringCellValue();
							int anoFisc = (int) sheet.getRow(i).getCell(185).getNumericCellValue();

							String descricao = "FISCALIZACI�N ECONOMICA " + nomeProjeto.toUpperCase();
							SubProducao fiscalizacao = subProducaoService.getFiscalizacaoByDescricaoAndAno(descricao,
									String.valueOf(anoFisc));

							if (Objects.nonNull(fiscalizacao)) {

							} else {
								System.out.println(
										"FISCALIZACI�N ECONOMICA " + nomeProjeto.toUpperCase() + " A�O " + anoFisc);
							}

						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void atualizarTipoContrato() {

		try {
			File file = new File("C:\\Users\\ggs\\Desktop\\Migra\\CHILE\\Basecontratos.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 823;

			for (int i = 1; i <= rows; i++) {

				System.out.println(sheet.getRow(i).getCell(2).getStringCellValue());

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void procuraProds() {

		try {
			File file = new File("C:\\Users\\GGS\\OneDrive - FI Group\\Escritorio\\migra\\chile.xlsx");
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wk = new XSSFWorkbook(fis);
			XSSFSheet sheet = wk.getSheetAt(0);

			int rows = 2020;

			for (int i = 4; i <= rows; i++) {

				String servico = sheet.getRow(i).getCell(28).getStringCellValue();
				if (servico.equalsIgnoreCase("POSTULACI�N")) {

					XSSFRow row = sheet.getRow(i);
					String rut = sheet.getRow(i).getCell(4).getStringCellValue();

					if (rut != null || rut != "") {

						rut = rut.replaceAll("[^a-zA-Z0-9]+", "");

						Cliente cliente = clienteService.findByRUT(rut);

						int ano = (int) sheet.getRow(i).getCell(1).getNumericCellValue();

						if (Objects.nonNull(cliente)) {

							String nombreProyecto = sheet.getRow(i).getCell(31).getStringCellValue();
							String obs = sheet.getRow(i).getCell(23).getStringCellValue();
							String nombreSIA = sheet.getRow(i).getCell(41).getStringCellValue();

							String msgProcura = "Procurando produ��o do cliente " + cliente.getRazaoSocial()
									+ " do ano " + ano + " Projeto: " + nombreProyecto;

							Producao producao = producaoService.getProducaoChileCriteria(String.valueOf(ano), 1l,
									cliente.getId(), obs, nombreProyecto, nombreSIA);

							if (Objects.nonNull(producao)) {
								msgProcura += " >> ENCONTRADO";
							} else {
								msgProcura += " !! N�O ENCONTRADO";
							}

							System.out.println(msgProcura);

						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
