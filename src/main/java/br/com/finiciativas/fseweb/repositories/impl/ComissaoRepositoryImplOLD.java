/*
 * package br.com.finiciativas.fseweb.repositories.impl;
 * 
 * import java.text.DecimalFormat; import java.text.DecimalFormatSymbols; import
 * java.util.ArrayList; import java.util.List; import java.util.Locale;
 * 
 * import javax.persistence.EntityManager; import javax.persistence.TypedQuery;
 * import javax.transaction.Transactional;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.stereotype.Repository;
 * 
 * import br.com.finiciativas.fseweb.enums.Cobrado; import
 * br.com.finiciativas.fseweb.models.consultor.Comissao; import
 * br.com.finiciativas.fseweb.models.consultor.Impostos; import
 * br.com.finiciativas.fseweb.models.contrato.Contrato; import
 * br.com.finiciativas.fseweb.models.contrato.EtapasPagamento; import
 * br.com.finiciativas.fseweb.models.producao.Producao; import
 * br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio; import
 * br.com.finiciativas.fseweb.repositories.ComissaoRepository; import
 * br.com.finiciativas.fseweb.services.impl.ComissaoServiceImpl; import
 * br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl; import
 * br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl; import
 * br.com.finiciativas.fseweb.services.impl.ImpostosServiceImpl; import
 * br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl; import
 * br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;
 * 
 * @Repository public class ComissaoRepositoryImplOLD implements
 * ComissaoRepository {
 * 
 * private static final Locale LOCAL = new Locale("pt","BR");
 * 
 * @Autowired private EntityManager em;
 * 
 * @Autowired private ComissaoServiceImpl comissaoService;
 * 
 * // @Autowired // private ContratoServiceImpl contratoService;
 * 
 * @Autowired private ProducaoServiceImpl producaoService;
 * 
 * @Autowired private ValorNegocioServiceImpl valorNegocioService;
 * 
 * @Autowired private ImpostosServiceImpl impostoService;
 * 
 * @Autowired private FaturamentoServiceImpl faturamentoService;
 * 
 * @Autowired private ConsultorServiceImpl consultorService;
 * 
 * @Autowired private NotaFiscalRepositoryImpl notaFiscalService;
 * 
 * 
 * @Transactional
 * 
 * @Override public Comissao create(Comissao comissao) { em.persist(comissao);
 * return comissao; }
 * 
 * @Override public Comissao update(Comissao comissao) { return null; }
 * 
 * @Override public void delete(Comissao comissao) { this.em.remove(comissao); }
 * 
 * @Override public Comissao findById(Long id) { return
 * this.em.find(Comissao.class, id); }
 * 
 * @Override public List<Comissao> getAll() { return
 * em.createQuery("SELECT c FROM Comissao c", Comissao.class).getResultList(); }
 * 
 * @Override public List<Comissao> getComissaoByConsultor(Long id) { try {
 * String sQuery = "SELECT c FROM Comissao c WHERE c.consultor.id = :pId";
 * return em.createQuery(sQuery, Comissao.class).setParameter("pId",
 * id).getResultList(); } catch (Exception e) { System.out.println(e);
 * List<Comissao> comis = new ArrayList<Comissao>(); return comis; } }
 * 
 * @Override public List<Comissao> getComissaoByGestor(Long id) {
 * System.out.println("Construir metodo getComissaoByGestor"); return null; }
 * 
 * @Override public List<Comissao> getComissaoByAnalista(Long id) {
 * System.out.println("Construir metodo getComissaoByAnalista"); return null; }
 * 
 * @Override public Comissao getLastAdded() {
 * System.out.println("Construir metodo getLastAdded"); return null; }
 * 
 * @Override public boolean checkIfComissaoExistWithProducao(Long id) {
 * 
 * return false;
 * 
 * }
 * 
 * @Transactional public void gerarComissaoByCampanha() {
 * System.out.println("Gerando comissoes da campanha de 2021"); // Puxar
 * comissoes por producoes de 2021 List<Producao> producoes =
 * producaoService.getAllByAnoWhereNN("2021");
 * 
 * // Deve iterar uma producao que pode gerar ate 3 comissoes, dependendo de
 * quantas etapas de faturamento existem for (Producao producao : producoes) {
 * 
 * // Producao producao = producaoService.findById((long) 39); Contrato contrato
 * = producao.getContrato(); EtapasPagamento etapaPagamento =
 * contrato.getEtapasPagamento(); // Sugest�o de melhoria, enviar s� produ��o
 * para query. ValorNegocio valorNegocio =
 * valorNegocioService.getVNbyEmpresaAndProducao(producao.getCliente().getId(),
 * producao.getId()); Impostos imposto =
 * impostoService.getImpostosByCampanha2020();
 * 
 * float ISS = imposto.getISS().floatValue(); //5% float COFINS =
 * imposto.getCOFINS().floatValue(); //3% float PIS =
 * imposto.getPIS().floatValue(); //0,65% float CSLL =
 * imposto.getCSLL().floatValue();// 1% float IRPJ =
 * imposto.getIRPJ().floatValue();// 1,5% // float percentualImposto =
 * imposto.getTotalPorcentagemImposto().floatValue(); //11,15%
 * 
 * // System.out.println( // "Processando comissoes com producao de Id: " +
 * producao.getId() + // "\nEmpresa: " + contrato.getCliente().getRazaoSocial()
 * + // "\nCampanha: " + producao.getAno() + // "\nContrato Id: " +
 * producao.getContrato().getId() + // "\nCliente: " +
 * producao.getCliente().getId()); try { //se comissao nao existe para esta
 * producao...
 * if(comissaoService.checkIfComissaoExistWithProducao(producao.getId()) ==
 * true){ //
 * System.out.println("Essa producao ja possui comissoes, atualizando...");
 * //listando comissoes desta producao List<Comissao> comissoes =
 * comissaoService.getAllByProducao(producao.getId()); int iteracao = 0;
 * for(Comissao comissao : comissoes) {
 * 
 * //setando comissao para ser atualizada comissao = comissoes.get(iteracao);
 * 
 * comissao.setCampanha(producao.getAno()); comissao.setContrato(contrato);
 * comissao.setProducao(producao);
 * 
 * // Se o contrato nao possui consultor e comissao nao possuir consultor, setar
 * para o comercial alterar posteriormente if
 * (producao.getContrato().getComercialResponsavelEntrada() == null &&
 * comissao.getConsultor() == null) {
 * comissao.setConsultor(consultorService.findById(424l)); }else
 * if(producao.getContrato().getComercialResponsavelEntrada() == null){
 * comissao.setConsultor(comissao.getConsultor()); }else
 * if(comissao.getConsultor() == null){
 * comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada()
 * ); }
 * 
 * comissao.setBonusEconomico(comissao.getBonusEconomico());
 * comissao.setValorNegocio(valorNegocioService.getVNbyProd(producao.getId()));
 * valorNegocio = comissao.getValorNegocio();
 * 
 * comissao.setDataDeRecebimentoCliente(comissao.getDataDeRecebimentoCliente());
 * comissao.setDataRecebimentoFuncionario(comissao.getDataRecebimentoFuncionario
 * ()); comissao.setCobrado(comissao.getCobrado());
 * 
 * //Tentando atualizar faturamento e notafiscal para comissao, caso erro = null
 * try { comissao.setFaturamento(comissao.getFaturamento());
 * comissao.setNotasFiscais(comissao.getNotasFiscais()); } catch (Exception e){
 * //System.out.
 * println("Nao forao encontrados faturamento e nota fiscal na comissao a ser atualizada"
 * ); comissao.setNotasFiscais(null); comissao.setFaturamento(null); }
 * 
 * comissao.setEtapaPagamento(etapaPagamento);
 * 
 * float porcentagemEtapa = 0; //Se etapa de pagamento nao for nula e iteracao
 * for numero correto, faca if(etapaPagamento.getEtapa1() != null && iteracao ==
 * 0) { comissao.setEtapaTrabalho(etapaPagamento.getEtapa1()); //coletando
 * porcentagem da etapa de trabalho para calculo porcentagemEtapa =
 * etapaPagamento.getPorcentagem1().floatValue(); try { //coletando faturamento
 * por producao, etapa de pagamento e contrato
 * comissao.setFaturamento(faturamentoService
 * .getFaturamentoByProducaoEtapaContrato2(producao.getId(),
 * etapaPagamento.getEtapa1().getId(), contrato.getId())); //coletando
 * notafiscal por faturamento comissao.setNotasFiscais(notaFiscalService
 * .findByFaturamento(comissao. getFaturamento().getId())); } catch (Exception
 * e) { comissao.setNotasFiscais(null); comissao.setFaturamento(null); } }else
 * if(etapaPagamento.getEtapa2() != null && iteracao == 1) {
 * 
 * comissao.setEtapaTrabalho(etapaPagamento.getEtapa2()); porcentagemEtapa =
 * etapaPagamento.getPorcentagem2().floatValue(); try {
 * comissao.setFaturamento(faturamentoService
 * .getFaturamentoByProducaoEtapaContrato2(producao.getId(),
 * etapaPagamento.getEtapa2().getId(), contrato.getId()));
 * comissao.setNotasFiscais(notaFiscalService .findByFaturamento(comissao.
 * getFaturamento().getId())); } catch (Exception e) {
 * comissao.setNotasFiscais(null); comissao.setFaturamento(null);
 * 
 * } }else if(etapaPagamento.getEtapa3() != null && iteracao == 2){
 * comissao.setEtapaTrabalho(etapaPagamento.getEtapa3()); porcentagemEtapa =
 * etapaPagamento.getPorcentagem3().floatValue(); try {
 * comissao.setFaturamento(faturamentoService
 * .getFaturamentoByProducaoEtapaContrato2(producao.getId(),
 * etapaPagamento.getEtapa3().getId(), contrato.getId()));
 * comissao.setNotasFiscais(notaFiscalService .findByFaturamento(comissao.
 * getFaturamento().getId())); } catch (Exception e) {
 * comissao.setNotasFiscais(null); comissao.setFaturamento(null); } } //setando
 * percentual da comissao
 * comissao.setComissaoPercentual(comissao.getComissaoPercentual()); float
 * comissaoPercentual = comissao.getComissaoPercentual();
 * 
 * float totalValorNegocio = valorNegocio.getValorBase().floatValue();
 * 
 * float valorISS; float valorCOFINS; float valorPIS; float valorCSLL; float
 * valorIRPJ;
 * 
 * //C�lculo de comissao float valorEtapa = (porcentagemEtapa / 100) *
 * totalValorNegocio; // System.out.println("Valor da etapa " + (iteracao + 1) +
 * ":R$" + valorEtapa);
 * 
 * valorISS = (ISS / 100) * valorEtapa; valorCOFINS = (COFINS / 100) *
 * valorEtapa; valorPIS = (PIS / 100) * valorEtapa; valorCSLL = (CSLL / 100) *
 * valorEtapa; valorIRPJ = (IRPJ / 100) * valorEtapa;
 * 
 * float aposImpostos = valorEtapa - (valorISS + valorCOFINS + valorPIS +
 * valorCSLL + valorIRPJ);
 * 
 * float valorFinal = 0.03f * aposImpostos;
 * 
 * String valorFinalString = Float.toString(valorFinal);
 * 
 * DecimalFormat df = new DecimalFormat("#,##0.00", new
 * DecimalFormatSymbols(LOCAL)); valorFinalString = df.format(valorFinal);
 * 
 * comissao.setComissaoValor(valorFinalString); //se o modo valor do VN for
 * "finalizado" comissao nao eh simulada if
 * (valorNegocio.getModoValor().equalsIgnoreCase("finalizado") ||
 * valorNegocio.getModoValor().equalsIgnoreCase("real")) {
 * comissao.setComissaoSimulada(false); } else {
 * comissao.setComissaoSimulada(true); }
 * 
 * comissao.setData_autorizacao(comissao.getData_autorizacao());
 * comissao.setData_pagamento(comissao.getData_pagamento());
 * comissao.setObservacoes(comissao.getObservacoes()); //atualizando comissao
 * update(comissao); //adicionando iteracao para proxima comissao desta producao
 * iteracao++; }
 * 
 * 
 * }else{ //
 * System.out.println("Essa producao nao possui comissoes, criando...");
 * 
 * //Gerar comissao a cada timing de faturamento if (etapaPagamento.getEtapa1()
 * != null) { Comissao comissao = new Comissao(); //
 * System.out.println("Comissao possui etapa 1"); float porcentagemEtapa;
 * 
 * comissao.setCampanha(producao.getAno()); comissao.setContrato(contrato);
 * comissao.setProducao(producao);
 * 
 * // Se o contrato nao possui consultor, setar para o comercial alterar
 * posteriormente if (producao.getContrato().getComercialResponsavelEntrada() ==
 * null) { comissao.setConsultor(consultorService.findById(424l)); }else {
 * comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada()
 * ); }
 * 
 * comissao.setBonusEconomico(null);
 * comissao.setValorNegocio(valorNegocioService.getVNbyProd(producao.getId()));
 * valorNegocio = comissao.getValorNegocio(); //
 * System.out.println("Valor total do negocio: R$" +
 * valorNegocio.getValorBase());
 * 
 * comissao.setDataDeRecebimentoCliente("");
 * comissao.setDataRecebimentoFuncionario(""); comissao.setCobrado(Cobrado.NAO);
 * 
 * //Tentando inserir faturamento e notafiscal para comissao, caso erro = null
 * try { comissao.setFaturamento(faturamentoService
 * .getFaturamentoByProducaoEtapaContrato2(producao.getId(),
 * etapaPagamento.getEtapa1().getId(), contrato.getId()));
 * 
 * try { comissao.setNotasFiscais(notaFiscalService.findByFaturamento(comissao.
 * getFaturamento().getId()));
 * 
 * } catch (Exception e) { comissao.setNotasFiscais(null); }
 * 
 * } catch (Exception e) { comissao.setFaturamento(null);
 * 
 * } comissao.setEtapaPagamento(etapaPagamento);
 * comissao.setEtapaTrabalho(etapaPagamento.getEtapa1());
 * 
 * porcentagemEtapa = etapaPagamento.getPorcentagem1().floatValue(); //
 * System.out.println("Porcentagem da etapa 1: " + porcentagemEtapa + "%");
 * 
 * comissao.setComissaoPercentual((float) 3.0);// 3% de comissao
 * 
 * float totalValorNegocio = valorNegocio.getValorBase().floatValue();
 * 
 * float valorISS; float valorCOFINS; float valorPIS; float valorCSLL; float
 * valorIRPJ;
 * 
 * //C�lculo de comissao float valorEtapa = (porcentagemEtapa / 100) *
 * totalValorNegocio;
 * 
 * valorISS = (ISS / 100) * valorEtapa; valorCOFINS = (COFINS / 100) *
 * valorEtapa; valorPIS = (PIS / 100) * valorEtapa; valorCSLL = (CSLL / 100) *
 * valorEtapa; valorIRPJ = (IRPJ / 100) * valorEtapa;
 * 
 * float aposImpostos = valorEtapa - (valorISS + valorCOFINS + valorPIS +
 * valorCSLL + valorIRPJ);
 * 
 * float valorFinal = 0.03f * aposImpostos;
 * 
 * String valorFinalString = Float.toString(valorFinal);
 * 
 * DecimalFormat df = new DecimalFormat("#,##0.00", new
 * DecimalFormatSymbols(LOCAL)); valorFinalString = df.format(valorFinal);
 * 
 * comissao.setComissaoValor(valorFinalString);
 * 
 * if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado") ||
 * valorNegocio.getModoValor().equalsIgnoreCase("real")) {
 * comissao.setComissaoSimulada(false); } else {
 * comissao.setComissaoSimulada(true); }
 * 
 * comissao.setData_autorizacao(""); comissao.setData_pagamento("");
 * comissao.setObservacoes("Insira as observacoes aqui");
 * 
 * create(comissao); // System.out.println("Etapa 1 criada"); }
 * 
 * if (etapaPagamento.getEtapa2() != null) { Comissao comissao = new Comissao();
 * // System.out.println("Comissao possui etapa 2"); float porcentagemEtapa;
 * 
 * comissao.setCampanha(producao.getAno()); comissao.setContrato(contrato);
 * comissao.setProducao(producao);
 * 
 * // Se o contrato nao possui consultor, setar para o comercial alterar
 * posteriormente if (producao.getContrato().getComercialResponsavelEntrada() ==
 * null) { comissao.setConsultor(consultorService.findById(424l)); }else {
 * comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada()
 * ); }
 * 
 * comissao.setBonusEconomico(null);
 * comissao.setValorNegocio(valorNegocioService.getVNbyProd(producao.getId()));
 * valorNegocio = comissao.getValorNegocio(); //
 * System.out.println("Valor total do negocio: R$" +
 * valorNegocio.getValorBase());
 * 
 * comissao.setDataDeRecebimentoCliente("");
 * comissao.setDataRecebimentoFuncionario(""); comissao.setCobrado(Cobrado.NAO);
 * 
 * //Tentando inserir faturamento e notafiscal para comissao, caso erro = null
 * try { comissao.setFaturamento(faturamentoService
 * .getFaturamentoByProducaoEtapaContrato2(producao.getId(),
 * etapaPagamento.getEtapa2().getId(), contrato.getId()));
 * 
 * 
 * try { comissao.setNotasFiscais(notaFiscalService.findByFaturamento(comissao.
 * getFaturamento().getId()));
 * 
 * } catch (Exception e) { comissao.setNotasFiscais(null); }
 * 
 * } catch (Exception e) { comissao.setFaturamento(null);
 * 
 * } comissao.setEtapaPagamento(etapaPagamento);
 * comissao.setEtapaTrabalho(etapaPagamento.getEtapa2());
 * 
 * porcentagemEtapa = etapaPagamento.getPorcentagem2().floatValue(); //
 * System.out.println("Porcentagem da etapa 2: " + porcentagemEtapa + "%");
 * 
 * comissao.setComissaoPercentual((float) 3.0);// 3% de comissao float
 * comissaoPercentual = comissao.getComissaoPercentual();
 * 
 * float totalValorNegocio = valorNegocio.getValorBase().floatValue();
 * 
 * float valorISS; float valorCOFINS; float valorPIS; float valorCSLL; float
 * valorIRPJ;
 * 
 * //C�lculo de comissao float valorEtapa = (porcentagemEtapa / 100) *
 * totalValorNegocio;
 * 
 * valorISS = (ISS / 100) * valorEtapa; valorCOFINS = (COFINS / 100) *
 * valorEtapa; valorPIS = (PIS / 100) * valorEtapa; valorCSLL = (CSLL / 100) *
 * valorEtapa; valorIRPJ = (IRPJ / 100) * valorEtapa;
 * 
 * float aposImpostos = valorEtapa - valorISS - valorCOFINS - valorPIS -
 * valorCSLL - valorIRPJ;
 * 
 * float valorFinal = (comissaoPercentual / 100) * aposImpostos;
 * 
 * String valorFinalString = Float.toString(valorFinal);
 * 
 * DecimalFormat df = new DecimalFormat("#,##0.00", new
 * DecimalFormatSymbols(LOCAL)); valorFinalString = df.format(valorFinal);
 * 
 * comissao.setComissaoValor(valorFinalString);
 * 
 * if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
 * comissao.setComissaoSimulada(false); } else {
 * comissao.setComissaoSimulada(true); }
 * 
 * comissao.setData_autorizacao(""); comissao.setData_pagamento("");
 * comissao.setObservacoes("Insira as observacoes aqui");
 * 
 * create(comissao); // System.out.println("Etapa 2 criada"); } if
 * (etapaPagamento.getEtapa3() != null) { Comissao comissao = new Comissao(); //
 * System.out.println("Comissao possui etapa 3"); float porcentagemEtapa;
 * 
 * comissao.setCampanha(producao.getAno()); comissao.setContrato(contrato);
 * comissao.setProducao(producao);
 * 
 * // Se o contrato nao possui consultor, setar para o comercial alterar
 * posteriormente if (producao.getContrato().getComercialResponsavelEntrada() ==
 * null) { comissao.setConsultor(consultorService.findById(424l)); }else {
 * comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada()
 * ); }
 * 
 * comissao.setBonusEconomico(null);
 * comissao.setValorNegocio(valorNegocioService.getVNbyProd(producao.getId()));
 * valorNegocio = comissao.getValorNegocio(); //
 * System.out.println("Valor total do negocio: R$" +
 * valorNegocio.getValorBase());
 * 
 * comissao.setDataDeRecebimentoCliente("");
 * comissao.setDataRecebimentoFuncionario(""); comissao.setCobrado(Cobrado.NAO);
 * 
 * //Tentando inserir faturamento e notafiscal para comissao, caso erro = null
 * try { comissao.setFaturamento(faturamentoService
 * .getFaturamentoByProducaoEtapaContrato2(producao.getId(),
 * etapaPagamento.getEtapa3().getId(), contrato.getId()));;
 * 
 * 
 * try { comissao.setNotasFiscais(notaFiscalService.findByFaturamento(comissao.
 * getFaturamento().getId()));
 * 
 * } catch (Exception e) { comissao.setNotasFiscais(null); }
 * 
 * } catch (Exception e) { comissao.setFaturamento(null);
 * 
 * } comissao.setEtapaPagamento(etapaPagamento);
 * comissao.setEtapaTrabalho(etapaPagamento.getEtapa3());
 * 
 * porcentagemEtapa = etapaPagamento.getPorcentagem3().floatValue(); //
 * System.out.println("Porcentagem da etapa 3: " + porcentagemEtapa + "%");
 * 
 * comissao.setComissaoPercentual((float) 3.0);// 3% de comissao float
 * comissaoPercentual = comissao.getComissaoPercentual();
 * 
 * float totalValorNegocio = valorNegocio.getValorBase().floatValue();
 * 
 * float valorISS; float valorCOFINS; float valorPIS; float valorCSLL; float
 * valorIRPJ;
 * 
 * //C�lculo de comissao float valorEtapa = (porcentagemEtapa / 100) *
 * totalValorNegocio;
 * 
 * valorISS = (ISS / 100) * valorEtapa; valorCOFINS = (COFINS / 100) *
 * valorEtapa; valorPIS = (PIS / 100) * valorEtapa; valorCSLL = (CSLL / 100) *
 * valorEtapa; valorIRPJ = (IRPJ / 100) * valorEtapa;
 * 
 * float aposImpostos = valorEtapa - valorISS - valorCOFINS - valorPIS -
 * valorCSLL - valorIRPJ;
 * 
 * float valorFinal = (comissaoPercentual / 100) * aposImpostos;
 * 
 * String valorFinalString = Float.toString(valorFinal);
 * 
 * DecimalFormat df = new DecimalFormat("#,##0.00", new
 * DecimalFormatSymbols(LOCAL)); valorFinalString = df.format(valorFinal);
 * 
 * comissao.setComissaoValor(valorFinalString);
 * 
 * if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
 * comissao.setComissaoSimulada(false); } else {
 * comissao.setComissaoSimulada(true); }
 * 
 * comissao.setData_autorizacao(""); comissao.setData_pagamento("");
 * comissao.setObservacoes("Insira as observacoes aqui");
 * 
 * create(comissao); // System.out.println("Etapa 3 criada"); } } }catch
 * (Exception e) { System.out.println("Erro na Producao: " + producao.getId());
 * } } System.out.println("\nMetodo gerarComissaoByCampanha2020 finalizado!"); }
 * 
 * @Override public Comissao findByProducao(Long id) { try { return this.em.
 * createQuery("SELECT distinct c FROM Comissao c WHERE c.producao.id = :pId",
 * Comissao.class) .setParameter("pId", id).getSingleResult();
 * 
 * } catch (Exception e) { System.out.println(e); return null; }
 * 
 * }
 * 
 * @Override public boolean getAllByProducao(Long id){
 * 
 * return false; }
 * 
 * 
 * public boolean checkIfComissaoExistWithproducao(Long id) { String sQuery =
 * "SELECT CASE WHEN (count(*) > 0) THEN TRUE ELSE FALSE END FROM Comissao c WHERE c.producao.id = :pId"
 * ; TypedQuery<Boolean> booleanQuery = em.createQuery(sQuery,
 * Boolean.class).setParameter("pId", id); boolean exists =
 * booleanQuery.getSingleResult(); if (exists == false) { return false; } else {
 * return true; }
 * 
 * }
 * 
 * }
 */