package br.com.finiciativas.fseweb.repositories.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocioHistorico;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Repository
public class ValorNegocioHistoricoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ValorNegocioServiceImpl valorNegocioService;
	
	@Transactional
	public void create(ValorNegocioHistorico ValorNegocioHistorico) {
		this.em.persist(ValorNegocioHistorico);
	}

	public void delete(ValorNegocioHistorico ValorNegocioHistorico) {
		this.em.remove(ValorNegocioHistorico);
	}

	@Transactional
	public ValorNegocioHistorico update(ValorNegocioHistorico ValorNegocioHistorico) {
		return this.em.merge(ValorNegocioHistorico);
	}
	
	public List<ValorNegocioHistorico> getAll() {
		return this.em.createQuery("SELECT distinct o FROM ValorNegocioHistorico o ", ValorNegocioHistorico.class)
				.getResultList();
	}
	
	public ValorNegocioHistorico findById(Long id) {
		return this.em.find(ValorNegocioHistorico.class, id);
	}
	
	@Transactional
	@Scheduled(cron = "0 0 0 * * SAT")
	public void captureValorNegocioHistorico(){
		
		List<ValorNegocio> vns = valorNegocioService.getAll();
		
		LocalDate date = LocalDate.now();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        String dataHoje = date.format(formatter);

		
		for (ValorNegocio valorNegocio : vns) {
			ValorNegocioHistorico valorNegocioHistorico = new ValorNegocioHistorico();
			
			valorNegocioHistorico.setProducao(valorNegocio.getProducao());
			valorNegocioHistorico.setValorBase(valorNegocio.getValorBase());
			valorNegocioHistorico.setData(dataHoje);
			
			create(valorNegocioHistorico);
			
		}
		
	}
	
}
