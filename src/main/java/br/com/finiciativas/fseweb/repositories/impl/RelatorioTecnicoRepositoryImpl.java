package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;

@Repository
public class RelatorioTecnicoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(RelatorioTecnico relatorio) {
		this.em.persist(relatorio);
	}

	public void delete(RelatorioTecnico relatorio) {
		this.em.remove(relatorio);
	}
	
	@Transactional
	public void deleteOfProducao(Long id){
		em.createQuery("DELETE FROM RelatorioTecnico r WHERE r.producao.id = :pId")
				.setParameter("pId", id)
				.executeUpdate();
	}

	@Transactional
	public RelatorioTecnico update(RelatorioTecnico relatorio) {
		return this.em.merge(relatorio);
	}
	
	public List<RelatorioTecnico> getAll() {
		return this.em.createQuery("SELECT distinct o FROM RelatorioTecnico o ", RelatorioTecnico.class)
				.getResultList();
	}

	public RelatorioTecnico findById(Long id) {
		return this.em.find(RelatorioTecnico.class, id);
	}

	public List<RelatorioTecnico> getRelatorioTecnicoByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM RelatorioTecnico o WHERE o.producao.id = :pId ", RelatorioTecnico.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
	public List<RelatorioTecnico> getRelatorioTecnicoByCriteria(String ano, SetorAtividade setor, Long idConsultor, Long idEquipe) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<RelatorioTecnico> root = cq.from(RelatorioTecnico.class);

		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (ano != null) {
			predicates.add(qb.equal(root.get("producao").get("ano"), ano));
		} 
		
		if (idEquipe != null) {
			predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), idEquipe));
		}
		
		if (idConsultor != null) {
			predicates.add(qb.equal(root.get("producao").get("consultor").get("id"), idConsultor));
		}
		
		if (setor != null) {
			predicates.add(qb.equal(root.get("producao").get("cliente").get("setorAtividade"), setor));
		}

		cq.orderBy(qb.desc(root.get("producao").get("cliente").get("razaoSocial")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		return em.createQuery(cq).getResultList();
	}
}
