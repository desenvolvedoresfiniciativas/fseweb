package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;

@Repository
public class EtapaPagamentoRepositoryImpl {

	@Autowired
	private EntityManager em;

	public List<EtapasPagamento> getAll() {
		return em.createQuery("SELECT distinct e FROM EtapasPagamento e", EtapasPagamento.class).getResultList();
	}

	public EtapasPagamento findByContratoId(Long id) {
		try {
			return em.createQuery("SELECT distinct e FROM EtapasPagamento e WHERE e.contrato.id = :pId",
					EtapasPagamento.class).setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			EtapasPagamento etapa = new EtapasPagamento();
			return etapa;
		}
	}
	
	public List<EtapasPagamento> getEtapasByContrato(Long id){
		try {
			return em.createQuery("SELECT distinct e FROM EtapasPagamento e WHERE e.contrato.id = :pId",
					EtapasPagamento.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			List<EtapasPagamento> etapas = new ArrayList<EtapasPagamento>();
			return etapas;
		}
	}

	@Transactional
	public void create(EtapasPagamento EtapasPagamento) {
		this.em.persist(EtapasPagamento);
	}

	@Transactional
	public EtapasPagamento update(EtapasPagamento EtapasPagamento) {
		return this.em.merge(EtapasPagamento);
	}

	public EtapasPagamento findById(Long id) {
		return this.em.find(EtapasPagamento.class, id);
	}

	@Transactional
	public void remove(Long id) {
		EtapasPagamento EtapasPagamentoRemove = this.em.find(EtapasPagamento.class, id);
		this.em.remove(EtapasPagamentoRemove);
	}

	public EtapasPagamento getLastAdded() {
		try {
			String sQuery = "SELECT ep FROM EtapasPagamento ep WHERE ep.id=(SELECT max(id) FROM EtapasPagamento)";
			return em.createQuery(sQuery, EtapasPagamento.class).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
