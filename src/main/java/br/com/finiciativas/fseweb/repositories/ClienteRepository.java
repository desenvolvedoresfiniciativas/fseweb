package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.cliente.Cliente;

public interface ClienteRepository {

	Cliente create(Cliente cliente);
	
	void delete(Long id);
	
	Cliente update(Cliente cliente);
	
	Cliente findById(Long id);
	
	Cliente findByCnpj(String cnpj);
	
	List<Cliente> getAll();
	
	Cliente loadFullEmpresa(Long id);
	
}
