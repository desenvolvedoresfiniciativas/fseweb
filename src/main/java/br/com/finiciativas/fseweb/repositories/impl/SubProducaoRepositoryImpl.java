package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;

@Repository
public class SubProducaoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ItemValoresServiceImpl itemValoresService;
	
	@Transactional
	public void create(SubProducao subProducao) {
		subProducao.setGerouFaturamento(false);
		this.em.persist(subProducao);
	}
	
	public void delete(SubProducao subProducao) {
		this.em.persist(subProducao);
	}
	
	@Transactional
	public void deleteSubProdOfProducao(Long id){
		em.createQuery("DELETE FROM SubProducao s WHERE s.producao.id = :pId")
				.setParameter("pId", id)
				.executeUpdate();
	}

	@Transactional
	public SubProducao update(SubProducao subProducao) {
		return this.em.merge(subProducao);
	}
	
	public List<SubProducao> getAll(){
		return this.em.createQuery("SELECT distinct p FROM SubProducao p", SubProducao.class)
				.getResultList();
	}
	
	public SubProducao findById(Long id) {
		return this.em.find(SubProducao.class, id);
	}
	
	public List<SubProducao> getSubProducaobyProducaoAndEtapa(Long idProd, Long idEtapa){
		return this.em.createQuery("SELECT distinct p FROM SubProducao p WHERE p.producao.id = :pIdProd AND p.etapaTrabalho.id = :pIdEtapa", SubProducao.class)
				.setParameter("pIdProd", idProd)
				.setParameter("pIdEtapa", idEtapa)
				.getResultList();
	}
	
	public SubProducao getLastAddedByProducao(Long id){
		return this.em.createQuery("SELECT distinct o FROM SubProducao o WHERE o.producao.id = :pId AND o.id = (SELECT max(b.id) FROM SubProducao b)", SubProducao.class)
				.setParameter("pId", id).getSingleResult();
	}
	
	public SubProducao getFiscalizacaoByDescricaoAndAno(String descricao, String ano){
		try{
			return this.em.createQuery("SELECT distinct o FROM SubProducao o WHERE o.descricao = :pDescricao AND o.ano = :pAno", SubProducao.class)
				.setParameter("pDescricao", descricao)
				.setParameter("pAno", ano)
				.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public SubProducao getSubProducaoByProducaoDescricaoAndEtapa(Long idProd, String descricao, Long idEtapa){
		try{
			return this.em.createQuery("SELECT o FROM SubProducao o WHERE  o.producao.id = :pIdProd AND o.descricao = :pDescricao AND o.etapaTrabalho.id = :pIdEtapa", SubProducao.class)
				.setParameter("pDescricao", descricao)
				.setParameter("pIdProd", idProd)
				.setParameter("pIdEtapa", idEtapa)
				.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	public List<SubProducao> getSubProducaoByProducao(Long id){
		try {
			return this.em.createQuery("SELECT distinct s FROM SubProducao s WHERE s.producao.id = :pIdProd", SubProducao.class)
					.setParameter("pIdProd", id).getResultList();
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	@Transactional
	public void deleteSubProducao(Long id){
		itemValoresService.deleteOfSubProducao(id);
		
		em.createQuery("DELETE FROM SubProducao s WHERE s.id = :pId")
				.setParameter("pId", id)
				.executeUpdate();
	}
	public List<SubProducao> getAllOrderProd(){
		return this.em.createQuery("SELECT distinct p FROM SubProducao p where p.producao.id is not null ORDER BY p.producao.id ", SubProducao.class)
				.getResultList();
	}
}
