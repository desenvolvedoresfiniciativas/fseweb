package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.AuditoriaProjetos;

@Repository
public class AuditoriaProjetosRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(AuditoriaProjetos AuditoriaProjetos) {
		this.em.persist(AuditoriaProjetos);
	}

	public void delete(AuditoriaProjetos AuditoriaProjetos) {
		this.em.remove(AuditoriaProjetos);
	}

	@Transactional
	public AuditoriaProjetos update(AuditoriaProjetos AuditoriaProjetos) {
		return this.em.merge(AuditoriaProjetos);
	}
	
	public List<AuditoriaProjetos> getAll() {
		return this.em.createQuery("SELECT distinct o FROM AuditoriaProjetos o ", AuditoriaProjetos.class)
				.getResultList();
	}
	
	public AuditoriaProjetos findById(Long id) {
		return this.em.find(AuditoriaProjetos.class, id);
	}
	
	public List<AuditoriaProjetos> getAuditoriaProjetosByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM AuditoriaProjetos o WHERE o.producao.id = :pId ", AuditoriaProjetos.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
