package br.com.finiciativas.fseweb.repositories.impl;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.Cidade;
import br.com.finiciativas.fseweb.repositories.CidadeRepository;

@Repository
public class CidadeRepositoryImpl implements CidadeRepository {

	@Autowired
	EntityManager em;

	@Override
	public Cidade findCidade(Cidade cidade) {
		return em
				.createQuery(
						"SELECT c FROM Cidade c "
								+ "INNER JOIN c.estado e WHERE c.nome = :nomecidade AND c.estado.uf = :uf",
						Cidade.class)
				.setParameter("nomecidade", cidade.getNome()).setParameter("uf", cidade.getEstado().getUf())
				.getSingleResult();
	}

}
