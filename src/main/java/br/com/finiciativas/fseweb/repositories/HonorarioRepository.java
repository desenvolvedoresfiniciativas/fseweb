package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.honorario.Honorario;

public interface HonorarioRepository {
	
	Honorario update(Honorario honorario);
	
	List<Honorario> getAll();
	
}
