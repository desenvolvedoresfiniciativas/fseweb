package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Tarefa;

public interface TarefaRepository {
	
	void create(Tarefa tarefa);
	
	List<Tarefa> getAll();
	
	Tarefa findById(Long id);

	Tarefa updateTarefaItens(Tarefa tarefa, Long produtoId, Long etapaId);
	
	Tarefa update(Tarefa tarefa);
	
	Tarefa getLastItemAdded();
	
	List<Tarefa> getTarefasByEtapa(EtapaTrabalho etapa);
	
}
