package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.repositories.ClienteRepository;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EnderecoClienteServiceImpl;

@Repository
public class ClienteRepositoryImpl implements ClienteRepository {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private ContatoClienteServiceImpl contatoEmpresaService;
	
	@Autowired
	private EnderecoClienteServiceImpl enderecoClienteService;

	@Override
	public Cliente create(Cliente cliente) {
				
		cliente.setAtivo(true);
		cliente.removeCharacteresCnpj();

		em.persist(cliente);
		
		return cliente;
		
	}

	@Override
	public void delete(Long id) {
		em.createQuery("UPDATE Cliente cli SET cli.ativo = 0 WHERE cli.id = :pId").setParameter("pId", id)
				.executeUpdate();
	}

	@Override
	@Transactional
	public Cliente update(Cliente cliente) {
		
		Cliente clienteAtt = findById(cliente.getId());
		System.out.println("cliente.getCaminhoRECO()"+cliente.getCaminhoRECO());
		clienteAtt.setCnpj(cliente.getCnpj());
		clienteAtt.setRazaoSocial(cliente.getRazaoSocial());
		clienteAtt.setSite(cliente.getSite());
		clienteAtt.setSetorAtividade(cliente.getSetorAtividade());
		clienteAtt.setCategoria(cliente.getCategoria());
		clienteAtt.setGrupoEconomico(cliente.getGrupoEconomico());
		clienteAtt.setPossuiContrato(cliente.isPossuiContrato());
		clienteAtt.setDivisao(cliente.getDivisao());
		clienteAtt.setCaminhoPasta(cliente.getCaminhoPasta());
		clienteAtt.setCategoriaProblematica(cliente.getCategoriaProblematica());
		clienteAtt.setFidelizado(cliente.isFidelizado());
		clienteAtt.setMotivoFidelizacao(cliente.getMotivoFidelizacao());
		clienteAtt.setCaminhoACT(cliente.getCaminhoACT());
		clienteAtt.setCaminhoRECO(cliente.getCaminhoRECO());
		clienteAtt.setClean(cliente.isClean());
		clienteAtt.setDataFidelizacao(cliente.getDataFidelizacao());
		clienteAtt.setFidelizacaoElogio(cliente.getFidelizacaoElogio());
		return em.merge(clienteAtt);
	}
	
	@Transactional
	public Cliente updateCL(Cliente cliente) {
		
		Cliente clienteAtt = findById(cliente.getId());
		
		clienteAtt.setId(cliente.getId());
		clienteAtt.setRazaoSocial(cliente.getRazaoSocial());
		clienteAtt.setRut(cliente.getRut());
		clienteAtt.setSetorAtividade(cliente.getSetorAtividade());
		clienteAtt.setCategoria(cliente.getCategoria());
		clienteAtt.setGrupoEconomico(cliente.getGrupoEconomico());
		clienteAtt.setPossuiContrato(cliente.isPossuiContrato());
		clienteAtt.setDivisao(cliente.getDivisao());
		clienteAtt.setSite(cliente.getSite());
		clienteAtt.setCaminhoPasta(cliente.getCaminhoPasta());
		clienteAtt.setScore(cliente.getScore());
		
		return em.merge(clienteAtt);
	}
	
	@Transactional
	public Cliente updateCO(Cliente cliente) {
		
		Cliente clienteAtt = findById(cliente.getId());
		
		clienteAtt.setId(cliente.getId());
		clienteAtt.setRazaoSocial(cliente.getRazaoSocial());
		clienteAtt.setNit(cliente.getNit());
		clienteAtt.setSetorAtividade(cliente.getSetorAtividade());
		clienteAtt.setCategoria(cliente.getCategoria());
		clienteAtt.setGrupoEconomico(cliente.getGrupoEconomico());
		clienteAtt.setPossuiContrato(cliente.isPossuiContrato());
		clienteAtt.setDivisao(cliente.getDivisao());
		clienteAtt.setSite(cliente.getSite());
		clienteAtt.setCaminhoPasta(cliente.getCaminhoPasta());
		
		return em.merge(clienteAtt);
	}

	@Override
	public Cliente findByCnpj(String cnpj) {
		
		try {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Cliente> query = criteriaBuilder.createQuery(Cliente.class);
			Root<Cliente> root = query.from(Cliente.class);
			
			Path<String> cnpjPath = root.get("cnpj");
			Path<Boolean> ativoPath = root.get("ativo");
			
			Predicate categoriaIgual = criteriaBuilder.equal(cnpjPath,cnpj);
			Predicate isAtivo = criteriaBuilder.equal(ativoPath,true);
			
			query.where(categoriaIgual,isAtivo);
			
			TypedQuery<Cliente> typedQuery = em.createQuery(query);
			
			return typedQuery.getSingleResult();
			
		} catch (Exception e) {
			return null;
		}

	}
	
	public Cliente findByRUT(String rut) {
		
		try {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Cliente> query = criteriaBuilder.createQuery(Cliente.class);
			Root<Cliente> root = query.from(Cliente.class);
			
			Path<String> rutPath = root.get("rut");
			Path<Boolean> ativoPath = root.get("ativo");
			
			Predicate categoriaIgual = criteriaBuilder.equal(rutPath,rut);
			Predicate isAtivo = criteriaBuilder.equal(ativoPath,true);
			
			query.where(categoriaIgual,isAtivo);
			
			TypedQuery<Cliente> typedQuery = em.createQuery(query);
			
			return typedQuery.getSingleResult();
			
		} catch (Exception e) {
			System.out.println("Aten��o RUT: "+rut+" N�O ENCONTRADO!!");
			return null;
		}

	}
	
public Cliente findByNIT(String nit) {
		
		try {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Cliente> query = criteriaBuilder.createQuery(Cliente.class);
			Root<Cliente> root = query.from(Cliente.class);
			
			Path<String> nitPath = root.get("nit");
			Path<Boolean> ativoPath = root.get("ativo");
			
			Predicate categoriaIgual = criteriaBuilder.equal(nitPath,nit);
			Predicate isAtivo = criteriaBuilder.equal(ativoPath,true);
			
			query.where(categoriaIgual,isAtivo);
			
			TypedQuery<Cliente> typedQuery = em.createQuery(query);
			
			return typedQuery.getSingleResult();
			
		} catch (Exception e) {
			System.out.println("Aten��o NIT: "+nit+" N�O ENCONTRADO!!");
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> getAll() {
		Session session = em.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Cliente.class);
		
		criteria.add(Restrictions.eq("ativo", true));
		criteria.addOrder(Order.asc("razaoSocial"));
		
		return (List<Cliente>) criteria.list();
	}

	@Override
	public Cliente findById(Long id) {
		return em.find(Cliente.class, id);
	}

	@Override
	public Cliente loadFullEmpresa(Long id) {		
		try {
			return em.createQuery("FROM Cliente cli "
					+ "LEFT JOIN FETCH cli.contatos ce WHERE cli.id = :pId", Cliente.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			return this.findById(id);
		}
		
	}
	
	public Cliente getLastItemAdded() {

		String sQuery = "SELECT p " + "FROM Cliente p " + "WHERE p.id=(SELECT max(id) FROM Cliente)";

		return em.createQuery(sQuery, Cliente.class).getSingleResult();
	}
	
	
	public List<Cliente> getFiltroProducao(String empresa){
		
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Cliente> root = cq.from(Cliente.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (empresa != null) {
			
			predicates.add(qb.like(root.get("razaoSocial"), "%" + empresa + "%"));
		}
		System.out.println("empresa: " +empresa);
	
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		return em.createQuery(cq).getResultList();
	
	}
	
	public List<Cliente> getFiltroCliente(String empresa){
		
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Cliente> root = cq.from(Cliente.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (empresa != null) {
			
			predicates.add(qb.like(root.get("razaoSocial"), "%" + empresa + "%"));
		}
		System.out.println("empresa: " +empresa);
	
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		return em.createQuery(cq).getResultList();
	
	}

}
