package br.com.finiciativas.fseweb.repositories.impl;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.vigencia.Vigencia;
import br.com.finiciativas.fseweb.repositories.VigenciaRepository;

@Repository
public class VigenciaRepositoryImpl implements VigenciaRepository {
	
	@Autowired
	private EntityManager em;
	
	@Transactional
	@Override
	public Vigencia update(Vigencia vigencia) {
		return this.em.merge(vigencia);
	}
	
	@Transactional
	public void delete(Vigencia vigencia) {
		this.em.remove(vigencia);
	}

}
