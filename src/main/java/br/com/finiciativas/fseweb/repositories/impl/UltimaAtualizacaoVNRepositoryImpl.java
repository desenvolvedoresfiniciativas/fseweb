package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.valorNegocio.UltimaAtualizacaoVN;

@Repository
public class UltimaAtualizacaoVNRepositoryImpl {
	@Autowired
	private EntityManager em;
	
	public void create(UltimaAtualizacaoVN vn) {
		this.em.persist(vn);
	}
	
	public void update(UltimaAtualizacaoVN vn) {
		em.merge(vn);
		em.flush();
	}
	
	public UltimaAtualizacaoVN findById(Long id) {
		try {
		return em.createQuery("SELECT t FROM UltimaAtualizacaoVN t WHERE t.id = :pId", UltimaAtualizacaoVN.class)
				.setParameter("pId", id).getSingleResult();
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<UltimaAtualizacaoVN> getAll() {
		return em.createQuery("SELECT c FROM UltimaAtualizacaoVN c ORDER BY c.id", UltimaAtualizacaoVN.class)
				.getResultList();
	}
}
