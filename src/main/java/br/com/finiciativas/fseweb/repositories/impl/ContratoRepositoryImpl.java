package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.repositories.ContratoRepository;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;

@Repository
public class ContratoRepositoryImpl implements ContratoRepository {
	
	@Autowired
	private EntityManager em;
	
	@Autowired
	private HonorarioServiceImpl honorarioService;
	
	@Transactional
	@Override
	public Long create(Contrato contrato) {	
		this.em.persist(contrato);
		this.em.flush();
		return contrato.getId();
	}
	
	@Transactional
	@Override
	public Contrato update(Contrato contrato) {
		
		List<Honorario> listHonorario = new ArrayList<>();
		
		for (Honorario h : contrato.getHonorarios()) {
			Honorario honorario = new Honorario();
			honorario = this.em.find(Honorario.class, h.getId());
			listHonorario.add(honorario);
		}
		
		contrato.setHonorarios(contrato.getHonorarios());
		
		return this.em.merge(contrato);
	}
	
	public void updateHonorarios(Contrato contrato) {
		for (Honorario honorario : contrato.getHonorarios()) {
			honorario.setContrato(contrato);
			honorarioService.update(honorario);
		}
	}

	@Override
	public boolean remove(Long id) {
		try {
			this.em.remove(this.em.find(Contrato.class, id));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<Contrato> getAll() {
		return this.em.createQuery("FROM Contrato c", Contrato.class).getResultList();
	}

	@Override
	@Transactional
	public Contrato findById(Long id) {
		return this.em.find(Contrato.class, id);
	}
	
	@Override
	public List<Contrato> getContratoByCliente(Long id){
		try{
			return this.em.createQuery("FROM Contrato c JOIN FETCH c.vigencia v WHERE c.cliente.id = :pId ", Contrato.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			List<Contrato> contratos = new ArrayList<Contrato>();
			return contratos;
		}
	}
	
	public Contrato getLastAdded(){
		String sQuery = "SELECT c FROM Contrato c WHERE c.id=(SELECT max(id) FROM Contrato)";
		return em.createQuery(sQuery, Contrato.class).getSingleResult();
	}
	
	public List<Contrato> getContratoByComercial(Long id){
		return this.em.createQuery("SELECT c FROM Contrato c WHERE c.comercialResponsavelEfetivo.id = :cId", 
				Contrato.class).setParameter("cId", id).getResultList();
	}
	
	public List<Contrato> getContratoByCampanha(String dataInicio){
		return this.em.createQuery("SELECT c FROM Contrato c WHERE c.dataEntrada = :cDataInicio",
				Contrato.class).setParameter("cDataInicio", dataInicio).getResultList();
	}

	@Override
	public List<Contrato> getAllByComercial(Long id) {
		return this.em.createQuery("SELECT c FROM Contrato c WHERE c.comercialResponsavelEfetivo.id = :cId "
															+ "OR c.comercialResponsavelEntrada.id = :cId",
				Contrato.class).setParameter("cId", id).getResultList();
	}
	@Override
	public List<Contrato> getProdutosPrincipais(Long id) {
			return this.em.createQuery("SELECT c FROM Contrato c WHERE c.produtoAlvo=1 AND c.cliente.id = :pId").setParameter("pId", id).getResultList();
	}
	
	@Override
	public List<Contrato> getListaProdutosAlvoByCliente(Long id){
		return this.em.createQuery("SELECT c FROM Contrato c WHERE  c.produtoAlvo=1 AND c.cliente.id = :pId").setParameter("pId", id).getResultList();
	}
	
	@Override
	public boolean getVerificaPrincipal(Long contratoId){
		return false;
		
	}
	
	
	@Override
	public List<String> getListaNomeCombo(Long clienteId) {
		return this.em.createQuery("SELECT DISTINCT c.nomeCombo FROM Contrato c WHERE c.nomeCombo !=null AND c.combo=1 AND c.cliente.id = :pId").setParameter("pId", clienteId).getResultList();
	}

	
	
	  
	  
	
	
	
	public List<Contrato> getRelatorioCombo(Long idProduto, Long comercialRespEntrada, Long comercialRespAtual,String empresa){
		
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Contrato> root = cq.from(Contrato.class);
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(qb.equal(root.get("combo"),true));
		
		System.out.println("o id do produto �: "+ idProduto);
		
		if (idProduto != null) {
			predicates.add(qb.equal(root.get("produto").get("id"), idProduto));
		}
			
		
		if (comercialRespEntrada != null) {
			
			predicates.add(qb.equal(root.get("comercialResponsavelEntrada").get("id"), comercialRespEntrada));
		}
		
		if (comercialRespAtual != null) {
			predicates.add(qb.equal(root.get("comercialResponsavelEfetivo").get("id"), comercialRespAtual));
		}
		
		
		if (empresa != null) {
			
			predicates.add(qb.like(root.get("razaoSocialFaturar"), "%" + empresa + "%"));
		}
		
		System.out.println("o RespEntrada �: " +comercialRespEntrada);
		System.out.println("o RespAtual �: " +comercialRespAtual);

		
		cq.orderBy(qb.desc(root.get("id")));
		cq.orderBy(qb.desc(root.get("nomeCombo")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		return em.createQuery(cq).getResultList();
	
	}

	@Override
	public List<Contrato> getContratosByProdutoAndCliente(Long idProduto, String ano, Long idCliente) {
		return this.em.createQuery("SELECT c FROM Contrato c WHERE c.produto.id = :pIdProduto AND  c.cliente.id = :pId")
				.setParameter("pId", idCliente).setParameter("pIdProduto", idProduto).getResultList();
	}
	
	public List<Contrato> getContratosByTipo(String tipo){
		try {
			return em.createQuery("SELECT c FROM Contrato c WHERE c.tipo= :tipo", Contrato.class).setParameter("tipo", tipo).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	public List<Contrato> findByIdList(Long id) {
		return em.createQuery("SELECT c FROM contrato c WHERE c.id = :cId",Contrato.class).setParameter("cId", id).getResultList();
	}
	
	
}
