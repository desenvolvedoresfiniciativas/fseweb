package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoTecnica;

@Repository
public class InformacaoTecnicaRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(InformacaoTecnica informacao) {
		this.em.persist(informacao);
	}

	public void delete(InformacaoTecnica informacao) {
		this.em.remove(informacao);
	}

	@Transactional
	public InformacaoTecnica update(InformacaoTecnica informacao) {
		return this.em.merge(informacao);
	}
	
	public List<InformacaoTecnica> getAll() {
		return this.em.createQuery("SELECT distinct o FROM InformacaoTecnica o ", InformacaoTecnica.class)
				.getResultList();
	}

	public InformacaoTecnica findById(Long id) {
		return this.em.find(InformacaoTecnica.class, id);
	}

	public List<InformacaoTecnica> getInformacaoTecnicaByAcompanhamento(Long id) {
		return this.em.createQuery("SELECT distinct o FROM InformacaoTecnica o WHERE o.acompanhamento.id = :pId ", InformacaoTecnica.class)
				.setParameter("pId", id)
				.getResultList();
	}
}
