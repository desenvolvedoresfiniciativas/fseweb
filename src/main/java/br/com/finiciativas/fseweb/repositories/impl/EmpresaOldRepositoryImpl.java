package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.old.EmpresaOld;

@Repository
public class EmpresaOldRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(EmpresaOld empresaOld) {
		
		
		this.em.persist(empresaOld);
	}

	public void delete(EmpresaOld empresaOld) {
		this.em.remove(empresaOld);
	}

	@Transactional
	public EmpresaOld update(EmpresaOld empresaOld) {
		return this.em.merge(empresaOld);
	}

	public List<EmpresaOld> getAll() {
		return this.em.createQuery("SELECT distinct EO FROM EmpresaOld EO ", EmpresaOld.class)
				.getResultList();
	}

	public EmpresaOld findById(Long id) {
		return this.em.find(EmpresaOld.class, id);
	}
	
}
