package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.old.ValorNegocioOld;

@Repository
public class ValorNegocioOldRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(ValorNegocioOld valorNegocioOld) {
		
		
		this.em.persist(valorNegocioOld);
	}

	public void delete(ValorNegocioOld valorNegocioOld) {
		this.em.remove(valorNegocioOld);
	}

	@Transactional
	public ValorNegocioOld update(ValorNegocioOld valorNegocioOld) {
		return this.em.merge(valorNegocioOld);
	}

	public List<ValorNegocioOld> getAll() {
		return this.em.createQuery("SELECT distinct VNO FROM ValorNegocioOld VNO ", ValorNegocioOld.class)
				.getResultList();
	}

	public ValorNegocioOld findById(Long id) {
		return this.em.find(ValorNegocioOld.class, id);
	}
	
	
}
