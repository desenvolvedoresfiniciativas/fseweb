package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.Eficiencia;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Repository
public class EficienciaRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ValorNegocioServiceImpl valorNegocioService;
	
	@Transactional
	public void create(Eficiencia Eficiencia) {
		Eficiencia.setVn(valorNegocioService.getVNbyProducao(Eficiencia.getProducao().getId()));
		this.em.persist(Eficiencia);
	}

	@Transactional
	public void delete(Eficiencia Eficiencia) {
		this.em.remove(Eficiencia);
	}

	@Transactional
	public Eficiencia update(Eficiencia Eficiencia) {
		Eficiencia.setVn(valorNegocioService.getVNbyProducao(Eficiencia.getProducao().getId()));
		return this.em.merge(Eficiencia);
	}
	
	public List<Eficiencia> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Eficiencia o ", Eficiencia.class)
				.getResultList();
	}
	
	public Eficiencia findById(Long id) {
		return this.em.find(Eficiencia.class, id);
	}
	
	public List<Eficiencia> getEficienciaByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM Eficiencia o WHERE o.producao.id = :pId ", Eficiencia.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
	public List<Eficiencia> getEficienciaByCriteria(String ano, SetorAtividade setor, Long idFilial, Long idEquipe, Long idConsultor,String razaoSocial,Situacao situacao) {
		
		CriteriaBuilder qb = em.getCriteriaBuilder(); 
		CriteriaQuery cq = qb.createQuery(); 
		Root<Eficiencia> root = cq.from(Eficiencia.class); 

		// Constructing list of parameters 
		List<Predicate> predicates = new ArrayList<Predicate>(); 

		// Adding predicates in case of parameter not being null 

		if (ano != null) { 
		predicates.add(qb.equal(root.get("producao").get("ano"), ano)); 
		}
		
		if (setor != null) { 
		predicates.add(qb.equal(root.get("cliente").get("setorAtividade"), setor)); 
		}

		if (idFilial != null) {
		predicates.add(qb.equal(root.get("producao").get("filial").get("id"), idFilial));
		} 

		if (idEquipe != null) {
		predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), idEquipe));
		}
		
		if (idConsultor != null) { 
		predicates.add(qb.equal(root.get("consultor").get("id"), idConsultor)); 
		} 
		
		if (razaoSocial != null) { 
			predicates.add(qb.like(root.get("producao").get("cliente").get("razaoSocial"), "%" + razaoSocial + "%"));
		} 
		
		if (situacao != null) {
			System.out.println("Entrou situacao");
			predicates.add(qb.equal(root.get("producao").get("situacao"), situacao));
		}
		
		// query itself  
		cq.select(root).where(predicates.toArray(new Predicate[] {})); 

		// execute query and do something with result 
		return em.createQuery(cq).getResultList(); 
		}
	
	@Transactional
	public void deleteOfProducao(Long id){
		em.createQuery("DELETE FROM Eficiencia e WHERE e.producao.id = :pId")
				.setParameter("pId", id)
				.executeUpdate();
	}
	
	public List<Eficiencia> getEficienciaByProdAndEtapa(Long idProducao) {
		return this.em.createQuery("SELECT distinct o FROM Eficiencia o WHERE o.producao.id = :pId ", Eficiencia.class)
				.setParameter("pId", idProducao)
				.getResultList();
	}
	
	
	
}
