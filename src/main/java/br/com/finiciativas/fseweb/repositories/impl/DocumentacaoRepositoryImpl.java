package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Documentacao;

@Repository
public class DocumentacaoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Documentacao balancete) {
		this.em.persist(balancete);
	}

	public void delete(Documentacao balancete) {
		this.em.remove(balancete);
	}

	@Transactional
	public Documentacao update(Documentacao balancete) {
		return this.em.merge(balancete);
	}
	
	public List<Documentacao> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Documentacao o ", Documentacao.class)
				.getResultList();
	}

	public Documentacao findById(Long id) {
		return this.em.find(Documentacao.class, id);
	}

	public List<Documentacao> getDocumentacaoByAcompanhamento(Long id) {
		return this.em.createQuery("SELECT distinct o FROM Documentacao o WHERE o.acompanhamento.id = :pId ", Documentacao.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
	
}
