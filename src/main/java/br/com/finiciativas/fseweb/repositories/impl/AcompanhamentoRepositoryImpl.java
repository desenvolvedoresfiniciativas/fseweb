package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Acompanhamento;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Documentacao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EnvioRelatorio;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoTecnica;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Questionamentos;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Recurso;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnicoFinanceiro;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.ResultadoFinal;

@Repository
public class AcompanhamentoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Acompanhamento acompanhamento) {
		Documentacao documentacao = new Documentacao();
		documentacao.setAcompanhamento(acompanhamento);
		
		EnvioRelatorio envioRelatorio = new EnvioRelatorio();
		envioRelatorio.setAcompanhamento(acompanhamento);
		
		InformacaoCalculo informacaoCalculo = new InformacaoCalculo();
		informacaoCalculo.setAcompanhamento(acompanhamento);
		
		InformacaoTecnica informacaoTecnica = new InformacaoTecnica();
		informacaoTecnica.setAcompanhamento(acompanhamento);
		
		Questionamentos questionamentos = new Questionamentos();
		questionamentos.setAcompanhamento(acompanhamento);
		
		Recurso recurso = new Recurso();
		recurso.setAcompanhamento(acompanhamento);
		
		RelatorioTecnicoFinanceiro relatorioTecnico = new RelatorioTecnicoFinanceiro();
		relatorioTecnico.setAcompanhamento(acompanhamento);
		
		ResultadoFinal resultadoFinal = new ResultadoFinal();
		resultadoFinal.setAcompanhamento(acompanhamento);
		
		acompanhamento.setDocumentacao(documentacao);
		acompanhamento.setEnvioRelatorio(envioRelatorio);
		acompanhamento.setInformacaoCalculo(informacaoCalculo);
		acompanhamento.setInformacaoTecnica(informacaoTecnica);
		acompanhamento.setQuestionamentos(questionamentos);
		acompanhamento.setRecurso(recurso);
		acompanhamento.setRelatorioTecnico(relatorioTecnico);
		acompanhamento.setResultadoFinal(resultadoFinal);
		
		this.em.persist(documentacao);
		this.em.persist(envioRelatorio);
		this.em.persist(informacaoCalculo);
		this.em.persist(informacaoTecnica);
		this.em.persist(questionamentos);
		this.em.persist(recurso);
		this.em.persist(relatorioTecnico);
		this.em.persist(resultadoFinal);
		this.em.persist(acompanhamento);
	}

	public void delete(Acompanhamento acompanhamento) {
		this.em.remove(acompanhamento);
	}

	@Transactional
	public Acompanhamento update(Acompanhamento acompanhamento) {
		return this.em.merge(acompanhamento);
	}
	
	public List<Acompanhamento> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Acompanhamento o ", Acompanhamento.class)
				.getResultList();
	}

	public Acompanhamento findById(Long id) {
		return this.em.find(Acompanhamento.class, id);
	}

	public List<Acompanhamento> getAcompanhamentoByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM Acompanhamento o WHERE o.producao.id = :pId ", Acompanhamento.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
