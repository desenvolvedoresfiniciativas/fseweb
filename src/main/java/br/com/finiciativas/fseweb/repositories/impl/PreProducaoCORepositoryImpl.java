package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.PreProducao;
import br.com.finiciativas.fseweb.models.producao.PreProducaoCO;

@Repository
public class PreProducaoCORepositoryImpl {

	@Autowired
	private EntityManager em;

	@Transactional
	public PreProducaoCO create(PreProducaoCO balancete) {
			this.em.persist(balancete);
			em.flush();
			
			return balancete;
	}

	public void delete(PreProducaoCO balancete) {
		this.em.remove(balancete);
	}

	@Transactional
	public PreProducaoCO update(PreProducaoCO balancete) {
		return this.em.merge(balancete);
	}

	public List<PreProducaoCO> getAll() {
		return this.em.createQuery("SELECT distinct o FROM PreProducaoCO o ", PreProducaoCO.class)
				.getResultList();
	}

	public PreProducaoCO findById(Long id) {
		return this.em.find(PreProducaoCO.class, id);
	}
	
	public List<PreProducaoCO> getAllByCliente(Long id) {
		return this.em.createQuery("SELECT distinct o FROM PreProducaoCO o WHERE o.cliente.id = :pId", PreProducaoCO.class)
				.setParameter("pId", id)
				.getResultList();
	}

}
