package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Documentacao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.produto.ordenacao.OrdemItensTarefa;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;

@Repository
public class OrdemItensTarefaRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ProdutoServiceImpl produtoServiceImpl;
	
	@Autowired
	private ItemValoresServiceImpl itemValoresService;
	
	@Transactional
	public void create(OrdemItensTarefa OrdemItensTarefa) {
		em.persist(OrdemItensTarefa);
	}
	
	public void delete(Documentacao balancete) {
		this.em.remove(balancete);
	}

	public List<OrdemItensTarefa> getAll() {
		return this.em.createQuery("FROM OrdemItensTarefa i", OrdemItensTarefa.class)
				.getResultList();
	}

	@Transactional
	public OrdemItensTarefa update(OrdemItensTarefa OrdemItensTarefa) {
		return this.em.merge(OrdemItensTarefa);
	}

	public OrdemItensTarefa findById(Long id) {
		return this.em.find(OrdemItensTarefa.class, id);
	}

	public void remove(Long id) {
		this.em.remove(id);
	}
	
	public List<OrdemItensTarefa> getOrdenacaoByProduto(Long id){
		try{
			return this.em.createQuery("FROM OrdemItensTarefa i WHERE i.produto.id = :pId", OrdemItensTarefa.class)
					.setParameter("pId", id)
					.getResultList();
		}catch (Exception e) {
			return Collections.emptyList();
		}
	}
	
	public List<OrdemItensTarefa> getOrdenacaoByTarefa(Long id){
		try{
			return this.em.createQuery("FROM OrdemItensTarefa i WHERE i.tarefa.id = :pId ORDER BY i.numero", OrdemItensTarefa.class)
					.setParameter("pId", id)
					.getResultList();
		}catch (Exception e) {
			System.out.println("ERRO NA BUSCA DA ORDNACAO");
			return Collections.emptyList();
		}
	}
	
	public OrdemItensTarefa checkOrdenacaoExists(Long idItem, Long idTarefa, Long idProduto){
		
		try{
			return this.em.createQuery("FROM OrdemItensTarefa i WHERE i.item.id = :pIdItem AND i.tarefa.id =:pIdTarefa AND i.produto.id = :pIdProduto ORDER BY i.numero", OrdemItensTarefa.class)
					.setParameter("pIdItem", idItem)
					.setParameter("pIdTarefa", idTarefa)
					.setParameter("pIdProduto", idProduto)
					.getSingleResult();
		}catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public OrdemItensTarefa getLastOrdemOfTarefa(Long id){
			return this.em
					.createQuery("SELECT distinct i FROM OrdemItensTarefa i WHERE i.numero = (SELECT max(oi.numero) FROM OrdemItensTarefa oi WHERE i.tarefa.id = :pId)", OrdemItensTarefa.class)
					.setParameter("pId", id).getSingleResult();
	}
	
	@Transactional
	public void createOrdenacaoOfProduto(Long id) {
		Produto produto = produtoServiceImpl.findById(id);
		List<EtapaTrabalho> etapas = produto.getEtapasTrabalho();
		System.out.println(etapas.size());
		List<Tarefa> tarefas = new ArrayList<Tarefa>();
		
		for (EtapaTrabalho etapaTrabalho : etapas) {
			tarefas.addAll(etapaTrabalho.getTarefas());
		}
		
		for (Tarefa tarefa : tarefas) {
			int i = 0;
			List<ItemTarefa> itens = tarefa.getItensTarefa();
			for (ItemTarefa item : itens) {
				OrdemItensTarefa ordem = new OrdemItensTarefa();
				ordem.setItem(item);
				ordem.setProduto(produto);
				ordem.setTarefa(tarefa);
				ordem.setNumero(i);
				
				create(ordem);
				
				i++;
			}
		}
		
	}
	
	@Transactional
	public void ordenaEmMassa(List<OrdemItensTarefa> ordenacao){
		for (OrdemItensTarefa ordemItem : ordenacao) {
			List<ItemValores> itens = itemValoresService.findByItemAndTarefaId(
					ordemItem.getItem().getId(), ordemItem.getTarefa().getId());
			for (ItemValores item : itens) {
				item.setNumeroOrdem(ordemItem.getNumero());

				itemValoresService.update(item);
			}
		}
	}
	
}
