package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.repositories.FilialRepository;

@Repository
public class FilialRepositoryImpl implements FilialRepository {
	
	@Autowired
	private EntityManager em;
	
	@Override
	public List<Filial> getAll() {
		return em
				.createQuery("SELECT distinct f FROM Filial f "
						+ "LEFT JOIN FETCH f.consultores consul ORDER BY f.nome", Filial.class)
				.getResultList();
	} 
	
	@Transactional
	@Override
	public void create(Filial filial) {		
		filial.setAtivo(true);
		this.em.persist(filial);
	}
	
	@Transactional
	@Override
	public Filial update(Filial filial) {		
		return this.em.merge(filial);
	} 

	@Override
	public Filial findById(Long id) {
		return this.em.find(Filial.class,id);
	}

	@Override
	public void remove(Long id) {
		Filial filial = this.findById(id);
		filial.setAtivo(false);
		this.update(filial);
	}

	@Override
	public List<Consultor> getConsultores(Long idFilial) {
		return em.createQuery("SELECT consultores " 
				+ "FROM Filial f "
				+ "LEFT JOIN f.consultores consultores WHERE f.id = :pId ORDER BY consultores.nome", Consultor.class)
				.setParameter("pId", idFilial)
				.getResultList();
	}
	
	public Filial findByNome(String nome){
		try {
		return em.createQuery("SELECT f FROM Filial f WHERE f.nome = :pNome", Filial.class)
				.setParameter("pNome", nome)
				.getSingleResult();
		} catch (Exception e) {
			System.out.println("Busca de Filial por Nome n�o encontrou nada");
			return null;
		}
	}

}
