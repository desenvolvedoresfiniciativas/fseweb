package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.consultor.Role;
import br.com.finiciativas.fseweb.repositories.RoleRepository;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

	@Autowired
	private EntityManager em;

	public List<Role> getAll() {
		return em.createQuery("SELECT r FROM Role r ORDER BY r.descricao", Role.class).getResultList();
	}

}
