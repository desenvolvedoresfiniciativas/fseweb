package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Role;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;

@Repository
public class EtapasConclusaoRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;
	
	@Autowired
	private ProducaoServiceImpl producaoServiceImpl;

	public List<EtapasConclusao> getAll() {
		return this.em.createQuery("SELECT distinct o FROM EtapasConclusao o ", EtapasConclusao.class).getResultList();
	}

	@Transactional
	public void create(EtapasConclusao EtapasConclusao) {
		this.em.merge(EtapasConclusao);
	}

	@Transactional
	public void deleteOfProducao(Long id) {
		em.createQuery("DELETE FROM EtapasConclusao e WHERE e.producao.id = :pId").setParameter("pId", id)
				.executeUpdate();
	}

	@Transactional
	public void delete(EtapasConclusao EtapasConclusao) {
		this.em.remove(EtapasConclusao);
	}

	@Transactional
	public EtapasConclusao update(EtapasConclusao EtapasConclusao) {
		try {
			System.out.println("Entrou no rep");
			return em.merge(EtapasConclusao);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public EtapasConclusao findById(Long id) {
		return this.em.find(EtapasConclusao.class, id);
	}

	public EtapasConclusao getEtapasConclusaoByProducaoAndEtapa(Long idProd, Long idEtapa) {
		try {
			System.out.println("Sucesso no metodo EtapasConclusaoRepositoryImpl.getEtapasConclusaoByProducaoAndEtapa");
			return this.em.createQuery(
					"SELECT distinct o FROM EtapasConclusao o WHERE o.producao.id = :pIdProd AND o.etapa.id = :pIdEtapa",
					EtapasConclusao.class).setParameter("pIdProd", idProd).setParameter("pIdEtapa", idEtapa)
					.getSingleResult();
		} catch (Exception e) {
			System.out.println("Erro no metodo EtapasConclusaoRepositoryImpl.getEtapasConclusaoByProducaoAndEtapa");
			EtapasConclusao EtapasConclusao = new EtapasConclusao();
			return EtapasConclusao;
		}
	}

	public List<EtapasConclusao> getAllEtapasConclusaoByProducao(Long id) {
		try {
			return this.em.createQuery(
					"SELECT o FROM EtapasConclusao o WHERE o.producao.id = :pId ORDER BY o.etapa.nome",
					EtapasConclusao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<EtapasConclusao> EtapasConclusao = new ArrayList<EtapasConclusao>();
			return EtapasConclusao;
		}
	}

	public EtapasConclusao getEtapaRelatorioByProducao(Long id) {
		try {
			return this.em.createQuery(
					"SELECT distinct o FROM EtapasConclusao o WHERE o.etapa.id = :pEtapaId AND o.producao.id = :pId ORDER BY o.etapa.nome",
					EtapasConclusao.class).setParameter("pId", id).setParameter("pEtapaId", 67l).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	@Transactional
	public void createEtapasConclusaoByProd(Producao producao) {
		List<EtapasConclusao> conclusion = getAllEtapasConclusaoByProducao(producao.getId());
		List<EtapaTrabalho> etapas = producao.getProduto().getEtapasTrabalho();
		for(EtapaTrabalho e : etapas) {
			System.out.println(e.getNome());
		}
		for(EtapaTrabalho e : etapas) {
			Boolean validate = false;
			for(EtapasConclusao c : conclusion) {
				if (c.getEtapa().getId() == e.getId()) {
					validate = true;
					System.out.println("valido");
				}
			}
			if(!validate){
				EtapasConclusao etapasConclusao = new EtapasConclusao();
				etapasConclusao.setEtapa(e);
				etapasConclusao.setProducao(producao);
				etapasConclusao.setConcluido(false);
				create(etapasConclusao);
			}
		}
		
	}

	public EtapasConclusao getLastItemAdded() {
		String sQuery = "SELECT p " + "FROM EtapasConclusao p " + "WHERE p.id=(SELECT max(id) FROM EtapasConclusao)";

		return em.createQuery(sQuery, EtapasConclusao.class).getSingleResult();
	}

	public List<EtapasConclusao> getAllEtapasOfConsultorByAno(String ano, Long id) {
		try {
			return this.em.createQuery(
					"SELECT distinct o FROM EtapasConclusao o WHERE o.producao.ano = :pAno AND o.producao.consultor.id = :pId AND (o.producao.situacao = 'ATIVA' OR o.producao.situacao = 'PREVISAO_ATIVA')",
					EtapasConclusao.class).setParameter("pAno", ano).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			List<EtapasConclusao> EtapasConclusao = new ArrayList<EtapasConclusao>();
			return EtapasConclusao;
		}
	}

	public List<EtapasConclusao> getAllEtapasOfEquipeByAno(String ano, Long id) {
		try {
			return this.em.createQuery(
					"SELECT distinct o FROM EtapasConclusao o WHERE o.producao.ano = :pAno AND o.producao.equipe.lider.id  = :pId AND (o.producao.situacao = 'ATIVA' OR o.producao.situacao = 'PREVISAO_ATIVA')",
					EtapasConclusao.class).setParameter("pAno", ano).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			List<EtapasConclusao> EtapasConclusao = new ArrayList<EtapasConclusao>();
			return EtapasConclusao;
		}
	}

	public List<EtapasConclusao> getAllEtapasOfDiretorByAno(String ano) {
		try {
			return this.em.createQuery(
					"SELECT distinct o FROM EtapasConclusao o WHERE o.producao.ano = :pAno AND (o.producao.situacao = 'ATIVA' OR o.producao.situacao = 'PREVISAO_ATIVA')",
					EtapasConclusao.class).setParameter("pAno", ano).getResultList();
		} catch (Exception e) {
			List<EtapasConclusao> EtapasConclusao = new ArrayList<EtapasConclusao>();
			return EtapasConclusao;
		}
	}

//	public List<EtapasConclusao> getAllEtapasByProduto(Long id){
//		String sQuery = "SELECT p " + "FROM EtapasConclusao p JOIN FETCH p.producao WHERE p.producao.produto.id = :pId AND (p.producao.situacao = 'ATIVA' OR p.producao.situacao = 'PREVISAO_ATIVA')";
//
//		return em.createQuery(sQuery, EtapasConclusao.class)
//				.setParameter("pId", id)
//				.getResultList();
//	}

	public List<EtapasConclusao> getAllEtapasByProduto(Long id, String ano) {
		String sQuery = "SELECT p " + "FROM EtapasConclusao p " + "WHERE p.producao.cliente.ativo = 1 "
				+ "AND p.producao.produto.id = :pId "
//				+ "AND p.producao.ano = :pAno "
				+ "AND (p.producao.situacao = 'ATIVA' OR p.producao.situacao = 'PREVISAO_ATIVA' OR p.producao.situacao = 'NEGOCIACAO')";

		return em.createQuery(sQuery, EtapasConclusao.class).setParameter("pId", id)
//				.setParameter("pAno", "2021")
				.getResultList();
	}

	// WIP: M�TODO PARA BUSCAR PONTO SITUA��O SOB DEMANDA
	public List<EtapasConclusao> getAllEtapasByProdutoAndCriteria(Long idProduto, String ano, Long idFilial,
			Long idEquipe, Long idConsultor) {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<EtapasConclusao> root = cq.from(EtapasConclusao.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		predicates.add(qb.equal(root.get("producao").get("produto").get("id"), idProduto));

		if (ano != null) {
			predicates.add(qb.equal(root.get("producao").get("ano"), ano));
		}
		if (idFilial != null) {
			predicates.add(qb.equal(root.get("producao").get("filial").get("id"), idFilial));
		}
		if (idEquipe != null) {
			predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), idEquipe));
		}
		if (idConsultor != null) {
			predicates.add(qb.equal(root.get("producao").get("consultor").get("id"), idConsultor));
		}

		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}

	public List<EtapasConclusao> getAllEtapasByProdutoAndEquipe(Long id, Long idEquipe, String ano) {
		String sQuery = "SELECT p " + "FROM EtapasConclusao p " + "WHERE p.producao.cliente.ativo = 1 "
				+ "AND p.producao.produto.id = :pId " + "AND p.producao.equipe.id = :pIdEquipe "
//				+ "AND p.producao.ano = :pAno "
				+ "AND (p.producao.situacao = 'ATIVA' OR p.producao.situacao = 'PREVISAO_ATIVA' OR p.producao.situacao = 'NEGOCIACAO')";

		return em.createQuery(sQuery, EtapasConclusao.class).setParameter("pId", id).setParameter("pIdEquipe", idEquipe)
//				.setParameter("pAno", "2021")
				.getResultList();
	}

	public EtapasConclusao getEtapasBalanceteProducao(Long id) {
		return this.em
				.createQuery("SELECT f FROM EtapasConclusao f WHERE f.etapa.id= :pIdEtapaT AND  f.producao.id  = :pId",
						EtapasConclusao.class)
				.setParameter("pIdEtapaT", 66l).setParameter("pId", id).getSingleResult();
	}

	public EtapasConclusao getEtapasRelatorioProducao(Long id) {
		return this.em
				.createQuery("SELECT f FROM EtapasConclusao f WHERE f.etapa.id= :pIdEtapaT AND  f.producao.id  = :pId",
						EtapasConclusao.class)
				.setParameter("pIdEtapaT", 67l).setParameter("pId", id).getSingleResult();
	}
	
	public List<EtapasConclusao> getAllEtapasByAno(String ano) {
		try {
			return this.em.createQuery("SELECT distinct o FROM EtapasConclusao o WHERE o.producao.ano = :pAno",
					EtapasConclusao.class)
					.setParameter("pAno", ano)
					.getResultList();
		} catch (Exception e) {
			List<EtapasConclusao> EtapasConclusao = new ArrayList<EtapasConclusao>();
			return EtapasConclusao;
		}
	}

	public List<EtapasConclusao> getAllEtapasByCriteriaAcompanhamento(String campanha, String razaoSocial,
			String tipoNegocio, Long idProduto, Long idFilial, Long idEtapa) {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<EtapasConclusao> root = cq.from(EtapasConclusao.class);

		
		
		// Constructing list of parameters
		List<Predicate> predicates = new ArrayList<Predicate>();
		

		
		//condi��o para pegar apenas empresas dispon�veis para faturamento
		predicates.add(qb.equal(root.get("producao").get("produto").get("id"), 23l));
		Predicate ativa = (qb.equal(root.get("producao").get("situacao"), Situacao.ATIVA));
		Predicate previsaoAtiva = (qb.equal(root.get("producao").get("situacao"), Situacao.PREVISAO_ATIVA));
		Predicate negociacao = (qb.equal(root.get("producao").get("situacao"), Situacao.NEGOCIACAO));
		predicates.add(qb.or(ativa, previsaoAtiva, negociacao));
		
		// Adding predicates in case of parameter not being null
		if (razaoSocial != null && razaoSocial != "") {
			System.out.println("entrou no raz�o social");
			predicates.add(qb.like(root.get("producao").get("cliente").get("razaoSocial"), '%' + razaoSocial + '%'));
		}

		if (campanha != null && campanha != "") {
			System.out.println("entrou no ano");
			predicates.add(qb.equal(root.get("producao").get("ano"), campanha));
		}
		
		if (tipoNegocio != null && tipoNegocio != "") {
			System.out.println("entrou no tipoNegocio");
			predicates.add(qb.equal(root.get("producao").get("tipoDeNegocio"), tipoNegocio));
		}
		
		if (idFilial != null) {
			System.out.println("entrou no ano");
			predicates.add(qb.equal(root.get("producao").get("filial").get("id"), idFilial));
		}
		
		


		

		
		// query itself
		cq.orderBy(qb.desc(root.get("producao").get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}

//	public void changeFinalizacaoEtapa(boolean finalizado, EtapasConclusao etapas) {
//		
//		if(finalizado==true){
//			etapas.setConcluido(true);
//			faturamentoService.
//		}
//		
//	}
	
	@Transactional
	public void addEtapaInProductions() {
		List<Producao> prods = producaoServiceImpl.getAll();
		
		for(Producao prod : prods) {
			createEtapasConclusaoByProd(prod);
		}
	}
}
