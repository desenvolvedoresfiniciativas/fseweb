package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.valorNegocioCO.ValorAprobadoCo;

import br.com.finiciativas.fseweb.models.valorNegocioCO.ValorEstimadoCo;
import br.com.finiciativas.fseweb.models.valorNegocioCO.ValorPostuladoCo;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;

import javax.transaction.Transactional;

@Repository
public class ValorNegocioCORepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ItemValoresServiceImpl itemValoresServiceImpl;

	@Autowired
	private ProducaoServiceImpl producaoServiceImpl;


	// Metodos para guardar en las tablas valor_aprovado, valor_estimado,
	// valor_postulado
	// (Inicio)-----------------------------------------------------------------------------------
	@Transactional
	public void guardarPersupuestoEstimado() {
		List<Producao> produccion = producaoServiceImpl.getAll();
		// recorrer las producciones
		for (Producao p : produccion) {
			// Obtener el id para consultar el producao_id en item_valores
			Long idProduccion = p.getId();
			
			//Limpiamos la tabla para agregar de nuevo los valores
			eliminarProduccionValorEstimadoById(idProduccion);

			// Busco en la tabla item_valores con el id de la produccion para encontrar los
			// valores colocados en los items valores
			List<ItemValores> arrValoresEstimados = itemValoresServiceImpl
					.findByItemIdAndProducaoValoresEstimados(idProduccion);

			// Obtener anio fiscal y campania
			Integer incrementoAnioFiscal = Integer.valueOf(arrValoresEstimados.get(0).getProducao().getAno());
			String anioCampania = arrValoresEstimados.get(0).getProducao().getAno();
			
			if (!(arrValoresEstimados.isEmpty())) {
				// obtener valores estimados y guardarlos en la tabla valor_estimados
				for (ItemValores valorEstimado : arrValoresEstimados) {

					// Objeto valor_estimado a guardar en tabla valor_estimado_co
					ValorEstimadoCo valorE = new ValorEstimadoCo();

					// si el valor no esta vacio agregue en valor_estimado
					if (!valorEstimado.getValor().equals("")) {
						valorE.setProducao(p);
						valorE.setContrato(p.getContrato());
						valorE.setValor(valorEstimado.getValor());
						valorE.setAnoFiscal(String.valueOf(incrementoAnioFiscal));
						valorE.setCampanha(anioCampania);
						guardarValorEstimado(valorE);
					} else System.out.println("value empty -> item_id: " + valorEstimado.getItem().getId() + " produccio'n:  " + valorEstimado.getProducao().getId());
					incrementoAnioFiscal++;
				}
			} else System.out.println("El arreglo ItemValores de valor esta' vaci'o.");
		}
	}

	@Transactional
	public void guardarPresupuestoPostulado() {
		List<Producao> producciones = producaoServiceImpl.getAll();
		for( Producao p : producciones) {
			Long idProduccion = p.getId();
			eliminarProduccionValorPostuladoById(idProduccion);
			List<ItemValores> arrValoresPostulados = itemValoresServiceImpl.findByItemIdAndProducaoValoresPostulados(idProduccion);
			Integer incrementoAnioFiscal = Integer.valueOf(arrValoresPostulados.get(0).getProducao().getAno());
			String anioCampania = arrValoresPostulados.get(0).getProducao().getAno();
			if (!(arrValoresPostulados.isEmpty())) {
				for( ItemValores vp : arrValoresPostulados) {
					ValorPostuladoCo valorP = new ValorPostuladoCo();
					if (!vp.getValor().equals("")) {
						valorP.setProducao(p);
						valorP.setContrato(p.getContrato());
						valorP.setValor(vp.getValor());
						valorP.setAnoFiscal(String.valueOf(incrementoAnioFiscal));
						valorP.setCampanha(anioCampania);
						guardarValorPostulado(valorP);
					} else System.out.println("value empty -> item_id: " + vp.getItem().getId() + " produccio'n:  " + vp.getProducao().getId());
					incrementoAnioFiscal++;
				}
			} else System.out.println("El arreglo ItemValores de valor esta' vaci'o.");
		}
	}

	@Transactional
	public void guardarPresupuestoAprobado() {
		List<Producao> producciones = producaoServiceImpl.getAll();
		for( Producao p : producciones) {
			Long idProduccion = p.getId();
			eliminarProduccionValorAprobadoById(idProduccion);
			List<ItemValores> arrValoresAprovados = itemValoresServiceImpl.findByItemIdAndProducaoValoresAprobados(idProduccion);
			Integer incrementoAnioFiscal = Integer.valueOf(arrValoresAprovados.get(0).getProducao().getAno());
			String anioCampania = arrValoresAprovados.get(0).getProducao().getAno();
			if (!(arrValoresAprovados.isEmpty())) {
				for( ItemValores va : arrValoresAprovados) {
					ValorAprobadoCo valorA = new ValorAprobadoCo();
					if (!va.getValor().equals("")) {
						valorA.setProducao(p);
						valorA.setValor(va.getValor());
						valorA.setAnoFiscal(String.valueOf(incrementoAnioFiscal));
						valorA.setCampanha(anioCampania);
						guardarValorAprobado(valorA);
					} else System.out.println("value empty -> item_id: " + va.getItem().getId() + " produccio'n:  " + va.getProducao().getId());
					incrementoAnioFiscal++;
				}
			} else System.out.println("El arreglo ItemValores de valor esta' vaci'o.");
		}
	}
	// (Fin)--------------------------------------------------------------------------------------
	
	
	// Metodos para eliminar valores en tabla ValorEstimadoCo, ValorPostuladoCo, ValorAprobadoCo
	// (Inicio)-----------------------------------------------------------------------------------
	@Transactional
	public void eliminarProduccionValorEstimadoById(Long id) {
		this.em.createNativeQuery("DELETE FROM valor_estimado_co WHERE producao_id = " + id.toString()).executeUpdate();
	}
	@Transactional
	public void eliminarProduccionValorPostuladoById(Long id) {
		this.em.createNativeQuery("DELETE FROM valor_postulado_co WHERE producao_id = " + id.toString()).executeUpdate();
	}
	@Transactional
	public void eliminarProduccionValorAprobadoById(Long id) {
		this.em.createNativeQuery("DELETE FROM valor_aprobado_co WHERE producao_id = " + id.toString()).executeUpdate();
	}
	// (Fin)--------------------------------------------------------------------------------------

	
	// Metodos a guardar en las tablas
	// * valor_estimado, * valor_postulado, * valor_aprobado
	// (Inicio)-----------------------------------------------------------------------------------
	@Transactional
	public void guardarValorEstimado(ValorEstimadoCo valorEstimado) {
		this.em.persist(valorEstimado);
	}
	public void guardarValorPostulado(ValorPostuladoCo valorPostulado) {
		this.em.persist(valorPostulado);
	}
	public void guardarValorAprobado(ValorAprobadoCo valorPostulado) {
		this.em.persist(valorPostulado);
	}
	// (Fin)--------------------------------------------------------------------------------------
	
	
	/**
	 * 
	 * Metodo para generar Valor de Negocio Colombia:
	 * 		Este metodo toma los valores de itemValores de las producciones 
	 * 		y llena estos datos en las tablas ValorEstimado, ValorPostulado, ValorAprovado 
	 */
	public void generarValorNegocioColombia() {
		System.out.println("Items Estimados");
		guardarPersupuestoEstimado();
		System.out.println("Items Postulados");
		guardarPresupuestoPostulado();
		System.out.println("Items Aprovados");
		guardarPresupuestoAprobado();
	}

}
