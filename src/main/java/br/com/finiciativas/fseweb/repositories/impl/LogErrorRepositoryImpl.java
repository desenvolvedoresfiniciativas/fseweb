package br.com.finiciativas.fseweb.repositories.impl;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.LogError;

@Repository
public class LogErrorRepositoryImpl {
	
	@Autowired
	private EntityManager em;
	
	public void delete(LogError logError) {
		this.em.remove(logError);
	}
	
	@Transactional
	public void create(LogError logError) {
		em.persist(logError);
    }
	
	public LogError findById(Long id) {
		return this.em.find(LogError.class, id);
	}
	
}
