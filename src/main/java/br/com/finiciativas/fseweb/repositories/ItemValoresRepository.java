package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.produto.ItemValores;

public interface ItemValoresRepository {

	void create(ItemValores item);
	
	List<ItemValores> getAll();
	
	ItemValores update(ItemValores item);
	
	ItemValores findById(Long id);
	
	List<ItemValores> findByTarefaId(Long id);
	
	List<ItemValores> findByTarefaAndProducao(Long id,Long idProd);
	
}
