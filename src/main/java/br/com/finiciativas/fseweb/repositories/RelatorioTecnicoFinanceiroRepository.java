package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnicoFinanceiro;

public interface RelatorioTecnicoFinanceiroRepository {

	void create(RelatorioTecnicoFinanceiro RelatorioTecnicoFinanceiro);

	void delete(RelatorioTecnicoFinanceiro RelatorioTecnicoFinanceiro);
		
	RelatorioTecnicoFinanceiro update(RelatorioTecnicoFinanceiro RelatorioTecnicoFinanceiro);
	
	List<RelatorioTecnicoFinanceiro> getAll();
	
	RelatorioTecnicoFinanceiro findById(Long id);
	
	List<RelatorioTecnicoFinanceiro> getRelatorioTecnicoFinanceiroByAcompanhamento(Long id);
	
}
