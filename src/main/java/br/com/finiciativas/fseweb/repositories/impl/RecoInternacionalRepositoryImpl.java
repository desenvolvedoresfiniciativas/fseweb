package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.IeM.RecoInternacional;

@Repository
public class RecoInternacionalRepositoryImpl {
	
	@Autowired
	private EntityManager em;

	@Transactional
	public void create(RecoInternacional reco) {
		
		reco.setOportunidade("Reco Internacional");
		
		this.em.persist(reco);
	}

	public void delete(RecoInternacional reco) {
		this.em.remove(reco);
	}
	
	@Transactional
	public RecoInternacional update(RecoInternacional reco) {
		return this.em.merge(reco);
	}
	
	public List<RecoInternacional> getAll() {
		return this.em.createQuery("SELECT distinct o FROM RecoInternacional o ", RecoInternacional.class)
				.getResultList();
	}
	
	public RecoInternacional findById(Long id) {
		return this.em.find(RecoInternacional.class, id);
	}
	
	

}
