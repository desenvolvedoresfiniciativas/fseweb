package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.old.NotaFiscalOld;

@Repository
public class NotaFiscalOldRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(NotaFiscalOld notaFiscalOld) {
		
		
		this.em.persist(notaFiscalOld);
	}

	public void delete(NotaFiscalOld notaFiscalOld) {
		this.em.remove(notaFiscalOld);
	}

	@Transactional
	public NotaFiscalOld update(NotaFiscalOld notaFiscalOld) {
		return this.em.merge(notaFiscalOld);
	}

	public List<NotaFiscalOld> getAll() {
		return this.em.createQuery("SELECT distinct NFO FROM NotaFiscalOld NFO", NotaFiscalOld.class)
				.getResultList();
	}

	public NotaFiscalOld findById(Long id) {
		return this.em.find(NotaFiscalOld.class, id);
	}
	
	
}
