package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Questionamentos;

public interface QuestionamentosRepository {

	void create(Questionamentos questionamento);

	void delete(Questionamentos questionamento);
		
	Questionamentos update(Questionamentos questionamento);
	
	List<Questionamentos> getAll();
	
	Questionamentos findById(Long id);
	
	List<Questionamentos> getQuestionamentosByProducao(Long id);
	
}
