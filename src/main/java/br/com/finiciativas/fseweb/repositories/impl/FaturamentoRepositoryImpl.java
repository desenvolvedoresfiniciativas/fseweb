package br.com.finiciativas.fseweb.repositories.impl;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import javax.mail.Session;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.swing.text.MaskFormatter;
import javax.transaction.Transactional;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.EstadoEtapa;
import br.com.finiciativas.fseweb.enums.TipoDeApuracao;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.honorario.FaixasEscalonado;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.AcompanhamentoExTarifario;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EnderecoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaPagamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ReceptoresEmailServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;

@Repository
public class FaturamentoRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private HonorarioServiceImpl honorarioService;

	@Autowired
	private EtapaPagamentoServiceImpl etapaPagamentoService;

	@Autowired
	private ContatoClienteServiceImpl ContatoClienteService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private EnderecoClienteServiceImpl enderecoService;

	@Autowired
	private ContatoClienteServiceImpl contatoClienteService;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ItemValoresServiceImpl itemValoresService;

	@Autowired
	private ReceptoresEmailServiceImpl receptoresEmailService;

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Autowired
	private SubProducaoServiceImpl subProducaoService;
	
	@Autowired
	private BalanceteCalculoServiceImpl balanceteCalculoService;

	public List<Faturamento> getAll() {
		return em.createQuery("SELECT f FROM Faturamento f", Faturamento.class).getResultList();
	}

	public List<Faturamento> getFatNaoLiberado() {
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.liberado = 0", Faturamento.class).getResultList();
	}

	public List<Faturamento> getFatLiberado() {
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.liberado = 1", Faturamento.class).getResultList();
	}

	public void unlinkFaturamentoOfProducao(Long id) {
		List<Faturamento> fats = getAllByProducao(id);
		for (Faturamento faturamento : fats) {
			faturamento.setProducao(null);
			update(faturamento);
		}
	}

	public List<Faturamento> getFatNaoLiberadoByCriteria(String razaoSocial, String ano, Long idProduto,
			EstadoEtapa estado, String tpNegocio, Long etapaId, String anoFiscal) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Faturamento> root = cq.from(Faturamento.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		predicates.add(qb.equal(root.get("liberado"), false));
		predicates.add(qb.notEqual(root.get("estado"), "CONSOLIDADO"));

		if (razaoSocial != null) {
			predicates.add(qb.like(root.get("cliente").get("razaoSocial"), '%' + razaoSocial + '%'));
		}
		if (ano != null) {
			predicates.add(qb.equal(root.get("campanha"), ano));
		}
		if (anoFiscal != null) {
			predicates.add(qb.equal(root.get("anoFiscal"), anoFiscal));
		}
		if (idProduto != null) {
			predicates.add(qb.equal(root.get("produto").get("id"), idProduto));
		}
		if (estado != null) {
			predicates.add(qb.equal(root.get("estado"), estado.displayName()));
		}
		if (tpNegocio != null) {
			predicates.add(qb.equal(root.get("producao").get("tipoDeNegocio"), tpNegocio));
		}
		if (etapaId != null) {
			predicates.add(qb.equal(root.get("etapa").get("id"), etapaId));
		}

		cq.orderBy(qb.desc(root.get("cliente").get("razaoSocial")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		return em.createQuery(cq).getResultList();

	}

	public List<Faturamento> getAllByProducao(Long id) {
		return this.em.createQuery("SELECT distinct f FROM Faturamento f WHERE f.producao.id = :pId", Faturamento.class)
				.setParameter("pId", id).getResultList();
	}

	public List<Faturamento> getAllByContrato(Long id) {
		return this.em.createQuery("SELECT distinct f FROM Faturamento f WHERE f.contrato.id = :pId", Faturamento.class)
				.setParameter("pId", id).getResultList();
	}

	public List<Faturamento> getAllByProducaoNotConsolidado(Long id) {
		return this.em.createQuery(
				"SELECT  f FROM Faturamento f WHERE f.producao.id = :pId AND (f.estado != 'Consolidado' AND f.consolidado = :pConsolidado) ORDER BY f.balancete.versao ",
				Faturamento.class).setParameter("pId", id).setParameter("pConsolidado", false).getResultList();
	}

	public List<Faturamento> getLastFaturamentoByBalancete(Long id) {
		try {
			String sQuery = "SELECT f FROM Faturamento f WHERE f.balancete.id = :pId AND f.id=(SELECT max(id) FROM Faturamento)";
			return em.createQuery(sQuery, Faturamento.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	public List<Faturamento> getContrato(Long idContrato) {
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.contrato.id = :cId", Faturamento.class)
				.setParameter("cId", idContrato).getResultList();
	}

	@Transactional
	public void create(Faturamento faturamento) {
		System.out.println(faturamento.getEstado());
		if (!faturamento.getEstado().equalsIgnoreCase("ETAPA OPERACIONAL FINALIZADA")) {
			faturamento.setEstado(EstadoEtapa.AGUARDANDO_ETAPA.displayName());
		}
		if (faturamento.getContrato() != null) {
			faturamento.setProduto(faturamento.getContrato().getProduto());
			faturamento.setRazaoSocialFaturar(faturamento.getContrato().getRazaoSocialFaturar());
			faturamento.setCnpjFaturar(faturamento.getContrato().getCnpjFaturar());
		}
		if (faturamento.getProducao() != null) {
			faturamento.setCampanha(faturamento.getProducao().getAno());
		}
		faturamento.setEmitido(false);
		faturamento.setConsolidado(false);
		faturamento.setConsolidadoPorFat(false);
		faturamento.setEnviadoAoCliente(false);
		em.persist(faturamento);
		em.flush();
	}

	@Transactional
	public void remove(Long id) {
		Faturamento faturamento = this.em.find(Faturamento.class, id);
		em.remove(faturamento);
	}

	@Transactional
	public Faturamento update(Faturamento faturamento) {
		if (faturamento.getConsolidado() != true) {
			faturamento.setConsolidado(false);
		}
		if (faturamento.getConsolidadoPorFat() != null) {
			if(faturamento.getConsolidadoPorFat() != true) {
				faturamento.setConsolidadoPorFat(false);
			}
		}else {
			faturamento.setConsolidadoPorFat(false);
		}
		
		if (faturamento.getEnviadoAoCliente() != null) {
			if (faturamento.getEnviadoAoCliente() != true) {
				faturamento.setEnviadoAoCliente(false);
			}
		} else {
			faturamento.setEnviadoAoCliente(false);
		}
		return em.merge(faturamento);
	}

	public Faturamento findById(Long id) {
		return em.find(Faturamento.class, id);
	}

	public List<Faturamento> getGestao() {
		return em.createQuery("SELECT c FROM Faturamento c", Faturamento.class).getResultList();
	}

	public List<Faturamento> getAllFaturamentoAnterioresByProducao(Long id) {
		return em.createQuery("SELECT c FROM Faturamento c WHERE c.producao.id = :pId", Faturamento.class)
				.setParameter("pId", id).getResultList();
	}

	public List<Faturamento> getAllFaturamentoByClienteAndCampanha(Long id, String ano) {
		try {
			return em.createQuery(
					"SELECT c FROM Faturamento c WHERE c.cliente.id = :pIdCliente AND c.producao.ano = :pAno AND c.estado != 'CONSOLIDADO'",
					Faturamento.class).setParameter("pIdCliente", id).setParameter("pAno", ano).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Transactional
	public Faturamento geraFaturamento(BalanceteCalculo balanceteObj, float percent, EtapaTrabalho etapa,
			EtapasConclusao etapas) {

		try {

			Producao producao = etapas.getProducao();
			BigDecimal teto = BigDecimal.valueOf(0);
			String obs = "Honor�rios: ";
			BigDecimal valorTotal = BigDecimal.valueOf(0);

			System.out.println("Produ��o recebida para faturamento com o ID: " + producao.getId());

			Faturamento faturamento = new Faturamento();
			Contrato contrato = producao.getContrato();

			List<ContatoCliente> contatos = ContatoClienteService.getContatosCliente(contrato.getCliente().getId());
			int contatoNF = 0;
			for (ContatoCliente contatoCliente : contatos) {
				if (contatoCliente.isPessoaNfBoleto()) {
					contatoNF++;
				}
			}

			System.out.println("Contatos com recebimento de nota " + contatoNF);

			List<Honorario> honorarios = contrato.getHonorarios();
			for (Honorario honorario : honorarios) {
				System.out.println(honorario.getNome());

				if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
					System.out.println("Bateu com o nome Percentual Escalonado");

					if (contatoNF > 0) {

						PercentualEscalonado pe = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());

						String stringValorBalancete = balanceteObj.getReducaoImposto();
						if (Objects.nonNull(pe)) {

							teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							stringValorBalancete = stringValorBalancete.replace(".", "");
							stringValorBalancete = stringValorBalancete.replace(",", ".");
							stringValorBalancete = stringValorBalancete.trim();
							BigDecimal balancete = new BigDecimal(stringValorBalancete);
							BigDecimal temp = new BigDecimal(stringValorBalancete);

							for (FaixasEscalonado faixa : faixas) {

								System.out.println("Temp atual " + temp);

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								// BigDecimal gap2 = temp.subtract(inicial);
								Float percentual = faixa.getValorPercentual();

								System.out.println("Faixa: " + inicial + " ... " + finalF);
								System.out.println("Diferen�a: " + gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(BigDecimal.ZERO) != 0) {
									if (temp.compareTo(gap1) >= 0) {

										System.out.println("Passa direto da faixa, calcula com o f...");

										valorCalc = valorCalc.add(gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

										System.out.println("Saldo para soma :" + gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										// Se est� entre a faixa, calcula com o temp direto na porcentagem.
									} else if (temp.compareTo(gap1) < 0) {

										valorCalc = valorCalc.add(temp
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										BigDecimal zero = BigDecimal.ZERO;
										temp = zero;
									}
								}
							}

							if (pe.getAcimaDe() != null) {
								if (balancete.compareTo(pe.getAcimaDe()) > 0) {
									if (temp.compareTo(BigDecimal.ZERO) > 0) {
										System.out.println("Ainda resta Temp: " + temp);
										System.out.println("Entrando no Acima de e aplicando " + pe.getPercentAcimaDe()
												+ " ao temp");
										BigDecimal acima = temp.multiply(
												BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
										valorCalc = valorCalc.add(acima);
									}
								}
							}

							EtapasPagamento pagamento = contrato.getEtapasPagamento();

							if (percent != 0) {
								valorCalc = valorCalc.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

								if (pe.getLimitacao() != null) {

									if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
										if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
											valorCalc = pe.getLimitacao();
										}
									}
								}

								faturamento.setCliente(contrato.getCliente());
								faturamento.setContrato(contrato);
								faturamento.setProducao(producao);
								faturamento.setEtapa(etapa);
								faturamento.setEtapas(etapas);
								faturamento.setPercentualE(pe);
								faturamento.setPercetEtapa(percent);
								faturamento.setBalancete(balanceteObj);
								faturamento.setCampanha(producao.getAno());
								faturamento.setProduto(producao.getProduto());

								System.out.println(balanceteObj.getProducao().getTipoDeApuracao());

								if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
									System.out.println("Apura��o anual, verificando se j� possui balancete... ");

									Faturamento fatAnterior = getLastFaturamentoByBalanceteAndEtapa(
											balanceteObj.getProducao().getId(), etapa.getId());
									if (Objects.nonNull(fatAnterior)) {
										if (fatAnterior.getEstado().equalsIgnoreCase("Emitida"))
											System.out.println("Achou, subtraindo valor anterior de: "
													+ fatAnterior.getValorEtapa() + " por: " + valorCalc);
										valorCalc = valorCalc.subtract(fatAnterior.getValorEtapa());

									}
								} else if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.TRIMESTRAL
										|| balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.MENSAL) {

//									if (balanceteObj.isConsolidado()) {
//										List<Faturamento> antFats = getAllFaturamentoAnterioresByProducao(
//												producao.getId());
//										BigDecimal valorFaturado = BigDecimal.valueOf(0);
//
//										for (Faturamento fat : antFats) {
//
//											System.out.println(fat.getEstado());
//
//											if (fat.getEstado().equalsIgnoreCase("Emitida")) {
//												valorFaturado = valorFaturado.add(fat.getValorEtapa());
//											}
//										}
//
//										valorCalc = valorCalc.subtract(valorFaturado);
//
//									}
								}

								valorTotal = valorTotal.add(valorCalc);
								obs += " Percentual Escalonado,";

								System.out.println(
										" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM PERCENTUAL ESCALONADO -- -- -- ");
								System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
								System.out.println(" -- Valor: " + valorCalc);
							}
						}
					} else {
						System.out.println("Fat n gerado pois n�o possui contato");
					}
				}

				if (contatoNF > 0) {

					if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
						ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
						if (Objects.nonNull(fixo)) {

							BigDecimal valorFixo = fixo.getValor();

							EtapasPagamento pagamento = producao.getContrato().getEtapasPagamento();

							// Calculo de porcentagem com BigDecimal
							BigDecimal valorCalc = valorFixo
									.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

							faturamento.setCliente(contrato.getCliente());
							faturamento.setContrato(contrato);
							faturamento.setProducao(producao);
							faturamento.setEtapa(etapa);
							faturamento.setEtapas(etapas);
							faturamento.setValorF(fixo);
							faturamento.setPercetEtapa(percent);
							faturamento.setBalancete(balanceteObj);

							if (pagamento.getEtapa1() != null && pagamento.getEtapa1().getId() == etapa.getId()) {
								pagamento.setEtapa1ok(true);
								faturamento.setPercetEtapa(pagamento.getPorcentagem1());
							}
							if (pagamento.getEtapa2() != null && pagamento.getEtapa2().getId() == etapa.getId()) {
								pagamento.setEtapa2ok(true);
								faturamento.setPercetEtapa(pagamento.getPorcentagem2());
							}
							if (pagamento.getEtapa3() != null && pagamento.getEtapa3().getId() == etapa.getId()) {
								pagamento.setEtapa3ok(true);
								faturamento.setPercetEtapa(pagamento.getPorcentagem3());
							}

							etapaPagamentoService.update(pagamento);
							valorTotal = valorTotal.add(valorCalc);
							obs += " Valor Fixo,";

							System.out.println(
									" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM VALOR FIXO -- -- -- ");
							System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
							System.out.println(" -- Valor: " + valorCalc);
						}
					}
				} else {
					System.out.println("Fat n gerado pois n�o possui contato");
				}

				if (contatoNF > 0) {

					if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
						PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(contrato.getId());

						if (Objects.nonNull(fixo)) {

							teto = fixo.getLimitacao();

							String stringValorBalancete = balanceteObj.getReducaoImposto();

							System.out.println("Redu��o de imposto: " + stringValorBalancete);

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							stringValorBalancete = stringValorBalancete.replace(".", "");
							stringValorBalancete = stringValorBalancete.replace(",", ".");
							stringValorBalancete = stringValorBalancete.trim();
							BigDecimal balancete = new BigDecimal(stringValorBalancete);

							valorCalc = balancete.multiply(
									BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

							EtapasPagamento pagamento = producao.getContrato().getEtapasPagamento();

							System.out.println("Valor final no momento (antes da % etapa ): " + valorCalc);

							if (percent != 0) {
								valorCalc = valorCalc.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

								if (fixo.getLimitacao() != null) {

									if (fixo.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
										if (valorCalc.compareTo(fixo.getLimitacao()) >= 0) {
											valorCalc = fixo.getLimitacao();
										}
									}
								}

								faturamento.setCliente(contrato.getCliente());
								faturamento.setContrato(contrato);
								faturamento.setProducao(producao);
								faturamento.setEtapa(etapa);
								faturamento.setEtapas(etapas);
								faturamento.setPercentualF(fixo);
								faturamento.setPercetEtapa(percent);
								faturamento.setBalancete(balanceteObj);

								if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
									System.out.println("Apura��o anual, verificando se j� possui balancete... ");

									Faturamento fatAnterior = getLastFaturamentoByBalanceteAndEtapa(
											balanceteObj.getProducao().getId(), etapa.getId());
									if (Objects.nonNull(fatAnterior)) {
										if (fatAnterior.getEstado().equalsIgnoreCase("Emitida")) {
											valorCalc = valorCalc.subtract(fatAnterior.getValorEtapa());
										}
									}
								} else if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.TRIMESTRAL
										|| balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.MENSAL) {

									System.out.println("Apura��o mensal/trimestral, tirando diferen�a ");
									System.out.println("Valor antes: " + valorCalc);

//									if (balanceteObj.isConsolidado()) {
//										List<Faturamento> antFats = getAllFaturamentoAnterioresByProducao(
//												producao.getId());
//										BigDecimal valorFaturado = BigDecimal.valueOf(0);
//
//										for (Faturamento fat : antFats) {
//											if (fat.getEstado().equalsIgnoreCase("Emitida")) {
//												valorFaturado = valorFaturado.add(fat.getValorEtapa());
//											}
//										}
//
//										valorCalc = valorCalc.subtract(valorFaturado);
//
//									}
								}

								if (pagamento.getEtapa1() != null && pagamento.getEtapa1().getId() == etapa.getId()) {
									pagamento.setEtapa1ok(true);
									faturamento.setPercetEtapa(pagamento.getPorcentagem1());
								}
								if (pagamento.getEtapa2() != null && pagamento.getEtapa2().getId() == etapa.getId()) {
									pagamento.setEtapa2ok(true);
									faturamento.setPercetEtapa(pagamento.getPorcentagem2());
								}
								if (pagamento.getEtapa3() != null && pagamento.getEtapa3().getId() == etapa.getId()) {
									pagamento.setEtapa3ok(true);
									faturamento.setPercetEtapa(pagamento.getPorcentagem3());
								}

								etapaPagamentoService.update(pagamento);

								valorTotal = valorTotal.add(valorCalc);
								obs += " Percentual Fixo,";

								System.out.println(
										" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM PERCENTUAL FIXO -- -- -- ");
								System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
								System.out.println(" -- Valor: " + valorCalc);

							}
						}
					}
				} else {
					System.out.println("Fat n gerado pois n�o possui contato");
				}

			}
			
			obs = obs.substring(0, obs.length() - 1);
			faturamento.setObservacao(obs);

			if (etapa.getId() == 66l) {
				faturamento.setEstado(EstadoEtapa.ETAPA_FINALIZADA.displayName());
			} else {
				faturamento.setEstado(EstadoEtapa.AGUARDANDO_ETAPA.displayName());
			}
			
			Date hoje = java.util.Calendar.getInstance().getTime();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String dataString = dateFormat.format(hoje);
			faturamento.setDataCriacao(dataString);

			
			//informa��es para calculo do total a faturar
			long idProducao = producao.getId();
			List<Faturamento> listaFaturamento = faturamentoService.getAllByProducao(idProducao);
			BigDecimal totalFaturar = new BigDecimal(0);
			BigDecimal totalFat = new BigDecimal(0);
			BalanceteCalculo balanceteDoFat = balanceteCalculoService.findById(balanceteObj.getId());
			BigDecimal valorEtapaLimitado = new BigDecimal(0);
			BigDecimal totalFatLimitado = new BigDecimal(0);
			BigDecimal auxiliar = new BigDecimal(0.00);
			
			//somando os valores etapas = totalfaturar
			for (Faturamento fats : listaFaturamento ){
				System.out.println("tamanho lista faturamento"+ listaFaturamento.size());
				System.out.println("tamanho lista faturamento id= "+ fats.getId()+"valor= "+fats.getValorEtapa());
				totalFaturar = totalFaturar.add(fats.getValorEtapa());
			}
			
			//--casos que gera o faturamento--
			
			//somando o total faturar dos fats da prod com o valor etapa do novo faturamento
			totalFat =totalFat.add(totalFaturar);
			totalFat =totalFat.add(valorTotal);
			
			int result = totalFat.compareTo(teto);
			int result2 = totalFaturar.compareTo(teto);
			
			System.out.println("TETO"+ teto);
			//A)n�o existe teto
			if(teto.compareTo(auxiliar) == 0) {
				System.out.println("entrou no n�o existe teto ");
				//setando o valorTotal em todos os fats da prod
				for (Faturamento fats : listaFaturamento ){
			        long idFat = fats.getId();
				    Faturamento faturament = faturamentoService.findById(idFat);
				    faturament.setValorTotal(totalFat);
	            }
				
				balanceteDoFat.setGerouFaturamento(true);
				//balanceteCalculoService.update(balancete);
				faturamento.setValorTotal(totalFat);
				faturamento.setValorEtapa(valorTotal);
				create(faturamento);
			}
			
			//B) existe teto
			//I)verifica se o total a faturar com o novo fat � menor ou igual ao teto
			if(result == -1 || result == 0) {
				//setando o valorTotal em todos os fats da prod
				for (Faturamento fats : listaFaturamento ){
			        long idFat = fats.getId();
				    Faturamento faturament = faturamentoService.findById(idFat);
				    faturament.setValorTotal(totalFat);
	            }
				
				balanceteDoFat.setGerouFaturamento(true);
				//balanceteCalculoService.update(balanceteDoFat);
				faturamento.setValorTotal(totalFat);
				faturamento.setValorEtapa(valorTotal);
				create(faturamento);
			}
			
			//II)caso onde ultrapassa o teto com o novo fat
			//verifica se o teto ainda n�o foi atingido sem o valor do novo faturamento e 
			//gera um faturamento com o valor (teto - total faturar antes do novo fat)
			else {
				if(result2 == -1 && result == 1) {
					valorEtapaLimitado = teto.subtract(totalFaturar);
					//somando o total faturar dos fats da prod com o valor etapaLimitado do novo faturamento
					totalFatLimitado =totalFatLimitado.add(totalFaturar);
					totalFatLimitado =totalFatLimitado.add(valorEtapaLimitado);
					
					//setando o valorTotal em todos os fats da prod
					for (Faturamento fats : listaFaturamento ){
				        long idFat = fats.getId();
					    Faturamento faturament = faturamentoService.findById(idFat);
					    faturament.setValorTotal(totalFatLimitado);
		            }
					
					for (Faturamento fats : listaFaturamento ){
				        long idFat = fats.getId();
					    Faturamento faturament = faturamentoService.findById(idFat);
		            }
					faturamento.setValorTotal(totalFatLimitado);
					faturamento.setValorEtapa(valorEtapaLimitado);
					balanceteDoFat.setGerouFaturamento(true);
					create(faturamento);
					
				}else {
					//se j� atingiu o teto sem o novo faturamento, n�o gera faturamento.
					if(result == 1 && teto.compareTo(auxiliar) != 0){
						System.out.println("total a faturar ultrapassou o teto");
						System.out.println("n�o gera fat");
						balanceteDoFat.setGerouFaturamento(false);
						faturamento = null;
					}
				}
				
			}
			
			System.out.println(" -- -- -- FATURAMENTO PERSISTIDO  -- -- -- ");
			System.out.println(" -- Valor: " + faturamento.getValorEtapa());

			return faturamento;
		} catch (Exception e) {

			e.printStackTrace();

			return null;
		}

	}

	public void aplicaLimitacao(Long id) {
		System.out.println("aplicaLimitacao");
		Producao producao = producaoService.findById(id);

		System.out.println(producao.getContrato().getId());

		List<Faturamento> fats = getAllFaturamentoAnterioresByProducao(id);

		System.out.println(fats.size());

		BigDecimal vfTotal = BigDecimal.valueOf(0);
		BigDecimal teto = BigDecimal.ZERO;

		List<Honorario> hons = producao.getContrato().getHonorarios();

		for (Honorario honorario : hons) {
			if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
				System.out.println("Escalonado");
				PercentualEscalonado pe = honorarioService
						.getPercentualEscalonadoByContrato(producao.getContrato().getId());
				teto = teto.valueOf(pe.getLimitacao().floatValue());
			}
			if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
				System.out.println("Fixo");
				PercentualFixo pf = honorarioService.getPercentualFixoByContrato(producao.getContrato().getId());
				if (pf.getLimitacao() == null) {
					pf.setLimitacao(BigDecimal.valueOf(0));
				}
				teto = teto.valueOf(pf.getLimitacao().floatValue());
			}
		}

		for (Faturamento faturamento2 : fats) {
			System.out.println(faturamento2.getValorEtapa());
			vfTotal = vfTotal.add(faturamento2.getValorEtapa());
		}

		System.out.println(vfTotal);
		System.out.println(teto);

		if (vfTotal.compareTo(teto) >= 0 && teto.compareTo(BigDecimal.ZERO) != 0) {
			vfTotal = teto;

			for (Faturamento faturamento2 : fats) {
				if (Objects.nonNull(faturamento2.getContrato().getEtapasPagamento().getEtapa1())) {
					if (faturamento2.getEtapa().getId() == faturamento2.getContrato().getEtapasPagamento().getEtapa1()
							.getId()) {
						faturamento2.setValorEtapa(teto.multiply(
								BigDecimal.valueOf(faturamento2.getContrato().getEtapasPagamento().getPorcentagem1())
										.divide(new BigDecimal(100))));
						faturamentoService.update(faturamento2);
					}
				}
				if (Objects.nonNull(faturamento2.getContrato().getEtapasPagamento().getEtapa2())) {
					if (faturamento2.getEtapa().getId() == faturamento2.getContrato().getEtapasPagamento().getEtapa2()
							.getId()) {
						faturamento2.setValorEtapa(teto.multiply(
								BigDecimal.valueOf(faturamento2.getContrato().getEtapasPagamento().getPorcentagem2())
										.divide(new BigDecimal(100))));
						faturamentoService.update(faturamento2);
					}
				}
				if (Objects.nonNull(faturamento2.getContrato().getEtapasPagamento().getEtapa3())) {
					if (faturamento2.getEtapa().getId() == faturamento2.getContrato().getEtapasPagamento().getEtapa3()
							.getId()) {
						faturamento2.setValorEtapa(teto.multiply(
								BigDecimal.valueOf(faturamento2.getContrato().getEtapasPagamento().getPorcentagem3())
										.divide(new BigDecimal(100))));
						faturamentoService.update(faturamento2);
					}
				}
			}
		}

	}

	// Inverte a ordem da lista
	public List<FaixasEscalonado> invertListFromFaixas(List<FaixasEscalonado> faixas) {
		List<FaixasEscalonado> faixasTemp = new ArrayList<>();
		System.out.println("size " + faixas.size());
		int tempSize = 1;
		for (int i = 0; i < faixas.size(); i++) {
			System.out.println("index " + tempSize);
			System.out.println(faixas.size() - tempSize);
			faixasTemp.add(faixas.get(faixas.size() - tempSize));
			tempSize++;
		}

		return faixasTemp;

	}

	public Faturamento geraFaturamentoLI(Producao producao, float percent, EtapaTrabalho etapa,
			EtapasConclusao etapas) {

		try {

			BigDecimal teto = BigDecimal.valueOf(0);
			String obs = "Honor�rios: ";
			BigDecimal valorTotal = BigDecimal.valueOf(0);

			System.out.println("Produ��o recebida para faturamento com o ID: " + producao.getId());

			Faturamento faturamento = new Faturamento();
			Contrato contrato = producao.getContrato();

			List<ContatoCliente> contatos = ContatoClienteService.getContatosCliente(contrato.getCliente().getId());
			int contatoNF = 0;
			for (ContatoCliente contatoCliente : contatos) {
				if (contatoCliente.isPessoaNfBoleto()) {
					contatoNF++;
				}
			}

			System.out.println("Contatos com recebimento de nota " + contatoNF);

			List<Honorario> honorarios = contrato.getHonorarios();
			for (Honorario honorario : honorarios) {
				System.out.println(honorario.getNome());

				if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
					System.out.println("Bateu com o nome Percentual Escalonado");

					if (contatoNF > 0) {

						PercentualEscalonado pe = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());

						ItemValores item = itemValoresService.getInvestimentoPeDByProducao(producao.getId());
						String stringValorBalancete = item.getValor();

						System.out.println(stringValorBalancete);

						if (Objects.nonNull(pe)) {

							teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							stringValorBalancete = stringValorBalancete.replace(".", "");
							stringValorBalancete = stringValorBalancete.replace(",", ".");
							stringValorBalancete = stringValorBalancete.trim();
							BigDecimal balancete = new BigDecimal(stringValorBalancete);
							BigDecimal temp = new BigDecimal(stringValorBalancete);

							for (FaixasEscalonado faixa : faixas) {

								System.out.println("Temp atual " + temp);

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								// BigDecimal gap2 = temp.subtract(inicial);
								Float percentual = faixa.getValorPercentual();

								System.out.println("Faixa: " + inicial + " ... " + finalF);
								System.out.println("Diferen�a: " + gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(BigDecimal.ZERO) != 0) {
									if (temp.compareTo(gap1) >= 0) {

										System.out.println("Passa direto da faixa, calcula com o f...");

										valorCalc = valorCalc.add(gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

										System.out.println("Saldo para soma :" + gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										// Se est� entre a faixa, calcula com o temp direto na porcentagem.
									} else if (temp.compareTo(gap1) < 0) {

										valorCalc = valorCalc.add(temp
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										BigDecimal zero = BigDecimal.ZERO;
										temp = zero;
									}
								}
							}

							if (pe.getAcimaDe() != null) {
								if (balancete.compareTo(pe.getAcimaDe()) > 0) {
									if (temp.compareTo(BigDecimal.ZERO) > 0) {
										System.out.println("Ainda resta Temp: " + temp);
										System.out.println("Entrando no Acima de e aplicando " + pe.getPercentAcimaDe()
												+ " ao temp");
										BigDecimal acima = temp.multiply(
												BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
										valorCalc = valorCalc.add(acima);
									}
								}
							}

							EtapasPagamento pagamento = contrato.getEtapasPagamento();

							if (percent != 0) {
								valorCalc = valorCalc.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

								if (pe.getLimitacao() != null) {

									if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
										if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
											valorCalc = pe.getLimitacao();
										}
									}
								}

								faturamento.setCliente(contrato.getCliente());
								faturamento.setContrato(contrato);
								faturamento.setProducao(producao);
								faturamento.setEtapa(etapa);
								faturamento.setEtapas(etapas);
								faturamento.setPercentualE(pe);
								faturamento.setPercetEtapa(percent);
								// faturamento.setBalancete(balanceteObj);
								faturamento.setCampanha(producao.getAno());
								faturamento.setProduto(producao.getProduto());

//								if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
//									System.out.println("Apura��o anual, verificando se j� possui balancete... ");
//
//									Faturamento fatAnterior = getLastFaturamentoByBalanceteAndEtapa(
//											balanceteObj.getProducao().getId(), etapa.getId());
//									if (Objects.nonNull(fatAnterior)) {
//										if (fatAnterior.getEstado().equalsIgnoreCase("Emitida"))
//											System.out.println("Achou, subtraindo valor anterior de: "
//													+ fatAnterior.getValorEtapa() + " por: " + valorCalc);
//										valorCalc = valorCalc.subtract(fatAnterior.getValorEtapa());
//
//									}
//								} else if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.TRIMESTRAL
//										|| balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.MENSAL) {
//
//									if (balanceteObj.isConsolidado()) {
//										List<Faturamento> antFats = getAllFaturamentoAnterioresByProducao(
//												producao.getId());
//										BigDecimal valorFaturado = BigDecimal.valueOf(0);
//
//										for (Faturamento fat : antFats) {
//
//											System.out.println(fat.getEstado());
//
//											if (fat.getEstado().equalsIgnoreCase("Emitida")) {
//												valorFaturado = valorFaturado.add(fat.getValorEtapa());
//											}
//										}
//
//										valorCalc = valorCalc.subtract(valorFaturado);
//
//									}
//								}

								valorTotal = valorTotal.add(valorCalc);
								obs += " Percentual Escalonado,";

								System.out.println(
										" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM PERCENTUAL ESCALONADO -- -- -- ");
								System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
								System.out.println(" -- Valor: " + valorCalc);
							}
						}
					} else {
						System.out.println("Fat n gerado pois n�o possui contato");
					}
				}

				if (contatoNF > 0) {

//					if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
//						ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
//						if (Objects.nonNull(fixo)) {
//
//							BigDecimal valorFixo = fixo.getValor();
//
//							EtapasPagamento pagamento = producao.getContrato().getEtapasPagamento();
//
//							// Calculo de porcentagem com BigDecimal
//							BigDecimal valorCalc = valorFixo
//									.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));
//
//							faturamento.setCliente(contrato.getCliente());
//							faturamento.setContrato(contrato);
//							faturamento.setProducao(producao);
//							faturamento.setEtapa(etapa);
//							faturamento.setEtapas(etapas);
//							faturamento.setValorF(fixo);
//							faturamento.setPercetEtapa(percent);
//							//faturamento.setBalancete(balanceteObj);
//
//							if (pagamento.getEtapa1() != null && pagamento.getEtapa1().getId() == etapa.getId()) {
//								pagamento.setEtapa1ok(true);
//								faturamento.setPercetEtapa(pagamento.getPorcentagem1());
//							}
//							if (pagamento.getEtapa2() != null && pagamento.getEtapa2().getId() == etapa.getId()) {
//								pagamento.setEtapa2ok(true);
//								faturamento.setPercetEtapa(pagamento.getPorcentagem2());
//							}
//							if (pagamento.getEtapa3() != null && pagamento.getEtapa3().getId() == etapa.getId()) {
//								pagamento.setEtapa3ok(true);
//								faturamento.setPercetEtapa(pagamento.getPorcentagem3());
//							}
//
//							etapaPagamentoService.update(pagamento);
//							valorTotal = valorTotal.add(valorCalc);
//							obs += " Valor Fixo,";
//
//							System.out.println(
//									" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM VALOR FIXO -- -- -- ");
//							System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
//							System.out.println(" -- Valor: " + valorCalc);
//						}
//					}
				} else {
					System.out.println("Fat n gerado pois n�o possui contato");
				}

				if (contatoNF > 0) {

					if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
						PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(contrato.getId());

						if (Objects.nonNull(fixo)) {

							teto = fixo.getLimitacao();

							ItemValores item = itemValoresService.getInvestimentoPeDByProducao(producao.getId());
							String stringValorBalancete = item.getValor();

							System.out.println("Redu��o de imposto: " + stringValorBalancete);

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							stringValorBalancete = stringValorBalancete.replace(".", "");
							stringValorBalancete = stringValorBalancete.replace(",", ".");
							stringValorBalancete = stringValorBalancete.trim();
							BigDecimal balancete = new BigDecimal(stringValorBalancete);

							valorCalc = balancete.multiply(
									BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

							EtapasPagamento pagamento = producao.getContrato().getEtapasPagamento();

							System.out.println("Valor final no momento (antes da % etapa ): " + valorCalc);

							if (percent != 0) {
								valorCalc = valorCalc.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

								if (fixo.getLimitacao() != null) {

									if (fixo.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
										if (valorCalc.compareTo(fixo.getLimitacao()) >= 0) {
											valorCalc = fixo.getLimitacao();
										}
									}
								}

								faturamento.setCliente(contrato.getCliente());
								faturamento.setContrato(contrato);
								faturamento.setProducao(producao);
								faturamento.setEtapa(etapa);
								faturamento.setEtapas(etapas);
								faturamento.setPercentualF(fixo);
								faturamento.setPercetEtapa(percent);
								// faturamento.setBalancete(balanceteObj);

//								if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
//									System.out.println("Apura��o anual, verificando se j� possui balancete... ");
//
//									Faturamento fatAnterior = getLastFaturamentoByBalanceteAndEtapa(
//											balanceteObj.getProducao().getId(), etapa.getId());
//									if (Objects.nonNull(fatAnterior)) {
//										if (fatAnterior.getEstado().equalsIgnoreCase("Emitida")) {
//											valorCalc = valorCalc.subtract(fatAnterior.getValorEtapa());
//										}
//									}
//								} else if (balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.TRIMESTRAL
//										|| balanceteObj.getProducao().getTipoDeApuracao() == TipoDeApuracao.MENSAL) {
//
//									System.out.println("Apura��o mensal/trimestral, tirando diferen�a ");
//									System.out.println("Valor antes: " + valorCalc);
//
//									if (balanceteObj.isConsolidado()) {
//										List<Faturamento> antFats = getAllFaturamentoAnterioresByProducao(
//												producao.getId());
//										BigDecimal valorFaturado = BigDecimal.valueOf(0);
//
//										for (Faturamento fat : antFats) {
//											if (fat.getEstado().equalsIgnoreCase("Emitida")) {
//												valorFaturado = valorFaturado.add(fat.getValorEtapa());
//											}
//										}
//
//										valorCalc = valorCalc.subtract(valorFaturado);
//
//									}
//								}

								if (pagamento.getEtapa1() != null && pagamento.getEtapa1().getId() == etapa.getId()) {
									pagamento.setEtapa1ok(true);
									faturamento.setPercetEtapa(pagamento.getPorcentagem1());
								}
								if (pagamento.getEtapa2() != null && pagamento.getEtapa2().getId() == etapa.getId()) {
									pagamento.setEtapa2ok(true);
									faturamento.setPercetEtapa(pagamento.getPorcentagem2());
								}
								if (pagamento.getEtapa3() != null && pagamento.getEtapa3().getId() == etapa.getId()) {
									pagamento.setEtapa3ok(true);
									faturamento.setPercetEtapa(pagamento.getPorcentagem3());
								}

								etapaPagamentoService.update(pagamento);

								valorTotal = valorTotal.add(valorCalc);
								obs += " Percentual Fixo,";

								System.out.println(
										" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM PERCENTUAL FIXO -- -- -- ");
								System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
								System.out.println(" -- Valor: " + valorCalc);

							}
						}
					}
				} else {
					System.out.println("Fat n gerado pois n�o possui contato");
				}

			}

			faturamento.setValorEtapa(valorTotal);
			obs = obs.substring(0, obs.length() - 1);
			faturamento.setObservacao(obs);
			Date hoje = java.util.Calendar.getInstance().getTime();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String dataString = dateFormat.format(hoje);
			faturamento.setDataCriacao(dataString);
			
			
			
			
			create(faturamento);

			System.out.println(" -- -- -- FATURAMENTO PERSISTIDO  -- -- -- ");
			System.out.println(" -- Valor: " + faturamento.getValorEtapa());

			return faturamento;

		} catch (Exception e) {

			System.out.println(e);
			System.out.println(e.getStackTrace()[0].getLineNumber());

			return null;
		}

	}

	public Faturamento geraFaturamentoExTarifario(String trigger, Producao producao, SubProducao subProducao,
			AcompanhamentoExTarifario acompanhamento) {

		Faturamento faturamento = new Faturamento();

		System.out.println("!-! Processando cria��o de faturamento EX-TARIF�RIO");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();

		switch (trigger) {
		case "Mapeamento":

			if (getFaturamentoByProducaoEtapa(producao.getId(), 78l).isEmpty()) {

				System.out.println("!-! Entrou mapeamento");

				EtapasPagamento timing = producao.getContrato().getEtapasPagamento();
				if (Objects.nonNull(timing.getEtapa1()) && !timing.getHonorario1().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa1().getId() == 78l) {
						faturamento.setValorEtapa(timing.getValor1());
					}
				}
				if (Objects.nonNull(timing.getEtapa2()) && !timing.getHonorario2().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa2().getId() == 78l) {
						faturamento.setValorEtapa(timing.getValor2());
					}
				}
				if (Objects.nonNull(timing.getEtapa3()) && !timing.getHonorario3().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa3().getId() == 78l) {
						faturamento.setValorEtapa(timing.getValor3());
					}
				}
				if (Objects.nonNull(timing.getEtapa4()) && !timing.getHonorario4().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa4().getId() == 78l) {
						faturamento.setValorEtapa(timing.getValor4());
					}
				}

				faturamento.setCampanha(producao.getAno());
				faturamento.setCliente(producao.getCliente());
				faturamento.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
				faturamento.setContrato(producao.getContrato());
				faturamento.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
				faturamento.setEtapa(etapaTrabalhoService.findById(78l));
				faturamento.setLiberado(false);
				faturamento.setManual(false);
				faturamento.setObsContrato(producao.getContrato().getObsContrato());
				faturamento.setProducao(producao);
				faturamento.setProduto(producao.getProduto());
				faturamento.setSubProducao(subProducao);
				Date hoje = java.util.Calendar.getInstance().getTime();
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String dataString = dateFormat.format(hoje);
				faturamento.setDataCriacao(dataString);
				faturamentoService.create(faturamento);
			}

			break;
		case "Envio do pleito":

			if (!subProducao.isGerouFaturamento()) {

				System.out.println("!-! Entrou Envio do pleito");

				EtapasPagamento timing = producao.getContrato().getEtapasPagamento();
				if (Objects.nonNull(timing.getEtapa1()) && !timing.getHonorario1().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa1().getId() == 79l) {
						faturamento.setValorEtapa(timing.getValor1());
					}
				}
				if (Objects.nonNull(timing.getEtapa2()) && !timing.getHonorario2().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa2().getId() == 79l) {
						faturamento.setValorEtapa(timing.getValor2());
					}
				}
				if (Objects.nonNull(timing.getEtapa3()) && !timing.getHonorario3().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa3().getId() == 79l) {
						faturamento.setValorEtapa(timing.getValor3());
					}
				}
				if (Objects.nonNull(timing.getEtapa4()) && !timing.getHonorario4().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa4().getId() == 79l) {
						faturamento.setValorEtapa(timing.getValor4());
					}
				}

				producao = producaoService.findById(producao.getId());

				faturamento.setCampanha(producao.getAno());
				faturamento.setCliente(producao.getCliente());
				faturamento.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
				faturamento.setContrato(producao.getContrato());
				faturamento.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
				faturamento.setEtapa(etapaTrabalhoService.findById(79l));
				faturamento.setLiberado(false);
				faturamento.setManual(false);
				faturamento.setObsContrato(producao.getContrato().getObsContrato());
				faturamento.setObservacao("Faturamento referente � " + subProducao.getDescricao());
				faturamento.setProducao(producao);
				faturamento.setProduto(producao.getProduto());
				faturamento.setSubProducao(subProducao);
				Date hoje = java.util.Calendar.getInstance().getTime();
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String dataString = dateFormat.format(hoje);
				faturamento.setDataCriacao(dataString);
				
				faturamentoService.create(faturamento);

				subProducao.setGerouFaturamento(true);
				subProducaoService.update(subProducao);

			}

			break;
		case "Acompanhamento":

			if (!subProducao.isGerouFaturamento()) {

				System.out.println("Entrou acpompanhamento");
				EtapasPagamento timing = producao.getContrato().getEtapasPagamento();

				if (Objects.nonNull(timing.getEtapa1()) && !timing.getHonorario1().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa1().getId() == 107l) {
						if (timing.getHonorario1().equalsIgnoreCase("Percentual Fixo")) {

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);

							BigDecimal valorFinal = beneficio
									.multiply(BigDecimal.valueOf(timing.getPorcentagem1()).divide(new BigDecimal(100)));
							faturamento.setValorEtapa(valorFinal);

						}
						if (timing.getHonorario1().equalsIgnoreCase("Percentual Escalonado")) {

							PercentualEscalonado pe = timing.getEscalonado1();
							BigDecimal teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);
							BigDecimal temp = beneficio;

							for (FaixasEscalonado faixa : faixas) {

								System.out.println("Temp atual " + temp);

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								// BigDecimal gap2 = temp.subtract(inicial);
								Float percentual = faixa.getValorPercentual();

								System.out.println("Faixa: " + inicial + " ... " + finalF);
								System.out.println("Diferen�a: " + gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(BigDecimal.ZERO) != 0) {
									if (temp.compareTo(gap1) >= 0) {

										System.out.println("Passa direto da faixa, calcula com o f...");

										valorCalc = valorCalc.add(gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

										System.out.println("Saldo para soma :" + gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										// Se est� entre a faixa, calcula com o temp direto na porcentagem.
									} else if (temp.compareTo(gap1) < 0) {

										valorCalc = valorCalc.add(temp
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										BigDecimal zero = BigDecimal.ZERO;
										temp = zero;
									}
								}
							}

							if (pe.getAcimaDe() != null) {
								if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
									if (temp.compareTo(BigDecimal.ZERO) > 0) {
										System.out.println("Ainda resta Temp: " + temp);
										System.out.println("Entrando no Acima de e aplicando " + pe.getPercentAcimaDe()
												+ " ao temp");
										BigDecimal acima = temp.multiply(
												BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
										valorCalc = valorCalc.add(acima);
									}
								}
							}

							if (pe.getLimitacao() != null) {

								if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
									if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
										valorCalc = pe.getLimitacao();
									}
								}
							}

							faturamento.setValorEtapa(valorCalc);

						}
						if (timing.getHonorario1().equalsIgnoreCase("Valor Fixo")) {
							faturamento.setValorEtapa(timing.getValor1());
						}
					}
				}
				if (Objects.nonNull(timing.getEtapa2()) && !timing.getHonorario2().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa2().getId() == 107l) {
						if (timing.getHonorario2().equalsIgnoreCase("Percentual Fixo")) {

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);

							BigDecimal valorFinal = beneficio
									.multiply(BigDecimal.valueOf(timing.getPorcentagem2()).divide(new BigDecimal(100)));
							faturamento.setValorEtapa(valorFinal);

						}
						if (timing.getHonorario2().equalsIgnoreCase("Percentual Escalonado")) {

							PercentualEscalonado pe = timing.getEscalonado2();
							BigDecimal teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);
							BigDecimal temp = beneficio;

							for (FaixasEscalonado faixa : faixas) {

								System.out.println("Temp atual " + temp);

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								// BigDecimal gap2 = temp.subtract(inicial);
								Float percentual = faixa.getValorPercentual();

								System.out.println("Faixa: " + inicial + " ... " + finalF);
								System.out.println("Diferen�a: " + gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(BigDecimal.ZERO) != 0) {
									if (temp.compareTo(gap1) >= 0) {

										System.out.println("Passa direto da faixa, calcula com o f...");

										valorCalc = valorCalc.add(gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

										System.out.println("Saldo para soma :" + gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										// Se est� entre a faixa, calcula com o temp direto na porcentagem.
									} else if (temp.compareTo(gap1) < 0) {

										valorCalc = valorCalc.add(temp
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										BigDecimal zero = BigDecimal.ZERO;
										temp = zero;
									}
								}
							}

							if (pe.getAcimaDe() != null) {
								if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
									if (temp.compareTo(BigDecimal.ZERO) > 0) {
										System.out.println("Ainda resta Temp: " + temp);
										System.out.println("Entrando no Acima de e aplicando " + pe.getPercentAcimaDe()
												+ " ao temp");
										BigDecimal acima = temp.multiply(
												BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
										valorCalc = valorCalc.add(acima);
									}
								}
							}

							if (pe.getLimitacao() != null) {

								if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
									if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
										valorCalc = pe.getLimitacao();
									}
								}
							}

							faturamento.setValorEtapa(valorCalc);

						}
						if (timing.getHonorario2().equalsIgnoreCase("Valor Fixo")) {
							faturamento.setValorEtapa(timing.getValor2());
						}
					}
				}
				if (Objects.nonNull(timing.getEtapa3()) && !timing.getHonorario3().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa3().getId() == 107l) {
						if (timing.getHonorario3().equalsIgnoreCase("Percentual Fixo")) {

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);

							BigDecimal valorFinal = beneficio
									.multiply(BigDecimal.valueOf(timing.getPorcentagem3()).divide(new BigDecimal(100)));
							faturamento.setValorEtapa(valorFinal);

						}
						if (timing.getHonorario3().equalsIgnoreCase("Percentual Escalonado")) {

							PercentualEscalonado pe = timing.getEscalonado3();
							BigDecimal teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);
							BigDecimal temp = beneficio;

							for (FaixasEscalonado faixa : faixas) {

								System.out.println("Temp atual " + temp);

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								// BigDecimal gap2 = temp.subtract(inicial);
								Float percentual = faixa.getValorPercentual();

								System.out.println("Faixa: " + inicial + " ... " + finalF);
								System.out.println("Diferen�a: " + gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(BigDecimal.ZERO) != 0) {
									if (temp.compareTo(gap1) >= 0) {

										System.out.println("Passa direto da faixa, calcula com o f...");

										valorCalc = valorCalc.add(gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

										System.out.println("Saldo para soma :" + gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										// Se est� entre a faixa, calcula com o temp direto na porcentagem.
									} else if (temp.compareTo(gap1) < 0) {

										valorCalc = valorCalc.add(temp
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										BigDecimal zero = BigDecimal.ZERO;
										temp = zero;
									}
								}
							}

							if (pe.getAcimaDe() != null) {
								if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
									if (temp.compareTo(BigDecimal.ZERO) > 0) {
										System.out.println("Ainda resta Temp: " + temp);
										System.out.println("Entrando no Acima de e aplicando " + pe.getPercentAcimaDe()
												+ " ao temp");
										BigDecimal acima = temp.multiply(
												BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
										valorCalc = valorCalc.add(acima);
									}
								}
							}

							if (pe.getLimitacao() != null) {

								if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
									if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
										valorCalc = pe.getLimitacao();
									}
								}
							}
							faturamento.setValorEtapa(valorCalc);
						}
						if (timing.getHonorario3().equalsIgnoreCase("Valor Fixo")) {
							faturamento.setValorEtapa(timing.getValor3());
						}
					}
				}
				if (Objects.nonNull(timing.getEtapa4()) && !timing.getHonorario4().equalsIgnoreCase("Isento")) {
					if (timing.getEtapa4().getId() == 107l) {
						if (timing.getHonorario4().equalsIgnoreCase("Percentual Fixo")) {

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);

							BigDecimal valorFinal = beneficio
									.multiply(BigDecimal.valueOf(timing.getPorcentagem4()).divide(new BigDecimal(100)));
							faturamento.setValorEtapa(valorFinal);
						}
						if (timing.getHonorario4().equalsIgnoreCase("Percentual Escalonado")) {

							PercentualEscalonado pe = timing.getEscalonado4();
							BigDecimal teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							ItemValores item = itemValoresService
									.getBeneficioCalculadoBySubProducao(subProducao.getId());
							String beneficioString = item.getValor();
							beneficioString = beneficioString.replace(".", "");
							beneficioString = beneficioString.replace(",", ".");
							beneficioString = beneficioString.trim();
							BigDecimal beneficio = new BigDecimal(beneficioString);
							BigDecimal temp = beneficio;

							for (FaixasEscalonado faixa : faixas) {

								System.out.println("Temp atual " + temp);

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								// BigDecimal gap2 = temp.subtract(inicial);
								Float percentual = faixa.getValorPercentual();

								System.out.println("Faixa: " + inicial + " ... " + finalF);
								System.out.println("Diferen�a: " + gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(BigDecimal.ZERO) != 0) {
									if (temp.compareTo(gap1) >= 0) {

										System.out.println("Passa direto da faixa, calcula com o f...");

										valorCalc = valorCalc.add(gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

										System.out.println("Saldo para soma :" + gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										// Se est� entre a faixa, calcula com o temp direto na porcentagem.
									} else if (temp.compareTo(gap1) < 0) {

										valorCalc = valorCalc.add(temp
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										BigDecimal zero = BigDecimal.ZERO;
										temp = zero;
									}
								}
							}

							if (pe.getAcimaDe() != null) {
								if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
									if (temp.compareTo(BigDecimal.ZERO) > 0) {
										System.out.println("Ainda resta Temp: " + temp);
										System.out.println("Entrando no Acima de e aplicando " + pe.getPercentAcimaDe()
												+ " ao temp");
										BigDecimal acima = temp.multiply(
												BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
										valorCalc = valorCalc.add(acima);
									}
								}
							}

							if (pe.getLimitacao() != null) {

								if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
									if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
										valorCalc = pe.getLimitacao();
									}
								}
							}
							faturamento.setValorEtapa(valorCalc);
						}
						if (timing.getHonorario4().equalsIgnoreCase("Valor Fixo")) {
							faturamento.setValorEtapa(timing.getValor4());
						}
					}
				}

				SubProducao pleito = subProducaoService.getSubProducaoByProducaoDescricaoAndEtapa(producao.getId(),
						subProducao.getDescricao(), 79l);

				if (producao.getCredito() != null) {
					if (producao.getCredito().compareTo(BigDecimal.ZERO) > 0) {

						faturamento.setObservacao("C�lculo do faturamento: R$ " + faturamento.getValorEtapa() + " - R$ "
								+ producao.getCredito() + "(Cr�dito da submiss�o do pleito)");

						if (faturamento.getValorEtapa().compareTo(producao.getCredito()) > 0) {
							faturamento.setValorEtapa(faturamento.getValorEtapa().subtract(producao.getCredito()));
							producao.setCredito(BigDecimal.valueOf(0));
						} else {
							producao.setCredito(producao.getCredito().subtract(faturamento.getValorEtapa()));
							faturamento.setValorEtapa(BigDecimal.valueOf(0));
						}
					}
				}
				
				Date hoje = java.util.Calendar.getInstance().getTime();
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String dataString = dateFormat.format(hoje);
				
				faturamento.setCampanha(producao.getAno());
				faturamento.setCliente(producao.getCliente());
				faturamento.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
				faturamento.setContrato(producao.getContrato());
				faturamento.setDataCriacao(dataString);
				faturamento.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
				faturamento.setEtapa(etapaTrabalhoService.findById(107l));
				faturamento.setLiberado(false);
				faturamento.setManual(false);
				faturamento.setObsContrato(producao.getContrato().getObsContrato());
				faturamento.setProducao(producao);
				faturamento.setProduto(producao.getProduto());
				faturamento.setSubProducao(subProducaoService.findById(subProducao.getId()));
				faturamentoService.create(faturamento);

				subProducao.setGerouFaturamento(true);
				subProducaoService.update(subProducao);

				producaoService.update(producao);

			}

			break;
		case "AT":

			EtapasPagamento timing = producao.getContrato().getEtapasPagamento();
			if (Objects.nonNull(timing.getEtapa1())) {
				if (timing.getEtapa1().getId() == 108l) {
					if (timing.getHonorario1().equalsIgnoreCase("Percentual Fixo")) {

						String beneficioString = acompanhamento.getBeneficioCalculado();
						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);

						BigDecimal valorFinal = beneficio
								.multiply(BigDecimal.valueOf(timing.getPorcentagem1()).divide(new BigDecimal(100)));
						faturamento.setValorEtapa(valorFinal);

					}
					if (timing.getHonorario1().equalsIgnoreCase("Percentual Escalonado")) {

						PercentualEscalonado pe = timing.getEscalonado1();
						BigDecimal teto = pe.getLimitacao();
						List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

						BigDecimal valorCalc = BigDecimal.valueOf(0);

						String beneficioString = acompanhamento.getBeneficioCalculado();
						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);
						BigDecimal temp = beneficio;

						for (FaixasEscalonado faixa : faixas) {

							System.out.println("Temp atual " + temp);

							// Define vari�veis
							BigDecimal inicial = faixa.getValorInicial();
							BigDecimal finalF = faixa.getValorFinal();
							BigDecimal gap1 = finalF.subtract(inicial);
							// BigDecimal gap2 = temp.subtract(inicial);
							Float percentual = faixa.getValorPercentual();

							System.out.println("Faixa: " + inicial + " ... " + finalF);
							System.out.println("Diferen�a: " + gap1);

							// Se � maior que o final, calcula direto com o gap1
							if (temp.compareTo(BigDecimal.ZERO) != 0) {
								if (temp.compareTo(gap1) >= 0) {

									System.out.println("Passa direto da faixa, calcula com o f...");

									valorCalc = valorCalc.add(
											gap1.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

									System.out.println("Saldo para soma :" + gap1
											.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									// Se est� entre a faixa, calcula com o temp direto na porcentagem.
								} else if (temp.compareTo(gap1) < 0) {

									valorCalc = valorCalc.add(
											temp.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									BigDecimal zero = BigDecimal.ZERO;
									temp = zero;
								}
							}
						}

						if (pe.getAcimaDe() != null) {
							if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
								if (temp.compareTo(BigDecimal.ZERO) > 0) {
									System.out.println("Ainda resta Temp: " + temp);
									System.out.println(
											"Entrando no Acima de e aplicando " + pe.getPercentAcimaDe() + " ao temp");
									BigDecimal acima = temp.multiply(
											BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
									valorCalc = valorCalc.add(acima);
								}
							}
						}

						if (pe.getLimitacao() != null) {

							if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
								if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
									valorCalc = pe.getLimitacao();
								}
							}
						}
						faturamento.setValorEtapa(valorCalc);
					}
					if (timing.getHonorario1().equalsIgnoreCase("Valor Fixo")) {
						faturamento.setValorEtapa(timing.getValor1());
					}
				}
			}
			if (Objects.nonNull(timing.getEtapa2())) {
				if (timing.getEtapa2().getId() == 108l) {
					if (timing.getHonorario2().equalsIgnoreCase("Percentual Fixo")) {

						String beneficioString = acompanhamento.getBeneficioCalculado();
						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);

						BigDecimal valorFinal = beneficio
								.multiply(BigDecimal.valueOf(timing.getPorcentagem2()).divide(new BigDecimal(100)));
						faturamento.setValorEtapa(valorFinal);
					}
					if (timing.getHonorario2().equalsIgnoreCase("Percentual Escalonado")) {

						PercentualEscalonado pe = timing.getEscalonado2();
						BigDecimal teto = pe.getLimitacao();
						List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

						BigDecimal valorCalc = BigDecimal.valueOf(0);

						String beneficioString = acompanhamento.getBeneficioCalculado();
						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);
						BigDecimal temp = beneficio;

						for (FaixasEscalonado faixa : faixas) {

							System.out.println("Temp atual " + temp);

							// Define vari�veis
							BigDecimal inicial = faixa.getValorInicial();
							BigDecimal finalF = faixa.getValorFinal();
							BigDecimal gap1 = finalF.subtract(inicial);
							// BigDecimal gap2 = temp.subtract(inicial);
							Float percentual = faixa.getValorPercentual();

							System.out.println("Faixa: " + inicial + " ... " + finalF);
							System.out.println("Diferen�a: " + gap1);

							// Se � maior que o final, calcula direto com o gap1
							if (temp.compareTo(BigDecimal.ZERO) != 0) {
								if (temp.compareTo(gap1) >= 0) {

									System.out.println("Passa direto da faixa, calcula com o f...");

									valorCalc = valorCalc.add(
											gap1.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

									System.out.println("Saldo para soma :" + gap1
											.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									// Se est� entre a faixa, calcula com o temp direto na porcentagem.
								} else if (temp.compareTo(gap1) < 0) {

									valorCalc = valorCalc.add(
											temp.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									BigDecimal zero = BigDecimal.ZERO;
									temp = zero;
								}
							}
						}

						if (pe.getAcimaDe() != null) {
							if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
								if (temp.compareTo(BigDecimal.ZERO) > 0) {
									System.out.println("Ainda resta Temp: " + temp);
									System.out.println(
											"Entrando no Acima de e aplicando " + pe.getPercentAcimaDe() + " ao temp");
									BigDecimal acima = temp.multiply(
											BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
									valorCalc = valorCalc.add(acima);
								}
							}
						}

						if (pe.getLimitacao() != null) {

							if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
								if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
									valorCalc = pe.getLimitacao();
								}
							}
						}
						faturamento.setValorEtapa(valorCalc);
					}
					if (timing.getHonorario2().equalsIgnoreCase("Valor Fixo")) {
						faturamento.setValorEtapa(timing.getValor4());
					}
				}
			}
			if (Objects.nonNull(timing.getEtapa3())) {
				if (timing.getEtapa3().getId() == 108l) {
					if (timing.getHonorario3().equalsIgnoreCase("Percentual Fixo")) {

						System.out.println("ENTROU NO CALCULO DO FAT");
						String beneficioString = acompanhamento.getBeneficioCalculado();
						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);

						BigDecimal valorFinal = beneficio
								.multiply(BigDecimal.valueOf(timing.getPorcentagem3()).divide(new BigDecimal(100)));
						faturamento.setValorEtapa(valorFinal);

						System.out.println("CALCULOU " + faturamento.getValorEtapa());

					}
					if (timing.getHonorario3().equalsIgnoreCase("Percentual Escalonado")) {

						PercentualEscalonado pe = timing.getEscalonado3();
						BigDecimal teto = pe.getLimitacao();
						List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

						BigDecimal valorCalc = BigDecimal.valueOf(0);

						String beneficioString = acompanhamento.getBeneficioCalculado();
						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);
						BigDecimal temp = beneficio;

						for (FaixasEscalonado faixa : faixas) {

							System.out.println("Temp atual " + temp);

							// Define vari�veis
							BigDecimal inicial = faixa.getValorInicial();
							BigDecimal finalF = faixa.getValorFinal();
							BigDecimal gap1 = finalF.subtract(inicial);
							// BigDecimal gap2 = temp.subtract(inicial);
							Float percentual = faixa.getValorPercentual();

							System.out.println("Faixa: " + inicial + " ... " + finalF);
							System.out.println("Diferen�a: " + gap1);

							// Se � maior que o final, calcula direto com o gap1
							if (temp.compareTo(BigDecimal.ZERO) != 0) {
								if (temp.compareTo(gap1) >= 0) {

									System.out.println("Passa direto da faixa, calcula com o f...");

									valorCalc = valorCalc.add(
											gap1.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

									System.out.println("Saldo para soma :" + gap1
											.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									// Se est� entre a faixa, calcula com o temp direto na porcentagem.
								} else if (temp.compareTo(gap1) < 0) {

									valorCalc = valorCalc.add(
											temp.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									BigDecimal zero = BigDecimal.ZERO;
									temp = zero;
								}
							}
						}

						if (pe.getAcimaDe() != null) {
							if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
								if (temp.compareTo(BigDecimal.ZERO) > 0) {
									System.out.println("Ainda resta Temp: " + temp);
									System.out.println(
											"Entrando no Acima de e aplicando " + pe.getPercentAcimaDe() + " ao temp");
									BigDecimal acima = temp.multiply(
											BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
									valorCalc = valorCalc.add(acima);
								}
							}
						}

						if (pe.getLimitacao() != null) {

							if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
								if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
									valorCalc = pe.getLimitacao();
								}
							}
						}
						faturamento.setValorEtapa(valorCalc);
					}
					if (timing.getHonorario3().equalsIgnoreCase("Valor Fixo")) {
						faturamento.setValorEtapa(timing.getValor4());
					}
				}
			}
			if (Objects.nonNull(timing.getEtapa4())) {
				if (timing.getEtapa4().getId() == 108l) {
					if (timing.getHonorario4().equalsIgnoreCase("Percentual Fixo")) {

						String beneficioString = acompanhamento.getBeneficioCalculado();

						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);

						BigDecimal valorFinal = beneficio
								.multiply(BigDecimal.valueOf(timing.getPorcentagem4()).divide(new BigDecimal(100)));
						faturamento.setValorEtapa(valorFinal);
					}
					if (timing.getHonorario4().equalsIgnoreCase("Percentual Escalonado")) {

						PercentualEscalonado pe = timing.getEscalonado4();
						BigDecimal teto = pe.getLimitacao();
						List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

						BigDecimal valorCalc = BigDecimal.valueOf(0);

						String beneficioString = acompanhamento.getBeneficioCalculado();

						beneficioString = beneficioString.replace(".", "");
						beneficioString = beneficioString.replace(",", ".");
						beneficioString = beneficioString.trim();
						BigDecimal beneficio = new BigDecimal(beneficioString);
						BigDecimal temp = beneficio;

						for (FaixasEscalonado faixa : faixas) {

							System.out.println("Temp atual " + temp);

							// Define vari�veis
							BigDecimal inicial = faixa.getValorInicial();
							BigDecimal finalF = faixa.getValorFinal();
							BigDecimal gap1 = finalF.subtract(inicial);
							// BigDecimal gap2 = temp.subtract(inicial);
							Float percentual = faixa.getValorPercentual();

							System.out.println("Faixa: " + inicial + " ... " + finalF);
							System.out.println("Diferen�a: " + gap1);

							// Se � maior que o final, calcula direto com o gap1
							if (temp.compareTo(BigDecimal.ZERO) != 0) {
								if (temp.compareTo(gap1) >= 0) {

									System.out.println("Passa direto da faixa, calcula com o f...");

									valorCalc = valorCalc.add(
											gap1.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

									System.out.println("Saldo para soma :" + gap1
											.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									// Se est� entre a faixa, calcula com o temp direto na porcentagem.
								} else if (temp.compareTo(gap1) < 0) {

									valorCalc = valorCalc.add(
											temp.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

									BigDecimal zero = BigDecimal.ZERO;
									temp = zero;
								}
							}
						}

						if (pe.getAcimaDe() != null) {
							if (beneficio.compareTo(pe.getAcimaDe()) > 0) {
								if (temp.compareTo(BigDecimal.ZERO) > 0) {
									System.out.println("Ainda resta Temp: " + temp);
									System.out.println(
											"Entrando no Acima de e aplicando " + pe.getPercentAcimaDe() + " ao temp");
									BigDecimal acima = temp.multiply(
											BigDecimal.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
									valorCalc = valorCalc.add(acima);
								}
							}
						}

						if (pe.getLimitacao() != null) {

							if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
								if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
									valorCalc = pe.getLimitacao();
								}
							}
						}
						faturamento.setValorEtapa(valorCalc);
					}
					if (timing.getHonorario4().equalsIgnoreCase("Valor Fixo")) {
						faturamento.setValorEtapa(timing.getValor4());
					}
				}
			}

			SubProducao pleito = subProducaoService.getSubProducaoByProducaoDescricaoAndEtapa(producao.getId(),
					subProducao.getDescricao(), 79l);

			if (producao.getCredito() != null) {
				if (producao.getCredito().compareTo(BigDecimal.ZERO) > 0) {

					faturamento.setObservacao("C�lculo do faturamento: R$ " + faturamento.getValorEtapa() + " - R$ "
							+ producao.getCredito() + "(Cr�dito da submiss�o do pleito)");

					if (faturamento.getValorEtapa().compareTo(producao.getCredito()) > 0) {
						faturamento.setValorEtapa(faturamento.getValorEtapa().subtract(producao.getCredito()));
						producao.setCredito(BigDecimal.valueOf(0));
					} else {
						producao.setCredito(producao.getCredito().subtract(faturamento.getValorEtapa()));
						faturamento.setValorEtapa(BigDecimal.valueOf(0));
					}
				}
			}

			faturamento.setObservacao(faturamento.getObservacao()
					+ "\n Faturamento referente ao acompanhamento trimestral do Pleito: " + pleito.getDescricao());
			faturamento.setCampanha(producao.getAno());
			faturamento.setCliente(producao.getCliente());
			faturamento.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
			faturamento.setContrato(producao.getContrato());
			faturamento.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
			faturamento.setEtapa(etapaTrabalhoService.findById(108l));
			faturamento.setLiberado(false);
			faturamento.setManual(false);
			faturamento.setObsContrato(producao.getContrato().getObsContrato());
			faturamento.setProducao(producao);
			faturamento.setProduto(producao.getProduto());
			faturamento.setSubProducao(subProducao);
			Date hoje = java.util.Calendar.getInstance().getTime();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String dataString = dateFormat.format(hoje);
			faturamento.setDataCriacao(dataString);
			faturamentoService.create(faturamento);

			producaoService.update(producao);

			break;

		default:
			break;
		}

		System.out.println("!-! Faturamento criado no valor de " + faturamento.getValorEtapa());

		return faturamento;

	}

	public Faturamento geraFaturamentoFinanciamento(String trigger, Producao producao, SubProducao subProducao) {

		Faturamento faturamento = new Faturamento();

		System.out.println("!-! Processando cria��o de faturamento EX-TARIF�RIO");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();

		switch (trigger) {
		case "Mapeamento":

			if (getFaturamentoByProducaoEtapa(producao.getId(), 111l).isEmpty()) {

				System.out.println("!-! Entrou mapeamento");

				EtapasPagamento timing = producao.getContrato().getEtapasPagamento();

				if (Objects.nonNull(timing.getEtapa1())) {
					if (timing.getEtapa1().getId() == 111l) {
						faturamento.setValorEtapa(timing.getValor1());
					}
				}
				if (Objects.nonNull(timing.getEtapa2())) {
					if (timing.getEtapa2().getId() == 111l) {
						faturamento.setValorEtapa(timing.getValor2());
					}
				}
				if (Objects.nonNull(timing.getEtapa3())) {
					if (timing.getEtapa3().getId() == 111l) {
						faturamento.setValorEtapa(timing.getValor3());
					}
				}
				if (Objects.nonNull(timing.getEtapa4())) {
					if (timing.getEtapa4().getId() == 111l) {
						faturamento.setValorEtapa(timing.getValor4());
					}
				}

				if (faturamento.getValorEtapa() != null) {

					faturamento.setCampanha(producao.getAno());
					faturamento.setCliente(producao.getCliente());
					faturamento.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
					faturamento.setContrato(producao.getContrato());
					faturamento.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
					faturamento.setEtapa(etapaTrabalhoService.findById(111l));
					faturamento.setLiberado(false);
					faturamento.setManual(false);
					faturamento.setObsContrato(producao.getContrato().getObsContrato());
					faturamento.setProducao(producao);
					faturamento.setProduto(producao.getProduto());
					Date hoje = java.util.Calendar.getInstance().getTime();
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String dataString = dateFormat.format(hoje);
					faturamento.setDataCriacao(dataString);
					faturamentoService.create(faturamento);
				}
			}

			break;
		case "Envio do pleito":

			if (getFaturamentoByProducaoEtapa(producao.getId(), 112l).isEmpty()) {

				System.out.println("!-! Entrou Envio do pleito");

				EtapasPagamento timing = producao.getContrato().getEtapasPagamento();

				if (Objects.nonNull(timing.getEtapa1())) {
					if (timing.getEtapa1().getId() == 112l) {
						faturamento.setValorEtapa(timing.getValor1());
					}
				}
				if (Objects.nonNull(timing.getEtapa2())) {
					if (timing.getEtapa2().getId() == 112l) {
						faturamento.setValorEtapa(timing.getValor2());
					}
				}
				if (Objects.nonNull(timing.getEtapa3())) {
					if (timing.getEtapa3().getId() == 112l) {
						faturamento.setValorEtapa(timing.getValor3());
					}
				}
				if (Objects.nonNull(timing.getEtapa4())) {
					if (timing.getEtapa4().getId() == 112l) {
						faturamento.setValorEtapa(timing.getValor4());
					}
				}

				if (faturamento.getValorEtapa() != null) {

					faturamento.setCampanha(producao.getAno());
					faturamento.setCliente(producao.getCliente());
					faturamento.setCnpjFaturar(producao.getContrato().getCnpjFaturar());
					faturamento.setContrato(producao.getContrato());
					faturamento.setEstado(EstadoEtapa.ETAPA_FINALIZADA.toString());
					faturamento.setEtapa(etapaTrabalhoService.findById(112l));
					faturamento.setLiberado(false);
					faturamento.setManual(false);
					faturamento.setObsContrato(producao.getContrato().getObsContrato());
					faturamento.setProducao(producao);
					faturamento.setProduto(producao.getProduto());
					
					Date hoje = java.util.Calendar.getInstance().getTime();
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String dataString = dateFormat.format(hoje);
					faturamento.setDataCriacao(dataString);
					faturamentoService.create(faturamento);
				}
			}

			break;

		case "Aprova��o/libera��o":

			break;

		case "Acompanhamento/parcelas":

			break;

		}

		System.out.println("!-! Faturamento criado no valor de " + faturamento.getValorEtapa());

		return faturamento;

	}

	public void sendEmail(Faturamento faturamento) {
		System.out.println("Envio de e-mail acionado!");

		try {

			String cnpj = faturamento.getCliente().getCnpj();

			try {
				MaskFormatter mask = new MaskFormatter("###.###.###/####-##");
				mask.setValueContainsLiteralCharacters(false);
				cnpj = mask.valueToString(cnpj);
			} catch (ParseException ex) {

			}

			List<EnderecoCliente> enderecos = enderecoService.getAllById(faturamento.getCliente().getId());
			EnderecoCliente endereco = new EnderecoCliente();
			for (EnderecoCliente enderecoCliente : enderecos) {
				if (enderecoCliente.isEnderecoNotaFiscalBoleto()) {
					endereco = enderecoCliente;
				}
			}

			List<ContatoCliente> contatos = contatoClienteService.getAllById(faturamento.getCliente().getId());
			String contatosS = "";
			String contatosE = "";
			String contatosT = "";
			for (ContatoCliente contatoCliente : contatos) {
				if (contatoCliente.isPessoaNfBoleto()) {
					contatosS = contatoCliente.getNome() + " / ";
					contatosE = contatoCliente.getEmail() + " / ";
					contatosT = contatoCliente.getTelefone1() + " / ";
				}
			}

			String etapaFaturar = "";
			if (Objects.nonNull(faturamento.getEtapa())) {
				etapaFaturar = faturamento.getEtapa().getNome();
			} else {
				etapaFaturar = "Sem etapa atribu�da.";
			}

			String percentualFaturado = "";
			if (Objects.nonNull(faturamento.getEtapa())) {
				EtapasPagamento etapas = faturamento.getContrato().getEtapasPagamento();
				if (etapas.getEtapa1() == faturamento.getEtapa()) {
					percentualFaturado = Float.toString(etapas.getPorcentagem1());
				}
				if (etapas.getEtapa2() == faturamento.getEtapa()) {
					percentualFaturado = Float.toString(etapas.getPorcentagem2());
				}
				if (etapas.getEtapa3() == faturamento.getEtapa()) {
					percentualFaturado = Float.toString(etapas.getPorcentagem3());
				}
			} else {
				percentualFaturado = "Sem percentual atribu�do";
			}

			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtps");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			props.put("mail.smtps.ssl.protocols", "TLSv1.2"); // <- TLSv1.2 is set here
			Session session = Session.getDefaultInstance(props);

			Email email = new SimpleEmail();
			email.setHostName("SMTP.office365.com");
			email.setSmtpPort(587);
			email.setStartTLSEnabled(true);
			email.getMailSession().getProperties().put("mail.smtps.ssl.protocols", "TLSv1.2");
//			email.setSSLOnConnect(true);
//			email.setStartTLSEnabled(true);
//			email.setStartTLSRequired(true);
			email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@@fi*1303"));
			email.setFrom("fseweb@f-iniciativas.com.br");
			email.addTo("gabriel.sassi@f-iniciativas.com.br");
			email.setSubject("Faturamento Liberado - " + faturamento.getCliente().getRazaoSocial());
			email.setMsg("-- Informa��es Gerais: " + "\n Raz�o Social: " + faturamento.getCliente().getRazaoSocial()
					+ "\n CNPJ: " + cnpj + "\n Total Faturar: R$ " + faturamento.getValorEtapa() + "\n Servi�os: "
					+ faturamento.getContrato().getProduto().getNome() + "\n Observa��es: "
					+ faturamento.getObservacao() + "\n" + "\n-- Informa��es da Nota Fiscal" + "\n Forma de Pagamento: "
					+ faturamento.getContrato().getFormaPagamento() + "\n Etapa Faturar: " + etapaFaturar
					+ "\n Percentual da Etapa Faturado: " + percentualFaturado + "%" + "\n"
					+ "\n-- Informa��es do Contato: " + "\n Nome Contatos: " + contatosS + "\n E-mail Contato: "
					+ contatosE + "\n Telefone Contato: " + contatosT + "\n Endere�o Entrega Boleto: "
					+ endereco.getLogradouro());

//			ReceptoresEmail receptores = receptoresEmailService.findById(1l);
//			List<Consultor> users = receptores.getReceptores();
//			for (Consultor consultor : users) {
//				email.addTo(consultor.getEmail());
//			}

			email.send();
			System.out.println("E-mail enviado!");
		} catch (org.apache.commons.mail.EmailException e) {
			e.printStackTrace();
		}
	}

	public void sendEmailSemContrato(Faturamento faturamento) {
		System.out.println("Envio de e-mail acionado!");

		try {

			List<EnderecoCliente> enderecos = enderecoService.getAllById(faturamento.getCliente().getId());
			EnderecoCliente endereco = new EnderecoCliente();
			for (EnderecoCliente enderecoCliente : enderecos) {
				if (enderecoCliente.isEnderecoNotaFiscalBoleto()) {
					endereco = enderecoCliente;
				}
			}

			List<ContatoCliente> contatos = contatoClienteService.getAllById(faturamento.getCliente().getId());
			String contatosS = "";
			String contatosE = "";
			String contatosT = "";
			for (ContatoCliente contatoCliente : contatos) {
				if (contatoCliente.isPessoaNfBoleto()) {
					contatosS = contatoCliente.getNome() + " / ";
					contatosE = contatoCliente.getEmail() + " / ";
					contatosT = contatoCliente.getTelefone1() + " / ";
				}
			}

			String etapaFaturar = "";
			if (Objects.nonNull(faturamento.getEtapa())) {
				etapaFaturar = faturamento.getEtapa().getNome();
			} else {
				etapaFaturar = "Sem etapa atribu�da. (Sem Contrato)";
			}

			String percentualFaturado = "Sem percentual atribu�do. (Sem Contrato)";

			Email email = new SimpleEmail();
			email.setHostName("SMTP.office365.com");
			email.setSmtpPort(25);
			email.setSSLOnConnect(false);
			email.setStartTLSEnabled(true);
			email.setStartTLSRequired(true);
			email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@idkOidj"));
			email.setFrom("fseweb@f-iniciativas.com.br");
			email.setSubject("Teste");
			email.setMsg("Faturamento Manual (Sem contrato): \n" + "- Informa��es Gerais: " + "\n Raz�o Social: "
					+ faturamento.getCliente().getRazaoSocial() + "\n CNPJ: " + faturamento.getCliente().getCnpj()
					+ "\n Total Faturar: R$ " + faturamento.getValorEtapa() + "\n Servi�os: " + faturamento.getProduto()
					+ "\n Observa��es: " + faturamento.getObservacao() + "\n" + "\n- Informa��es da Nota Fiscal"
					+ "\n Etapa Faturar: " + etapaFaturar + "\n Percentual da Etapa Faturado: " + percentualFaturado
					+ "\n" + "\n- Informa��es do Contato: " + "\n Nome Contatos: " + contatosS + "\n E-mail Contato: "
					+ contatosE + "\n Telefone Contato: " + contatosT + "\n Endere�o Entrega Boleto: "
					+ endereco.getLogradouro());
			email.addTo("ericson.dutra@f-inicitivas.com.br");
			email.send();
			System.out.println("E-mail enviado!");
		} catch (org.apache.commons.mail.EmailException e) {
			System.out.println(e);
		}
	}

	public void geraTxtFaturamento(List<Faturamento> fat) {

		LocalDate localDate = LocalDate.now();// For reference
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String data = localDate.format(formatter);

		data = data.replace("/", "");

		String linhaA = "";
		String linhaB = "";
		String linhaC = "";
		String linhaF = "";
		String linhaE = "";

		String cep = "";
		String estoque = "999999999";
		String rais = "";
		String num = "568";
		String bairro = "";
		String referencia = "8";
		String subgrupo = "SERVI�OS PRESTADOS";

		String txtFinal = "";

		EnderecoCliente enderecoPrincipal = null;

		int index = 0;

		for (Faturamento faturamento : fat) {

			List<EnderecoCliente> endereco = enderecoService.getAllById(faturamento.getCliente().getId());
			for (EnderecoCliente ender : endereco) {
				if (ender.isEnderecoNotaFiscalBoleto()) {
					enderecoPrincipal = ender;
					cep = ender.getCep();
					bairro = ender.getBairro();
				}
			}

			cep = cep.replace("-", "");

			// Registro A
			// 0
			linhaA = "A;"
					// 1 Raz�o Social
					+ faturamento.getCliente().getRazaoSocial() + ";"
					// 2 Nome Fantasia
					+ faturamento.getCliente().getRazaoSocial() + ";"
					// 3 CNPJ/ CPF
					+ faturamento.getCliente().getCnpj() + ";"
					// 4 CEP
					+ cep + ";"
					// 5 C�digo RAIS !!
					+ rais + ";"
					// 6 Endere�o
					+ enderecoPrincipal.getLogradouro() + ";"
					// 7 N�mero Endere�o
					+ num + ";"
					// 8 Complemento
					+ ";"
					// 9 Bairro
					+ bairro + ";"
					// 10 Inscri��o Estadual
					+ "ISENTO;"
					// 11 Inscri��o Municipal
					+ ";"
					// 12 DDD
					+ ";"
					// 13 Telefone
					+ ";"
					// 14 Email
					+ ";"
					// 15 Transportador
					+ ";"
					// 16 Retem PIS
					+ ";"
					// 17 Retem COFINS
					+ ";"
					// 18 Retem CSLL
					+ ";"
					// 19 Retem INSS
					+ ";"
					// 20 Retem IRRF
					+ ";"
					// 21 Retem ISS
					+ ";"
					// 22 Desconta Retidos Total
					+ ";\n";

			// Registro B
			// 0
			linhaB = "B;"
					// 1 Empresa
					+ faturamento.getCliente().getId() + ";"
					// 2 Refer�ncia
					+ referencia + ";"
					// 3 Conta/Sub-grupo
					+ subgrupo + ";"
					// 4 Descri��o Sub-grupo
					+ "SERVI�OS;"
					// 5 Descri��o Produto
					+ faturamento.getContrato().getProduto().getNome() + ";"
					// 6 Sigla Unidade
					+ "UN;"
					// 7 Valor de Venda
					+ "1.00;"
					// 8 Tipo do Item
					+ ";"
					// 9 NCM do Item
					+ "99999999;"
					// 10 C�digo EAN do Item
					+ ";"
					// 11 Classifica��o
					+ ";"
					// 12 Peso Bruto
					+ ";"
					// 13 Peso Liquido
					+ ";\n";

			// Registro C
			// 0
			linhaC = "C;"
					// 1 C�digo Empresa
					+ faturamento.getCliente().getId() + ";"
					// 2 C�digo Pedido
					+ faturamento.getId() + ";"
					// 3 CPF/CNPJ Cliente
					+ faturamento.getCliente().getCnpj() + ";"
					// 4 CPF/CNPJ Vend.
					+ "13773581000146;"
					// 5 Forma de Pagto
					+ "1" + ";"
					// 6 Data Entrega
					+ data + ";"
					// 7 Data Emiss�o
					+ data + ";"
					// 8 Tipo do Frete
					+ "9;"
					// 9 Observa��es
					+ ";"
					// 10 Carteira
					+ ";"
					// 11 Ordem de Compra
					+ ";\n";

			// Registro F
			// 0
			linhaF = "F;"
					// 1 C�digo Pedido
					+ faturamento.getId() + ";"
					// 2 Refer�ncia
					+ referencia + ";"
					// 3 Natureza de Estoque
					+ estoque + ";"
					// 4 CPF/CNPJ Vend.
					+ "13773581000146" + ";"
					// 5 Qtd. Vendida
					+ "1;"
					// 6 Valor Unit�rio
					+ faturamento.getValorEtapa() + ";"
					// 7 Valor Total
					+ faturamento.getValorEtapa() + ";"
					// 8 Observa��es
					+ ";"
					// 9 Al�quota ISS
					+ ";"
					// 10 Al�quota IRRF
					+ ";"
					// 11 Al�quota INSS
					+ ";"
					// 12 Al�quota CSLL
					+ ";\n";

			txtFinal += linhaA + linhaB + linhaC + linhaF;

			index++;

		}

		linhaE = "E;" + index + ";\n";
		txtFinal += linhaE;

		try (PrintWriter out = new PrintWriter(new FileOutputStream("filename.txt"))) {
			out.write(txtFinal);
			out.flush();
			out.close();
		} catch (Exception e) {

		}

	}

	public List<Faturamento> getFaturamentoByProducaoEtapaContrato(Long producao, Long etapa, Long contrato) {
		try {
			return em.createQuery(
					"SELECT f FROM Faturamento f WHERE f.producao.id = :pProducao AND f.contrato.id = :pContrato AND f.etapa.id = :pEtapa",
					Faturamento.class).setParameter("pProducao", producao).setParameter("pContrato", contrato)
					.setParameter("pEtapa", etapa).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Faturamento> getFaturamentosNaoConsolidadosByProducaoAndEtapa(Long idProducao, Long etapa){
		try {
			return em.createQuery(
					"SELECT f FROM Faturamento f WHERE f.producao.id = :pProducao AND f.consolidado = 0 AND f.etapa.id = :pEtapa",
					Faturamento.class).setParameter("pProducao", idProducao)
					.setParameter("pEtapa", etapa).getResultList();
		}catch(Exception e) {
			System.out.println("sem faturamento");
			List<Faturamento> faturamentos = new ArrayList();
			return faturamentos;
		}
	}

	public Faturamento getFaturamentoByProducaoEtapaContrato2(Long producao, Long etapa, Long contrato) {
		try {
			return em.createQuery(
					"SELECT f FROM Faturamento f WHERE f.producao.id = :pProducao AND f.etapa.id = :pEtapa AND f.contrato.id = :pContrato",
					Faturamento.class).setParameter("pProducao", producao).setParameter("pEtapa", etapa)
					.setParameter("pContrato", contrato).getSingleResult();
		} catch (Exception e) {
			System.out.println("N�o encontramos nenhum faturamento com os par�metros");
			System.out.println(e);
			return null;
		}
	}

	public List<Faturamento> getFaturamentoByProducaoEtapa(Long producao, Long etapa) {
		try {
			return em
					.createQuery(
							"SELECT f FROM Faturamento f WHERE f.producao.id = :pProducao AND f.etapa.id = :pEtapa",
							Faturamento.class)
					.setParameter("pProducao", producao).setParameter("pEtapa", etapa).getResultList();
		} catch (Exception e) {
			System.out.println("N�o encontramos nenhum faturamento com os par�metros");
			System.out.println(e);
			return null;
		}
	}

	public Faturamento getFaturamentoByProd(Long idProducao) {
		try {
			return em.createQuery("SELECT f FROM Faturamento f WHERE f.producao.id = :pProducao", Faturamento.class)
					.setParameter("pProducao", idProducao).getSingleResult();
		} catch (Exception e) {
			System.out.println("Erro no m�todo: getFaturamentoByProd " + e);
			return null;
		}
	}

	public Faturamento getFaturamentoByProdOrContrato(Long idProducao, Long idContrato) {
		System.out.println(idProducao);
		try {
			return em.createQuery(
					"SELECT f FROM Faturamento f WHERE f.producao.id = :pProducao OR f.contrato.id = :pContrato",
					Faturamento.class).setParameter("pProducao", idProducao).setParameter("pContrato", idContrato)
					.getSingleResult();
		} catch (Exception e) {
			System.out.println("Erro no m�todo: getFaturamentoByProd " + e);
			return null;
		}
	}

	public List<Faturamento> getFaturamentoByContrato(Long contrato) {
		try {
			return em.createQuery("SELECT f FROM Faturamento f WHERE f.contrato.id = :pContrato", Faturamento.class)
					.setParameter("pContrato", contrato).getResultList();
		} catch (Exception e) {
			System.out.println("N�o encontramos nenhum faturamento com os par�metros");
			return null;
		}
	}

	public Faturamento getLastFaturamentoByBalanceteAndEtapa(Long idProd, Long idEtapa) {
		try {
			return em.createQuery(
					"SELECT f FROM Faturamento f WHERE f.producao.id = :pIdProd AND f.etapa.id = :pIdEtapa AND f.id = (SELECT max(f.id) FROM Faturamento f WHERE f.producao.id = :pIdProd AND f.etapa.id = :pIdEtapa)",
					Faturamento.class).setParameter("pIdProd", idProd).setParameter("pIdEtapa", idEtapa)
					.getSingleResult();
		} catch (Exception e) {
			System.out.println("N�o encontramos nenhum faturamento com os par�metros");
			return null;
		}
	}

	public void envioFaturamentoLiberado(Faturamento fat) {

		if (!fat.isManual()) {
			List<Honorario> hons = fat.getContrato().getHonorarios();

			try {

				Properties props = System.getProperties();
				props.put("mail.transport.protocol", "smtps");
				props.put("mail.smtp.port", "587");
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.starttls.required", "true");
				props.put("mail.smtps.ssl.protocols", "TLSv1.2"); // <- TLSv1.2 is set here
				Session session = Session.getDefaultInstance(props);

				// Create the email message
				HtmlEmail email = new HtmlEmail();
				email.setHostName("SMTP.office365.com");
				email.setSmtpPort(587);
				email.setSSLOnConnect(false);
				email.setStartTLSEnabled(true);
				email.setStartTLSRequired(true);
				email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@@fi*1303"));
				email.setFrom("fseweb@f-iniciativas.com.br");
				email.setSubject("Faturamento Liberado " + fat.getCliente().getRazaoSocial());
				email.addTo("antonio.marques@fi-group.com");

				// set the html message
				StringBuffer msg = new StringBuffer();
				msg.append("<html><body>");
				msg.append("<b>Aten��o! Envio autom�tico pelo FSEWeb, n�o responder.</b><br/>");
				msg.append("<br/>");
				msg.append("Prezado(a),<br/>");
				msg.append("<br/>");
				msg.append("Novo faturamento liberado para o cliente: <b>" + fat.getCliente().getRazaoSocial()
						+ "</b><br/>");
				msg.append("<br/>");
				msg.append("Este referente �: <b>" + fat.getProduto().getNome() + ' ' + fat.getCampanha() + "</b>");
				msg.append("<br/>");
				msg.append("<br/>");

				NumberFormat nf = NumberFormat.getCurrencyInstance();
				String valor = nf.format(fat.getValorEtapa());

				msg.append("A nota fiscal a ser emitida ser� do seguinte valor: " + valor);
				msg.append("<br/>");

				BalanceteCalculo balanceteLast = fat.getBalancete();

				for (Honorario honorario : hons) {
					if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
						msg.append("<br/>");
						msg.append("<b>C�lculo de Fatura��o. (% Escalonado)</b><br/>");
						msg.append("<br/>");
						msg.append(
								"<table style='width: 0 auto;  border: 1px solid black; border-collapse: collapse;'>");
						msg.append("<tr><th style='padding: 5px; border: 1px solid black;'>Intervalo de Fatura��o</th>"
								+ "<th style='padding: 5px; border: 1px solid black;'>%</th>"
								+ "<th style='padding: 5px; border: 1px solid black;'>Valor de Benef�cio Associado</th>"
								+ "<th style='padding: 5px; border: 1px solid black;'>Fatura��o FI</th></tr>");

						PercentualEscalonado pe = honorarioService
								.getPercentualEscalonadoByContrato(fat.getContrato().getId());
						String stringValorBalancete = balanceteLast.getReducaoImposto();

						if (Objects.nonNull(pe)) {

							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorFinal = BigDecimal.valueOf(0);
							BigDecimal valorCalc = BigDecimal.valueOf(0);

							stringValorBalancete = stringValorBalancete.replace(".", "");
							stringValorBalancete = stringValorBalancete.replace(",", ".");
							stringValorBalancete = stringValorBalancete.trim();
							BigDecimal balancete = new BigDecimal(stringValorBalancete);
							BigDecimal temp = new BigDecimal(stringValorBalancete);

							for (int i = 0; i < faixas.size(); i++) {
								FaixasEscalonado faixa = faixas.get(i);

								msg.append("<tr>");

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								BigDecimal gap2 = temp.subtract(inicial);
								BigDecimal gapSum = BigDecimal.valueOf(0);
								Float percentual = faixa.getValorPercentual();

								NumberFormat nfStr = NumberFormat.getCurrencyInstance();
								String inicialStr = nfStr.format(inicial);
								String finalStr = nfStr.format(finalF);

								msg.append(
										"<td style='padding: 5px; border: 1px solid black; text-align: center; '>De: "
												+ inicialStr + " � " + finalStr + "</td>");
								msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
										+ percentual + "%</td>");

								String gapStr = nfStr.format(gap1);

								msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>"
										+ gapStr + "</td>");

								gapSum.add(gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(gap1) >= 0) {

									valorFinal = valorFinal.add(
											gap1.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));
									valorCalc = gap1
											.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100)));

									temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

									// Se est� entre a faixa, calcula com o temp direto na porcentagem.
								} else {

									valorFinal = valorFinal.add(
											temp.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));
									valorCalc = temp
											.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100)));

									BigDecimal zero = BigDecimal.ZERO;
									temp = zero;

								}

								String calcStr = nfStr.format(valorCalc);

								msg.append("<td style='padding: 5px; border: 1px solid black;  text-align: center; '>"
										+ calcStr + "</td>");
								msg.append("</tr>");

								if (i == faixas.size() - 1) {
									if (pe.getAcimaDe() != null) {
										msg.append("<tr>");
										if (balancete.compareTo(pe.getAcimaDe()) > 0) {
											if (temp.compareTo(BigDecimal.ZERO) > 0) {
												BigDecimal acima = temp.multiply(BigDecimal
														.valueOf(pe.getPercentAcimaDe()).divide(new BigDecimal(100)));
												valorFinal = valorFinal.add(acima);

												String acimaStr = nfStr.format(pe.getAcimaDe());
												String acimaStr2 = nfStr.format(acima);

												msg.append(
														"<td style='padding: 5px; border: 1px solid black; text-align: center; '>Acima de "
																+ acimaStr + "</td>");
												msg.append(
														"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
																+ pe.getPercentAcimaDe() + "%</td>");
												msg.append(
														"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
																+ acimaStr2 + "</td>");
												msg.append(
														"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
																+ acimaStr2 + "</td>");
												msg.append("</tr>");

											}
										}
									}

									String vfStr = nfStr.format(valorFinal);
									String gapSumStr = nfStr.format(gapSum);

									msg.append(
											"<td colspan='2' style='padding: 5px; border: 1px solid black; text-align: center;'>Total</td>");
									msg.append(
											"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
													+ gapSumStr + "</td>");
									msg.append(
											"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
													+ vfStr + "</td>");

								}
							}

							msg.append("</table>");

						}
					} else if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {

						Float calc = 0.0f;
						ValorFixo fixo = honorarioService.getValorFixoByContrato(fat.getContrato().getId());
						if (Objects.nonNull(fixo)) {

							BigDecimal valorFixo = fixo.getValor();
							calc = valorFixo.setScale(2, RoundingMode.DOWN).floatValue();
						}

						String calcStr = nf.format(calc);

						msg.append("<br/>");
						msg.append("<b>Valor Fixo:</b> " + calcStr);
						msg.append("<br/>");

					} else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {

						PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(fat.getContrato().getId());

						if (Objects.nonNull(fixo)) {

							String stringValorBalancete = balanceteLast.getReducaoImposto();

							BigDecimal valorFinal = BigDecimal.valueOf(0);

							stringValorBalancete = stringValorBalancete.replace(".", "");
							stringValorBalancete = stringValorBalancete.replace(",", ".");
							stringValorBalancete = stringValorBalancete.trim();
							BigDecimal balancete = new BigDecimal(stringValorBalancete);

							valorFinal = balancete.multiply(
									BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

							if (fixo.getLimitacao() != null) {
								if (fixo.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
									if (valorFinal.compareTo(fixo.getLimitacao()) >= 0) {
										valorFinal = fixo.getLimitacao();
									}
								}
							}

							String balStr = nf.format(balancete);
							String valStr = nf.format(valorFinal);

							msg.append("<br/>");
							msg.append("<b>Percentual Fixo:</b>");
							msg.append("<br/>");
							msg.append(
									"<table style='width: 0 auto;  border: 1px solid black; border-collapse: collapse;'>");
							msg.append(
									"<tr><th style='padding: 5px; border: 1px solid black; text-align: center;' >%</th><th style='padding: 5px; border: 1px solid black; text-align: center;' >Valor do Benef�cio</th><th style='padding: 5px; border: 1px solid black; text-align: center;'>Fatura��o FI</th></tr>");
							msg.append("<tr><td style='padding: 5px; border: 1px solid black; text-align: center;' >"
									+ fixo.getValorPercentual() + "%</td>");
							msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>"
									+ balStr + "</td>");
							msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>"
									+ valStr + "</td>");
							msg.append("</table>");

						}
					}
				}

				String vfStr = nf.format(fat.getValorEtapa());

				msg.append("<br/>");
				msg.append("<b>Timings De Fatura��o Definidos Em Contrato</b>");
				msg.append("<table style='width: 0 auto;  border: 1px solid black; border-collapse: collapse;'>");
				msg.append(
						"<tr><th style='padding: 5px; border: 1px solid black; text-align: center;' >Timings</th><th style='padding: 5px; border: 1px solid black; text-align: center;' >%</th><th style='padding: 5px; border: 1px solid black; text-align: center;'>Valor</th></tr>");
				msg.append("<tr>");
				msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>"
						+ fat.getEtapa().getNome() + "</td>");
				msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>"
						+ fat.getPercetEtapa() + "%</td>");
				msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>" + vfStr + "</td>");
				msg.append("</tr></table>");
				msg.append("<br/>");
				msg.append("O modelo de c�lculo tem em considera��o a informa��o definida em contrato.");
				msg.append("<br/>");
				msg.append("Obrigado.");

				msg.append("</body></html>");
				email.setHtmlMsg(msg.toString());

				// set the alternative message
				email.setTextMsg("Your email client does not support HTML messages");
				// send the email
				email.send();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			try {

				HtmlEmail email = new HtmlEmail();
				email.setHostName("SMTP.office365.com");
				email.setSmtpPort(587);
				email.setSSLOnConnect(false);
				email.setStartTLSEnabled(true);
				email.setStartTLSRequired(true);
				email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@@fi*1303"));
				email.setFrom("fseweb@f-iniciativas.com.br");
				email.setSubject("Faturamento Liberado " + fat.getCliente().getRazaoSocial());
				email.addTo("gabriel.sassi@fi-group.com");

				StringBuffer msg = new StringBuffer();
				msg.append("<html><body>");
				msg.append("<b>Aten��o! Envio autom�tico pelo FSEWeb, n�o responder.</b><br/>");
				msg.append("<br/>");
				msg.append("Prezado(a),<br/>");
				msg.append("<br/>");
				msg.append("Novo faturamento liberado para o cliente: <b>" + fat.getCliente().getRazaoSocial()
						+ "</b><br/>");
				msg.append("<br/>");
				msg.append("Este referente �: <b>" + fat.getProduto().getNome() + ' ' + fat.getCampanha() + "</b>");
				msg.append("<br/>");
				msg.append("<br/>");

				NumberFormat nf = NumberFormat.getCurrencyInstance();
				String valor = nf.format(fat.getValorEtapa());

				msg.append("A nota fiscal a ser emitida ser� do seguinte valor: " + valor);
				msg.append("<br/>");
				msg.append("<br/>");
				msg.append(
						"O faturamento em quest�o trata-se de um lan�amento manual no sistema, sendo assim, n�o existe l�gica de c�lculo dispon�vel para exibi��o.");
				msg.append("<br/>");
				msg.append("<br/>");
				msg.append("Obrigado.");

				email.setHtmlMsg(msg.toString());

				// set the alternative message
				email.setTextMsg("Your email client does not support HTML messages");
				// send the email
				email.send();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void envioDeCobranca(Faturamento fat) {

		List<Honorario> hons = fat.getContrato().getHonorarios();

		try {

			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtps");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			props.put("mail.smtps.ssl.protocols", "TLSv1.2"); // <- TLSv1.2 is set here
			Session session = Session.getDefaultInstance(props);

			// Create the email message
			HtmlEmail email = new HtmlEmail();
			email.setHostName("SMTP.office365.com");
			email.setSmtpPort(587);
			email.setSSLOnConnect(false);
			email.setStartTLSEnabled(true);
			email.setStartTLSRequired(true);
			email.setAuthenticator(new DefaultAuthenticator("fseweb@f-iniciativas.com.br", "@@fi*1303"));
			email.setFrom("fseweb@f-iniciativas.com.br");
			email.setSubject("Faturamento Liberado");
			email.addTo("gabriel.sassi@fi-group.com");

			// set the html message
			StringBuffer msg = new StringBuffer();
			msg.append("<html><body>");
			msg.append("Prezado(a),<br/>");
			msg.append("<br/>");
			msg.append(
					"Venho atrav�s do presente e-mail informar que o timing da nossa parceria nos exige a tratativa da emiss�o da Nota Fiscal da presta��o dos servi�os realizados.<br/>");
			msg.append("Este e-mail vem no sentido de alinhar essa tratativa.<br/>");
			msg.append("<br/>");
			msg.append("Estes referentes �: <b>" + fat.getProduto().getNome() + ' ' + fat.getCampanha() + "</b>");
			msg.append("<br/>");
			msg.append("<br/>");

			NumberFormat nf = NumberFormat.getCurrencyInstance();
			String valor = nf.format(fat.getValorEtapa());

			msg.append("Desse modo, informo que a nota fiscal a ser emitida ser� do seguinte valor: " + valor);
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append(
					"Para melhor entendimento, passo a apresentar o demonstrativo para o c�lculo do valor de faturamento.<br/>");

			BalanceteCalculo balanceteLast = fat.getBalancete();

			for (Honorario honorario : hons) {
				if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
					msg.append("<br/>");
					msg.append("<b>C�lculo de Fatura��o. (% Escalonado)</b><br/>");
					msg.append("<br/>");
					msg.append("<table style='width: 0 auto;  border: 1px solid black; border-collapse: collapse;'>");
					msg.append("<tr><th style='padding: 5px; border: 1px solid black;'>Intervalo de Fatura��o</th>"
							+ "<th style='padding: 5px; border: 1px solid black;'>%</th>"
							+ "<th style='padding: 5px; border: 1px solid black;'>Valor de Benef�cio Associado</th>"
							+ "<th style='padding: 5px; border: 1px solid black;'>Fatura��o FI</th></tr>");

					PercentualEscalonado pe = honorarioService
							.getPercentualEscalonadoByContrato(fat.getContrato().getId());
					String stringValorBalancete = balanceteLast.getReducaoImposto();

					if (Objects.nonNull(pe)) {

						List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

						BigDecimal valorFinal = BigDecimal.valueOf(0);
						BigDecimal valorCalc = BigDecimal.valueOf(0);

						stringValorBalancete = stringValorBalancete.replace(".", "");
						stringValorBalancete = stringValorBalancete.replace(",", ".");
						stringValorBalancete = stringValorBalancete.trim();
						BigDecimal balancete = new BigDecimal(stringValorBalancete);
						BigDecimal temp = new BigDecimal(stringValorBalancete);

						for (int i = 0; i < faixas.size(); i++) {
							FaixasEscalonado faixa = faixas.get(i);

							msg.append("<tr>");

							// Define vari�veis
							BigDecimal inicial = faixa.getValorInicial();
							BigDecimal finalF = faixa.getValorFinal();
							BigDecimal gap1 = finalF.subtract(inicial);
							BigDecimal gap2 = temp.subtract(inicial);
							BigDecimal gapSum = BigDecimal.valueOf(0);
							Float percentual = faixa.getValorPercentual();

							NumberFormat nfStr = NumberFormat.getCurrencyInstance();
							String inicialStr = nfStr.format(inicial);
							String finalStr = nfStr.format(finalF);

							msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center; '>De: "
									+ inicialStr + " � " + finalStr + "</td>");
							msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
									+ percentual + "%</td>");

							String gapStr = nfStr.format(gap1);

							msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>"
									+ gapStr + "</td>");

							gapSum.add(gap1);

							// Se � maior que o final, calcula direto com o gap1
							if (temp.compareTo(gap1) >= 0) {

								valorFinal = valorFinal
										.add(gap1.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));
								valorCalc = gap1.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100)));

								temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

								// Se est� entre a faixa, calcula com o temp direto na porcentagem.
							} else {

								valorFinal = valorFinal
										.add(temp.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));
								valorCalc = temp.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100)));

								BigDecimal zero = BigDecimal.ZERO;
								temp = zero;

							}

							String calcStr = nfStr.format(valorCalc);

							msg.append("<td style='padding: 5px; border: 1px solid black;  text-align: center; '>"
									+ calcStr + "</td>");
							msg.append("</tr>");

							if (i == faixas.size() - 1) {
								if (pe.getAcimaDe() != null) {
									msg.append("<tr>");
									if (balancete.compareTo(pe.getAcimaDe()) > 0) {
										if (temp.compareTo(BigDecimal.ZERO) > 0) {
											BigDecimal acima = temp.multiply(BigDecimal.valueOf(pe.getPercentAcimaDe())
													.divide(new BigDecimal(100)));
											valorFinal = valorFinal.add(acima);

											String acimaStr = nfStr.format(pe.getAcimaDe());
											String acimaStr2 = nfStr.format(acima);

											msg.append(
													"<td style='padding: 5px; border: 1px solid black; text-align: center; '>Acima de "
															+ acimaStr + "</td>");
											msg.append(
													"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
															+ pe.getPercentAcimaDe() + "%</td>");
											msg.append(
													"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
															+ acimaStr2 + "</td>");
											msg.append(
													"<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
															+ acimaStr2 + "</td>");
											msg.append("</tr>");

										}
									}
								}

								String vfStr = nfStr.format(valorFinal);
								String gapSumStr = nfStr.format(gapSum);

								msg.append(
										"<td colspan='2' style='padding: 5px; border: 1px solid black; text-align: center;'>Total</td>");
								msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
										+ gapSumStr + "</td>");
								msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center; '>"
										+ vfStr + "</td>");

							}
						}

						msg.append("</table>");

					}
				} else if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {

					Float calc = 0.0f;
					ValorFixo fixo = honorarioService.getValorFixoByContrato(fat.getContrato().getId());
					if (Objects.nonNull(fixo)) {

						BigDecimal valorFixo = fixo.getValor();
						calc = valorFixo.setScale(2, RoundingMode.DOWN).floatValue();
					}

					String calcStr = nf.format(calc);

					msg.append("<br/>");
					msg.append("<b>Valor Fixo:</b> " + calcStr);
					msg.append("<br/>");

				} else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {

					PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(fat.getContrato().getId());

					if (Objects.nonNull(fixo)) {

						String stringValorBalancete = balanceteLast.getReducaoImposto();

						BigDecimal valorFinal = BigDecimal.valueOf(0);

						stringValorBalancete = stringValorBalancete.replace(".", "");
						stringValorBalancete = stringValorBalancete.replace(",", ".");
						stringValorBalancete = stringValorBalancete.trim();
						BigDecimal balancete = new BigDecimal(stringValorBalancete);

						valorFinal = balancete
								.multiply(BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

						if (fixo.getLimitacao() != null) {
							if (fixo.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
								if (valorFinal.compareTo(fixo.getLimitacao()) >= 0) {
									valorFinal = fixo.getLimitacao();
								}
							}
						}

						String balStr = nf.format(balancete);
						String valStr = nf.format(valorFinal);

						msg.append("<br/>");
						msg.append("<b>Percentual Fixo:</b>");
						msg.append("<br/>");
						msg.append(
								"<table style='width: 0 auto;  border: 1px solid black; border-collapse: collapse;'>");
						msg.append(
								"<tr><th style='padding: 5px; border: 1px solid black; text-align: center;' >%</th><th style='padding: 5px; border: 1px solid black; text-align: center;' >Valor do Benef�cio</th><th style='padding: 5px; border: 1px solid black; text-align: center;'>Fatura��o FI</th></tr>");
						msg.append("<tr><td style='padding: 5px; border: 1px solid black; text-align: center;' >"
								+ fixo.getValorPercentual() + "%</td>");
						msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>" + balStr
								+ "</td>");
						msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>" + valStr
								+ "</td>");
						msg.append("</table>");

					}
				}
			}

			String vfStr = nf.format(fat.getValorEtapa());

			msg.append("<br/>");
			msg.append("<b>Timings De Fatura��o Definidos Em Contrato</b>");
			msg.append("<table style='width: 0 auto;  border: 1px solid black; border-collapse: collapse;'>");
			msg.append(
					"<tr><th style='padding: 5px; border: 1px solid black; text-align: center;' >Timings</th><th style='padding: 5px; border: 1px solid black; text-align: center;' >%</th><th style='padding: 5px; border: 1px solid black; text-align: center;'>Valor</th></tr>");
			msg.append("<tr>");
			msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>"
					+ fat.getEtapa().getNome() + "</td>");
			msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>" + fat.getPercetEtapa()
					+ "%</td>");
			msg.append("<td style='padding: 5px; border: 1px solid black; text-align: center;'>" + vfStr + "</td>");
			msg.append("</tr></table>");
			msg.append("<br/>");
			msg.append("Refor�o que o modelo de c�lculo tem em considera��o a informa��o definida em contrato.");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("Tendo em considera��o o demonstrado, podemos seguir com a emiss�o da Nota Fiscal?");
			msg.append("<br/>");
			msg.append(
					"Dentro do vosso processo interno de recebimento e tratativa desse tipo de documento, � necess�rio algum n�mero de pedido ou outro tipo de registro?");
			msg.append("<br/>");
			msg.append("<br/>");

			try {
				MaskFormatter mask = new MaskFormatter("###.###.###/####-##");
				mask.setValueContainsLiteralCharacters(false);
				msg.append("Favor confirmar o CNPJ no qual a nota deve ser emitida. "
						+ mask.valueToString(fat.getCliente().getCnpj()) + ", correto?");
				msg.append("<br/>");
			} catch (ParseException ex) {
			}

			msg.append(
					"Aguardamos retorno em at� 3 dias �teis, ap�s este prazo consideramos que a empresa segue o processo padr�o de recebimento de Nota Fiscal e portanto iremos emitir a Nota.");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("Caso tenha alguma d�vida permanecemos a disposi��o.");
			msg.append("<br/>");
			msg.append("<br/>");
			msg.append("Obrigado.");

			msg.append("</body></html>");
			email.setHtmlMsg(msg.toString());

			// set the alternative message
			email.setTextMsg("Your email client does not support HTML messages");
			// send the email
			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Faturamento> getControllers() {
		return em.createQuery("SELECT distinct f FROM Faturamento f", Faturamento.class).getResultList();
	}

	public List<Faturamento> getAllFaturamentosByCriteria(String ano, Long idProduto, Long idFilial, String empresa,
			String tipoDeNegocio, Long etapa) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Faturamento> root = cq.from(Faturamento.class);

		// Constructing list of parameters
		List<Predicate> faturamentos = new ArrayList<Predicate>();
		System.out.println("--------------campanha" + ano);
		System.out.println("--------------produto" + idProduto);
		System.out.println("--------------filial" + idFilial);
		System.out.println("--------------empresa" + empresa);
		System.out.println("--------------tipo de neg�cio: " + tipoDeNegocio);
		System.out.println("--------------etapa " + etapa);

		if (ano != null) {
			System.out.println("Entrou ano");
			faturamentos.add(qb.equal(root.get("campanha"), ano));
		}
		if (idProduto != null) {
			System.out.println("Entrou produto");
			// faturamentos.add(qb.equal(root.get("produto").get("id"), idProduto));
			faturamentos.add(qb.equal(root.get("produto").get("id"), idProduto));
		}
		if (empresa != null) {
			System.out.println("Entrou empresa");
			faturamentos.add(qb.like(root.get("razaoSocialFaturar"), "%" + empresa + "%"));
		}
		if (idFilial != null) {
			System.out.println("Entrou filial");
			faturamentos.add(qb.equal(root.get("producao").get("filial").get("id"), idFilial));
		}
		if (tipoDeNegocio != null) {
			faturamentos.add(qb.equal(root.get("producao").get("tipoDeNegocio"), tipoDeNegocio));
		}
		if (etapa != null) {
			System.out.println("entrou no if etapa" + etapa);
			faturamentos.add(qb.equal(root.get("etapa").get("id"), etapa));
		}

		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(faturamentos.toArray(new Predicate[] {}));

		return em.createQuery(cq).getResultList();

	}

	public List<Faturamento> getFaturamentosByCriteria(String ano, Long idProduto, Long etapa, Long idFilial,
			String empresa, String tipoNegocio, String mesSelecionado) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Faturamento> root = cq.from(Faturamento.class);

		List<Predicate> faturamentos = new ArrayList<Predicate>();

		faturamentos.add(qb.equal(root.get("emitido"), false));
		faturamentos.add(qb.equal(root.get("liberado"), true));

		if (ano != null) {
			faturamentos.add(qb.equal(root.get("campanha"), ano));
		}
		if (idProduto != null) {
			faturamentos.add(qb.equal(root.get("produto").get("id"), idProduto));
		}
		if (empresa != null) {
			faturamentos.add(qb.like(root.get("razaoSocialFaturar"), "%" + empresa + "%"));
		}
		if (idFilial != null) {
			faturamentos.add(qb.equal(root.get("producao").get("filial").get("id"), idFilial));
		}
		if (tipoNegocio != null) {
			faturamentos.add(qb.equal(root.get("producao").get("tipoDeNegocio"), tipoNegocio));
		}
		if (etapa != null) {
			faturamentos.add(qb.equal(root.get("etapa").get("id"), etapa));
		}
		
		if (mesSelecionado != null) {
			System.out.println("mes selecionado texto             " + mesSelecionado);
			faturamentos.add(qb.like(root.get("dataCriacao"), "___" + mesSelecionado + "_____"));
		}
		
//		System.out.println("datas que precisam ter ano (((((((((" + dataInicio + "     " + dataFinal);
//		if (dataInicio != null || dataFinal != null) {
			
//			faturamentos.add(qb.between(root.get("dataCriacao"), localToTimeStamp(LocalDate.of(2021, 12, 12)), localToTimeStamp(LocalDate.of(2021, 12, 14))));
//		}

		// cq.orderBy(qb.desc(root.get("id")));
		// cq.select(root).where(root.get("emitido").in(false)).where(root.get("liberado").in(true));
		// cq.select(root).where(faturamentos.toArray(new Predicate[]
		// {})).where(root.get("emitido").in(false)).where(root.get("liberado").in(true));
		// cq.select(root).where(faturamentos.toArray(new Predicate[] {}));

		// execute query and do something with result
		// return em.createQuery(cq).getResultList();

		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(faturamentos.toArray(new Predicate[] {}));
		return em.createQuery(cq).getResultList();

	}
	
	private static Timestamp localToTimeStamp(LocalDate date) {
	      return Timestamp.from(date.atStartOfDay().toInstant(ZoneOffset.UTC));
	  }

	public List<Faturamento> getFaturamentosNaoEmitidos() {
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.emitido = 0 AND f.liberado = 1", Faturamento.class)
				.getResultList();
	}

	public List<Faturamento> getFaturamentosEmitidos() {
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.emitido = 1 AND f.liberado = 1", Faturamento.class)
				.getResultList();
	}

	public List<Faturamento> getFaturamentoBySubProducao(Long id) {
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.emitido = 1 AND f.subProducao.id = :pSubProdId",
				Faturamento.class).setParameter("pSubProdId", id).getResultList();
	}

	public List<Faturamento> getFaturamentoByCliente(Long id) {
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.cliente.id = :pClienteId AND f.emitido = 0",
				Faturamento.class).setParameter("pClienteId", id).getResultList();
	}

	public List<Faturamento> getFaturamentoByProdAndCampaign(Long idCliente, String campanha) {
		try {
			return em
					.createQuery("SELECT f FROM Faturamento f WHERE f.cliente.id = :idCliente "
							+ "AND f.campanha = :campanha")
					.setParameter("idCliente", idCliente).setParameter("campanha", campanha).getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	public Faturamento geraFaturamentoConsolidado(Producao producao, BigDecimal beneficioConsolidado, Float percent,
			Long idEtapaFaturar) {

		try {

			BigDecimal teto = BigDecimal.valueOf(0);
			String obs = "Honor�rios: ";
			BigDecimal valorTotal = BigDecimal.valueOf(0);

			System.out.println("Produ��o recebida para faturamento com o ID: " + producao.getId());

			Faturamento faturamento = new Faturamento();
			Contrato contrato = producao.getContrato();

			List<ContatoCliente> contatos = ContatoClienteService.getContatosCliente(contrato.getCliente().getId());
			int contatoNF = 0;
			for (ContatoCliente contatoCliente : contatos) {
				if (contatoCliente.isPessoaNfBoleto()) {
					contatoNF++;
				}
			}

			System.out.println("Contatos com recebimento de nota " + contatoNF);

			List<Honorario> honorarios = contrato.getHonorarios();
			for (Honorario honorario : honorarios) {
				System.out.println(honorario.getNome());

				if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
					System.out.println("Bateu com o nome Percentual Escalonado");

					if (contatoNF > 0) {

						PercentualEscalonado pe = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());

						if (Objects.nonNull(pe)) {

							teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);
							BigDecimal temp = beneficioConsolidado;

							for (FaixasEscalonado faixa : faixas) {

								System.out.println("Temp atual " + temp);

								// Define vari�veis
								BigDecimal inicial = faixa.getValorInicial();
								BigDecimal finalF = faixa.getValorFinal();
								BigDecimal gap1 = finalF.subtract(inicial);
								// BigDecimal gap2 = temp.subtract(inicial);
								Float percentual = faixa.getValorPercentual();

								System.out.println("Faixa: " + inicial + " ... " + finalF);
								System.out.println("Diferen�a: " + gap1);

								// Se � maior que o final, calcula direto com o gap1
								if (temp.compareTo(BigDecimal.ZERO) != 0) {
									if (temp.compareTo(gap1) >= 0) {

										System.out.println("Passa direto da faixa, calcula com o f...");

										valorCalc = valorCalc.add(gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

										System.out.println("Saldo para soma :" + gap1
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));
										System.out.println("Total at� o momento: " + valorCalc);

										// Se est� entre a faixa, calcula com o temp direto na porcentagem.
									} else if (temp.compareTo(gap1) < 0) {

										valorCalc = valorCalc.add(temp
												.multiply(BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

										BigDecimal zero = BigDecimal.ZERO;
										temp = zero;
									}
								}
							}
							if (pe.getAcimaDe() != null) {
								if (beneficioConsolidado.compareTo(pe.getAcimaDe()) > 0) {
									if (temp.compareTo(BigDecimal.ZERO) > 0) {
										System.out.println("Ainda resta Temp: " + temp);
										System.out.println("Entrando no Acima de e aplicando " + pe.getPercentAcimaDe()
												+ " ao temp");
										BigDecimal acima = temp
												.multiply(new BigDecimal(Float.toString(pe.getPercentAcimaDe()))
														.divide(new BigDecimal(100)));
										System.out.println("Resultado do acima: " + acima);
										valorCalc = valorCalc.add(acima);
										System.out.println(
												"A soma do acima de com o total at� o momento deu: " + valorCalc);
									}
								}
							}

							if (percent != 0) {

								if (pe.getLimitacao() != null) {

									if (pe.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
										System.out.println("Comparando se o resultado de " + valorCalc
												+ " � maior que a limita��o de " + pe.getLimitacao());
										if (valorCalc.compareTo(pe.getLimitacao()) >= 0) {
											valorCalc = pe.getLimitacao();
										}
									}
								}

								valorCalc = valorCalc.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

								faturamento.setCliente(contrato.getCliente());
								faturamento.setContrato(contrato);
								faturamento.setProducao(producao);
								faturamento.setEtapa(etapaTrabalhoService.findById(idEtapaFaturar));
								faturamento.setEtapas(null);
								faturamento.setPercentualE(pe);
								faturamento.setPercetEtapa(percent);
								faturamento.setBalancete(null);
								faturamento.setCampanha(producao.getAno());
								faturamento.setProduto(producao.getProduto());

								valorTotal = valorTotal.add(valorCalc);
								obs += " Percentual Escalonado,";

								System.out.println(
										" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM PERCENTUAL ESCALONADO -- -- -- ");
								System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
								System.out.println(" -- Valor: " + valorCalc);
							}
						}
					} else {
						System.out.println("Fat n gerado pois n�o possui contato");
					}
				}

				if (contatoNF > 0) {

					if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
						ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
						if (Objects.nonNull(fixo)) {

							BigDecimal valorFixo = fixo.getValor();

							EtapasPagamento pagamento = producao.getContrato().getEtapasPagamento();

							// Calculo de porcentagem com BigDecimal
							BigDecimal valorCalc = valorFixo
									.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

							faturamento.setCliente(contrato.getCliente());
							faturamento.setContrato(contrato);
							faturamento.setProducao(producao);
							faturamento.setEtapa(etapaTrabalhoService.findById(idEtapaFaturar));
							faturamento.setEtapas(null);
							faturamento.setValorF(fixo);
							faturamento.setPercetEtapa(percent);
							faturamento.setBalancete(null);

							valorTotal = valorTotal.add(valorCalc);
							obs += " Valor Fixo,";

							System.out.println(
									" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM VALOR FIXO -- -- -- ");
							System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
							System.out.println(" -- Valor: " + valorCalc);
						}
					}
				} else {
					System.out.println("Fat n gerado pois n�o possui contato");
				}

				if (contatoNF > 0) {

					if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
						PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(contrato.getId());

						if (Objects.nonNull(fixo)) {

							teto = fixo.getLimitacao();

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							BigDecimal balancete = beneficioConsolidado;

							valorCalc = balancete.multiply(
									BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

							EtapasPagamento pagamento = producao.getContrato().getEtapasPagamento();

							System.out.println("Valor final no momento (antes da % etapa ): " + valorCalc);

							if (percent != 0) {
								valorCalc = valorCalc.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal(100)));

								if (fixo.getLimitacao() != null) {

									if (fixo.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
										if (valorCalc.compareTo(fixo.getLimitacao()) >= 0) {
											valorCalc = fixo.getLimitacao();
										}
									}
								}

								faturamento.setCliente(contrato.getCliente());
								faturamento.setContrato(contrato);
								faturamento.setProducao(producao);
								faturamento.setEtapa(etapaTrabalhoService.findById(idEtapaFaturar));
								faturamento.setEtapas(null);
								faturamento.setPercentualF(fixo);
								faturamento.setPercetEtapa(percent);
								faturamento.setBalancete(null);

								valorTotal = valorTotal.add(valorCalc);
								obs += " Percentual Fixo,";

								System.out.println(
										" -- -- -- FATURAMENTO CRIADO POR FINALIZA��O DE TAREFA EM PERCENTUAL FIXO -- -- -- ");
								System.out.println(" -- Cliente: " + faturamento.getContrato().getCliente());
								System.out.println(" -- Valor: " + valorCalc);

							}
						}
					}
				} else {
					System.out.println("Fat n gerado pois n�o possui contato");
				}

			}

			faturamento.setValorEtapa(valorTotal);
			obs = obs.substring(0, obs.length() - 1);
			faturamento.setObservacao(obs);

			System.out.println(" -- -- -- FATURAMENTO CONSOLIDADO RETORNADO  -- -- -- ");
			System.out.println(" -- Valor: " + faturamento.getValorEtapa());

			return faturamento;

		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}

	/* teste */
	public Faturamento geraConsolidacaoByFat(Producao producao){

		try {

			// BigDecimal teto = BigDecimal.valueOf(0);
			String obs = "Honor�rios: ";
			// BigDecimal valorTotal = BigDecimal.valueOf(0);

			System.out
					.println("geraConsolidacaoByFat-Produ��o recebida para faturamento com o ID: " + producao.getId());

			Faturamento faturamento = new Faturamento();
			Contrato contrato = producao.getContrato();

			List<ContatoCliente> contatos = ContatoClienteService.getContatosCliente(contrato.getCliente().getId());
			int contatoNF = 0;
			for (ContatoCliente contatoCliente : contatos) {
				if (contatoCliente.isPessoaNfBoleto()) {
					contatoNF++;
				}
			}

			System.out.println("geraConsolidacaoByFat-Contatos com recebimento de nota " + contatoNF);

			faturamento.setCliente(contrato.getCliente());
			faturamento.setContrato(contrato);
			faturamento.setProducao(producao);
			//faturamento.setEtapa(etapaTrabalhoService.findById(etapaFaturar));

			obs = obs.substring(0, obs.length() - 1);
			faturamento.setObservacao(obs);

			return faturamento;

		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}
	}

public List<Faturamento> getFaturamentoByBalancete(Long idBalancete) {
	return em.createQuery("SELECT f FROM Faturamento f WHERE f.balancete.id = :pId ",
			Faturamento.class).setParameter("pId", idBalancete).getResultList();
}

	@Transactional
	public void deleteByContrato(Long idContrato){
		em.createQuery("DELETE FROM Faturamento f WHERE f.contrato.id = :pId").setParameter("pId", idContrato).executeUpdate();

	}
	
	public List<Faturamento> pegaDatasFormatadas() {		
		return em.createQuery("SELECT f FROM Faturamento f WHERE f.dataCriacao IS NOT NULL",
				Faturamento.class).getResultList();
	}
	
	@Transactional
	public void salvaDatasFormatadas(Long idFaturamento, String data) {
		
		em.createQuery("UPDATE Faturamento f set f.dataCriacao = '"+data+"' where f.id = :pId")
		.setParameter("pId", idFaturamento).executeUpdate();
		
		
	}
	
}
