package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ObrigacoesPeriodicas;
import br.com.finiciativas.fseweb.models.produto.ItemValores;

public interface ObrigacoesPeriodicasRepository {

	List<ObrigacoesPeriodicas> getAll();
	
	void create (ObrigacoesPeriodicas obrigacoes);
	
	void delete (ObrigacoesPeriodicas obrigacoes);
	
	ObrigacoesPeriodicas update (ObrigacoesPeriodicas obrigacoes);
	
	ObrigacoesPeriodicas findById(Long id);
	
	ObrigacoesPeriodicas getLastItemAdded();
	
	void addObrigacoesByFrequencia(ItemValores itemvalor);
	
	List<ObrigacoesPeriodicas> getObrigacoesByProducao(Long id);
	
	ItemValores getFNDCTbyProducaoId(Long id);
	
	ItemValores getObrigacoesPeriodicasByProducao(Long id);
	
}
