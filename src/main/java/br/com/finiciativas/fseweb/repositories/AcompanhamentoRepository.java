package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Acompanhamento;

public interface AcompanhamentoRepository {

	void create(Acompanhamento acompanhamento);

	void delete(Acompanhamento acompanhamento);
		
	Acompanhamento update(Acompanhamento acompanhamento);
	
	List<Acompanhamento> getAll();
	
	Acompanhamento findById(Long id);
	
	List<Acompanhamento> getAcompanhamentoByProducao(Long id);
	
}
