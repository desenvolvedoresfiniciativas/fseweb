package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.FITechReport;

@Repository
public class FITechReportRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(FITechReport FITechReport) {
		FITechReport.setDataReport(java.time.LocalDate.now().toString());
		this.em.persist(FITechReport);
	}

	public void delete(FITechReport FITechReport) {
		this.em.remove(FITechReport);
	}

	@Transactional
	public FITechReport update(FITechReport FITechReport) {
		return this.em.merge(FITechReport);
	}
	
	public List<FITechReport> getAll() {
		return this.em.createQuery("SELECT distinct o FROM FITechReport o ", FITechReport.class)
				.getResultList();
	}
	
	public FITechReport findById(Long id) {
		return this.em.find(FITechReport.class, id);
	}
	
}
