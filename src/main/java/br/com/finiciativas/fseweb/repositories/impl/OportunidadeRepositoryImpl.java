package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.CRM.Oportunidade;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Documentacao;

@Repository
public class OportunidadeRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	public void create(Oportunidade Oportunidade) {
		em.persist(Oportunidade);
	}
	
	public void delete(Documentacao balancete) {
		this.em.remove(balancete);
	}

	public List<Oportunidade> getAll() {
		return this.em.createQuery("FROM Oportunidade i", Oportunidade.class)
				.getResultList();
	}

	@Transactional
	public Oportunidade update(Oportunidade Oportunidade) {
		return this.em.merge(Oportunidade);
	}

	public Oportunidade findById(Long id) {
		return this.em.find(Oportunidade.class, id);
	}

	public void remove(Long id) {
		this.em.remove(id);
	}	
	
}
