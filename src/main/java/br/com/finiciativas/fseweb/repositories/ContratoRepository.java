package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;

public interface ContratoRepository {
	
	Long create(Contrato contrato);
	
	Contrato update(Contrato contrato);
	
	Contrato findById(Long id);
	
	boolean remove(Long id);
	
	List<Contrato> getAll();
	
	List<Contrato> getContratoByCliente(Long id);

	Contrato getLastAdded();
	
	List<Contrato> getAllByComercial(Long id);

	void updateHonorarios(Contrato contrato);
	
	List<Contrato> getContratoByCampanha(String dataInicio);

	List<Contrato> getProdutosPrincipais(Long id);
	
	List<Contrato> getListaProdutosAlvoByCliente(Long id);
	
	boolean getVerificaPrincipal(Long contratoId);
	
	List<Contrato> getRelatorioCombo(Long idProduto, Long comercialRespEntrada, Long comercialRespAtual,String empresa);
	
	
	List<String> getListaNomeCombo(Long clienteId) ;

	List<Contrato> getContratosByProdutoAndCliente(Long idProduto, String ano, Long idCliente);
	
	
	
	

	
	
}
