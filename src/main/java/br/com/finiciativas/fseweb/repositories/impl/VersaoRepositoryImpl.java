package br.com.finiciativas.fseweb.repositories.impl;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import br.com.finiciativas.fseweb.models.Versao;

@Repository
public class VersaoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	public Versao getVersaoAtual() {
		return this.em.createQuery("SELECT v FROM Versao v)", Versao.class)
				.getSingleResult();
	}
	
}
