package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Questionamentos;

@Repository
public class QuestionamentosRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Questionamentos Questionamentos) {
		this.em.persist(Questionamentos);
	}

	public void delete(Questionamentos Questionamentos) {
		this.em.remove(Questionamentos);
	}

	@Transactional
	public Questionamentos update(Questionamentos Questionamentos) {
		return this.em.merge(Questionamentos);
	}
	
	public List<Questionamentos> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Questionamentos o ", Questionamentos.class)
				.getResultList();
	}

	public Questionamentos findById(Long id) {
		return this.em.find(Questionamentos.class, id);
	}

	public List<Questionamentos> getQuestionamentosByAcompanhamento(Long id) {
		return this.em.createQuery("SELECT distinct o FROM Questionamentos o WHERE o.producao.id = :pId ", Questionamentos.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
