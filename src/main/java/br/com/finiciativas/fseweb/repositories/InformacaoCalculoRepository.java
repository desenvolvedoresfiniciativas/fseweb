package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.InformacaoCalculo;

public interface InformacaoCalculoRepository {

	void create(InformacaoCalculo informacao);

	void delete(InformacaoCalculo informacao);
		
	InformacaoCalculo update(InformacaoCalculo informacao);
	
	List<InformacaoCalculo> getAll();
	
	InformacaoCalculo findById(Long id);
	
	List<InformacaoCalculo> getInformacaoCalculoByAcompanhamento(Long id);
	
}
