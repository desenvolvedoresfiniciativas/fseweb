package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Departamento;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.PeriodoPagamento;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Role;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;

@Repository
public class PeriodoPagamentoRepositoryImpl {
	
	@Autowired
	private EntityManager em;

	public void create(PeriodoPagamento periodoPagamento) {
		em.persist(periodoPagamento);
		em.flush();
	}

	public List<PeriodoPagamento> getAll() {
		return em.createQuery("SELECT p FROM PeriodoPagamento p", PeriodoPagamento.class).getResultList();
	}

	public List<PeriodoPagamento> getAllNaoPagos() {
		return em.createQuery("SELECT p FROM PeriodoPagamento p WHERE p.pago = 0", PeriodoPagamento.class).getResultList();
	}

	public List<PeriodoPagamento> getAllPeriodosNaoPagosByDepartamento(String departamento) {
		System.out.println("O OBJETO RECEBIDO NA QUERY �: "  + departamento);
		return em.createQuery("SELECT p FROM PeriodoPagamento p WHERE p.pago = 0 AND p.departamento = :departamento ", PeriodoPagamento.class).setParameter("departamento", departamento).getResultList();	
	}

}
