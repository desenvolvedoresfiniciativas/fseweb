package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.consultor.Consultor;

public interface EquipeRepository {
	
	List<Equipe> getAll();
	
	void create(Equipe equipe);
	
	Equipe update(Equipe equipe);
	
	Equipe findById(Long id);
	
	void remove(Long id);
	
	List<Consultor> getConsultores(Long idEquipe);
	
	Equipe getConsultorByEquipe(Long id);
	
	Equipe findEquipeByConsultor(Long idConsultor);

	List<Consultor> getAllConsultores();
	
}
