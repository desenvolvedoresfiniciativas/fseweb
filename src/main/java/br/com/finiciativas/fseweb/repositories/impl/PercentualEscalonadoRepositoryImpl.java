package br.com.finiciativas.fseweb.repositories.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;

@Repository
public class PercentualEscalonadoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	public PercentualEscalonado create(PercentualEscalonado pe) {
		em.persist(pe);
		return pe;
	}

	@Transactional
	public PercentualEscalonado update(PercentualEscalonado PercentualEscalonado) {
		if(PercentualEscalonado.getLimitacao()==null){
			PercentualEscalonado.setLimitacao(BigDecimal.valueOf(0));
		}
		return this.em.merge(PercentualEscalonado);
	}
	

	public List<PercentualEscalonado> getAll() {
		return this.em.createQuery("FROM PercentualEscalonado", PercentualEscalonado.class).getResultList();
	}
	
	public PercentualEscalonado findById(Long id) {
		return this.em.find(PercentualEscalonado.class, id);
	}
	
}
