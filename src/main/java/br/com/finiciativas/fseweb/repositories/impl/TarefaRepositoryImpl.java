package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.TarefaRepository;
import br.com.finiciativas.fseweb.services.impl.EtapaTrabalhoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProdutoServiceImpl;

@Repository
public class TarefaRepositoryImpl implements TarefaRepository {

	@Autowired
	private EntityManager em;

	@Autowired
	private ProdutoServiceImpl produtoService;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ItemValoresServiceImpl ItemValoresService;

	@Autowired
	private ItemTarefaServiceImpl ItemTarefaService;

	@Autowired
	private EtapaTrabalhoServiceImpl etapaTrabalhoService;

	@Transactional
	@Override
	public void create(Tarefa tarefa) {
		em.persist(tarefa);
	}

	@Override
	public List<Tarefa> getAll() {
		return em.createQuery("SELECT distinct t FROM Tarefa t", Tarefa.class).getResultList();
	}

	@Override
	public Tarefa findById(Long id) {
		return em.createQuery("SELECT t FROM Tarefa t WHERE t.id = :pId", Tarefa.class).setParameter("pId", id)
				.getSingleResult();
	}

	@Transactional
	@Override
	public Tarefa updateTarefaItens(Tarefa tarefa, Long produtoId, Long etapaId) {
		Tarefa tarefaAtt = em.find(Tarefa.class, tarefa.getId());

		if (produtoId != null) {
			System.out.println("Produto NOT NULL");
			Produto produto = produtoService.findById(produtoId);
			EtapaTrabalho etapa = etapaTrabalhoService.findById(etapaId);

			System.out.println("Nome da etapa identificada: " + etapa.getNome());
			System.out.println("A etapa � subprodu��o? " + etapa.isSubProducao());

			List<ItemTarefa> itensAtuais = tarefaAtt.getItensTarefa();
			List<ItemTarefa> itensNovosCrud = tarefa.getItensTarefa();
			List<ItemTarefa> itensNovos = new ArrayList<ItemTarefa>();

			for (ItemTarefa itemTarefa : itensNovosCrud) {
				itensNovos.add(ItemTarefaService.findById(itemTarefa.getId()));
			}

			itensNovos.removeAll(itensAtuais);

			System.out.println(itensNovos.size());

			for (ItemTarefa itemTarefa : itensNovos) {
				System.out.println(itemTarefa.getNome());
			}

			List<Producao> producoes = producaoService.getAllProducaoBynome(produto.getNome());

			for (Producao producao : producoes) {
				for (ItemTarefa item : itensNovos) {

					ItemValores itemValor = new ItemValores();
					itemValor.setProducao(producao);
					itemValor.setTarefa(tarefa);
					itemValor.setItem(item);
					itemValor.setValor("");
					itemValor.setDataPrevisao("");
					itemValor.setDataRealizado("");

					ItemValoresService.create(itemValor);

				}

				if (etapa.isSubProducao()) {
					List<SubProducao> subProds = producaoService.getSubproducoesOfProducao(producao.getId());
					for (SubProducao subProducao : subProds) {
						for (ItemTarefa item : itensNovos) {

							ItemValores itemValor = new ItemValores();
							itemValor.setProducao(subProducao.getProducao());
							itemValor.setSubProducao(true);
							itemValor.setSubProducaoIndex(subProducao);
							itemValor.setTarefa(tarefa);
							itemValor.setItem(item);
							itemValor.setValor("");
							itemValor.setDataPrevisao("");
							itemValor.setDataRealizado("");

							ItemValoresService.create(itemValor);
						}
					}
				}
			}

			tarefaAtt.setNome(tarefa.getNome());
			tarefaAtt.setDescricao(tarefa.getDescricao());
			tarefaAtt.setItensTarefa(tarefa.getItensTarefa());
		} else {
			System.out.println("PRODUTO NULL");
		}

		return this.em.merge(tarefaAtt);
	}

	@Transactional
	public Tarefa update(Tarefa tarefa) {
		return this.em.merge(tarefa);
	}

	public Tarefa getLastItemAdded() {

		String sQuery = "SELECT t " + "FROM Tarefa t " + "WHERE t.id=(SELECT max(id) FROM Tarefa)";

		return em.createQuery(sQuery, Tarefa.class).getSingleResult();
	}

	public List<Tarefa> getTarefasByEtapa(EtapaTrabalho etapa) {

		String sQuery = "SELECT t " + "FROM Tarefa t " + "JOIN t.etapas e " + "WHERE e.id = :pId";

		return em.createQuery(sQuery, Tarefa.class).setParameter("pId", etapa.getId()).getResultList();
	}

	public boolean checkItensTarefaSize(Tarefa tarefa) {

		Tarefa tarefaComparator = em.find(Tarefa.class, tarefa.getId());
		List<ItemTarefa> itensComparator = tarefaComparator.getItensTarefa();
		List<ItemTarefa> itensTarefa = tarefa.getItensTarefa();

		if (itensComparator.size() == itensTarefa.size()) {
			return true;
		} else {
			return false;
		}

	}

	public List<ItemTarefa> getDifferentItemTarefas(List<ItemTarefa> itensAtuais, List<ItemTarefa> itensNovos) {

		List<ItemTarefa> itensDiferentes = new ArrayList();
		int index = 0;

		System.out.println(itensAtuais.size());

		for (ItemTarefa itemTarefaAtual : itensAtuais) {
			for (ItemTarefa itemTarefaNovo : itensNovos) {
				if (itemTarefaNovo.getId() != itemTarefaAtual.getId()) {
					itensDiferentes.add(itemTarefaNovo);
				}
			}
			itensAtuais.remove(index);
			index++;
		}

		return itensDiferentes;
	}

}
