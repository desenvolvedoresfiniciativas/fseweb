package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.IeM.Marketing;

@Repository
public class MarketingRepositoryImpl {
	
	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Marketing reco) {
		
		reco.setOportunidade("Marketing");
		
		this.em.persist(reco);
	}

	public void delete(Marketing reco) {
		this.em.remove(reco);
	}

	@Transactional
	public Marketing update(Marketing reco) {
		return this.em.merge(reco);
	}

	public List<Marketing> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Marketing o ", Marketing.class)
				.getResultList();
	}

	public Marketing findById(Long id) {
		return this.em.find(Marketing.class, id);
	}

}
