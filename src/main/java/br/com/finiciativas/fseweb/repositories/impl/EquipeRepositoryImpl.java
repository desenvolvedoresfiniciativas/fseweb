package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.repositories.EquipeRepository;

@Repository
public class EquipeRepositoryImpl implements EquipeRepository {
	
	@Autowired
	private EntityManager em;

	@Override
	public List<Equipe> getAll() {
		List<Equipe> equipe = em
				.createQuery("SELECT distinct e FROM Equipe e INNER JOIN FETCH e.consultores con ORDER BY e.nome", Equipe.class)
				.getResultList();
		
		return equipe;
	}
	
	@Transactional
	@Override
	public void create(Equipe equipe) {
		Consultor lider = em.find(Consultor.class, equipe.getLider().getId());
		Consultor coordenador = em.find(Consultor.class, equipe.getCoordenador().getId());
		Consultor gerente = em.find(Consultor.class, equipe.getGerente().getId());
		equipe.setLider(lider);
		equipe.setCoordenador(coordenador);
		equipe.setGerente(gerente);
		this.em.persist(equipe);
	}

	@Transactional
	@Override
	public Equipe update(Equipe equipe) {

			Equipe equipeAtt = em.find(Equipe.class, equipe.getId());
			
			equipeAtt.setNome(equipe.getNome());
			equipeAtt.setLider(equipe.getLider());
			equipeAtt.setCoordenador(equipe.getCoordenador());
			equipeAtt.setGerente(equipe.getGerente());
			equipeAtt.setConsultores(equipe.getConsultores());
			equipeAtt.setFilial(equipe.getFilial());
			
			return this.em.merge(equipeAtt);
			
	}
	
	@Override
	public Equipe findById(Long id) {
		return this.em.find(Equipe.class,id);
	}
	
	@Transactional
	@Override
	public void remove(Long id) {
		Equipe equipeRemove = this.em.find(Equipe.class, id);
		this.em.remove(equipeRemove);
	}
	
	@Override
	public List<Consultor> getConsultores(Long idEquipe) {

		if (idEquipe != null) {
			return em.createQuery("SELECT consultores " 
					+ "FROM Equipe e "
					+ "LEFT JOIN e.consultores consultores WHERE e.id = :pId ORDER BY consultores.nome", Consultor.class)
					.setParameter("pId", idEquipe)
					.getResultList();
		} else {
			return em.createQuery("SELECT consultores " 
					+ "FROM Equipe e "
					+ "LEFT JOIN e.consultores consultores WHERE e.id = :pId ORDER BY consultores.nome", Consultor.class)
					.setParameter("pId", idEquipe)
					.getResultList();
		}
		
	}
	
	@Override
	public List<Consultor> getAllConsultores() {
		return em.createQuery("SELECT consultores " 
				+ "FROM Equipe e "
				+ "LEFT JOIN e.consultores consultores WHERE e.id = :pId ORDER BY consultores.nome", Consultor.class)
				.setParameter("pId", 2)
				.getResultList();
		}
	
	@Override
	public Equipe getConsultorByEquipe(Long id) {
		try {
			System.out.println("Entrou no try teste -------- e o id �: " + id);
			String sQuery = "SELECT e FROM Equipe e INNER JOIN e.consultores c WHERE c.id = :eId OR e.lider.id = :eId";

			return em.createQuery(sQuery, Equipe.class)
			.setParameter("eId", id)
			.getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public Equipe findByNome(String nome){
		try {
		return em.createQuery("SELECT e FROM Equipe e WHERE e.nome = :pNome", Equipe.class)
				.setParameter("pNome", nome)
				.getSingleResult();
		} catch (Exception e) {
			System.out.println("Busca de Equipe por Nome n�o encontrou nada");
			return null;
		}
	}
	
	public Equipe findEquipeByConsultor(Long idConsultor) {
		try {
			return em.createQuery("SELECT e FROM Equipe e INNER JOIN e.consultores c WHERE "
					+ "c.id = :pId", Equipe.class)
					.setParameter("pId", idConsultor)
					.getSingleResult();
			} catch (Exception e) {
				System.out.println("Busca de Equipe por Consultor n�o encontrou nada");
				return null;
			}
	}
	
	public Equipe findEquipeByLider(Long idConsultor) {
		try {
			return em.createQuery("SELECT e FROM Equipe e INNER JOIN e.consultores c WHERE "
					+ "e.lider.id = :pId", Equipe.class)
					.setParameter("pId", idConsultor)
					.getSingleResult();
			} catch (Exception e) {
				System.out.println("Busca de Equipe por Consultor n�o encontrou nada");
				return null;
			}
	}

	public List<Equipe> getAllEquipesByCoordenador(Long idCoordenador) {
		try {
			return em.createQuery("SELECT e FROM Equipe e WHERE e.coordenador.id = :pId", Equipe.class)
					.setParameter("pId", idCoordenador)
					.getResultList();
			} catch (Exception e) {
				System.out.println("Busca de Equipe por Coordenador n�o encontrou nada");
				return null;
			}
	}
	
	public List<Equipe> getAllEquipesByGerenteGeral(Long idGerenteGeral) {
		try {
			return em.createQuery("SELECT e FROM Equipe e WHERE e.gerente.id = :pId AND e.ativa = 1", Equipe.class)
					.setParameter("pId", idGerenteGeral)
					.getResultList();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Busca de Equipe por Gerente geral n�o encontrou nada");
				return null;
			}
	}
	
	public Equipe getEquipeByLider(Long idLider){
		try {
			System.out.println("ID L�DER "+idLider);
			return em.createQuery("SELECT e FROM Equipe e WHERE e.lider.id = :pId", Equipe.class)
					.setParameter("pId", idLider)
					.getSingleResult();
			} catch (Exception e) {
				System.out.println("Busca de Equipe por Lider n�o encontrou nada");
				return null;
			}
	}

	public List<Equipe> getAllEquipesByFilial(Long id) {
		try {
			
			return em.createQuery("SELECT e FROM Equipe e WHERE e.filial.id = :pId", Equipe.class)
					.setParameter("pId", id)
					.getResultList();
			} catch (Exception e) {
				List<Equipe> equipes = new ArrayList();
				return equipes;
			}
	}
}
