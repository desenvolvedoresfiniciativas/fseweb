package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Documentacao;

public interface DocumentacaoRepository {

	void create(Documentacao documentacao);

	void delete(Documentacao documentacao);
		
	Documentacao update(Documentacao documentacao);
	
	List<Documentacao> getAll();
	
	Documentacao findById(Long id);
	
	List<Documentacao> getDocumentacaoByAcompanhamento(Long id);
	
}
