package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.ReceptoresEmail;

@Repository
public class ReceptoresEmailRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(ReceptoresEmail ReceptoresEmail) {
		this.em.persist(ReceptoresEmail);
	}

	public void delete(ReceptoresEmail ReceptoresEmail) {
		this.em.remove(ReceptoresEmail);
	}

	@Transactional
	public ReceptoresEmail update(ReceptoresEmail ReceptoresEmail) {
		return this.em.merge(ReceptoresEmail);
	}
	
	public List<ReceptoresEmail> getAll() {
		return this.em.createQuery("SELECT distinct o FROM ReceptoresEmail o ", ReceptoresEmail.class)
				.getResultList();
	}

	public ReceptoresEmail findById(Long id) {
		return this.em.find(ReceptoresEmail.class, id);
	}
	
	public ReceptoresEmail getByTipo(String tipo) {
		return this.em.createQuery("SELECT distinct o FROM ReceptoresEmail o WHERE o.tipoRegistro = :pId", ReceptoresEmail.class)
				.setParameter("pId", tipo)
				.getSingleResult();
	}
	
}
