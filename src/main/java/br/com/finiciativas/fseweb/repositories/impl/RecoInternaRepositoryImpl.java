package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.IeM.RecoInterna;

@Repository
public class RecoInternaRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(RecoInterna reco) {
		
		reco.setOportunidade("Reco Interna");
		
		this.em.persist(reco);
	}

	public void delete(RecoInterna reco) {
		this.em.remove(reco);
	}

	@Transactional
	public RecoInterna update(RecoInterna reco) {
		return this.em.merge(reco);
	}

	public List<RecoInterna> getAll() {
		return this.em.createQuery("SELECT distinct o FROM RecoInterna o ", RecoInterna.class)
				.getResultList();
	}

	public RecoInterna findById(Long id) {
		return this.em.find(RecoInterna.class, id);
	}
	
}
