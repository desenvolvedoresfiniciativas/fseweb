package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Tarefa;

public interface EtapaTrablhoRepository {
	
	void create(EtapaTrabalho etapaTrabalho);
	
	List<EtapaTrabalho> getAll();

	Tarefa getTarefas(Long idEtapaTrabalho);

	EtapaTrabalho findById(Long id);

	EtapaTrabalho update(EtapaTrabalho etapaTrabalho);
	
	void bindEtapaToTarefas(EtapaTrabalho etapa);
	
	public EtapaTrabalho getLastAdded();
	
}
