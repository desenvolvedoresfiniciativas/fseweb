package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.repositories.ContatoClienteRepository;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;

@Repository
public class ContatoClienteRepositoryImpl implements ContatoClienteRepository {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ClienteServiceImpl clienteService;

	@Override
	@SuppressWarnings("unchecked")
	public List<ContatoCliente> getContatosCliente(Long id) {
		return em.createQuery("SELECT c FROM ContatoCliente c WHERE c.cliente.id = :pCliente")
				.setParameter("pCliente", id)
				.getResultList();
	}

	@Override
	public void create(ContatoCliente contatoEmpresa, Long id) {
		
		System.out.println("Entrou no m�todo");
		
		contatoEmpresa.setAtivo(true);
		contatoEmpresa.setCliente(clienteService.findById(id));
		
		em.persist(contatoEmpresa);
		
//		Cliente empresa = em.find(Cliente.class, id);
//		
//		List<ContatoCliente> contatos = empresa.getContatos();
//		
//		contatos.add(contatoEmpresa);
//		
//		clienteService.update(empresa);

	}

	@Override
	@Transactional
	public void update(ContatoCliente contatoEmpresa) {
		
		ContatoCliente contatoEmpresaAtt = new ContatoCliente(contatoEmpresa);
		contatoEmpresaAtt.setCliente(contatoEmpresa.getCliente());
		contatoEmpresaAtt.setAtivo(true);
		
		em.merge(contatoEmpresaAtt);
		
	}

	@Override
	public void remove(ContatoCliente contatoEmpresa) {

		ContatoCliente ce = em.find(ContatoCliente.class, contatoEmpresa.getId());

		ce.setAtivo(false);

		em.merge(ce);

	}

	@Override
	public ContatoCliente findById(Long id) {
		return em.find(ContatoCliente.class, id);
	}
	
	public List<ContatoCliente> getAllById(Long id){
	return em.createQuery("SELECT d FROM ContatoCliente d WHERE d.cliente.id = :pId AND d.ativo = 1 ORDER BY d.nome", ContatoCliente.class)
		.setParameter("pId", id)
		.getResultList();
	}

	@Override
	public Long getIdClienteOfContact(Long id) {
		TypedQuery<Long> query = em.createQuery("SELECT cli.id FROM Cliente cli "
				+ "INNER JOIN cli.contatos ce WHERE ce.id = :id", Long.class)
				.setParameter("id", id);
		return query.getSingleResult();
	}

	public void delete(ContatoCliente cont) {
		// TODO Auto-generated method stub
		
	}
	
	List<ContatoCliente> getContatoCliente(Long idCliente){
		return em.createQuery("SELECT c FROM ContatoCliente c WHERE c.cliente.id = :pId AND  c.pessoaNfBoleto= 1 "  , ContatoCliente.class)
			.setParameter("pId", idCliente)
			.getResultList();
		}
	
	
	
		
}
