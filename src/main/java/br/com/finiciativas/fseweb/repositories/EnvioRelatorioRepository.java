package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.EnvioRelatorio;

public interface EnvioRelatorioRepository {

	void create(EnvioRelatorio envio);

	void delete(EnvioRelatorio envio);
		
	EnvioRelatorio update(EnvioRelatorio envio);
	
	List<EnvioRelatorio> getAll();
	
	EnvioRelatorio findById(Long id);
	
	List<EnvioRelatorio> getEnvioRelatorioByAcompanhamento(Long id);
	
}
