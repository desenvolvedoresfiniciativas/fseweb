package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Tarefa;

public interface ItemTarefaRepository {
	
	void create(ItemTarefa itemTarefa);
	
	List<ItemTarefa> getAll();
	
	List<ItemTarefa> getLista();
	
	ItemTarefa update(ItemTarefa itemTarefa);
	
	List<ItemTarefa> getItensByProducao(Producao producao);
	
	List<ItemTarefa> getItensByTarefa(Tarefa tarefa);
	
	ItemTarefa findById(Long id);
}
