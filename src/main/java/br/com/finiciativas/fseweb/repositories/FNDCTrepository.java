package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.FNDCT;

public interface FNDCTrepository {

	List<FNDCT> getAll();

	void create(FNDCT fndct);

	void delete(FNDCT fndct);

	FNDCT update(FNDCT fndct);

	FNDCT findById(Long id);

	FNDCT getFNDCTByProducao(Long id);
	
}
