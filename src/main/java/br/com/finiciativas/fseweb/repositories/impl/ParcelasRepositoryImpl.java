package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.Parcela;

@Repository
public class ParcelasRepositoryImpl {
	
	@Autowired
	private EntityManager em;
	
	@Transactional
	public Parcela create(Parcela parcela) {
		
		this.em.persist(parcela);
		em.flush();
		
		return parcela;
	}
	
	public void delete(Parcela parcela) {
		this.em.remove(parcela);
	}
	
	@Transactional
	public Parcela update(Parcela parcela) {
		return this.em.merge(parcela);
	}
	
	public Parcela findById(Long id) {
		return this.em.find(Parcela.class, id);
	}
	
	
	public List<Parcela> getAll(){
		return this.em.createQuery("SELECT distinct o FROM NotaFiscal o", Parcela.class)
				.getResultList();
	}
	
	public List<Parcela> getParcelasByNF(Long idNf) {
		return this.em.createQuery("SELECT p FROM Parcela p WHERE p.notaFiscal.id = :pId", Parcela.class)
				.setParameter("pId", idNf)
				.getResultList();
		}
	
	public List<Parcela> getParcelasPagasByNF(Long idNf) {
		return this.em.createQuery("SELECT p FROM Parcela p WHERE p.notaFiscal.id = :pId AND p.pago = 1", Parcela.class)
			.setParameter("pId", idNf)
			.getResultList();
	}
}
