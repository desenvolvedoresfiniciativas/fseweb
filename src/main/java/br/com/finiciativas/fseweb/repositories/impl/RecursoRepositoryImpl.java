package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Recurso;

@Repository
public class RecursoRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Recurso recurso) {
		this.em.persist(recurso);
	}

	public void delete(Recurso recurso) {
		this.em.remove(recurso);
	}

	@Transactional
	public Recurso update(Recurso recurso) {
		return this.em.merge(recurso);
	}
	
	public List<Recurso> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Recurso o ", Recurso.class)
				.getResultList();
	}

	public Recurso findById(Long id) {
		return this.em.find(Recurso.class, id);
	}

	public List<Recurso> getRecursoByAcompanhamento(Long id) {
		return this.em.createQuery("SELECT distinct o FROM Acompanhamento o WHERE o.producao.id = :pId ", Recurso.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
