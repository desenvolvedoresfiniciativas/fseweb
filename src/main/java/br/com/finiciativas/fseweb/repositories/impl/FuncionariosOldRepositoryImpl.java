package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.old.FuncionariosOld;

@Repository
public class FuncionariosOldRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(FuncionariosOld funcionarioOld) {
		
		
		this.em.persist(funcionarioOld);
	}

	public void delete(FuncionariosOld funcionarioOld) {
		this.em.remove(funcionarioOld);
	}

	@Transactional
	public FuncionariosOld update(FuncionariosOld funcionarioOld) {
		return this.em.merge(funcionarioOld);
	}

	public List<FuncionariosOld> getAll() {
		return this.em.createQuery("SELECT distinct FO FROM FuncionariosOld FO ", FuncionariosOld.class)
				.getResultList();
	}

	public FuncionariosOld findById(Long id) {
		return this.em.find(FuncionariosOld.class, id);
	}
	
}
