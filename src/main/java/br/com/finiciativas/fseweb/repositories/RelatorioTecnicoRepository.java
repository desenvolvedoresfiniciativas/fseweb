package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;

public interface RelatorioTecnicoRepository {

	void create(RelatorioTecnico relatorio);

	void delete(RelatorioTecnico relatorio);

	RelatorioTecnico update(RelatorioTecnico relatorio);
	
	List<RelatorioTecnico> getAll();

	RelatorioTecnico findById(Long id);

	List<RelatorioTecnico> getRelatorioTecnicoByProducao(Long id);
	
}
