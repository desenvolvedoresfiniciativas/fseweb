package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.ItemTarefaRepository;

@Repository
public class ItemTarefaRepositoryImpl implements ItemTarefaRepository {

	@Autowired
	private EntityManager em;

	@Transactional
	@Override
	public void create(ItemTarefa itemTarefa) {
		em.persist(itemTarefa);
	}

	@Transactional
	@Override
	public ItemTarefa update(ItemTarefa itemTarefa) {
		ItemTarefa itemAtt = this.em.find(ItemTarefa.class, itemTarefa.getId());
		return em.merge(itemAtt);
	}

	@Override
	public List<ItemTarefa> getAll() {
		return em.createQuery("SELECT distinct itarefa FROM ItemTarefa itarefa ORDER BY itarefa.nome ASC",
				ItemTarefa.class).getResultList();
	}

	@Override
	public List<ItemTarefa> getLista() {
		return em.createQuery("SELECT distinct * FROM ItemTarefa i WHERE i.tipo = :pLista ", ItemTarefa.class)
				.setParameter("pLista", "LISTA").getResultList();
	}

	@Override
	public List<ItemTarefa> getItensByProducao(Producao producao) {
		Produto prod = producao.getProduto();

		List<EtapaTrabalho> etapa = new ArrayList<>();
		List<Tarefa> tarefas = new ArrayList<>();
		List<ItemTarefa> itens = new ArrayList<>();

		etapa.addAll(prod.getEtapasTrabalho());

		for (EtapaTrabalho etapaTrabalho : etapa) {
			tarefas.addAll(etapaTrabalho.getTarefas());
		}

		for (Tarefa tarefa : tarefas) {
			itens.addAll(tarefa.getItensTarefa());
		}

		return itens;
	}

	@Override
	public List<ItemTarefa> getItensByTarefa(Tarefa tarefa) {
		return tarefa.getItensTarefa();
	}

	public ItemTarefa findById(Long id) {
		return this.em.find(ItemTarefa.class, id);
	}
	
	public List<ItemTarefa> getItemByTarefa(Long idTarefa) {
		return em.createQuery("SELECT i FROM ItemTarefa i  JOIN i.tarefa f  WHERE f.id = :pId",  ItemTarefa.class)
				.setParameter("pId", idTarefa).getResultList();
	}
	
}
