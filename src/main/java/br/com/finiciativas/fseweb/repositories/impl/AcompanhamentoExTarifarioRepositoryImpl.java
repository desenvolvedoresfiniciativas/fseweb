package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.AcompanhamentoExTarifario;

@Repository
public class AcompanhamentoExTarifarioRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Transactional
	public AcompanhamentoExTarifario create(AcompanhamentoExTarifario acompanhamento) {
			this.em.persist(acompanhamento);
			em.flush();
			
			return acompanhamento;
	}

	public void delete(AcompanhamentoExTarifario acompanhamento) {
		this.em.remove(acompanhamento);
	}

	@Transactional
	public AcompanhamentoExTarifario update(AcompanhamentoExTarifario acompanhamento) {
		return this.em.merge(acompanhamento);
	}

	public List<AcompanhamentoExTarifario> getAll() {
		return this.em.createQuery("SELECT distinct o FROM AcompanhamentoExTarifario o ", AcompanhamentoExTarifario.class)
				.getResultList();
	}

	public AcompanhamentoExTarifario findById(Long id) {
		return this.em.find(AcompanhamentoExTarifario.class, id);
	}

	public List<AcompanhamentoExTarifario> getAcompanhamentoExTarifarioBySubProducao(Long id) {
		em.clear();
		return this.em.createQuery("SELECT o FROM AcompanhamentoExTarifario o WHERE o.subProducao.id = :pId ",
				AcompanhamentoExTarifario.class).setParameter("pId", id).getResultList();
	}

	
}
