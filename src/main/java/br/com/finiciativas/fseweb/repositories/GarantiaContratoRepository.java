package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.contrato.GarantiaContrato;

public interface GarantiaContratoRepository {
	
	List<GarantiaContrato> getAll();
	
	GarantiaContrato findById(Long id);

}
