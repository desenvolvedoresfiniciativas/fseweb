package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.old.ContratoOld;

@Repository
public class ContratoOldRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(ContratoOld contratoOld) {
		
		this.em.persist(contratoOld);
	}
	
	public void delete(ContratoOld contratoOld) {
		this.em.remove(contratoOld);
	}

	@Transactional
	public ContratoOld update(ContratoOld contratoOld) {
		return this.em.merge(contratoOld);
	}

	public List<ContratoOld> getAll() {
		return this.em.createQuery("SELECT distinct CO FROM ContratoOld CO ", ContratoOld.class)
				.getResultList();
	}

	public ContratoOld findById(Long id) {
		return this.em.find(ContratoOld.class, id);
	}
	
}
