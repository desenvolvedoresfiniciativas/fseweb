package br.com.finiciativas.fseweb.repositories.impl;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.consultor.Impostos;
import br.com.finiciativas.fseweb.repositories.ImpostosRepository;

@Repository
public class ImpostosRepositoryImpl implements ImpostosRepository{

	@Autowired
	private EntityManager em;
	
	@Override
	public Impostos getImpostosByCampanha2020() {
		try {
			return em.createQuery("SELECT i from Impostos i WHERE campanha = '2020'", Impostos.class).getSingleResult();
			
		} catch (Exception e) {
			System.out.println("Erro no m�todo: getImpostosByCampanha2020" + e);
			return null;
		}
	}
	@Override
	public Impostos getImpostosbyId(Long id) {
		return em.createQuery("SELECT i from Impostos i WHERE i.id = :pId", Impostos.class).setParameter("pId", id).getSingleResult();
	}
}
