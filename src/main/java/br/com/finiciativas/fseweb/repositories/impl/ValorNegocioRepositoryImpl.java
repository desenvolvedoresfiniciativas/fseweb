package br.com.finiciativas.fseweb.repositories.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

//import org.hibernate.criterion.CriteriaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.abstractclass.Endereco;
import br.com.finiciativas.fseweb.enums.TipoDeApuracao;
import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;
import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.honorario.FaixasEscalonado;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.valorNegocio.UltimaAtualizacaoVN;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContatoClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.UltimaAtualizacaoVNService;

@Repository
public class ValorNegocioRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private BalanceteCalculoServiceImpl balanceteService;

	@Autowired
	private HonorarioServiceImpl honorarioService;

	@Autowired
	private ItemValoresServiceImpl itemValoresService;

	@Autowired
	private ContatoClienteServiceImpl ContatoClienteService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private UltimaAtualizacaoVNService ultimaAtualizacaoService;
	
	@Autowired
	private EnderecoClienteRepositoryImpl enderecoClienteRepository;

	public List<ValorNegocio> getAll() {
		return this.em.createQuery("SELECT distinct l FROM ValorNegocio l ", ValorNegocio.class).getResultList();
	}

	@Transactional
	public void create(ValorNegocio vn) {
		this.em.persist(vn);
	}

	public void delete(ValorNegocio vn) {
		this.em.remove(vn);
	}

	@Transactional
	public void deleteOfProducao(Long id) {
		em.createQuery("DELETE FROM ValorNegocio v WHERE v.producao.id = :pId").setParameter("pId", id).executeUpdate();
		em.createQuery("DELETE FROM ValorNegocioHistorico v WHERE v.producao.id = :pId").setParameter("pId", id)
				.executeUpdate();
	}

	@Transactional
	public void update(ValorNegocio vn) {
		em.merge(vn);
		em.flush();
	}

	public ValorNegocio findById(Long id) {
		return em.createQuery("SELECT t FROM ValorNegocio t WHERE t.id = :pId", ValorNegocio.class)
				.setParameter("pId", id).getSingleResult();
	}

	public List<ValorNegocio> getVNbyAno(String ano) {
		try {
			return this.em.createQuery(
					"SELECT distinct l FROM ValorNegocio l LEFT JOIN l.producao p WHERE l.campanha = :pAno and p.produto.id = :pProd",
					ValorNegocio.class).setParameter("pAno", ano).setParameter("pProd", 23L).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<ValorNegocio> getAllAtivasbyAno(String ano) {
		try {
			return this.em.createQuery(
					"SELECT distinct l FROM ValorNegocio l WHERE l.campanha = :pAno and l.situacao = 'ATIVA'",
					ValorNegocio.class).setParameter("pAno", ano).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ValorNegocio getVNbyProd(Long id) {
		ValorNegocio vn = new ValorNegocio();
		try {
			vn = this.em.createQuery("SELECT l FROM ValorNegocio l WHERE l.producao.id = :pId", ValorNegocio.class)
					.setParameter("pId", id).getSingleResult();
			return vn;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro no metodo: getVNbyProd.ValorNegocioRepositoryImpl " + e);
			return null;
		}
	}

	public List<ValorNegocio> getVNbyEmpresa(Long id) {
		ValorNegocio vn = new ValorNegocio();
		try {
			return this.em.createQuery("SELECT l FROM ValorNegocio l WHERE l.cliente.id = :pId", ValorNegocio.class)
					.setParameter("pId", id).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ValorNegocio getVNbyEmpresaAndProducao(Long idEmpresa, Long idProducao) {

		try {
			return this.em
					.createQuery("SELECT l FROM ValorNegocio l WHERE  l.producao.id = :pIdProducao", ValorNegocio.class)
					.setParameter("pIdProducao", idProducao).getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro no metodo: getVNbyEmpresaAndProducao" + e);
			return null;
		}
	}

	public ValorNegocio getVNbyProducao(Long id) {

		try {
			return this.em.createQuery("SELECT l FROM ValorNegocio l WHERE l.producao.id = :pId", ValorNegocio.class)
					.setParameter("pId", id).getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// "SELECT SUM(valor_base) FROM vn WHERE vn.equipe = equipe"
	public List<ValorNegocio> getVNByEquipe(String nome) {
		try {
			return this.em.createQuery("SELECT vn FROM ValorNegocio vn WHERE vn.equipe = :pNome", ValorNegocio.class)
					.setParameter("pNome", nome).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro na sele��o da soma de valor de neg�cio da equipe: " + nome);
			return null;
		}

	}

	@Transactional
	@Scheduled(cron = "0 0 9 * * *")
	@Scheduled(cron = "0 0 13 * * *")
	@Scheduled(cron = "0 0 17 * * *")
	public void geraValorDeNegocioByAno() {
		String ano = "2021";
		System.out.println(ano + " anoooooooooo");

//		// VN ALL
		List<Producao> producoes = producaoService.getAllByAno(ano);

		// VN BY EQUIPE
//		List<BalanceteCalculo> balancetes = balanceteService.getAllBalanceteByAno(ano);
//		List<Producao> producoesA = producaoService.getAllProducaoByEquipeAndAno("2020", 411l);

		// VN BY PRODUCAO
		//List<Producao> producoes = new ArrayList<Producao>();
		//Producao prod = producaoService.findById(2130l);
		//producoes.add(prod);

		String prevBeneficioOut;
		BigDecimal teto = BigDecimal.valueOf(0);
		ValorNegocio vn = new ValorNegocio();

		for (Producao producao : producoes) {
			try {
				System.out.println("! ! ! --- Processando a campanha de ID:" + producao.getId() + " da Empresa: "
						+ producao.getContrato().getCliente().getRazaoSocial());

				Contrato contrato = producao.getContrato();

				if (producao.getProduto().getId() != 23l) {

					System.out.println("VN de Outros Produtos: " + producao.getContrato().getProduto().getNome());

					vn = getVNbyProd(producao.getId());
					if (Objects.isNull(vn)) {
						System.out.println("Valor de Neg�cio n�o encontrado, criando novo...");
						vn = null;
						vn = new ValorNegocio();
					}

					vn.setCampanha(producao.getAno());
					vn.setProducao(producao);
					vn.setTipoNegocio(producao.getTipoDeNegocio());
					vn.setCliente(producao.getContrato().getCliente());
					try {
						vn.setComercial(producao.getContrato().getComercialResponsavelEfetivo().getSigla());
					} catch (Exception e) {
						e.printStackTrace();
						vn.setComercial("Erro cadastral");
					}
					try {
						vn.setDivisao(producao.getContrato().getCliente().getDivisao().toString());
					} catch (Exception e) {
						e.printStackTrace();
						vn.setDivisao(null);
					}

					vn.setSituacao(producao.getSituacao().toString());
					vn.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
					// vn.setAnt_fat(ant_fat);
					vn.setCategoria(producao.getContrato().getCliente().getCategoria());
					vn.setCtrObs(producao.getContrato().getObsContrato());
					vn.setEquipe(producao.getEquipe().getNome());
					String estimativaComercial = producao.getContrato().getEstimativaComercial();
					estimativaComercial = estimativaComercial.replace(".", "");
					estimativaComercial = estimativaComercial.replace(",", ".");

					if (estimativaComercial.isEmpty()) {
						vn.setEstComercial(0.0f);
					} else {
						vn.setEstComercial(Float.parseFloat(estimativaComercial));
						vn.setValorBase(BigDecimal.valueOf(Float.parseFloat(estimativaComercial)));
					}

					vn.setModoValor("Comercial");
					vn.setSigla(producao.getConsultor().getSigla());

					continue;

				}

				// Reseta variaveis tempor�rias
				BigDecimal valorNegocio = BigDecimal.valueOf(0);

				vn = getVNbyProd(producao.getId());
				if (Objects.isNull(vn)) {
					System.out.println("Valor de Neg�cio n�o encontrado, criando novo...");
					vn = null;
					vn = new ValorNegocio();

				} else {
					if (vn.getModoValor().equalsIgnoreCase("Manual")) {
						System.out.println("Achou manual e n�o prosseguiu");
						vn.setCampanha(producao.getAno());
						vn.setProducao(producao);
						vn.setTipoNegocio(producao.getTipoDeNegocio());
						vn.setCliente(producao.getContrato().getCliente());
						vn.setSituacao(producao.getSituacao().toString());
						vn.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
						// vn.setAnt_fat(ant_fat);
						vn.setCategoria(producao.getContrato().getCliente().getCategoria());
						vn.setCtrObs(producao.getContrato().getObsContrato());
						vn.setEquipe(producao.getEquipe().getNome());
						vn.setSigla(producao.getConsultor().getSigla());	
						//atualizar o endere�o do cliente
						List<EnderecoCliente> enderecos = enderecoClienteRepository.getAllById(vn.getCliente().getId());
						if(Objects.nonNull(enderecos)) {
							boolean matriz = false;
							
							for (EnderecoCliente endereco : enderecos) {
								if(endereco.getTipo().equalsIgnoreCase("matriz")) {
									matriz = true;
									vn.setCidade(endereco.getCidade());
									vn.setEstado(endereco.getEstado());
								}
							}
							
							if(matriz == false && enderecos.size() > 0) {
								vn.setCidade(enderecos.get(0).getCidade());
								vn.setEstado(enderecos.get(0).getEstado());
							}
						}
						
						update(vn);

						continue;
					}
				}
				
				
				
				
				List<BalanceteCalculo> balancetesProducoes = balanceteService
						.getBalanceteCalculoByProducao(producao.getId());

				float beneficio = 0;

				BalanceteCalculo balanceteLast = new BalanceteCalculo();
				if (balancetesProducoes.size() > 0) {
					balanceteLast.setVersao(0);
					for (BalanceteCalculo balanceteCalculo : balancetesProducoes) {
						if (balanceteCalculo.getVersao() > balanceteLast.getVersao()) {
							balanceteLast = balanceteCalculo;
						}
						if (!balanceteCalculo.getReducaoImposto().isEmpty()) {
							beneficio += Float.parseFloat(
									balanceteCalculo.getReducaoImposto().replace(".", "").replace(",", "."));
						}
					}
				} else {
					System.out.println("Sem Balancete, deixando Last = NULL");
					balanceteLast = null;
				}

				List<Honorario> honorarios = contrato.getHonorarios();
				for (Honorario honorario : honorarios) {

					// Tem balancete?
					if (Objects.nonNull(balanceteLast)) {
						System.out.println("Produ��o Possui Balancete, Verificando honor�rios...");

						// Valor Fixo
						if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
							System.out.println("Valor Fixo");

							ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
							if (Objects.nonNull(fixo)) {

								BigDecimal valorFixo = fixo.getValor();
								if (fixo.isValorTrimestral()) {
									valorFixo = valorFixo.multiply(BigDecimal.valueOf(4));
								} else if (fixo.isValorMensal()) {
									valorFixo = valorFixo.multiply(BigDecimal.valueOf(12));
								}

								valorNegocio = valorNegocio.add(valorFixo);
								if (balanceteLast.isVersaoFinal()) {
									vn.setModoValor("Finalizado");
								} else {
									vn.setModoValor("Real");
								}
							}
						}
						// Percentual Escalonado
						else if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
							System.out.println("Percentual Escalonado");

							PercentualEscalonado pe = honorarioService
									.getPercentualEscalonadoByContrato(contrato.getId());

							String stringValorBalancete = "0,00";

							if (producao.getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
								stringValorBalancete = balanceteLast.getReducaoImposto().replace(".", "")
										.replace(",", ".").trim();
							} else {
								System.out.println("Valor consolidado da produ��o: " + beneficio);
								stringValorBalancete = String.valueOf(beneficio);
							}

							if (Objects.nonNull(pe)) {
								if (stringValorBalancete == "" || stringValorBalancete == null
										|| stringValorBalancete.isEmpty()) {
									System.out.println("balancete vazio");
									stringValorBalancete = "0.0";
								} else {
									System.out.println(
											"Balancete n�o identificado como vazio, valor: " + stringValorBalancete);
								}

								teto = pe.getLimitacao();
								List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

								BigDecimal valorCalc = BigDecimal.valueOf(0);

								System.out.println("STRING BALANCETE -----> " + stringValorBalancete);

								BigDecimal balancete = new BigDecimal(stringValorBalancete);
								BigDecimal temp = new BigDecimal(stringValorBalancete);

								for (FaixasEscalonado faixa : faixas) {

									System.out.println("Temp atual " + temp);

									// Define vari�veis
									BigDecimal inicial = faixa.getValorInicial();
									BigDecimal finalF = faixa.getValorFinal();
									BigDecimal gap1 = finalF.subtract(inicial);
									// BigDecimal gap2 = temp.subtract(inicial);
									Float percentual = faixa.getValorPercentual();

									System.out.println("Faixa: " + inicial + " ... " + finalF);
									System.out.println("Diferen�a: " + gap1);

									// Se � maior que o final, calcula direto com o gap1
									if (temp.compareTo(BigDecimal.ZERO) != 0) {
										if (temp.compareTo(gap1) >= 0) {

											System.out.println("Passa direto da faixa, calcula com o f...");

											valorCalc = valorCalc.add(gap1.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

											System.out.println("Temp ap�s iter�a�o " + temp);
											System.out.println("Saldo para soma :" + gap1.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											// Se est� entre a faixa, calcula com o temp direto na porcentagem.
										} else if (temp.compareTo(gap1) < 0) {

											valorCalc = valorCalc.add(temp.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											BigDecimal zero = BigDecimal.ZERO;
											temp = zero;
										}
									}
								}

								// Se possui acima de, aplica.
								if (pe.getAcimaDe() != null) {
									System.out.println(
											"Possui 'Acima de' calculando: " + temp + " x " + pe.getPercentAcimaDe());
									if (balancete.compareTo(pe.getAcimaDe()) > 0) {
										if (temp.compareTo(BigDecimal.ZERO) > 0) {
											BigDecimal acima = temp.multiply(BigDecimal.valueOf(pe.getPercentAcimaDe())
													.divide(new BigDecimal(100)));
											valorCalc = valorCalc.add(acima);
										}
									}
								} else {
									System.out.println("N�o identificou 'Acima de'");
								}

								// Soma o C�lculo de Percentual Escalonado ao VN
								valorNegocio = valorNegocio.add(valorCalc);

								// Se possui limita��o, aplica.
								if (pe.getLimitacao() != null) {
									if (pe.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
										System.out.println("Possui limita��o de " + pe.getLimitacao());
										if (valorNegocio.compareTo(pe.getLimitacao()) > 0) {
											System.out.println("VN maior que teto");

											valorNegocio = valorNegocio
													.valueOf(Double.valueOf(pe.getLimitacao().toString()));
											valorNegocio = valorNegocio.setScale(2, BigDecimal.ROUND_HALF_UP);

										}
									}
								}

								System.out.println("PE calculado de: " + valorCalc);

								if (balanceteLast.isVersaoFinal()) {
									System.out.println("Finalizou");
									vn.setModoValor("Finalizado");
								} else {
									System.out.println("Realizou");
									vn.setModoValor("Real");
								}
							}
							// Percentual Fixo
						} else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
							PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(contrato.getId());
							System.out.println("Percentual Fixo");

							if (Objects.nonNull(fixo)) {

								String stringValorBalancete = "0,00";

								if (producao.getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
									stringValorBalancete = balanceteLast.getReducaoImposto().replace(".", "")
											.replace(",", ".").trim();
								} else {
									System.out.println("Valor consolidado da produ��o: " + beneficio);
									stringValorBalancete = String.valueOf(beneficio);
								}

								if (stringValorBalancete == "" || stringValorBalancete == null
										|| stringValorBalancete.isEmpty()) {
									System.out.println("balancete vazio");
									stringValorBalancete = "0,0";
								} else {
									System.out.println(
											"Balancete n�o identificado como vazio, valor: " + stringValorBalancete);
								}

								BigDecimal valorCalc = BigDecimal.valueOf(0);
								BigDecimal balancete = new BigDecimal(stringValorBalancete);

								System.out.println("!-!-! Valor a ser calculado: " + balancete + " x "
										+ fixo.getValorPercentual());

								valorCalc = balancete.multiply(
										BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

								// Soma o C�lculo de Percentual Escalonado ao VN
								valorNegocio = valorNegocio.add(valorCalc);

								// Se possui limita��o, aplica.
								if (fixo.getLimitacao() != null) {
									if (fixo.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
										System.out.println("Possui limita��o de " + fixo.getLimitacao());
										if (valorNegocio.compareTo(fixo.getLimitacao()) > 0) {
											System.out.println("VN maior que teto");

											valorNegocio = valorNegocio
													.valueOf(Double.valueOf(fixo.getLimitacao().toString()));
											valorNegocio = valorNegocio.setScale(2, BigDecimal.ROUND_HALF_UP);

										}
									}
								}
								if (balanceteLast.isVersaoFinal()) {
									vn.setModoValor("Finalizado");
								} else {
									vn.setModoValor("Real");
								}
							}
						}
					} else {
						System.out.println("N�o tem balancete, gerando prev de benef�cio");

						// Pega a previs�o de Disp�ndio
						ItemValores itemPrevDispendio = itemValoresService.getPrevValorDispendio(producao.getId());
						// Pega a previs�o de Exclus�o
						ItemValores itemPrevEclusao = itemValoresService.getValorExclusao(producao.getId());
						// Pega al�quota
						ItemValores itemAli = itemValoresService.getAliquotaByProducao(producao.getId());

						if (Objects.nonNull(itemPrevDispendio) && Objects.nonNull(itemPrevEclusao)
								&& Objects.nonNull(itemAli)) {
							if (itemPrevDispendio.getValor().equalsIgnoreCase("")) {
								itemPrevDispendio.setValor("0,0");
							}

							// Converte Previs�o Disp�ndio para BigDecimal
							BigDecimal prevDispendio = new BigDecimal(0);
							if (!itemPrevDispendio.getValor().equals("")) {
								if (!itemPrevDispendio.getValor().equalsIgnoreCase("SELECIONE")) {
									if (!itemPrevDispendio.getValor().isEmpty()) {
										String strPrevDispendio = itemPrevDispendio.getValor();
										strPrevDispendio = strPrevDispendio.replace(".", "");
										strPrevDispendio = strPrevDispendio.replace(",", ".");
										strPrevDispendio = strPrevDispendio.trim();
										prevDispendio = new BigDecimal(strPrevDispendio);
									}
								}
							}

							System.out.println("Previs�o Disp�ndio: " + prevDispendio);

							// Converte Previs�o Exclus�o para int
							float prevExclusao = 0.0f;
							if (!itemPrevEclusao.getValor().equals("")) {
								if (!itemPrevEclusao.getValor().equalsIgnoreCase("SELECIONE")) {
									if (!itemPrevEclusao.getValor().isEmpty()) {
										String strPrevExclusao = itemPrevEclusao.getValor();
										strPrevExclusao = strPrevExclusao.replace("%", "");
										strPrevExclusao = strPrevExclusao.replace(",", ".");
										prevExclusao = Float.parseFloat(strPrevExclusao) / 100;
									}
								}
							}

							System.out.println("Previs�o Exclus�o: " + prevExclusao);

							// Converte Al�quota para int
							float aliquota = 0.0f;
							if (!itemAli.getValor().equals("")) {
								if (!itemAli.getValor().equalsIgnoreCase("SELECIONE")) {
									if (!itemAli.getValor().isEmpty()) {
										aliquota = Float.parseFloat(itemAli.getValor()) / 100;
									}
								}
							}
							System.out.println("Previs�o aliquota: " + aliquota);

							BigDecimal prevBeneficio = new BigDecimal(0.0);
							if (prevDispendio.compareTo(BigDecimal.ZERO) > 0 && prevExclusao > 0.0f
									&& aliquota > 0.0f) {
								prevBeneficio = prevDispendio.multiply(new BigDecimal(prevExclusao))
										.multiply(new BigDecimal(aliquota));
								vn.setPrevBeneficio(prevBeneficio.toString());
							} else {
								vn.setPrevBeneficio(null);
							}

							prevBeneficioOut = prevBeneficio.toString();
							System.out.println("Previs�o de Benef�cio c�lculado: " + prevBeneficioOut);

							if (prevBeneficio.compareTo(BigDecimal.ZERO) == 0) {
								System.out.println("Entrou em estimativa Comercial pelo beneficio ser 0");
								String estimativaComercial = producao.getContrato().getEstimativaComercial();

								if (estimativaComercial.isEmpty()) {
									estimativaComercial = "0,00";
								}

								estimativaComercial = estimativaComercial.replace(".", "");
								estimativaComercial = estimativaComercial.replace(",", ".");
								valorNegocio = BigDecimal.valueOf(Float.parseFloat(estimativaComercial));
								vn.setModoValor("Comercial");
							} else {
								// Estimado Valor Fixo
								if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
									System.out.println("Entrou valor fixo");
									ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
									if (Objects.nonNull(fixo)) {

										BigDecimal valorFixo = fixo.getValor();

										valorNegocio = valorNegocio.add(valorFixo);
										vn.setModoValor("Estimado");

									}
								}
								// Estimado Percentual Escalonado
								else if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
									System.out.println("Entrou PE");
									PercentualEscalonado pe = honorarioService
											.getPercentualEscalonadoByContrato(contrato.getId());

									BigDecimal stringValorBalancete = prevBeneficio;
									if (Objects.nonNull(pe)) {
										System.out.println("PE N�o � Nulo");

										vn.setModoValor("Estimado");

										teto = pe.getLimitacao();
										List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

										BigDecimal valorCalc = BigDecimal.valueOf(0);

										BigDecimal balancete = stringValorBalancete;
										BigDecimal temp = stringValorBalancete;

										try {

											for (FaixasEscalonado faixa : faixas) {

												System.out.println("Temp atual " + temp);

												// Define vari�veis
												BigDecimal inicial = faixa.getValorInicial();
												BigDecimal finalF = faixa.getValorFinal();
												BigDecimal gap1 = finalF.subtract(inicial);
												// BigDecimal gap2 = temp.subtract(inicial);
												Float percentual = faixa.getValorPercentual();

												System.out.println("Faixa: " + inicial + " ... " + finalF);
												System.out.println("Diferen�a: " + gap1);

												// Se � maior que o final, calcula direto com o gap1
												if (temp.compareTo(BigDecimal.ZERO) != 0) {
													if (temp.compareTo(gap1) >= 0) {

														System.out.println("Passa direto da faixa, calcula com o f...");

														valorCalc = valorCalc.add(gap1.multiply(BigDecimal
																.valueOf(percentual).divide(new BigDecimal(100))));

														temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

														System.out.println("Saldo para soma :"
																+ gap1.multiply(BigDecimal.valueOf(percentual)
																		.divide(new BigDecimal(100))));

														// Se est� entre a faixa, calcula com o temp direto na
														// porcentagem.
													} else if (temp.compareTo(gap1) < 0) {

														valorCalc = valorCalc.add(temp.multiply(BigDecimal
																.valueOf(percentual).divide(new BigDecimal(100))));

														BigDecimal zero = BigDecimal.ZERO;
														temp = zero;
													}
												}
											}

											System.out.println("PE calculado com o valor de " + valorCalc);

											// Se possui acima de, aplica.
											if (pe.getAcimaDe() != null) {
												System.out.println("Possui 'Acima de' calculando: " + temp + " x "
														+ pe.getPercentAcimaDe());
												if (balancete.compareTo(pe.getAcimaDe()) > 0) {
													if (temp.compareTo(BigDecimal.ZERO) > 0) {
														BigDecimal acima = temp
																.multiply(BigDecimal.valueOf(pe.getPercentAcimaDe())
																		.divide(new BigDecimal(100)));
														valorCalc = valorCalc.add(acima);
													}
												}
											} else {
												System.out.println("N�o identificou 'Acima de'");
											}

											// Soma o C�lculo de Percentual Escalonado ao VN
											valorNegocio = valorNegocio.add(valorCalc);

											// Se possui limita��o, aplica.
											if (pe.getLimitacao() != null) {
												if (pe.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
													System.out.println("Possui limita��o de " + pe.getLimitacao());
													if (valorNegocio.compareTo(pe.getLimitacao()) > 0) {
														System.out.println("VN maior que teto");

														valorNegocio = valorNegocio
																.valueOf(Double.valueOf(pe.getLimitacao().toString()));
														valorNegocio = valorNegocio.setScale(2,
																BigDecimal.ROUND_HALF_UP);

													}
												}
											}

										} catch (Exception e) {
											e.printStackTrace();
											valorNegocio = valorNegocio.add(valorCalc);
											vn.setModoValor("Erro cadastral no Contrato");
											vn.setValorBase(BigDecimal.valueOf(0));
										}
									}
									// Estimado Percentual Fixo
								} else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
									System.out.println("Entrou PF");
									PercentualFixo fixo = honorarioService
											.getPercentualFixoByContrato(contrato.getId());

									if (Objects.nonNull(fixo)) {

										BigDecimal valorCalc = BigDecimal.valueOf(0);

										if (fixo.getValorPercentual() == null) {
											vn.setModoValor("Erro cadastral no Contrato");
											fixo.setValorPercentual(0.0f);
										}

										BigDecimal balancete = prevBeneficio;
										valorCalc = balancete.multiply(BigDecimal.valueOf(fixo.getValorPercentual())
												.divide(new BigDecimal(100)));

										// Soma o C�lculo de Percentual Escalonado ao VN
										valorNegocio = valorNegocio.add(valorCalc);

										// Se possui limita��o, aplica.
										if (fixo.getLimitacao() != null) {
											if (fixo.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
												System.out.println("Possui limita��o de " + fixo.getLimitacao());
												if (valorNegocio.compareTo(fixo.getLimitacao()) > 0) {
													System.out.println("VN maior que teto");

													valorNegocio = valorNegocio
															.valueOf(Double.valueOf(fixo.getLimitacao().toString()));
													valorNegocio = valorNegocio.setScale(2, BigDecimal.ROUND_HALF_UP);

												}
											}
										}
										vn.setModoValor("Estimado");
									}
								}
							}
						}
					}

					ItemValores itemPrevEclusao = itemValoresService.getValorExclusao(producao.getId());
					String prevExclusao = "0";
					if (Objects.nonNull(itemPrevEclusao)) {
						if (!itemPrevEclusao.getValor().equals("")) {
							if (!itemPrevEclusao.getValor().equalsIgnoreCase("SELECIONE")) {
								if (!itemPrevEclusao.getValor().isEmpty()) {
									String strPrevExclusao = itemPrevEclusao.getValor();
									prevExclusao = strPrevExclusao;
								}
							}
						}
					}

					vn.setValorBase(valorNegocio);
					vn.setCampanha(producao.getAno());
					vn.setProducao(producao);
					vn.setTipoNegocio(producao.getTipoDeNegocio());
					vn.setCliente(producao.getContrato().getCliente());
					
					//atualizar o endere�o do cliente
					
					List<EnderecoCliente> enderecos = enderecoClienteRepository.getAllById(vn.getCliente().getId());
					System.out.println("o tamanho da lista �: " + enderecos.size());
					
					if(Objects.nonNull(enderecos)) {
						boolean matriz = false;
						
						for (EnderecoCliente endereco : enderecos) {
							if(endereco.getTipo() != null) {
								if(endereco.getTipo().equalsIgnoreCase("matriz")) {
									matriz = true;
									vn.setCidade(endereco.getCidade());
									vn.setEstado(endereco.getEstado());
								}
							}
							
						}
						
						if(matriz == false && enderecos.size() > 0) {
							vn.setCidade(enderecos.get(0).getCidade());
							vn.setEstado(enderecos.get(0).getEstado());
						}
					}
					
					try {
						vn.setComercial(producao.getContrato().getComercialResponsavelEfetivo().getSigla());
					} catch (Exception e) {
						e.printStackTrace();
						vn.setComercial("Erro cadastral");
					}
					try {
						vn.setDivisao(producao.getContrato().getCliente().getDivisao().toString());
					} catch (Exception e) {
						// e.printStackTrace();
						vn.setDivisao(null);
					}

					vn.setExclPrev(prevExclusao);
					if (Objects.nonNull(balanceteLast)) {
						vn.setExclReal(balanceteLast.getExclusaoUtilizada());
					} else {
						vn.setExclReal(null);
					}

					String estimativaComercial = producao.getContrato().getEstimativaComercial();
					estimativaComercial = estimativaComercial.replace(".", "");
					estimativaComercial = estimativaComercial.replace(",", ".");

					if (estimativaComercial.isEmpty()) {
						vn.setEstComercial(0.0f);
					} else {
						vn.setEstComercial(Float.parseFloat(estimativaComercial));
					}

					EtapasPagamento timing = producao.getContrato().getEtapasPagamento();

					// SYSOUT PARA VER VALOR ATUAL E ENTRADA DE CONDI��ES !!!!
					if (Objects.nonNull(timing)) {

						float economico = 0;
						float submissao = 0;
						float mctic = 0;

						// Balancete
						if (Objects.nonNull(timing.getEtapa1()) && timing.getEtapa1() != null) {
							System.out.println("Entrou EC etapa 1 " + timing.getEtapa1().getId());
							if (timing.getEtapa1().getId() == 66l) {
								economico = timing.getPorcentagem1();
								System.out.println(economico + " ec1");
								vn.setCtrPcDe(economico);
							}
						}
						if (Objects.nonNull(timing.getEtapa2()) && timing.getEtapa2() != null) {
							System.out.println("Entrou EC etapa 2 " + timing.getEtapa2().getId());
							if (timing.getEtapa2().getId() == 66l) {
								economico = timing.getPorcentagem2();
								System.out.println(economico + " ec2");
								vn.setCtrPcDe(economico);
							}
						}
						if (Objects.nonNull(timing.getEtapa3()) && timing.getEtapa3() != null) {
							System.out.println("Entrou EC etapa 3 " + timing.getEtapa3().getId());
							if (timing.getEtapa3().getId() == 66l) {
								economico = timing.getPorcentagem3();
								System.out.println(economico + " ec3");
								vn.setCtrPcDe(economico);
							}
						}

						// Submiss�o
						if (Objects.nonNull(timing.getEtapa1()) && timing.getEtapa1() != null) {
							System.out.println("Entrou sub etapa 1 " + timing.getEtapa1().getId());
							if (timing.getEtapa1().getId() == 68l) {
								submissao = timing.getPorcentagem1();
								System.out.println(submissao + " sub1");
								vn.setCtrPcSm(submissao);
							}
						}
						if (Objects.nonNull(timing.getEtapa2()) && timing.getEtapa2() != null) {
							System.out.println("Entrou sub etapa 2 " + timing.getEtapa2().getId());
							if (timing.getEtapa2().getId() == 68l) {
								submissao = timing.getPorcentagem2();
								System.out.println(submissao + " sub2");
								vn.setCtrPcSm(submissao);
							}
						}
						if (Objects.nonNull(timing.getEtapa3()) && timing.getEtapa3() != null) {
							System.out.println("Entrou sub etapa 3 " + timing.getEtapa3().getId());
							if (timing.getEtapa3().getId() == 68l) {
								submissao = timing.getPorcentagem3();
								System.out.println(submissao + " sub3");
								vn.setCtrPcSm(submissao);
							}
						}

						// MCTIC
						if (Objects.nonNull(timing.getEtapa1()) && timing.getEtapa1() != null) {
							System.out.println("Entrou mcti etapa 1 " + timing.getEtapa1().getId());
							if (timing.getEtapa1().getId() == 70l) {
								mctic = timing.getPorcentagem1();
								vn.setCtrPcRm(mctic);
							}
						}
						if (Objects.nonNull(timing.getEtapa2()) && timing.getEtapa2() != null) {
							System.out.println("Entrou mcti etapa 2 " + timing.getEtapa2().getId());
							if (timing.getEtapa2().getId() == 70l) {
								mctic = timing.getPorcentagem2();
								vn.setCtrPcRm(mctic);
							}
						}
						if (Objects.nonNull(timing.getEtapa3()) && timing.getEtapa3() != null) {
							System.out.println("Entrou mcti etapa 3 " + timing.getEtapa3().getId());
							if (timing.getEtapa3().getId() == 70l) {
								mctic = timing.getPorcentagem3();
								vn.setCtrPcRm(mctic);
							}
						}

						List<ContatoCliente> contatos = ContatoClienteService
								.getAllById(producao.getContrato().getCliente().getId());

						boolean verifyFidelization = false;
						for (ContatoCliente contato : contatos) {
							if (contato.isPesquisaSatisfacao()) {
								verifyFidelization = true;
							}
						}

						if (verifyFidelization == true) {
							vn.setFidelizado(true);
						} else {
							vn.setFidelizado(false);
						}

//						vn.setCtrPcDe(economico);
//						vn.setCtrPcSm(submissao);
//						vn.setCtrPcRm(mctic);
						if (producao.getContrato().getCliente().getSetorAtividade() != null) {
							vn.setSetor(producao.getContrato().getCliente().getSetorAtividade().toString());
						} else {
							vn.setSetor("");
						}
					}

					ItemValores prevRelatorios = itemValoresService.getPrevisaoDeRelatoriosByProducao(producao.getId());
					if (Objects.nonNull(prevRelatorios)) {
						if (prevRelatorios.getValor().matches("-?\\d+(\\.\\d+)?")) {
							vn.setPrevRelatorios(prevRelatorios.getValor());
						} else {
							vn.setPrevRelatorios("0");
						}
					} else {
						vn.setPrevRelatorios("0");
					}

					vn.setSigla(producao.getConsultor().getSigla());

					ItemValores formPeD = itemValoresService.getStatusFormByProducao(producao.getId());
					ItemValores submissaoItem = itemValoresService.getDataSubmissaoByProducao(producao.getId());

					if (Objects.nonNull(formPeD)) {
						if (!formPeD.getValor().equalsIgnoreCase("Pendente")
								|| !formPeD.getValor().equalsIgnoreCase("ENVIADO AO CLIENTE")
								|| !formPeD.getValor().equalsIgnoreCase("RECEBIDO CLIENTE")) {
							vn.setFormPeD(false);
						} else {
							vn.setFormPeD(true);
						}
					}

					if (Objects.nonNull(submissaoItem)) {
						if (submissaoItem.getValor() == "" || submissaoItem.getValor().isEmpty()) {
							vn.setSubmissao(false);
						} else {
							vn.setSubmissao(true);
						}
					}
				}

				List<Faturamento> fats = faturamentoService.getAllByProducao(producao.getId());
				BigDecimal fatReal = new BigDecimal(0);

				if (Objects.nonNull(fats)) {
					System.out.println("Quantidade de faturamentos encontrados: " + fats.size());
					for (Faturamento faturamento : fats) {
						System.out.println("Soma fat");
						fatReal = fatReal.add(faturamento.getValorEtapa());
						System.out.println("Valor de Fat somado: " + fatReal);
					}
				}

				ItemValores previsaoBalancete = itemValoresService.getPrevisaoBalanceteByProducao(producao.getId());
				if (Objects.nonNull(previsaoBalancete)) {
					vn.setPreEntregaCalc(previsaoBalancete.getValor());
				}

				String prevDispendioConvert = itemValoresService.getPrevValorDispendio(producao.getId()).getValor();
				prevDispendioConvert = prevDispendioConvert.replace(".", "");
				prevDispendioConvert = prevDispendioConvert.replace(",", ".");
				prevDispendioConvert = prevDispendioConvert.trim();

				if (!prevDispendioConvert.isEmpty()) {
					vn.setPrevDespesa(Float.parseFloat(prevDispendioConvert));
				}

				vn.setFatReal(fatReal.floatValue());

				if (vn.getProducao() != null) {
					if (vn.getModoValor().equalsIgnoreCase("estimado")) {
						ItemValores itemDispendio = itemValoresService.findByTarefaItemTarefaAndProducao(142l,
								producao.getId(), 323l);
						ItemValores itemExclusao = itemValoresService.findByTarefaItemTarefaAndProducao(142l,
								producao.getId(), 324l);
						ItemValores itemAliquota = itemValoresService.findByTarefaItemTarefaAndProducao(142l,
								producao.getId(), 368l);
						if (itemDispendio.getValor() != "") {
							System.out.println("encontrou um �tem com o valor preenchido");

							String dispendioFormatado = itemDispendio.getValor().replaceAll("\\.", "");
							System.out.println("O VALOR FORMATADO �: " + dispendioFormatado);
							dispendioFormatado = dispendioFormatado.replace(",", ".");
							System.out.println("O VALOR FORMATADO �: " + dispendioFormatado);
							vn.setDispendioAtual(Float.parseFloat(dispendioFormatado));
							// vn.setDispendioAtual(BigDecimal.valueOf(Double.parseDouble(itemDispendio.getValor())));
							System.out.println(
									"o valor do �tem �: " + vn.getDispendioAtual() + " " + itemDispendio.getValor());
						} else {
							System.out.println("encontrou um �tem com o valor vazio");
							System.out.println("o valor do �tem �: " + itemDispendio.getValor());
							vn.setDispendioAtual(0.0f);
							// vn.setDispendioAtual(BigDecimal.ZERO);
						}

						if (!itemExclusao.getValor().equalsIgnoreCase("selecione") || itemExclusao.getValor() != "") {
							vn.setExclusaoAtual(Float.parseFloat(
									itemExclusao.getValor().substring(0, itemExclusao.getValor().length() - 1)));
							System.out.println("O VALOR DA EXCLUS�O ATUAL �: " + vn.getExclusaoAtual());
						} else {
							vn.setExclusaoAtual(0.0f);

						}
						if (!itemAliquota.getValor().equalsIgnoreCase("selecione") || itemAliquota.getValor() != "") {
							vn.setAliquotaAtual(Float.parseFloat(itemAliquota.getValor()));
						} else {
							vn.setAliquotaAtual(0.0f);
						}

						vn.setValorCalculo(
								vn.getDispendioAtual() * (vn.getExclusaoAtual() / 100) * (vn.getAliquotaAtual() / 100));
						System.out.println("O VALOR DO CALCULO ATUAL E: " + vn.getValorCalculo());

					} else if (vn.getModoValor().equalsIgnoreCase("real")
							|| vn.getModoValor().equalsIgnoreCase("finalizado")) {

						ItemValores itemAliquota = itemValoresService.findByTarefaItemTarefaAndProducao(142l,
								producao.getId(), 368l);

						float aliquotaAtual = 0.0f;
						if (Objects.nonNull(itemAliquota) || !itemAliquota.getValor().equalsIgnoreCase("selecione")
								|| !itemAliquota.getValor().equalsIgnoreCase("")
								|| !itemAliquota.getValor().isEmpty()) {
							System.out.println("O �TEM VALOR � :" + itemAliquota.getValor());
							System.out.println("O ID DO �TEM � : " + itemAliquota.getId());
							try {
								aliquotaAtual = Float.parseFloat(itemAliquota.getValor());
							} catch (Exception e) {

							}

						} else {
							aliquotaAtual = 0.0f;
						}

						vn.setAliquotaAtual(aliquotaAtual);

						if (Objects.nonNull(producao.getTipoDeApuracao())) {
							if (producao.getTipoDeApuracao().equals(TipoDeApuracao.ANUAL)) {
								List<BalanceteCalculo> balancetes = balanceteService
										.getBalancetesNaoPrejuizoByProducao(producao.getId());

								String exclusaoUtilizada = "";
								Float dispendioAtual = 0.0f;
								if (Objects.nonNull(balancetes) && balancetes.size() > 0) {
									System.out.println("o tamanho do array �: " + balancetes.size());

									exclusaoUtilizada = balancetes.get(balancetes.size() - 1).getExclusaoUtilizada();
								}

								if (Objects.nonNull(balancetes) && balancetes.size() > 0) {
									System.out.println("o tamanho do array �: " + balancetes.size());
									String dispendioAtualString;
									dispendioAtualString = balancetes.get(balancetes.size() - 1).getValorTotal();
									dispendioAtualString = dispendioAtualString.replaceAll("\\.", "");
									dispendioAtualString = dispendioAtualString.replace(",", ".");
									dispendioAtual = Float.parseFloat(dispendioAtualString);
									vn.setDispendioAtual(dispendioAtual);
								}

								if (Objects.nonNull(exclusaoUtilizada)
										|| !exclusaoUtilizada.equalsIgnoreCase("selecione")
										|| !exclusaoUtilizada.equalsIgnoreCase("") || !exclusaoUtilizada.isEmpty()) {

									if (exclusaoUtilizada.equalsIgnoreCase("sessenta")) {
										vn.setExclusaoAtual(60.0f);
									} else if (exclusaoUtilizada.equalsIgnoreCase("setenta")) {
										vn.setExclusaoAtual(70.0f);
									} else if (exclusaoUtilizada.equalsIgnoreCase("oitenta")) {
										vn.setExclusaoAtual(80.0f);
									}
								} else {
									vn.setExclusaoAtual(0.0f);
								}

								vn.setValorCalculo(vn.getDispendioAtual() * (vn.getExclusaoAtual() / 100)
										* (vn.getAliquotaAtual() / 100));

							} else {
								List<BalanceteCalculo> balancetes = balanceteService
										.getBalancetesNaoPrejuizoByProducao(producao.getId());
								String exclusaoUtilizada = "";
								if (Objects.nonNull(balancetes) && balancetes.size() > 0) {
									System.out.println("o tamanho do array �: " + balancetes.size());
									exclusaoUtilizada = balancetes.get(balancetes.size() - 1).getExclusaoUtilizada();
								}

								if (Objects.nonNull(exclusaoUtilizada)
										|| !exclusaoUtilizada.equalsIgnoreCase("selecione")
										|| !exclusaoUtilizada.equalsIgnoreCase("") || !exclusaoUtilizada.isEmpty()) {

									if (exclusaoUtilizada.equalsIgnoreCase("sessenta")) {
										vn.setExclusaoAtual(60.0f);
									} else if (exclusaoUtilizada.equalsIgnoreCase("setenta")) {
										vn.setExclusaoAtual(70.0f);
									} else if (exclusaoUtilizada.equalsIgnoreCase("oitenta")) {
										vn.setExclusaoAtual(80.0f);
									}
								} else {
									vn.setExclusaoAtual(0.0f);
								}
								Float dispendioAtual = 0.0f;
								for (BalanceteCalculo balancete : balancetes) {
									String valorTotalString = balancete.getValorTotal().replaceAll("\\.", "");
									valorTotalString = valorTotalString.replace(",", ".");
									dispendioAtual += Float.parseFloat(valorTotalString);
								}
								vn.setDispendioAtual(dispendioAtual);
								vn.setValorCalculo(vn.getDispendioAtual() * (vn.getExclusaoAtual() / 100)
										* (vn.getAliquotaAtual() / 100));
								System.out.println("O VALOR DO CALCULO ATUAL E: " + vn.getValorCalculo());
							}
						}

					} else if (vn.getModoValor().equalsIgnoreCase("comercial")) {
						ItemValores itemDispendio = itemValoresService.findByTarefaItemTarefaAndProducao(142l,
								producao.getId(), 323l);

						if (itemDispendio.getValor() != "") {
							System.out.println("encontrou um �tem com o valor preenchido");

							String dispendioFormatado = itemDispendio.getValor().replaceAll("\\.", "");
							dispendioFormatado = dispendioFormatado.replace(",", ".");
							vn.setDispendioAtual(Float.parseFloat(dispendioFormatado));
							vn.setValorCalculo(Float.parseFloat(dispendioFormatado));

						} else {
							vn.setDispendioAtual(0.0f);
							vn.setValorCalculo(0.0f);
						}
					}

					// this.em.detach(vn);
					vn.setSituacao(producao.getSituacao().toString());
					vn.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
					// vn.setAnt_fat(ant_fat);
					vn.setCategoria(producao.getContrato().getCliente().getCategoria());
					vn.setCtrObs(producao.getContrato().getObsContrato());
					vn.setEquipe(producao.getEquipe().getNome());

					if (vn.getModoValor().equalsIgnoreCase("Real") || vn.getModoValor().equalsIgnoreCase("Estimado")) {
						String modo = checkBalancetes(vn);
						if (!modo.isEmpty()) {
							vn.setModoValor(modo);
						}
					}

					vn.setValorBase(vn.getValorBase().setScale(2, RoundingMode.CEILING));

					vn.setPrevBeneficio(atualizaPrevBeneficio(vn));
					update(vn);

					if (vn.getModoValor().equalsIgnoreCase("Real")) {
						swapToEstimativa(vn);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
				vn.setCliente(producao.getContrato().getCliente());
				vn.setModoValor("Erro ao gerar VN");
				this.em.detach(vn);
				update(vn);
			}
		}

		List<UltimaAtualizacaoVN> atualizacoes = ultimaAtualizacaoService.getAll();

		if (atualizacoes.isEmpty()) {
			UltimaAtualizacaoVN ultimaAtualizacao = new UltimaAtualizacaoVN();
			ultimaAtualizacao.setUltimaAtualizacao(java.util.Calendar.getInstance().getTime());
			ultimaAtualizacao.setAno(ano);
			ultimaAtualizacaoService.create(ultimaAtualizacao);
		} else {
			for (UltimaAtualizacaoVN ultimaAtualizacaoVN : atualizacoes) {
				ultimaAtualizacaoVN.setUltimaAtualizacao(java.util.Calendar.getInstance().getTime());
				ultimaAtualizacaoVN.setAno(ano);
				ultimaAtualizacaoService.update(ultimaAtualizacaoVN);
			}
		}

	}
	
	
	@Transactional
	@Scheduled(cron = "0 0 11 * * *")
	@Scheduled(cron = "0 0 15 * * *")
	public void geraValorDeNegocioByAno2022() {
		String ano = "2022";
		System.out.println(ano + " anoooooooooo");

//		// VN ALL
		List<Producao> producoes = producaoService.getAllByAno(ano);

		// VN BY EQUIPE
//		List<BalanceteCalculo> balancetes = balanceteService.getAllBalanceteByAno(ano);
//		List<Producao> producoesA = producaoService.getAllProducaoByEquipeAndAno("2020", 411l);

		// VN BY PRODUCAO
		//List<Producao> producoes = new ArrayList<Producao>();
		//Producao prod = producaoService.findById(2130l);
		//producoes.add(prod);

		String prevBeneficioOut;
		BigDecimal teto = BigDecimal.valueOf(0);
		ValorNegocio vn = new ValorNegocio();

		for (Producao producao : producoes) {
			try {
				System.out.println("! ! ! --- Processando a campanha de ID:" + producao.getId() + " da Empresa: "
						+ producao.getContrato().getCliente().getRazaoSocial());

				Contrato contrato = producao.getContrato();

				if (producao.getProduto().getId() != 23l) {

					System.out.println("VN de Outros Produtos: " + producao.getContrato().getProduto().getNome());

					vn = getVNbyProd(producao.getId());
					if (Objects.isNull(vn)) {
						System.out.println("Valor de Neg�cio n�o encontrado, criando novo...");
						vn = null;
						vn = new ValorNegocio();
					}

					vn.setCampanha(producao.getAno());
					vn.setProducao(producao);
					vn.setTipoNegocio(producao.getTipoDeNegocio());
					vn.setCliente(producao.getContrato().getCliente());
					try {
						vn.setComercial(producao.getContrato().getComercialResponsavelEfetivo().getSigla());
					} catch (Exception e) {
						e.printStackTrace();
						vn.setComercial("Erro cadastral");
					}
					try {
						vn.setDivisao(producao.getContrato().getCliente().getDivisao().toString());
					} catch (Exception e) {
						e.printStackTrace();
						vn.setDivisao(null);
					}

					vn.setSituacao(producao.getSituacao().toString());
					vn.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
					// vn.setAnt_fat(ant_fat);
					vn.setCategoria(producao.getContrato().getCliente().getCategoria());
					vn.setCtrObs(producao.getContrato().getObsContrato());
					vn.setEquipe(producao.getEquipe().getNome());
					String estimativaComercial = producao.getContrato().getEstimativaComercial();
					estimativaComercial = estimativaComercial.replace(".", "");
					estimativaComercial = estimativaComercial.replace(",", ".");

					if (estimativaComercial.isEmpty()) {
						vn.setEstComercial(0.0f);
					} else {
						vn.setEstComercial(Float.parseFloat(estimativaComercial));
						vn.setValorBase(BigDecimal.valueOf(Float.parseFloat(estimativaComercial)));
					}

					vn.setModoValor("Comercial");
					vn.setSigla(producao.getConsultor().getSigla());

					continue;

				}

				// Reseta variaveis tempor�rias
				BigDecimal valorNegocio = BigDecimal.valueOf(0);

				vn = getVNbyProd(producao.getId());
				if (Objects.isNull(vn)) {
					System.out.println("Valor de Neg�cio n�o encontrado, criando novo...");
					vn = null;
					vn = new ValorNegocio();

				} else {
					if (vn.getModoValor().equalsIgnoreCase("Manual")) {
						System.out.println("Achou manual e n�o prosseguiu");
						vn.setCampanha(producao.getAno());
						vn.setProducao(producao);
						vn.setTipoNegocio(producao.getTipoDeNegocio());
						vn.setCliente(producao.getContrato().getCliente());
						vn.setSituacao(producao.getSituacao().toString());
						vn.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
						// vn.setAnt_fat(ant_fat);
						vn.setCategoria(producao.getContrato().getCliente().getCategoria());
						vn.setCtrObs(producao.getContrato().getObsContrato());
						vn.setEquipe(producao.getEquipe().getNome());
						vn.setSigla(producao.getConsultor().getSigla());	
						//atualizar o endere�o do cliente
						List<EnderecoCliente> enderecos = enderecoClienteRepository.getAllById(vn.getCliente().getId());
						if(Objects.nonNull(enderecos)) {
							boolean matriz = false;
							
							for (EnderecoCliente endereco : enderecos) {
								if(endereco.getTipo().equalsIgnoreCase("matriz")) {
									matriz = true;
									vn.setCidade(endereco.getCidade());
									vn.setEstado(endereco.getEstado());
								}
							}
							
							if(matriz == false && enderecos.size() > 0) {
								vn.setCidade(enderecos.get(0).getCidade());
								vn.setEstado(enderecos.get(0).getEstado());
							}
						}
						
						update(vn);
						
						continue;
					}
				}
				
				
				
				
				List<BalanceteCalculo> balancetesProducoes = balanceteService
						.getBalanceteCalculoByProducao(producao.getId());

				float beneficio = 0;

				BalanceteCalculo balanceteLast = new BalanceteCalculo();
				if (balancetesProducoes.size() > 0) {
					balanceteLast.setVersao(0);
					for (BalanceteCalculo balanceteCalculo : balancetesProducoes) {
						if (balanceteCalculo.getVersao() > balanceteLast.getVersao()) {
							balanceteLast = balanceteCalculo;
						}
						if (!balanceteCalculo.getReducaoImposto().isEmpty()) {
							beneficio += Float.parseFloat(
									balanceteCalculo.getReducaoImposto().replace(".", "").replace(",", "."));
						}
					}
				} else {
					System.out.println("Sem Balancete, deixando Last = NULL");
					balanceteLast = null;
				}

				List<Honorario> honorarios = contrato.getHonorarios();
				for (Honorario honorario : honorarios) {

					// Tem balancete?
					if (Objects.nonNull(balanceteLast)) {
						System.out.println("Produ��o Possui Balancete, Verificando honor�rios...");

						// Valor Fixo
						if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
							System.out.println("Valor Fixo");

							ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
							if (Objects.nonNull(fixo)) {

								BigDecimal valorFixo = fixo.getValor();
								if (fixo.isValorTrimestral()) {
									valorFixo = valorFixo.multiply(BigDecimal.valueOf(4));
								} else if (fixo.isValorMensal()) {
									valorFixo = valorFixo.multiply(BigDecimal.valueOf(12));
								}

								valorNegocio = valorNegocio.add(valorFixo);
								if (balanceteLast.isVersaoFinal()) {
									vn.setModoValor("Finalizado");
								} else {
									vn.setModoValor("Real");
								}
							}
						}
						// Percentual Escalonado
						else if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
							System.out.println("Percentual Escalonado");

							PercentualEscalonado pe = honorarioService
									.getPercentualEscalonadoByContrato(contrato.getId());

							String stringValorBalancete = "0,00";

							if (producao.getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
								stringValorBalancete = balanceteLast.getReducaoImposto().replace(".", "")
										.replace(",", ".").trim();
							} else {
								System.out.println("Valor consolidado da produ��o: " + beneficio);
								stringValorBalancete = String.valueOf(beneficio);
							}

							if (Objects.nonNull(pe)) {
								if (stringValorBalancete == "" || stringValorBalancete == null
										|| stringValorBalancete.isEmpty()) {
									System.out.println("balancete vazio");
									stringValorBalancete = "0.0";
								} else {
									System.out.println(
											"Balancete n�o identificado como vazio, valor: " + stringValorBalancete);
								}

								teto = pe.getLimitacao();
								List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

								BigDecimal valorCalc = BigDecimal.valueOf(0);

								System.out.println("STRING BALANCETE -----> " + stringValorBalancete);

								BigDecimal balancete = new BigDecimal(stringValorBalancete);
								BigDecimal temp = new BigDecimal(stringValorBalancete);

								for (FaixasEscalonado faixa : faixas) {

									System.out.println("Temp atual " + temp);

									// Define vari�veis
									BigDecimal inicial = faixa.getValorInicial();
									BigDecimal finalF = faixa.getValorFinal();
									BigDecimal gap1 = finalF.subtract(inicial);
									// BigDecimal gap2 = temp.subtract(inicial);
									Float percentual = faixa.getValorPercentual();

									System.out.println("Faixa: " + inicial + " ... " + finalF);
									System.out.println("Diferen�a: " + gap1);

									// Se � maior que o final, calcula direto com o gap1
									if (temp.compareTo(BigDecimal.ZERO) != 0) {
										if (temp.compareTo(gap1) >= 0) {

											System.out.println("Passa direto da faixa, calcula com o f...");

											valorCalc = valorCalc.add(gap1.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

											System.out.println("Temp ap�s iter�a�o " + temp);
											System.out.println("Saldo para soma :" + gap1.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											// Se est� entre a faixa, calcula com o temp direto na porcentagem.
										} else if (temp.compareTo(gap1) < 0) {

											valorCalc = valorCalc.add(temp.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											BigDecimal zero = BigDecimal.ZERO;
											temp = zero;
										}
									}
								}

								// Se possui acima de, aplica.
								if (pe.getAcimaDe() != null) {
									System.out.println(
											"Possui 'Acima de' calculando: " + temp + " x " + pe.getPercentAcimaDe());
									if (balancete.compareTo(pe.getAcimaDe()) > 0) {
										if (temp.compareTo(BigDecimal.ZERO) > 0) {
											BigDecimal acima = temp.multiply(BigDecimal.valueOf(pe.getPercentAcimaDe())
													.divide(new BigDecimal(100)));
											valorCalc = valorCalc.add(acima);
										}
									}
								} else {
									System.out.println("N�o identificou 'Acima de'");
								}

								// Soma o C�lculo de Percentual Escalonado ao VN
								valorNegocio = valorNegocio.add(valorCalc);

								// Se possui limita��o, aplica.
								if (pe.getLimitacao() != null) {
									if (pe.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
										System.out.println("Possui limita��o de " + pe.getLimitacao());
										if (valorNegocio.compareTo(pe.getLimitacao()) > 0) {
											System.out.println("VN maior que teto");

											valorNegocio = valorNegocio
													.valueOf(Double.valueOf(pe.getLimitacao().toString()));
											valorNegocio = valorNegocio.setScale(2, BigDecimal.ROUND_HALF_UP);

										}
									}
								}

								System.out.println("PE calculado de: " + valorCalc);

								if (balanceteLast.isVersaoFinal()) {
									System.out.println("Finalizou");
									vn.setModoValor("Finalizado");
								} else {
									System.out.println("Realizou");
									vn.setModoValor("Real");
								}
							}
							// Percentual Fixo
						} else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
							PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(contrato.getId());
							System.out.println("Percentual Fixo");

							if (Objects.nonNull(fixo)) {

								String stringValorBalancete = "0,00";

								if (producao.getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
									stringValorBalancete = balanceteLast.getReducaoImposto().replace(".", "")
											.replace(",", ".").trim();
								} else {
									System.out.println("Valor consolidado da produ��o: " + beneficio);
									stringValorBalancete = String.valueOf(beneficio);
								}

								if (stringValorBalancete == "" || stringValorBalancete == null
										|| stringValorBalancete.isEmpty()) {
									System.out.println("balancete vazio");
									stringValorBalancete = "0,0";
								} else {
									System.out.println(
											"Balancete n�o identificado como vazio, valor: " + stringValorBalancete);
								}

								BigDecimal valorCalc = BigDecimal.valueOf(0);
								BigDecimal balancete = new BigDecimal(stringValorBalancete);

								System.out.println("!-!-! Valor a ser calculado: " + balancete + " x "
										+ fixo.getValorPercentual());

								valorCalc = balancete.multiply(
										BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

								// Soma o C�lculo de Percentual Escalonado ao VN
								valorNegocio = valorNegocio.add(valorCalc);

								// Se possui limita��o, aplica.
								if (fixo.getLimitacao() != null) {
									if (fixo.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
										System.out.println("Possui limita��o de " + fixo.getLimitacao());
										if (valorNegocio.compareTo(fixo.getLimitacao()) > 0) {
											System.out.println("VN maior que teto");

											valorNegocio = valorNegocio
													.valueOf(Double.valueOf(fixo.getLimitacao().toString()));
											valorNegocio = valorNegocio.setScale(2, BigDecimal.ROUND_HALF_UP);

										}
									}
								}
								if (balanceteLast.isVersaoFinal()) {
									vn.setModoValor("Finalizado");
								} else {
									vn.setModoValor("Real");
								}
							}
						}
					} else {
						System.out.println("N�o tem balancete, gerando prev de benef�cio");

						// Pega a previs�o de Disp�ndio
						ItemValores itemPrevDispendio = itemValoresService.getPrevValorDispendio(producao.getId());
						// Pega a previs�o de Exclus�o
						ItemValores itemPrevEclusao = itemValoresService.getValorExclusao(producao.getId());
						// Pega al�quota
						ItemValores itemAli = itemValoresService.getAliquotaByProducao(producao.getId());

						if (Objects.nonNull(itemPrevDispendio) && Objects.nonNull(itemPrevEclusao)
								&& Objects.nonNull(itemAli)) {
							if (itemPrevDispendio.getValor().equalsIgnoreCase("")) {
								itemPrevDispendio.setValor("0,0");
							}

							// Converte Previs�o Disp�ndio para BigDecimal
							BigDecimal prevDispendio = new BigDecimal(0);
							if (!itemPrevDispendio.getValor().equals("")) {
								if (!itemPrevDispendio.getValor().equalsIgnoreCase("SELECIONE")) {
									if (!itemPrevDispendio.getValor().isEmpty()) {
										String strPrevDispendio = itemPrevDispendio.getValor();
										strPrevDispendio = strPrevDispendio.replace(".", "");
										strPrevDispendio = strPrevDispendio.replace(",", ".");
										strPrevDispendio = strPrevDispendio.trim();
										prevDispendio = new BigDecimal(strPrevDispendio);
									}
								}
							}

							System.out.println("Previs�o Disp�ndio: " + prevDispendio);

							// Converte Previs�o Exclus�o para int
							float prevExclusao = 0.0f;
							if (!itemPrevEclusao.getValor().equals("")) {
								if (!itemPrevEclusao.getValor().equalsIgnoreCase("SELECIONE")) {
									if (!itemPrevEclusao.getValor().isEmpty()) {
										String strPrevExclusao = itemPrevEclusao.getValor();
										strPrevExclusao = strPrevExclusao.replace("%", "");
										strPrevExclusao = strPrevExclusao.replace(",", ".");
										prevExclusao = Float.parseFloat(strPrevExclusao) / 100;
									}
								}
							}

							System.out.println("Previs�o Exclus�o: " + prevExclusao);

							// Converte Al�quota para int
							float aliquota = 0.0f;
							if (!itemAli.getValor().equals("")) {
								if (!itemAli.getValor().equalsIgnoreCase("SELECIONE")) {
									if (!itemAli.getValor().isEmpty()) {
										aliquota = Float.parseFloat(itemAli.getValor()) / 100;
									}
								}
							}
							System.out.println("Previs�o aliquota: " + aliquota);

							BigDecimal prevBeneficio = new BigDecimal(0.0);
							if (prevDispendio.compareTo(BigDecimal.ZERO) > 0 && prevExclusao > 0.0f
									&& aliquota > 0.0f) {
								prevBeneficio = prevDispendio.multiply(new BigDecimal(prevExclusao))
										.multiply(new BigDecimal(aliquota));
								vn.setPrevBeneficio(prevBeneficio.toString());
							} else {
								vn.setPrevBeneficio(null);
							}

							prevBeneficioOut = prevBeneficio.toString();
							System.out.println("Previs�o de Benef�cio c�lculado: " + prevBeneficioOut);

							if (prevBeneficio.compareTo(BigDecimal.ZERO) == 0) {
								System.out.println("Entrou em estimativa Comercial pelo beneficio ser 0");
								String estimativaComercial = producao.getContrato().getEstimativaComercial();

								if (estimativaComercial.isEmpty()) {
									estimativaComercial = "0,00";
								}

								estimativaComercial = estimativaComercial.replace(".", "");
								estimativaComercial = estimativaComercial.replace(",", ".");
								valorNegocio = BigDecimal.valueOf(Float.parseFloat(estimativaComercial));
								vn.setModoValor("Comercial");
							} else {
								// Estimado Valor Fixo
								if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
									System.out.println("Entrou valor fixo");
									ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
									if (Objects.nonNull(fixo)) {

										BigDecimal valorFixo = fixo.getValor();

										valorNegocio = valorNegocio.add(valorFixo);
										vn.setModoValor("Estimado");

									}
								}
								// Estimado Percentual Escalonado
								else if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
									System.out.println("Entrou PE");
									PercentualEscalonado pe = honorarioService
											.getPercentualEscalonadoByContrato(contrato.getId());

									BigDecimal stringValorBalancete = prevBeneficio;
									if (Objects.nonNull(pe)) {
										System.out.println("PE N�o � Nulo");

										vn.setModoValor("Estimado");

										teto = pe.getLimitacao();
										List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

										BigDecimal valorCalc = BigDecimal.valueOf(0);

										BigDecimal balancete = stringValorBalancete;
										BigDecimal temp = stringValorBalancete;

										try {

											for (FaixasEscalonado faixa : faixas) {

												System.out.println("Temp atual " + temp);

												// Define vari�veis
												BigDecimal inicial = faixa.getValorInicial();
												BigDecimal finalF = faixa.getValorFinal();
												BigDecimal gap1 = finalF.subtract(inicial);
												// BigDecimal gap2 = temp.subtract(inicial);
												Float percentual = faixa.getValorPercentual();

												System.out.println("Faixa: " + inicial + " ... " + finalF);
												System.out.println("Diferen�a: " + gap1);

												// Se � maior que o final, calcula direto com o gap1
												if (temp.compareTo(BigDecimal.ZERO) != 0) {
													if (temp.compareTo(gap1) >= 0) {

														System.out.println("Passa direto da faixa, calcula com o f...");

														valorCalc = valorCalc.add(gap1.multiply(BigDecimal
																.valueOf(percentual).divide(new BigDecimal(100))));

														temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

														System.out.println("Saldo para soma :"
																+ gap1.multiply(BigDecimal.valueOf(percentual)
																		.divide(new BigDecimal(100))));

														// Se est� entre a faixa, calcula com o temp direto na
														// porcentagem.
													} else if (temp.compareTo(gap1) < 0) {

														valorCalc = valorCalc.add(temp.multiply(BigDecimal
																.valueOf(percentual).divide(new BigDecimal(100))));

														BigDecimal zero = BigDecimal.ZERO;
														temp = zero;
													}
												}
											}

											System.out.println("PE calculado com o valor de " + valorCalc);

											// Se possui acima de, aplica.
											if (pe.getAcimaDe() != null) {
												System.out.println("Possui 'Acima de' calculando: " + temp + " x "
														+ pe.getPercentAcimaDe());
												if (balancete.compareTo(pe.getAcimaDe()) > 0) {
													if (temp.compareTo(BigDecimal.ZERO) > 0) {
														BigDecimal acima = temp
																.multiply(BigDecimal.valueOf(pe.getPercentAcimaDe())
																		.divide(new BigDecimal(100)));
														valorCalc = valorCalc.add(acima);
													}
												}
											} else {
												System.out.println("N�o identificou 'Acima de'");
											}

											// Soma o C�lculo de Percentual Escalonado ao VN
											valorNegocio = valorNegocio.add(valorCalc);

											// Se possui limita��o, aplica.
											if (pe.getLimitacao() != null) {
												if (pe.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
													System.out.println("Possui limita��o de " + pe.getLimitacao());
													if (valorNegocio.compareTo(pe.getLimitacao()) > 0) {
														System.out.println("VN maior que teto");

														valorNegocio = valorNegocio
																.valueOf(Double.valueOf(pe.getLimitacao().toString()));
														valorNegocio = valorNegocio.setScale(2,
																BigDecimal.ROUND_HALF_UP);

													}
												}
											}

										} catch (Exception e) {
											e.printStackTrace();
											valorNegocio = valorNegocio.add(valorCalc);
											vn.setModoValor("Erro cadastral no Contrato");
											vn.setValorBase(BigDecimal.valueOf(0));
										}
									}
									// Estimado Percentual Fixo
								} else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
									System.out.println("Entrou PF");
									PercentualFixo fixo = honorarioService
											.getPercentualFixoByContrato(contrato.getId());

									if (Objects.nonNull(fixo)) {

										BigDecimal valorCalc = BigDecimal.valueOf(0);

										if (fixo.getValorPercentual() == null) {
											vn.setModoValor("Erro cadastral no Contrato");
											fixo.setValorPercentual(0.0f);
										}

										BigDecimal balancete = prevBeneficio;
										valorCalc = balancete.multiply(BigDecimal.valueOf(fixo.getValorPercentual())
												.divide(new BigDecimal(100)));

										// Soma o C�lculo de Percentual Escalonado ao VN
										valorNegocio = valorNegocio.add(valorCalc);

										// Se possui limita��o, aplica.
										if (fixo.getLimitacao() != null) {
											if (fixo.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
												System.out.println("Possui limita��o de " + fixo.getLimitacao());
												if (valorNegocio.compareTo(fixo.getLimitacao()) > 0) {
													System.out.println("VN maior que teto");

													valorNegocio = valorNegocio
															.valueOf(Double.valueOf(fixo.getLimitacao().toString()));
													valorNegocio = valorNegocio.setScale(2, BigDecimal.ROUND_HALF_UP);

												}
											}
										}
										vn.setModoValor("Estimado");
									}
								}
							}
						}
					}

					ItemValores itemPrevEclusao = itemValoresService.getValorExclusao(producao.getId());
					String prevExclusao = "0";
					if (Objects.nonNull(itemPrevEclusao)) {
						if (!itemPrevEclusao.getValor().equals("")) {
							if (!itemPrevEclusao.getValor().equalsIgnoreCase("SELECIONE")) {
								if (!itemPrevEclusao.getValor().isEmpty()) {
									String strPrevExclusao = itemPrevEclusao.getValor();
									prevExclusao = strPrevExclusao;
								}
							}
						}
					}

					vn.setValorBase(valorNegocio);
					vn.setCampanha(producao.getAno());
					vn.setProducao(producao);
					vn.setTipoNegocio(producao.getTipoDeNegocio());
					vn.setCliente(producao.getContrato().getCliente());
					
					//atualizar o endere�o do cliente
					
					List<EnderecoCliente> enderecos = enderecoClienteRepository.getAllById(vn.getCliente().getId());
					System.out.println("o tamanho da lista �: " + enderecos.size());
					
					if(Objects.nonNull(enderecos)) {
						boolean matriz = false;
						
						for (EnderecoCliente endereco : enderecos) {
							if(endereco.getTipo() != null) {
								if(endereco.getTipo().equalsIgnoreCase("matriz")) {
									matriz = true;
									vn.setCidade(endereco.getCidade());
									vn.setEstado(endereco.getEstado());
								}
							}
							
						}
						
						if(matriz == false && enderecos.size() > 0) {
							vn.setCidade(enderecos.get(0).getCidade());
							vn.setEstado(enderecos.get(0).getEstado());
						}
					}
					
					try {
						vn.setComercial(producao.getContrato().getComercialResponsavelEfetivo().getSigla());
					} catch (Exception e) {
						e.printStackTrace();
						vn.setComercial("Erro cadastral");
					}
					try {
						vn.setDivisao(producao.getContrato().getCliente().getDivisao().toString());
					} catch (Exception e) {
						//e.printStackTrace();
						vn.setDivisao(null);
					}

					vn.setExclPrev(prevExclusao);
					if (Objects.nonNull(balanceteLast)) {
						vn.setExclReal(balanceteLast.getExclusaoUtilizada());
					} else {
						vn.setExclReal(null);
					}

					String estimativaComercial = producao.getContrato().getEstimativaComercial();
					estimativaComercial = estimativaComercial.replace(".", "");
					estimativaComercial = estimativaComercial.replace(",", ".");

					if (estimativaComercial.isEmpty()) {
						vn.setEstComercial(0.0f);
					} else {
						vn.setEstComercial(Float.parseFloat(estimativaComercial));
					}

					EtapasPagamento timing = producao.getContrato().getEtapasPagamento();

					// SYSOUT PARA VER VALOR ATUAL E ENTRADA DE CONDI��ES !!!!
					if (Objects.nonNull(timing)) {

						float economico = 0;
						float submissao = 0;
						float mctic = 0;

						// Balancete
						if (Objects.nonNull(timing.getEtapa1()) && timing.getEtapa1()!=null) {
							System.out.println("Entrou EC etapa 1 "+timing.getEtapa1().getId());
							if (timing.getEtapa1().getId() == 66l) {
								economico = timing.getPorcentagem1();
								System.out.println(economico + " ec1");
								vn.setCtrPcDe(economico);
							}
						}
						if (Objects.nonNull(timing.getEtapa2()) && timing.getEtapa2()!=null) {
							System.out.println("Entrou EC etapa 2 "+timing.getEtapa2().getId());
							if (timing.getEtapa2().getId() == 66l) {
								economico = timing.getPorcentagem2();
								System.out.println(economico + " ec2");
								vn.setCtrPcDe(economico);
							}
						}
						if (Objects.nonNull(timing.getEtapa3()) && timing.getEtapa3()!=null) {
							System.out.println("Entrou EC etapa 3 "+timing.getEtapa3().getId());
							if (timing.getEtapa3().getId() == 66l) {
								economico = timing.getPorcentagem3();
								System.out.println(economico + " ec3");
								vn.setCtrPcDe(economico);
							}
						}

						// Submiss�o
						if (Objects.nonNull(timing.getEtapa1()) && timing.getEtapa1()!=null) {
							System.out.println("Entrou sub etapa 1 "+timing.getEtapa1().getId());
							if (timing.getEtapa1().getId() == 68l) {
								submissao = timing.getPorcentagem1();
								System.out.println(submissao + " sub1");
								vn.setCtrPcSm(submissao);
							}
						}
						if (Objects.nonNull(timing.getEtapa2()) && timing.getEtapa2()!=null) {
							System.out.println("Entrou sub etapa 2 "+timing.getEtapa2().getId());
							if (timing.getEtapa2().getId() == 68l) {
								submissao = timing.getPorcentagem2();
								System.out.println(submissao + " sub2");
								vn.setCtrPcSm(submissao);
							}
						}
						if (Objects.nonNull(timing.getEtapa3()) && timing.getEtapa3()!=null) {
							System.out.println("Entrou sub etapa 3 "+timing.getEtapa3().getId());
							if (timing.getEtapa3().getId() == 68l) {
								submissao = timing.getPorcentagem3();
								System.out.println(submissao + " sub3");
								vn.setCtrPcSm(submissao);
							}
						}

						// MCTIC
						if (Objects.nonNull(timing.getEtapa1()) && timing.getEtapa1()!=null) {
							System.out.println("Entrou mcti etapa 1 "+timing.getEtapa1().getId());
							if (timing.getEtapa1().getId() == 70l) {
								mctic = timing.getPorcentagem1();
								vn.setCtrPcRm(mctic);
							}
						}
						if (Objects.nonNull(timing.getEtapa2()) && timing.getEtapa2()!=null) {
							System.out.println("Entrou mcti etapa 2 "+timing.getEtapa2().getId());
							if (timing.getEtapa2().getId() == 70l) {
								mctic = timing.getPorcentagem2();
								vn.setCtrPcRm(mctic);
							}
						}
						if (Objects.nonNull(timing.getEtapa3()) && timing.getEtapa3()!=null) {
							System.out.println("Entrou mcti etapa 3 "+timing.getEtapa3().getId());
							if (timing.getEtapa3().getId() == 70l) {
								mctic = timing.getPorcentagem3();
								vn.setCtrPcRm(mctic);
							}
						}

						List<ContatoCliente> contatos = ContatoClienteService
								.getAllById(producao.getContrato().getCliente().getId());

						boolean verifyFidelization = false;
						for (ContatoCliente contato : contatos) {
							if (contato.isPesquisaSatisfacao()) {
								verifyFidelization = true;
							}
						}

						if (verifyFidelization == true) {
							vn.setFidelizado(true);
						} else {
							vn.setFidelizado(false);
						}

//						vn.setCtrPcDe(economico);
//						vn.setCtrPcSm(submissao);
//						vn.setCtrPcRm(mctic);
						if (producao.getContrato().getCliente().getSetorAtividade() != null) {
							vn.setSetor(producao.getContrato().getCliente().getSetorAtividade().toString());
						} else {
							vn.setSetor("");
						}
					}

					ItemValores prevRelatorios = itemValoresService.getPrevisaoDeRelatoriosByProducao(producao.getId());
					if (Objects.nonNull(prevRelatorios)) {
						if (prevRelatorios.getValor().matches("-?\\d+(\\.\\d+)?")) {
							vn.setPrevRelatorios(prevRelatorios.getValor());
						} else {
							vn.setPrevRelatorios("0");
						}
					} else {
						vn.setPrevRelatorios("0");
					}

					vn.setSigla(producao.getConsultor().getSigla());

					ItemValores formPeD = itemValoresService.getStatusFormByProducao(producao.getId());
					ItemValores submissaoItem = itemValoresService.getDataSubmissaoByProducao(producao.getId());

					if (Objects.nonNull(formPeD)) {
						if (!formPeD.getValor().equalsIgnoreCase("Pendente")
								|| !formPeD.getValor().equalsIgnoreCase("ENVIADO AO CLIENTE")
								|| !formPeD.getValor().equalsIgnoreCase("RECEBIDO CLIENTE")) {
							vn.setFormPeD(false);
						} else {
							vn.setFormPeD(true);
						}
					}

					if (Objects.nonNull(submissaoItem)) {
						if (submissaoItem.getValor() == "" || submissaoItem.getValor().isEmpty()) {
							vn.setSubmissao(false);
						} else {
							vn.setSubmissao(true);
						}
					}
				}

				List<Faturamento> fats = faturamentoService.getAllByProducao(producao.getId());
				BigDecimal fatReal = new BigDecimal(0);

				if (Objects.nonNull(fats)) {
					System.out.println("Quantidade de faturamentos encontrados: " + fats.size());
					for (Faturamento faturamento : fats) {
						System.out.println("Soma fat");
						fatReal = fatReal.add(faturamento.getValorEtapa());
						System.out.println("Valor de Fat somado: " + fatReal);
					}
				}

				ItemValores previsaoBalancete = itemValoresService.getPrevisaoBalanceteByProducao(producao.getId());
				if (Objects.nonNull(previsaoBalancete)) {
					vn.setPreEntregaCalc(previsaoBalancete.getValor());
				}

				String prevDispendioConvert = itemValoresService.getPrevValorDispendio(producao.getId()).getValor();
				prevDispendioConvert = prevDispendioConvert.replace(".", "");
				prevDispendioConvert = prevDispendioConvert.replace(",", ".");
				prevDispendioConvert = prevDispendioConvert.trim();

				if (!prevDispendioConvert.isEmpty()) {
					vn.setPrevDespesa(Float.parseFloat(prevDispendioConvert));
				}

				vn.setFatReal(fatReal.floatValue());

				if (vn.getProducao() != null) {
					if(vn.getModoValor().equalsIgnoreCase("estimado")) {
						ItemValores itemDispendio = itemValoresService.findByTarefaItemTarefaAndProducao(142l,producao.getId(), 323l);
						ItemValores itemExclusao = itemValoresService.findByTarefaItemTarefaAndProducao(142l, producao.getId(), 324l);
						ItemValores itemAliquota = itemValoresService.findByTarefaItemTarefaAndProducao(142l, producao.getId(), 368l);
						if(itemDispendio.getValor() != "") {
							System.out.println("encontrou um �tem com o valor preenchido");
							
							String dispendioFormatado = itemDispendio.getValor().replaceAll("\\.", "");
							System.out.println("O VALOR FORMATADO �: " + dispendioFormatado);
							dispendioFormatado = dispendioFormatado.replace(",", ".");
							System.out.println("O VALOR FORMATADO �: " + dispendioFormatado);
							vn.setDispendioAtual(Float.parseFloat(dispendioFormatado));
							//vn.setDispendioAtual(BigDecimal.valueOf(Double.parseDouble(itemDispendio.getValor())));
							System.out.println("o valor do �tem �: " + vn.getDispendioAtual() + " " + itemDispendio.getValor());
						}else {
							System.out.println("encontrou um �tem com o valor vazio");
							System.out.println("o valor do �tem �: " + itemDispendio.getValor());
							vn.setDispendioAtual(0.0f);
							//vn.setDispendioAtual(BigDecimal.ZERO);
						}
						
						
						
						if(!itemExclusao.getValor().equalsIgnoreCase("selecione") || itemExclusao.getValor() != "") {
							vn.setExclusaoAtual(Float.parseFloat(itemExclusao.getValor().substring(0, itemExclusao.getValor().length() - 1)));
							System.out.println("O VALOR DA EXCLUS�O ATUAL �: " + vn.getExclusaoAtual());
						}else {
							vn.setExclusaoAtual(0.0f);
							
						}
						if(!itemAliquota.getValor().equalsIgnoreCase("selecione") || itemAliquota.getValor() != "") {
							vn.setAliquotaAtual(Float.parseFloat(itemAliquota.getValor()));
						}else {
							vn.setAliquotaAtual(0.0f);
						}
						
						vn.setValorCalculo(vn.getDispendioAtual() * (vn.getExclusaoAtual() / 100 )* (vn.getAliquotaAtual() / 100 ));
						System.out.println("O VALOR DO CALCULO ATUAL E: " + vn.getValorCalculo());
						
					}else if (vn.getModoValor().equalsIgnoreCase("real") || vn.getModoValor().equalsIgnoreCase("finalizado")) {
						
						ItemValores itemAliquota = itemValoresService.findByTarefaItemTarefaAndProducao(142l, producao.getId(), 368l);
						
						float aliquotaAtual = 0.0f;
						if(Objects.nonNull(itemAliquota) || !itemAliquota.getValor().equalsIgnoreCase("selecione") || !itemAliquota.getValor().equalsIgnoreCase("") || !itemAliquota.getValor().isEmpty()) {
							System.out.println("O �TEM VALOR � :" + itemAliquota.getValor());
							System.out.println("O ID DO �TEM � : " + itemAliquota.getId());
							try {
								aliquotaAtual = Float.parseFloat(itemAliquota.getValor());	
							}catch (Exception e) {
								
							}
							
						}else {
							aliquotaAtual = 0.0f;
						}
						
						vn.setAliquotaAtual(aliquotaAtual);
						
						if(Objects.nonNull(producao.getTipoDeApuracao())) {
							if(producao.getTipoDeApuracao().equals(TipoDeApuracao.ANUAL)) {
								List<BalanceteCalculo> balancetes = balanceteService.getBalancetesNaoPrejuizoByProducao(producao.getId());
								
								String exclusaoUtilizada = "";
								Float dispendioAtual = 0.0f;
								if(Objects.nonNull(balancetes) && balancetes.size() > 0) {
									System.out.println("o tamanho do array �: " + balancetes.size());
									
									 exclusaoUtilizada = balancetes.get(balancetes.size() - 1).getExclusaoUtilizada();
								}
								
								if(Objects.nonNull(balancetes) && balancetes.size() > 0) {
									System.out.println("o tamanho do array �: " + balancetes.size());
									String dispendioAtualString;
									dispendioAtualString = balancetes.get(balancetes.size() - 1).getValorTotal();
									dispendioAtualString = dispendioAtualString.replaceAll("\\.", "");
									dispendioAtualString = dispendioAtualString.replace(",",".");
									dispendioAtual = Float.parseFloat(dispendioAtualString); 
									vn.setDispendioAtual(dispendioAtual);
								}
								
								if(Objects.nonNull(exclusaoUtilizada) || !exclusaoUtilizada.equalsIgnoreCase("selecione") || !exclusaoUtilizada.equalsIgnoreCase("") || !exclusaoUtilizada.isEmpty()) {
									
									
									if(exclusaoUtilizada.equalsIgnoreCase("sessenta")) {
										vn.setExclusaoAtual(60.0f);
									}else if(exclusaoUtilizada.equalsIgnoreCase("setenta")) {
										vn.setExclusaoAtual(70.0f);
									}else if(exclusaoUtilizada.equalsIgnoreCase("oitenta")) {
										vn.setExclusaoAtual(80.0f);
									}
								}else {
									vn.setExclusaoAtual(0.0f);
								}
								
								vn.setValorCalculo(vn.getDispendioAtual() * (vn.getExclusaoAtual() / 100 )* (vn.getAliquotaAtual() / 100 ));
								
							}else {
								List<BalanceteCalculo> balancetes = balanceteService.getBalancetesNaoPrejuizoByProducao(producao.getId());
								String exclusaoUtilizada = "";
								if(Objects.nonNull(balancetes) && balancetes.size() > 0) {
									System.out.println("o tamanho do array �: " + balancetes.size());
									 exclusaoUtilizada = balancetes.get(balancetes.size() - 1).getExclusaoUtilizada();
								}
								
								if(Objects.nonNull(exclusaoUtilizada) || !exclusaoUtilizada.equalsIgnoreCase("selecione") || !exclusaoUtilizada.equalsIgnoreCase("") || !exclusaoUtilizada.isEmpty()) {
									
									
									if(exclusaoUtilizada.equalsIgnoreCase("sessenta")) {
										vn.setExclusaoAtual(60.0f);
									}else if(exclusaoUtilizada.equalsIgnoreCase("setenta")) {
										vn.setExclusaoAtual(70.0f);
									}else if(exclusaoUtilizada.equalsIgnoreCase("oitenta")) {
										vn.setExclusaoAtual(80.0f);
									}
								}else {
									vn.setExclusaoAtual(0.0f);
								}
								Float dispendioAtual = 0.0f;
								for (BalanceteCalculo balancete : balancetes) {
									String valorTotalString = balancete.getValorTotal().replaceAll("\\.", "");
									valorTotalString = valorTotalString.replace(",", ".");
									dispendioAtual += Float.parseFloat(valorTotalString);
								}
								vn.setDispendioAtual(dispendioAtual);
								vn.setValorCalculo(vn.getDispendioAtual() * (vn.getExclusaoAtual() / 100 )* (vn.getAliquotaAtual() / 100 ));
								System.out.println("O VALOR DO CALCULO ATUAL E: " + vn.getValorCalculo());
							}
						}
						
						
					} else if(vn.getModoValor().equalsIgnoreCase("comercial")) {
						ItemValores itemDispendio = itemValoresService.findByTarefaItemTarefaAndProducao(142l,producao.getId(), 323l);
						
						if(itemDispendio.getValor() != "") {
							System.out.println("encontrou um �tem com o valor preenchido");
							
							String dispendioFormatado = itemDispendio.getValor().replaceAll("\\.", "");
							dispendioFormatado = dispendioFormatado.replace(",", ".");
							vn.setDispendioAtual(Float.parseFloat(dispendioFormatado));
							vn.setValorCalculo(Float.parseFloat(dispendioFormatado));
							
						}else {
							vn.setDispendioAtual(0.0f);
							vn.setValorCalculo(0.0f);
						}
					}
					
					// this.em.detach(vn);
					vn.setSituacao(producao.getSituacao().toString());
					vn.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
					// vn.setAnt_fat(ant_fat);
					vn.setCategoria(producao.getContrato().getCliente().getCategoria());
					vn.setCtrObs(producao.getContrato().getObsContrato());
					vn.setEquipe(producao.getEquipe().getNome());

					if (vn.getModoValor().equalsIgnoreCase("Real") || vn.getModoValor().equalsIgnoreCase("Estimado")) {
						String modo = checkBalancetes(vn);
						if (!modo.isEmpty()) {
							vn.setModoValor(modo);
						}
					}

					vn.setValorBase(vn.getValorBase().setScale(2, RoundingMode.CEILING));
					
					vn.setPrevBeneficio(atualizaPrevBeneficio(vn));
					update(vn);

					if (vn.getModoValor().equalsIgnoreCase("Real")) {
						swapToEstimativa(vn);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
				vn.setCliente(producao.getContrato().getCliente());
				vn.setModoValor("Erro ao gerar VN");
				this.em.detach(vn);
				update(vn);
			}
		}

		List<UltimaAtualizacaoVN> atualizacoes = ultimaAtualizacaoService.getAll();

		if (atualizacoes.isEmpty()) {
			UltimaAtualizacaoVN ultimaAtualizacao = new UltimaAtualizacaoVN();
			ultimaAtualizacao.setUltimaAtualizacao(java.util.Calendar.getInstance().getTime());
			ultimaAtualizacao.setAno(ano);
			ultimaAtualizacaoService.create(ultimaAtualizacao);
		} else {
			for (UltimaAtualizacaoVN ultimaAtualizacaoVN : atualizacoes) {
				ultimaAtualizacaoVN.setUltimaAtualizacao(java.util.Calendar.getInstance().getTime());
				ultimaAtualizacaoVN.setAno(ano);
				ultimaAtualizacaoService.update(ultimaAtualizacaoVN);
			}
		}

	}
	
	

	public String checkBalancetes(ValorNegocio vn) {

		List<BalanceteCalculo> balancetes = balanceteService.getBalanceteCalculoByProducao(vn.getProducao().getId());
		String modo = "";
		boolean possuiVFinal = false;

		for (BalanceteCalculo balanceteCalculo : balancetes) {
			if (balanceteCalculo.isVersaoFinal()) {
				possuiVFinal = true;
			}
		}

		if (vn.getProducao().getTipoDeApuracao() == TipoDeApuracao.MENSAL) {
			if (balancetes.size() >= 12 && possuiVFinal == true) {
				modo = "Finalizado";
			}
		}
		if (vn.getProducao().getTipoDeApuracao() == TipoDeApuracao.TRIMESTRAL) {
			System.out.println("Entrou trimestral, vendo se bal size � =4 (" + balancetes.size());
			if (balancetes.size() >= 4 && possuiVFinal == true) {
				modo = "Finalizado";
			}
		}
		if (vn.getProducao().getTipoDeApuracao() == TipoDeApuracao.ANUAL) {
			if (balancetes.size() >= 1 && possuiVFinal == true) {
				modo = "Finalizado";
			}
		}

		return modo;

	}

	@Transactional
	public void swapToEstimativa(ValorNegocio vn) {

		Producao producao = vn.getProducao();

		System.out.println("Fazendo verifica��o de se Est.Tec > Real");
		BigDecimal valorNegocio = BigDecimal.valueOf(0);

		// Pega a previs�o de Disp�ndio
		ItemValores itemPrevDispendio = itemValoresService.getPrevValorDispendio(producao.getId());
		// Pega a previs�o de Exclus�o
		ItemValores itemPrevEclusao = itemValoresService.getValorExclusao(producao.getId());
		// Pega al�quota
		ItemValores itemAli = itemValoresService.getAliquotaByProducao(producao.getId());

		if (Objects.nonNull(itemPrevDispendio) && Objects.nonNull(itemPrevEclusao) && Objects.nonNull(itemAli)) {
			if (itemPrevDispendio.getValor().equalsIgnoreCase("")) {
				itemPrevDispendio.setValor("0,0");
			}

			// Converte Previs�o Disp�ndio para BigDecimal
			BigDecimal prevDispendio = new BigDecimal(0);
			if (!itemPrevDispendio.getValor().equals("")) {
				if (!itemPrevDispendio.getValor().equalsIgnoreCase("SELECIONE")) {
					if (!itemPrevDispendio.getValor().isEmpty()) {
						String strPrevDispendio = itemPrevDispendio.getValor();
						strPrevDispendio = strPrevDispendio.replace(".", "");
						strPrevDispendio = strPrevDispendio.replace(",", ".");
						strPrevDispendio = strPrevDispendio.trim();
						prevDispendio = new BigDecimal(strPrevDispendio);
					}
				}
			}

			System.out.println("Previs�o Disp�ndio: " + prevDispendio);

			// Converte Previs�o Exclus�o para int
			float prevExclusao = 0.0f;
			if (!itemPrevEclusao.getValor().equals("")) {
				if (!itemPrevEclusao.getValor().equalsIgnoreCase("SELECIONE")) {
					if (!itemPrevEclusao.getValor().isEmpty()) {
						String strPrevExclusao = itemPrevEclusao.getValor();
						strPrevExclusao = strPrevExclusao.replace("%", "");
						strPrevExclusao = strPrevExclusao.replace(",", ".");
						prevExclusao = Float.parseFloat(strPrevExclusao) / 100;
					}
				}
			}

			System.out.println("Previs�o Exclus�o: " + prevExclusao);

			// Converte Al�quota para int
			float aliquota = 0.0f;
			if (!itemAli.getValor().equals("")) {
				if (!itemAli.getValor().equalsIgnoreCase("SELECIONE")) {
					if (!itemAli.getValor().isEmpty()) {
						aliquota = Float.parseFloat(itemAli.getValor()) / 100;
					}
				}
			}
			System.out.println("Previs�o aliquota: " + aliquota);

			BigDecimal prevBeneficio = new BigDecimal(0.0);
			if (prevDispendio.compareTo(BigDecimal.ZERO) > 0 && prevExclusao > 0.0f && aliquota > 0.0f) {
				prevBeneficio = prevDispendio.multiply(new BigDecimal(prevExclusao)).multiply(new BigDecimal(aliquota));
				vn.setPrevBeneficio(prevBeneficio.toString());
			} else {
				vn.setPrevBeneficio(null);
			}

			String prevBeneficioOut = prevBeneficio.toString();
			System.out.println("Previs�o de Benef�cio c�lculado: " + prevBeneficioOut);

			if (prevBeneficio.compareTo(BigDecimal.ZERO) == 0) {
				String estimativaComercial = producao.getContrato().getEstimativaComercial();

				if (estimativaComercial.isEmpty()) {
					estimativaComercial = "0,00";
				}

				estimativaComercial = estimativaComercial.replace(".", "");
				estimativaComercial = estimativaComercial.replace(",", ".");
				valorNegocio = BigDecimal.valueOf(Float.parseFloat(estimativaComercial));
				vn.setModoValor("Comercial");
			} else {
				Contrato contrato = producao.getContrato();
				List<Honorario> honorarios = contrato.getHonorarios();
				for (Honorario honorario : honorarios) {

					if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
						PercentualEscalonado pe = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());

						BigDecimal stringValorBalancete = prevBeneficio;
						if (Objects.nonNull(pe)) {

							BigDecimal teto = pe.getLimitacao();
							List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();

							BigDecimal valorCalc = BigDecimal.valueOf(0);
							BigDecimal balancete = stringValorBalancete;
							BigDecimal temp = stringValorBalancete;

							try {

								for (FaixasEscalonado faixa : faixas) {

									System.out.println("Temp atual " + temp);

									// Define vari�veis
									BigDecimal inicial = faixa.getValorInicial();
									BigDecimal finalF = faixa.getValorFinal();
									BigDecimal gap1 = finalF.subtract(inicial);
									// BigDecimal gap2 = temp.subtract(inicial);
									Float percentual = faixa.getValorPercentual();

									System.out.println("Faixa: " + inicial + " ... " + finalF);
									System.out.println("Diferen�a: " + gap1);

									// Se � maior que o final, calcula direto com o gap1
									if (temp.compareTo(BigDecimal.ZERO) != 0) {
										if (temp.compareTo(gap1) >= 0) {

											System.out.println("Passa direto da faixa, calcula com o f...");

											valorCalc = valorCalc.add(gap1.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											temp = temp.subtract((gap1.add(BigDecimal.valueOf(0.1))));

											System.out.println("Saldo para soma :" + gap1.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											// Se est� entre a faixa, calcula com o temp direto na
											// porcentagem.
										} else if (temp.compareTo(gap1) < 0) {

											valorCalc = valorCalc.add(temp.multiply(
													BigDecimal.valueOf(percentual).divide(new BigDecimal(100))));

											BigDecimal zero = BigDecimal.ZERO;
											temp = zero;
										}
									}
								}

								// Se possui acima de, aplica.
								if (pe.getAcimaDe() != null) {
									System.out.println(
											"Possui 'Acima de' calculando: " + temp + " x " + pe.getPercentAcimaDe());
									if (balancete.compareTo(pe.getAcimaDe()) > 0) {
										if (temp.compareTo(BigDecimal.ZERO) > 0) {
											BigDecimal acima = temp.multiply(BigDecimal.valueOf(pe.getPercentAcimaDe())
													.divide(new BigDecimal(100)));
											valorCalc = valorCalc.add(acima);
										}
									}
								} else {
									System.out.println("N�o identificou 'Acima de'");
								}

								// Soma o C�lculo de Percentual Escalonado ao VN
								valorNegocio = valorNegocio.add(valorCalc);

								// Se possui limita��o, aplica.
								if (pe.getLimitacao().compareTo(new BigDecimal("0.00")) > 0) {
									System.out.println("Possui limita��o de " + pe.getLimitacao());
									if (valorNegocio.compareTo(pe.getLimitacao()) > 0) {
										System.out.println("VN maior que teto");

										valorNegocio = valorNegocio
												.valueOf(Double.valueOf(pe.getLimitacao().toString()));
										valorNegocio = valorNegocio.setScale(2, BigDecimal.ROUND_HALF_UP);

									}
								}

								vn.setModoValor("Estimado");

							} catch (Exception e) {
								e.printStackTrace();
								valorNegocio = valorNegocio.add(valorCalc);
								vn.setModoValor("Erro cadastral no Contrato");
								vn.setValorBase(BigDecimal.valueOf(0));
							}
						}

					} else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {
						PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(contrato.getId());

						if (Objects.nonNull(fixo)) {

							BigDecimal valorCalc = BigDecimal.valueOf(0);

							if (fixo.getValorPercentual() == null) {
								vn.setModoValor("Erro cadastral no Contrato");
								fixo.setValorPercentual(0.0f);
							}

							BigDecimal balancete = prevBeneficio;
							valorCalc = balancete.multiply(
									BigDecimal.valueOf(fixo.getValorPercentual()).divide(new BigDecimal(100)));

							if (fixo.getLimitacao() != null) {
								if (fixo.getLimitacao().compareTo(BigDecimal.ZERO) != 0) {
									if (valorCalc.compareTo(fixo.getLimitacao()) >= 0) {
										valorCalc = fixo.getLimitacao();
									}
								}
							}

							valorNegocio = valorNegocio.add(valorCalc);
							vn.setModoValor("Estimado");

						}
					} else if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
						ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
						if (Objects.nonNull(fixo)) {

							BigDecimal valorFixo = fixo.getValor();

							valorNegocio = valorNegocio.add(valorFixo);
							vn.setModoValor("Estimado");

						}
					}

					if (valorNegocio.compareTo(vn.getValorBase()) == 1) {
						vn.setValorBase(valorNegocio);
						vn.setModoValor("Estimado");
						update(vn);
					}
				}
			}
		}

	}

	public List<ValorNegocio> getAllByConsultor(Long idConsultor) {
		try {
			return this.em.createQuery("SELECT v FROM ValorNegocio v WHERE v.producao.consultor.id = :pId",
					ValorNegocio.class).setParameter("pId", idConsultor).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro no metodo: ValorNegocioRepositoryImpl.getAllByConsultor");
			return null;
		}
	}

	public List<ValorNegocio> getAllByConsultorAndNN2020(Long id, String campanha) {
		try {
			return this.em
					.createQuery("SELECT v FROM ValorNegocio v "
							+ "WHERE 	v.producao.contrato.comercialResponsavelEfetivo.id = :pId "
							+ "AND 		v.producao.ano = :pCampanha "
							+ "OR 		v.producao.contrato.comercialResponsavelEntrada.id = :pId "
							+ "AND 		v.producao.ano = :pCampanha", ValorNegocio.class)
					.setParameter("pId", id).setParameter("pCampanha", campanha).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro no metodo: ValorNegocioRepositoryImpl.getAllByConsultorAndNN2020");
			return null;
		}
	}

	public List<ValorNegocio> getAllByConsultor2020Finalizado(Long id, String campanha) {
		try {
			return this.em
					.createQuery(
							"SELECT v FROM ValorNegocio v "
									+ "WHERE 	v.producao.contrato.comercialResponsavelEfetivo.id = :pId "
									+ "AND 		v.producao.ano = :pCampanha " + "AND 		v.modoValor = 'finalizado' "
									+ "OR 		v.producao.contrato.comercialResponsavelEntrada.id = :pId "
									+ "AND 		v.producao.ano = :pCampanha " + "AND 		v.modoValor = 'finalizado'",
							ValorNegocio.class)
					.setParameter("pId", id).setParameter("pCampanha", campanha).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro no metodo: ValorNegocioRepositoryImpl.getAllByConsultor2020Finalizado \n" + e);
			return null;
		}
	}

	public List<ValorNegocio> getGerenciarValorNegocio(String cnpj, String nome_empresa) {
		System.out.println("entrou aqui repo:      ");
		System.out.println("entrou aqui repo cnpj:" + cnpj);
		System.out.println("entrou aqui repo nome_empresa:" + nome_empresa);
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<ValorNegocio> root = cq.from(ValorNegocio.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		if (nome_empresa != null) {
			predicates.add(qb.like(root.get("nome_empresa"), "%" + nome_empresa + "%"));
		}

		// if (cnpj != null) {
		// predicates.add(qb.equal(root.get("cnpj"), "%" + cnpj + "%"));
		// }

		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		return em.createQuery(cq).getResultList();

	}

	private String atualizaPrevBeneficio(ValorNegocio vn) {

		Producao producao = vn.getProducao();
		String prevBeneficioOut = "";

		// Pega a previs�o de Disp�ndio
		ItemValores itemPrevDispendio = itemValoresService.getPrevValorDispendio(producao.getId());
		// Pega a previs�o de Exclus�o
		ItemValores itemPrevEclusao = itemValoresService.getValorExclusao(producao.getId());
		// Pega al�quota
		ItemValores itemAli = itemValoresService.getAliquotaByProducao(producao.getId());

		if (Objects.nonNull(itemPrevDispendio) && Objects.nonNull(itemPrevEclusao) && Objects.nonNull(itemAli)) {
			if (itemPrevDispendio.getValor().equalsIgnoreCase("")) {
				itemPrevDispendio.setValor("0,0");
			}

			// Converte Previs�o Disp�ndio para BigDecimal
			BigDecimal prevDispendio = new BigDecimal(0);
			if (!itemPrevDispendio.getValor().equals("")) {
				if (!itemPrevDispendio.getValor().equalsIgnoreCase("SELECIONE")) {
					if (!itemPrevDispendio.getValor().isEmpty()) {
						String strPrevDispendio = itemPrevDispendio.getValor();
						strPrevDispendio = strPrevDispendio.replace(".", "");
						strPrevDispendio = strPrevDispendio.replace(",", ".");
						strPrevDispendio = strPrevDispendio.trim();
						prevDispendio = new BigDecimal(strPrevDispendio);
					}
				}
			}

			System.out.println("Previs�o Disp�ndio: " + prevDispendio);

			// Converte Previs�o Exclus�o para int
			float prevExclusao = 0.0f;
			if (!itemPrevEclusao.getValor().equals("")) {
				if (!itemPrevEclusao.getValor().equalsIgnoreCase("SELECIONE")) {
					if (!itemPrevEclusao.getValor().isEmpty()) {
						String strPrevExclusao = itemPrevEclusao.getValor();
						strPrevExclusao = strPrevExclusao.replace("%", "");
						strPrevExclusao = strPrevExclusao.replace(",", ".");
						prevExclusao = Float.parseFloat(strPrevExclusao) / 100;
					}
				}
			}

			System.out.println("Previs�o Exclus�o: " + prevExclusao);

			// Converte Al�quota para int
			float aliquota = 0.0f;
			if (!itemAli.getValor().equals("")) {
				if (!itemAli.getValor().equalsIgnoreCase("SELECIONE")) {
					if (!itemAli.getValor().isEmpty()) {
						aliquota = Float.parseFloat(itemAli.getValor()) / 100;
					}
				}
			}
			System.out.println("Previs�o aliquota: " + aliquota);

			BigDecimal prevBeneficio = new BigDecimal(0.0);
			if (prevDispendio.compareTo(BigDecimal.ZERO) > 0 && prevExclusao > 0.0f && aliquota > 0.0f) {
				prevBeneficio = prevDispendio.multiply(new BigDecimal(prevExclusao)).multiply(new BigDecimal(aliquota));
			} else {
			}

			prevBeneficioOut = prevBeneficio.toString();
			System.out.println("Previs�o de Benef�cio c�lculado: " + prevBeneficioOut);

		}

		return prevBeneficioOut;

	}

	// VN LEI DA INFORM�TICA
	@Transactional
	public void geraValorDeNegocioLIByAno(String ano) {
		System.out.println("geraValorDeNegocioLIByAno-ano: " + ano);

		long produtoId = 24;
		ValorNegocio vnLI = new ValorNegocio();

		// pegando todas as produ��es com produto lei da inform�tica
		List<Producao> producoes = producaoService.getAllProducaoByProdutoAndAno(produtoId, ano);
		System.out.println("tamanho lista getAllProducaoByProdutoAndAno " + producoes.size());

		for (Producao producao : producoes) {
			vnLI = getVNbyProd(producao.getId());
			Contrato contrato = producao.getContrato();
			System.out.println("contrato " + contrato.getId());
			
			// indentificando se j� existe valor de negocio para cada produ��o,se n�o
			// existe,cria vn para essa produ��o
			if (Objects.isNull(vnLI)) {
				System.out.println("N�O EXISTE VN PRODUTO LI");
				// cria vn para essa produ��o
				vnLI = null;
				vnLI = new ValorNegocio();
			}

			try {
				// VALOR EFETIVO MANUAL
				if (vnLI.getModoValor().equalsIgnoreCase("Manual")) {
					System.out.println("vn manual");
					System.out.println("vn manual vnLI.getValorBase()" + vnLI.getValorBase());

					// setar as informa��es do vn
					vnLI.setValorBase(vnLI.getValorBase());
					vnLI.setCampanha(producao.getAno());
					vnLI.setProducao(producao);
					vnLI.setTipoNegocio(producao.getTipoDeNegocio());
					vnLI.setCliente(producao.getContrato().getCliente());
					vnLI.setSituacao(producao.getSituacao().toString());
					vnLI.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
					vnLI.setCategoria(producao.getContrato().getCliente().getCategoria());
					vnLI.setCtrObs(producao.getContrato().getObsContrato());
					vnLI.setEquipe(producao.getEquipe().getNome());
					vnLI.setSigla(producao.getConsultor().getSigla());
					update(vnLI);
					continue;
				}
			} catch (Exception e) {
			}
			
			//variaveias teto(limita��o)
			BigDecimal tetoPercentualFixo = BigDecimal.valueOf(0);
			BigDecimal tetoEscalonado = BigDecimal.valueOf(0);

			// valor de cr�dito
			long tarefaIdCredito = 188;
			long itemTarefaIdCredito = 597;
			
			// valor do cr�dito 
			long itemTarefaIdDoCredito = 823;
			
			ItemValores valorCredito = itemValoresService.findByTarefaItemTarefaAndProducao(tarefaIdCredito,
					producao.getId(), itemTarefaIdDoCredito);
			if (Objects.nonNull(valorCredito)) {
				System.out.println("entrou no if credito,itemTarefaIdDoCredito ");
			}else{
				System.out.println("entrou no else credito,itemTarefaIdCredito ");
				 valorCredito = itemValoresService.findByTarefaItemTarefaAndProducao(tarefaIdCredito,
						producao.getId(), itemTarefaIdCredito);
			}
			
			String credito = "";
				credito = valorCredito.getValor();
				credito = credito.replace(".", "");
				credito = credito.replace(",", ".");
			
			// se n�o tem valor de credito, entra nas condi��es (estimativa tecnica ou
			// comercial)
			if (credito.isEmpty()) {
				
				// previs�o do beneficio
				long tarefaIdPrevBeneficio = 181;
				long itemTarefaIdPrevBeneficio = 822;
				ItemValores previsaoBeneficio = itemValoresService.findByTarefaItemTarefaAndProducao(
				tarefaIdPrevBeneficio, producao.getId(), itemTarefaIdPrevBeneficio);
				
				String beneficio = previsaoBeneficio.getValor();
				beneficio = beneficio.replace(".", "");
				beneficio = beneficio.replace(",", ".");
				
				
				// ESTIMATIVA COMERCIAL
				if (beneficio.isEmpty()) {
					// se n�o tem beneficio, o valor de negocio � em cima da estimativa  comercial
					System.out.println("ESTIMATIVA COMERCIAL-prod" + producao.getId() + "estimativa comercial: "+ contrato.getEstimativaComercial());
					// setar a estimativa comercial
					String estimativaComercial = contrato.getEstimativaComercial();
					estimativaComercial = estimativaComercial.replace(".", "");
					estimativaComercial = estimativaComercial.replace(",", ".");
					BigDecimal comercial = new BigDecimal(estimativaComercial);
					vnLI.setValorBase(comercial);
					vnLI.setModoValor("Comercial");
				}

				// ESTIMATIVA TECNICA
			     else {
					// se tem beneficio,o vn � em cima do honorario
					System.out.println("____________________________________________________________________");
					System.out.println("ESTIMATIVA TECNICA -producaoId: " + producao.getId() + "previsaoBeneficio: "+ previsaoBeneficio.getValor());
						
						// cria variavel valor de negocio
						BigDecimal valorNegocioTecnico = BigDecimal.valueOf(0);
						BigDecimal valorBeneficio = new BigDecimal(beneficio);
						
						valorNegocioTecnico = regraContrato( valorBeneficio, contrato, producao);
						
						System.out.println("valorNegocio total " + valorNegocioTecnico);
						vnLI.setModoValor("Estimado");
						vnLI.setValorBase(valorNegocioTecnico);	
			     }		
							
				// setar as informa��es do vn
				vnLI.setCampanha(producao.getAno());
				vnLI.setProducao(producao);
				vnLI.setTipoNegocio(producao.getTipoDeNegocio());
				vnLI.setCliente(producao.getContrato().getCliente());
				vnLI.setSituacao(producao.getSituacao().toString());
				vnLI.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
				vnLI.setCategoria(producao.getContrato().getCliente().getCategoria());
				vnLI.setCtrObs(producao.getContrato().getObsContrato());
				vnLI.setEquipe(producao.getEquipe().getNome());
				vnLI.setSigla(producao.getConsultor().getSigla());
				update(vnLI);
				continue;
				
				
				// VALOR EFETIVO ATUAL
			} else {
				// se tem valor de credito, o vn � em cima do honorario
				System.out.println("____________________________________________________________________");
				System.out.println("VALOR EFETIVO ATUAL -producaoId: " + producao.getId() + "valorCredito: "+ valorCredito.getValor());
				
				// cria variavel valor de negocio
				BigDecimal valorNegocioAtual = BigDecimal.valueOf(0);
				BigDecimal valorCredit = new BigDecimal(credito);
				
				valorNegocioAtual = regraContrato( valorCredit, contrato, producao);
				
					System.out.println("valorNegocio TOTAL " + valorNegocioAtual);
					// setar as informa��es do vn
					vnLI.setValorBase(valorNegocioAtual);
					vnLI.setModoValor("Atual");
					vnLI.setCampanha(producao.getAno());
					vnLI.setProducao(producao);
					vnLI.setTipoNegocio(producao.getTipoDeNegocio());
					vnLI.setCliente(producao.getContrato().getCliente());
					vnLI.setSituacao(producao.getSituacao().toString());
					vnLI.setNome_empresa(producao.getContrato().getCliente().getRazaoSocial());
					vnLI.setCategoria(producao.getContrato().getCliente().getCategoria());
					vnLI.setCtrObs(producao.getContrato().getObsContrato());
					vnLI.setEquipe(producao.getEquipe().getNome());
					vnLI.setSigla(producao.getConsultor().getSigla());
					update(vnLI);
					continue;
			}
		}
	}

	
	public BigDecimal calculoPercentualFixo(float  porcentagem, BigDecimal itemValor) {
		System.out.println("entrou no metodo calculoPercentualFixo");
		System.out.println("calculoPercentualFixo porcentagem:"+ porcentagem);
		System.out.println("calculoPercentualFixo itemValor:"+ itemValor);
		
		BigDecimal percentualFixo = BigDecimal.valueOf(0);
		percentualFixo = itemValor.multiply(new BigDecimal(porcentagem / 100).setScale(2, RoundingMode.HALF_DOWN));
		return percentualFixo;
	}
	
	
	public BigDecimal calculaPercentualEscalonado(PercentualEscalonado percentual, BigDecimal valorItemValor) {
		
		PercentualEscalonado pe = percentual;
		List<FaixasEscalonado> faixas = pe.getFaixasEscalonamento();
		BigDecimal valorTotal = BigDecimal.valueOf(0);
		
		for (FaixasEscalonado faixa : faixas) {
			System.out.println("------------------");
			System.out.println("Percentual Escalonado entrou no for");
			// Define vari�veis
			BigDecimal inicial = faixa.getValorInicial();
			BigDecimal finalF = faixa.getValorFinal();
			BigDecimal gap1 = finalF.subtract(inicial);
			BigDecimal acimaDe = pe.getAcimaDe();
			

			System.out.println("Faixa: " + inicial + " ... " + finalF);
			System.out.println("finalF: " + finalF);
			System.out.println("porcentagem: " + faixa.getValorPercentual());
			System.out.println("credito: " + valorItemValor);
			System.out.println("acimaDe: " + acimaDe);

			// indentifica qual a faixa de escalonamento que o valor de credito esta
			// mutiplica o valor de credito pelo percentual
			if (valorItemValor.compareTo(acimaDe) == 1 || valorItemValor.compareTo(acimaDe) == 0) {
				System.out.println(
						"1-credito maior que o acima de porcentagem: " + pe.getPercentAcimaDe());
				valorTotal = valorItemValor.multiply(new BigDecimal(pe.getPercentAcimaDe() / 100).setScale(2, RoundingMode.HALF_DOWN));
				System.out.println("vvalorTotal" + valorTotal);
				break;
				
				
			}
			if (valorItemValor.compareTo(inicial) == 1 && valorItemValor.compareTo(finalF) == -1) {
				System.out.println("2-credito entre uma faixa de escalonado -porcentagem: "
						+ faixa.getValorPercentual());
				valorTotal = valorItemValor.multiply(new BigDecimal(faixa.getValorPercentual() / 100)
						.setScale(2, RoundingMode.HALF_DOWN));
				System.out.println("vvalorTotal" + valorTotal);
			}	
		}
		
	return valorTotal;
	}
		
	public BigDecimal regraContrato(BigDecimal itemTafefa, Contrato contrato, Producao producao){
		
		System.out.println("entrou no metodo regraContrato");
		System.out.println("itemTafefa"+itemTafefa);
		System.out.println("contrato"+contrato.getId());
		System.out.println("producao"+producao.getId());
		
		//variaveias teto(limita��o)
		BigDecimal tetoPercentualFixo = BigDecimal.valueOf(0);
		BigDecimal tetoEscalonado = BigDecimal.valueOf(0);
		BigDecimal valorNeg = BigDecimal.valueOf(0);
		
		if(contrato.isPagamentoData() == true) {
			// pegar o honorario
			List<Honorario> honorarios = contrato.getHonorarios();
			
			// percorre a lista para indentificar o tipo de honorario
			// o valor do vn(valor base) � a soma de todos os valores dos honorarios do
			// contrato
			for (Honorario honorario : honorarios) {
				
				// Valor Fixo
				if (honorario.getNome().equalsIgnoreCase("Valor Fixo")) {
					System.out.println("Valor Fixo");

					ValorFixo fixo = honorarioService.getValorFixoByContrato(contrato.getId());
					System.out.println("contrato.getId()" + contrato.getId());
					System.out.println("fixo" + fixo);
					if (Objects.nonNull(fixo)) {
						BigDecimal valorFixo = fixo.getValor();
						if (fixo.isValorTrimestral()) {
							valorFixo = valorFixo.multiply(BigDecimal.valueOf(4));
						} else if (fixo.isValorMensal()) {
							valorFixo = valorFixo.multiply(BigDecimal.valueOf(12));
						}
						valorNeg = valorNeg.add(valorFixo);
						System.out.println("valor negocio fixo" + valorNeg);
					}
				}
				// Percentual Escalonado
				else if (honorario.getNome().equalsIgnoreCase("Percentual Escalonado")) {
					PercentualEscalonado pe = honorarioService.getPercentualEscalonadoByContrato(contrato.getId());
					
					BigDecimal valorEscalonado = BigDecimal.valueOf(0);
					
					valorEscalonado = calculaPercentualEscalonado(pe,itemTafefa);
					
					valorNeg = valorNeg.add(valorEscalonado);
					  System.out.println("valor Fixo " + valorNeg);
					
				}
				// Percentual Fixo
				else if (honorario.getNome().equalsIgnoreCase("Percentual Fixo")) {

					// pega o percentual fixo
					PercentualFixo fixo = honorarioService.getPercentualFixoByContrato(contrato.getId());
					BigDecimal percentualFixo = BigDecimal.valueOf(0);

					// mutiplica o valor de credito pelo porcentagem do percentual fixo
					if (Objects.nonNull(fixo)) {
						percentualFixo = itemTafefa.multiply(new BigDecimal(fixo.getValorPercentual() / 100)
								.setScale(2, RoundingMode.HALF_DOWN));
						valorNeg  = valorNeg .add(percentualFixo);
					}
					System.out.println("valorNegocio Percentual Fixo " + valorNeg );
				}
			}
		}	
		else if(contrato.isPagamentoEntrega() == true){
			System.out.println("Pagamento por entrega, entrou no valor Atual");	
			//acessando os honorarios a partir dos timings de faturamento da tabela de etapas pagametos
			EtapasPagamento timing = producao.getContrato().getEtapasPagamento();
			
			EtapaTrabalho etapa = new EtapaTrabalho();
			String  honorario = "";
			BigDecimal valor = BigDecimal.valueOf(0);
			float porcentagem = 0.0f;
			PercentualEscalonado escalonado = new PercentualEscalonado();
			
			for(int i=0;i<= 3;i++){
				System.out.println("entrou no for");
				   if (i==0){
					   etapa = timing.getEtapa1();
					   honorario = timing.getHonorario1();
					   valor = timing.getValor1();
					   porcentagem = timing.getPorcentagem1();
					   escalonado = timing.getEscalonado1();
				   }else if (i==1) {
					   etapa = timing.getEtapa2();
					   honorario = timing.getHonorario2();
					   valor = timing.getValor2();
					   porcentagem = timing.getPorcentagem2();
					   escalonado = timing.getEscalonado2();
				   }else if (i==2) {
					   etapa = timing.getEtapa3();
					   honorario = timing.getHonorario3();
					   valor = timing.getValor3();
					   porcentagem = timing.getPorcentagem3();
					   escalonado = timing.getEscalonado3();
				   }else if (i==3) {
					   etapa = timing.getEtapa4();
					   honorario = timing.getHonorario4();
					   valor = timing.getValor4();
					   porcentagem = timing.getPorcentagem4();
					   escalonado = timing.getEscalonado4();
				   }
				   
					//HONORARIOs
					if (Objects.nonNull(etapa) && !honorario.equalsIgnoreCase("Isento")) {
						System.out.println("HONORARIO" + i);
							
						//Valor Fixo
						if (honorario.equalsIgnoreCase("Valor Fixo")) {
							System.out.println("Valor Fixo");
							BigDecimal valorFixo = BigDecimal.valueOf(0);
							valorFixo = valor;
							valorNeg  = valorNeg .add(valorFixo);
							System.out.println("valor Fixo " + valorNeg );
						}
							
							//Percentual Escalonado
							if(honorario.equalsIgnoreCase("Percentual Escalonado")) {
								System.out.println("Percentual Escalonado");
								BigDecimal valorEscalonado = BigDecimal.valueOf(0);
								
								valorEscalonado = calculaPercentualEscalonado(escalonado,itemTafefa);
								valorNeg  = valorNeg .add(valorEscalonado);
								System.out.println("Percentual Escalonado " + valorNeg);
							}
							
							//Percentual Fixo
							if (honorario.equalsIgnoreCase("Percentual Fixo")) {
								System.out.println("Percentual Fixo");
								BigDecimal percentualFixo = BigDecimal.valueOf(0);
								
								// mutiplica o valor de credito pelo porcentagem do percentual fixo
								percentualFixo =  calculoPercentualFixo(porcentagem,itemTafefa);
								valorNeg  = valorNeg .add(percentualFixo);
									System.out.println(" Percentual Fixo " + porcentagem);
								System.out.println("valorNegocio Percentual Fixo " + valorNeg );
							}
						}
					
			}
			
		}
		
		//comparando com o teto
		
		//teto Escalonado
		if (tetoEscalonado != null && tetoPercentualFixo == null) {
			if (tetoEscalonado.compareTo(new BigDecimal("0.00")) > 0) {
				if (valorNeg .compareTo(tetoEscalonado) > 0) {
					System.out.println("VN maior que teto");
					valorNeg  = tetoEscalonado;
					valorNeg  = valorNeg .setScale(2, BigDecimal.ROUND_HALF_UP);
				}
			}
		}
		//teto PercentualFixo
		else if (tetoEscalonado == null && tetoPercentualFixo != null) {
			if (tetoPercentualFixo.compareTo(new BigDecimal("0.00")) > 0) {
				if (valorNeg .compareTo(tetoPercentualFixo) > 0) {
					System.out.println("VN maior que teto");
					valorNeg  = tetoPercentualFixo;
					valorNeg  = valorNeg .setScale(2, BigDecimal.ROUND_HALF_UP);
				}
			}
		}
		
		//possui os dois tetos, considera apenas o do escalonado
		else if (tetoEscalonado != null && tetoPercentualFixo != null) {
			if (tetoEscalonado.compareTo(new BigDecimal("0.00")) > 0) {
				if (valorNeg .compareTo(tetoEscalonado) > 0) {
					System.out.println("VN maior que teto");
					valorNeg  = tetoEscalonado;
					valorNeg  = valorNeg .setScale(2, BigDecimal.ROUND_HALF_UP);
				}
			}
		}
		return valorNeg;
	}
}
