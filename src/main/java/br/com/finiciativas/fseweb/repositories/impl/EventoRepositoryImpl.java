package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.IeM.Evento;

@Repository
public class EventoRepositoryImpl {
	

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Evento reco) {
		
		reco.setOportunidade("Evento");
		
		this.em.persist(reco);
	}

	public void delete(Evento reco) {
		this.em.remove(reco);
	}

	@Transactional
	public Evento update(Evento reco) {
		return this.em.merge(reco);
	}

	public List<Evento> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Evento o ", Evento.class)
				.getResultList();
	}

	public Evento findById(Long id) {
		return this.em.find(Evento.class, id);
	}

}
