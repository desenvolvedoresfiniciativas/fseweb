package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.HabilitacaoPortaria;

@Repository
public class HabilitacaoPortariaRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(HabilitacaoPortaria HabilitacaoPortaria) {
		this.em.persist(HabilitacaoPortaria);
	}

	public void delete(HabilitacaoPortaria HabilitacaoPortaria) {
		this.em.remove(HabilitacaoPortaria);
	}

	@Transactional
	public HabilitacaoPortaria update(HabilitacaoPortaria HabilitacaoPortaria) {
		return this.em.merge(HabilitacaoPortaria);
	}
	
	public List<HabilitacaoPortaria> getAll() {
		return this.em.createQuery("SELECT distinct o FROM HabilitacaoPortaria o ", HabilitacaoPortaria.class)
				.getResultList();
	}
	
	public HabilitacaoPortaria findById(Long id) {
		return this.em.find(HabilitacaoPortaria.class, id);
	}
	
	public List<HabilitacaoPortaria> getHabilitacaoPortariaByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM HabilitacaoPortaria o WHERE o.producao.id = :pId ", HabilitacaoPortaria.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
