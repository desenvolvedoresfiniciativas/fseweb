package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.honorario.FaixasEscalonado;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.honorario.ValorFixo;
import br.com.finiciativas.fseweb.repositories.HonorarioRepository;

@Repository
public class HonorarioRepositoryImpl implements HonorarioRepository {

	@Autowired
	private EntityManager em;

	@Transactional
	@Override
	public Honorario update(Honorario honorario) {
		return this.em.merge(honorario);
	}
	
	@Transactional
	public Honorario updatePercentualFixo(PercentualFixo honorario) {
		System.out.println(honorario.getLimitacao());
		honorario.setLimitacao(honorario.getLimitacao());
		return this.em.merge(honorario);
	}

	@Override
	public List<Honorario> getAll() {
		return this.em.createQuery("FROM Honorario", Honorario.class).getResultList();
	}
	
	@Transactional
	public void delete(Honorario honorario) {
		this.em.remove(honorario);
	}

	public Honorario getHonorarioByNomeAndContrato(Long id, String nome) {
		return this.em.createQuery("FROM Honorario h WHERE h.contrato.id = :pId AND h.nome = :pNome", Honorario.class)
				.setParameter("pId", id)
				.setParameter("pNome", nome)
				.getSingleResult();
	}
	
	public ValorFixo getValorFixoByContrato(Long id) {
		try {
			return this.em.createQuery("FROM ValorFixo v WHERE v.contratoId = :pId", ValorFixo.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("N�o existe");
			return null;
		}
	}
	
	public List<Honorario> getListHonorarioByContrato(Long id) {
		return this.em.createQuery("FROM Honorario h WHERE h.contrato.id = :pId", Honorario.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
	public PercentualFixo getPercentualFixoByContrato(Long id) {
		try {
			return this.em.createQuery("FROM PercentualFixo v WHERE v.contratoId = :pId", PercentualFixo.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("N�o existe");
			return null;
		}
	}
	
	public FaixasEscalonado getMaxFaixa() {
			return this.em.createQuery("FROM FaixasEscalonado f ORDER BY f.id DESC LIMIT 1;", FaixasEscalonado.class).getSingleResult();
	}

	public PercentualEscalonado getPercentualEscalonadoByContrato(Long id) {
		System.out.println(id);
		try {
			return this.em
					.createQuery("FROM PercentualEscalonado p WHERE p.contratoId = :pId", PercentualEscalonado.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	public PercentualEscalonado getPercentualEscalonadoById(Long id) {
		try {
			return this.em
					.createQuery("FROM PercentualEscalonado p WHERE p.id = :pId", PercentualEscalonado.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}
	public PercentualFixo getPercentualFixoById(Long id) {
		try {
			return this.em.createQuery("FROM PercentualFixo v WHERE v.id = :pId", PercentualFixo.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("N�o existe");
			return null;
		}
	}
}
