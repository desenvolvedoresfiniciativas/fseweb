package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ResultadoFinal;

public interface ResultadoFinalRepository {

	void create(ResultadoFinal resultado);

	void delete(ResultadoFinal resultado);
		
	ResultadoFinal update(ResultadoFinal resultado);
	
	List<ResultadoFinal> getAll();
	
	ResultadoFinal findById(Long id);
	
	List<ResultadoFinal> getResultadoFinalByProducao(Long id);
	
}
