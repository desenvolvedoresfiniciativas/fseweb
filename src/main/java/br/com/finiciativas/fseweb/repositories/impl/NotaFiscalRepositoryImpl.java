package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.services.ComissaoService;

@Repository
public class NotaFiscalRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ComissaoService comissaoService;

	@Transactional
	public NotaFiscal create(NotaFiscal notaFiscal) {
	
		this.em.persist(notaFiscal);
		em.flush();
		
		return notaFiscal;
	}

	public void delete(NotaFiscal notaFiscal) {
		this.em.remove(notaFiscal);
	}

	@Transactional
	public NotaFiscal update(NotaFiscal notaFiscal) {
		return this.em.merge(notaFiscal);
	}

	public NotaFiscal findById(Long id) {
		return this.em.find(NotaFiscal.class, id);
	}

	public List<NotaFiscal> getAll() {
		return this.em.createQuery("SELECT distinct o FROM NotaFiscal o", NotaFiscal.class).getResultList();
	}

	public NotaFiscal findByClienteContratoFaturamento(Long idCliente, Long idContrato, Long idFaturamento) {
		try {
			return em
					.createQuery(
							"SELECT n FROM NotaFiscal n " + "WHERE n.cliente.id = :pCliente "
									+ "AND n.contrato.id = :pContrato " + "AND n.faturamento.id = :pFaturamento",
							NotaFiscal.class)
					.setParameter("pCliente", idCliente).setParameter("pContrato", idContrato)
					.setParameter("pFaturamento", idFaturamento).getSingleResult();
		} catch (Exception e) {
			System.out.println("N�o encontramos nenhuma Nota fiscal com os par�metros");
			return null;
		}
	}

	public NotaFiscal findByFaturamento(Long idFaturamento) {
		try {
			return em.createQuery("SELECT n FROM NotaFiscal n WHERE n.faturamento.id = :pFaturamento", NotaFiscal.class)
					.setParameter("pFaturamento", idFaturamento).getSingleResult();
		} catch (Exception e) {
//			System.out.println("N�o encontramos nenhuma Nota fiscal com o faturamento de Id: " + idFaturamento);
			return null;
		}
	}
	
	public List<NotaFiscal> getNotasByCliente(Long id) {
		System.out.println("entrou na repository de notaFiscal " + id);
		return em.createQuery("SELECT f FROM NotaFiscal f WHERE f.cliente.id = :pClienteId", NotaFiscal.class)
				.setParameter("pClienteId", id).getResultList();
	}
	
	public List<NotaFiscal> getNotasByFaturamento(Long id) {
		System.out.println("entrou na repository de notaFiscal " + id);
		return em.createQuery("SELECT f FROM NotaFiscal f WHERE f.faturamento.id = :pFaturamentoId", NotaFiscal.class)
				.setParameter("pFaturamentoId", id).getResultList();
	}
	
	public List<NotaFiscal> getNotasByComercialEntrada(Long id) {
		System.out.println("entrou na repository de notaFiscal por comercial entrada " + id);
		return em.createQuery("SELECT f FROM NotaFiscal f WHERE f.contrato.comercialResponsavelEntrada.id = :pContratoId", NotaFiscal.class)
				.setParameter("pContratoId", id).getResultList();
	}
	
	public List<NotaFiscal> getNotasByAnoWhereNN(String ano) {
		return em.createQuery("SELECT distinct n FROM NotaFiscal n  WHERE n.faturamento.producao.ano = :pAno AND n.faturamento.producao.tipoDeNegocio = 'NN'", NotaFiscal.class)
				.setParameter("pAno", ano).getResultList();
	}
	
	public List<NotaFiscal> getNotasByCampanha(String ano) {
		return em.createQuery("SELECT distinct n FROM NotaFiscal n WHERE n.faturamento.campanha = :pAno", NotaFiscal.class)
				.setParameter("pAno", ano).getResultList();
	}


	public List<NotaFiscal> getAllNotasByCriteria(String ano, Long idProduto, Long idFilial, String empresa,
			String tipoDeNegocio, Long etapa, Boolean cancelada, Boolean emAtraso, String motivoCancelamento,
			String numeroNF, String dataInicioEmissao, String dataFinalEmissao, String dataInicioCobrar,
			String dataFinalCobrar, String dataInicioCobranca, String dataFinalCobranca) {


		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<NotaFiscal> root = cq.from(NotaFiscal.class);

		// Constructing list of parameters
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (ano != null) {
			predicates.add(qb.equal(root.get("faturamento").get("campanha"), ano));
		}
		if (idProduto != null) {
			// faturamentos.add(qb.equal(root.get("produto").get("id"), idProduto));
			predicates.add(qb.equal(root.get("contrato").get("produto").get("id"), idProduto));
		}
		if (empresa != null) {
			predicates.add(qb.like(root.get("razaoSocialFaturar"), "%" + empresa + "%"));
		}
		if (idFilial != null) {
			predicates.add(qb.equal(root.get("filial").get("id"), idFilial));
		}

		if(tipoDeNegocio != null) {
			predicates.add(qb.equal(root.get("faturamento").get("producao").get("tipoDeNegocio"), tipoDeNegocio));

		}
		if (etapa != null) {
			predicates.add(qb.equal(root.get("faturamento").get("etapa").get("id"), etapa));
		}

		if (cancelada != null) {
		predicates.add(qb.equal(root.get("cancelada"), cancelada));
		}
		
		if (emAtraso != null) {
		predicates.add(qb.equal(root.get("atraso"), emAtraso));
		}

		if (motivoCancelamento != null) {
			predicates.add(qb.equal(root.get("motivoCancelamento"), motivoCancelamento));
		}
		if (numeroNF != null) {
			predicates.add(qb.equal(root.get("numeroNF"), numeroNF));
		}

		if (dataInicioEmissao != null || dataFinalEmissao != null) {
			predicates.add(qb.between(root.get("dataEmissao"), dataInicioEmissao, dataFinalEmissao));
		}

		if (dataInicioCobrar != null || dataFinalCobrar != null) {
			predicates.add(qb.between(root.get("dataVencimento"), dataInicioCobrar, dataFinalCobrar));
		}

		if (dataInicioCobranca != null || dataFinalCobranca != null) {
			predicates.add(qb.between(root.get("dataCobranca"), dataInicioCobranca, dataFinalCobranca));
		}
		
		
		
		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}
	
	public List<NotaFiscal> getNotasByCriteria(String ano, Long idProduto, Long etapa, Long idFilial, String empresa,
			String tipoNegocio, String numeroNF, Boolean atraso, Boolean pago){
		
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery(); 
		Root<NotaFiscal> root = cq.from(NotaFiscal.class); 
		
		List<Predicate> predicates = new ArrayList<Predicate>(); 	
		
		if(ano != null) {
			System.out.println("Entrou ano" + ano);
			predicates.add(qb.equal(root.get("faturamento").get("campanha"), ano));
		}
		
		if(idProduto != null) {
			predicates.add(qb.equal(root.get("contrato").get("produto").get("id"), idProduto));
		}
		if(etapa !=null) {
			predicates.add(qb.equal(root.get("faturamento").get("etapa").get("id"),etapa));
		}
		if(idFilial != null) {
			predicates.add(qb.equal(root.get("filial").get("id"), idFilial));
		}
		if(empresa != null) {
			predicates.add(qb.like(root.get("razaoSocialFaturar"), "%"+empresa+"%"));
		}
		if(numeroNF != null) {
			predicates.add(qb.equal(root.get("numeroNF"), numeroNF));
		}
		if(tipoNegocio != null) {
			predicates.add(qb.equal(root.get("faturamento").get("producao").get("tipoDeNegocio"), tipoNegocio));
		}
		if(atraso != null) {
			predicates.add(qb.equal(root.get("atraso"), atraso));
		}
		if(pago != null) {
			predicates.add(qb.equal(root.get("cobranca"), pago));
		}
		
		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}
	
	
	
	public List<NotaFiscal> getNotaSubstituida(String idNotaSubstituida) {
		return em.createQuery("SELECT n FROM NotaFiscal n  WHERE n.numeroNotaSubstituida = :pid", NotaFiscal.class)
				.setParameter("pid", idNotaSubstituida).getResultList();
	}

	public List<NotaFiscal> getSomaPagamentoByCriteria(String dataInicial, String dataFinal, String campanha) {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery(); 
		Root<NotaFiscal> root = cq.from(NotaFiscal.class); 
		
		List<Predicate> predicates = new ArrayList<Predicate>(); 	
		
		
		
		Predicate datas = (qb.between(root.get("dataCobranca"), dataInicial, dataFinal));
		Predicate paga = (qb.equal(root.get("cobranca"), true));
		Predicate cancelada = (qb.equal(root.get("cancelada"), false));
		Predicate ano = (qb.equal(root.get("faturamento").get("producao").get("ano"), campanha));
		predicates.add(qb.and(datas, paga, cancelada,ano));
		//Expression<Double> resultado = qb.sumAsDouble(root.get("cobrar").in(qb.and(datas, paga, cancelada)));
		//Expression<Double> resultado = qb.and(datas, paga, cancelada).;
		
		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
	
		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}
	
	public List<NotaFiscal> getSomaPagamentoDateByCriteria(Date dataInicial, Date dataFinal, String campanha) {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery(); 
		Root<NotaFiscal> root = cq.from(NotaFiscal.class); 
		
		List<Predicate> predicates = new ArrayList<Predicate>(); 	
		
		
		
		Predicate datas = (qb.between(root.get("dataCobranca"), dataInicial, dataFinal));
		Predicate paga = (qb.equal(root.get("cobranca"), true));
		Predicate cancelada = (qb.equal(root.get("cancelada"), false));
		Predicate ano = (qb.equal(root.get("faturamento").get("producao").get("ano"), campanha));
		predicates.add(qb.and(datas, paga, cancelada,ano));
		//Expression<Double> resultado = qb.sumAsDouble(root.get("cobrar").in(qb.and(datas, paga, cancelada)));
		//Expression<Double> resultado = qb.and(datas, paga, cancelada).;
		
		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
	
		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}
	


	}


