package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.PreProducao;

@Repository
public class PreProducaoRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Transactional
	public PreProducao create(PreProducao balancete) {
			this.em.persist(balancete);
			em.flush();
			
			return balancete;
	}

	public void delete(PreProducao balancete) {
		this.em.remove(balancete);
	}

	@Transactional
	public PreProducao update(PreProducao balancete) {
		return this.em.merge(balancete);
	}

	public List<PreProducao> getAll() {
		return this.em.createQuery("SELECT distinct o FROM PreProducao o ", PreProducao.class)
				.getResultList();
	}

	public PreProducao findById(Long id) {
		return this.em.find(PreProducao.class, id);
	}
	
	public List<PreProducao> getAllByCliente(Long id) {
		return this.em.createQuery("SELECT distinct o FROM PreProducao o WHERE o.cliente.id = :pId", PreProducao.class)
				.setParameter("pId", id)
				.getResultList();
	}
	public PreProducao getPreByAnoCliente(Long id,String ano) {
		return this.em.createQuery("SELECT o FROM PreProducao o WHERE o.cliente.id = :cId and o.campanha = :aId",PreProducao.class)
				.setParameter("cId", id).setParameter("aId", ano).getSingleResult();
	}
}
