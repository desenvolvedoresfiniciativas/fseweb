package br.com.finiciativas.fseweb.repositories;

import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;

public interface EnderecoClienteRepository {
	
	EnderecoCliente update(EnderecoCliente enderecoCliente);
	
	void create(EnderecoCliente enderecoCliente);

	
}
