package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;

@Repository
public class BalanceteCalculoRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Transactional
	public BalanceteCalculo create(BalanceteCalculo balancete) {
			this.em.persist(balancete);
			em.flush();
			
			return balancete;
	}

	public void delete(BalanceteCalculo balancete) {
		this.em.remove(balancete);
	}
	
	@Transactional
	public void deleteOfProducao(Long id){
		em.createQuery("DELETE FROM BalanceteCalculo b WHERE b.producao.id = :pId")
				.setParameter("pId", id)
				.executeUpdate();
	}

	@Transactional
	public BalanceteCalculo update(BalanceteCalculo balancete) {
		return this.em.merge(balancete);
	}

	public List<BalanceteCalculo> getAll() {
		return this.em.createQuery("SELECT distinct o FROM BalanceteCalculo o ", BalanceteCalculo.class)
				.getResultList();
	}

	public BalanceteCalculo findById(Long id) {
		return this.em.find(BalanceteCalculo.class, id);
	}

	public List<BalanceteCalculo> getBalanceteCalculoByProducao(Long id) {
		em.clear();
		try {
			return this.em.createQuery("SELECT o FROM BalanceteCalculo o WHERE o.producao.id = :pId ",
					BalanceteCalculo.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<BalanceteCalculo> balancete = new ArrayList<BalanceteCalculo>();
			return balancete;
		}
		
	}

	public BalanceteCalculo getLastBalanceteCalculoByProducao(Long id) {
		
		try {
			return this.em
					.createQuery("SELECT distinct o FROM BalanceteCalculo o WHERE o.producao.id = :pId AND o.versao = "
							+ "(SELECT max(b.versao) FROM BalanceteCalculo b)", BalanceteCalculo.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("Sem balancete");
			BalanceteCalculo balancete = new BalanceteCalculo();
			balancete = null;
			return balancete;
		}
	}
	
	public List<BalanceteCalculo> getAllBalanceteByAno(String ano){
		return this.em.createQuery("SELECT o FROM BalanceteCalculo o WHERE o.producao.ano = :pAno", BalanceteCalculo.class).setParameter("pAno", ano).getResultList();
	}

	public List<BalanceteCalculo> getBalancetesByProducao(Long idProducao){
		return this.em.createQuery("SELECT o FROM BalanceteCalculo o WHERE o.producao.id = :pId", 
				BalanceteCalculo.class).setParameter("pId", idProducao).getResultList();
	}

	public List<BalanceteCalculo> getBalancetesPrejuizoByProducao(Long idProducao) {
		return this.em.createQuery("SELECT o FROM BalanceteCalculo o WHERE o.producao.id = :pId AND o.prejuizo = 1 ORDER BY o.versao", 
				BalanceteCalculo.class).setParameter("pId", idProducao).getResultList();
	}

	public List<BalanceteCalculo> getBalancetesNaoPrejuizoByProducao(Long idProducao) {
		return this.em.createQuery("SELECT o FROM BalanceteCalculo o WHERE o.producao.id = :pId AND o.prejuizo = 0 ORDER BY o.versao", 
				BalanceteCalculo.class).setParameter("pId", idProducao).getResultList();
	}
}
