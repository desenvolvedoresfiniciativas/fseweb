package br.com.finiciativas.fseweb.repositories.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.honorario.FaixasEscalonado;
import br.com.finiciativas.fseweb.models.honorario.Honorario;
import br.com.finiciativas.fseweb.models.honorario.PercentualEscalonado;
import br.com.finiciativas.fseweb.models.honorario.PercentualFixo;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorAnual;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorEstimado;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorFiscalizado;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorGlobal;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorMixto;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorNegocioChile;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorPostulado;
import br.com.finiciativas.fseweb.services.impl.HonorarioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PreProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;

@Repository
public class ValorNegocioCLRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ContratoRepositoryImpl contratoRepositoryImpl;

	@Autowired
	private ProducaoServiceImpl producaoServiceImpl;

	@Autowired
	private ItemValoresServiceImpl itemValoresServiceImpl;

	@Autowired
	private PresupuestosValorNegocioCRepositoryImpl presupuesto;

	@Autowired
	private ValorDisponibleRepositoryImpl disponible;

	@Autowired
	private HonorarioServiceImpl honorarioServiceImpl;

	@Autowired
	PreProducaoServiceImpl preProducaoServiceImpl;

	@Transactional
	public void geraValorAnual() {
		List<Contrato> contratosAnual = contratoRepositoryImpl.getContratosByTipo("Anual");
		BigDecimal dispendio = new BigDecimal("0.35");
		deleteAllFromAnual();
		for (Contrato contrato : contratosAnual) {
			System.out.println(contrato.getId());
			BigDecimal vAno = BigDecimal.ZERO;
			List<String> anosProd = producaoServiceImpl.getTotalAnos(contrato.getId());
			HashMap<String, BigDecimal> allValues = new HashMap<String, BigDecimal>();
			//
			for (String campanha : anosProd) {
				String etapaAtual = "";
				
				List<Producao> prodByContrato = producaoServiceImpl.getProducaoByAnoAndContrato(contrato.getId(),
						campanha);
				
				for (Producao prod : prodByContrato) {
					try {
						ItemValores etapa = itemValoresServiceImpl.getEtapaAtualByProd(prod.getId());
						System.out.println(etapa.getValor());
						if (etapa.getValor().equals("APROBADO")) {
							List<String> anoGasto = disponible.getAnoGastoByProducao(prod.getId());
							for (String ano : anoGasto) {
								List<ValorFiscalizado> fiscalizados = disponible.getValoresByProd(prod.getId(), ano);
								for (ValorFiscalizado fiscalizado : fiscalizados) {
									BigDecimal total = new BigDecimal(fiscalizado.getValor());
									try {
										if (allValues.get(ano) != null) {
											allValues.put(ano.toString(), allValues.get(ano).add(total));
											
										} else {
											allValues.put(ano.toString(), total);
										}
										

									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "APROBADO";
							}
						} else if (etapa.getValor().equals("POSTULADO")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoPostulado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorPostulado> postulados = presupuesto.getPostuladoByProdAndAno(prod.getId(),
										ano);
								for (ValorPostulado postulado : postulados) {
									BigDecimal total = new BigDecimal(postulado.getValor());

									try {
										if (allValues.get(ano) != null) {
											allValues.put(ano.toString(), allValues.get(ano).add(total));
										} else {
											allValues.put(ano.toString(), total);
										}
										
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "POSTULADO";
							}
						} else if (etapa.getValor().equals("RECURSO REPOSICIÓN")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoPostulado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorPostulado> postulados = presupuesto.getPostuladoByProdAndAno(prod.getId(),
										ano);
								for (ValorPostulado postulado : postulados) {
									BigDecimal total = new BigDecimal(postulado.getValor());

									try {
										if (allValues.get(ano) != null) {
											allValues.put(ano.toString(), allValues.get(ano).add(total));
										} else {
											allValues.put(ano.toString(), total);
										}
										
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "Recurso";
							}
						} else if (etapa.getValor().equals("LEVANTAMIENTO") || etapa.getValor().equals("FORMULACIÓN")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoEstimado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorEstimado> estimados = presupuesto.getEstimadoByProdAndAno(prod.getId(), ano);
								for (ValorEstimado estimado : estimados) {
									BigDecimal total = new BigDecimal(estimado.getValor());
									try {
										if (allValues.get(ano) != null) {
											allValues.put(ano.toString(), allValues.get(ano).add(total));
										} else {
											allValues.put(ano.toString(), total);
										}
										
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "ESTIMADO";
							}
						} else {
								etapaAtual = "NA";
						}

					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}
				}

			}
			//
			
			
			
			
			
			for (String campanha : anosProd) {
				String etapaAtual = "";
				
				List<Producao> prodByContrato = producaoServiceImpl.getProducaoByAnoAndContrato(contrato.getId(),
						campanha);
				
				for (Producao prod : prodByContrato) {
					HashMap<String, BigDecimal> atual = new HashMap<String, BigDecimal>();
					try {
						ItemValores etapa = itemValoresServiceImpl.getEtapaAtualByProd(prod.getId());
						System.out.println(etapa.getValor());
						if (etapa.getValor().equals("APROBADO")) {
							List<String> anoGasto = disponible.getAnoGastoByProducao(prod.getId());
							for (String ano : anoGasto) {
								List<ValorFiscalizado> fiscalizados = disponible.getValoresByProd(prod.getId(), ano);
								for (ValorFiscalizado fiscalizado : fiscalizados) {
									BigDecimal total = new BigDecimal(fiscalizado.getValor());
									try {
										if (atual.get(ano) != null) {
											atual.put(ano.toString(), atual.get(ano).add(total));
											
										} else {
											atual.put(ano.toString(), total);
										}
										

									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "APROBADO";
							}
						} else if (etapa.getValor().equals("POSTULADO")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoPostulado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorPostulado> postulados = presupuesto.getPostuladoByProdAndAno(prod.getId(),
										ano);
								for (ValorPostulado postulado : postulados) {
									BigDecimal total = new BigDecimal(postulado.getValor());

									try {
										if (atual.get(ano) != null) {
											atual.put(ano.toString(), atual.get(ano).add(total));
										} else {
											atual.put(ano.toString(), total);
										}
										
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "POSTULADO";
							}
						} else if (etapa.getValor().equals("RECURSO REPOSICIÓN")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoPostulado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorPostulado> postulados = presupuesto.getPostuladoByProdAndAno(prod.getId(),
										ano);
								for (ValorPostulado postulado : postulados) {
									BigDecimal total = new BigDecimal(postulado.getValor());

									try {
										if (atual.get(ano) != null) {
											atual.put(ano.toString(), atual.get(ano).add(total));
										} else {
											atual.put(ano.toString(), total);
										}
										
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "Recurso";
							}
						} else if (etapa.getValor().equals("LEVANTAMIENTO") || etapa.getValor().equals("FORMULACIÓN")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoEstimado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorEstimado> estimados = presupuesto.getEstimadoByProdAndAno(prod.getId(), ano);
								for (ValorEstimado estimado : estimados) {
									BigDecimal total = new BigDecimal(estimado.getValor());
									try {
										if (atual.get(ano) != null) {
											atual.put(ano.toString(), atual.get(ano).add(total));
										} else {
											atual.put(ano.toString(), total);
										}
										
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								etapaAtual = "ESTIMADO";
							}
						} else {
								etapaAtual = "NA";
						}

					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}
					
					
					
					
					
					
					if (!etapaAtual.equals("NA")) {
						for (Map.Entry<String, BigDecimal> entry : atual.entrySet()) {
							
							
							Float fee = 0f;
							Integer feeFixo = 0;
							vAno = allValues.get(entry.getKey()).multiply(dispendio);
							
							
							List<Honorario> tipoFees = honorarioServiceImpl.getListHonorarioByContrato(contrato.getId());
							for (Honorario tipoFee : tipoFees) {
								if (tipoFee.getNome().equalsIgnoreCase("Percentual Escalonado")) {

									PercentualEscalonado pe = honorarioServiceImpl
											.getPercentualEscalonadoById(tipoFee.getId());
									List<FaixasEscalonado> pefe = pe.getFaixasEscalonamento();
									for (FaixasEscalonado faixa : pefe) {
										PercentualEscalonado acimaDe = honorarioServiceImpl
												.getPercentualEscalonadoById(tipoFee.getId());

										if (vAno.compareTo(acimaDe.getAcimaDe()) == 1
												|| vAno.compareTo(acimaDe.getAcimaDe()) == 0) {
											fee = acimaDe.getPercentAcimaDe();
										}
										if (vAno.compareTo(faixa.getValorInicial()) == 1
												&& vAno.compareTo(faixa.getValorFinal()) == -1) {
											fee = faixa.getValorPercentual();
										}
									}
								} else if (tipoFee.getNome().equalsIgnoreCase("Percentual Fixo")) {
									PercentualFixo fixo = honorarioServiceImpl.getPercentualFixoById(tipoFee.getId());
									fee = fixo.getValorPercentual();
								}
							}
							
							
							
							BigDecimal valorTotal = new BigDecimal("0.00");
							ValorAnual vAnual = new ValorAnual();
							vAnual.setCampanha(campanha);
							vAnual.setContrato(contrato);
							vAnual.setEtapa(etapaAtual);
							vAnual.setAnoFiscal(entry.getKey());
							
							valorTotal = entry.getValue().multiply(dispendio).multiply(new BigDecimal(fee/100)).setScale(2,
									RoundingMode.HALF_DOWN);; 
							
							vAnual.setFee(fee.toString());
							vAnual.setValor(valorTotal.toString());
							
							CreateAnual(vAnual);
							
						}
					}
				}

			}

		}
	}

	@Transactional
	public void CreateAnual(ValorAnual valorAnual) {
		this.em.persist(valorAnual);
	}

	public List<ValorAnual> getValorAnualByContrato(Long id) {
		try {
			return this.em.createQuery("SELECT va from ValorAnual va WHERE va.contrato.id = :cId", ValorAnual.class)
					.setParameter("cId", id).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Transactional
	public void deleteAllFromAnual() {
		this.em.createNativeQuery("DELETE FROM valor_anual").executeUpdate();
	}

	@Transactional
	public void geraValorGlobal() {
		List<Contrato> contratosGlobal = contratoRepositoryImpl.getContratosByTipo("Global");
		BigDecimal dispendio = new BigDecimal("0.35");
		deleteAllFromGlobal();
		for (Contrato contrato : contratosGlobal) {
			List<String> anosProd = producaoServiceImpl.getTotalAnos(contrato.getId());
			for (String campanha : anosProd) {
				String etapaAtual = "";
				BigDecimal vTotal = new BigDecimal("0.0");
				
				List<Producao> prodByContrato = producaoServiceImpl.getProducaoByAnoAndContrato(contrato.getId(),
						campanha);

				for (Producao prod : prodByContrato) {
					HashMap<String, BigDecimal> atual = new HashMap<String, BigDecimal>();
					try {
						
						ItemValores etapa = itemValoresServiceImpl.getEtapaAtualByProd(prod.getId());
						if (etapa.getValor().equals("APROBADO")) {
							List<String> anoGasto = disponible.getAnoGastoByProducao(prod.getId());
							for (String ano : anoGasto) {
								List<ValorFiscalizado> fiscalizados = disponible.getValoresByProd(prod.getId(), ano);
								for (ValorFiscalizado fiscalizado : fiscalizados) {
									vTotal = vTotal.add(new BigDecimal(fiscalizado.getValor()));
									BigDecimal valorAtual = new BigDecimal(fiscalizado.getValor());
									if (valorAtual.compareTo(BigDecimal.ZERO) != 0) {
										try {
											if (atual.get(ano) != null) {
												atual.put(ano.toString(), atual.get(ano).add(valorAtual));
											} else {
												atual.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println(e);
										}
									}
								}
								etapaAtual = "APROBADO";
							}
						} else if (etapa.getValor().equals("POSTULADO")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoPostulado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorPostulado> postulados = presupuesto.getPostuladoByProdAndAno(prod.getId(),
										ano);
								for (ValorPostulado postulado : postulados) {
									vTotal = vTotal.add(new BigDecimal(postulado.getValor()));
									BigDecimal valorAtual = new BigDecimal(postulado.getValor());
									if (valorAtual.compareTo(BigDecimal.ZERO) != 0) {
										try {
											if (atual.get(ano) != null) {
												atual.put(ano.toString(), atual.get(ano).add(valorAtual));
											} else {
												atual.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println(e);
										}
									}
								}
								etapaAtual = "POSTULADO";
							}
						} else if (etapa.getValor().equals("LEVANTAMIENTO") || etapa.getValor().equals("FORMULACIÓN")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoEstimado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorEstimado> estimados = presupuesto.getEstimadoByProdAndAno(prod.getId(), ano);
								for (ValorEstimado estimado : estimados) {
									vTotal = vTotal.add(new BigDecimal(estimado.getValor()));
									BigDecimal valorAtual = new BigDecimal(estimado.getValor());
									if (valorAtual.compareTo(BigDecimal.ZERO) != 0) {
										try {
											if (atual.get(ano) != null) {
												atual.put(ano.toString(), atual.get(ano).add(valorAtual));
											} else {
												atual.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println(e);
										}
									}
								}
								etapaAtual = "ESTIMADO";
							}
						}else {
							etapaAtual = "NA";
						}
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}
					if (!etapaAtual.equals("NA")) {
						for (Map.Entry<String, BigDecimal> entry : atual.entrySet()) {
							try {
								System.out.println(etapaAtual);
								System.out.println(contrato.getId());
								System.out.println(entry);
								System.out.println("atual :" + atual);
								ValorGlobal vGlobal = new ValorGlobal();
								vGlobal.setCampanha(campanha);
								vGlobal.setContrato(contrato);
								vGlobal.setEtapa(etapaAtual);
								vGlobal.setAnoFiscal(entry.getKey());

								BigDecimal valorInicial = vTotal.multiply(dispendio).setScale(2, RoundingMode.HALF_DOWN);

								Float fee = 0f;
								List<Honorario> tipoFees = honorarioServiceImpl.getListHonorarioByContrato(contrato.getId());
								for (Honorario tipoFee : tipoFees) {
									if (tipoFee.getNome().equalsIgnoreCase("Percentual Escalonado")) {

										PercentualEscalonado pe = honorarioServiceImpl
												.getPercentualEscalonadoById(tipoFee.getId());
										List<FaixasEscalonado> pefe = pe.getFaixasEscalonamento();
										for (FaixasEscalonado faixa : pefe) {
											PercentualEscalonado acimaDe = honorarioServiceImpl
													.getPercentualEscalonadoById(tipoFee.getId());

											if (valorInicial.compareTo(acimaDe.getAcimaDe()) == 1
													|| valorInicial.compareTo(acimaDe.getAcimaDe()) == 0) {
												fee = acimaDe.getPercentAcimaDe();
											}
											if (valorInicial.compareTo(faixa.getValorInicial()) == 1
													&& valorInicial.compareTo(faixa.getValorFinal()) == -1) {
												fee = faixa.getValorPercentual();
											}
										}
									} else if (tipoFee.getNome().equalsIgnoreCase("Percentual Fixo")) {
										PercentualFixo fixo = honorarioServiceImpl.getPercentualFixoById(tipoFee.getId());
										fee = fixo.getValorPercentual();
									}
								}

								BigDecimal valorTotal = new BigDecimal("0.00");
								BigDecimal percentValue = entry.getValue().multiply(new BigDecimal("0.35")).divide(valorInicial,
										10, RoundingMode.HALF_DOWN);
								valorTotal = valorInicial.multiply(percentValue).setScale(2, RoundingMode.HALF_DOWN);
								if (fee > 0) {
									valorTotal = valorTotal.multiply(new BigDecimal(fee / 100)).setScale(2,
											RoundingMode.HALF_DOWN);
									vGlobal.setErro(false);
								} else {
									valorTotal = valorTotal.setScale(2, RoundingMode.HALF_DOWN);
									vGlobal.setErro(true);
								}
								vGlobal.setFee(fee.toString());
								vGlobal.setValor(valorTotal.toString());

								CreateGlobal(vGlobal);
							} catch (Exception e) {
								System.out.println(e);
								ValorGlobal vGlobal = new ValorGlobal();
								vGlobal.setCampanha(campanha);
								vGlobal.setContrato(contrato);
								vGlobal.setAnoFiscal(entry.getKey());
								vGlobal.setFee("0.0");
								vGlobal.setValor("0.0");
								vGlobal.setEtapa("ERRO");
								vGlobal.setErro(true);
								CreateGlobal(vGlobal);

							}

						}
					}
				}
				

			}
		}

	}

	@Transactional
	public void CreateGlobal(ValorGlobal valorGlobal) {
		this.em.persist(valorGlobal);
	}

	public List<ValorGlobal> getValorGlobalByContrato(Long id) {
		return this.em.createQuery("SELECT vg from ValorGlobal vg WHERE vg.contrato.id = :cId", ValorGlobal.class)
				.setParameter("cId", id).getResultList();
	}

	public void deleteAllFromGlobal() {
		this.em.createQuery("DELETE FROM ValorGlobal").executeUpdate();
	}

	@Transactional
	public void geraValorMixto() {
		List<Contrato> contratosMixto = contratoRepositoryImpl.getContratosByTipo("Misto");
		BigDecimal dispendio = new BigDecimal("0.35");
		deleteAllFromMixto();
		for (Contrato contrato : contratosMixto) {
			System.out.println(contrato.getId() + " Contrato autla");
			HashMap<String, BigDecimal> passado = new HashMap<String, BigDecimal>();
			List<String> anosProd = producaoServiceImpl.getTotalAnos(contrato.getId());
			for (String campanha : anosProd) {
				Integer contador = 0;
				String etapaAtual = "";
				BigDecimal vTotal = new BigDecimal("0.0");
				
				List<Producao> prodByContrato = producaoServiceImpl.getProducaoByAnoAndContrato(contrato.getId(),
						campanha);

				for (Producao prod : prodByContrato) {
					HashMap<String, BigDecimal> atualGlobal = new HashMap<String, BigDecimal>();
					HashMap<String, BigDecimal> atualAnual = new HashMap<String, BigDecimal>();
					try {
						
						ItemValores etapa = itemValoresServiceImpl.getEtapaAtualByProd(prod.getId());
						if (etapa.getValor().equals("APROBADO")) {
							List<String> anoGasto = disponible.getAnoGastoByProducao(prod.getId());
							for (String ano : anoGasto) {
								List<ValorFiscalizado> fiscalizados = disponible.getValoresByProd(prod.getId(), ano);
								for (ValorFiscalizado fiscalizado : fiscalizados) {
									if (contador >= 2) {
										BigDecimal valorAtual = new BigDecimal(fiscalizado.getValor());
										try {
											if (atualAnual.get(ano) != null) {
												atualAnual.put(ano.toString(), atualAnual.get(ano).add(valorAtual));
											} else {
												atualAnual.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
										}
									} else {
										vTotal = vTotal.add(new BigDecimal(fiscalizado.getValor()));
										BigDecimal valorAtual = new BigDecimal(fiscalizado.getValor());
										try {
											if (atualGlobal.get(ano) != null) {
												atualGlobal.put(ano.toString(), atualGlobal.get(ano).add(valorAtual));
											} else {
												atualGlobal.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println(e);
										}
									}
								}
								etapaAtual = "APROBADO";
								contador = contador + 1;
							}

						} else if (etapa.getValor().equals("POSTULADO")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoPostulado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorPostulado> postulados = presupuesto.getPostuladoByProdAndAno(prod.getId(),
										ano);
								for (ValorPostulado postulado : postulados) {
									if (contador >= 2) {
										BigDecimal valorAtual = new BigDecimal(postulado.getValor());
										try {
											if (atualAnual.get(ano) != null) {
												atualAnual.put(ano.toString(), atualAnual.get(ano).add(valorAtual));
											} else {
												atualAnual.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
										}

									} else {
										vTotal = vTotal.add(new BigDecimal(postulado.getValor()));
										BigDecimal valorAtual = new BigDecimal(postulado.getValor());
										try {
											if (atualGlobal.get(ano) != null) {
												atualGlobal.put(ano.toString(), atualGlobal.get(ano).add(valorAtual));
											} else {
												atualGlobal.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println(e);
										}

									}
								}
								etapaAtual = "POSTULADO";
								contador = contador + 1;

							}

						} else if (etapa.getValor().equals("LEVANTAMIENTO") || etapa.getValor().equals("FORMULACIÓN")) {
							List<String> anoGasto = presupuesto.getAnoGastoByProducaoEstimado(prod.getId());
							for (String ano : anoGasto) {
								List<ValorEstimado> estimados = presupuesto.getEstimadoByProdAndAno(prod.getId(), ano);
								for (ValorEstimado estimado : estimados) {
									if (contador >= 2) {
										BigDecimal valorAtual = new BigDecimal(estimado.getValor());
										try {
											if (atualAnual.get(ano) != null) {
												atualAnual.put(ano.toString(), atualAnual.get(ano).add(valorAtual));
											} else {
												atualAnual.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
										}
									} else {
										vTotal = vTotal.add(new BigDecimal(estimado.getValor()));
										BigDecimal valorAtual = new BigDecimal(estimado.getValor());
										try {
											if (atualGlobal.get(ano) != null) {
												atualGlobal.put(ano.toString(), atualGlobal.get(ano).add(valorAtual));
											} else {
												atualGlobal.put(ano.toString(), valorAtual);
											}
										} catch (Exception e) {
											// TODO: handle exception
											System.out.println(e);
										}
									}
								}
								etapaAtual = "Estimado";
								contador = contador + 1;

							}
						}else {
							etapaAtual = "NA";
						}
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}
					if (!etapaAtual.equals("NA")) {
						for (Map.Entry<String, BigDecimal> entryGlobal : atualGlobal.entrySet()) {
							ValorMixto vGlobal = new ValorMixto();
							vGlobal.setCampanha(campanha);
							vGlobal.setContrato(contrato);
							vGlobal.setEtapa(etapaAtual);
							vGlobal.setAnoFiscal(entryGlobal.getKey());

							BigDecimal valorInicial = vTotal.multiply(dispendio).setScale(2, RoundingMode.HALF_DOWN);
							Float fee = 0f;

							List<Honorario> tipoFees = honorarioServiceImpl.getListHonorarioByContrato(contrato.getId());
							for (Honorario tipoFee : tipoFees) {
								if (tipoFee.getNome().equalsIgnoreCase("Percentual Escalonado")) {

									PercentualEscalonado pe = honorarioServiceImpl.getPercentualEscalonadoById(tipoFee.getId());
									List<FaixasEscalonado> pefe = pe.getFaixasEscalonamento();
									for (FaixasEscalonado faixa : pefe) {
										PercentualEscalonado acimaDe = honorarioServiceImpl
												.getPercentualEscalonadoById(tipoFee.getId());

										try {
											if (valorInicial.compareTo(acimaDe.getAcimaDe()) == 1
													|| valorInicial.compareTo(acimaDe.getAcimaDe()) == 0) {
												fee = acimaDe.getPercentAcimaDe();
											}
											if (valorInicial.compareTo(faixa.getValorInicial()) == 1
													&& valorInicial.compareTo(faixa.getValorFinal()) == -1) {
												fee = faixa.getValorPercentual();
											}
										} catch (Exception e) {
											// TODO: handle exception
										}
									}
								} else if (tipoFee.getNome().equalsIgnoreCase("Percentual Fixo")) {
									PercentualFixo fixo = honorarioServiceImpl.getPercentualFixoById(tipoFee.getId());
									fee = fixo.getValorPercentual();
								}
							}
							BigDecimal valorTotal = new BigDecimal("0.00");
							BigDecimal percentValue = entryGlobal.getValue().multiply(new BigDecimal("0.35"))
									.divide(valorInicial, 10, RoundingMode.HALF_DOWN);
							valorTotal = valorInicial.multiply(percentValue).setScale(2, RoundingMode.HALF_DOWN);
							valorTotal = valorTotal.multiply(new BigDecimal(fee / 100)).setScale(2, RoundingMode.HALF_DOWN);
							vGlobal.setFee(fee.toString());
							vGlobal.setValor(valorTotal.toString());
							Create(vGlobal);
						}
						for (Map.Entry<String, BigDecimal> entryAnual : atualAnual.entrySet()) {
							ValorMixto vAnual = new ValorMixto();
							vAnual.setCampanha(campanha);
							vAnual.setContrato(contrato);
							vAnual.setEtapa(etapaAtual);
							vAnual.setAnoFiscal(entryAnual.getKey());

							BigDecimal valorTotal = new BigDecimal("0.00");
							BigDecimal valorInicial = new BigDecimal("0.00");
							if (passado.get("presupuestoPassado " + entryAnual.getKey()) != null) {
								valorInicial = entryAnual.getValue().multiply(dispendio);
								valorInicial = valorInicial.add(passado.get("presupuestoPassado " + entryAnual.getKey()))
										.setScale(2, RoundingMode.HALF_DOWN);
							} else {
								valorInicial = entryAnual.getValue().multiply(dispendio).setScale(2, RoundingMode.HALF_DOWN);
							}
							if (passado.get("presupuestoPassado " + entryAnual.getKey()) != null) {
								passado.put("presupuestoPassado " + entryAnual.getKey(),
										passado.get("presupuestoPassado " + entryAnual.getKey())
												.add(entryAnual.getValue().multiply(dispendio))
												.setScale(2, RoundingMode.HALF_DOWN));
							} else {
								passado.put("presupuestoPassado " + entryAnual.getKey(),
										entryAnual.getValue().multiply(dispendio));
							}

							Float fee = 0f;
							List<Honorario> tipoFees = honorarioServiceImpl.getListHonorarioByContrato(contrato.getId());
							for (Honorario tipoFee : tipoFees) {
								if (tipoFee.getNome().equalsIgnoreCase("Percentual Escalonado")) {

									PercentualEscalonado pe = honorarioServiceImpl.getPercentualEscalonadoById(tipoFee.getId());
									List<FaixasEscalonado> pefe = pe.getFaixasEscalonamento();
									for (FaixasEscalonado faixa : pefe) {
										PercentualEscalonado acimaDe = honorarioServiceImpl
												.getPercentualEscalonadoById(tipoFee.getId());

										if (valorInicial.compareTo(acimaDe.getAcimaDe()) == 1
												|| valorInicial.compareTo(acimaDe.getAcimaDe()) == 0) {
											fee = acimaDe.getPercentAcimaDe();
										}
										if (valorInicial.compareTo(faixa.getValorInicial()) == 1
												&& valorInicial.compareTo(faixa.getValorFinal()) == -1) {
											fee = faixa.getValorPercentual();
										}
									}
								} else if (tipoFee.getNome().equalsIgnoreCase("Percentual Fixo")) {
									PercentualFixo fixo = honorarioServiceImpl.getPercentualFixoById(tipoFee.getId());
									fee = fixo.getValorPercentual();
								}
							}


							if (passado.get(entryAnual.getKey()) != null) {
								valorTotal = valorInicial.multiply(new BigDecimal(fee / 100)).setScale(2,
										RoundingMode.HALF_DOWN);
								valorTotal = valorTotal.subtract(passado.get(entryAnual.getKey())).setScale(2,
										RoundingMode.HALF_DOWN);
							} else {
								valorTotal = valorInicial.multiply(new BigDecimal(fee / 100)).setScale(2,
										RoundingMode.HALF_DOWN);
							}
							vAnual.setFee(fee.toString());
							vAnual.setValor(valorTotal.toString());
							try {
								passado.put(entryAnual.getKey(), valorTotal.add(passado.get(entryAnual.getKey())));
							} catch (Exception e) {
								// TODO: handle exception
								passado.put(entryAnual.getKey(), valorTotal);
							}
							Create(vAnual);
						}
					}
				}
				
			}
		}
	}

	@Transactional
	public void Create(ValorMixto valorMixto) {
		this.em.persist(valorMixto);
	}

	@Transactional
	public void deleteAllFromMixto() {
		this.em.createNativeQuery("DELETE FROM valor_mixto").executeUpdate();
	}

	public List<ValorMixto> getValorMixtoByContrato(Long id) {
		try {
			return this.em.createQuery("SELECT vm from ValorMixto vm WHERE vm.contrato.id = :cId", ValorMixto.class)
					.setParameter("cId", id).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Transactional
	public void geraVN() {
		System.out.println("gerar");
		presupuesto.geraValorEstimado();
		presupuesto.geraValorPostulado();
		presupuesto.geraValorAprovado();
		disponible.geraValorDisponible();
		disponible.geraValorFiscalizado();

		geraValorAnual();
		geraValorGlobal();
		geraValorMixto();
		deleteAllVNCL();
		List<Contrato> contratos = contratoRepositoryImpl.getAll();
		for (Contrato contrato : contratos) {
			try {
				switch (contrato.getTipo()) {
				case "Global":
					List<ValorGlobal> globals = getValorGlobalByContrato(contrato.getId());
					for (ValorGlobal global : globals) {
						ValorNegocioChile vn = new ValorNegocioChile();
						vn.setCampanha(global.getCampanha());
						vn.setAno_fiscal(global.getAnoFiscal());
						vn.setContrato(global.getContrato());
						vn.setEtapa(global.getEtapa());
						vn.setFee(global.getFee());
						vn.setNome_empresa(global.getContrato().getCliente().getRazaoSocial());
						vn.setTipo("Global");
						vn.setValor(global.getValor());
						List<Producao> tipoP = producaoServiceImpl
								.getProducaoByAnoAndContrato(global.getContrato().getId(), global.getCampanha());
						for (Producao p : tipoP) {
							try {
								vn.setTipo_negocio(p.getPreProducao().getTipoNegocio());
							} catch (Exception e) {
								// TODO: handle exception
								vn.setTipo_negocio(null);
							}
						}
						try {

							vn.setSituacao(preProducaoServiceImpl
									.getPreByAnoCliente(global.getContrato().getCliente().getId(), global.getCampanha())
									.getSituacao().toString());
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("erro " + global.getContrato().getCliente().getId());
							vn.setSituacao("");
						}

						Create(vn);
					}

					break;
				case "Anual":
					List<ValorAnual> anuals = getValorAnualByContrato(contrato.getId());
					for (ValorAnual anual : anuals) {
						ValorNegocioChile vn = new ValorNegocioChile();
						vn.setCampanha(anual.getCampanha());
						vn.setAno_fiscal(anual.getAnoFiscal());
						vn.setContrato(anual.getContrato());
						vn.setEtapa(anual.getEtapa());
						vn.setFee(anual.getFee());
						vn.setNome_empresa(anual.getContrato().getCliente().getRazaoSocial());
						vn.setTipo("Anual");
						vn.setValor(anual.getValor());
						List<Producao> tipoP = producaoServiceImpl
								.getProducaoByAnoAndContrato(anual.getContrato().getId(), anual.getCampanha());
						for (Producao p : tipoP) {
							try {
								vn.setTipo_negocio(p.getPreProducao().getTipoNegocio());
							} catch (Exception e) {
								// TODO: handle exception
								vn.setTipo_negocio(null);
							}
						}
						try {
							vn.setSituacao(preProducaoServiceImpl
									.getPreByAnoCliente(anual.getContrato().getCliente().getId(), anual.getCampanha())
									.getSituacao().toString());
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("cliente " + anual.getContrato().getCliente().getId());
							System.out.println("campanha" + anual.getCampanha());
							vn.setSituacao("");
						}
						Create(vn);
					}

					break;

				case "Misto":
					List<ValorMixto> mixtos = getValorMixtoByContrato(contrato.getId());
					for (ValorMixto mixto : mixtos) {
						ValorNegocioChile vn = new ValorNegocioChile();
						vn.setCampanha(mixto.getCampanha());
						vn.setAno_fiscal(mixto.getAnoFiscal());
						vn.setContrato(mixto.getContrato());
						vn.setEtapa(mixto.getEtapa());
						vn.setFee(mixto.getFee());
						vn.setNome_empresa(mixto.getContrato().getCliente().getRazaoSocial());
						vn.setTipo("Mixto");
						vn.setValor(mixto.getValor());
						List<Producao> tipoP = producaoServiceImpl
								.getProducaoByAnoAndContrato(mixto.getContrato().getId(), mixto.getCampanha());
						for (Producao p : tipoP) {
							try {
								vn.setTipo_negocio(p.getPreProducao().getTipoNegocio());
							} catch (Exception e) {
								// TODO: handle exception
								vn.setTipo_negocio(null);
							}
						}
						try {

							vn.setSituacao(preProducaoServiceImpl
									.getPreByAnoCliente(mixto.getContrato().getCliente().getId(), mixto.getCampanha())
									.getSituacao().toString());
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("erro " + mixto.getContrato().getCliente().getId());
							vn.setSituacao("");
						}

						Create(vn);
					}
					break;

				default:
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
		}
	}

	@Transactional
	public void Create(ValorNegocioChile valorNegocioChile) {
		this.em.persist(valorNegocioChile);
	}

	@Transactional
	public void deleteAllVNCL() {
		this.em.createNativeQuery("DELETE FROM valor_negocio_chile").executeUpdate();
	}

	public List<ValorNegocioChile> getAllFromVNCL() {
		return em.createQuery("SELECT v FROM ValorNegocioChile v", ValorNegocioChile.class).getResultList();
	}

}
