package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.cliente.EnderecoCliente;
import br.com.finiciativas.fseweb.repositories.EnderecoClienteRepository;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;

@Repository
public class EnderecoClienteRepositoryImpl implements EnderecoClienteRepository {

	@Autowired
	private EntityManager em;
	
	@Autowired
	private ClienteServiceImpl clienteService;

	@Override
	public EnderecoCliente update(EnderecoCliente enderecoCliente) {

		EnderecoCliente enderAtt = new EnderecoCliente();
		
		System.out.println(" Id do objeto entrando no m�todo: "+enderecoCliente.getId());

		enderAtt.setId(enderecoCliente.getId());
		enderAtt.setLogradouro(enderecoCliente.getLogradouro());
		enderAtt.setCidade(enderecoCliente.getCidade());
		enderAtt.setEstado(enderecoCliente.getEstado());
		enderAtt.setNumero(enderecoCliente.getNumero());
		enderAtt.setComplemento(enderecoCliente.getComplemento());
		enderAtt.setCep(enderecoCliente.getCep());
		enderAtt.setEnderecoNotaFiscalBoleto(enderecoCliente.isEnderecoNotaFiscalBoleto());
		enderAtt.setTipo(enderecoCliente.getTipo());
		enderAtt.setCliente(enderecoCliente.getCliente());
		enderAtt.setBairro(enderecoCliente.getBairro());

		System.out.println(" Id do objeto saindo no m�todo: "+enderAtt.getId());
		
		return this.em.merge(enderAtt);
	}

	public EnderecoCliente getLastItemAdded() {

		String sQuery = "SELECT p " + "FROM EnderecoCliente p " + "WHERE p.id=(SELECT max(id) FROM EnderecoCliente)";

		return em.createQuery(sQuery, EnderecoCliente.class).getSingleResult();
	}

	public List<EnderecoCliente> getAllById(Long id) {
		return em.createQuery("SELECT d FROM EnderecoCliente d WHERE d.cliente.id = :pId", EnderecoCliente.class)
				.setParameter("pId", id).getResultList();
	}

	@Transactional
	public void delete(EnderecoCliente ender) {
		this.em.remove(ender);
	}

	@Override
	@Transactional
	public void create(EnderecoCliente enderecoCliente) {
		this.em.persist(enderecoCliente);
	}

	public EnderecoCliente findById(Long id) {
		return this.em.find(EnderecoCliente.class, id);
	}
	
	public List<EnderecoCliente> getEnderecoClienteNF(Long idCliente) {
		return em.createQuery("SELECT d FROM EnderecoCliente d WHERE d.enderecoNotaFiscalBoleto = 1 AND d.cliente.id = :pId", EnderecoCliente.class)
				.setParameter("pId", idCliente).getResultList();
	}

}
