package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculoRota2030;

@Repository
public class BalanceteCalculoROTARepositoryImpl {

	@Autowired
	private EntityManager em;

	@Transactional
	public BalanceteCalculoRota2030 create(BalanceteCalculoRota2030 balancete) {
			this.em.persist(balancete);
			em.flush();
			
			return balancete;
	}

	public void delete(BalanceteCalculoRota2030 balancete) {
		this.em.remove(balancete);
	}

	@Transactional
	public BalanceteCalculoRota2030 update(BalanceteCalculoRota2030 balancete) {
		return this.em.merge(balancete);
	}

	public List<BalanceteCalculoRota2030> getAll() {
		return this.em.createQuery("SELECT distinct o FROM BalanceteCalculoRota2030 o ", BalanceteCalculoRota2030.class)
				.getResultList();
	}

	public BalanceteCalculoRota2030 findById(Long id) {
		return this.em.find(BalanceteCalculoRota2030.class, id);
	}

	public List<BalanceteCalculoRota2030> getBalanceteCalculoRota2030ByProducao(Long id) {
		em.clear();
		return this.em.createQuery("SELECT o FROM BalanceteCalculoRota2030 o WHERE o.producao.id = :pId ",
				BalanceteCalculoRota2030.class).setParameter("pId", id).getResultList();
	}

	public BalanceteCalculoRota2030 getLastBalanceteCalculoRota2030ByProducao(Long id) {
		
		try {
			return this.em
					.createQuery("SELECT distinct o FROM BalanceteCalculoRota2030 o WHERE o.producao.id = :pId AND o.versao = "
							+ "(SELECT max(b.versao) FROM BalanceteCalculoRota2030 b)", BalanceteCalculoRota2030.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("Sem balancete");
			BalanceteCalculoRota2030 balancete = new BalanceteCalculoRota2030();
			balancete = null;
			return balancete;
		}
	}
	
	public List<BalanceteCalculoRota2030> getAllBalanceteByAno(String ano){
		return this.em.createQuery("SELECT o FROM BalanceteCalculoRota2030 o WHERE o.producao.ano = :pAno", BalanceteCalculoRota2030.class).setParameter("pAno", ano).getResultList();
	}
	
}
