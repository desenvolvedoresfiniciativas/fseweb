package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.instituicao.ContatoInstituicao;
import br.com.finiciativas.fseweb.models.instituicao.Instituicao;
import br.com.finiciativas.fseweb.repositories.ContatoInstituicaoRepository;

@Repository
public class ContatoInstituicaoRepositoryImpl implements ContatoInstituicaoRepository {

	@Autowired
	private EntityManager em;

	@Override
	@SuppressWarnings("unchecked")
	public List<ContatoInstituicao> getContatosInstituicao(Long id) {
		Instituicao instituicao = new Instituicao();
		instituicao.setId(id);
		return em.createQuery("SELECT c FROM Instituicao insti JOIN insti.contatos c WHERE insti = :pInstituicao")
				.setParameter("pInstituicao", instituicao)
				.getResultList();

	} 

	@Override
	public void create(ContatoInstituicao contatoInstituicao, Long id) {
		
		contatoInstituicao.setAtivo(true);
		
		em.persist(contatoInstituicao);
		
		Instituicao empresa = em.find(Instituicao.class, id);
		
		List<ContatoInstituicao> contatos = empresa.getContatos();
		
		contatos.add(contatoInstituicao);
		
		em.merge(empresa);

	}

	@Override
	public void update(ContatoInstituicao contatoInstituicao) {
		
		ContatoInstituicao contatoInstituicaoAtt = new ContatoInstituicao(contatoInstituicao);
		
		contatoInstituicaoAtt.setAtivo(true);
		
		em.merge(contatoInstituicaoAtt);
		
	}

	@Override
	public void remove(ContatoInstituicao contatoInstituicao) {

		ContatoInstituicao ce = em.find(ContatoInstituicao.class, contatoInstituicao.getId());

		ce.setAtivo(false);

		em.merge(ce);

	}

	@Override
	public ContatoInstituicao findById(Long id) {
		return em.find(ContatoInstituicao.class, id);
	}

	@Override
	public Long getIdInstituicaoOfContact(Long id) {
		TypedQuery<Long> query = em.createQuery("SELECT insti.id FROM Instituicao insti "
				+ "INNER JOIN insti.contatos ce WHERE ce.id = :id", Long.class)
				.setParameter("id", id);
		return query.getSingleResult();
	}
	
}
