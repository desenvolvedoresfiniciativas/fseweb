package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.lista.ValoresLista;

@Repository
public class ValoresListaRepositoryImpl {

	@Autowired
	private EntityManager em;

	public List<ValoresLista> getAll() {
		return this.em.createQuery("SELECT distinct l FROM ValoresLista l ", ValoresLista.class).getResultList();
	}

	@Transactional
	public void create(ValoresLista lista) {
		ValoresLista valorAdd = new ValoresLista();
		valorAdd.setId(lista.getId());
		valorAdd.setItem(lista.getItem());
		valorAdd.setValor(lista.getValor());
		this.em.merge(lista);
	}
	
	public void delete(ValoresLista lista) {
		this.em.remove(lista);
	}

	@Transactional
	public ValoresLista update(ValoresLista lista) {

		ValoresLista listaUpp = this.em.find(ValoresLista.class, lista.getId());
		listaUpp.setValor(lista.getValor());

		return this.em.merge(listaUpp);
	}

	public List<ValoresLista> findByItemId(Long id) {
		return this.em.createQuery("SELECT distinct l FROM ValoresLista l JOIN l.item i WHERE i.id = :pId", ValoresLista.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
