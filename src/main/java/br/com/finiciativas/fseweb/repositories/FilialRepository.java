package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.Filial;
import br.com.finiciativas.fseweb.models.consultor.Consultor;

public interface FilialRepository {
	
	List<Filial> getAll();
	
	void create(Filial filial);
	
	Filial update(Filial filial);
	
	Filial findById(Long id);
	
	void remove(Long id);
	
	List<Consultor> getConsultores(Long idFilial);
	
}
