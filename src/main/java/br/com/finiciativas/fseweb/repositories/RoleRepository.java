package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.consultor.Role;

public interface RoleRepository {
	
	List<Role> getAll();
	
}

