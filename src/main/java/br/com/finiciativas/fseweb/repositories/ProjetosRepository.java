package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.Projetos;

public interface ProjetosRepository {

	void create(Projetos projetos);

	void delete(Projetos projetos);

	Projetos update(Projetos projetos);
	
	List<Projetos> getAll();
	
	Projetos findById(Long id);
	
	List<Projetos> getProjetosByProducao(Long id);
	
}
