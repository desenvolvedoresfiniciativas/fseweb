package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.IeM.Institucional;


@Repository
public class InstitucionalRepositoryImpl {
	
	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(Institucional reco) {
		
		reco.setOportunidade("Institucional");
		
		this.em.persist(reco);
	}

	public void delete(Institucional reco) {
		this.em.remove(reco);
	}

	@Transactional
	public Institucional update(Institucional reco) {
		return this.em.merge(reco);
	}

	public List<Institucional> getAll() {
		return this.em.createQuery("SELECT distinct o FROM Institucional o ", Institucional.class)
				.getResultList();
	}

	public Institucional findById(Long id) {
		return this.em.find(Institucional.class, id);
	}

}
