package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.CategoriaConsultor;
import br.com.finiciativas.fseweb.models.consultor.Consultor;

public interface ConsultorRepository {
	
	Consultor findById(Long id);
	
	Consultor findByUsername(String username);
	
	List<Consultor> getAll();
	
	List<Consultor> getAllComercialAndConsultor();
	
	List<Consultor> getAtivo();
	
	void create(Consultor consultor);
	
	void delete(Long id);
	
	Consultor update(Consultor consultor);
	
	boolean enviarEmailBoasVindas(Consultor consultor);
	
	List<Consultor> getConsultorByCargo(Cargo cargo);
	
	List<Consultor> getConsultoresSemEquipe();
	
	public List<Consultor> getConsultores();
	
	Consultor getConsultorBySigla();
	
	List<Consultor> getCategoriaConsultor(CategoriaConsultor categoriaConsultor);

	List<Consultor> getLideres();

	List<Consultor> getCoordenadores();

	List<Consultor> getGerentes();

	Consultor getUserByEmail(String email);
	
	List<Consultor> getAtivosAndInativos();

	List<Consultor> getComerciais(Cargo cargo, Cargo especialistaComercial, Cargo supervisorAnalistaComercial,
			Cargo analistaComercial, Cargo coordenadorComercial, Cargo coordenadorTecnico, Cargo liderTecnico,
			Cargo consultorLiderTecnico);

	List<Consultor> getAllComerciais(Cargo cargo, Cargo especialistaComercial, Cargo supervisorAnalistaComercial,
			Cargo analistaComercial, Cargo coordenadorComercial, Cargo coordenadorTecnico, Cargo liderTecnico,
			Cargo consultorLiderTecnico);


	
}
