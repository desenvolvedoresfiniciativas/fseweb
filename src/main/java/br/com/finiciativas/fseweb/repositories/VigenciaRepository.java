package br.com.finiciativas.fseweb.repositories;

import br.com.finiciativas.fseweb.models.vigencia.Vigencia;

public interface VigenciaRepository {
	
	Vigencia update(Vigencia vigencia);

}
