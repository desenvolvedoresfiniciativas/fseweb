package br.com.finiciativas.fseweb.repositories.impl;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.ProdutoRepository;

@Repository
public class ProdutoRepositoryImpl implements ProdutoRepository {

	@Autowired
	private EntityManager em;

	public List<Produto> getAll() {
		return this.em.createQuery("SELECT distinct p FROM Produto p ", Produto.class).getResultList();
	}

	@Transactional
	public void create(Produto produto) {
		produto.setDataCriacao(LocalDate.now());
		this.em.persist(produto);
	}

	public void delete(Produto produto) {
		this.em.remove(produto);
	}

	@Transactional
	public Produto update(Produto produto) {

		Produto produtoAtt = this.em.find(Produto.class, produto.getId());

		produtoAtt.setNome(produto.getNome());
		produtoAtt.setDescricao(produto.getDescricao());
		produtoAtt.setEtapasTrabalho(produto.getEtapasTrabalho());

		return this.em.merge(produtoAtt);
	}

	public Produto findById(Long id) {
		return this.em.find(Produto.class, id);
	}

	@Override
	public List<EtapaTrabalho> getEtapasTrabalho(Long idProduto) {

		String sQuery = "SELECT et FROM EtapaTrabalho et JOIN FETCH et.produto p WHERE p.id = :pIdProduto";

		return em.createQuery(sQuery, EtapaTrabalho.class).setParameter("pIdProduto", idProduto).getResultList();
	}

	@Override
	public List<Tarefa> getTarefas(Long idEtapa) {

		String sQuery = "SELECT t FROM Tarefas t JOIN FETCH t.etapaTrabalho p WHERE p.id = :pIdProduto";

		return em.createQuery(sQuery, Tarefa.class).setParameter("pIdProduto", idEtapa).getResultList();
	}

}
