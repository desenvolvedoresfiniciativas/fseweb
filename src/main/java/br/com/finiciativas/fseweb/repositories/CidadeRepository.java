package br.com.finiciativas.fseweb.repositories;

import br.com.finiciativas.fseweb.models.Cidade;

public interface CidadeRepository {
	
	Cidade findCidade(Cidade cidade);
	
}
