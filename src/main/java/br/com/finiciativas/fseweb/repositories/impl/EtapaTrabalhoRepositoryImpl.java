package br.com.finiciativas.fseweb.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.repositories.EtapaTrablhoRepository;
import br.com.finiciativas.fseweb.services.impl.TarefaServiceImpl;


@Repository
public class EtapaTrabalhoRepositoryImpl implements EtapaTrablhoRepository {
	
	@Autowired
	private EntityManager em;
	
	@Autowired
	private TarefaServiceImpl TarefaService;
	
	@Transactional
	@Override
	public void create(EtapaTrabalho etapaTrabalho) {
		em.persist(etapaTrabalho);
	}
	
	@Override
	public List<EtapaTrabalho> getAll() {
		return em.createQuery("SELECT distinct et FROM EtapaTrabalho et ", EtapaTrabalho.class)
				.getResultList();
	}

	@Override
	public Tarefa getTarefas(Long idEtapaTrabalho) {
		return em.
				createQuery("SELECT et FROM Tarefa et"
						+ " JOIN FETCH etapaTrabalho ett "
						+ " WHERE ett.id = :pIdEtapaTrabalho", Tarefa.class)
				.setParameter("pIdEtapaTrabalho", idEtapaTrabalho)
				.getSingleResult();
	}
	
	@Override
	public EtapaTrabalho findById(Long id) {
		return this.em.find(EtapaTrabalho.class, id);
	}
	
	@Transactional
	@Override
	public EtapaTrabalho update(EtapaTrabalho etapaTrabalho) {
		EtapaTrabalho etapaTrabalhoAtt = this.em.find(EtapaTrabalho.class, etapaTrabalho.getId());
		
		etapaTrabalhoAtt.setNome(etapaTrabalho.getNome());
		etapaTrabalhoAtt.setDescricao(etapaTrabalho.getDescricao());
		etapaTrabalhoAtt.setTarefas(etapaTrabalho.getTarefas());
		etapaTrabalhoAtt.setSubProducao(etapaTrabalho.isSubProducao());
		
		return this.em.merge(etapaTrabalhoAtt);
	}

	@Override
	public EtapaTrabalho getLastAdded(){
		String sQuery = "SELECT et " + "FROM EtapaTrabalho et " + "WHERE et.id=(SELECT max(id) FROM EtapaTrabalho)";

		return em.createQuery(sQuery, EtapaTrabalho.class).getSingleResult();
	}
	
	@Override
	public void bindEtapaToTarefas(EtapaTrabalho etapa){
		List<Tarefa> tar = new ArrayList<>();
		List<EtapaTrabalho> etapas = new ArrayList<>();
		etapas.add(etapa);
		tar.addAll(etapa.getTarefas());
		System.out.println(etapa.getNome());
		List<Tarefa> tare = new ArrayList<>();
		for (Tarefa tarefa : tar) {
			tare.add(TarefaService.findById(tarefa.getId()));
		}
		
		Long id = null;
		
		for (Tarefa tarefa : tare) {
			tarefa.setEtapas(etapas);
			TarefaService.update(tarefa);
		}

	}
}
