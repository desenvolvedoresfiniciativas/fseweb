package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;

public interface ComissaoRepository {

	Comissao create(Comissao comissao);

	Comissao update(Comissao comissao);

	Comissao findById(Long id);

	void delete(Comissao comissao);

	Comissao getLastAdded();

	List<Comissao> getAll();

	List<Comissao> getComissaoByConsultor(Long id);

	List<Comissao> getComissaoByGestor(Long id);

	List<Comissao> getComissaoByAnalista(Long id);

	Comissao findByProducao(Long id);

	Comissao checkIfComissaoExistWithProducao(Long id);

	List<Comissao> getAllByProducao(Long id);
	
	public void createComissaoByNota(NotaFiscal nota);

	List<Comissao> getComissaoByFaturamento(Long id);

	List<Comissao> getComissaoAtivaByConsultor(Long id);

	List<Comissao> getComissoesReaisByContrato(Long id);

	void updateComissaoByNota(Comissao comissao);

}
