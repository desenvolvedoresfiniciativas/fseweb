package br.com.finiciativas.fseweb.repositories.impl;

import static org.junit.Assert.assertArrayEquals;

import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Departamento;
import br.com.finiciativas.fseweb.models.PeriodoPagamento;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Premio;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.repositories.PremioRepository;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.NotaFiscalServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PeriodoPagamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.PremioServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Repository
public class PremioRepositoryImpl implements PremioRepository {

	private static final Locale LOCAL = new Locale("pt", "BR");

	@Autowired
	private EntityManager em;

	@Autowired
	private PeriodoPagamentoServiceImpl periodoPagamentoService;

	@Autowired
	private PremioServiceImpl premioService;

	@Autowired
	private ValorNegocioServiceImpl valorNegocioService;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private NotaFiscalServiceImpl notaFiscalService;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	
	@Transactional
	public void create(Premio premio) {
		em.persist(premio);
		em.flush();
		
	}

	@Override
	public Premio update(Premio premio) {
		return this.em.merge(premio);
	}

	@Override
	public void delete(Premio premio) {
		this.em.remove(premio);
	}

	@Override
	public Premio findById(Long id) {
		return this.em.find(Premio.class, id);
	}

	@Override
	public Premio findByConsultorAndCampanha(Long idConsultor, String campanha) {
		try {
			return this.em
					.createQuery("SELECT p FROM Premio p WHERE p.consultor.id = :pId and p.campanha = :pCampanha",
							Premio.class)
					.setParameter("pId", idConsultor).setParameter("pCampanha", campanha).getSingleResult();
		} catch (Exception e) {
			System.out.println("Erro no metodo PremioRepositoryImpl.findByConsultorAndCampanha \n" + e);
			return null;
		}
	}

	@Override
	public List<Premio> getAll() {
		System.out.println("Construir metodo getAll");
		return null;
	}

	@Override
	public List<Premio> getAllByConsultor(Long id) {
		try {
			return this.em.createQuery("SELECT p FROM Premio p WHERE p.consultor.id = :pId", Premio.class)
					.setParameter("pId", id).getResultList();

		} catch (Exception e) {
			System.out.println("Erro no metodo: Premio.REpositoryImpl.getPremioByConsultor");
			return null;
		}
	}

	@Override
	public List<Premio> getAllByAnalista(Long id) {
		System.out.println("Construir metodo getAllByAnalista");
		return null;
	}

	@Override
	public List<Premio> getAllByGestor(Long id) {
		System.out.println("Construir metodo getAllByGestor");
		return null;
	}

	@Override
	public List<Premio> getAllByProducao(Long id) {
		try {
			return this.em.createQuery("SELECT DISTINCT p FROM Premio p WHERE p.producao.id = :pId", Premio.class)
					.setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println("Erro no metodo: PremioRepositoryImpl.getAllByProducao");
			return null;
		}
	}

	@Override
	public List<Premio> getAllByContrato(Long id) {
		try {
			return this.em.createQuery("SELECT DISTINCT p FROM Premio p WHERE p.contrato.id = :pId", Premio.class)
					.setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println("Erro no metodo: PremioRepositoryImpl.getAllByProducao");
			return null;
		}
	}

	@Override
	public Premio getLastAdded() {
		return null;
	}

	@SuppressWarnings("static-access")
	@Transactional
	public void gerarPremioByCampanha2020() {
		System.out.println("Gerando premiacoes da campanha de 2020");
		String campanha = "2020";

		List<Consultor> consultores = consultorService.getAllComercialAndConsultor();

		for (Consultor consultor : consultores) {
			try {
				// System.out.println("\nConsultor nome: " + consultor.getNome() + " Id: " +
				// consultor.getId());
				boolean premioNovo;
				Premio premio = new Premio();
				if (premioService.checkIfExistsByConsultorAndCampanha(consultor.getId(), campanha) == false) {
					premioNovo = true;
					System.out.println("Consultor nao possui premio, criando um novo");
					premio.setConsultor(consultor);
					premio.setDataDeRecebimentoCliente("Nao inserido");
					premio.setDataRecebimentoFuncionario("Nao inserido");
					premio.setObservacoes("Sem observacoes");
					premio.setCampanha(campanha);
				} else {
					premioNovo = false;
					System.out.println("Consultor possui premio, atualizando premio atual");
					premio = premioService.findByConsultorAndCampanha(consultor.getId(), campanha);

					premio.setConsultor(premio.getConsultor());
					premio.setDataDeRecebimentoCliente(premio.getDataDeRecebimentoCliente());
					premio.setDataRecebimentoFuncionario(premio.getDataRecebimentoFuncionario());
					premio.setObservacoes(premio.getObservacoes());
					premio.setCampanha(campanha);
					premio.setPorcentagemPremio(premio.getPorcentagemPremio());
				}

				float VNAtingido = 0;
				List<ValorNegocio> valoresNegocioConsultor = valorNegocioService
						.getAllByConsultorAndNN2020(consultor.getId(), campanha);

				float VNFinalizados = 0;
				for (ValorNegocio valorNegocio : valoresNegocioConsultor) {
					VNAtingido = (valorNegocio.getValorBase().floatValue() + VNAtingido);

					if (valorNegocio.getModoValor().equalsIgnoreCase("Finalizado")) {
						VNFinalizados = VNFinalizados + 1;
					}
				}

				// System.out.println("Quantidade de VN's encontrados: " +
				// valoresNegocioConsultor.size());
				// System.out.println("Quantidade de VN's finalizados: " + VNFinalizados);

				premio.setValorNegocioRealizado(VNAtingido);
				premio.setFaturamentoRealizado(VNFinalizados);

				boolean KPIDesbloqueado = false;

				float premioKPIEficiencia = 0;
				float premioKPIServicos = 0;
				float premioKPITicketMedio = 0;
				float premioKPIFluxoDeCaixa = 0;
				float valorPremio = 0;

				// if(consultor.getCategoriaConsultor().equalsIgnoreCase("junior")){
				if (consultor.getCategoriaConsultor() == consultor.getCategoriaConsultor().JUNIOR) {
					// System.out.println("Consultor Junior encontrado!");

					premio.setValorNegocioPretendido(600000);// VN a ser alcancado pelo consultor Pleno = R$ 600.000,00
					float VNPretendido = premio.getValorNegocioPretendido();

					String VNPretendidoString = Float.toString(VNPretendido);
					DecimalFormat dfReal = new DecimalFormat("#,###,###.00");
					VNPretendidoString = dfReal.format(VNPretendido);

					// System.out.println("VN Pretendido: " + VNPretendidoString);

					String VNAtingidoString = Float.toString(VNAtingido);
					VNAtingidoString = dfReal.format(VNAtingido);

					// System.out.println("VN Atingido: " + VNAtingidoString);

					// float porcentagemVN = VNAtingido / VNPretendido;

					// String porcentagemVNString = Float.toString(porcentagemVN);
					// DecimalFormat dfPorcentagem = new DecimalFormat("###%");
					// porcentagemVNString = dfPorcentagem.format(porcentagemVN);

					// System.out.println("Porcentagem de VN: " + porcentagemVNString);

					if (VNAtingido >= (0.70 * VNPretendido)) {
						// System.out.println("VN maior que 70%");

						premioKPIEficiencia = 3000;
						premioKPIServicos = 3000;
						premioKPITicketMedio = 3000;
						premioKPIFluxoDeCaixa = 3000;

						KPIDesbloqueado = true;

					}
					if (VNAtingido >= (0.80 * VNPretendido)) {
						// System.out.println("VN maior que 80%");

						premioKPIEficiencia = 6000;
						premioKPIServicos = 6000;
						premioKPITicketMedio = 6000;
						premioKPIFluxoDeCaixa = 3000;

						KPIDesbloqueado = true;
					}

					if (KPIDesbloqueado == true) {
						premio.setEficienciaPropostasPretendido(52);
						premio.setEficienciaContratosPretendido(12);
						premio.setTicketMedioPretendido(50000);

						float propostasPretendidas = premio.getEficienciaPropostasPretendido();
						float contratosPretendidos = premio.getEficienciaContratosPretendido();

						// =====================L�GICA DE PR�MIOS==============================//
						// ====================EFICIENCIA======================================//
						// Eficiencia: Quantas propostas viraram contratos(propostas / contratos)
						// Quantidade de propostas e contratos atingidos vir� do CRM
						float propostasAtingidas = 52;
						premio.setEficienciaPropostasRealizado(propostasAtingidas);

						List<Contrato> listContratosFechados = contratoService.getAllByComercial(consultor.getId());
						float contratosAtingidos = listContratosFechados.size();

						// System.out.println("Contratos pretendidos: " +
						// premio.getEficienciaContratosPretendido() + " Contratos atingidos: " +
						// contratosAtingidos);
						// System.out.println("Propostas pretendidas: " +
						// premio.getEficienciaPropostasPretendido() + " Propostas atingidas: " +
						// propostasAtingidas);

						premio.setEficienciaContratosRealizado(contratosAtingidos);

						float porcentagemEficiencia = propostasAtingidas / contratosAtingidos;

						// String porcentagemEficienciaString = Float.toString(porcentagemEficiencia);
						// DecimalFormat dfPorcentagem2 = new DecimalFormat("###.00%");

						// porcentagemEficienciaString = dfPorcentagem2.format(porcentagemEficiencia);

						// System.out.println("PorcentagemEficiencia: " + porcentagemEficienciaString);

						float resultadoPremioKPIEficiencia = premioKPIEficiencia * porcentagemEficiencia;
						// System.out.println("Resultado Premio Eficiencia: " +
						// resultadoPremioKPIEficiencia);

						if (contratosAtingidos >= contratosPretendidos && propostasAtingidas >= propostasPretendidas) {
							if (resultadoPremioKPIEficiencia >= premioKPIEficiencia) { // Acima do limite do pr�mio do
																						// KPI Eficiencia
								premio.setPremioKPIEficiencia(premioKPIEficiencia);
								valorPremio = valorPremio + premioKPIEficiencia;
							} else {
								premio.setPremioKPIEficiencia(resultadoPremioKPIEficiencia);
								valorPremio = valorPremio + premioKPIEficiencia;
							}
							// System.out.println("KPI Eficiencia check!");
						}

						// ====================SERVICOS========================================//
						if (contratosAtingidos >= contratosPretendidos) {
							premio.setPremioKPIServicos(premioKPIServicos);
							valorPremio = valorPremio + premioKPIServicos;
							// System.out.println("KPI Servicos check!");
						}
						// ===================TICKET MEDIO=====================================//
						float ticketMedioPretendido = premio.getTicketMedioPretendido();

						float ticketMedioAtingido = VNAtingido / premio.getEficienciaContratosRealizado();
						premio.setTicketMedioRealizado(ticketMedioAtingido);

						// String ticketMedioAtingidoString = Float.toString(ticketMedioAtingido);
						// ticketMedioAtingidoString = dfReal.format(ticketMedioAtingido);

						// System.out.println("Ticket medio pretendido: " + ticketMedioPretendido + "
						// Ticket medio atingido: " + ticketMedioAtingidoString);

						if (ticketMedioAtingido >= (0.70 * ticketMedioPretendido)) {
							premio.setPremioKPITicketMedio(premioKPITicketMedio);
							valorPremio = valorPremio + premioKPITicketMedio;
							// System.out.println("KPI Ticket Medio check!");
						} else {
							premio.setPremioKPITicketMedio(0);
						}

						// =====================FLUXO DE CAIXA=================================//
						premio.setFaturamentoPretendido((float) (valoresNegocioConsultor.size() * 0.70));
						premio.setFaturamentoRealizado(VNFinalizados);

						// System.out.println("Valores de negocio pretendidos: " +
						// premio.getFaturamentoPretendido());
						// System.out.println("Valores de negocio finalizados: " +
						// premio.getFaturamentoRealizado());

						// float porcentagemFluxoCaixa = premio.getFaturamentoRealizado() /
						// premio.getFaturamentoPretendido();
						// System.out.println("Porcentagem de fluxo de caixa: " +
						// porcentagemFluxoCaixa);

						if (premio.getFaturamentoRealizado() >= premio.getFaturamentoPretendido()) {// Garantir que 70%
																									// dos NN sejam
																									// faturados ate o
																									// termino do ano
																									// fiscal Seguinte
																									// ao inicio da
																									// campanha
							premio.setPremioKPIFluxoDeCaixa(premioKPIFluxoDeCaixa);
							valorPremio = valorPremio + premioKPIFluxoDeCaixa;
							// System.out.println("KPI Fluxo de caixa check!");
						} else {
							premio.setPremioKPIFluxoDeCaixa(0);
						}

						if (valorPremio > 21000) {
							valorPremio = 21000;
						}
						String stringValorPremio = Float.toString(valorPremio);
						//premio.setValorPremio(stringValorPremio);// Limite do valor do premio para Junior = R$ 21.000,00

					} else {
						premio.setPremioKPIEficiencia(0);
						premio.setPremioKPIServicos(0);
						premio.setPremioKPITicketMedio(0);
						premio.setPremioKPIFluxoDeCaixa(0);
						//premio.setValorPremio("0");
					}

				} else if (consultor.getCategoriaConsultor() == consultor.getCategoriaConsultor().PLENO) {
					// System.out.println("Consultor Pleno encontrado!");

					premio.setValorNegocioPretendido(1100000);// VN a ser alcancado pelo consultor Pleno = R$
																// 1.100.000,00
					float VNPretendido = premio.getValorNegocioPretendido();

					// String VNPretendidoString = Float.toString(VNPretendido);
					// DecimalFormat dfReal = new DecimalFormat("#,###,###.00");
					// VNPretendidoString = dfReal.format(VNPretendido);
					//
					// System.out.println("VN Pretendido: " + VNPretendidoString);
					//
					// String VNAtingidoString = Float.toString(VNAtingido);
					// VNAtingidoString = dfReal.format(VNAtingido);
					//
					// System.out.println("VN Atingido: " + VNAtingidoString);

					// float porcentagemVN = VNAtingido / VNPretendido;

					// String porcentagemVNString = Float.toString(porcentagemVN);
					// DecimalFormat dfPorcentagem = new DecimalFormat("###%");
					// porcentagemVNString = dfPorcentagem.format(porcentagemVN);

					// System.out.println("Porcentagem de VN: " + porcentagemVNString);

					if (VNAtingido >= (0.70 * VNPretendido)) {
						// System.out.println("VN maior que 70%");

						premioKPIEficiencia = 5500;
						premioKPIServicos = 5500;
						premioKPITicketMedio = 5500;
						premioKPIFluxoDeCaixa = 5500;

						KPIDesbloqueado = true;

					}
					if (VNAtingido >= (0.80 * VNPretendido)) {
						// System.out.println("VN maior que 80%");

						premioKPIEficiencia = 11000;
						premioKPIServicos = 11000;
						premioKPITicketMedio = 11000;
						premioKPIFluxoDeCaixa = 5500;

						KPIDesbloqueado = true;
					}

					if (KPIDesbloqueado == true) {
						premio.setEficienciaPropostasPretendido(80);
						premio.setEficienciaContratosPretendido(20);
						premio.setTicketMedioPretendido(59081);

						float propostasPretendidas = premio.getEficienciaPropostasPretendido();
						float contratosPretendidos = premio.getEficienciaContratosPretendido();

						// =====================L�GICA DE PR�MIOS==============================//
						// ====================EFICIENCIA======================================//
						// Eficiencia: Quantas propostas viraram contratos(propostas / contratos)
						// Quantidade de propostas e contratos atingidos vir� do CRM
						float propostasAtingidas = 70;
						premio.setEficienciaPropostasRealizado(propostasAtingidas);

						List<Contrato> listContratosFechados = contratoService.getAllByComercial(consultor.getId());
						float contratosAtingidos = listContratosFechados.size();

						// System.out.println("Contratos pretendidos: " +
						// premio.getEficienciaContratosPretendido() + " Contratos atingidos: " +
						// contratosAtingidos);
						// System.out.println("Propostas pretendidas: " +
						// premio.getEficienciaPropostasPretendido() + " Propostas atingidas: " +
						// propostasAtingidas);

						premio.setEficienciaContratosRealizado(contratosAtingidos);

						float porcentagemEficiencia = propostasAtingidas / contratosAtingidos;

						// String porcentagemEficienciaString = Float.toString(porcentagemEficiencia);
						// DecimalFormat dfPorcentagem2 = new DecimalFormat("###.00%");

						// porcentagemEficienciaString = dfPorcentagem2.format(porcentagemEficiencia);

						// System.out.println("PorcentagemEficiencia: " + porcentagemEficienciaString);

						float resultadoPremioKPIEficiencia = premioKPIEficiencia * porcentagemEficiencia;
						// System.out.println("Teste Eficiencia: " + resultadoPremioKPIEficiencia);

						if (contratosAtingidos >= contratosPretendidos && propostasAtingidas >= propostasPretendidas) {
							if (resultadoPremioKPIEficiencia >= premioKPIEficiencia) { // Acima do limite do pr�mio do
																						// KPI Eficiencia
								premio.setPremioKPIEficiencia(premioKPIEficiencia);
								valorPremio = valorPremio + premioKPIEficiencia;
							} else {
								premio.setPremioKPIEficiencia(resultadoPremioKPIEficiencia);
								valorPremio = valorPremio + premioKPIEficiencia;
							}
							// System.out.println("KPI Eficiencia check!");
						}

						// ====================SERVICOS========================================//
						if (contratosAtingidos >= contratosPretendidos) {
							premio.setPremioKPIServicos(premioKPIServicos);
							valorPremio = valorPremio + premioKPIServicos;
							// System.out.println("KPI Servicos check!");
						}
						// ===================TICKET MEDIO=====================================//
						float ticketMedioPretendido = premio.getTicketMedioPretendido();

						float ticketMedioAtingido = VNAtingido / premio.getEficienciaContratosRealizado();
						premio.setTicketMedioRealizado(ticketMedioAtingido);

						// String ticketMedioAtingidoString = Float.toString(ticketMedioAtingido);
						// ticketMedioAtingidoString = dfReal.format(ticketMedioAtingido);

						// System.out.println("Ticket medio pretendido: " + ticketMedioPretendido + "
						// Ticket medio atingido: " + ticketMedioAtingidoString);

						if (ticketMedioAtingido >= (0.70 * ticketMedioPretendido)) {
							premio.setPremioKPITicketMedio(premioKPITicketMedio);
							valorPremio = valorPremio + premioKPITicketMedio;
							// System.out.println("KPI Ticket Medio check!");
						} else {
							premio.setPremioKPITicketMedio(0);
						}

						// =====================FLUXO DE CAIXA=================================//
						premio.setFaturamentoPretendido((float) (valoresNegocioConsultor.size() * 0.70));
						premio.setFaturamentoRealizado(VNFinalizados);

						// System.out.println("Valores de negocio pretendidos: " +
						// premio.getFaturamentoPretendido());
						// System.out.println("Valores de negocio finalizados: " +
						// premio.getFaturamentoRealizado());

						// float porcentagemFluxoCaixa = premio.getFaturamentoRealizado() /
						// premio.getFaturamentoPretendido();
						// System.out.println("Porcentagem de fluxo de caixa: " +
						// porcentagemFluxoCaixa);

						if (premio.getFaturamentoRealizado() >= premio.getFaturamentoPretendido()) {// Garantir que 70%
																									// dos NN sejam
																									// faturados ate o
																									// termino do ano
																									// fiscal Seguinte
																									// ao inicio da
																									// campanha
							premio.setPremioKPIFluxoDeCaixa(premioKPIFluxoDeCaixa);
							valorPremio = valorPremio + premioKPIFluxoDeCaixa;
							// System.out.println("KPI Fluxo de caixa check!");
						} else {
							premio.setPremioKPIFluxoDeCaixa(0);
						}

						if (valorPremio > 38500) {
							valorPremio = 38500;
						}
						String stringValorPremio = Float.toString(valorPremio);
						//premio.setValorPremio(stringValorPremio);// Limite do valor do premio para Pleno = R$ 38.500,00

					} else {
						premio.setPremioKPIEficiencia(0);
						premio.setPremioKPIServicos(0);
						premio.setPremioKPITicketMedio(0);
						premio.setPremioKPIFluxoDeCaixa(0);
						//premio.setValorPremio("0");
					}

				} else if (consultor.getCategoriaConsultor() == consultor.getCategoriaConsultor().SENIOR
						|| consultor.getCargo() == consultor.getCargo().CONSULTOR_LIDER_TECNICO) {
					// System.out.println("Consultor Senior ou Consultor Lider Tecnico
					// encontrado!");

					premio.setValorNegocioPretendido(1900000);// VN a ser alcancado pelo consultor Senior = R$
																// 1.900.000,00
					float VNPretendido = premio.getValorNegocioPretendido();

					// String VNPretendidoString = Float.toString(VNPretendido);
					// DecimalFormat dfReal = new DecimalFormat("#,###,###.00");
					// VNPretendidoString = dfReal.format(VNPretendido);
					//
					// System.out.println("VN Pretendido: " + VNPretendidoString);
					//
					// String VNAtingidoString = Float.toString(VNAtingido);
					// VNAtingidoString = dfReal.format(VNAtingido);
					//
					// System.out.println("VN Atingido: " + VNAtingidoString);

					// float porcentagemVN = VNAtingido / VNPretendido;
					//
					// String porcentagemVNString = Float.toString(porcentagemVN);
					// DecimalFormat dfPorcentagem = new DecimalFormat("###%");
					// porcentagemVNString = dfPorcentagem.format(porcentagemVN);

					// System.out.println("Porcentagem de VN: " + porcentagemVNString);

					if (VNAtingido >= (0.70 * VNPretendido)) {
						// System.out.println("VN maior que 70%");

						premioKPIEficiencia = 5500;
						premioKPIServicos = 5500;
						premioKPITicketMedio = 5500;
						premioKPIFluxoDeCaixa = 9500;

						KPIDesbloqueado = true;

					}
					if (VNAtingido >= (0.80 * VNPretendido)) {
						// System.out.println("VN maior que 80%");

						premioKPIEficiencia = 11000;
						premioKPIServicos = 11000;
						premioKPITicketMedio = 11000;
						premioKPIFluxoDeCaixa = 9500;

						KPIDesbloqueado = true;
					}

					if (KPIDesbloqueado == true) {
						premio.setEficienciaContratosPretendido(32);
						premio.setEficienciaPropostasPretendido(134);
						premio.setTicketMedioPretendido(59375);

						float contratosPretendidos = premio.getEficienciaContratosPretendido();
						float propostasPretendidas = premio.getEficienciaPropostasPretendido();

						// =====================L�GICA DE PR�MIOS==============================//
						// ====================EFICIENCIA======================================//
						// Eficiencia: Quantas propostas viraram contratos(propostas / contratos)
						// Quantidade de propostas e contratos atingidos vir� do CRM
						float propostasAtingidas = 135;
						premio.setEficienciaPropostasRealizado(propostasAtingidas);

						List<Contrato> listContratosFechados = contratoService.getAllByComercial(consultor.getId());
						float contratosAtingidos = listContratosFechados.size();

						// System.out.println("Contratos pretendidos: " +
						// premio.getEficienciaContratosPretendido() + " Contratos atingidos: " +
						// contratosAtingidos);
						// System.out.println("Propostas pretendidas: " +
						// premio.getEficienciaPropostasPretendido() + " Propostas atingidas: " +
						// propostasAtingidas);

						premio.setEficienciaContratosRealizado(contratosAtingidos);

						float porcentagemEficiencia = propostasAtingidas / contratosAtingidos;

						// String porcentagemEficienciaString = Float.toString(porcentagemEficiencia);
						// DecimalFormat dfPorcentagem2 = new DecimalFormat("###.00%");

						// porcentagemEficienciaString = dfPorcentagem2.format(porcentagemEficiencia);

						// System.out.println("PorcentagemEficiencia: " + porcentagemEficienciaString);

						float resultadoPremioKPIEficiencia = premioKPIEficiencia * porcentagemEficiencia;
						// System.out.println("Teste Eficiencia: " + resultadoPremioKPIEficiencia);

						if (contratosAtingidos >= contratosPretendidos && propostasAtingidas >= propostasPretendidas) {
							if (resultadoPremioKPIEficiencia >= premioKPIEficiencia) { // Acima do limite do pr�mio do
																						// KPI Eficiencia
								premio.setPremioKPIEficiencia(premioKPIEficiencia);
								valorPremio = valorPremio + premioKPIEficiencia;
							} else {
								premio.setPremioKPIEficiencia(resultadoPremioKPIEficiencia);
								valorPremio = valorPremio + premioKPIEficiencia;
							}
							// System.out.println("KPI Eficiencia check!");
						}

						// ====================SERVICOS========================================//
						if (contratosAtingidos >= contratosPretendidos) {
							premio.setPremioKPIServicos(premioKPIServicos);
							valorPremio = valorPremio + premioKPIServicos;
							// System.out.println("KPI Servicos check!");
						}
						// ===================TICKET MEDIO=====================================//
						float ticketMedioPretendido = premio.getTicketMedioPretendido();

						float ticketMedioAtingido = VNAtingido / premio.getEficienciaContratosRealizado();
						premio.setTicketMedioRealizado(ticketMedioAtingido);

						// String ticketMedioAtingidoString = Float.toString(ticketMedioAtingido);
						// ticketMedioAtingidoString = dfReal.format(ticketMedioAtingido);

						// System.out.println("Ticket medio pretendido: " + ticketMedioPretendido + "
						// Ticket medio atingido: " + ticketMedioAtingidoString);

						if (ticketMedioAtingido >= (0.70 * ticketMedioPretendido)) {
							premio.setPremioKPITicketMedio(premioKPITicketMedio);
							valorPremio = valorPremio + premioKPITicketMedio;
							// System.out.println("KPI Ticket Medio check!");
						} else {
							premio.setPremioKPITicketMedio(0);
						}

						// =====================FLUXO DE CAIXA=================================//
						premio.setFaturamentoPretendido((float) (valoresNegocioConsultor.size() * 0.70));
						premio.setFaturamentoRealizado(VNFinalizados);

						// System.out.println("Valores de negocio pretendidos: " +
						// premio.getFaturamentoPretendido());
						// System.out.println("Valores de negocio finalizados: " +
						// premio.getFaturamentoRealizado());

						// float porcentagemFluxoCaixa = premio.getFaturamentoRealizado() /
						// premio.getFaturamentoPretendido();
						// System.out.println("Porcentagem de fluxo de caixa: " +
						// porcentagemFluxoCaixa);

						if (premio.getFaturamentoRealizado() >= premio.getFaturamentoPretendido()) {// Garantir que 70%
																									// dos NN sejam
																									// faturados ate o
																									// termino do ano
																									// fiscal Seguinte
																									// ao inicio da
																									// campanha
							premio.setPremioKPIFluxoDeCaixa(premioKPIFluxoDeCaixa);
							valorPremio = valorPremio + premioKPIFluxoDeCaixa;
							// System.out.println("KPI Fluxo de caixa check!");
						} else {
							premio.setPremioKPIFluxoDeCaixa(0);
						}

						if (valorPremio > 42000) {
							valorPremio = 42000;
						}
						String stringValorPremio = Float.toString(valorPremio);

						DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
						stringValorPremio = df.format(valorPremio);

						//premio.setValorPremio(stringValorPremio);// Limite do valor do premio para Senior = R$ 42.500,00

					} else {
						premio.setPremioKPIEficiencia(0);
						premio.setPremioKPIServicos(0);
						premio.setPremioKPITicketMedio(0);
						premio.setPremioKPIFluxoDeCaixa(0);
						//premio.setValorPremio("0");
					}

				}
				premio.setContrato(contratoService.findById(32l));
				premio.setFaturamento(faturamentoService.findById(543l));
				premio.setProducao(producaoService.findById(1l));
				premio.setValorNegocio(valorNegocioService.findById(3132l));

				// System.out.println("KPI Eficiencia: " + premio.getPremioKPIEficiencia());
				// System.out.println("KPI Servicos: " + premio.getPremioKPIServicos());
				// System.out.println("KPI Ticket Medio: " + premio.getPremioKPITicketMedio());
				// System.out.println("KPI Fluxo de caixa: " +
				// premio.getPremioKPIFluxoDeCaixa());

				System.out.println("Porcentagem Premio: " + premio.getPorcentagemPremio() + "%"
						+ " 	Valor Premio: R$" + premio.getValorPremio());

				if (premioNovo == true) {
					// System.out.println("Criando premio");
					try {
						create(premio);
					} catch (Exception e) {
						System.out.println("Erro na cria��o do premio:" + premio.getId() + " \n" + e);
					}

				} else {
					// System.out.println("Atualizando premio");
					try {
						em.merge(premio);
					} catch (Exception e) {
						System.out.println("Erro na atualizacao do premio: " + premio.getId() + " \n " + e);
					}
				}
			} catch (Exception e) {
				System.out.println("Erro no consultor de id: " + consultor.getId() + " Nome: " + consultor.getNome());
			}
		}
		System.out.println("\n Metodo gerarPremioByCampanha2020 finalizado!");
	}

	@Override
	public boolean checkIfPremioExistsInProducao(Long id) {
		String sQuery = "SELECT CASE WHEN (count (*) > 0) THEN TRUE ELSE FALSE END FROM Premio p WHERE p.producao.id = :pId";
		TypedQuery<Boolean> booleanQuery = em.createQuery(sQuery, Boolean.class).setParameter("pId", id);
		boolean exists = booleanQuery.getSingleResult();
		if (exists == false) {
			return false;
		} else {
			return true;
		}

	}

	@Override
	public boolean checkIfPremioExistsInContrato(Long id) {
		String sQuery = "SELECT CASE WHEN (count (*) > 0) THEN TRUE ELSE FALSE END FROM Premio p WHERE p.contrato.id = :pId";
		TypedQuery<Boolean> booleanQuery = em.createQuery(sQuery, Boolean.class).setParameter("pId", id);
		boolean exists = booleanQuery.getSingleResult();
		if (exists == false) {
			return false;
		} else {
			return true;
		}

	}

	public boolean checkIfExistsByConsultorAndCampanha(Long idConsultor, String campanha) {
		String sQuery = "SELECT CASE WHEN (count(*) > 0) THEN TRUE ELSE FALSE END FROM Premio p WHERE p.consultor.id = :pIdConsultor AND p.campanha = :pCampanha";
		TypedQuery<Boolean> booleanQuery = em.createQuery(sQuery, Boolean.class)
				.setParameter("pIdConsultor", idConsultor).setParameter("pCampanha", campanha);
		boolean exists = booleanQuery.getSingleResult();
		if (exists == false) {
			return false;
		} else {
			return true;
		}
	}

	public List<JSONObject> readExcelFuncionarios() {
		List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
		try {

			String filePath = "C:\\Users\\felipe.farias.FI-GROUP\\OneDrive - FI Group\\Escritorio\\testeImport\\planilhaTeste.xlsx";
			try {
				FileInputStream file = new FileInputStream(new File(filePath));
				XSSFWorkbook workbook = new XSSFWorkbook(file);
				XSSFSheet sheet = workbook.getSheetAt(0);

				Iterator rowIterator = sheet.rowIterator();

				// int rowNumbers = 417;
				int rowNumbers = sheet.getPhysicalNumberOfRows();
				System.out.println(rowNumbers);

				for (int i = 2; i <= rowNumbers; i++) {
					JSONObject jsonObjectLinha = new JSONObject();
					XSSFRow row = sheet.getRow(i);

					String setor = "";
					String nome = "";
					String email = "";
					String cargo = "";
					String situacao = "";
					String dataFormatadaAdmissao = "";
					String dataFormatadaDemissao = "";
					if (sheet.getRow(i).getCell(1) == null
							|| sheet.getRow(i).getCell(1).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						setor = sheet.getRow(i).getCell(1).getStringCellValue();
						jsonObjectLinha.put("setor", setor);
					}

					if (sheet.getRow(i).getCell(2) == null
							|| sheet.getRow(i).getCell(2).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						nome = sheet.getRow(i).getCell(2).getStringCellValue();
						jsonObjectLinha.put("nome", nome);
					}

					if (sheet.getRow(i).getCell(3) == null
							|| sheet.getRow(i).getCell(3).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						email = sheet.getRow(i).getCell(3).getStringCellValue();
						jsonObjectLinha.put("email", email);
					}

					if (sheet.getRow(i).getCell(4) == null
							|| sheet.getRow(i).getCell(4).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						cargo = sheet.getRow(i).getCell(4).getStringCellValue();
						jsonObjectLinha.put("cargo", cargo);
					}

					if (sheet.getRow(i).getCell(6) == null
							|| sheet.getRow(i).getCell(6).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						situacao = sheet.getRow(i).getCell(6).getStringCellValue();
						jsonObjectLinha.put("situacao", situacao);
					}
					// Date dataAdmissao = sheet.getRow(i).getCell(5).getDateCellValue();

					// Date dataDemissao = sheet.getRow(i).getCell(7).getDateCellValue();
					// Double salario = sheet.getRow(i).getCell(8).getNumericCellValue();
					// Date dataAdmissao = sheet.getRow(i).getCell(5).getDateCellValue();;
					if (sheet.getRow(i).getCell(5) == null
							|| sheet.getRow(i).getCell(5).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						// DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						Date dataAdmissao = sheet.getRow(i).getCell(5).getDateCellValue();
						jsonObjectLinha.put("dataAdmissao", dataAdmissao);
						// dataFormatadaAdmissao = df.format(dataAdmissao);

					}

					if (sheet.getRow(i).getCell(7) == null
							|| sheet.getRow(i).getCell(7).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						// DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						Date dataDemissao = sheet.getRow(i).getCell(7).getDateCellValue();
						jsonObjectLinha.put("dataDemissao", dataDemissao);
						// dataFormatadaDemissao = df.format(dataDemissao);

					}

					Double salario = 0.0;
					if (sheet.getRow(i).getCell(8) == null
							|| sheet.getRow(i).getCell(8).getCellTypeEnum() == CellType.BLANK) {
						System.out.println("C�LULA EM BRANCO");
					} else {
						salario = sheet.getRow(i).getCell(8).getNumericCellValue();
						jsonObjectLinha.put("salario", salario);
					}

					if (Objects.nonNull(jsonObjectLinha)) {
						jsonObjects.add(jsonObjectLinha);
					}

					XSSFCell cell = sheet.getRow(i).getCell(5);
					/*
					 * if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					 * System.out.println("Cell type for date data type is numeric."); }
					 * 
					 * if (HSSFDateUtil.isCellDateFormatted(cell)) { dataAdmissao =
					 * cell.getDateCellValue(); }
					 */
					System.out.println(setor + " " + nome + " " + email + " " + cargo + " " + dataFormatadaAdmissao
							+ " " + situacao + " " + dataFormatadaDemissao + " " + salario);
				}

				/*
				 * while(rowIterator.hasNext()) { Row row = (Row) rowIterator.next();
				 * System.out.println(row.getRowNum()); if(row.getRowNum() == 1){
				 * System.out.println("primeira coluna"); continue; }else { Iterator
				 * cellIterator = row.cellIterator();
				 * 
				 * while(cellIterator.hasNext()) { Cell cell = (Cell) cellIterator.next();
				 * 
				 * switch(cell.getColumnIndex()) { case 1: String setor =
				 * cell.getStringCellValue(); System.out.println(setor); break; case 2: String
				 * email = cell.getStringCellValue(); System.out.println(email); case 3: String
				 * cargo = cell.getStringCellValue(); System.out.println(cargo); case 4: Date
				 * dataAdmissao = cell.getDateCellValue(); System.out.println(dataAdmissao);
				 * case 45: String situacao = cell.getStringCellValue();
				 * System.out.println(situacao); case 6: Date dataDemissao =
				 * cell.getDateCellValue(); System.out.println(dataDemissao); case 7: float
				 * salario = Float.parseFloat(cell.getStringCellValue());
				 * System.out.println(salario); } } }
				 * 
				 * 
				 * }
				 */

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObjects;
	}
	
	@Transactional
	public void calculaPremio2021(List<JSONObject> jsonObjects) {
		String setor = "";
		String email = "";
		String cargo = "";
		// calcular evolu��o antes de calcular a premia��o

		BigDecimal metaBig = new BigDecimal(77000000);
		float meta = 77000000;

		// Date dataInicial = LocalDate.parse("2021-");
		Calendar calendar = Calendar.getInstance();

		// To Initialize Date to a specific Date of your choice (here I'm initializing
		// the date to 2022-06-15)
		calendar.set(2021, Calendar.AUGUST, 1);
		Date dataInicial = calendar.getTime();
		//Date dataInicial = new Date(2021,8,1);
		//calendar.set(2022, Calendar.JULY, 31);
		Date dataFinalCampanha = java.util.Calendar.getInstance().getTime();
		//Date dataFinal = java.util.Calendar.getInstance().getTime();
		Date dataFinal = new Date();
		List<ValorNegocio> valorNegocioList = valorNegocioService.getAllAtivasbyAno("2021");
		System.out.println("A QUANTIDADE DE VALOR DE NEG�CIO �: " + valorNegocioList.size());
		BigDecimal totalVn = BigDecimal.ZERO;
		for (ValorNegocio valorNegocio : valorNegocioList) {
			if(valorNegocio.getValorBase() != null) {
				totalVn = totalVn.add(valorNegocio.getValorBase());
			}
			
			// totalVn+= valorNegocio.getValorBase();
			// System.out.println("O VALOR CALCULO �: " + valorNegocio.getValorCalculo() + "
			// A SOMA DO CALCULO �: " + totalVn);
		}

		DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
		DecimalFormat df2 = new DecimalFormat("#.##");

		int vnComparado = totalVn.compareTo(metaBig);
		System.out.println("o total do vn �: " + totalVn);
		// c�digo para verificar se o vn atingiu a meta

		BigDecimal percentualPremio = BigDecimal.ZERO;

		if (vnComparado == -1 && totalVn.compareTo(new BigDecimal(53900000)) == 1) {
			percentualPremio = totalVn.divide(metaBig, RoundingMode.HALF_DOWN).multiply(new BigDecimal(100));

			System.out.println("o percentual do pr�mio �: " + percentualPremio);
		}else {
			percentualPremio = new BigDecimal(100);
		}

		System.out.println("A COMPARA��O DE VN COM A META �: " + vnComparado);

		System.out.println("o total de vn calculado �: " + df.format(totalVn));
		// String percentualVnString = df2.format(percentualVn);

		// System.out.println("O PERCENTUAL ATINGIDO DE VN �: " + percentualVnString);
		
		String dataInicio = dataInicial.toString();
		String dataFim = dataFinal.toString();
		String dataInicioFormatada = "";
		String dataFimFormatada = "";
		if (dataInicio.matches(".*/.*/.*")) {
			
		} else {
			int ano = dataInicial.getYear();
			System.out.println("o ano �: " + ano);
			int mes = dataInicial.getMonth();
			int dia = dataInicial.getDay();
		    dataInicioFormatada = ano +"-"+ mes +"-"+ dia;

		}
		
		if (dataFim.matches(".*/.*/.*")) {
			
		} else {
			int ano = dataFinal.getYear();
			System.out.println("o ano �: " + ano);
			int mes = dataFinal.getMonth();
			int dia = dataFinal.getDay();
			dataFimFormatada = ano +"-"+ mes +"-"+ dia;
			

		}
		
		System.out.println("a data de inicio antes do m�todo �: " + dataInicial);
		System.out.println("a data de finaliza��o antes do m�todo �: " + dataFinal);
		System.out.println("a data de inicio �: " + dataInicioFormatada);
		System.out.println("a data final �: " + dataFimFormatada);
		List<NotaFiscal> notasPagas = notaFiscalService.getSomaPagamentoDateByCriteria(dataInicial, dataFinal, "2021");
		System.out.println("A QUANTIDADE DE NOTAS PAGAS �: " + notasPagas.size());
		float valorPago = 0;
		for (NotaFiscal nota : notasPagas) {
			String valorCobrancaString = nota.getCobrar();
			if (Objects.nonNull(valorCobrancaString) && !valorCobrancaString.equalsIgnoreCase("")) {
				valorCobrancaString = valorCobrancaString.replaceAll("\\.", "");
				valorCobrancaString = valorCobrancaString.replace(",", ".");

				valorPago += Float.parseFloat(valorCobrancaString);
			}

		}

		String valorFinalString = df.format(valorPago);
		System.out.println("o valor pago �: " + valorFinalString);
		BigDecimal percentualAtingido =  BigDecimal.ZERO;
		
		System.out.println("a compara�ao de notas pagas com a meta �: " + new BigDecimal(valorPago).compareTo(metaBig));
		if(new BigDecimal(valorPago).compareTo(metaBig) == -1) {
			System.out.println("entrou no if do valor pago");
			percentualAtingido = new BigDecimal((valorPago / meta) * 100);
		}else{
			System.out.println("entrou no else do valor pago");
			percentualAtingido = new BigDecimal(100);
		}
		
		
		
		

		String percentualString = df2.format(percentualAtingido);
		System.out.println("O PERCENTUAL ATINGIDO � :" + percentualString);

		/*
		 * List<PeriodoPagamento> periodos = periodoPagamentoService.getAllNaoPagos();
		 * 
		 * for (PeriodoPagamento periodo : periodos) {
		 * 
		 * List<NotaFiscal> notasPagasPeriodo =
		 * notaFiscalService.getSomaPagamentoByCriteria(periodo.getDataInicio(),
		 * periodo.getDataFim(), "2021");
		 * 
		 * 
		 * float evolucaoPeriodo = 0;
		 * 
		 * for (NotaFiscal nota : notasPagasPeriodo) { String valorCobrancaString =
		 * nota.getCobrar();
		 * 
		 * if(Objects.nonNull(valorCobrancaString) &&
		 * !valorCobrancaString.equalsIgnoreCase("")) { valorCobrancaString =
		 * valorCobrancaString.replaceAll("\\.", ""); valorCobrancaString =
		 * valorCobrancaString.replace(",", ".");
		 * 
		 * evolucaoPeriodo += Float.parseFloat(valorCobrancaString); } }
		 * 
		 * 
		 * //Premio premio = premioService.getPremioByConsultorCampanhaAndPeriodo(); }
		 */

		System.out.println("TESTE PERCORRENDO JSON OBJECTS");
		for (JSONObject jsonObject : jsonObjects) {
			setor = null;
			email = null;
			cargo = null;
			try {

				setor = jsonObject.get("setor").toString();
				email = jsonObject.get("email").toString();
				cargo = jsonObject.get("cargo").toString();
				BigDecimal salario = new BigDecimal(jsonObject.get("salario").toString());
				String situacao = jsonObject.get("situacao").toString();
				String nome = jsonObject.get("nome").toString();
				Date dataAdmissao = (Date) jsonObject.get("dataAdmissao");
				Date dataDemissao = (Date) jsonObject.get("dataDemissao");
				System.out.println("O NOME �: " + nome + " O E-MAIL �: " + email + " O CARGO �: " + cargo
						+ " A DATA ADMISS�O �:" + dataAdmissao + " A SITUA�AO �: " + situacao + " A DATA DEMISS�O �:"
						+ dataDemissao + " O SAL�RIO �: " + salario);

			} catch (Exception e) {

			}

		}

		for (JSONObject jsonObject : jsonObjects) {
			Consultor consultor = new Consultor();

			if (Objects.nonNull(jsonObject.get("setor"))) {
				setor = jsonObject.get("setor").toString();
				System.out.println(setor);
				if (jsonObject.get("email") != null) {
					if (!jsonObject.get("email").equals("")) {
						email = jsonObject.get("email").toString();

						consultor = consultorService.findUserByEmail(email);
						// System.out.println("o consultor � " + consultor.getNome());
					}
				}
				if (Objects.nonNull(consultor)) {

					if (consultor.getNome() != null) {
						System.out.println("o nome do funcion�rio antes do if � " + consultor.getNome());

						cargo = jsonObject.get("cargo").toString();
						String situacao = jsonObject.get("situacao").toString();
						Date dataDemissao = null;
						
						try {

							if (cargo.equalsIgnoreCase("CONSULTOR TECNICO JR")) {
								// incluir calculo para consultor jr
								System.out.println("ENTROU NA CONDI��O CONSULTOR TECNICO");

								// calculo do valor geral do pr�mio
								BigDecimal salario = new BigDecimal(jsonObject.get("salario").toString());

								//BigDecimal premioTotal = BigDecimal.ZERO;

								// calcular percentual trabalhado da campanha
								

								Date dataAdmissao = (Date) jsonObject.get("dataAdmissao");
								if(Objects.nonNull(jsonObject.get("dataDemissao"))) {
									if(jsonObject.get("dataDemissao") != "") {
										dataDemissao = (Date) jsonObject.get("dataDemissao");
									}
								}
								
								
								System.out.println("a data de inicio antes do m�todo �: " + dataInicial);
								System.out.println("a data de finaliza��o antes do m�todo �: " + dataFinalCampanha);
								System.out.println("a data de admiss�o do funcion�rio �: " + dataAdmissao);
								System.out.println("a data de demiss�o do funcion�rio �: " + dataDemissao);
								
								boolean recebePremio = verificaRecebimentoDePremio2021(situacao, dataDemissao, dataFinalCampanha);
								System.out.println("esse funcion�rio receber� pr�mio? " + recebePremio);
								if(recebePremio == true) {
								

								Double percentualTrabalhadoCampanha = calculaPercentualTrabalhado(dataAdmissao, dataDemissao, situacao, dataInicial,
										dataFinalCampanha);
								System.out.println("o percentual trabalhado na campanha �: " + percentualTrabalhadoCampanha);

								// o consultor t�cnico jr tem um percentual de 6%
								/*
								 * premioTotal = salario.multiply(new BigDecimal(13))
								 * .multiply(percentualPremio.divide(new BigDecimal(100))) .multiply(new
								 * BigDecimal(0.06)); System.out.println("o sal�rio �: " + salario);
								 * System.out.println( "o percentual dividido �: " + percentualPremio.divide(new
								 * BigDecimal(100))); System.out.println("o valor total do pr�mio �: " +
								 * premioTotal);
								 */

								BigDecimal premio = calculaPremioConsultoria2021(salario, percentualPremio, new BigDecimal(0.06));
								
								BigDecimal premioFinal = premio.multiply(new BigDecimal(percentualTrabalhadoCampanha / 100));
								
								
								
								System.out.println("O VALOR DO PR�MIO SEM DESCONTAR O PERCENTUAL �: " + premio);
								System.out.println("O VALOR TOTAL DO PR�MIO �: " + premioFinal);
								
								//verificar se j� existe um pr�mio total para esse consultor
								
								Premio premioTotal = premioService.getPremioTotalByConsultorAndCampanha(consultor.getId(), "2021");
								
								if(Objects.nonNull(premioTotal)) {
									//condi��o para atualizar o valor de um premio j� existente
									System.out.println("entrou na condi��o de atualizar premio");
									if(!premioTotal.isPago()) {
										if(situacao.equalsIgnoreCase("Trabalhando")) {
											premioTotal.setAtivo(true);
										}else {
											if(dataDemissao.before(dataFinalCampanha)) {
												premioTotal.setAtivo(false);
											}else {
												premioTotal.setAtivo(true);
											}
										}
										
										premioTotal.setEvolucaoTotal(percentualPremio);
										premioTotal.setValorPremio(premioFinal);
										update(premioTotal);
									}
								}else {
									//condi��o para criar um pr�mio novo
									System.out.println("entrou na condi��o de criar o premio");
									premioTotal = new Premio();
									premioTotal.setConsultor(consultor);
									premioTotal.setAtivo(true);
									premioTotal.setCampanha("2021");
									premioTotal.setEvolucaoTotal(percentualPremio);
									premioTotal.setValorPremio(premioFinal);
									create(premioTotal);
								}

								try {
									List<PeriodoPagamento> periodosNaoPagos = periodoPagamentoService
											.getAllPeriodosNaoPagosByDepartamento("CONSULTORIA_TECNICA");
									System.out.println("O TAMANHO DA LISTA � :" + periodosNaoPagos.size());
									if (Objects.nonNull(periodosNaoPagos)) {
										
										
										for (PeriodoPagamento periodoPagamento : periodosNaoPagos) {
											String dataInicioPeriodo = periodoPagamento.getDataInicio().toString();
											String dataFimPeriodo = periodoPagamento.getDataFim().toString();
											String dataInicioFormatadaPeriodo = "";
											String dataFimFormatadaPeriodo = "";
											
											
											if (dataInicioPeriodo.matches(".*/.*/.*")) {
												
											} else {
												String ano = dataInicioPeriodo.substring(0, 4);
												String mes = dataInicioPeriodo.substring(5, 7);
												String dia = dataInicioPeriodo.substring(8, 10);
												dataInicioFormatadaPeriodo = ano +"-"+ mes +"-"+ dia;

											}
											
											if (dataFimPeriodo.matches(".*/.*/.*")) {
												
											} else {
												String ano = dataFimPeriodo.substring(0, 4);
												String mes = dataFimPeriodo.substring(5, 7);
												String dia = dataFimPeriodo.substring(8, 10);
												dataFimFormatadaPeriodo = ano +"-"+ mes +"-"+ dia;
											}
											System.out.println("a data de inicio que ser� verificada na query �: " + dataInicioPeriodo);
											System.out.println("a data de inicio formatada que ser� verificada na query �: " + dataInicioFormatadaPeriodo);
											System.out.println("a data fim que ser� verificada na query �: " + dataFimPeriodo);
											System.out.println("a data fim formatada que ser� verificada na query �: " + dataFimFormatadaPeriodo);
											List<NotaFiscal> notasPagasPeriodo = notaFiscalService.getSomaPagamentoDateByCriteria(periodoPagamento.getDataInicio(),periodoPagamento.getDataFim(), "2021");
											System.out.println("a quantidade de notas pagas no per�odo �: "
													+ notasPagasPeriodo.size());
											float evolucaoPeriodo = 0;
											float valorPagoPeriodo = 0;
											for (NotaFiscal nota : notasPagasPeriodo) {
												String valorCobrancaString = nota.getCobrar();

												if (Objects.nonNull(valorCobrancaString)
														&& !valorCobrancaString.equalsIgnoreCase("")) {
													valorCobrancaString = valorCobrancaString.replaceAll("\\.", "");
													valorCobrancaString = valorCobrancaString.replace(",", ".");

													valorPagoPeriodo += Float.parseFloat(valorCobrancaString);
													//System.out.println("a data de inicio do periodo �: " + periodoPagamento.getDataInicio());
													//System.out.println("a data final do per�odo e :" + periodoPagamento.getDataFim());
													//System.out.println("a data de pagamento da nota do per�odo �: " + nota.getDataCobranca());
													// System.out.println("o valor pago �: " + valorPagoPeriodo);

												}
											}

											String valorFinalPeriodoString = df.format(valorPagoPeriodo);
											System.out.println("o valor pago �: " + valorFinalPeriodoString);

											evolucaoPeriodo = (valorPagoPeriodo / meta) * 100;
											if(evolucaoPeriodo > 100) {
												evolucaoPeriodo = 100;
											}
											
											String percentualPeriodoString = df2.format(evolucaoPeriodo);
											
											
											System.out.println("a evolu��o do per�odo �: " + percentualPeriodoString);

											// List<Premio> premios =
											// premioService.getAllByConsultor(consultor.getId());

											/*
											 * if(premios.contains(periodoPagamento.getId())) {
											 * 
											 * }else {
											 * 
											 * }
											 */
										}
									}

								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							} else if (cargo.equalsIgnoreCase("CONSULTOR TECNICO PL")) {

							}

							/*
							 * if(cargo.equalsIgnoreCase("CONSULTOR TECNICO JR") ||
							 * cargo.equalsIgnoreCase("CONSULTOR TECNICO PL") ||
							 * cargo.equalsIgnoreCase("CONSULTOR TECNICO JR I") ||
							 * cargo.equalsIgnoreCase("CONSULTOR TECNICO SENIOR") ||
							 * cargo.equalsIgnoreCase("COORDENADOR TECNICO") ||
							 * cargo.equalsIgnoreCase("CONSULTOR TECNICO TRAINEE") ||
							 * cargo.equalsIgnoreCase("LIDER TECNICO") ||
							 * cargo.equalsIgnoreCase("CONSULTOR TECNICO PLENO")) {
							 * System.out.println("ENCONTROU UM CONSULTOR"); System.out.println(cargo);
							 * Cargo cargo1 = consultor.getCargo(); float salario =
							 * Float.parseFloat(jsonObject.get("salario").toString());
							 * cargo1.calcularPremio2021(salario, percentualAtingido);
							 * 
							 * }
							 * 
							 * if(consultor.getCargo() == Cargo.LIDER_TECNICO || consultor.getCargo() ==
							 * Cargo.CONSULTOR_TECNICO_TRAINEE || consultor.getCargo() ==
							 * Cargo.CONSULTOR_LIDER_TECNICO ||consultor.getCargo() ==
							 * Cargo.GERENTE_REGIONAL || consultor.getCargo() == Cargo.CONSULTOR_TECNICO ||
							 * consultor.getCargo() == Cargo.COORDENADOR_TECNICO) {
							 * 
							 * }
							 */
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}

		}

	}

	public Double calculaPercentualTrabalhado(Date dataAdmissao, Date dataDemissao, String situacao,
			Date dataInicioCampanha, Date dataFimCampanha) {
		// long nDiasCampanha = ChronoUnit.DAYS.between(dataInicioCampanha,
		// dataFimCampanha);
		// Days.daysBetween(dataInicioCampanha, dataFimCampanha);
		Long diferencaDiasCampanha = Math.abs(dataInicioCampanha.getTime() - dataFimCampanha.getTime());
		Long diasCampanha = TimeUnit.DAYS.convert(diferencaDiasCampanha, TimeUnit.MILLISECONDS);
		// System.out.println("a quantidade de dias de campanha �: " +
		// Math.round(diasCampanha));
		System.out.println("a quantidade de dias de campanha �: " + diasCampanha);
		System.out.println("a data demiss�o antes do if �: " + dataDemissao);
		System.out.println("a situa��o �: " + situacao);
		
		
		if (situacao.equalsIgnoreCase("Trabalhando")) {
			dataDemissao = dataFimCampanha;
		}
		System.out.println("a data demiss�o depois do if �: " + dataDemissao);

		System.out.println("a data de admiss�o antes do if �: " + dataAdmissao);
		if (dataAdmissao.before(dataInicioCampanha)) {
			System.out.println("a data de in�cio da campanha �: " + dataInicioCampanha);
			dataAdmissao = dataInicioCampanha;
		}
		System.out.println("a data de admiss�o depois do if �: " + dataAdmissao);

		long diasTrabalhadosCampanha = Math.abs(dataAdmissao.getTime() - dataDemissao.getTime());
		Long trabalhadosCampanha = TimeUnit.DAYS.convert(diasTrabalhadosCampanha, TimeUnit.MILLISECONDS);
		
		Double percentualTrabalhado = ((trabalhadosCampanha.doubleValue() / diasCampanha.doubleValue()) * 100);
		
		Double resultado = percentualTrabalhado.doubleValue();

		//System.out.println("a quantidade de dias trabalhados durante a campanha �: " + trabalhadosCampanha);
		//System.out.println("o percentual trabalhado durante a campanha �: " + resultado);
		
		if (situacao.equalsIgnoreCase("Desligado")) {
			System.out.println("ENTROU NA CONDI��O DESLIGADO");
			if(dataDemissao.before(dataInicioCampanha)) {
				System.out.println("ENTROU NA CONDI��O DATA DEMISS�O ANTES");
				return 0.0;
			}else {
				
				return resultado;
			}
		}else {
			return resultado;
		}
		
	}

	public BigDecimal calculaPremioConsultoria2021(BigDecimal salario, BigDecimal percentualPremio,
			BigDecimal percentualCargo) {
		BigDecimal salarios = salario.multiply(new BigDecimal(13));
		System.out.println("os sal�rios somados s�o: " + salarios);
		BigDecimal percentual = salarios.multiply(percentualPremio.divide(new BigDecimal(100)));
		System.out.println("os salarios com o percentual � " + percentual);
		BigDecimal valorCargo = percentual.multiply(percentualCargo);
		System.out.println("o valor total � " + valorCargo);

		return valorCargo;
	}
	
	public boolean verificaRecebimentoDePremio2021(String situacao, Date dataDemissao,Date dataFimCampanha) {
		
		if(Objects.nonNull(situacao) && !situacao.equalsIgnoreCase("")) {
			if(situacao.equalsIgnoreCase("Desligado")) {
				System.out.println("entrou na condi��o de desligado");
				if(dataDemissao.before(dataFimCampanha)) {
					System.out.println("a data de demiss�o do funcion�rio �: " + dataDemissao);
					System.out.println("essa pessoa n�o tem direito ao pr�mio");
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return false;
		}
		
	}

	public Premio getPremioTotalByConsultorAndCampanha(Long id, String campanha) {
		try {
			return em.createQuery(
					"SELECT p FROM Premio p WHERE p.consultor.id = :pId AND p.campanha = :pCampanha AND p.premioTotal = 1",
					Premio.class).setParameter("pId", id).setParameter("pCampanha", campanha).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
