package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.Cidade;
import br.com.finiciativas.fseweb.models.instituicao.ContatoInstituicao;
import br.com.finiciativas.fseweb.models.instituicao.Instituicao;
import br.com.finiciativas.fseweb.repositories.InstituicaoRepository;

@Repository
public class InstituicaoRepositoryImpl implements InstituicaoRepository {
	
	@Autowired
	private EntityManager em;
	
	@Autowired
	private CidadeRepositoryImpl repositoryCidade;
	
	@Autowired
	private ContatoInstituicaoRepositoryImpl repositoryContato;

	@Transactional
	@Override
	public void create(Instituicao instituicao) {
		Cidade cidade = new Cidade();
		instituicao.setAtivo(true);
		em.persist(instituicao);
	}
	
	@Override
	public void delete(Long id) {
		em.createQuery("UPDATE Instituicao inst SET inst.ativo = 0 WHERE inst.id = :pId").setParameter("pId", id)
				.executeUpdate();
	}

	@Override
	public List<Instituicao> getAll() {
		return this.em.createQuery("FROM Instituicao i", Instituicao.class)
				.getResultList();
	}

	@Transactional
	@Override
	public Instituicao update(Instituicao instituicao) {
		
		List<ContatoInstituicao> contatos = this.repositoryContato.getContatosInstituicao(instituicao.getId());
		
		instituicao.setContatos(contatos);
		
		return this.em.merge(instituicao);
	}

	@Override
	public Instituicao findById(Long id) {
		return this.em.find(Instituicao.class, id);
	}

	@Override
	public void remove(Instituicao instituicao) {
		this.em.remove(instituicao);
	}	
	
	@Override
	public Instituicao loadFullEmpresa(Long id) {		
		try {
			return em.createQuery("FROM Instituicao inst "
					+ "LEFT JOIN FETCH inst.contatos ce WHERE inst.id = :pId", Instituicao.class)
					.setParameter("pId", id).getSingleResult();
		} catch (Exception e) {
			return this.findById(id);
		}
		
	}

}
