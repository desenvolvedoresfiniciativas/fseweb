package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.CategoriaConsultor;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.repositories.ConsultorRepository;
import br.com.finiciativas.fseweb.services.impl.EmailServiceImpl;

@Repository
public class ConsultorRepositoryImpl implements ConsultorRepository {

	@Autowired
	private EntityManager em;

	@Override
	public Consultor findByUsername(String username) {

		try {
			return em
					.createQuery("SELECT c FROM Consultor c " + "LEFT JOIN FETCH c.roles rol "
							+ "WHERE c.username = :username AND c.ativo = :ativo", Consultor.class)
					.setParameter("username", username).setParameter("ativo", true).getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<Consultor> getAll() {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.ativo = 1 ORDER BY c.username", Consultor.class)
				.getResultList();
	}
	
	@Override
	public List<Consultor> getAtivosAndInativos() {
		return em.createQuery("SELECT c FROM Consultor c ORDER BY c.username", Consultor.class)
				.getResultList();
	}
	
	@Override
	public List<Consultor> getAtivo() {
		return em.createQuery("SELECT c FROM Consultor c ORDER BY c.username", Consultor.class)
				.getResultList();
	}

	public Consultor getConsultorByUser(String username) {
		try {
			return em.createQuery("SELECT c FROM Consultor c WHERE c.username = :pUser ORDER BY c.username", Consultor.class)
					.setParameter("pUser", username).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void create(Consultor consultor) {
		consultor.setAtivo(true);
		consultor.setPrimeiroLogin(true);
		System.out.println("o valor de promeiro login �: " + consultor.isPrimeiroLogin());
		em.persist(consultor);
	}

	@Override
	public void delete(Long id) {
		em.createQuery("UPDATE Consultor c SET c.ativo = 0 WHERE c.id = :id").setParameter("id", id).executeUpdate();
	}

	@Override
	public Consultor update(Consultor consultor) {

		Consultor consultorAtt = this.findById(consultor.getId());

		consultorAtt.setEmail(consultor.getEmail());
		consultorAtt.setNome(consultor.getNome());
		consultorAtt.setUsername(consultor.getUsername());
		consultorAtt.setPrimeiroLogin(consultor.isPrimeiroLogin());
		consultorAtt.setCategoriaConsultor(consultor.getCategoriaConsultor());
		consultorAtt.setPais(consultor.getPais());

		if (!consultor.getRoles().equals(consultorAtt.getRoles())) {
			consultorAtt.setRoles(consultor.getRoles());
		}
		
		consultorAtt.setSetorAtividade(consultor.getSetorAtividade());
		consultorAtt.setDepartamento(consultor.getDepartamento());
		consultorAtt.setCargo(consultor.getCargo());
		consultorAtt.setSigla(consultor.getSigla());

		return em.merge(consultorAtt);
	}
	
	public Consultor updateSenha(Consultor consultor) {

		Consultor consultorAtt = this.findById(consultor.getId());
		
		consultor.criptarSenha();
		consultorAtt.setSenha(consultor.getSenha());

		em.merge(consultorAtt);
		return consultor;
	}

	@Override
	public boolean enviarEmailBoasVindas(Consultor consultor) {
		try {
			StringBuilder msgBuilder = new StringBuilder();

			msgBuilder.append("Bem vindo ao FSE ! \nSeu usu�rio �: ");
			msgBuilder.append(consultor.getUsername());
			msgBuilder.append("\nSua senha �: ");
			msgBuilder.append(consultor.getSenha());

			new EmailServiceImpl().send(consultor.getNome(), consultor.getEmail(), "Bem vindo ao FSE",
					msgBuilder.toString());

			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public Consultor findById(Long id) {
		return em.find(Consultor.class, id);
	}

	@Override
	public List<Consultor> getConsultorByCargo(Cargo cargo) {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.cargo = :cargo AND c.ativo = :ativo ORDER BY c.username", Consultor.class)
				.setParameter("cargo", cargo).setParameter("ativo", true).getResultList();
	}
	
	@Override
	public List<Consultor> getComerciais(Cargo cargo, Cargo especialistaComercial, Cargo supervisorAnalistaComercial, Cargo analistaComercial, Cargo coordenadorComercial, Cargo coordenadorTecnico, Cargo liderTecnico, Cargo consultorLiderTecnico) {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.cargo = :cargo OR c.cargo = :analistaComercial OR c.cargo = :supervisorAnalistaComercial OR c.cargo = :especialistaComercial OR "
				+ "c.cargo = :coordenadorTecnico OR c.cargo = :liderTecnico OR c.cargo = :consultorLiderTecnico OR c.cargo = :coordenadorComercial"
				+ " AND c.ativo = :ativo ORDER BY c.username", Consultor.class)
				.setParameter("cargo", cargo).setParameter("analistaComercial", analistaComercial).setParameter("supervisorAnalistaComercial", supervisorAnalistaComercial).setParameter("especialistaComercial", especialistaComercial)
				.setParameter("coordenadorTecnico", coordenadorTecnico).setParameter("consultorLiderTecnico", consultorLiderTecnico).setParameter("coordenadorComercial", coordenadorComercial).setParameter("liderTecnico", liderTecnico)
				.setParameter("ativo", true).getResultList();
	}
	
	@Override
	public List<Consultor> getAllComerciais(Cargo cargo, Cargo especialistaComercial, Cargo supervisorAnalistaComercial, Cargo analistaComercial, Cargo coordenadorComercial, Cargo coordenadorTecnico, Cargo liderTecnico, Cargo consultorLiderTecnico) {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.cargo = :cargo OR c.cargo = :analistaComercial OR c.cargo = :supervisorAnalistaComercial OR c.cargo = :especialistaComercial OR "
				+ "c.cargo = :coordenadorTecnico OR c.cargo = :liderTecnico OR c.cargo = :consultorLiderTecnico OR c.cargo = :coordenadorComercial"
				+ " ORDER BY c.username", Consultor.class)
				.setParameter("cargo", cargo).setParameter("analistaComercial", analistaComercial).setParameter("supervisorAnalistaComercial", supervisorAnalistaComercial).setParameter("especialistaComercial", especialistaComercial)
				.setParameter("coordenadorTecnico", coordenadorTecnico).setParameter("consultorLiderTecnico", consultorLiderTecnico).setParameter("coordenadorComercial", coordenadorComercial).setParameter("liderTecnico", liderTecnico)
				.getResultList();
	}
	
	@Override
	public List<Consultor> getLideres() {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.cargo IN ('CONSULTOR_LIDER_TECNICO', 'LIDER_TECNICO') AND c.ativo = :ativo ORDER BY c.username", Consultor.class)
				.setParameter("ativo", true).getResultList();
	}

	@Override
	public List<Consultor> getCoordenadores() {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.cargo IN ('COORDENADOR_TECNICO') AND c.ativo = :ativo ORDER BY c.username", Consultor.class)
				.setParameter("ativo", true).getResultList();
	}
	
	@Override
	public List<Consultor> getGerentes() {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.cargo IN ('GERENTE_DE_NEGOCIOS') AND c.ativo = :ativo ORDER BY c.username", Consultor.class)
				.setParameter("ativo", true).getResultList();
	}

	public List<Consultor> getConsultorByCargoAll(Cargo cargo) {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.cargo = :cargo ORDER BY c.username", Consultor.class)
				.setParameter("cargo", cargo)
				.getResultList();
	}
	
	public List<Consultor> getConsultores() {
        return em
                .createQuery("SELECT DISTINCT c FROM Consultor c WHERE (c.cargo = :cargo1 OR "
                		+ "c.cargo = :cargo2 OR c.cargo = :cargo3 OR c.cargo = :cargo4 OR "
                		+ "c.cargo = :cargo5 OR c.cargo = :cargo6 OR c.cargo = :cargo7 OR c.cargo = :cargo8  OR c.cargo = :cargo9) "
                		+ "AND c.ativo = :ativo  ORDER BY c.username",Consultor.class)
                .setParameter("cargo1", Cargo.CONSULTOR_TECNICO)
                .setParameter("cargo2", Cargo.ESTAGIARIO_TECNICO)
                .setParameter("cargo3", Cargo.CONSULTOR_LIDER_TECNICO)
                .setParameter("cargo4", Cargo.CONSULTOR_TRAINEE)
                .setParameter("cargo5", Cargo.COORDENADOR_TECNICO)
                .setParameter("cargo6", Cargo.GERENTE_CONSULTORIA_TECNICA)
                .setParameter("cargo7", Cargo.LIDER_TECNICO)
                .setParameter("cargo8", Cargo.GERENTE_GERAL)
                .setParameter("cargo9", Cargo.CONSULTOR_TECNICO_TRAINEE)
                .setParameter("ativo", true)
                .getResultList();
    }

	public List<Consultor> getEquipeUtilizandoConsultorLogado() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = ((Equipe) auth.getPrincipal());
		
		System.out.println("equipe: " + equipe);
		
		return em
				.createQuery("SELECT DISTINCT e FROM Equipe e WHERE ()"
								+ " AND c.ativo = :ativo AND c.equipe = :equipe ORDER BY c.username", 
						Consultor.class)
				.setParameter("cargo1", Cargo.CONSULTOR_TECNICO)
				.setParameter("cargo2", Cargo.ESTAGIARIO_TECNICO)
				.setParameter("ativo", true)
				.setParameter("equipe", consultor.getEquipe())
				.getResultList();
	}

	@Override
	public List<Consultor> getConsultoresSemEquipe() {
		return em
				.createQuery(
						"SELECT c FROM Consultor c WHERE c.ativo = 1 AND (c.cargo = :pCargo OR c.cargo = :pCargoTrainee OR c.cargo = :pCargoEst) AND c.id NOT IN (SELECT cons.id FROM Equipe e INNER JOIN e.consultores cons) ORDER BY c.username",
						Consultor.class)
				.setParameter("pCargo", Cargo.CONSULTOR_TECNICO)
				.setParameter("pCargoEst", Cargo.ESTAGIARIO_TECNICO)
				.setParameter("pCargoTrainee", Cargo.CONSULTOR_TRAINEE)
				.getResultList();
	}
	
	public Consultor getConsultorBySigla() {

		String sQuery = "SELECT c FROM Consultor c ORDER BY c.sigla";

		return em.createQuery(sQuery, Consultor.class).getSingleResult();
	}
	
	public List<Consultor> getCategoriaConsultor(CategoriaConsultor categoriaConsultor) {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.categoriaConsultor = :categoriaConsultor ORDER BY c.username", Consultor.class)
				.setParameter("categoriaConsultor", categoriaConsultor)
				.getResultList();
	}
	
	public Consultor findBySigla(String sigla){
		try {
		return em.createQuery("SELECT c FROM Consultor c WHERE c.sigla = :pSigla", Consultor.class)
				.setParameter("pSigla", sigla)
				.getSingleResult();
		} catch (Exception e) {
			System.out.println("Busca de consultor por sigla n�o encontrou nada");
			return null;
		}
	}

	@Override
	public List<Consultor> getAllComercialAndConsultor() {
		try {
			return em.createQuery("SELECT DISTINCT c FROM Consultor c WHERE ("
					+ "c.cargo = :cargo1 OR "
					+ "c.cargo = :cargo2 OR "
					+ "c.cargo = :cargo3 OR "
					+ "c.cargo = :cargo4 OR "
					+ "c.cargo = :cargo5) AND "
					+ "c.ativo = :ativo ORDER BY c.username", Consultor.class)
					.setParameter("cargo1", Cargo.CONSULTOR_COMERCIAL)
					.setParameter("cargo2", Cargo.CONSULTOR_TECNICO)
					.setParameter("cargo3", Cargo.CONSULTOR_LIDER_TECNICO)
					.setParameter("cargo4", Cargo.CONSULTOR_TRAINEE)
					.setParameter("cargo5", Cargo.GERENTE_CONSULTORIA_TECNICA)
					.setParameter("ativo", true)
					.getResultList();
			} catch (Exception e) {
				System.out.println("Erro no Metodo getAllComercialAndConsultor.ConsultorRepositoryImpl" + e );
				return null;
			}
	}

	@Override
	public Consultor getUserByEmail(String email) {
		try {
			return em
					.createQuery("SELECT c FROM Consultor c "
							+ "WHERE c.email = :email", Consultor.class)
					.setParameter("email", email).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}
