package br.com.finiciativas.fseweb.repositories.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorAprovado;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorEstimado;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorFiscalizado;
import br.com.finiciativas.fseweb.models.valorNegocioCL.ValorPostulado;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioCLServiceImpl;

@Repository
public class PresupuestosValorNegocioCRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ItemValoresServiceImpl ItemValor;
	
	@Transactional
	public void geraValorEstimado() {
		List<Producao> producoes = producaoService.getAll();
		try {
			for (Producao producao : producoes) {
				System.out.println(producao.getId());
				List<ValorEstimado> valorByProd = getEstimadoByProducao_id(producao.getId());
				Double duracionPreliminar = 0.0;
				
				try {
					duracionPreliminar = Double
							.parseDouble(ItemValor.getDuracionPreliminarByProducao(producao.getId()).getValor());
				} catch (Exception e) {
					duracionPreliminar = 0.0;
				}
				System.out.println(duracionPreliminar);
				if (valorByProd.size() < duracionPreliminar || valorByProd.size() > duracionPreliminar) {
					if (duracionPreliminar != 0.0) {
						try {
							DeleteAllEstimadoByProducao(producao.getId());
						} catch (Exception e) {
							// TODO: handle exception
						}
						BigDecimal valor = new BigDecimal("0.00");
						
						try {
							if (ItemValor.getPresupuestoPreliminarByProducao(producao.getId()).getValor().contains(",")) {
								valor = new BigDecimal(ItemValor.getPresupuestoPreliminarByProducao(producao.getId()).getValor().replace(".", "").replaceAll(",", "."));
							}
							else {
								valor = new BigDecimal(ItemValor.getPresupuestoPreliminarByProducao(producao.getId()).getValor());								
							}
						} catch (Exception e) {
							
						}
						System.out.println(valor+" valor");
						BigDecimal valorAnual = valor.divide(new BigDecimal(duracionPreliminar),2,RoundingMode.HALF_DOWN);
						String fechaInicio = ItemValor.getFechaInicioByProducao(producao.getId()).getValor();
						Integer fechaInicioAno = 0;
						
						try {
							String parts[] = fechaInicio.split("/");
							fechaInicioAno = Integer.parseInt(parts[2]);

						} catch (Exception e) {
							fechaInicioAno = 0;
						}
						
						String fechaGasto = ItemValor.getFechaGastoByProducao(producao.getId()).getValor();
						Integer fechaGastoAno = 0;
						try {
							String part[] = fechaGasto.split("/");
							fechaGastoAno = Integer.parseInt(part[2]);
						} catch (Exception e) {
							fechaGastoAno = 0;
						}
						System.out.println("meio");
						Integer a = 0;
						for (int i = 0; i < duracionPreliminar; i++) {
							ValorEstimado valorEstimado = new ValorEstimado();
							valorEstimado.setCampanha(producao.getAno());
							valorEstimado.setProducao(producao);

							if (valorEstimado.getAnoFiscal() == null) {
								valorEstimado.setAnoFiscal("2013");
							}

							if (fechaInicioAno <= 0 || fechaInicioAno == null) {
								valorEstimado.setFechaReferencia(producao.getAno());
							} else {
								if (fechaInicioAno > fechaGastoAno) {
									valorEstimado.setFechaReferencia(String.valueOf(fechaInicioAno));
								} else {
									valorEstimado.setFechaReferencia(String.valueOf(fechaGastoAno));
								}
							}

							System.out.println("cheguei");
							String newYear = String.valueOf(Integer.parseInt(valorEstimado.getAnoFiscal()) + a);
							valorEstimado.setAnoFiscal(newYear);
							if (Integer.parseInt(valorEstimado.getAnoFiscal()) >= Integer
									.parseInt(valorEstimado.getFechaReferencia())) {
								valorEstimado.setValor(valorAnual.toString());
								valorEstimado.setContrato(producao.getContrato());
								System.out.println("criei");
								CreateEstimado(valorEstimado);
							} else {
								// valorEstimado.setValor("0");
								// Create(valorEstimado);
								i--;
							}
							a++;
						}
					}
				}

			}
			System.out.println("acabou");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<ValorEstimado> getAll() {
		return this.em.createQuery("SELECT distinct ve FROM ValorEstimado ve ", ValorEstimado.class).getResultList();
	}

	public List<ValorEstimado> getEstimadoByProducao_id(Long id) {
		return this.em
				.createQuery("SELECT ve FROM ValorEstimado ve WHERE ve.producao.id = :prodId", ValorEstimado.class)
				.setParameter("prodId", id).getResultList();
	}

	@Transactional
	public void DeleteAllEstimadoByProducao(Long id) {
		this.em.createNativeQuery("DELETE FROM valor_estimado WHERE producao_id = " + id.toString()).executeUpdate();
	}

	@Transactional
	public void DeleteAllFromEstimado() {
		this.em.createNativeQuery("DELETE FROM valor_estimado").executeUpdate();
	}

	@Transactional
	public void CreateEstimado(ValorEstimado valorEstimado) {
		this.em.persist(valorEstimado);
	}
	
	public List<String> getAnoGastoByProducaoEstimado(Long id) {
		try {
			return em.createNativeQuery("SELECT DISTINCT(ano_fiscal) FROM valor_estimado WHERE producao_id = "+id+" ORDER BY ano_fiscal").getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public List<ValorEstimado> getEstimadoByProdAndAno(Long id,String ano) {
		try {
			return em.createQuery("SELECT ve FROM ValorEstimado ve WHERE ve.producao.id = :prodId and ve.anoFiscal = :ano ORDER BY ve.anoFiscal", ValorEstimado.class)
					.setParameter("prodId", id).setParameter("ano", ano).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	@Transactional
	public void geraValorPostulado() {
		List<Producao> producoes = producaoService.getAll();
		try {
			for (Producao producao : producoes) {
				System.out.println(producao.getId());
				List<ItemValores> checkCreate = ItemValor.getCheckPostuladoByProducao(producao.getId());
				if (checkCreate.size() != FindPostuladoByProducao(producao.getId()).size()) {
					try {
						DeleteFromPostuladoByProducao(producao.getId());
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("n�o criado ainda");
					}
					Integer a = 0;
					String fechaInicio = "0";
					String fechaGasto = "0";
					try {
						if (!(ItemValor.getFechaInicioByProducao(producao.getId()).getValor()).isEmpty()) {
							fechaInicio = ItemValor.getFechaInicioByProducao(producao.getId()).getValor();

						} else if (!(ItemValor.getFechaGastoByProducao(producao.getId()).getValor()).isEmpty()) {
							fechaGasto = ItemValor.getFechaGastoByProducao(producao.getId()).getValor();

						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					Integer fechaGastoAno = 0;
					Integer fechaInicioAno = 0;
					try {
						String parts[] = fechaInicio.split("/");
						fechaInicioAno = Integer.parseInt(parts[2]);
					} catch (Exception e) {
						fechaInicioAno = 0;
					}
					try {
						String part[] = fechaGasto.split("/");
						fechaGastoAno = Integer.parseInt(part[2]);
					} catch (Exception e) {
						fechaGastoAno = 0;
					}
					String fechaReferencia = "";

					if (fechaInicioAno <= 0 || fechaInicioAno == null) {
						fechaReferencia = producao.getAno();
					} else if (fechaInicioAno > fechaGastoAno) {
						fechaReferencia = String.valueOf(fechaInicioAno);
					} else {
						fechaReferencia = String.valueOf(fechaGastoAno);
					}
					System.out.println(fechaReferencia);
					for (Long i = 0L; i <= 25L; i++) {
						ValorPostulado valor = new ValorPostulado();

						valor.setProducao(producao);
						valor.setFechaReferencia(fechaReferencia);
						valor.setCampanha(producao.getAno());
						valor.setContrato(producao.getContrato());

						if (valor.getAnoFiscal() == null) {
							valor.setAnoFiscal("2013");
						}
						BigDecimal postulado = new BigDecimal("0.00");
						try {
							if (ItemValor.getPresupuestoPostuladoByProducao(producao.getId(), 63L + i).getValor().contains(",")) {
								postulado = new BigDecimal(ItemValor
										.getPresupuestoPostuladoByProducao(producao.getId(), 63L + i).getValor().replace(".", "").replace(",", "."));
							}else {
								postulado = new BigDecimal(ItemValor
										.getPresupuestoPostuladoByProducao(producao.getId(), 63L + i).getValor());
							}
							System.out.println(postulado);
							Integer newYear = Integer.parseInt(valor.getFechaReferencia().toString()) + a;
							if (postulado.compareTo(new BigDecimal("0.00")) == 0 || postulado != null) {
								try {
									BigDecimal apostulado = postulado;
								} catch (Exception e) {
									break;
									// TODO: handle exception
								}
							}
							System.out.println("socorro");
							if (Integer.parseInt(valor.getCampanha()) >= Integer.parseInt(valor.getFechaReferencia())) {
								System.out.println("deu ruim aqui");
								valor.setValor(postulado.toString());
								valor.setAnoFiscal(newYear.toString());
								CreatePostulado(valor);
							} else {
								// a++;
								// continue;
							}
							a++;
						} catch (Exception e) {
							// TODO: handle exception
							break;
						}

					}

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	@Transactional
	public void DeleteFromPostuladoByProducao(Long id) {
		this.em.createNativeQuery("DELETE FROM valor_postulado WHERE producao_id = " + id.toString()).executeUpdate();
	}

	@Transactional
	public void CreatePostulado(ValorPostulado valorPostulado) {
		this.em.persist(valorPostulado);
	}
	public List<ValorPostulado> FindPostuladoByProducao(Long id) {
		return this.em.createQuery("SELECT vp FROM ValorPostulado vp WHERE vp.producao.id = :pId", ValorPostulado.class)
				.setParameter("pId", id).getResultList();
	}
	public List<String> getAnoGastoByProducaoPostulado(Long id) {
		try {
			return em.createNativeQuery("SELECT DISTINCT(ano_fiscal) FROM valor_postulado WHERE producao_id = " + id
					+ " ORDER BY ano_fiscal").getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public List<ValorPostulado> getPostuladoByProdAndAno(Long id, String ano) {
		try {
			return em.createQuery(
					"SELECT vp FROM ValorPostulado vp WHERE vp.producao.id = :prodId and vp.anoFiscal = :ano ORDER BY vp.anoFiscal",
					ValorPostulado.class).setParameter("prodId", id).setParameter("ano", ano).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
		@Transactional
		public void geraValorAprovado() {
			List<Producao> producoes = producaoService.getAll();
	 		try {
				for (Producao producao : producoes) {
					System.out.println(producao.getId());
					List<ItemValores> checkCreate = ItemValor.getCheckAprovadoByProducao(producao.getId());
					if (checkCreate.size() != FindAprovadoByProducao(producao.getId()).size()) {
						try {
							DeleteFromAprovadoByProducao(producao.getId());
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println(e);
						}
						Integer a = 0;
						String fechaInicio = ItemValor.getFechaInicioByProducao(producao.getId()).getValor();
						System.out.println("fechaInicio  " +fechaInicio);
						Integer fechaInicioAno = 0;

						try {
							String parts[] = fechaInicio.split("/");
							fechaInicioAno = Integer.parseInt(parts[2]);
						} catch (Exception e) {
							fechaInicioAno = 0;
						}
						String fechaGasto = ItemValor.getFechaGastoByProducao(producao.getId()).getValor();
						Integer fechaGastoAno = 0;
						System.out.println("fechaGasto "+ fechaGasto);
						try {
							String part[] = fechaGasto.split("/");
							fechaGastoAno = Integer.parseInt(part[2]);
						} catch (Exception e) {
							fechaGastoAno = 0;
						}
						String fechaReferencia = "";

						if (fechaInicioAno <= 0 || fechaInicioAno == null) {
							fechaReferencia = producao.getAno();
						} else if (fechaInicioAno > fechaGastoAno) {
							fechaReferencia = String.valueOf(fechaInicioAno);
						} else {
							fechaReferencia = String.valueOf(fechaGastoAno);
						}
						System.out.println("fecha");
						for (Long i = 0L; i < 25L; i++) {
							ValorAprovado valor = new ValorAprovado();

							valor.setProducao(producao);
							valor.setFechaReferencia(fechaReferencia);
							valor.setCampanha(producao.getAno());

							if (valor.getAnoFiscal() == null) {
								valor.setAnoFiscal("2013");
							}
							BigDecimal aprovado = new BigDecimal("0.00");
							try {
								String testeAprovado = ItemValor.getPresupuestoAprovadoByProducao(producao.getId(), 125L + i).getValor();
								if (!testeAprovado.isEmpty()) {
									if (ItemValor.getPresupuestoAprovadoByProducao(producao.getId(), 125L + i).getValor().contains(",")) {
										aprovado = new BigDecimal(ItemValor.getPresupuestoAprovadoByProducao(producao.getId(), 125L + i).getValor().replace(".", "").replace(",","."));
									}else {
										aprovado = new BigDecimal(ItemValor.getPresupuestoAprovadoByProducao(producao.getId(), 125L + i).getValor());
									}
								
								
								
								Integer newYear = Integer.parseInt(valor.getFechaReferencia().toString()) + a;
								if (aprovado.compareTo(new BigDecimal("0.00")) != 0 || aprovado != null) {
									try {
										BigDecimal provado = aprovado;
									} catch (Exception e) {
										break;
										// TODO: handle exception
									}
									System.out.println(aprovado);
								}

								System.out.println(valor.getCampanha() + " a " + valor.getFechaReferencia());
								System.out.println(valor.getCampanha()); 
								System.out.println(valor.getFechaReferencia());
								if (newYear >= Integer.parseInt(valor.getFechaReferencia())) {
									System.out.println("aqui");
									valor.setValor(aprovado.toString());
									valor.setAnoFiscal(newYear.toString());
									System.out.println("criado");
									CreateAprovado(valor);
								} else {
									// a++;
									// continue;
								}
								a++;
								}
							} catch (Exception e) {
								// TODO: handle exception
								System.out.println("erou");						
								e.printStackTrace();
								break;
							}

						}

					}
				}

			} catch (

			Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		@Transactional
		public void DeleteFromAprovadoByProducao(Long id) {
			this.em.createNativeQuery("DELETE FROM valor_aprovado WHERE producao_id = " + id.toString()).executeUpdate();
		}

		@Transactional
		public void CreateAprovado(ValorAprovado valorAprovado) {
			this.em.persist(valorAprovado);
		}

		@Transactional
		public void DeleteAllFromAprovado() {
			this.em.createNativeQuery("DELETE FROM valor_aprovado").executeUpdate();
		}

		public String getAprovadoByProducao(Long id) {
			try {
				return em.createQuery("SELECT sum(va.valor) FROM ValorAprovado va WHERE va.producao.id = :pId", String.class)
						.setParameter("pId", id).getSingleResult();
			} catch (Exception e) {
				// TODO: handle exception
				return null;
			}
		}

		public List<ValorAprovado> FindAprovadoByProducao(Long id) {
			return this.em.createQuery("SELECT va FROM ValorAprovado va WHERE va.producao.id = :pId", ValorAprovado.class)
					.setParameter("pId", id).getResultList();
		}
		
		
		
		
		
}