package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.BalanceteCalculo;

public interface BalanceteCalculoRepository {

	void create(BalanceteCalculo balancete);

	void delete(BalanceteCalculo balancete);
		
	BalanceteCalculo update(BalanceteCalculo balancete);
	
	List<BalanceteCalculo> getAll();
	
	BalanceteCalculo findById(Long id);
	
	List<BalanceteCalculo> getBalanceteCalculoByProducao(Long id);
	
}
