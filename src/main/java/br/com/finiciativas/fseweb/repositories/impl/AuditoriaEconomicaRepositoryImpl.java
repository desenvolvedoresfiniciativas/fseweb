package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.AuditoriaEconomica;

@Repository
public class AuditoriaEconomicaRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(AuditoriaEconomica AuditoriaEconomica) {
		this.em.persist(AuditoriaEconomica);
	}

	public void delete(AuditoriaEconomica AuditoriaEconomica) {
		this.em.remove(AuditoriaEconomica);
	}

	@Transactional
	public AuditoriaEconomica update(AuditoriaEconomica AuditoriaEconomica) {
		return this.em.merge(AuditoriaEconomica);
	}
	
	public List<AuditoriaEconomica> getAll() {
		return this.em.createQuery("SELECT distinct o FROM AuditoriaEconomica o ", AuditoriaEconomica.class)
				.getResultList();
	}
	
	public AuditoriaEconomica findById(Long id) {
		return this.em.find(AuditoriaEconomica.class, id);
	}
	
	public List<AuditoriaEconomica> getAuditoriaEconomicaByProducao(Long id) {
		return this.em.createQuery("SELECT distinct o FROM AuditoriaEconomica o WHERE o.producao.id = :pId ", AuditoriaEconomica.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
