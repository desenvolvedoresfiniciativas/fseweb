package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.consultor.Premio;

public interface PremioRepository {

	void delete(Premio premio);
	
	Premio update(Premio premio);
	
	Premio findById(Long id);
	
	Premio findByConsultorAndCampanha(Long idConsultor, String campanha);
	
	Premio getLastAdded();
	
	List<Premio> getAll();
	
	List<Premio> getAllByAnalista(Long id);
	
	List<Premio> getAllByGestor(Long id);

	List<Premio> getAllByConsultor(Long id);

	List<Premio> getAllByProducao(Long id);

	List<Premio> getAllByContrato(Long id);
	
	boolean checkIfPremioExistsInProducao(Long id);

	boolean checkIfPremioExistsInContrato(Long id);
	
	boolean checkIfExistsByConsultorAndCampanha(Long idConsultor, String campanha);

}
