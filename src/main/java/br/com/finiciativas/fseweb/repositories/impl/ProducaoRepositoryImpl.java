package br.com.finiciativas.fseweb.repositories.impl;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Especialidade;
import br.com.finiciativas.fseweb.enums.Estado;
import br.com.finiciativas.fseweb.enums.SetorAtividade;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.Equipe;
import br.com.finiciativas.fseweb.models.cliente.Cliente;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.consultor.Role;
import br.com.finiciativas.fseweb.models.producao.PreProducao;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.producao.SubProducao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.EtapasConclusao;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.Projetos;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioSintetico;
import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnico;
import br.com.finiciativas.fseweb.models.produto.EtapaTrabalho;
import br.com.finiciativas.fseweb.models.produto.ItemTarefa;
import br.com.finiciativas.fseweb.models.produto.ItemValores;
import br.com.finiciativas.fseweb.models.produto.Produto;
import br.com.finiciativas.fseweb.models.produto.Tarefa;
import br.com.finiciativas.fseweb.models.produto.ordenacao.OrdemItensTarefa;
import br.com.finiciativas.fseweb.pojo.SeguimentoPojo;
import br.com.finiciativas.fseweb.services.impl.BalanceteCalculoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ClienteServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ComissaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EficienciaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EquipeServiceImpl;
import br.com.finiciativas.fseweb.services.impl.EtapasConclusaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ItemValoresServiceImpl;
import br.com.finiciativas.fseweb.services.impl.OrdemItensTarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.RelatorioTecnicoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.SubProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.TarefaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Repository
public class ProducaoRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Autowired
	private ItemValoresServiceImpl itemValoresService;

	@Autowired
	private TarefaServiceImpl TarefaService;

	@Autowired
	private ItemTarefaServiceImpl itemTarefaService;

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ClienteServiceImpl clienteService;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private OrdemItensTarefaServiceImpl ordemService;

	@Autowired
	private EtapasConclusaoServiceImpl etapasConclusaoService;

	@Autowired
	private EquipeServiceImpl equipeService;

	private SubProducaoServiceImpl subProducaoService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;

	@Autowired
	private BalanceteCalculoServiceImpl balanceteCalculoService;

	@Autowired
	private RelatorioTecnicoServiceImpl relatorioTecnicoService;

	@Autowired
	private ValorNegocioServiceImpl vnService;

	@Autowired
	private EficienciaServiceImpl eficienciaService;

	@Autowired
	private ComissaoServiceImpl comissaoService;

	public List<Producao> getAll() {
		return this.em.createQuery("SELECT distinct p FROM Producao p ", Producao.class).getResultList();
	}

	@Transactional
	public void deleteProducao(Long id) {

		Producao producao = findById(id);

		itemValoresService.deleteOfProducao(producao.getId());

		try {
			subProducaoService.deleteSubProdOfProducao(id);
		} catch (Exception e) {
			// TODO: handle exception
		}

		etapasConclusaoService.deleteOfProducao(producao.getId());
		faturamentoService.unlinkFaturamentoOfProducao(producao.getId());
		comissaoService.unlinkComissaoOfVNByProducao(producao.getId());
		comissaoService.unlinkComissaoOfProducao(id);
		balanceteCalculoService.deleteOfProducao(producao.getId());
		relatorioTecnicoService.deleteOfProducao(producao.getId());
		vnService.deleteOfProducao(producao.getId());
		eficienciaService.deleteOfProducao(producao.getId());

		em.flush();
		delete(producao);

	}

	public Producao checkForProducao(Long idCliente, Long idProduto, String ano) {
		String sQuery = "SELECT p FROM Producao p JOIN FETCH p.contrato WHERE p.cliente.id = :pClienteId AND p.produto.id = :pProdutoId AND p.ano = :pAno";
		try {
			return em.createQuery(sQuery, Producao.class).setParameter("pClienteId", idCliente)
					.setParameter("pProdutoId", idProduto).setParameter("pAno", ano).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public PreProducao checkForPreProducao(Long idCliente, String ano) {
		String sQuery = "SELECT p FROM PreProducao p WHERE p.cliente.id = :pClienteId AND p.campanha = :pAno";
		try {
			return em.createQuery(sQuery, PreProducao.class).setParameter("pClienteId", idCliente)
					.setParameter("pAno", ano).getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	@Transactional
	public Producao create(Producao producao) {

		producao.setCliente(producao.getCliente());

		return this.em.merge(producao);
	}

	public void delete(Producao producao) {
		this.em.remove(producao);
	}

	@Transactional
	public Producao update(Producao producao) {

		Producao prodAtt = findById(producao.getId());

		prodAtt.setConsultor(producao.getConsultor());
		prodAtt.setFilial(producao.getFilial());
		prodAtt.setEquipe(producao.getEquipe());

		return this.em.merge(prodAtt);

	}

	@Transactional
	public Producao updateOperacional(Producao producao) {
		return this.em.merge(producao);

	}

	public Producao findById(Long id) {
		return this.em.find(Producao.class, id);
	}

	public List<Producao> getProducaoByContrato(Long id) {

		String sQuery = "SELECT p FROM Producao p WHERE p.contrato.id = :pIdContrato ";

		return em.createQuery(sQuery, Producao.class).setParameter("pIdContrato", id).getResultList();
	}

	public List<Producao> getProducaoAByContrato(Long id) {

		String sQuery = "SELECT p FROM Producao p WHERE p.contrato.id = :pIdContrato ";

		return em.createQuery(sQuery, Producao.class).setParameter("pIdContrato", id).getResultList();
	}

	public List<Producao> getProducaoByConsultor(Long id) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE p.consultor.id = :pId ORDER BY p.cliente.razaoSocial";
			return em.createQuery(sQuery, Producao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Producao> prods = new ArrayList<Producao>();
			return prods;
		}
	}

	public List<Producao> getProducaoByEquipe(Long id) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE p.equipe.lider.id = :pId ORDER BY p.cliente.razaoSocial";

			return em.createQuery(sQuery, Producao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public List<Producao> getProducaoByCoordenador(Long id) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE p.equipe.coordenador.id = :pId ORDER BY p.cliente.razaoSocial";

			return em.createQuery(sQuery, Producao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public List<Producao> getProducaoByGerente(Long id) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE p.equipe.gerente.id = :pId ORDER BY p.cliente.razaoSocial";

			return em.createQuery(sQuery, Producao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public List<Producao> getProducaoByDiretor() {
		try {
			String sQuery = "SELECT distinct p FROM Producao p JOIN FETCH p.contrato ORDER BY p.cliente.razaoSocial";

			return em.createQuery(sQuery, Producao.class).getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	public List<Producao> getProducaoDaEquipe(Long id) {
		try {
			String sQuery = "SELECT p FROM Producao p JOIN FETCH p.equipe_consultores WHERE p.equipe.id = equipe_consultores.id ORDER BY p.cliente.razaoSocial";
			String nova = "SELECT p FROM Producao p WHERE p.equipe.id  = :eId";
			return em.createQuery(nova, Producao.class).setParameter("eId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Producao> prods = new ArrayList<Producao>();
			return prods;
		}
	}

	public Producao getLastItemAdded() {

		String sQuery = "SELECT p " + "FROM Producao p " + "WHERE p.id=(SELECT max(id) FROM Producao)";

		return em.createQuery(sQuery, Producao.class).getSingleResult();
	}

	public void createItemValores(Producao producao) {

		Produto prod = producao.getProduto();

		List<EtapaTrabalho> etapa = new ArrayList<>();
		List<Tarefa> tarefas = new ArrayList<>();
		List<ItemTarefa> itens = new ArrayList<>();

		etapa.addAll(prod.getEtapasTrabalho());

		for (EtapaTrabalho etapaTrabalho : etapa) {
			if (!etapaTrabalho.isSubProducao()) {
				tarefas.addAll(etapaTrabalho.getTarefas());
			}
		}

		for (Tarefa tarefa : tarefas) {

			itens = itemTarefaService.getItensByTarefa(TarefaService.findById(tarefa.getId()));
			Long id = tarefa.getId();
			producaoService.addValor(id, itens, producao);

		}

		limpaItemTarefas(itens);
	}

	public void addValor(Long id, List<ItemTarefa> itens, Producao producao) {
		for (ItemTarefa itemTarefa : itens) {
			ItemValores valores = new ItemValores();
			valores.setItem(itemTarefa);
			valores.setTarefa(TarefaService.findById(id));
			valores.setProducao(producao);
			valores.setNumeroOrdem(0);
			valores.setSubProducao(false);
			List<Tarefa> tarefa = new ArrayList<>();
			tarefa.add(TarefaService.findById(id));
			itemTarefa.setTarefa(tarefa);
			itemTarefaService.update(itemTarefa);
			itemValoresService.create(valores);
		}

	}

	public void addValorSubProducao(Long id, List<ItemTarefa> itens, SubProducao subProd) {
		for (ItemTarefa itemTarefa : itens) {
			ItemValores valores = new ItemValores();
			valores.setItem(itemTarefa);
			valores.setTarefa(TarefaService.findById(id));
			valores.setProducao(subProd.getProducao());
			valores.setSubProducao(true);
			valores.setSubProducaoIndex(subProd);
			List<Tarefa> tarefa = new ArrayList<>();
			tarefa.add(TarefaService.findById(id));
			itemTarefa.setTarefa(tarefa);
			itemTarefaService.update(itemTarefa);
			itemValoresService.create(valores);
		}

	}

	public void limpaItemTarefas(List<ItemTarefa> itens) {
		for (ItemTarefa itemTarefa : itens) {
			itemTarefa.setTarefa(null);
			itemTarefaService.update(itemTarefa);
		}
	}

	public List<RelatorioTecnico> getRelatorioAnalitico(String ano, SetorAtividade setor, Especialidade especialidade,
			Long idEquipe, Long idConsultor, Long idFeitoPor, Long idCorrigidoPor, Estado estado) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<RelatorioTecnico> root = cq.from(RelatorioTecnico.class);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));

		// Constructing list of parameters(Construindo lista de parametros)
		List<Predicate> predicates = new ArrayList<Predicate>();

		// Parametro fixo no predicado para buscar somente Lei do Bem
		predicates.add(qb.equal(root.get("producao").get("produto").get("id"), 23l));

		// Adding predicates in case of parameter not being null(Adicionando predicados
		// no caso do parametro n�o ser nulo)

		if (ano != null) {
			predicates.add(qb.equal(root.get("producao").get("ano"), ano));
		}

		if (setor != null) {
			predicates.add(qb.equal(root.get("producao").get("cliente").get("setorAtividade"), setor));
		}

		if (especialidade != null) {
			predicates.add(qb.equal(root.get("especialidade"), especialidade));
		}

		if (idEquipe != null) {
			predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), idEquipe));
		}

		if (idConsultor != null) {
			System.out.println("consultor" + idConsultor);
			predicates.add(qb.equal(root.get("producao").get("consultor").get("id"), idConsultor));
		} else if (idConsultor == null && equipe != null) {
			System.out.println("AQUI, PAPAI");
			predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), equipe.getId()));
		}

//		if (idConsultor != null) {
//			predicates.add(qb.equal(root.get("producao").get("consultor").get("id"), idConsultor));
//		} else {
//			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//			Consultor consultor = ((Consultor) auth.getPrincipal());
//			predicates.add(qb.equal(root.get("producao").get("consultor").get("id"), consultor.getId()));
//		}

		if (idFeitoPor != null) {
			predicates.add(qb.equal(root.get("feitoPor").get("id"), idFeitoPor));
		}

		if (idCorrigidoPor != null) {
			predicates.add(qb.equal(root.get("validadoPor").get("id"), idCorrigidoPor));
		}

		if (estado != null) {
			predicates.add(qb.equal(root.get("estado"), estado));
		}

		// query itself
		cq.orderBy(qb.desc(root.get("producao").get("cliente").get("razaoSocial")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result(Executa a query e faz algo com o
		// resultado)
		return em.createQuery(cq).getResultList();
	}

	public List<Producao> getRelatorioSintetico(String ano, SetorAtividade setorAtividade, Long idConsultor,
			Long idEquipe) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Producao> root = cq.from(Producao.class);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Consultor consultor = ((Consultor) auth.getPrincipal());
		Equipe equipe = ((Equipe) equipeService.findEquipeByConsultor(consultor.getId()));

		// Constructing list of parameters
		List<Predicate> predicates = new ArrayList<Predicate>();

		// Adding predicates in case of parameter not being null
		if (ano != null) {
			System.out.println("ano" + ano);
			predicates.add(qb.equal(root.get("ano"), ano));
		}

		if (setorAtividade != null) {
			System.out.println("setor" + setorAtividade);
			predicates.add(qb.equal(root.get("cliente").get("setorAtividade"), setorAtividade));
		}

		if (idConsultor != null) {
			System.out.println("consultor" + idConsultor);
			predicates.add(qb.equal(root.get("consultor").get("id"), idConsultor));
		} else if (idConsultor == null && equipe != null) {
			System.out.println("AQUI, PAPAI");
			predicates.add(qb.equal(root.get("equipe").get("id"), equipe.getId()));
		}

		if (idEquipe != null) {
			System.out.println("equipe" + idEquipe);
			predicates.add(qb.equal(root.get("equipe").get("id"), idEquipe));
		}

		// query itself
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result
		return em.createQuery(cq).getResultList();
	}

	public void bindProjetosToRelatorio(List<Projetos> proj) {
		clearTableRelatorios();

		for (Projetos projetos : proj) {
			String razaoSocial = projetos.getProducao().getCliente().getRazaoSocial();

			if (verificarlinha(razaoSocial) == false) {

				RelatorioSintetico rel = new RelatorioSintetico();
				rel.setRazaoSocial(razaoSocial);
				rel.setSetorAtividade(projetos.getProducao().getCliente().getSetorAtividade().toString());

				int pendente = 0;
				int redacao = 0;
				int correcao = 0;
				int completo = 0;
				int revisao = 0;
				int cliente = 0;
				int aprovado = 0;
				int total = 0;

				rel.setaRedigir(pendente);
				rel.setRedacao(redacao);
				rel.setCorrecao(correcao);
				rel.setCompleo(completo);
				rel.setRevisao(revisao);
				rel.setCliente(cliente);
				rel.setAprovado(aprovado);
				rel.setRelCadastrados(total + 1);

				if (projetos.getEstadoEconomico() != null) {
					if (projetos.getEstadoEconomico() == Estado.PENDENTE) {
						rel.setaRedigir(pendente + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.EM_REDA��O) {
						rel.setRedacao(redacao + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.EM_CORRECAO) {
						rel.setCorrecao(correcao + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.COMPLETO) {
						rel.setCompleo(completo + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.EM_REVISAO) {
						rel.setRevisao(revisao + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.CLIENTE) {
						rel.setCliente(cliente + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.APROVADO) {
						rel.setAprovado(aprovado + 1);
					}
				}

				if (projetos.getEstadoTecnico() != null) {
					if (projetos.getEstadoTecnico() == Estado.PENDENTE) {
						rel.setaRedigir(pendente + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.EM_REDA��O) {
						rel.setRedacao(redacao + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.EM_CORRECAO) {
						rel.setCorrecao(correcao + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.COMPLETO) {
						rel.setCompleo(completo + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.EM_REVISAO) {
						rel.setRevisao(revisao + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.CLIENTE) {
						rel.setCliente(cliente + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.APROVADO) {
						rel.setAprovado(aprovado + 1);
					}
				}
				em.merge(rel);
			} else {

				RelatorioSintetico rela = getCadastroExistente(razaoSocial);
				int pendente = rela.getaRedigir();
				int redacao = rela.getRedacao();
				int correcao = rela.getCorrecao();
				int completo = rela.getCompleo();
				int revisao = rela.getRevisao();
				int cliente = rela.getCliente();
				int aprovado = rela.getAprovado();
				int total = rela.getRelCadastrados();

				rela.setRelCadastrados(total + 1);

				if (projetos.getEstadoEconomico() != null) {
					if (projetos.getEstadoEconomico() == Estado.PENDENTE) {
						rela.setaRedigir(pendente + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.EM_REDA��O) {
						rela.setRedacao(redacao + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.EM_CORRECAO) {
						rela.setCorrecao(correcao + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.COMPLETO) {
						rela.setCompleo(completo + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.EM_REVISAO) {
						rela.setRevisao(revisao + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.CLIENTE) {
						rela.setCliente(cliente + 1);
					}
					if (projetos.getEstadoEconomico() == Estado.APROVADO) {
						rela.setAprovado(aprovado + 1);
					}
				}
				if (projetos.getEstadoTecnico() != null) {
					if (projetos.getEstadoTecnico() == Estado.PENDENTE) {
						rela.setaRedigir(pendente + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.EM_REDA��O) {
						rela.setRedacao(redacao + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.EM_CORRECAO) {
						rela.setCorrecao(correcao + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.COMPLETO) {
						rela.setCompleo(completo + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.EM_REVISAO) {
						rela.setRevisao(revisao + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.CLIENTE) {
						rela.setCliente(cliente + 1);
					}
					if (projetos.getEstadoTecnico() == Estado.APROVADO) {
						rela.setAprovado(aprovado + 1);
					}
				}
				em.merge(rela);
			}
		}
	}

	public List<RelatorioSintetico> getAllRelatorio() {
		return this.em.createQuery("SELECT distinct p FROM RelatorioSintetico p ", RelatorioSintetico.class)
				.getResultList();
	}

	public RelatorioSintetico getCadastroExistente(String razaoSocial) {
		return this.em.createQuery("SELECT distinct p FROM RelatorioSintetico p WHERE p.razaoSocial = :pNome ",
				RelatorioSintetico.class).setParameter("pNome", razaoSocial).getSingleResult();
	}

	public boolean verificarlinha(String razaoSocial) {
		String query = "select case when (count(*) > 0)  then true else false end from RelatorioSintetico p WHERE p.razaoSocial = :pNome ";
		TypedQuery<Boolean> booleanQuery = em.createQuery(query, Boolean.class).setParameter("pNome", razaoSocial);
		boolean exists = booleanQuery.getSingleResult();
		if (exists == false) {
			return false;
		} else {
			return true;
		}
	}

	@Transactional
	public void clearTableRelatorios() {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaDelete<RelatorioSintetico> query = builder.createCriteriaDelete(RelatorioSintetico.class);
		query.from(RelatorioSintetico.class);
		em.createQuery(query).executeUpdate();
	}

	public void criarExcelAnalitico(List<Projetos> projetos, String caminho, String nomeArquivo) {

		String[] colunas = { "Raz�o Social", "Ano", "Setor Atividade", "Descri��o do projeto",
				"Rel. Econ�mico feito por", "Rel. Econ�mico validado por", "Rel. T�cnico feito por",
				"Rel. T�cnico validado por", "Estado Rel. Econ�mico", "Estado Rel. T�cnico" };

		Workbook workbook = new XSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();
		Sheet sheet = workbook.createSheet("Relat�rio Anal�tico");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < colunas.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(colunas[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 1;

		for (Projetos projeto : projetos) {
			Row row = sheet.createRow(rowNum++);

			// Raz�o Social
			if (projeto.getProducao().getCliente().getRazaoSocial() != null) {
				row.createCell(0).setCellValue(projeto.getProducao().getCliente().getRazaoSocial());
			}

			// Ano
			if (projeto.getProducao().getAno() != null) {
				row.createCell(1).setCellValue(projeto.getProducao().getAno());
			}

			// Setor Atividade
			if (projeto.getProducao().getCliente().getSetorAtividade() != null) {
				row.createCell(2).setCellValue(projeto.getProducao().getCliente().getSetorAtividade().toString());
			}

			// Descri��o
			if (projeto.getDescricao() != null) {
				row.createCell(3).setCellValue(projeto.getDescricao());
			}

			// Feito Por Economico
			if (projeto.getRelatorioEconomicoFeitoPor() != null) {
				row.createCell(4).setCellValue(projeto.getRelatorioEconomicoFeitoPor().getNome());
			}

			// Validado Por Economico
			if (projeto.getRelatorioEconomicoValidadoPor() != null) {
				row.createCell(5).setCellValue(projeto.getRelatorioEconomicoValidadoPor().getNome());
			}

			// Feito Por Tecnico
			if (projeto.getRelatorioTecnicoFeitoPor() != null) {
				row.createCell(6).setCellValue(projeto.getRelatorioTecnicoFeitoPor().getNome());
			}

			// Validado Por Tecnico
			if (projeto.getRelatorioTecnicoValidadoPor() != null) {
				row.createCell(7).setCellValue(projeto.getRelatorioTecnicoValidadoPor().getNome());
			}

			// Estado Economico
			if (projeto.getEstadoEconomico() != null) {
				row.createCell(8).setCellValue(projeto.getEstadoEconomico().toString());
			}

			// Estado Tecnico
			if (projeto.getEstadoTecnico() != null) {
				row.createCell(9).setCellValue(projeto.getEstadoTecnico().toString());
			}

			for (int i = 0; i < colunas.length; i++) {
				sheet.autoSizeColumn(i);
			}

		}

		try {
			String extensao = ".xlsx";
			FileOutputStream fileOut = new FileOutputStream(caminho + "\\" + nomeArquivo + extensao);
			System.out.println(caminho + "\\" + nomeArquivo + extensao);
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public void criarExcelSintetico(List<RelatorioSintetico> relatorios, String caminho, String nomeArquivo) {

		String[] colunas = { "Raz�o Social", "Setor Atividade", "Relat�rios Cadastrados", "Pendente", "Em reda��o",
				"Completo", "Em revis�o", "Cliente", "Aprovado" };

		Workbook workbook = new XSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();
		Sheet sheet = workbook.createSheet("Relat�rio Anal�tico");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < colunas.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(colunas[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 1;

		for (RelatorioSintetico relatorio : relatorios) {
			Row row = sheet.createRow(rowNum++);

			// Raz�o Social
			if (relatorio.getRazaoSocial() != null) {
				row.createCell(0).setCellValue(relatorio.getRazaoSocial());
			}

			// Setor Atividade
			if (relatorio.getSetorAtividade() != null) {
				row.createCell(1).setCellValue(relatorio.getSetorAtividade());
			}

			// Cadastrados
			row.createCell(2).setCellValue(relatorio.getRelCadastrados());

			// Pendente
			row.createCell(3).setCellValue(relatorio.getaRedigir());

			// Em Reda��o
			row.createCell(4).setCellValue(relatorio.getRedacao());

			// Completo
			row.createCell(5).setCellValue(relatorio.getCompleo());

			// Em Revis�o
			row.createCell(6).setCellValue(relatorio.getRevisao());

			// Cliente
			row.createCell(7).setCellValue(relatorio.getCliente());

			// Aprovado
			row.createCell(8).setCellValue(relatorio.getAprovado());

			for (int i = 0; i < colunas.length; i++) {
				sheet.autoSizeColumn(i);
			}

		}

		try {
			String extensao = ".xlsx";
			FileOutputStream fileOut = new FileOutputStream(caminho + "\\" + nomeArquivo + extensao);
			System.out.println(caminho + "\\" + nomeArquivo + extensao);
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public List<Producao> getProducaoByProduto(Long id) {

		String sQuery = "SELECT p FROM Producao p WHERE p.produto.id = :pId";

		return em.createQuery(sQuery, Producao.class).setParameter("pId", id).getResultList();
	}

	public List<ItemValores> getItensByProdutoAndDefinidor(Long id) {

		String sQuery = "SELECT i FROM ItemValores p WHERE p. = :pId";

		return em.createQuery(sQuery, ItemValores.class).setParameter("pId", id).getResultList();
	}

	public Producao getProducaoByAnoEmpresaProduto(String ano, Long id, Long idProd) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE p.ano = :ano AND p.produto.id = :pIdProd AND p.cliente.id = :id";

			return em.createQuery(sQuery, Producao.class).setParameter("ano", ano).setParameter("id", id)
					.setParameter("pIdProd", idProd).getSingleResult();

		} catch (Exception e) {
			System.out.println("N�o foi encontrada produ��o com os parametros, m�todo: getProducaoByAnoEmpresaProduto");
			return null;
		}

	}

	public Producao getProducaoByAnoClienteProdutoOnlyNN(String ano, Long idCliente, Long idProd) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE p.ano = :pAno AND p.cliente.id = :pIdCliente AND p.produto.id = :pIdProd AND tipo_de_negocio = 'nn'";

			return em.createQuery(sQuery, Producao.class).setParameter("pAno", ano)
					.setParameter("pIdCliente", idCliente).setParameter("pIdProd", idProd).getSingleResult();

		} catch (Exception e) {
			System.out.println(
					"N�o foi encontrada produ��o com os parametros, m�todo: getProducaoByAnoClienteProdutoOnlyNN");
			return null;
		}

	}

	public Producao getProducaoByAnoAndRazaoSocial(String ano, String nome) {

		String sQuery = "SELECT p FROM Producao p WHERE p.ano = :ano AND p.cliente.razaoSocial = :nome";

		return em.createQuery(sQuery, Producao.class).setParameter("ano", ano).setParameter("nome", nome)
				.getSingleResult();

	}

	public List<Producao> getAllProducaoBynome(String nome) {
		return this.em.createQuery("SELECT distinct p FROM Producao p WHERE p.produto.nome = :pNome ", Producao.class)
				.setParameter("pNome", nome).getResultList();
	}

	public List<Producao> getAllProducaoByConsultorAndSituacao(Situacao situacao, Long id) {
		return this.em.createQuery(
				"SELECT distinct p FROM Producao p WHERE p.situacao = :pSituacao AND p.consultor.id = :pId",
				Producao.class).setParameter("pSituacao", situacao).setParameter("pId", id).getResultList();
	}

	public List<Producao> getAllProducaoByConsultorAndNN(Long id) {
		System.out.println("entrou na query" + id);

		String sQuery = "SELECT p FROM Producao p JOIN FETCH p.contrato WHERE p.contrato.comercialResponsavelEntrada.id = :pConsultorId AND p.tipoDeNegocio = 'NN'";
		try {
			return em.createQuery(sQuery, Producao.class).setParameter("pConsultorId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public List<Producao> getAllProducaoByEquipeAndSituacao(Situacao situacao, Long id) {
		return this.em.createQuery(
				"SELECT distinct p FROM Producao p WHERE p.situacao = :pSituacao AND p.equipe.lider.id = :pId",
				Producao.class).setParameter("pSituacao", situacao).setParameter("pId", id).getResultList();

	}

	public List<Producao> getAllProducaoByDiretorAndSituacao(Situacao situacao) {
		return this.em.createQuery("SELECT distinct p FROM Producao p WHERE p.situacao = :pSituacao", Producao.class)
				.setParameter("pSituacao", situacao).getResultList();

	}

	public List<Producao> getAllProducaoByConsultorAndAno(String ano, Long id) {
		return this.em.createQuery("SELECT distinct p FROM Producao p WHERE p.ano = :pAno AND p.consultor.id = :pId",
				Producao.class).setParameter("pAno", ano).setParameter("pId", id).getResultList();
	}

	public List<Producao> getAllProducaoByEquipeAndAno(String ano, Long id) {
		return this.em.createQuery("SELECT distinct p FROM Producao p WHERE p.ano = :pAno AND p.equipe.lider.id = :pId",
				Producao.class).setParameter("pAno", ano).setParameter("pId", id).getResultList();
	}

	public List<Producao> getAllProducaoByDiretorAndAno(String ano) {
		return this.em.createQuery("SELECT distinct p FROM Producao p WHERE p.ano = :pAno", Producao.class)
				.setParameter("pAno", ano).getResultList();
	}

	public Producao getProducaoChileCriteria(String ano, Long idProd, Long idCliente, String observacoes,
			String nomeProjeto, String nombreSIA) {
		try {
			return this.em.createQuery(
					"SELECT distinct p FROM Producao p WHERE p.ano = :pAno AND p.produto.id = :pProduto AND p.cliente.id = :pidCliente AND p.nomeProjeto = :pnomeProjeto AND p.observacoes = :pobservacoes AND p.nomeSIA = :pnombreSIA",
					Producao.class).setParameter("pAno", ano).setParameter("pProduto", idProd)
					.setParameter("pidCliente", idCliente).setParameter("pobservacoes", observacoes)
					.setParameter("pnomeProjeto", nomeProjeto).setParameter("pnombreSIA", nombreSIA).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public List<Producao> getProducoesByCliente(Long id) {

		String sQuery = "SELECT p FROM Producao p WHERE p.cliente.id = :pIdCliente";

		return em.createQuery(sQuery, Producao.class).setParameter("pIdCliente", id).getResultList();
	}

	public List<Producao> getAllByAno(String ano) {
		return this.em.createQuery(
				"SELECT distinct p FROM Producao p WHERE p.ano = :pAno AND p.cliente.ativo = 1 AND p.produto.id = 23",
				Producao.class).setParameter("pAno", ano).getResultList();
	}

	public List<Producao> getAllByAnoWhereNN(String ano) {
		return this.em.createQuery("SELECT distinct p FROM Producao p WHERE p.ano = :pAno AND p.tipoDeNegocio = 'NN'",
				Producao.class).setParameter("pAno", ano).getResultList();
	}

	public List<Producao> getAllAtivasByAnoWhereNN(String ano) {
		return this.em.createQuery(
				"SELECT distinct p FROM Producao p WHERE p.ano = :pAno AND p.tipoDeNegocio = 'NN' AND p.situacao = 'ativa' or p.situacao = 'previsao_ativa' or p.situacao = 'negociacao'",
				Producao.class).setParameter("pAno", ano).getResultList();
	}

	public List<SubProducao> getSubproducoesOfProducao(Long id) {
		return this.em.createQuery("SELECT p FROM SubProducao p WHERE p.producao.id = :pId", SubProducao.class)
				.setParameter("pId", id).getResultList();
	}

	public List<SeguimentoPojo> getProducaoByCriteria(String razaoSocial, String ano, Long idFilial, Long idEquipe,
			Long idConsultor, Situacao situacao, SetorAtividade setor) {
		
		StringBuilder query = new StringBuilder();
		query.append("SELECT p FROM Producao p WHERE p.produto.id = 23 and ");

		// Adding predicates in case of parameter not being null

		if (razaoSocial != null) {
			System.out.println("Entrou razao");
			query.append("p.cliente.razaoSocial LIKE '%" + razaoSocial + "' and ");
			// predicates.add(qb.like(root.get("cliente").get("razaoSocial"), '%' +
			// razaoSocial + '%'));
		}

		if (ano != null) {
			System.out.println("Entrou ano");
			query.append("p.ano = " + ano + " and ");
			// predicates.add(qb.equal(root.get("ano"), ano));
		}

		if (idFilial != null) {
			System.out.println("Entrou filial");
			query.append("p.filial.id = " + idFilial + " and ");
			// predicates.add(qb.equal(root.get("filial").get("id"), idFilial));
		}

		if (idEquipe != null) {
			System.out.println("Entrou equipe");
			query.append("p.equipe.id = " + idEquipe + " and ");
			// predicates.add(qb.equal(root.get("equipe").get("id"), idEquipe));
		}
		if (idConsultor != null) {
			System.out.println("Entrou consultor");
			query.append("p.consultor.id = " + idConsultor + " and ");
			// predicates.add(qb.equal(root.get("consultor").get("id"), idConsultor));
		}

		if (situacao != null) {
			System.out.println("Entrou situacao");
			query.append("p.situacao= " + situacao + " and ");
			// predicates.add(qb.equal(root.get("situacao"), situacao));
		}

		if (setor != null) {
			System.out.println("Entrou setor");
			query.append("p.setor = " + setor + " and ");
			// predicates.add(qb.equal(root.get("cliente").get("setorAtividade"), setor));
		}

		query.append("1=1");
		List<Producao> prod = em.createQuery(String.valueOf(query), Producao.class).getResultList();
		List<SeguimentoPojo> pojo = new ArrayList<SeguimentoPojo>();
		List<ItemValores> liv = em.createQuery("SELECT i FROM ItemValores i WHERE i.item.nome = 'REGIME TRIBUTA��O' or i.item.nome = 'LUCRO FISCAL' or i.item.nome = 'PREVIS�O ENTREGA C�LCULO' or i.item.nome = 'PREVIS�O VALOR DISP�NDIO' or i.item.nome = 'PREVIS�O VALOR EXCLUS�O'",ItemValores.class).getResultList();
		
		for (Producao p : prod) {
			SeguimentoPojo seg = new SeguimentoPojo();
			seg.setId(p.getId());
			seg.setAno(p.getAno());
			seg.setEquipe(p.getEquipe());
			seg.setConsultor(p.getConsultor());
			seg.setSituacao(p.getSituacao());
			seg.setMotivoInatividade(p.getMotivoInatividade());
			seg.setRazaoInatividade(p.getRazaoInatividade());
			seg.setTipoDeNegocio(p.getTipoDeNegocio());
			seg.setCliente(p.getCliente());
			System.out.println(p.getId());
			try {
				
				for (ItemValores i : liv) {

					if (i.getProducao().getId() == p.getId()) {
						switch (String.valueOf(i.getItem().getId())) {
						case "318":
							try {
								seg.setRegimeTributacao(i.getValor());
							} catch (Exception e) {
								seg.setRegimeTributacao("Error");
							}

							break;
						case "319":
							try {
								seg.setLucroFiscal(i.getValor());
							} catch (Exception e) {
								seg.setLucroFiscal("Error");
							}
							break;
						case "325":
							try {
								seg.setPrevisaoEntrega(i.getValor());
							} catch (Exception e) {
								seg.setPrevisaoEntrega("Error");
							}
							break;
						case "323":
							try {
								seg.setPrevValorDispendio(i.getValor());
							} catch (Exception e) {
								seg.setPrevValorDispendio("Error");
							}
							break;
						case "324":
							try {
								seg.setPrevValorExclusao(i.getValor());
							} catch (Exception e) {
								seg.setPrevValorExclusao("Error");
							}
							break;

						default:
							break;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			pojo.add(seg);
			System.out.println(p.getId());
		}
		return pojo;
	}

	public List<Producao> getProducaoByCodigo(String codigo) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<ItemValores> root = cq.from(ItemValores.class);

		// Constructing list of parameters
		List<Predicate> predicates = new ArrayList<Predicate>();

		System.out.println(codigo);

		predicates.add(qb.like(root.get("valor"), '%' + codigo + '%'));
		predicates.add(qb.equal(root.get("item").get("id"), 20l));

		// query itself
		cq.orderBy(qb.desc(root.get("producao").get("cliente").get("razaoSocial")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));
		// execute query and do something with result

		List<Producao> prods = new ArrayList<Producao>();
		List<ItemValores> itens = em.createQuery(cq).getResultList();

		System.out.println("Encontrou " + itens.size() + " produ��es");
		for (ItemValores itemValores : itens) {
			prods.add(itemValores.getProducao());
		}

		return prods;

	}

	public List<Producao> getProducaoByCriteriaOP(String razaoSocial, String ano, Long idFilial, Long idEquipe,
			Long idConsultor, Situacao situacao, SetorAtividade setor, Long idProduto) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Producao> root = cq.from(Producao.class);

		// Constructing list of parameters
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(qb.notEqual(root.get("produto").get("id"), 23l));

		// Adding predicates in case of parameter not being null

		if (razaoSocial != null) {
			predicates.add(qb.like(root.get("cliente").get("razaoSocial"), '%' + razaoSocial + '%'));
		}

		if (ano != null) {
			predicates.add(qb.equal(root.get("ano"), ano));
		}

		if (idFilial != null) {
			predicates.add(qb.equal(root.get("filial").get("id"), idFilial));
		}

		if (idEquipe != null) {
			predicates.add(qb.equal(root.get("equipe").get("id"), idEquipe));
		}

		if (idConsultor != null) {
			predicates.add(qb.equal(root.get("consultor").get("id"), idConsultor));
		}

		if (situacao != null) {
			predicates.add(qb.equal(root.get("situacao"), situacao));
		}

		if (setor != null) {
			predicates.add(qb.equal(root.get("cliente").get("setorAtividade"), setor));
		}

		if (idProduto != null) {
			predicates.add(qb.equal(root.get("produto").get("id"), idProduto));
		}

		// query itself
		cq.orderBy(qb.desc(root.get("cliente").get("razaoSocial")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result
		return em.createQuery(cq).getResultList();

	}

	public List<Producao> getProducoesByCnpjEProduto(String cnpj, Long produto) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE p.cliente.cnpj = :pCnpj AND p.produto.id = :pProduto";
			return em.createQuery(sQuery, Producao.class).setParameter("pCnpj", cnpj).setParameter("pProduto", produto)
					.getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Producao> prods = new ArrayList<Producao>();
			return prods;
		}
	}

	public void geraProducoes(String ano) {

		List<Cliente> clientes = clienteService.getAll();

		int i = 1;
		int j = 1;

		for (Cliente cliente : clientes) {

			System.out.println("Processando cliente " + i + " de " + clientes.size());

			Producao producao = new Producao();
			List<Producao> producoes = producaoService.getProducoesByCliente(cliente.getId());
			for (Producao producaoAtual : producoes) {
				j = 1;
				System.out.println("Processando producao " + j + " de " + producoes.size());
				if (producaoAtual.getAno() != "2020" || producaoAtual.getAno() != "2021") {

					producao.setAno(ano);
					producao.setCliente(cliente);
					producao.setConsultor(producaoAtual.getConsultor());
					producao.setContrato(producaoAtual.getContrato());
					producao.setEquipe(producaoAtual.getEquipe());
					producao.setFilial(producaoAtual.getFilial());
					producao.setProduto(producaoAtual.getProduto());
					producao.setTipoDeApuracao(producaoAtual.getTipoDeApuracao());

					if (producaoAtual.getSituacao() == Situacao.ATIVA
							|| producaoAtual.getSituacao() == Situacao.PREVISAO_ATIVA) {
						producao.setSituacao(Situacao.PREVISAO_ATIVA);
						producao.setTipoDeNegocio("RR");
					} else {
						producao.setSituacao(Situacao.PREVISAO_INATIVA);
						producao.setTipoDeNegocio("NN");
					}

					Producao prod = producaoService.create(producao);

					etapasConclusaoService.createEtapasConclusaoByProd(prod);

					producaoService.createItemValores(prod);

					List<OrdemItensTarefa> ordenacao = ordemService.getOrdenacaoByProduto(prod.getProduto().getId());

					for (OrdemItensTarefa ordemItem : ordenacao) {
						List<ItemValores> itens = itemValoresService.findByItemAndTarefaId(ordemItem.getItem().getId(),
								ordemItem.getTarefa().getId());
						for (ItemValores item : itens) {
							item.setNumeroOrdem(ordemItem.getNumero());

							itemValoresService.update(item);
						}
					}

					System.out.println("Producao gerada");

				} else {
					System.out.println("Producao n�o gerada por j� ter 2021");
				}
				j++;
			}
			i++;
		}
	}

	public List<Producao> getListaBalancete(Long id) {
		return this.em.createQuery("SELECT b FROM BalanceteCalculo b WHERE  b.producao.cliente.id  = :pId")
				.setParameter("pId", id).getResultList();
	}

	public List<Producao> getListaRelatorioTecnico(Long id) {
		return this.em.createQuery("SELECT r FROM RelatorioTecnico r WHERE  r.producao.cliente.id  = :pId")
				.setParameter("pId", id).getResultList();
	}

	public List<Producao> getListaBalanceteFaturamento(Long idProducao) {
		return this.em.createQuery("SELECT b FROM BalanceteCalculo b WHERE  b.producao.id  = :pId")
				.setParameter("pId", idProducao).getResultList();
	}

	public List<Producao> getProducoesByClienteProdutoCampanha(Long id, Long produto, String ano) {
		try {
			String sQuery = "SELECT p FROM Producao p WHERE (p.cliente.id = :pCid AND p.produto.id = :pProduto AND p.ano = :pAno)";
			return em.createQuery(sQuery, Producao.class).setParameter("pCid", id).setParameter("pProduto", produto)
					.setParameter("pAno", ano).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Producao> prods = new ArrayList<Producao>();
			return prods;
		}
	}

	public Producao getProducaoById(Long id) {

		String sQuery = "SELECT p FROM Producao p WHERE p.id = :pId";

		return em.createQuery(sQuery, Producao.class).setParameter("pId", id).getSingleResult();
	}

	public List<EtapasConclusao> getProducaoAndEtapasByCriteria(String razaoSocial, String ano, String siglaConsultor,
			Long idEquipe) {

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		// Metamodel m = em.getMetamodel();
		// EntityType<EtapasConclusao> metaModel = m.entity(EtapasConclusao.class);
		Root<EtapasConclusao> root = cq.from(EtapasConclusao.class);
		// ParameterExpression<Producao> pProducao = qb.parameter(Producao.class);
		// cq.where(qb.equal(root.get("producao"), pProducao));
		// Join<Producao, EtapasConclusao> etapa =
		// root.join(etapaMetaModel.getSet("producao", EtapasConclusao.class));
		// Join<EtapasConclusao, Producao> producao =
		// root.join(metaModel.getSet("producao", Producao.class));
		// cq.multiselect(root,producao);

		// Constructing list of parameters
		List<Predicate> predicates = new ArrayList<Predicate>();

		// Adding predicates in case of parameter not being null

		predicates.add(qb.equal(root.get("producao").get("produto").get("id"), 23l));
		Predicate ativa = (qb.equal(root.get("producao").get("situacao"), Situacao.ATIVA));
		Predicate previsaoAtiva = (qb.equal(root.get("producao").get("situacao"), Situacao.PREVISAO_ATIVA));
		Predicate negociacao = (qb.equal(root.get("producao").get("situacao"), Situacao.NEGOCIACAO));
		predicates.add(qb.or(ativa, previsaoAtiva, negociacao));

		if (razaoSocial != null && razaoSocial != "") {
			System.out.println("entrou no raz�o social");
			predicates.add(qb.like(root.get("producao").get("cliente").get("razaoSocial"), '%' + razaoSocial + '%'));
		}

		if (ano != null && ano != "") {
			System.out.println("entrou no ano");
			predicates.add(qb.equal(root.get("producao").get("ano"), ano));
		}

		if (siglaConsultor != null && siglaConsultor != "") {
			System.out.println("entrou na sigla");
			predicates.add(qb.equal(root.get("producao").get("consultor").get("sigla"), siglaConsultor));
		} else {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Consultor consultorLogado = ((Consultor) auth.getPrincipal());
			Cargo cargo = consultorLogado.getCargo();
			List<Role> roles = consultorLogado.getRoles();
			boolean admin = false;
			for (Role role : roles) {

				if (role.getNome().equalsIgnoreCase("ROLE_ADMIN")) {
					admin = true;
				}
			}

			if (admin = true) {
				System.out.println("entrou no if role admin");
			} else {
				System.out.println("entrou no else de siglaConsultor");
				if (cargo == Cargo.CONSULTOR_TECNICO || cargo == Cargo.CONSULTOR_COMERCIAL
						|| cargo == Cargo.CONSULTOR_COMERCIAL_JUNIOR_3 || cargo == Cargo.CONSULTOR_TECNICO_TRAINEE
						|| cargo == Cargo.CONSULTOR_TRAINEE || cargo == Cargo.CONSULTOR_ESPECIALISTA) {
					predicates.add(qb.equal(root.get("producao").get("consultor").get("id"), consultorLogado.getId()));
				} else if (cargo == Cargo.CONSULTOR_LIDER || cargo == Cargo.CONSULTOR_LIDER_TECNICO
						|| cargo == Cargo.LIDER_TECNICO || cargo == Cargo.LIDER_DE_TRANSFORMACAO_DIGITAL
						|| cargo == Cargo.LIDER_REGIONAL) {
					predicates.add(qb.equal(root.get("producao").get("equipe").get("lider").get("id"),
							consultorLogado.getId()));
				} else if (cargo == Cargo.ESTAGIARIO_TECNICO) {
					Equipe equipe = equipeService.findEquipeByConsultor(consultorLogado.getId());
					predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), equipe.getId()));
				} else if (cargo == Cargo.COORDENADOR_TECNICO) {
					predicates.add(qb.equal(root.get("producao").get("equipe").get("coordenador").get("id"),
							consultorLogado.getId()));
				} else if (cargo == Cargo.GERENTE_GERAL || cargo == Cargo.GERENTE_CONSULTORIA_TECNICA
						|| cargo == Cargo.GERENTE_GERAL || cargo == Cargo.GERENTE_REGIONAL || cargo == Cargo.GERENTE_KAM
						|| cargo == Cargo.GERENTE_DE_NEGOCIOS) {
					predicates.add(qb.equal(root.get("producao").get("equipe").get("gerente").get("id"),
							consultorLogado.getId()));
				}

			}

		}

		if (idEquipe != null) {
			System.out.println("entrou na equipe");
			predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), idEquipe));
		} else {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Consultor consultorLogado = ((Consultor) auth.getPrincipal());
			Cargo cargo = consultorLogado.getCargo();
			List<Role> roles = consultorLogado.getRoles();
			boolean admin = false;
			for (Role role : roles) {
				if (role.getNome().equalsIgnoreCase("ROLE_ADMIN")) {
					admin = true;
				}
			}

			if (admin = true) {
				System.out.println("entrou no if role admin");
			} else {
				System.out.println("entrou no else de equipe");
				if (cargo == Cargo.CONSULTOR_TECNICO || cargo == Cargo.CONSULTOR_COMERCIAL
						|| cargo == Cargo.CONSULTOR_COMERCIAL_JUNIOR_3 || cargo == Cargo.CONSULTOR_TECNICO_TRAINEE
						|| cargo == Cargo.CONSULTOR_TRAINEE || cargo == Cargo.CONSULTOR_ESPECIALISTA) {
					Equipe equipe = equipeService.findEquipeByConsultor(consultorLogado.getId());
					predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), equipe.getId()));
				} else if (cargo == Cargo.CONSULTOR_LIDER || cargo == Cargo.CONSULTOR_LIDER_TECNICO
						|| cargo == Cargo.LIDER_TECNICO || cargo == Cargo.LIDER_DE_TRANSFORMACAO_DIGITAL
						|| cargo == Cargo.LIDER_REGIONAL) {
					predicates.add(qb.equal(root.get("producao").get("equipe").get("lider").get("id"),
							consultorLogado.getId()));
				} else if (cargo == Cargo.ESTAGIARIO_TECNICO) {
					Equipe equipe = equipeService.findEquipeByConsultor(consultorLogado.getId());
					predicates.add(qb.equal(root.get("producao").get("equipe").get("id"), equipe.getId()));
				} else if (cargo == Cargo.COORDENADOR_TECNICO) {
					predicates.add(qb.equal(root.get("producao").get("equipe").get("coordenador").get("id"),
							consultorLogado.getId()));
				} else if (cargo == Cargo.GERENTE_GERAL || cargo == Cargo.GERENTE_CONSULTORIA_TECNICA
						|| cargo == Cargo.GERENTE_GERAL || cargo == Cargo.GERENTE_REGIONAL || cargo == Cargo.GERENTE_KAM
						|| cargo == Cargo.GERENTE_DE_NEGOCIOS) {
					predicates.add(qb.equal(root.get("producao").get("equipe").get("gerente").get("id"),
							consultorLogado.getId()));
				}
			}
		}

		/*
		 * 
		 * if (abertura != null) { System.out.println("entrou na equipe"); Predicate
		 * amConcluido = (qb.equal(root.get("concluido"), abertura)); Predicate
		 * amIdEtapa = (qb.equal(root.get("etapa").get("id"), 63l)); Predicate
		 * etapaConcluida = qb.and(amConcluido, amIdEtapa).in(
		 * root.get("producao").get("id"),amConcluido);
		 * 
		 * System.out.println("o amConcluido �: " + amConcluido); //Predicate producao =
		 * (qb.equal(root.get("producao").get("id"),amConcluido));
		 * //predicates.add(qb.and(amConcluido, amIdEtapa));
		 * //predicates.add(qb.or(etapaConcluida, producao));
		 * //predicates.add(etapaConcluida);
		 * 
		 * //cq.where(root.get("producao").get("id").in(amConcluido));
		 * //predicates.add(qb.and(amConcluido, amIdEtapa)); }
		 * 
		 * if (identificacao != null) { System.out.println("entrou na identificacao");
		 * Predicate Concluido = (qb.equal(root.get("concluido"), identificacao));
		 * Predicate IdEtapa = (qb.equal(root.get("etapa").get("id"), 65l)); //Predicate
		 * producao = (qb.equal(root.get("producao").get("id"), Concluido));
		 * //predicates.add(qb.or(producao, Concluido).in(IdEtapa));
		 * //predicates.add(qb.and(Concluido, IdEtapa).in(producao)); }
		 * 
		 * if (quantificacao != null) { System.out.println("entrou na quantificacao");
		 * Predicate Concluido = (qb.equal(root.get("concluido"), quantificacao));
		 * Predicate IdEtapa = (qb.equal(root.get("etapa").get("id"), 66l));
		 * predicates.add(qb.and(Concluido, IdEtapa)); }
		 * 
		 * if (defesa != null) { System.out.println("entrou na defesa"); Predicate
		 * Concluido = (qb.equal(root.get("concluido"), defesa)); Predicate IdEtapa =
		 * (qb.equal(root.get("etapa").get("id"), 67l));
		 * predicates.add(qb.and(Concluido, IdEtapa)); }
		 * 
		 * if (submissao != null) { System.out.println("entrou na submissao"); Predicate
		 * Concluido = (qb.equal(root.get("concluido"), submissao)); Predicate IdEtapa =
		 * (qb.equal(root.get("etapa").get("id"), 68l));
		 * predicates.add(qb.and(Concluido, IdEtapa)); }
		 * 
		 * if (encerramento != null) { System.out.println("entrou na encerramento");
		 * Predicate Concluido = (qb.equal(root.get("concluido"), encerramento));
		 * Predicate IdEtapa = (qb.equal(root.get("etapa").get("id"), 69l));
		 * predicates.add(qb.and(Concluido, IdEtapa)); }
		 * 
		 * if (mcti != null) { System.out.println("entrou na mcti"); Predicate Concluido
		 * = (qb.equal(root.get("concluido"), mcti)); Predicate IdEtapa =
		 * (qb.equal(root.get("etapa").get("id"), 70l));
		 * predicates.add(qb.and(Concluido, IdEtapa)); }
		 * 
		 * if (rfb != null) { System.out.println("entrou  rfb"); Predicate Concluido =
		 * (qb.equal(root.get("concluido"), rfb)); Predicate IdEtapa =
		 * (qb.equal(root.get("etapa").get("id"), 71l));
		 * predicates.add(qb.and(Concluido, IdEtapa)); }
		 * 
		 */

		// query itself
		cq.orderBy(qb.desc(root.get("producao").get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result
		return em.createQuery(cq).getResultList();

	}

	public List<String> getTotalAnos(Long id) {
		return this.em
				.createNativeQuery("SELECT DISTINCT(ano) FROM producao WHERE contrato_id = " + id + " ORDER BY ano")
				.getResultList();
	}

	public List<Producao> getProducaoByAnoAndContrato(Long id, String ano) {
		return em.createQuery("SELECT p FROM Producao p WHERE p.contrato.id = :cId AND p.ano = :ano", Producao.class)
				.setParameter("cId", id).setParameter("ano", ano).getResultList();
	}

	@Transactional
	public void updatePreProducao(Long id, Long pre) {
		em.createQuery("UPDATE Producao p set p.pre_producao.id = :prId where p.id = :pId").setParameter("prId", pre)
				.setParameter("pId", id).executeUpdate();
	}

	public List<Producao> getProducoesByFinanciamento(String campanha, String razaoSocial, String seguimento,
			Long idEquipe) {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();

		Root<EtapasConclusao> root = cq.from(Producao.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(qb.equal(root.get("produto").get("id"), 29l));
		Predicate ativa = (qb.equal(root.get("situacao"), Situacao.ATIVA));
		predicates.add(qb.or(ativa));

		if (campanha != null && campanha != "") {
			predicates.add(qb.equal(root.get("ano"), campanha));
		}

		if (razaoSocial != null && razaoSocial != "") {
			predicates.add(qb.like(root.get("cliente").get("razaoSocial"), '%' + razaoSocial + '%'));
		}

		if (seguimento != null && seguimento != "") {
			predicates.add(qb.like(root.get("cliente").get("razaoSocial"), '%' + razaoSocial + '%'));
		}

		if (idEquipe != null) {
			predicates.add(qb.equal(root.get("cliente").get("setorAtividade"), seguimento));
		}

		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(predicates.toArray(new Predicate[] {}));

		return em.createQuery(cq).getResultList();
	}

	public List<Producao> getAllProducaoByProdutoAndAno(Long produtoId, String ano) {
		return em.createQuery("SELECT p FROM Producao p WHERE p.produto.id = :pId AND p.ano = :pAno", Producao.class)
				.setParameter("pId", produtoId).setParameter("pAno", ano).getResultList();
	}
}
