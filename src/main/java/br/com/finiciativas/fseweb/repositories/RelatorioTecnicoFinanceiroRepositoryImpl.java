package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.RelatorioTecnicoFinanceiro;

@Repository
public class RelatorioTecnicoFinanceiroRepositoryImpl {

	@Autowired
	private EntityManager em;

	@Transactional
	public void create(RelatorioTecnicoFinanceiro RelatorioTecnicoFinanceiro) {
		this.em.persist(RelatorioTecnicoFinanceiro);
	}

	public void delete(RelatorioTecnicoFinanceiro RelatorioTecnicoFinanceiro) {
		this.em.remove(RelatorioTecnicoFinanceiro);
	}

	@Transactional
	public RelatorioTecnicoFinanceiro update(RelatorioTecnicoFinanceiro RelatorioTecnicoFinanceiro) {
		return this.em.merge(RelatorioTecnicoFinanceiro);
	}

	public List<RelatorioTecnicoFinanceiro> getAll() {
		return this.em
				.createQuery("SELECT distinct o FROM RelatorioTecnicoFinanceiro o ", RelatorioTecnicoFinanceiro.class)
				.getResultList();
	}

	public RelatorioTecnicoFinanceiro findById(Long id) {
		return this.em.find(RelatorioTecnicoFinanceiro.class, id);
	}

	public List<RelatorioTecnicoFinanceiro> getRelatorioTecnicoFinanceiroByAcompanhamento(Long id) {
		return this.em
				.createQuery("SELECT distinct o FROM RelatorioTecnicoFinanceiro o WHERE o.acompanhamento.id = :pId ",
						RelatorioTecnicoFinanceiro.class)
				.setParameter("pId", id).getResultList();
	}

}
