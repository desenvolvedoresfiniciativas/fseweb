package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.cliente.ContatoCliente;

public interface ContatoClienteRepository {
	
	List<ContatoCliente> getContatosCliente(Long id);
	
	void create(ContatoCliente contatoCliente, Long id);
	
	void update(ContatoCliente contatoCliente);
	
	void remove(ContatoCliente contatoCliente);
	
	ContatoCliente findById(Long id);
	
	Long getIdClienteOfContact(Long id);
	
}
