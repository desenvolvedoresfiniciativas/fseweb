package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.FNDCT;

@Repository
public class FNDCTrepositoryImpl {

	@Autowired
	private EntityManager em;

	public List<FNDCT> getAll() {
		return this.em.createQuery("SELECT distinct o FROM FNDCT o ", FNDCT.class)
				.getResultList();
	}

	@Transactional
	public void create(FNDCT fndct) {
		this.em.merge(fndct);
	}

	public void delete(FNDCT fndct) {
		this.em.remove(fndct);
	}

	@Transactional
	public FNDCT update(FNDCT fndct) {
		
		return this.em.merge(fndct);
	}

	public FNDCT findById(Long id) {
		return this.em.find(FNDCT.class, id);
	}

	public FNDCT getFNDCTByProducao(Long id) {
		try{
			return this.em.createQuery("SELECT distinct o FROM FNDCT o WHERE o.producao.id = :pId ", FNDCT.class)
				.setParameter("pId", id)
				.getSingleResult();
		}catch (Exception e) {
			FNDCT fndct = new FNDCT();
			return fndct;
		}
	}
	
}
