package br.com.finiciativas.fseweb.repositories.impl;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.repositories.EmailRepository;

@Repository
public class EmailRepositoryImpl implements EmailRepository {

	@Override
	public void send(String nome, String emailDestinatario, String assunto, String msg) {
		try {

			Email email = new SimpleEmail();
			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("renanluquete@gmail.com", "14899841"));
			email.setSSLOnConnect(true);

			email.setFrom("renanluquete@gmail.com", "Renan Luquete Cavalcante");
			email.setSubject(assunto);
			email.setMsg(msg);
			email.addTo(emailDestinatario);
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
