package br.com.finiciativas.fseweb.repositories.impl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.enums.Cobrado;
import br.com.finiciativas.fseweb.enums.Situacao;
import br.com.finiciativas.fseweb.models.Parcela;
import br.com.finiciativas.fseweb.models.consultor.Comissao;
import br.com.finiciativas.fseweb.models.consultor.Consultor;
import br.com.finiciativas.fseweb.models.contrato.Contrato;
import br.com.finiciativas.fseweb.models.contrato.EtapasPagamento;
import br.com.finiciativas.fseweb.models.faturamento.Faturamento;
import br.com.finiciativas.fseweb.models.faturamento.NotaFiscal;
import br.com.finiciativas.fseweb.models.producao.Producao;
import br.com.finiciativas.fseweb.models.valorNegocio.ValorNegocio;
import br.com.finiciativas.fseweb.repositories.ComissaoRepository;
import br.com.finiciativas.fseweb.services.ClienteService;
import br.com.finiciativas.fseweb.services.impl.ComissaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ConsultorServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ContratoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.FaturamentoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.NotaFiscalServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ParcelaServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ProducaoServiceImpl;
import br.com.finiciativas.fseweb.services.impl.ValorNegocioServiceImpl;

@Repository
public class ComissaoRepositoryImpl implements ComissaoRepository {

	@Autowired
	private EntityManager em;

	private static final Locale LOCAL = new Locale("pt", "BR");

	@Autowired
	private ProducaoServiceImpl producaoService;

	@Autowired
	private ContratoServiceImpl contratoService;

	@Autowired
	private ValorNegocioServiceImpl valorNegocioService;

	@Autowired
	private ComissaoServiceImpl comissaoService;

	@Autowired
	private ConsultorServiceImpl consultorService;

	@Autowired
	private FaturamentoServiceImpl faturamentoService;
	
	@Autowired
	private ParcelaServiceImpl parcelaService;

	@Autowired
	private NotaFiscalServiceImpl notaFiscalService;
	
	@Autowired
	private ClienteService clienteService;

	@Override
	public Comissao create(Comissao comissao) {
		// TODO Auto-generated method stub
		em.persist(comissao);
		return comissao;
	}

	
	@Override
	@Transactional
	public Comissao update(@Valid Comissao comissao) {
			
		em.merge(comissao);
		em.flush();
		return comissao;
		/*
		 * Comissao comissaoAtt = findById(comissao.getId());
		 * System.out.println("entrou no m�todo");
		 * //System.out.println("o funcion�rio �: " + comissao.getConsultor().getId());
		 * 
		 * 
		 * comissaoAtt.setAutorizacao(comissao.isAutorizacao());
		 * comissaoAtt.setBonusEconomico(comissao.getBonusEconomico());
		 * comissaoAtt.setCampanha(comissao.getCampanha());
		 * comissaoAtt.setCobrado(comissao.getCobrado());
		 * comissaoAtt.setComissaoPercentual(comissao.getComissaoPercentual());
		 * comissaoAtt.setComissaoSimulada(comissao.isComissaoSimulada());
		 * comissaoAtt.setComissaoValor(comissao.getComissaoValor());
		 * comissaoAtt.setConsultor(comissao.getConsultor());
		 * comissaoAtt.setContrato(comissao.getContrato());
		 * comissaoAtt.setData_autorizacao(comissao.getData_autorizacao());
		 * comissaoAtt.setData_pagamento(comissao.getData_pagamento());
		 * comissaoAtt.setDataDeRecebimentoCliente(comissao.getDataDeRecebimentoCliente(
		 * )); comissaoAtt.setDataRecebimentoFuncionario(comissao.
		 * getDataRecebimentoFuncionario());
		 * comissaoAtt.setEtapaPagamento(comissao.getEtapaPagamento());
		 * comissaoAtt.setEtapaTrabalho(comissao.getEtapaTrabalho());
		 * comissaoAtt.setFaturamento(comissao.getFaturamento());
		 * comissaoAtt.setNotaFiscal(comissao.getNotaFiscal());
		 * comissaoAtt.setObservacoes(comissao.getObservacoes());
		 * comissaoAtt.setPagamento(comissao.isPagamento());
		 * comissaoAtt.setProducao(comissao.getProducao());
		 * comissaoAtt.setValidado(comissao.isValidado());
		 * comissaoAtt.setValorNegocio(comissao.getValorNegocio());
		 * comissaoAtt.setAtiva(comissao.isAtiva());
		 */
		  
		 
		  
	}

	@Override
	public Comissao findById(Long id) {
		return this.em.find(Comissao.class, id);
	}

	@Override
	public List<Comissao> getAll() {
		return em.createQuery("SELECT c FROM Comissao c", Comissao.class).getResultList();
	}

	@Override
	public void delete(Comissao comissao) {
		// TODO Auto-generated method stub
		this.em.remove(comissao);
	}

	@Override
	public Comissao getLastAdded() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Comissao> getComissaoByConsultor(Long id) {
		try {
			String sQuery = "SELECT c FROM Comissao c WHERE c.consultor.id = :pId";
			return em.createQuery(sQuery, Comissao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Comissao> comis = new ArrayList<Comissao>();
			return comis;
		}
	}
	
	
	public Comissao getComissaoByNotaFiscal(Long id) {
		return this.em.createQuery("SELECT distinct c FROM Comissao c WHERE c.notaFiscal.id = :pId", Comissao.class)
				.setParameter("pId", id).getSingleResult();
	}
	
	
	@Override
	@Transactional
	public List<Comissao> getComissaoAtivaByConsultor(Long id) {
		try {
			String sQuery = "SELECT c FROM Comissao c WHERE c.consultor.id = :pId and c.ativa=1";
			return em.createQuery(sQuery, Comissao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Comissao> comis = new ArrayList<Comissao>();
			return comis;
		}
	}
	
	
	
	@Override
	public List<Comissao> getComissaoByFaturamento(Long id) {
		try {
			String sQuery = "SELECT c FROM Comissao c WHERE c.faturamento.id = :pId";
			return em.createQuery(sQuery, Comissao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Comissao> comissao = new ArrayList<Comissao>();
			return comissao;
		}
	}
	
	public List<Comissao> getComissaoByProdAndProduto(Producao prod, Long id) {
		try {
			String sQuery = "SELECT c FROM Comissao c WHERE c.producao.id = :pId and c.producao.produto.id = :produtoId";
			return em.createQuery(sQuery, Comissao.class).setParameter("pId", prod.getId()).setParameter("produtoId", id).getResultList();
		} catch (Exception e) {
			System.out.println("ERRO NA QUERY DE PRODaNDpRODUTO " + e);
			List<Comissao> comissao = new ArrayList<Comissao>();
			return comissao;
		}
	}
	
	@Override
	public List<Comissao> getComissoesReaisByContrato(Long id) {
		try {
			String sQuery = "SELECT c FROM Comissao c WHERE c.contrato.id = :pId and c.comissaoSimulada = true";
			return em.createQuery(sQuery, Comissao.class).setParameter("pId", id).getResultList();
		} catch (Exception e) {
			System.out.println(e);
			List<Comissao> comissao = new ArrayList<Comissao>();
			return comissao;
		}
	}
	
	

	@Override
	public List<Comissao> getComissaoByGestor(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Comissao> getComissaoByAnalista(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Comissao findByProducao(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Comissao checkIfComissaoExistWithProducao(Long id) {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("SELECT f FROM comissao f WHERE f.producao.id = :pProducao", Comissao.class)
					.setParameter("pProducao", id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Comissao> getAllByProducao(Long id) {
		List<Comissao> comissoes = new ArrayList<Comissao>();
		try {
			comissoes = em.createQuery("SELECT distinct c FROM Comissao c WHERE c.producao.id = :pId", Comissao.class)
					.setParameter("pId", id).getResultList();
			return comissoes;
		} catch (Exception e) {
			System.out.println("Erro no metodo: ComissaoRepositoryImpl.getAllByProducao" + e);
			return comissoes;
		}
	}

	@Transactional
	public void calculaComissaoByAno(String ano) {
		switch (ano) {
		case "2020":
			System.out.println("o ano recebido foi 2020" + ano);
			break;
		case "2021":
			System.out.println("o ano recebido foi 2021" + ano);
			calculaComissao2021(ano);
			break;
		default:
			System.out.println("n�o existe um calculo de comiss�es para o ano selecionado");
			break;
		}
	}
	
	@Transactional
	public void gerarComissaoByConsultor(Long idConsultor) {
		System.out.println("cheguei na repository e o ID recebido foi: " + idConsultor);
		
		List<Producao> producoes =  producaoService.getAllProducaoByConsultorAndNN(idConsultor);
		
		if(!producoes.isEmpty()) {
			for (Producao producao : producoes) {
				System.out.println("encontrou a produ��o" + producao.getId());
				
				if(producao.getAno().equalsIgnoreCase("2021")) {
				
				Contrato contrato = producao.getContrato();
				EtapasPagamento etapaPagamento = contrato.getEtapasPagamento();
				ValorNegocio valorNegocio = valorNegocioService.getVNbyEmpresaAndProducao(producao.getCliente().getId(),
						producao.getId());

				

				//Comissao comissao1 = new Comissao();
				List<Comissao> comissoes = comissaoService.getAllByProducao(producao.getId());

				if (!comissoes.isEmpty()) {
					System.out.println("----------------------Existe comiss�o para essa produ�ao " + producao.getId());
					
					
					

					int iteracao = 0;
					for (Comissao comissao : comissoes) {
						System.out.println("A COMISS�O QUE EST� SENDO VERIFICADA �: " + comissao.getId() );
						if(comissao.isAtiva() == true && comissao.isManual() == false && comissao.isFinalizado() == false) {
						System.out.println("A COMISS�O " + comissao.getId() + "ENTROU NO IF");	
						
						comissao.setCampanha(producao.getAno());
						comissao.setContrato(contrato);
						comissao.setProducao(producao);
						
						//definir percentual do consultor para c�lculo 
						Consultor consultor = contrato.getComercialResponsavelEntrada();
						
						if(consultor.getCargo() == Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
							comissao.setComissaoPercentual(3.5f);
							
						}else {
							comissao.setComissaoPercentual(3.0f);
						}
						
						try {
							if(comissao.getNotaFiscal() != null) {
								System.out.println("VERIFICOU QUE EXISTE UMA NOTA FISCAL PARA A COMISS�O");
								updateComissaoByNota(comissao);
							}
						}catch(Exception e) {
							
						}

						if (producao.getContrato().getComercialResponsavelEntrada() == null
								&& comissao.getConsultor() == null) {
							comissao.setConsultor(consultorService.findById(424l));
						} else if (producao.getContrato().getComercialResponsavelEntrada() == null) {
							comissao.setConsultor(comissao.getConsultor());
						} else if (comissao.getConsultor() == null) {
							comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada());
						}

						//comissao.setBonusEconomico(comissao.getBonusEconomico());
						
						comissao.setValorNegocio(valorNegocio);

						comissao.setDataDeRecebimentoCliente(comissao.getDataDeRecebimentoCliente());
						comissao.setDataRecebimentoFuncionario(comissao.getDataRecebimentoFuncionario());
						comissao.setCobrado(comissao.getCobrado());

						

						comissao.setEtapaPagamento(etapaPagamento);

						float porcentagemEtapa = 0;
						// Confirmar se � necess�rio verificar a itera��o
						if (etapaPagamento.getEtapa1() != null && iteracao == 0) {
							comissao.setEtapaTrabalho(etapaPagamento.getEtapa1());
							porcentagemEtapa = etapaPagamento.getPorcentagem1();

							try {
								// coletando faturamento por producao, etapa de pagamento e contrato
								comissao.setFaturamento(faturamentoService.getFaturamentoByProducaoEtapaContrato2(
										producao.getId(), etapaPagamento.getEtapa1().getId(), contrato.getId()));
								// coletando notafiscal por faturamento
							
								  //comissao.setNotaFiscal(
									//	notaFiscalService.findByFaturamento(comissao.getFaturamento().getId()));
								  
								  
						//		  List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
						//		  for (NotaFiscal nota : notas) {
						//			  System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
						//			if(checkExistByNota(nota.getId()) == false) {
						//				createComissaoByNota(nota);
						//			}else {
						//				Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
						//				updateComissaoByNota(comissaoNota);
						//			}
						//		}
								
							} catch (Exception e) {
								//comissao.setNotaFiscal(null);
								//comissao.setFaturamento(null);
								e.printStackTrace();
							}
						}else if(etapaPagamento.getEtapa2() != null && iteracao == 1) {
							
							comissao.setEtapaTrabalho(etapaPagamento.getEtapa2());
							porcentagemEtapa = etapaPagamento.getPorcentagem2();
							try {
								comissao.setFaturamento(faturamentoService
										.getFaturamentoByProducaoEtapaContrato2(producao.getId(), etapaPagamento.getEtapa2().getId(), contrato.getId()));
							//	comissao.setNotaFiscal(notaFiscalService
								//		.findByFaturamento(comissao.
									//			getFaturamento().getId()));
							//	List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
							//	  for (NotaFiscal nota : notas) {
							//		  System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
							//		if(checkExistByNota(nota.getId()) == false) {
							//			createComissaoByNota(nota);
							//		}else {
							//			Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
							//			updateComissaoByNota(comissaoNota);
							//		}
							//	  }
								  
							} catch (Exception e) {
								//comissao.setNotaFiscal(null);
								//comissao.setFaturamento(null);
								e.printStackTrace();
								
							}
						}else if(etapaPagamento.getEtapa3() != null && iteracao == 2){
							comissao.setEtapaTrabalho(etapaPagamento.getEtapa3());
							porcentagemEtapa = etapaPagamento.getPorcentagem3();
							try {
								comissao.setFaturamento(faturamentoService
										.getFaturamentoByProducaoEtapaContrato2(producao.getId(), etapaPagamento.getEtapa3().getId(), contrato.getId()));
							//	comissao.setNotaFiscal(notaFiscalService
								//		.findByFaturamento(comissao.
									//			getFaturamento().getId()));
								
						//		List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
						//		  for (NotaFiscal nota : notas) {
						//			  System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
						//			if(checkExistByNota(nota.getId()) == false) {
						//				createComissaoByNota(nota);
						//			}else {
						//				Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
						//				updateComissaoByNota(comissaoNota);	
						//				
						//			}
						//		  }
							} catch (Exception e) {
								//comissao.setNotaFiscal(null);
								//comissao.setFaturamento(null);
								e.printStackTrace();
							}
						} else if (etapaPagamento.getEtapa4() != null && iteracao == 3){
							comissao.setEtapaTrabalho(etapaPagamento.getEtapa4());
							porcentagemEtapa = etapaPagamento.getPorcentagem4();
							try {
								comissao.setFaturamento(faturamentoService
										.getFaturamentoByProducaoEtapaContrato2(producao.getId(), etapaPagamento.getEtapa4().getId(), contrato.getId()));
							//	comissao.setNotaFiscal(notaFiscalService
								//		.findByFaturamento(comissao.
									//			getFaturamento().getId()));
								
							//	List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
							//	  for (NotaFiscal nota : notas) {
							//		  System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
							//		if(checkExistByNota(nota.getId()) == false) {
							//			createComissaoByNota(nota);
							//			comissao.setNotaFiscal(nota);
							//			comissaoService.update(comissao);
							//		}else {
							//			Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
							//			updateComissaoByNota(comissaoNota);
							//		}
							//	  }
							} catch (Exception e) {
								//comissao.setNotaFiscal(null);
								//comissao.setFaturamento(null);
								e.printStackTrace();
							}
						}
						
						if(comissao.getNotaFiscal() == null) {
						System.out.println("o id da comiss�o que est� sendo atualizada �: " + comissao.getId());	
						comissao.setComissaoPercentual(comissao.getComissaoPercentual());
						float comissaoPercentual = comissao.getComissaoPercentual();
						
						float totalValorNegocio = 0;
						
						if(producao.getProduto().getId() == 23) {
							totalValorNegocio = valorNegocio.getValorBase().floatValue();
						}else {
							String estimativaComercialString = producao.getContrato().getEstimativaComercial();
							
							estimativaComercialString = estimativaComercialString.replaceAll("\\.", "");
							estimativaComercialString = estimativaComercialString.replace(",", ".");
							
							totalValorNegocio = Float.parseFloat(estimativaComercialString);
							 
						}
						
						 
						
						float iss = 5f;
						float cofins = 3f;
						float csll = 1.0f;
						float pis = 0.65f;
						float irpj = 1.5f;
						
						float valorEtapa = (porcentagemEtapa / 100) * totalValorNegocio;

						float valorISS = (iss / 100) * valorEtapa;
						float valorCOFINS = (cofins / 100) * valorEtapa;
						float valorPIS = (pis / 100) * valorEtapa;
						float valorCSLL = (csll / 100) * valorEtapa;
						float valorIRPJ = (irpj / 100) * valorEtapa;
						
						float aposImpostos = valorEtapa - (valorISS + valorCOFINS + valorPIS + valorCSLL + valorIRPJ);
						

						float valorFinal = (comissaoPercentual / 100) * aposImpostos;
						
						System.out.println("O VALOR DA ETAPA �: " + valorEtapa);
						System.out.println("o valor dos impostos s�o: ISS: " + valorISS + "confins " + valorCOFINS + "PIS " + valorPIS + "CRLL: " + valorCSLL + "IRPJ: " + valorIRPJ);	
						System.out.println("O VALOR AP�S OS IMPOSTOS �: " + aposImpostos);
						System.out.println("O VALOR FINAL SEM NOTA FISCAL �: " + valorFinal);
						
						

						String valorFinalString = Float.toString(valorFinal);

						DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
						valorFinalString = df.format(valorFinal);

						comissao.setComissaoValor(valorFinalString);
						

						//if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
						//	comissao.setComissaoSimulada(false);
						//} else {
						//	comissao.setComissaoSimulada(true);
						//}
						
						comissao.setData_autorizacao(comissao.getData_autorizacao());
						comissao.setData_pagamento(comissao.getData_pagamento());
						comissao.setObservacoes(comissao.getObservacoes());
						update(comissao);
						}
						}
						iteracao++;
					}

				} else {
					System.out.println("N�o existe comiss�o para essa produ�ao " + producao.getId());
					// condi��o para verificar se o contrato possui comiss�o em outro ano de
					// campanha
					System.out.println(
							"---------TESTE DE VERIFICA��O DE COMISS�O " + contrato.isComissaoGerada() + "-------------");
					
					if(producao.getTipoDeNegocio().equalsIgnoreCase("NN") && producao.getAno().equalsIgnoreCase("2021")) {
						
						
						Comissao comissao = new Comissao();
						try {
							
						
						if (producao.getContrato().getComercialResponsavelEntrada() == null) {
							comissao.setConsultor(consultorService.findById(424l));
						} else {
							comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada());
						}
						
						String estimativaComercialString = producao.getContrato().getEstimativaComercial();
						
						estimativaComercialString = estimativaComercialString.replaceAll("\\.", "");
						estimativaComercialString = estimativaComercialString.replace(",", ".");
						
						float estimativaComercial = Float.parseFloat(estimativaComercialString);
						System.out.println("A ESTIMATIVA COMERCIAL FORMATADA �: " + estimativaComercial);
						
						float iss = 5f;
						float cofins = 3f;
						float csll = 1.0f;
						float pis = 0.65f;
						float irpj = 1.5f;

						float valorISS = (iss / 100) * estimativaComercial;
						float valorCOFINS = (cofins / 100) * estimativaComercial;
						float valorPIS = (pis / 100) * estimativaComercial;
						float valorCSLL = (csll / 100) * estimativaComercial;
						float valorIRPJ = (irpj / 100) * estimativaComercial;
						
						
						
						float aposImpostos = estimativaComercial - (valorISS + valorCOFINS + valorPIS + valorCSLL + valorIRPJ);
						
						System.out.println("o valor dos impostos s�o: ISS: " + valorISS + "confins " + valorCOFINS + "PIS " + valorPIS + "CRLL: " + valorCSLL + "IRPJ: " + valorIRPJ);	
						System.out.println("-------- O VALOR AP�S OS IMPOSTOS �: " + aposImpostos);
						
						float comissaoPercentual;
						
						//Regra para que o Wenderson receba a comiss�o de 3.5%
						if(comissao.getConsultor().getCargo() != Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
							comissao.setComissaoPercentual(3.0f);
							comissaoPercentual = comissao.getComissaoPercentual();
						}else {
							comissao.setComissaoPercentual(3.5f);
							comissaoPercentual = comissao.getComissaoPercentual();
						}

						float valorFinal = (comissaoPercentual / 100) * aposImpostos;

						String valorFinalString = Float.toString(valorFinal);

						DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
						valorFinalString = df.format(valorFinal);

						comissao.setComissaoValor(valorFinalString);
						} catch(Exception e) {
							e.printStackTrace();
						}

						//if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
						//	comissao.setComissaoSimulada(false);
						//} else {
						//	comissao.setComissaoSimulada(true);
						//}

						comissao.setData_autorizacao("");
						comissao.setData_pagamento("");
						comissao.setObservacoes("Insira as observacoes aqui");
						comissao.setAtiva(true);
						comissao.setComissaoSimulada(true);
						comissao.setContrato(producao.getContrato());
						comissao.setProducao(producao);
						System.out.println("A PRODU��O DESSA COMISS�O �: " + comissao.getProducao().getId());
						comissao.setDataCriacao(java.util.Calendar.getInstance().getTime());
						comissao.setCampanha(producao.getAno());
						
						create(comissao);
						System.out.println("----------------COMISS�O PARA OUTROS PRODUTOS GERADA--------------------------------");
					}	
					if (contrato.isComissaoGerada() == false) {
						System.out.println("verificou que n�o existe uma comiss�o para a produ��o");
						
						try {
						
						if (Objects.nonNull(etapaPagamento.getEtapa1())) {
							geraComissaoByEtapa("1", producao, contrato, etapaPagamento, valorNegocio);
						} 
						if (Objects.nonNull(etapaPagamento.getEtapa2())) {
							geraComissaoByEtapa("2", producao, contrato, etapaPagamento, valorNegocio);
						} 
						if (Objects.nonNull(etapaPagamento.getEtapa3())) {
							geraComissaoByEtapa("3", producao, contrato, etapaPagamento, valorNegocio);
						} 
						
						  if (Objects.nonNull(etapaPagamento.getEtapa4())) { geraComissaoByEtapa("4",
						  producao, contrato, etapaPagamento, valorNegocio); }
						 
						 
						}catch(Exception e){
							System.out.println("erro etapa null: " + e);
						}
						
						 

					}
				}
				}else {
					System.out.println("produ��o n�o � de 2021");
				}
			}
		}else {
			System.out.println("lista vazia");
		}
		
		List<NotaFiscal> notas = notaFiscalService.getNotasByComercialEntrada(idConsultor);
		for (NotaFiscal nota : notas) {
			boolean existeComissao = checkExistByNota(nota.getId());
			if(existeComissao == true) {
				updateComissaoByNota(getComissaoByNotaFiscal(nota.getId()));
			}else {
				createComissaoByNota(nota);
			}
		}
		
		
	}
	
	public void calculaComissao2021(String ano) {
		System.out.println("entrou no m�todo que calcula comiss�o 2021");
		
		//c�digo para produ��o
		List<Producao> producoes = producaoService.getAllAtivasByAnoWhereNN(ano);
		
		
		
		//c�digo para teste de cliente
		//List <Producao> producoes = producaoService.getProducoesByCliente((long) 2220);
		
		//c�digo para teste de produ��o 
		//Producao producaoTeste = producaoService.findById(2221l);
		//List<Producao> producoes = new ArrayList();
		//producoes.add(producaoTeste);
		
		
		
		for (Producao producao : producoes) {
			System.out.println(producao.getCliente().getRazaoSocial() + "id da produ��o: " + producao.getId());
			System.out.println("------------------------ENTROU NO LOOP-------------------");
			System.out.println( "O ESTADO DA PRODU��O �: " + producao.getSituacao());
			System.out.println("O ID DOPRODUTO �: " + producao.getProduto().getId());
			System.out.println("O ANO DE CAMPANHA �: " + producao.getAno());
			
			
			
			Contrato contrato = producao.getContrato();
			EtapasPagamento etapaPagamento = contrato.getEtapasPagamento();
			//System.out.println(etapaPagamento.getId());
			ValorNegocio valorNegocio = valorNegocioService.getVNbyEmpresaAndProducao(producao.getCliente().getId(),
					producao.getId());

			

			//Comissao comissao1 = new Comissao();
			List<Comissao> comissoes = comissaoService.getAllByProducao(producao.getId());

			if (!comissoes.isEmpty()) {
				System.out.println("----------------------Existe comiss�o para essa produ�ao " + producao.getId());
				
				
				

				
				for (Comissao comissao : comissoes) {
					System.out.println("A COMISS�O QUE EST� SENDO VERIFICADA �: " + comissao.getId() );
					if(comissao.getProducao().getSituacao() == Situacao.INATIVA) {
						comissao.setAtiva(false);
						comissaoService.update(comissao);
					}
					
					if(comissao.isAtiva() == true && comissao.isManual() == false && comissao.isFinalizado() == false) {
					System.out.println("A COMISS�O " + comissao.getId() + "ENTROU NO IF");	
					
					comissao.setCampanha(producao.getAno());
					comissao.setContrato(contrato);
					comissao.setProducao(producao);
					
					//definir percentual do consultor para c�lculo 
					Consultor consultor = contrato.getComercialResponsavelEntrada();
					
					try {
						if(consultor.getCargo() == Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
							comissao.setComissaoPercentual(3.5f);
							
						}else {
							comissao.setComissaoPercentual(3.0f);
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					
					
					try {
						if(comissao.getNotaFiscal() != null) {
							System.out.println("VERIFICOU QUE EXISTE UMA NOTA FISCAL PARA A COMISS�O");
							updateComissaoByNota(comissao);
						}
					}catch(Exception e) {
						
					}

					if (producao.getContrato().getComercialResponsavelEntrada() == null
							&& comissao.getConsultor() == null) {
						comissao.setConsultor(consultorService.findById(424l));
					} else if (producao.getContrato().getComercialResponsavelEntrada() == null) {
						comissao.setConsultor(comissao.getConsultor());
					} else if (comissao.getConsultor() == null) {
						comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada());
					}

					//comissao.setBonusEconomico(comissao.getBonusEconomico());
					comissao.setValorNegocio(valorNegocio);
					valorNegocio = comissao.getValorNegocio();

					comissao.setDataDeRecebimentoCliente(comissao.getDataDeRecebimentoCliente());
					comissao.setDataRecebimentoFuncionario(comissao.getDataRecebimentoFuncionario());
					comissao.setCobrado(comissao.getCobrado());

					//try {
					//	comissao.setFaturamento(comissao.getFaturamento());
					//	comissao.setNotaFiscal(comissao.getNotaFiscal());
					//} catch (Exception e) {
						// System.out.println("Nao forao encontrados faturamento e nota fiscal na
						// comissao a ser atualizada");
					//	comissao.setNotaFiscal(null);
					//	comissao.setFaturamento(null);
					//}

					//comissao.setEtapaPagamento(etapaPagamento);
					float porcentagemEtapa = 0;
					//c�digo para comissoes com entrega por tarefas
					if(contrato.isPagamentoEntrega() == true) {
						for (int iteracao = 0; iteracao<=3; iteracao++) {
							
							// Confirmar se � necess�rio verificar a itera��o
							if (etapaPagamento.getEtapa1() != null && iteracao == 0) {
								comissao.setEtapaTrabalho(etapaPagamento.getEtapa1());
								porcentagemEtapa = etapaPagamento.getPorcentagem1();
								System.out.println("entrou na etapa de pagamento 1");
								try {
									// coletando faturamento por producao, etapa de pagamento e contrato
									comissao.setFaturamento(faturamentoService.getFaturamentoByProducaoEtapaContrato2(
											producao.getId(), etapaPagamento.getEtapa1().getId(), contrato.getId()));
									// coletando notafiscal por faturamento
								
									  //comissao.setNotaFiscal(
										//	notaFiscalService.findByFaturamento(comissao.getFaturamento().getId()));
									  
									  
							//		  List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
							//		  for (NotaFiscal nota : notas) {
							//			  System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
							//			if(checkExistByNota(nota.getId()) == false) {
							//				createComissaoByNota(nota);
							//			}else {
							//				Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
							//				updateComissaoByNota(comissaoNota);
							//			}
							///		}
									
								} catch (Exception e) {
									//comissao.setNotaFiscal(null);
									//comissao.setFaturamento(null);
									//e.printStackTrace();
								}
							} if(etapaPagamento.getEtapa2() != null && iteracao == 1) {
								System.out.println("entrou na etapa de pagamento 2");
								comissao.setEtapaTrabalho(etapaPagamento.getEtapa2());
								porcentagemEtapa = etapaPagamento.getPorcentagem2();
								try {
									comissao.setFaturamento(faturamentoService
											.getFaturamentoByProducaoEtapaContrato2(producao.getId(), etapaPagamento.getEtapa2().getId(), contrato.getId()));
								//	comissao.setNotaFiscal(notaFiscalService
									//		.findByFaturamento(comissao.
										//			getFaturamento().getId()));
								//	List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
								//	  for (NotaFiscal nota : notas) {
								//		  System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
								//		if(checkExistByNota(nota.getId()) == false) {
								//			createComissaoByNota(nota);
								//		}else {
								//			Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
								//			updateComissaoByNota(comissaoNota);
								//		}
								//	  }
									  
								} catch (Exception e) {
									//comissao.setNotaFiscal(null);
									//comissao.setFaturamento(null);
									//e.printStackTrace();
									
								}
							} if(etapaPagamento.getEtapa3() != null && iteracao == 2){
								System.out.println("entrou na etapa de pagamento 3");
								comissao.setEtapaTrabalho(etapaPagamento.getEtapa3());
								porcentagemEtapa = etapaPagamento.getPorcentagem3();
								try {
									comissao.setFaturamento(faturamentoService
											.getFaturamentoByProducaoEtapaContrato2(producao.getId(), etapaPagamento.getEtapa3().getId(), contrato.getId()));
								//	comissao.setNotaFiscal(notaFiscalService
									//		.findByFaturamento(comissao.
										//			getFaturamento().getId()));
									
									//List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
									  //for (NotaFiscal nota : notas) {
										 // System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
										//if(checkExistByNota(nota.getId()) == false) {
										//	createComissaoByNota(nota);
										//}else {
										//	Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
										//	updateComissaoByNota(comissaoNota);	
											
										//}
									 // }
								} catch (Exception e) {
									//comissao.setNotaFiscal(null);
									//comissao.setFaturamento(null);
									//e.printStackTrace();
								}
							}  if (etapaPagamento.getEtapa4() != null && iteracao == 3){
								System.out.println("entrou na etapa de pagamento 4");
								comissao.setEtapaTrabalho(etapaPagamento.getEtapa4());
								porcentagemEtapa = etapaPagamento.getPorcentagem4();
								try {
									comissao.setFaturamento(faturamentoService
											.getFaturamentoByProducaoEtapaContrato2(producao.getId(), etapaPagamento.getEtapa4().getId(), contrato.getId()));
								//	comissao.setNotaFiscal(notaFiscalService
									//		.findByFaturamento(comissao.
										//			getFaturamento().getId()));
									
							//	List<NotaFiscal> notas = notaFiscalService.getNotasByFaturamento(comissao.getFaturamento().getId());
							//	  for (NotaFiscal nota : notas) {
							//		  System.out.println("--------------- ENCONTROU UMA NOTA FISCAL PARA O TIMING---------------");
							//		if(checkExistByNota(nota.getId()) == false) {
							//			createComissaoByNota(nota);
							//			}else {
							//				Comissao comissaoNota = getComissaoByNotaFiscal(nota.getId());
							//				updateComissaoByNota(comissaoNota);
							//			}
							//		  }
								} catch (Exception e) {
									//comissao.setNotaFiscal(null);
									//comissao.setFaturamento(null);
								//	e.printStackTrace();
								}
							}
						}
					}//c�digo para comiss�o com entrega por data
					//if(contrato.isPagamentoData()) {
					//	comissao = updateComissaoData(comissao);
					//}

					
					
					if(comissao.getNotaFiscal() == null) {
					System.out.println("o id da comiss�o que est� sendo atualizada �: " + comissao.getId());	
					comissao.setComissaoPercentual(comissao.getComissaoPercentual());
					float comissaoPercentual = comissao.getComissaoPercentual();
					try {
						float totalValorNegocio = valorNegocio.getValorBase().floatValue();
						
						float iss = 5f;
						float cofins = 3f;
						float csll = 1.0f;
						float pis = 0.65f;
						float irpj = 1.5f;
						
						float valorEtapa = (porcentagemEtapa / 100) * totalValorNegocio;

						float valorISS = (iss / 100) * valorEtapa;
						float valorCOFINS = (cofins / 100) * valorEtapa;
						float valorPIS = (pis / 100) * valorEtapa;
						float valorCSLL = (csll / 100) * valorEtapa;
						float valorIRPJ = (irpj / 100) * valorEtapa;
						
						float aposImpostos = valorEtapa - (valorISS + valorCOFINS + valorPIS + valorCSLL + valorIRPJ);
						

						float valorFinal = (comissaoPercentual / 100) * aposImpostos;
						
						System.out.println("O VALOR DA ETAPA �: " + valorEtapa);
						System.out.println("o valor dos impostos s�o: ISS: " + valorISS + "confins " + valorCOFINS + "PIS " + valorPIS + "CRLL: " + valorCSLL + "IRPJ: " + valorIRPJ);	
						System.out.println("O VALOR AP�S OS IMPOSTOS �: " + aposImpostos);
						System.out.println("O VALOR FINAL SEM NOTA FISCAL �: " + valorFinal);
						
						

						String valorFinalString = Float.toString(valorFinal);

						DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
						valorFinalString = df.format(valorFinal);

						comissao.setComissaoValor(valorFinalString);
					}catch(Exception e) {
						
					}
					
					
					
					

					//if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
					//	comissao.setComissaoSimulada(false);
					//} else {
					//	comissao.setComissaoSimulada(true);
					//}
					
					comissao.setComissaoSimulada(true);
					comissao.setData_autorizacao(comissao.getData_autorizacao());
					comissao.setData_pagamento(comissao.getData_pagamento());
					comissao.setObservacoes(comissao.getObservacoes());
					update(comissao);
					
					//atualizar comiss�o caso seja outros produtos
					if(producao.getProduto().getId() != 23) {
						if(producao.getTipoDeNegocio().equalsIgnoreCase("NN")) {
							
						
						
						try {
							
						
						if (producao.getContrato().getComercialResponsavelEntrada() == null) {
							comissao.setConsultor(consultorService.findById(424l));
						} else {
							comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada());
						}
						
						String estimativaComercialString = producao.getContrato().getEstimativaComercial();
						
						estimativaComercialString = estimativaComercialString.replaceAll("\\.", "");
						estimativaComercialString = estimativaComercialString.replace(",", ".");
						
						float estimativaComercial = Float.parseFloat(estimativaComercialString);
						System.out.println("A ESTIMATIVA COMERCIAL FORMATADA �: " + estimativaComercial);
						
						float iss = 5f;
						float cofins = 3f;
						float csll = 1.0f;
						float pis = 0.65f;
						float irpj = 1.5f;

						float valorISS = (iss / 100) * estimativaComercial;
						float valorCOFINS = (cofins / 100) * estimativaComercial;
						float valorPIS = (pis / 100) * estimativaComercial;
						float valorCSLL = (csll / 100) * estimativaComercial;
						float valorIRPJ = (irpj / 100) * estimativaComercial;
						
						
						
						float aposImpostos = estimativaComercial - (valorISS + valorCOFINS + valorPIS + valorCSLL + valorIRPJ);
						
						System.out.println("o valor dos impostos s�o: ISS: " + valorISS + "confins " + valorCOFINS + "PIS " + valorPIS + "CRLL: " + valorCSLL + "IRPJ: " + valorIRPJ);	
						System.out.println("-------- O VALOR AP�S OS IMPOSTOS �: " + aposImpostos);
						

						
						//Regra para que o Wenderson receba a comiss�o de 3.5%
						if(comissao.getConsultor().getCargo() != Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
							comissao.setComissaoPercentual(3.0f);
							comissaoPercentual = comissao.getComissaoPercentual();
						}else {
							comissao.setComissaoPercentual(3.5f);
							comissaoPercentual = comissao.getComissaoPercentual();
						}

						float valorFinal = (comissaoPercentual / 100) * aposImpostos;

						String valorFinalString = Float.toString(valorFinal);

						DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
						valorFinalString = df.format(valorFinal);

						comissao.setComissaoValor(valorFinalString);
						update(comissao);
						}catch(Exception e) {
							
						}
					}else {
						comissao.setAtiva(false);
					}
					}	
					}
					
					
					}
					
				}
				
				
			} else {
				System.out.println("N�o existe comiss�o para essa produ�ao " + producao.getId());
				// condi��o para verificar se o contrato possui comiss�o em outro ano de
				// campanha
				
				if(producao.getProduto().getId() != 23) {
					if(producao.getTipoDeNegocio().equalsIgnoreCase("NN") && producao.getAno().equalsIgnoreCase("2021")) {
						
					
					Comissao comissao = new Comissao();
					try {
						
					
					if (producao.getContrato().getComercialResponsavelEntrada() == null) {
						comissao.setConsultor(consultorService.findById(424l));
					} else {
						comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada());
					}
					
					String estimativaComercialString = producao.getContrato().getEstimativaComercial();
					
					estimativaComercialString = estimativaComercialString.replaceAll("\\.", "");
					estimativaComercialString = estimativaComercialString.replace(",", ".");
					
					float estimativaComercial = Float.parseFloat(estimativaComercialString);
					System.out.println("A ESTIMATIVA COMERCIAL FORMATADA �: " + estimativaComercial);
					
					float iss = 5f;
					float cofins = 3f;
					float csll = 1.0f;
					float pis = 0.65f;
					float irpj = 1.5f;

					float valorISS = (iss / 100) * estimativaComercial;
					float valorCOFINS = (cofins / 100) * estimativaComercial;
					float valorPIS = (pis / 100) * estimativaComercial;
					float valorCSLL = (csll / 100) * estimativaComercial;
					float valorIRPJ = (irpj / 100) * estimativaComercial;
					
					
					
					float aposImpostos = estimativaComercial - (valorISS + valorCOFINS + valorPIS + valorCSLL + valorIRPJ);
					
					System.out.println("o valor dos impostos s�o: ISS: " + valorISS + "confins " + valorCOFINS + "PIS " + valorPIS + "CRLL: " + valorCSLL + "IRPJ: " + valorIRPJ);	
					System.out.println("-------- O VALOR AP�S OS IMPOSTOS �: " + aposImpostos);
					
					float comissaoPercentual;
					
					//Regra para que o Wenderson receba a comiss�o de 3.5%
					if(comissao.getConsultor().getCargo() != Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
						comissao.setComissaoPercentual(3.0f);
						comissaoPercentual = comissao.getComissaoPercentual();
					}else {
						comissao.setComissaoPercentual(3.5f);
						comissaoPercentual = comissao.getComissaoPercentual();
					}

					float valorFinal = (comissaoPercentual / 100) * aposImpostos;

					String valorFinalString = Float.toString(valorFinal);

					DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
					valorFinalString = df.format(valorFinal);

					comissao.setComissaoValor(valorFinalString);
					} catch(Exception e) {
						e.printStackTrace();
					}

					//if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
					//	comissao.setComissaoSimulada(false);
					//} else {
					//	comissao.setComissaoSimulada(true);
					//}

					comissao.setData_autorizacao("");
					comissao.setData_pagamento("");
					comissao.setObservacoes("Insira as observacoes aqui");
					comissao.setAtiva(true);
					comissao.setComissaoSimulada(true);
					comissao.setContrato(producao.getContrato());
					comissao.setProducao(producao);
					System.out.println("A PRODU��O DESSA COMISS�O �: " + comissao.getProducao().getId());
					comissao.setDataCriacao(java.util.Calendar.getInstance().getTime());
					comissao.setCampanha(producao.getAno());
					
					create(comissao);
					System.out.println("----------------COMISS�O PARA OUTROS PRODUTOS GERADA--------------------------------");
					
				}
				
				System.out.println(
						"---------TESTE DE VERIFICA��O DE COMISS�O " + contrato.isComissaoGerada() + "-------------");
				if (contrato.isComissaoGerada() == false) {
					System.out.println("verificou que n�o existe uma comiss�o para a produ��o");
					
					try {
					
					if (Objects.nonNull(etapaPagamento.getEtapa1())) {
						geraComissaoByEtapa("1", producao, contrato, etapaPagamento, valorNegocio);
					} 
					if (Objects.nonNull(etapaPagamento.getEtapa2())) {
						geraComissaoByEtapa("2", producao, contrato, etapaPagamento, valorNegocio);
					} 
					if (Objects.nonNull(etapaPagamento.getEtapa3())) {
						geraComissaoByEtapa("3", producao, contrato, etapaPagamento, valorNegocio);
					} 
					
					  if (Objects.nonNull(etapaPagamento.getEtapa4())) { geraComissaoByEtapa("4",
					  producao, contrato, etapaPagamento, valorNegocio); }
					 
					 
					}catch(Exception e){
						System.out.println("erro etapa null: " + e);
					}
					
					

				}
			}
			}

		}
		
		List<NotaFiscal> notas = notaFiscalService.getNotasByCampanha(ano);
		for (NotaFiscal nota : notas) {
			boolean existeComissao = checkExistByNota(nota.getId());
			if(existeComissao == true) {
				updateComissaoByNota(getComissaoByNotaFiscal(nota.getId()));
			}else {
				createComissaoByNota(nota);
			}
		}
	}

	private Comissao updateComissaoData(Comissao comissao) {
		// TODO Auto-generated method stub
		return comissao;
	}
	
	private void createComissaoData(Producao producao, Contrato contrato, ValorNegocio valorNegocio) {
		Comissao comissao = new Comissao();

		comissao.setCampanha(producao.getAno());
		comissao.setContrato(contrato);
		comissao.setProducao(producao);

		// Confirmar regra posteriormente
		if (producao.getContrato().getComercialResponsavelEntrada() == null) {
			comissao.setConsultor(consultorService.findById(424l));
		} else {
			comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada());
		}

		
		if (Objects.nonNull(valorNegocio)) {
			System.out.println("encontrou o valor de neg�cio");
			
			comissao.setValorNegocio(valorNegocio);
			//valorNegocio = comissao.getValorNegocio();

			comissao.setDataDeRecebimentoCliente("");
			comissao.setDataRecebimentoFuncionario("");
			comissao.setCobrado(Cobrado.NAO);
			
			try {
				comissao.setFaturamento(faturamentoService.getFaturamentoByProd(producao.getId()));
			}catch(Exception e) {
				e.printStackTrace();
			}
			 

			//float porcentagemEtapa = 0;

			
			
			float comissaoPercentual;
			
			//Regra para que o Wenderson receba a comiss�o de 3.5%
			if(comissao.getConsultor().getCargo() != Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
				comissao.setComissaoPercentual(3.0f);
				comissaoPercentual = comissao.getComissaoPercentual();
			}else {
				comissao.setComissaoPercentual(3.5f);
				comissaoPercentual = comissao.getComissaoPercentual();
			}
			
			
			System.out.println("-----------TESTE VALOR DE NEG�CIO O ID �: " + valorNegocio.getId());
			float totalValorNegocio = valorNegocio.getValorBase().floatValue();
			System.out.println(
					"-------------------VALOR TOTAL NEG�CIO " + totalValorNegocio + "--------------------------------");
			float valorEtapa = 100 * totalValorNegocio;
			 
			float iss = 5f;
			float cofins = 3f;
			float csll = 1.0f;
			float pis = 0.65f;
			float irpj = 1.5f;

			float valorISS = (iss / 100) * valorEtapa;
			float valorCOFINS = (cofins / 100) * valorEtapa;
			float valorPIS = (pis / 100) * valorEtapa;
			float valorCSLL = (csll / 100) * valorEtapa;
			float valorIRPJ = (irpj / 100) * valorEtapa;
			
			
			
			float aposImpostos = valorEtapa - (valorISS + valorCOFINS + valorPIS + valorCSLL + valorIRPJ);
			
			System.out.println("o valor dos impostos s�o: ISS: " + valorISS + "confins " + valorCOFINS + "PIS " + valorPIS + "CRLL: " + valorCSLL + "IRPJ: " + valorIRPJ);	
			System.out.println("-------- O VALOR DA ETAPA �: " + valorEtapa);
			System.out.println("-------- O VALOR AP�S OS IMPOSTOS �: " + aposImpostos);
			

			float valorFinal = (comissaoPercentual / 100) * aposImpostos;

			String valorFinalString = Float.toString(valorFinal);

			DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
			valorFinalString = df.format(valorFinal);

			comissao.setComissaoValor(valorFinalString);

			//if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
			//	comissao.setComissaoSimulada(false);
			//} else {
			//	comissao.setComissaoSimulada(true);
			//}

			comissao.setData_autorizacao("");
			comissao.setData_pagamento("");
			comissao.setObservacoes("Insira as observacoes aqui");
			comissao.setAtiva(true);
			comissao.setDataCriacao(java.util.Calendar.getInstance().getTime());
			
			create(comissao);
			System.out.println("----------------COMISS�O POR DATA GERADA--------------------------------");
	
		}
	}


	public void geraComissaoByEtapa(String etapa, Producao producao, Contrato contrato, EtapasPagamento etapaPagamento,
			ValorNegocio valorNegocio) {
		System.out.println("entrou no m�todo geraComissaoByEtapa");
		Comissao comissao = new Comissao();

		comissao.setCampanha(producao.getAno());
		comissao.setContrato(contrato);
		comissao.setProducao(producao);

		// Confirmar regra posteriormente
		if (producao.getContrato().getComercialResponsavelEntrada() == null) {
			comissao.setConsultor(consultorService.findById(424l));
		} else {
			comissao.setConsultor(producao.getContrato().getComercialResponsavelEntrada());
		}

		
		if (Objects.nonNull(valorNegocio)) {
			System.out.println("encontrou o valor de neg�cio");
			
			comissao.setValorNegocio(valorNegocio);
			//valorNegocio = comissao.getValorNegocio();

			comissao.setDataDeRecebimentoCliente("");
			comissao.setDataRecebimentoFuncionario("");
			comissao.setCobrado(Cobrado.NAO);

			try {
				if (etapa == "1") {
					comissao.setFaturamento(faturamentoService.getFaturamentoByProducaoEtapaContrato2(producao.getId(),
							etapaPagamento.getEtapa1().getId(), contrato.getId()));
				}
				if (etapa == "2") {
					comissao.setFaturamento(faturamentoService.getFaturamentoByProducaoEtapaContrato2(producao.getId(),
							etapaPagamento.getEtapa2().getId(), contrato.getId()));
				}
				if (etapa == "3") {
					comissao.setFaturamento(faturamentoService.getFaturamentoByProducaoEtapaContrato2(producao.getId(),
							etapaPagamento.getEtapa3().getId(), contrato.getId()));
				}
				if (etapa == "4") {
					comissao.setFaturamento(faturamentoService.getFaturamentoByProducaoEtapaContrato2(producao.getId(),
							etapaPagamento.getEtapa4().getId(), contrato.getId()));
				}
				

			} catch (Exception e) {
				comissao.setFaturamento(null);

			}
			
			

			float porcentagemEtapa = 0;

			comissao.setEtapaPagamento(etapaPagamento);
			if (etapa == "1") {
				comissao.setEtapaTrabalho(etapaPagamento.getEtapa1());
				porcentagemEtapa = etapaPagamento.getPorcentagem1();
			}
			if (etapa == "2") {
				comissao.setEtapaTrabalho(etapaPagamento.getEtapa2());
				porcentagemEtapa = etapaPagamento.getPorcentagem2();
			}
			if (etapa == "3") {
				comissao.setEtapaTrabalho(etapaPagamento.getEtapa3());
				porcentagemEtapa = etapaPagamento.getPorcentagem3();
			}
			if (etapa == "4") {
				comissao.setEtapaTrabalho(etapaPagamento.getEtapa4());
				porcentagemEtapa = etapaPagamento.getPorcentagem4();
			}
			
			float comissaoPercentual;
			
			//Regra para que o Wenderson receba a comiss�o de 3.5%
			if(comissao.getConsultor().getCargo() != Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
				comissao.setComissaoPercentual(3.0f);
				comissaoPercentual = comissao.getComissaoPercentual();
			}else {
				comissao.setComissaoPercentual(3.5f);
				comissaoPercentual = comissao.getComissaoPercentual();
			}
			System.out.println("-----------TESTE VALOR DE NEG�CIO O ID �: " + valorNegocio.getId());
			float totalValorNegocio = valorNegocio.getValorBase().floatValue();
			System.out.println(
					"-------------------VALOR TOTAL NEG�CIO " + totalValorNegocio + "--------------------------------");
			float valorEtapa = 0;
			if(contrato.isPagamentoEntrega()) {
				 valorEtapa = (porcentagemEtapa / 100) * totalValorNegocio;
			}else {
				  valorEtapa =  100 * totalValorNegocio;
			}
			

			float iss = 5f;
			float cofins = 3f;
			float csll = 1.0f;
			float pis = 0.65f;
			float irpj = 1.5f;

			float valorISS = (iss / 100) * valorEtapa;
			float valorCOFINS = (cofins / 100) * valorEtapa;
			float valorPIS = (pis / 100) * valorEtapa;
			float valorCSLL = (csll / 100) * valorEtapa;
			float valorIRPJ = (irpj / 100) * valorEtapa;
			
			
			
			float aposImpostos = valorEtapa - (valorISS + valorCOFINS + valorPIS + valorCSLL + valorIRPJ);
			
			System.out.println("o valor dos impostos s�o: ISS: " + valorISS + "confins " + valorCOFINS + "PIS " + valorPIS + "CRLL: " + valorCSLL + "IRPJ: " + valorIRPJ);	
			System.out.println("-------- O VALOR DA ETAPA �: " + valorEtapa);
			System.out.println("-------- O VALOR AP�S OS IMPOSTOS �: " + aposImpostos);
			

			float valorFinal = (comissaoPercentual / 100) * aposImpostos;

			String valorFinalString = Float.toString(valorFinal);

			DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
			valorFinalString = df.format(valorFinal);

			comissao.setComissaoValor(valorFinalString);

			//if (valorNegocio.getModoValor().equalsIgnoreCase("finalizado")) {
			//	comissao.setComissaoSimulada(false);
			//} else {
			//	comissao.setComissaoSimulada(true);
			//}

			comissao.setData_autorizacao("");
			comissao.setData_pagamento("");
			comissao.setObservacoes("Insira as observacoes aqui");
			comissao.setComissaoSimulada(true);
			comissao.setAtiva(true);
			comissao.setDataCriacao(java.util.Calendar.getInstance().getTime());
			
			create(comissao);
			System.out.println("----------------COMISS�O GERADA--------------------------------");

			// Criar m�todo para atualizar que a comiss�o foi gerada
			//contrato.setComissaoGerada(true);
			contratoService.update(contrato);
		}
	}
	
	@Override
	public void createComissaoByNota(NotaFiscal nota) {
		System.out.println("ENTROU NO M�TODO QUE CRIA NOTA FISCAL");
		System.out.println("o id da nota �: " + nota.getId());
		
		Contrato contrato = nota.getContrato();
		Faturamento fat = nota.getFaturamento();
		Producao prod = producaoService.getProducaoByAnoEmpresaProduto(fat.getCampanha(), nota.getCliente().getId(), fat.getProduto().getId());
		//System.out.println("O ID DA PRODU��O �: " + prod.getId());
		//System.out.println("o tipo de neg�cio � " + prod.getTipoDeNegocio());

		try {

		if(prod.getTipoDeNegocio().equalsIgnoreCase("NN")) {
		System.out.println("novo neg�cio");	
		
		Comissao comissao = new Comissao();
		comissao.setProducao(prod);
		//try {
		//	Producao prod = producaoService.findById(nota.getFaturamento().getProducao().getId());
		//	System.out.println("O TIPO DE NEG�CIO �: " + prod.getTipoDeNegocio());
		//	System.out.println("O ANO DE CAMPANHA �: " + prod.getAno());
		//	comissao.setProducao(prod);
		//}catch(Exception e) {
		//	System.out.println("nota sem produ��o");
		//}
		
		
		//if(prod.getTipoDeNegocio().equalsIgnoreCase("nn") && prod.getAno().equalsIgnoreCase("2021")) {
		//System.out.println("ENTROU NA CONDI��O");
		
		
		
		
		Consultor consultor = nota.getContrato().getComercialResponsavelEntrada();
		
		List<Faturamento> faturamentos = faturamentoService.getContrato(contrato.getId());
		List<Comissao> comissoes = comissaoService.getComissoesReaisByContrato(contrato.getId());
		
		// Condi��o para verificar se j� existem comiss�es para cada timing de faturamento
    	//if(comissoes.size() < faturamentos.size()) {

		
		
		comissao.setConsultor(consultor);
		comissao.setFaturamento(nota.getFaturamento());
		comissao.setContrato(nota.getContrato());
		comissao.setCampanha(nota.getFaturamento().getCampanha());
		comissao.setNotaFiscal(nota);
		
		
		if(consultor.getCargo() == Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
			comissao.setComissaoPercentual(3.5f);
		}else {
			comissao.setComissaoPercentual(3.0f);
		}
		
		
		//C�digo para "substituir" comiss�o simulada por uma comiss�o real
		
		List<Comissao> comissoesSimuladas = new ArrayList<Comissao>();
		
		if(prod.getProduto().getId() == 23l) {
			comissoesSimuladas = comissaoService.getComissaoByFaturamento(nota.getFaturamento().getId());
			System.out.println("produto lei do bem");
		}else {
			comissoesSimuladas = comissaoService.getComissaoByProdAndProduto(prod, prod.getProduto().getId());
			System.out.println("nota outros produtos");
		}
		
		
		
		comissao.setDataCriacao(java.util.Calendar.getInstance().getTime());
		if(!comissoesSimuladas.isEmpty()) {
			for (Comissao comissaoSimulada : comissoesSimuladas) {
				if(comissaoSimulada.isComissaoSimulada() == true) {
					//System.out.println("o faturamento �: " + comissaoSimulada.getFaturamento().getId());
					comissaoSimulada.setAtiva(false);
					comissaoService.update(comissaoSimulada);
					comissao.setDataCriacao(comissaoSimulada.getDataCriacao());
					System.out.println("EXISTE UMA COMISS�O SIMULADA PARA ESSE FATURAMENTO"); 
				}else {
					comissao.setDataCriacao(java.util.Calendar.getInstance().getTime());
				}
			}
		}
		
		try {
			String valorCobrancaString = nota.getCobrar();
			String valorBrutoString = nota.getFaturar();
			System.out.println("o valor antes de atualizar �: " + valorCobrancaString);
			valorBrutoString = valorBrutoString.replaceAll("\\.", "");
			valorBrutoString = valorBrutoString.replace(",", ".");
			
		
			 valorCobrancaString = valorCobrancaString.replaceAll("\\.", "");
			 System.out.println("O VALOR DE COBRAN�A AP�S TIRAR O PONTO: " + valorCobrancaString);
			 valorCobrancaString = valorCobrancaString.replace(",", ".");
			 System.out.println("O VALOR DE COBRAN�A EST� ESCRITO COMO: " + valorCobrancaString);
			float resultado = 0;
			float valorLiquido = Float.parseFloat(valorCobrancaString);
			float valorBruto = Float.parseFloat(valorBrutoString);
			
			
			resultado = valorLiquido - ((5f / 100) * valorBruto);
			System.out.println("o valor com a redu��o de impostos �: " + resultado);

			float valorFinal = (comissao.getComissaoPercentual() / 100) * resultado;
			System.out.println("o valor calculado �" + valorFinal);
			
			String valorFinalString = Float.toString(valorFinal);

			DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
			valorFinalString = df.format(valorFinal);

			comissao.setComissaoValor(valorFinalString);
		
		
		
		
		comissao.setComissaoSimulada(false);
		comissao.setAtiva(true);
		comissao.setDataCriacao(comissao.getDataCriacao());
	    create(comissao);
	    System.out.println("COMISS�O CRIADA COM SUCESSO!!!!!!!!!!!!!!!!!!!");
	    nota.setComissao(comissao);
	    notaFiscalService.update(nota);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	    
	    
	   
		//}else {
			//System.out.println("essa nota excedeu o limite de comiss�es");
		//   }
		}
		}catch(Exception e) {
			
		}

		
		}
	


	public void updateComissaoByNota(Comissao comissao) {
		System.out.println("ENTROU NO M�TODO QUE ATUALIZA AS COMISS�ES");
		NotaFiscal nota = comissao.getNotaFiscal();
		
		try {
			Producao prod = producaoService.findById(nota.getFaturamento().getProducao().getId());
			System.out.println("O TIPO DE NEG�CIO �: " + prod.getTipoDeNegocio());
			System.out.println("O ANO DE CAMPANHA �: " + prod.getAno());
			comissao.setProducao(prod);
		}catch(Exception e) {
			System.out.println("nota sem produ��o");
		}
		
		
		
		//if(prod.getTipoDeNegocio().equalsIgnoreCase("nn") && prod.getAno().equalsIgnoreCase("2021")) {
		//System.out.println("ENTROU NA CONDI��O");

		
		
		Consultor consultor  = nota.getContrato().getComercialResponsavelEntrada();

		
		// Condi��o para verificar se j� existem comiss�es para cada timing de faturamento
    	//if(comissoes.size() < faturamentos.size()) {

		
		
		comissao.setConsultor(consultor);
		comissao.setFaturamento(nota.getFaturamento());
		comissao.setContrato(nota.getContrato());
		comissao.setCampanha(nota.getFaturamento().getCampanha());
		comissao.setNotaFiscal(nota);
		
		
		if(Objects.nonNull(consultor.getCargo()) && consultor.getCargo() == Cargo.CONSULTOR_COMERCIAL_JUNIOR_3) {
			comissao.setComissaoPercentual(3.5f);
		}else {
			comissao.setComissaoPercentual(3.0f);
		}
		
		
		//C�digo para "substituir" comiss�o simulada por uma comiss�o real
		
		List<Comissao> comissoesSimuladas = comissaoService.getComissaoByFaturamento(nota.getFaturamento().getId());
		
		if(!comissoesSimuladas.isEmpty()) {
			System.out.println("ENCONTROU UMA COMISS�O SIMULADA");
			for (Comissao comissaoSimulada : comissoesSimuladas) {
				comissao.setEtapaPagamento(comissaoSimulada.getEtapaPagamento());
				comissao.setEtapaTrabalho(comissaoSimulada.getEtapaTrabalho());
				comissao.setValorNegocio(comissaoSimulada.getValorNegocio());
				if(comissaoSimulada.isComissaoSimulada() == true) {
					System.out.println("o faturamento �: " + comissaoSimulada.getFaturamento().getId());
					comissaoSimulada.setAtiva(false);
					comissaoService.update(comissaoSimulada);
					System.out.println("EXISTE UMA COMISS�O SIMULADA PARA ESSE FATURAMENTO"); 
				}
			}
		}
		
			System.out.println("O VALOR NA VARI�VEL COBRAR �: " + nota.getCobrar());
		try {
			String valorBrutoString = nota.getFaturar();
			String valorLiquidoString = nota.getCobrar();
			
			valorLiquidoString = valorLiquidoString.replaceAll("\\.", "");
			System.out.println("O VALOR LIQUIDO SEM PONTOS �: " + valorLiquidoString);
			valorLiquidoString = valorLiquidoString.replace(",", ".");
			System.out.println("O VALOR LIQUIDO SEM VIRGULA �: " + valorLiquidoString);
			 valorBrutoString = valorBrutoString.replaceAll("\\.", "");
			 System.out.println("O VALOR DE COBRAN�A AP�S TIRAR O PONTO: " + valorBrutoString);
			 valorBrutoString = valorBrutoString.replace(",", ".");
			 System.out.println("O VALOR DE COBRAN�A EST� ESCRITO COMO: " + valorBrutoString);
			
			float valorLiquido = Float.parseFloat(valorLiquidoString);
			float valorBruto = Float.parseFloat(valorBrutoString);
			System.out.println("o valor sem o desconto de impostos �: " + valorLiquido);
			
			//valorLiquido = (valorBruto - (5f / 100)) * valorLiquido;
			System.out.println("o valor com a redu��o de impostos �: " + valorLiquido);
			System.out.println("O VALOR SEMM ISS �: " +  (valorBruto * (5f / 100)));
			float resultado = valorLiquido - (valorBruto * (5f / 100));

			float valorFinal = (comissao.getComissaoPercentual() / 100) * resultado;
			System.out.println("o valor calculado �" + valorFinal);
			
			String valorFinalString = Float.toString(valorFinal);

			DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));
			valorFinalString = df.format(valorFinal);

			comissao.setComissaoValor(valorFinalString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		  
		
		if(nota.getParcela() > 0) {
			System.out.println("A NOTA POSSUI PARCELAS");
			List<Parcela> parcelas = parcelaService.getParcelasByNF(nota.getId());
			for (Parcela parcela : parcelas) {
				if(parcela.isPago() == true) {
					System.out.println("A PARCELA ESTA PAGA: " + parcela.getId() + parcela.getDataCobranca());
					
					String dataPagamento = nota.getDataCobranca();
					String mesPagamento =  dataPagamento.substring(3, 5);
					comissao.setTrimestrePagamento(findTrimestrePagamento(mesPagamento));
					comissao.setFinalizado(true);
					
				}
			}
		}else {
		
			if(nota.getDataCobranca() != null && nota.getDataCobranca() != "") {
				System.out.println("o id da nota � " + nota.getId() + "a data e: " + nota.getDataCobranca());
				String dataPagamento = nota.getDataCobranca();
				String mesPagamento =  dataPagamento.substring(3, 5);
				comissao.setTrimestrePagamento(findTrimestrePagamento(mesPagamento));
				if(comissao.getTrimestrePagamento() == "1") {
					String anoPagamento = dataPagamento.substring(6);
					int anoPagamentoInt = Integer.parseInt(anoPagamento);
					anoPagamentoInt++;
					comissao.setAnoPagamento(Integer.toString(anoPagamentoInt));
					
				}else {
					String anoPagamento = dataPagamento.substring(6);
					comissao.setAnoPagamento(anoPagamento);
				}
			
				comissao.setFinalizado(true);
			
			}
		
		}
		
		
		
	    update(comissao);
	    System.out.println("COMISS�O ATUALIZADA COM SUCESSO!!!!!!!!!!!!!!!!!!!");
		
	   if(nota.isCancelada()) {
		   comissao.setAtiva(false);
		   update(comissao);
	   }
	    
	   
		//}else {
			//System.out.println("essa nota excedeu o limite de comiss�es");
		//}
		
		}
	
	//}
	
	public String findTrimestrePagamento(String mesCobranca) {
		
		
		
		switch (mesCobranca) {
		
		case "07":
			return "4";
			
			
		case "08":
			return "4";
		
			
		case "09":
			return "4";
		
			
		case "10":
			return "1";
	
		case "11":
			return "1";
		
			
		case "12":
			return "1";
	
			
		case "01":
			return "2";
		
		case "02":
			return "2";
	
		case "03":
			return "2";
		
		
		case "04":
			return "3";
	
		
		case "05":
			return "3";
	
			
		case "06":
			return "3";
		
			
		default:
			return "ERRO NO M�TODO QUE VERIFICA O TRIMESTRE DE COBRAN�A";
			
		}
	}
	
	public boolean checkExistByNota(Long id) {
		System.out.println("ENTROU NO M�TODO QUE CHECA SE A COMISS�O EXISTE");
		
			//String sQuery = "SELECT c FROM Comissao c WHERE c.NotaFiscal.id = :pId";
			 //Comissao comissao = em.createQuery(sQuery, Comissao.class).setParameter("pId", id).getSingleResult();
		
		try {
			Comissao comissao = em.createQuery("SELECT c FROM Comissao c WHERE c.notaFiscal.id = :pId", Comissao.class)
					.setParameter("pId", id).getSingleResult();
			System.out.println("O ID DA COMISS�O � " + comissao.getId());
			
			if (Objects.isNull(comissao)) {
				 System.out.println("n�o existe comiss�o");
				 return false;
			 } else {
				 System.out.println("existe comiss�o: " + comissao.getId());
				 return true;
			 }
		} catch(Exception e) {
			System.out.println("O ERRO �" +e);
			//e.printStackTrace();
			return false;
		}
			 
		
	}
	
	public void unlinkComissaoOfVNByProducao(Long id) {
		List<Comissao> comissoes = getAllByProducao(id);
		for (Comissao comissao : comissoes) {
			System.out.println(comissao.getId());
			comissao.setValorNegocio(null);
			em.merge(comissao);
		}
	}
	
	public void unlinkComissaoOfProducao(Long id) {
		List<Comissao> comissoes = getAllByProducao(id);
		for (Comissao comissao : comissoes) {
			System.out.println(comissao.getId());
			comissao.setProducao(null);
			em.merge(comissao);
		}
	}

	public List<Comissao> getComissoesByCriteria(String ano,Boolean pagamento, Boolean simulada, String trimestrePagamento, String anoPagamento, Long id, Long idProduto, String empresa, Long etapa) {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery cq = qb.createQuery();
		Root<Comissao> root = cq.from(Comissao.class);

		// Constructing list of parameters
		List<Predicate> comissoes = new ArrayList<Predicate>();
		
		comissoes.add(qb.equal(root.get("ativa"), true));
		comissoes.add(qb.equal(root.get("consultor").get("id"), id));
		
		if(idProduto != null) {
			System.out.println("Entrou produto");
			comissoes.add(qb.equal(root.get("producao").get("produto").get("id"), idProduto));
		}
		
		if (empresa != null && empresa != "") {
			System.out.println("Entrou empresa");
			comissoes.add(qb.like(root.get("contrato").get("cliente").get("razaoSocial"), "%" + empresa + "%"));
		}
		
		if(etapa != null) {
			System.out.println("Entrou etapa");
			comissoes.add(qb.equal(root.get("etapaTrabalho").get("id"), etapa));
		}
		
		if (ano != null && ano != "") {
			System.out.println("Entrou ano");
			comissoes.add(qb.equal(root.get("campanha"), ano));
		}
		
		if (pagamento != null) {
			System.out.println("Entrou pagamento");
			comissoes.add(qb.equal(root.get("pagamento"), pagamento));
		}
		
		if (simulada != null) {
			System.out.println("Entrou simulada");
			comissoes.add(qb.equal(root.get("comissaoSimulada"), simulada));
		}
		
		if (trimestrePagamento != null && trimestrePagamento != "") {
			System.out.println("Entrou trimestrePagamento");
			comissoes.add(qb.equal(root.get("trimestrePagamento"), trimestrePagamento));
		}
		
		if (anoPagamento != null && anoPagamento != "") {
			System.out.println("Entrou anoPagamento");
			comissoes.add(qb.equal(root.get("anoPagamento"), anoPagamento));
		}
	
		//if (idProduto != null) {
		//	System.out.println("Entrou produto");
			// faturamentos.add(qb.equal(root.get("produto").get("id"), idProduto));
	//		faturamentos.add(qb.equal(root.get("produto").get("id"), idProduto));
	//	}
		//if (empresa != null) {
			//System.out.println("Entrou empresa");
			//faturamentos.add(qb.like(root.get("razaoSocialFaturar"), "%" + empresa + "%"));
		//}
		//if (idFilial != null) {
			//System.out.println("Entrou filial");
			//faturamentos.add(qb.equal(root.get("producao").get("filial").get("id"), idFilial));
		//}
		
		//if (etapa != null) {
			//System.out.println("entrou no if etapa" + etapa);
			//faturamentos.add(qb.equal(root.get("etapa").get("id"), etapa));
		//}

		// query itself
		cq.orderBy(qb.desc(root.get("id")));
		cq.select(root).where(comissoes.toArray(new Predicate[] {}));

		return em.createQuery(cq).getResultList();
	}
	
	@Transactional
	public void deleteByContrato(Long idContrato) {
		em.createQuery("DELETE FROM Comissao c WHERE c.contrato.id = :pId")
				.setParameter("pId", idContrato)
				.executeUpdate();
	}
	
	@Transactional
	public void deleteByFaturamento(Long idFaturamento) {
		em.createQuery("DELETE FROM Comissao c WHERE c.faturamento.id = :pId")
				.setParameter("pId", idFaturamento)
				.executeUpdate();
	}
}
