package br.com.finiciativas.fseweb.repositories;

import java.util.List;

import br.com.finiciativas.fseweb.models.instituicao.Instituicao;

public interface InstituicaoRepository {
	
	void create(Instituicao instituicao);
	
	List<Instituicao> getAll();
	
	Instituicao update(Instituicao instituicao);
	
	Instituicao findById(Long id);
	
	void remove(Instituicao instituicao);
	
	Instituicao loadFullEmpresa(Long id);
	
	void delete(Long id);
	
}
