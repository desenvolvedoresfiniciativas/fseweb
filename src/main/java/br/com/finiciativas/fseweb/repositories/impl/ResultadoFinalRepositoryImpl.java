package br.com.finiciativas.fseweb.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.finiciativas.fseweb.models.producao.acompanhamento.ResultadoFinal;

@Repository
public class ResultadoFinalRepositoryImpl {

	@Autowired
	private EntityManager em;
	
	@Transactional
	public void create(ResultadoFinal resultado) {
		this.em.persist(resultado);
	}

	public void delete(ResultadoFinal resultado) {
		this.em.remove(resultado);
	}

	@Transactional
	public ResultadoFinal update(ResultadoFinal resultado) {
		return this.em.merge(resultado);
	}
	
	public List<ResultadoFinal> getAll() {
		return this.em.createQuery("SELECT distinct o FROM ResultadoFinal o ", ResultadoFinal.class)
				.getResultList();
	}

	public ResultadoFinal findById(Long id) {
		return this.em.find(ResultadoFinal.class, id);
	}

	public List<ResultadoFinal> getResultadoFinalByAcompanhamento(Long id) {
		return this.em.createQuery("SELECT distinct o FROM ResultadoFinal o WHERE o.acompanhamento.id = :pId ", ResultadoFinal.class)
				.setParameter("pId", id)
				.getResultList();
	}
	
}
