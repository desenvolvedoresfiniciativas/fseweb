package br.com.finiciativas.fseweb.presenters;

import br.com.finiciativas.fseweb.enums.Cargo;
import br.com.finiciativas.fseweb.models.consultor.Consultor;

public class ConsultorPresenter {

	private Long id;
	private String username;
	private String nome;
	private String email;
	private Cargo cargo;
	private boolean ativo;

	public ConsultorPresenter(Consultor consultor) {

		if (consultor != null) {
			this.id = consultor.getId();
			this.username = consultor.getUsername();
			this.nome = consultor.getNome();
			this.email = consultor.getEmail();
			this.cargo = consultor.getCargo();
			this.ativo = consultor.isAtivo();
		}

	}
	
	public Consultor converter(){
		Consultor consultor = new Consultor();
		consultor.setId(this.id);
		consultor.setUsername(this.username);
		consultor.setNome(this.nome);
		consultor.setEmail(this.email);
		consultor.setCargo(this.cargo);
		consultor.setAtivo(this.ativo);
		return consultor;
	}

	public ConsultorPresenter() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
