package br.com.finiciativas.fseweb.conf;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.TemplateResolver;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import br.com.finiciativas.fseweb.repositories.impl.ValorNegocioCORepositoryImpl;

@SpringBootApplication(scanBasePackages = "br.com.finiciativas.fseweb")
@EntityScan(basePackages = "br.com.finiciativas.fseweb.models")
@EnableJpaRepositories(basePackages = "br.com.finiciativas.fseweb.repositories")
@EnableScheduling
@EnableAsync
public class SpringBootConfig extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootConfig.class, args);
	}

	@Bean(destroyMethod = "close")
	public DataSource getDataSource() throws PropertyVetoException {
		
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		
		// -- Drivers de Conex�o --
		//MySQL
		//dataSource.setDriverClass("com.mysql.jdbc.Driver");

		//SQL
		dataSource.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		
		// -- Strings de Conex�o --
		//AZURE
		//PRE
		//dataSource.setJdbcUrl("jdbc:sqlserver://fitechbr-pre.database.windows.net:1433;database=APP-FSE-PRE;user=managefi@fitechbr-pre;password=celsius@DMZ;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;");
		//PROD
		dataSource.setJdbcUrl("jdbc:sqlserver://fitechbr-prod.database.windows.net:1433;database=FSIWeb-Prod;user=ProFSEWEB.user;password=9iRhMjTK6aIV;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;");
		
		//PROD COLOMBIA
		//dataSource.setJdbcUrl("jdbc:sqlserver://fitechbr-prod.database.windows.net:1433;database=APP-FSECO-PRO;user=PROCO.user;password=JRq9xw7jbkwUah;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;");
		
		//PROD CHILE
		//dataSource.setJdbcUrl("jdbc:sqlserver://fitechbr-prod.database.windows.net:1433;database=APP-FSECL-PRO;user=PROCL.user;password=x8OR28V6EBpuJN1;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;");
				
		// OLD

		//dataSource.setJdbcUrl("jdbc:mysql://FIniciativasServer:3307/nfse");
		//dataSource.setJdbcUrl("jdbc:mysql://FIniciativasServer:3307/nfseDev");
		//dataSource.setJdbcUrl("jdbc:mysql://FIniciativasServer:3307/nfseChile");
		//dataSource.setJdbcUrl("jdbc:mysql://FIniciativasServer:3307/nfseChileDev");
		//dataSource.setJdbcUrl("jdbc:mysql://10.33.1.210:3307/nfseColombia");
		  
		//dataSource.setUser("root");
		//	dataSource.setPassword("AdminFI");	
		
		dataSource.setMinPoolSize(40);
		dataSource.setMaxPoolSize(80);
		
		return dataSource;
		
	}

	@Bean
	public SpringTemplateEngine templateEngine(TemplateResolver templateResolver) {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addDialect(new SpringSecurityDialect());
		templateEngine.setTemplateResolver(templateResolver);
		return templateEngine;
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(SpringBootConfig.class);
	}

}
